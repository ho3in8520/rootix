<?php
use Illuminate\Support\Facades\Route;
///// Route Admin
Route::namespace("App\Http\Controllers\Admin\Auth")->group(function () {
    Route::get('login', 'LoginController@loginForm')->name('admin.login.form');
    Route::post('login', 'LoginController@login')->name('admin.login');
});
Route::namespace("App\Http\Controllers\Admin")->group(function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('dashboard', "AdminController@dashboard")->name('admin.dashboard');
    Route::get('support', "AdminController@support_form")->name('admin.support_form');
    Route::post('support', "AdminController@support")->name('admin.support');
    Route::get('laws', "AdminController@laws_form")->name('admin.laws_form');
    Route::post('laws', "AdminController@laws")->name('admin.laws');

    Route::get('setting', "SettingController@form")->name('admin.setting');
    Route::post('setting/monetary-unit', "SettingController@monetary_unit")->name('admin.setting.monetary-unit');
    Route::post('setting/trade-swap-status', "SettingController@trade_swap_status")->name('admin.setting.trade-swap-status');
    Route::post('setting/admin-notifs-status','SettingController@admin_notifs_status')->name('admin.setting.notifs-status');

    Route::get('wd-currency-setting','CurrencyController@form')->name('admin.wd-currency-setting');
    Route::post('wd-currency-status','CurrencyController@wd_currency_status')->name('admin.wd-currency-status');
//    Route::get('manage-withdraw-swap-page', 'AdminController@manage_withdraw_swap_page')->name('manage-withdraw-swap-page');
//    Route::post('manage_withdraw_update', 'AdminController@manage_withdraw_update')->name('manage-withdraw-update');
//    Route::post('manage-swap-currency', 'AdminController@manage_swap_update')->name('manage-swap-update');
    Route::post('change-unit', 'AdminController@change_unit')->name('change-unit');
    Route::post('change-unit-for-withdraw', 'AdminController@change_unit_for_withdraw')->name('change-unit-for-withdraw');

    Route::post('withdraw-rial/reject/{id}', 'WithdrawRialController@reject')->name('admin.withdraw-rial.reject');
    Route::post('withdraw-rial/confirm/{id}', 'WithdrawRialController@confirm')->name('admin.withdraw-rial.confirm');
    Route::resource('withdraw-rial', 'WithdrawRialController', ['as' => 'admin']);


    // مدیریت کارمزد ها
    Route::get('manage/fees', "AdminController@fees_index")->name('admin.manage.fees.index');
    Route::post('manage/fees', "AdminController@fees_store")->name('admin.manage.fees.store');


    //    TRADE MANAGE
//    Route::get('trade-management', 'AdminController@manage_trade_page')->name('trade-management');
//    Route::post('trade-fee-update', 'AdminController@trad_fee_update')->name('trad-fee-update');
//    Route::post('trade-fees-update', 'AdminController@trad_fees_update')->name('trad-fees-update'); // اینو خانم سواری گفت post کنم
    Route::get('get-currency-fees', 'AdminController@get_currency_fees')->name('get-currency-fees');
    Route::get('set-fees', 'AdminController@set_fees')->name('set-fees');

    Route::get('users', 'UserController@index')->name('user.index');
    Route::get('users/authenticate/{user}', 'UserController@form_authenticate')->name('user.authenticate.form');
    Route::get('users/authenticate', 'UserController@authenticate')->name('user.authenticate.index');
    Route::get('users/{user}', 'UserController@show')->name('user.show');
    Route::post('users/{user}/wallet', 'UserController@wallet')->name('admin.users.wallet');
    Route::get('user/changeStatus', 'UserController@change_status');
    Route::post('user/{user}/edit', 'UserController@edit')->name('admin.user.edit');
//    Route::post('user/{user}/role', 'UserController@role')->name('admin.user.manage-role');
    Route::get('user/{user}/login-panel', 'UserController@login_panel')->name('admin.user.login-panel');
    Route::get('user/disableStatus', 'UserController@disable_status');
    Route::get('tickets', 'TicketController@index')->name('ticket.admin.index');
    Route::get('ticket/create', 'TicketController@create')->name('ticket.admin.create');
    Route::post('ticket/store', 'TicketController@store')->name('ticket.admin.store');
    Route::get('my_tickets', 'TicketController@index')->name('my_ticket.admin.index');
    Route::get('ticket/{id}', 'TicketController@show')->name('ticket.admin.show');
    Route::get('ticket/ban/{id}', 'TicketController@ban')->name('ticket.admin.ban');
    Route::get('get-users', 'TicketController@users')->name('admin.users');
    Route::delete('ticket/destroy/{id}', 'TicketController@destroy')->name('ticket.destroy');
    Route::post('ticket/route/{id}', 'TicketController@route')->name('ticket.route');
    Route::post('comment/{id}', 'TicketController@comment')->name('comment.admin.store');
    Route::resource('currencies', 'CurrencyController');
    Route::get('roles', "RolesController@index")->name('roles.index');
    Route::get('roles/create', "RolesController@create")->name('roles.create');
    Route::post('roles/store', "RolesController@store")->name('roles.store');
    Route::get('roles/{id}/edit', "RolesController@edit")->name('roles.edit');
    Route::post('roles/{id}/update', "RolesController@update")->name('roles.update');
    Route::delete('roles/delete', "RolesController@destroy")->name('roles.destroy');
    Route::get('banks', 'BankController@index')->name('admin.bank.index');
    Route::post('banks/{bank}/confirm', 'BankController@confirm')->name('admin.bank.confirm');
    Route::post('banks/{bank}/reject', 'BankController@reject')->name('admin.bank.reject');
    Route::resource('notifications', 'NotificationController');
    Route::get('get-user', 'NotificationController@users')->name('notifications.get_user');
    Route::get('reports', 'ReportController@index')->name('admin.report.index');
    Route::resource('posts', 'PostController');
    Route::post('posts/reject', 'PostController@reject')->name('posts.reject');
    Route::post('posts/accept', 'PostController@accept')->name('posts.accept');
    Route::resource('categories', 'CategoryController');
    Route::post('categories/delete', 'CategoryController@delete')->name('categories.delete');
//    Route::post('manage-fee-withdraw-admin-update', 'AdminController@fee_withdraw_admin_update')->name('manage-fee-withdraw-admin-update');
    Route::get('request/withdraw', 'WDController@index_withdraw')->name('admin.request.withdraw');
    Route::get('request/withdraw/{id}/show', 'WDController@show_withdraw')->name('admin.request.withdraw.show');
    Route::post('request/withdraw/balance/admin', 'WDController@balance_admin')->name('admin.request.withdraw.balance-admin');
    Route::post('request/withdraw/update-status', 'WDController@update_status')->name('admin.request.withdraw.update-status');

    Route::resource('announcements','AnnouncementController')->names('admin.ann');
});
