<?php

use App\Jobs\SendSms;
use App\Mail\LoginMail;
use App\Models\BaseData;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Morilog\Jalali\Jalalian;
use PragmaRX\Google2FA\Google2FA;
use PragmaRX\Google2FALaravel\Support\Authenticator;

//use Kavenegar;


function get_price_token($unit = 'btt')
{
    $response2 = Http::withoutVerifying()->withHeaders(['content-type: application/json'])->post('https://api.nobitex.ir/market/stats', [
        'srcCurrency' => 'rls',
        'dstCurrency' => 'usdt'
    ]);
    return $response2['global'];
}

function toFixed($number)
{
    return number_format(floor($number * 1000) / 1000, 3);
}

function verifyGatewayIrankish(array $param)
{

    $terminalID = '08052518';
    $password = '5688823446637855';
    $acceptorId = "992180008052518";
    $pub_key = '-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqjnFp+hLOuHrNj1baxi0r7RZP
mrxzAVpa68SBLFSyBZM0AoRQdZUF9InsaDvxqmoRud1kPolopBVfZd++DJkAaD4q
HGL6oDv7Hq5N7xcK3u3blq0g0BCXxs1q/W4jUvfvN81Y/kx0DNO9nQ5dKDS5yj+g
xLgfAdM8GN0GpWnRNQIDAQAB
-----END PUBLIC KEY-----';

    $data = array_merge($param, [
        "terminalId" => $terminalID,
    ]);

    $data_string = json_encode($data);
    $ch = curl_init('https://ikc.shaparak.ir/api/v3/confirmation/purchase');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string)
    ));

    $result = curl_exec($ch);
    $result = json_decode($result);

    return $result;
}

function smsVerify($message, $number, $mode)
{
    try {

        $receptor = $number;//Receptors numbers

        Kavenegar::VerifyLookup($receptor, $message, '', '', $mode);
    } catch (\Kavenegar\Exceptions\ApiException $e) {
        // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
        echo $e->errorMessage();
    } catch (\Kavenegar\Exceptions\HttpException $e) {
        // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
        echo $e->errorMessage();
    }
//    \sms()->send($message)->to($number)->dispatch();
}

function google2faVerify(\Illuminate\Http\Request $request)
{
    $authenticator = app(Authenticator::class)->boot($request);
    if ($authenticator->isAuthenticated()) {
        return true;
    }
    return $authenticator->makeRequestOneTimePasswordResponse();
}

function upload($file, $type, $resize = false, $width = null, $height = null)
{
//    $guard = getCurrentGuard();
    $guard = ''; // چون جدول ادمین با جدول یوزر یکی شد اپلود با گارد به مشکل میخورد
    $path = \Illuminate\Support\Facades\Storage::disk('uploaded')->put(auth($guard)->user()->id . '/' . $type, $file);
    if ($resize) {
        $pa = explode('/', $path);
        $path_1 = storage_path('app\uploaded' . DIRECTORY_SEPARATOR . $pa[0] . DIRECTORY_SEPARATOR . $pa[1] . DIRECTORY_SEPARATOR . $pa[2]);
        $path_1 = str_replace('\\', '/', $path_1);
        $image = new \Imagick($path_1);
        $image->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1);
        $image->writeImage($path_1);
        $image->destroy();
    }
    return $path;
}

function getImage($path, $mode = null)
{
    $ex = explode('/', $path);
    if ($mode = 'download')
        return route("image.download", ['type' => $ex[0], 'id' => $ex[1], 'file' => $ex[2]]);
    else
        return route("image.view", ['type' => $ex[0], 'id' => $ex[1], 'file' => $ex[2]]);

}

function amountTransactionDays()
{
    $transaction = \App\Models\BaseData::where('type', 'transactions')->where('extra_field1', 1)->first();
//    $result= \App\Models\Asset::select([
//        '*',
//        'total_amount_transaction'=>\App\Models\Finance_transaction::select([\Illuminate\Support\Facades\DB::raw('ifnull(sum(amount),0) as total')])
//            ->where('created_at',\Carbon\Carbon::now()->format('Y-m-d'))->where('transact_type',$transaction->id)->limit(1)
//    ])->where('user_id',auth()->user()->id)->where('unit','rial')->first();
    $result = \App\Models\Finance_transaction::select([\Illuminate\Support\Facades\DB::raw('ifnull(sum(amount),0) as total')])
        ->where('created_at', \Carbon\Carbon::now()->format('Y-m-d'))->where('transact_type', $transaction->id)->first();
    return $result;
}

function transactionLimit()
{
    // unit is rls
    $array = [
        0 => 0,
        1 => 5000000000000, //5,000,000,000,000
        2 => 10000000000000, // 10,000,000,000,000
        3 => 1000000000000000, // 1,000,000,000,000,000
        4 => 5000000000000000, // 5,000,000,000,000,000
        5 => 10000000000000000, // 10,000,000,000,000,000
        6 => 100000000000000000, // 100,000,000,000,000,000
    ];
    $result = $array[auth()->user()->step_complate];
    return $result;
}

function convertStepToPersian($data)
{
    $step = [0, 1, 2, 3, 4, 5, 6];
    $persian = [trans('attributes.number.zero'),
        trans('attributes.number.one'),
        trans('attributes.number.two'),
        trans('attributes.number.three'),
        trans('attributes.number.four'),
        trans('attributes.number.five'),
        trans('attributes.number.six')];
    $result = str_replace($step, $persian, $data);
    return $result;
}

function jdate_from_gregorian($input, $format = '%A, %d %B %Y | H:i:s')
{
    return \Morilog\Jalali\Jalalian::fromDateTime($input)->format($format);
}

function jalali_to_timestamp($date, $spliter = '/', $first = true)
{
    if ($first == true) {
        $h = "00";
        $i = "00";
        $s = "00";
    } else {
        $h = "23";
        $i = "59";
        $s = "59";
    }

    list($y, $m, $d) = explode($spliter, $date);
    return (new \Morilog\Jalali\Jalalian($y, $m, $d, $h, $i, $s))->toArray()['timestamp'];
}

function faTOen($string)
{
    return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
}

/*
 * تبدیل تاریخ شمسی به میلادی
 */
function jalali_to_gregorian($date, $splitter = '/')
{
    return \Morilog\Jalali\CalendarUtils::createCarbonFromFormat('Y/m/d H:i:s', faTOen($date));
}

function showData($view, array $array = [])
{
    if (request()->ajax()) {
        $temp = ["status" => "100", "FormatHtml" => $view->renderSections()['content']];
        $temp = array_merge($temp, $array);
        return json_encode($temp);
    } else
        return $view;
}

function convertEn($string)
{
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);

    $englishNumbersOnly = str_replace($persian, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}

function convertFa($string)
{
    $num = range(0, 9);
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $convertedPersianNums = str_replace($num, $persian, $string);
    return str_replace($num, $persian, $convertedPersianNums);
}

function banks_name()
{
    return ['تجارت', 'سپه', 'ملت', 'ملی', 'مسکن', 'کشاورزی', 'پست بانک', 'سایر'];
}

// گرفتن رنگ متناسب با ارز ها
function currency_color($currency_name = null)
{
    $currency_name = strtolower($currency_name);
    $currency_color = [
        'btc' => '#f7931a',
        'eth' => '#627eea',
        'usdt' => '#26a17b',
        'doge' => '#c3a634',
        'xrp' => '#23292f',
        'ltc' => '#335d9d',
    ];
    if (!is_null($currency_name)) {
        if (key_exists($currency_name, $currency_color))
            return $currency_color[$currency_name];
        else
            return false;
    }
    return $currency_color;
}


function convertToParameter($address, $amount)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://convert-data.rootix.io/convert-data",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "address=" . $address . "&amount=" . $amount . "",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded"
        ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    curl_close($curl);
    if ($httpcode == 200) {
        return $response;
    }
    return false;
}

function contractUnit($unit)
{
    $array = currentUnits();

    return $array[$unit];
}

function currentUnits()
{
    $array = [
        'btc' => ['type_token' => 20, 'contract' => 'TN3W4H6rK2ce4vX9YnFQHwKENnHjoxb3m9', 'precision' => 8],
        'eth' => ['type_token' => 20, 'contract' => 'THb4CqiFdwNHsWsQCs4JhzwjMWys4aqCbF', 'precision' => 18],
        'btt' => ['type_token' => 20, 'contract' => 'TAFjULxiVgT4qWk6UZwjqwZXTSaGaqnVp4', 'precision' => 18],
        'wbtc' => ['type_token' => 20, 'contract' => 'TXpw8XeWYeTUd4quDskoUqeQPowRh4jY65', 'precision' => 8],
        'jst' => ['type_token' => 20, 'contract' => 'TCFLL5dx5ZJdKnWuesXxi1VPwjLVmWZZy9', 'precision' => 18],
        'win' => ['type_token' => 20, 'contract' => 'TLa2f6VPqDgRE67v1736s7bJ8Ray5wYjU7', 'precision' => 6],
        'ltc' => ['type_token' => 20, 'contract' => 'TR3DLthpnDdCGabhVDbD3VMsiJoCXY3bZd', 'precision' => 8],
        'trx' => ['type_token' => 10, 'contract' => 'trx', 'precision' => 6],
        'usdt' => ['type_token' => 20, 'contract' => 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t', 'precision' => 6],

        'bnb' => ['type_token' => 20, 'contract' => 'TDgkC3ZZBgaDqkteSgx9F14rPfqRgktyzh', 'precision' => 6], // آدرس های ترون از اینجا به بعد فیک هستند
        'xrp' => ['type_token' => 20, 'contract' => 'TGcjqn3vtazhgCtt26L1QxuHw5fiEEdcFU', 'precision' => 6],
        'sol' => ['type_token' => 20, 'contract' => null, 'precision' => 8],
        'ada' => ['type_token' => 20, 'contract' => 'TAyWDQjBC5QDPtSou7gpANPXa6Jpmiqx7Q', 'precision' => 10],
        'avax' => ['type_token' => 20, 'contract' => 'TWv9yPxrPSUawymhCTXXGbHj9ts5pNHCAj', 'precision' => 6],
        'doge' => ['type_token' => 20, 'contract' => 'THbVQp8kMjStKNnf2iCY6NEzThKMK5aBHg', 'precision' => 8],
        'dot' => ['type_token' => 20, 'contract' => 'TJxJQvAu7a8Yvi15p1c5xTpawQuhc64w1E', 'precision' => 8],
        'shib' => ['type_token' => 20, 'contract' => 'TU7FGwhbXuEitSau9BKjFx1Kwnnkd7tJPE', 'precision' => 6],
        'matic' => ['type_token' => 20, 'contract' => 'TWJWGBSdZFjFfC4ZZxzdnrrQZpmNDnmjKL', 'precision' => 6],
        'atom' => ['type_token' => 20, 'contract' => 'TDdbdkAR2DXXPYMFTpj89nXYxZ5i9mmL6G', 'precision' => 6],
        'etc' => ['type_token' => 20, 'contract' => 'TSVe4vYoHnCk7mXnR8aQuCJFzx7eBiQTBe', 'precision' => 18],
        'xlm' => ['type_token' => 20, 'contract' => 'TQM2uTGYwJCHnTW66FxqNuPGDxsxXz8emA', 'precision' => 6],
        'vet' => ['type_token' => 20, 'contract' => 'TPEZoihnwvpbsL4cWN4vnKZt6pS54JxSEb', 'precision' => 6],
        'hbar' => ['type_token' => 20, 'contract' => 'TPWRGps84eL5d4cR8ufUb7LGDfH27BVRrb', 'precision' => 8],
        'fil' => ['type_token' => 20, 'contract' => 'TAoKoBU78uQiJGw6Jr3qycZAJtFTwKzcud', 'precision' => 6],
        'icp' => ['type_token' => 20, 'contract' => 'TYabSmogufFStyCKWPNFnKS3ciGD8sad7f', 'precision' => 6],
        'egld' => ['type_token' => 20, 'contract' => 'TEZHu52qgNypUs4uceuHh4VvNhR8uTqxqv', 'precision' => 4],
        'theta' => ['type_token' => 20, 'contract' => 'TMN3QoohY16CBYnF6VjPjCkkkiTjyTyGQW', 'precision' => 6],
        'sand' => ['type_token' => 20, 'contract' => 'TKUaHQJBjydXTZVFA86SAuU5uSTDudjM58', 'precision' => 4],
        'ftm' => ['type_token' => 20, 'contract' => 'TWP3jhyJTLN4jFw5qjAVof3SxrtLVZAtgY', 'precision' => 4],
        'axs' => ['type_token' => 20, 'contract' => 'TVB66QERHrRsaDLEQeRsQkih5NG4Yi7A4J', 'precision' => 18],
        'xtz' => ['type_token' => 20, 'contract' => 'TSpGkgqt2ur5uZwigr8xBKViR2nND84up8', 'precision' => 6],
        'waves' => ['type_token' => 20, 'contract' => 'TYcJPUmup9b3f6djekCbtBtBbHwrUZfS3x', 'precision' => 18],
        'hnt' => ['type_token' => 20, 'contract' => 'TMWJbMjALhw6QnbQUNdMRYRrdgWCCeY18L', 'precision' => 4],
        'aave' => ['type_token' => 20, 'contract' => 'TVUmDWgXqN27EfkyBS8EHtcispWamMcexm', 'precision' => 8],
        'cake' => ['type_token' => 20, 'contract' => 'TPS2YfDckq9jwTw3kojvSpYXRgAiBKXiGm', 'precision' => 18],
        'zec' => ['type_token' => 20, 'contract' => 'TVfyUDz1JqEbFn1iQTnS9mpMrFfKHwe7x5', 'precision' => 6],
        'eos' => ['type_token' => 20, 'contract' => 'TEY7FVMxMpYWnLuELGXXvedHhj1diSUM1m', 'precision' => 6],
        'flow' => ['type_token' => 20, 'contract' => 'TB4GzzS8xbmgP1L8nxkc85rLESj2TU6eKD', 'precision' => 8],
        'rls' => ['type_token' => 0, 'contract' => null, 'precision' => 1],

    ];
    return $array;
}

//تبدیل نوع فایل اپلود شده به فارسی
function convert_upload_file_to_persian($type): string
{
    switch ($type) {
        case 'phone_bill':
            return 'قبض تلفن';
        case 'national':
            return 'کارت ملی';
        default:
            return '--';
    }
}

function currency_change($change)
{
    $sign = substr($change['dayChange'], 0, 1);
    $value = $change['dayChange'];
    if ($sign == '-') {
        $sign_class = 'red-currency';
        $sign = '-';
        $value = substr($value, 1);
    } else {
        $sign_class = 'green-currency';
        $sign = '+';
    }

    return [$sign_class, $sign, $value];

}

function currency_change1($key, $value)
{
//    dd($key,$value);
    $fa_name = '';
    $en_name = '';
    $day_change = $value['dayChange'];
    switch ($key) {
        case 'btc':
            $fa_name = 'بیت کوین';
            $en_name = 'BTC';
            break;
        case 'eth':
            $fa_name = 'اتریوم';
            $en_name = 'ETH';
            break;
        case 'usdt':
            $fa_name = 'تتر';
            $en_name = 'USDT';
            break;
        case 'trx':
            $fa_name = 'ترون';
            $en_name = 'TRX';
            break;
        case 'doge':
            $fa_name = 'دوج کوین';
            $en_name = 'DOGE';
            break;
    }
    $sign = substr($day_change, 0, 1);
    if ($sign == '-') {
        $sign_class = 'red-currency';
        $sign = '-';
        $day_change = substr($day_change, 1);
    } else {
        $sign_class = 'green-currency';
        $sign = '+';
    }

    return [$sign_class, $sign, $day_change, $fa_name, $en_name];

}

function rila_to_usdt($array, $currency)
{
    $rial = $array[$currency]['dayHigh'];
    $usdt = $array['usdt']['dayHigh'];

    return $rial / $usdt;
}

// چک کردن درست بودن یا درست نبودن کد گوگل اتنتیکاتور
function getCurrentOtp($code)
{
    $google2fa = new Google2FA();
    $user = auth('web')->user();
    $otp = $google2fa->getCurrentOtp($user->google2fa_secret);
    if ($code == $otp) {
        return true;
    }
    return false;
}

function index($data, $loop)
{
    return ($data->currentPage() - 1) * $data->perPage() + 1 + $loop->index;
}

function getPrefix()
{
    $temp = ltrim(request()->route()->getPrefix(), "/");
    return str_replace('/', '.', $temp);
}

function getCurrentGuard()
{
    $prefix = getPrefix();
    if ($prefix == "admin") {
        return 'admin';
    } else {
        return 'web';
    }
}

function getCurrentUser()
{
    $guard = getCurrentGuard();
    if ($guard == "admin") {
        return auth('admin')->user();
    } elseif ($guard == "web") {
        return auth('web')->user();
    }
}

function getCategory($id)
{
    $category = \App\Models\PostCategory::findOrFail($id);
    return $category->name;
}

function countNotificationsNotView()
{
    $user = getCurrentUser();
    $result = \App\Models\Notification::where('user_id', $user->user_guard_id)->whereDoesntHave('notification_view')
        ->orWhere('user_id', 0)->whereDoesntHave('notification_view')
        ->count();
    return $result;
}

function getNotificationsNotView()
{
    $user = getCurrentUser();
    $result = \App\Models\Notification::where('user_id', $user->user_guard_id)->whereDoesntHave('notification_view')
        ->orWhere('user_id', 0)->whereDoesntHave('notification_view')
        ->get();
    return $result;
}

// اضافه کردن نوتیفیکیشن
function insert_notif($request): bool
{
    $base_data = BaseData::query()->where('type', $request['type'] . "_notif")->first();
    $base_data = $base_data ? $base_data->id : null;
    if (is_null($base_data)) return false;
    Notification::query()->create([
        'refer_id' => $request['refer_id'] ?? null,
        'user_id' => $request['user_id'] ?? auth()->user()->id,
        'type' => $base_data,
        'details' => $request['details'] ?? []
    ]);
    return true;
}

// گرفتن لیست نوتیفیکشن ها
function get_notification(): array
{
    $all_notif = auth()->user()->notifications()->join('base_data', 'base_data.id', '=', 'notifications.type')->get(['notifications.*', 'base_data.extra_field1', 'base_data.extra_field2', 'base_data.extra_field3', 'base_data.type']);
    $notifications = [];
    foreach ($all_notif as $notif) {
        $ext1 = json_decode($notif->extra_field1, true);
        $link = get_notif_link($notif);
        $title = get_notif_title($notif);
        $notifications[] = [
            'icon' => $ext1['icon'] ?? null,
            'color' => $ext1['color'] ?? null,
            'link' => $link,
            'title' => $title,
            'created_at' => $notif->created_at
        ];
    }
    return $notifications;
}

// گرفتن لیست نوتیفیکشن های ادمین
function get_admin_notification(): array
{
    if (auth()->user()->hasRole('admin')) {
        $base_data = BaseData::query()->where('type', 'LIKE', '%_notif%')->get();
        $all_notif['ticket'] = \App\Models\TicketMaster::query()
            ->where('status', 1)
            ->where(function ($q) {
                $q->whereNull('accept_id')
                    ->orWhere('accept_id', auth()->user()->id);
            })
            ->count();

        $all_notif['authenticate'] = \App\Models\UserAuthForm::query()->groupBy('user_id')->where('status', 1)->get('user_id')->count();
        $all_notif['bank'] = \App\Models\Bank::query()->where('status', 0)->count();
        $all_notif['withdraw_rial'] = \App\Models\WithdrawRial::query()->where('status', 0)->count();
        $all_notif['withdraw_crypto'] = \App\Models\Withdraw::query()->where('status', 0)->count();
        $notifications = [];
        foreach ($all_notif as $key => $count) {
            if ($count == 0)
                continue;
            $key2 = $key == 'withdraw_rial' || $key == 'withdraw_crypto' ? 'withdraw' : $key;
            $item_base = $base_data->where('type', "{$key2}_notif")->first();
            $ext1 = $item_base ? json_decode($item_base->extra_field1, true) : null;
            $link = get_notif_link(['type' => $key], true);
            $title = get_notif_title(['type' => $key, 'count' => $count], true);
            $notifications[] = [
                'icon' => $ext1 ? $ext1['icon'] : null,
                'color' => $ext1 ? $ext1['color'] : null,
                'link' => $link,
                'title' => $title,
                'created_at' => ''
            ];
        }
        return $notifications;
    }
    return [];
}

// گرفتن لیست اطلاعیه ها
function get_announcements($type = '')
{
    $user_id = auth()->user()->id;
    $roles_id = auth()->user()->roles->pluck('id')->toArray();

    $announcement = \App\Models\Announcement::query()
        ->where(function ($q) {
            return $q->where('end_at', '>=', date('Y-m-d H:i'))
                ->orWhereNull('end_at');
        })
        ->where('started_at', '<=', date('Y-m-d H:i'));

    if ($type == 'not-visited') {
        $announcement = $announcement->whereJsonDoesntContain('visited', $user_id);
    }
    return $announcement->where(function ($query) use ($user_id, $roles_id) {
        $query
            ->WhereHas('users', function ($q) use ($user_id) {
                $q->where('id', $user_id);
            })
            ->orWhereHas('roles', function ($q) use ($roles_id) {
                $q->whereIn('id', $roles_id);
            });
    })->with(['files' => function ($q) {
        $q->select(['id', 'uploadable_type', 'uploadable_id', 'path']);
    }])->get();
}

// گرفتن لینک هر ایتم نوتیفیکیشن
function get_notif_link($notif, $is_admin = false): string
{
    $link = '';
    if ($is_admin == true) { // اگه برای مدیر بود
        switch ($notif['type']) {
            case 'ticket':
                $link = route('ticket.admin.index');
                break;
            case 'authenticate':
                $link = route('user.authenticate.index');
                break;
            case 'bank':
                $link = route('admin.bank.index');
                break;
            case 'withdraw_rial':
                $link = route('admin.withdraw-rial.index');
                break;
            case 'withdraw_crypto':
                $link = route('admin.request.withdraw');
                break;
        }
    } else { // اگه برای کاربر بود
        switch ($notif->type) {
            case 'withdraw_notif':
                if ($notif->details['type'] == 'rial') {
                    $link = route('wallet.withdraw.rial.index');
                } else {
                    $link = '#';
                }
                break;
            case 'bank_notif':
                $link = route('banks.index');
                break;
            case 'ticket_notif':
                break;
            case 'admin_user_notif':
                break;
            case 'authenticate_notif':
                break;
        }
    }
    return $link;
}

// گرفتن عنوان هر ایتم نوتیفیکیشن
function get_notif_title($notif, $is_admin = false)
{
    $title = '';
    if ($is_admin == true) { // اگه برای مدیر بود
        switch ($notif['type']) {
            case 'ticket':
                $title = '<h6 class="title" style="padding-top: 4px;" dir="rtl">' . "<b>{$notif['count']}</b>";
                $title .= " تیکت بررسی نشده";
                $title .= '</h6>';
                break;
            case 'authenticate':
                $title = '<h6 class="title" style="padding-top: 4px;" dir="rtl">' . "<b>{$notif['count']}</b>";
                $title .= " کاربر در انتظار احراز هویت";
                $title .= '</h6>';
                break;
            case 'bank':
                $title = '<h6 class="title" style="padding-top: 4px;" dir="rtl">' . "<b>{$notif['count']}</b>";
                $title .= " حساب بانکی بررسی نشده";
                $title .= '</h6>';
                break;
            case 'withdraw_rial':
                $title = '<h6 class="title" style="padding-top: 4px;" dir="rtl">' . "<b>{$notif['count']}</b>";
                $title .= " درخواست برداشت ریالی";
                $title .= '</h6>';
                break;
            case 'withdraw_crypto':
                $title = '<h6 class="title" style="padding-top: 4px;" dir="rtl">' . "<b>{$notif['count']}</b>";
                $title .= " درخواست برداشت ارز";
                $title .= '</h6>';
                break;
        }
    } else { // اگه برای کاربر بود
        switch ($notif->type) {
            case 'withdraw_notif':
                $title = '<h6 class="title" style="padding-top: 4px;">درخواست برداشت شما ';
                $title .= $notif->details['status'] == 1 ? '<b class="text-success">تایید شد</b>' : '<b class="text-danger">رد شد</b>';
                $title .= '</h6>';
                break;
            case 'bank_notif':
                $card_number = $notif->details['card_number'];
                $title = '<h6 class="title" style="padding-top: 4px;">حساب بانکی شما ';
                $title .= $notif->details['status'] == 1 ? '<b class="text-success">تایید شد</b>' : '<b class="text-danger">رد شد</b>';
                $title .= '</h6>';
                break;
            case 'ticket_notif':
                $subject = $notif->details['subject'];
                $title = '<h6 class="title" style="padding-top: 4px;">تیکت شما با موضوع ';
                $title .= "<b> $subject </b> ";
                $title .= 'پاسخ داده شد</h6>';
                break;
            case 'admin_user_notif':
                $message = $notif->details['message'];
                $title = '<h6 class="title" style="padding-top: 4px;">پیام ادمین:  ';
                $title .= "<b> $message </b>";
                break;
            case 'authenticate_notif':
                $step = convertStepToPersian($notif->details['step']);
                $status = $notif->details['status'];
                $title = '<h6 class="title" style="padding-top: 4px;">مرحله ی ';
                $title .= "<b> $step </b> احراز هویت شما ";
                $title .= $status == 1 ? '<b class="text-success">تایید شد</b>' : '<b class="text-danger">رد شد</b>';
                break;
        }
    }
    return $title;
}

// حداکثر سقف برداشت روزانه به ریال بر اساس سطح
function max_withdraw_daily()
{
    switch (auth()->user()->step_complate) {
        case 2:
        case 3:
            return 5000000000;
        case 4:
            return -1; // یعنی نامحدود
        default:
            return 0;
    }
}

function usdt_to_rls($price, $usdt_rls)
{
    return $price * $usdt_rls / 10;
}

// حذف صفر ها بعد از ممیز
function TrimTrailingZeroes($nbr)
{
    if (strpos($nbr, '.') !== false) $nbr = rtrim($nbr, '0');
    return rtrim($nbr, '.') ?: '0';
}

// تابع نامبر فرمت اختصاصی
function num_format($number, $decimal = 8)
{
    $floor_number = pow(10, $decimal);
    $number = floor($number * $floor_number) / $floor_number;
    $number = number_format($number, $decimal);
    return floatval($number) == 0 ? 0 : TrimTrailingZeroes($number);
}

function rial_to_unit($amount, $unit, $mod = false)
{
    $unit = $unit == 'rial' ? 'rls' : strtolower($unit);
    $amount = is_string($amount) ? str_replace(',', '', $amount) : $amount;
    $post_fix = get_unit($unit);
    if ($unit == 'rls') {
        $amount /= 10;
    }
    return $mod == true ? num_format($amount, contractUnit($unit)['precision']) . ' ' . $post_fix : $amount;
}

function unit_to_rial($amount, $mod = false, $unit = 'rls') // $unit == Null بود و برای مشکل سواپ اومدم به جای null , rls پاس دادم
{
    $unit = strtolower($unit);
    $amount = is_string($amount) ? str_replace(',', '', $amount) : $amount;
    if ($unit == 'rls')
        $amount *= 10;

    return $mod == true ? num_format($amount, contractUnit($unit)['precision']) . ' ریال' : $amount;
}

function get_unit($unit = 'rial', $lang = 'fa'): string
{
    if ($unit == 'rial' || $unit == 'rls')
        return ($lang == 'fa') ? 'تومان' : 'Toman';
    return $unit;
}

function get_all_currencies()
{
    try {
        $all_currencies = Http::withoutVerifying()->post('http://api.rootix.io/currencies/market/stats/all-currencies', ['api_token' => 'isZXuDxtOOnMwEl2YwORhHeBo2woMaf3U8JwwV47tlEnPvsbN9'])->body();
        return json_decode($all_currencies, true);
    } catch (\Exception $exception) {
        response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. لطفا بعدا امتحان کنید.']);
//        Log::useDailyFiles(storage_path() . '/logs/api.log');
//        Log::info($exception);
    }

}

function get_specific_currencies($currency)
{
    try {
        $currency = Http::withoutVerifying()->post('http://api.rootix.io/currencies/market/stats', ['api_token' => 'isZXuDxtOOnMwEl2YwORhHeBo2woMaf3U8JwwV47tlEnPvsbN9', 'currencies' => $currency])->body();
        return $currency ? json_decode($currency, true) : [];
    } catch (\Exception $exception) {
        return [];
    }

}

function coinex()
{
    $model = new \App\Library\Coinex(env('COINEX_ACCESS_ID'), env('COINEX_SECRET_KEY'));
    return $model;
}

// تبدیل ارزها بهمدیگر
/**
 * @param $source_unit
 * @param $des_unit
 * @param $source_amount
 * @param false $mode
 * @param false $list_currencies
 * @return false|float|int|mixed|string|string[]
 * بر این usdt_amount رو پاس دادیم که تو تابع خواستیم فراخوانی کنیم هی نیاد درخواست به سرور بزنه مقدار ارزهارو بگیره و قبل حلقه خودمون بهش پاس بدیم اونو
 */
function convert_currency($source_unit, $des_unit, $source_amount, $mode = false, $list_currencies = false)
{
    if (isset($list_currencies['RIAL'])) {
        $list_currencies['RLS'] = $list_currencies['RIAL'];
        unset($list_currencies['RIAL']);
    }

    $source_unit = strtolower($source_unit) == 'rial' ? 'rls' : $source_unit;
    $des_unit = strtolower($des_unit) == 'rial' ? 'rls' : $des_unit;

    $source_unit = strtoupper($source_unit);
    $des_unit = strtoupper($des_unit);

    if ($list_currencies == false)
        $list_currencies = get_all_currencies();

    if ($source_unit === $des_unit)
        return rial_to_unit($source_amount, $des_unit, $mode);

    if (!isset($list_currencies[$source_unit]))
        return false;

    if ($des_unit === 'USDT') {
        return rial_to_unit($list_currencies[$source_unit]['price'] * $source_amount, $des_unit, $mode);
    }

    if ($des_unit === 'RLS') {
        return rial_to_unit($list_currencies[$source_unit]['rls'] * $source_amount, $des_unit, $mode);
    }

    if ($des_unit === 'BTC')
        return rial_to_unit($list_currencies[$source_unit]['price_btc'] * $source_amount, $des_unit, $mode);

    if (!isset($list_currencies[$des_unit]))
        return false;

    return rial_to_unit($list_currencies[$source_unit]['price'] * $source_amount / $list_currencies[$des_unit]['price'], $des_unit, $mode);
//    $des_amount = rial_to_unit($list_currencies[$source_unit]['price'] * $source_amount / $list_currencies[$des_unit]['price'], $des_unit);
//    $des_unit = get_unit($des_unit);
//    return $mode === true ? num_format($des_amount) . " $des_unit" : $des_amount;
}

function market_cap_volume($volume)
{
    if (str_contains($volume, '.'))
        $volume = substr($volume, 0, strpos($volume, "."));
    $length = strlen($volume);
    if ($length >= 13)
        $volume_info = [number_format($volume / 1000000000000, 2), 'تریلیون دلار'];
    elseif ($length >= 10)
        $volume_info = [number_format($volume / 1000000000, 2), ' میلیارد دلار'];
    elseif ($length >= 7)
        $volume_info = [number_format($volume / 1000000, 2), ' میلیون دلار'];
    else
        $volume_info = [number_format($volume / 1000, 2), ' هزار دلار'];
    return $volume_info;

}


function banks()
{
    return ['تجارت', 'سپه', 'ملت', 'ملی', 'مسکن', 'کشاورزی', 'پست بانک', 'پاسارگاد'];
}

function get_bank_logo($name)
{
    switch ($name) {
        case 'تجارت':
            return 'tejarat';
        case 'سپه':
            return 'sepah';
        case 'ملت':
            return 'mellat';
        case 'ملی':
            return 'melli';
        case 'مسکن':
            return 'maskan';
        case 'کشاورزی':
            return 'keshavarzi';
        case 'پست بانک':
            return 'post_bank';
        default:
            return 'other';
    }
}

function negasht($path)
{
    // index: ادرس قبلی
    //value: ادرس جدید
    $negasht = [
        'about' => 'about-rootix',
        'contact-us' => 'contact-rootix',
    ];
    return $negasht[$path] ?? false;
}

function trade_logs($trade)
{
    if ($trade->status == 1) {
        $usdt_price = get_specific_currencies('USDT');
        $usdt_price = $usdt_price['USDT']['rls'];
        $currency = str_replace('USDT', '', $trade->market);
        $currency = strtolower($currency);
        $unit = ($trade->unit == 'rial' || $trade->unit == 'rls') ? 'rls' : 'usdt';
        $unit_asset = $trade->user->assets->where('unit', $unit)->first();
        $currency_asset = $trade->user->assets->where('unit', $currency)->first();
        $trade_fee = BaseData::query()->where('type', 'trad_fee')->first();
        $transact_type = BaseData::query()->where('type', 'trad')->first();
        $type = $trade->type == 0 ? 'sell' : 'buy';
        if ($type == 'sell') {
            $trade_amount = ($trade->amount * $trade->price) - ($trade_fee->extra_field1 * $trade->amount * $trade->price / 100);
            if ($trade->unit == 'rls' || $trade->unit == 'rial')
                $trade_amount = $trade_amount * $usdt_price;
            $total_amount = $unit_asset->amount + $trade_amount;
            $unit_asset->update([
                'amount' => $total_amount
            ]);
            $unit_asset->save();
            $unit_asset->transaction()->create([
                'tracking_code' => '',
                'refer_id' => $trade->id,
                'user_id' => $trade->user_id,
                'transact_type' => $transact_type->id,
                'amount' => $trade_amount,
                'type' => 2,
                'unit' => $trade->unit,
                'theter_rls ' => 'USDT',
                'extra_field1' => '',
                'description' => rial_to_unit($trade_amount, $trade->unit, true) . ' بابت ترید به کیف پول شما افزوده شد.',
            ]);
            $currency_asset->transaction()->create([
                'tracking_code' => '',
                'refer_id' => $trade->id,
                'user_id' => $trade->user_id,
                'transact_type' => $transact_type->id,
                'amount' => $trade->amount,
                'type' => 1,
                'unit' => $currency_asset->unit,
                'theter_rls' => 'USDT',
                'extra_field1' => '',
                'description' => rial_to_unit($trade->amount, $currency, true) . ' بابت ترید از کیف پول شما کسر شد.',
            ]);
        } else {
//            $trade_amount = ($trade->amount * $trade->price) + ($trade_fee->extra_field1 * $trade->amount * $trade->price / 100);
//            $total_amount=$usdt_asset->amount-$trade_amount;
//            $usdt_asset->update([
//                'amount' => $trade->total_amount
//            ]);
//            $usdt_asset->save();
            $currency_amount = $currency_asset->amount + $trade->amount;
            $currency_asset->update([
                'amount' => $currency_amount
            ]);
            $currency_asset->save();
            $unit_asset->transaction()->create([
                'tracking_code' => '',
                'refer_id' => $trade->id,
                'user_id' => $trade->user_id,
                'transact_type' => $transact_type->id,
                'amount' => $trade->total_amount,
                'type' => 1,
                'unit' => $unit_asset->unit,
                'theter_rls' => 'USDT',
                'extra_field1' => '',
                'description' => rial_to_unit($trade->total_amount, $trade->unit, true) . ' بابت ترید از کیف پول شما کسر شد.',
            ]);
            $currency_asset->transaction()->create([
                'tracking_code' => '',
                'refer_id' => $trade->id,
                'user_id' => $trade->user_id,
                'transact_type' => $transact_type->id,
                'amount' => $trade->amount,
                'type' => 2,
                'unit' => $currency_asset->unit,
                'theter_rls' => 'USDT',
                'extra_field1' => '',
                'description' => rial_to_unit($trade->amount, $currency, true) . ' بابت ترید به کیف پول شما افزوده شد.',
            ]);
        }
    }

}

function calculate_time($time)
{
    $time = strtotime($time);
    $time = time() - $time; // to get the time since that moment
    $time = ($time < 1) ? 1 : $time;
    $tokens = array(
        31536000 => 'سال',
        2592000 => 'ماه',
        604800 => 'هفته',
        86400 => 'روز',
        3600 => 'ساعت',
        60 => 'دقیقه',
        1 => 'ثانیه'
    );
    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits . ' ' . $text;
    }

}

/// وضعیت احراز هویت کاربر ونمایش مراحل انجلم شده به صورت آرایه یا html
function status_steps_auth_user($auth_forms, $user_auth_forms, $mode = 'all', $type = 'array', $single_item = null)
{
    $array = [];
    $titles = [0 => 'بررسی نشده', 1 => 'تکمیل شده', 2 => 'تایید شده', 3 => 'رد شده'];
    $colors = [0 => 'info', 1 => 'warning', 2 => 'success', 3 => 'danger'];
    $icons = [0 => 'fa-info', 1 => 'fa-question', 2 => 'fa-check', 3 => 'fa-times'];
    foreach ($auth_forms as $key => $forms) {
        foreach ($user_auth_forms as $user_form) {
            if ($forms['type'] == $user_form['type']) {
                $array[$forms['type']] = [
                    'id' => $user_form['id'],
                    'status' => $user_form['status'],
                    'title_status' => $titles[$user_form['status']],
                    'status_color' => $colors[$user_form['status']],
                    'status_icon' => $icons[$user_form['status']],
                    'msg_reject' => $user_form['reject_reason'],
                ];
                break;
            } else {
                /// اگر مرحله خواسته شده توسط کاربر تکمیل نشده بود
                $array[$forms['type']] = [
                    'id' => 0,
                    'status' => -1,
                    'title_status' => 'تکمیل نشده',
                    'status_color' => $colors[3],
                    'status_icon' => $icons[3],
                    'msg_reject' => null,
                ];
            }
        }
    }
    if ($mode == 'all')
        return all_show_steps_completed($array, $type);
    return single_show_steps_completed($array, $type, $single_item);
}

function all_show_steps_completed($data, $type)
{
    $result = null;
    switch ($type) {
        case 'array':
            $result = $data;
            break;
        case 'notif':
            foreach ($data as $key => $item) {
                $result[$key] = '<span class="text-' . $item['status_color'] . ' pointer"
                              data-toggle="tooltip" data-placement="top"
                              title="' . $item['title_status'] . '">
                              <i class="fa ' . $item['status_icon'] . '"></i>
                              </span>';
            }
    }
    return $result;
}

function single_show_steps_completed($data, $type, $item)
{
    $result = null;

    switch ($type) {
        case 'array':
            $result = $data[$item] ?? [];
            break;
        case 'notif':

            $result = '<span class="text-' . $data[$item]['status_color'] . ' pointer"
                          data-toggle="tooltip" data-placement="top"
                           title="' . $data[$item]['title_status'] . '">
                            <i class="fa ' . $data[$item]['status_icon'] . '"></i>
                             </span>';
    }
    return $result;
}

function name_roles_string_middleware()
{
    $roles = \App\Models\Role::pluck('name')->toArray();

    if (count($roles) > 0) {
        return implode('|', $roles);
    }

}

// گرفتن پست های بلاگ
function get_blog_posts()
{

    //        $new_post =  file_get_contents("https://blog.rootix.io/wp-json/wp/v2/posts?_fields=thumbnail_url,title,id,link,date,modified");
//        $new_post =  $new_post ? json_decode($new_post, true) : [];

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://blog.rootix.io/wp-json/wp/v2/posts?_fields=thumbnail_url,title,id,link,date,modified',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));
    $new_post = curl_exec($curl);
    curl_close($curl);
    return $new_post ? json_decode($new_post, true) : [];
}

function blocked_asset($asset)
{
    $unit = strtoupper($asset->unit) . 'USDT';
    $trades = auth()->user()->trades->where('market', $unit)->where('status', 0);
    $amount = $asset->amount;
    $description = '';
    $blocked_amount = 0;
    if (count($trades) > 0) {
        foreach ($trades as $trade) {
            $blocked_amount += $trade->amount;
            $amount += $trade->amount;
        }
        $description = $blocked_amount . ' از موجودی شما به دلیل سفارش باز بلاک شده است';
    }

    return ['amount' => $amount, 'des' => $description];
}

//ارسال ایمیل و پیامک به پشتیبان ها برای تیکت جدید یا درخواست برداشت جدید
function support_notif($type, $mobile = true, $email = true)
{
    $base_data = BaseData::query()->where('type', 'support_setting')->first();
    $base_data = unserialize($base_data->extra_field3);
    $today = \Carbon\Carbon::now()->format('l');
    $support = \App\Models\User::query()->where('email', $base_data[$today])->first();
    if ($mobile)
        SendSms::dispatch($support->mobile, $type, 'support-check');

    if ($email) {
        $msg = $type . ' جدیدی ثبت شده است. لطفا پنل مدیریت را بررسی کنید.';
        Mail::to($support->email)->send(new \App\Mail\SupportMail($type, $msg));
    }
}

function user_ip()
{
    return $_SERVER['HTTP_CF_CONNECTING_IP'];
}
