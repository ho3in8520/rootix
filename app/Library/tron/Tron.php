<?php

namespace App\Library\tron;

use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Currency;
use App\Models\TransactionTron;
use App\Models\User;
use Illuminate\Support\Facades\DB;

trait Tron
{
    private $unitsTrc10;
    private $unitsTrc20;
    private $commission = ['trx'];
    private $address;
    private $private_key;
    private $fee;
    private $user;
    private $fee_withdraw_trc10;
    private $fee_withdraw_trc20;

    private function construct()
    {
        ///// ساخت آرایه ارزهای trc10
        $this->unitsTrc10 = $this->createArrayUnits(currentUnits(), 10);
        ///// ساخت آرایه ارزهای trc20
        $this->unitsTrc20 = $this->createArrayUnits(currentUnits(), 20);
        // کارمزد های تراکنش
        $fee = BaseData::select([
            'trc10' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc10')->limit(1),
            'trc20' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc20')->limit(1),
        ])->first();
        $this->fee_withdraw_trc10 = $fee->trc10;
        $this->fee_withdraw_trc20 = $fee->trc20;
    }

    private function createArrayUnits($units, $trc = 10)
    {
        $array = [];
        foreach ($units as $key => $unit) {
            $array[$unit['contract']] = [
                'unit' => $key,
                'type_token' => $unit['type_token'],
                'precision' => $unit['precision'],
            ];
        }
        return $trc == 10 ? $array : $units;
    }

    public function getInformationAccount($addressBase58)
    {
        try {
            return Tron()->getAccountInfo($addressBase58);
        } catch (\Exception $e) {
            return 0;
        }
    }

    public function getAmounts($address, $trc = 'all')
    {
        $amount = [];
        if ($trc == 'all') {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unitsTrc10[$item['key']])) {
                    $amount[$this->unitsTrc10[$item['key']]] = $item['value'];
                }
            }
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unitsTrc20[array_keys($item)[0]])) {
                    $amount[$this->unitsTrc20[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        } elseif ($trc == 10) {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unitsTrc10[$item['key']])) {
                    $amount[$this->unitsTrc10[$item['key']]] = $item['value'];
                }
            }
        } elseif ($trc == 20) {
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unitsTrc20[array_keys($item)[0]])) {
                    $amount[$this->unitsTrc20[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        }
        return $amount;
    }

    public function checkAmountUnit(string $address, string $private_key, User $user)
    {
        $this->address = $address;
        $this->private_key = $private_key;
        $this->user = $user;
        $this->construct();
        $hash = $this->removeCommissionArray();
        if (count($hash) > 0):
            $this->updateAssets($hash);
        endif;
    }

    private function transactionHashTrc20()
    {
        try {
            $transactions = Tron($this->private_key)->getTransactionByAccountTrc20($this->address);
        } catch (\Exception $e) {
            return false;
        }
        $array = [];
        foreach ($transactions['data'] as $row) {
            $unit = strtolower($row['token_info']['symbol']);
            // بررسی موجود بودن unit در لیت unit های سامانه و بررسی ثبت نشدن تراکنش ادمین
            if (in_array($unit, array_keys($this->unitsTrc20)) && $row['to'] != env('ADDRESS_TRON_BASE')) {
                $array[] = [
                    'unit' => strtolower($row['token_info']['symbol']),
                    'amount' => $this->amount($row['value'], $this->unitsTrc20[$unit]['precision'], 'division'),
                    'txID' => $row['transaction_id'],
                    'owner_address' => $row['from'],
                    'to_address' => $row['to'],
                    'trc' => 20,
                    'commission' => $this->commission(20),
                ];
            }
        }
        return $array;
    }

    private function transactionHashTrc10()
    {
        try {
            $transactions = Tron($this->private_key)->getTransactionByAccountTrc10($this->address);
        } catch (\Exception $e) {
            return false;
        }
        $array = [];
        foreach ($transactions['data'] as $row) {
            // بررسی trc10 بودن تراکنش
            if (isset($row['raw_data']['contract'][0]['parameter']['value']['asset_name'])) {
                $asset_name = $row['raw_data']['contract'][0]['parameter']['value']['asset_name'];
                $amount = $row['raw_data']['contract'][0]['parameter']['value']['amount'];
                $to_address = Tron()->hexString2Address($row['raw_data']['contract'][0]['parameter']['value']['to_address']);
                $owner_address = Tron()->hexString2Address($row['raw_data']['contract'][0]['parameter']['value']['owner_address']);
                $tx_id = $row['txID'];
                // بررسی موجود بودن unit در لیت unit های سامانه
                if (in_array($asset_name, array_keys($this->unitsTrc10)) && $to_address != env('ADDRESS_TRON_BASE')) {
                    $array[] = [
                        'unit' => $this->unitsTrc10[$asset_name]['unit'],
                        'amount' => $this->amount($amount, $this->unitsTrc10[$asset_name]['precision'], 'division'),
                        'txID' => $tx_id,
                        'owner_address' => $owner_address,
                        'to_address' => $to_address,
                        'trc' => 10,
                        'commission' => $this->commission(10),
                    ];
                }
            } elseif (!isset($row['raw_data']['contract'][0]['parameter']['value']['asset_name']) && !isset($row['raw_data']['contract'][0]['parameter']['value']['contract_address'])) {
                $amount = $row['raw_data']['contract'][0]['parameter']['value']['amount'];
                $tx_id = $row['txID'];
                $to_address = Tron()->hexString2Address($row['raw_data']['contract'][0]['parameter']['value']['to_address']);
                $owner_address = Tron()->hexString2Address($row['raw_data']['contract'][0]['parameter']['value']['owner_address']);
                $array[] = [
                    'unit' => $this->unitsTrc10['trx']['unit'],
                    'amount' => $this->amount($amount, $this->unitsTrc10['trx']['precision'], 'division'),
                    'txID' => $tx_id,
                    'owner_address' => $owner_address,
                    'to_address' => $to_address,
                    'trc' => 10,
                    'commission' => $this->commission(10),
                ];
            }
        }
        return $array;
    }

    private function arrayMergeCurrencies()
    {
        $result = array_merge($this->transactionHashTrc10(), $this->transactionHashTrc20());
        return $result;
    }

    private function removeReviewsFromTheArray()
    {
        $currencies = $this->arrayMergeCurrencies();
        $txID = array_column($currencies, 'txID');
        $model = TransactionTron::select('first_hash')->whereIn('first_hash', $txID)->get()->toArray();
        $txID_available = array_column($model, 'first_hash');

        foreach ($currencies as $key => $currency) {
            if (in_array($currency['txID'], $txID_available)) {
                unset($currencies[$key]);
            }
        }
        return $currencies;
    }

    private function removeCommissionArray()
    {
        $currencies = $this->removeReviewsFromTheArray();
        foreach ($currencies as $key => $currency) {
            if (in_array($currency['unit'], $this->commission) && $currency['owner_address'] == env('ADDRESS_TRON_BASE')) {
                unset($currencies[$key]);
            }
            $address = coinex()->get_deposit_address($currency['unit'], 'TRC' . $currency['trc']);
            $address = json_decode($address)->data->coin_address;
            if ($currency['to_address'] == $address) {
                unset($currencies[$key]);
            }
            //// حذف تراکنش ایجاد شده خود شبکه ترون (جایزه ترون ک خودش یه تراکنش ثبت میکند)
            if ($currency['unit'] == 'trx' && $currency['amount'] < 1) {
                unset($currencies[$key]);
            }
        }
        return $currencies;
    }

    private function updateAssets($data)
    {
        $this->addTransactionTron($data);
        /*   $array = [];
           $transaction = BaseData::where('type', 'transactions')->where('extra_field1', 4)->first();
           $user = $this->user;
           DB::beginTransaction();
           try {
               foreach ($data as $k => $unit) {
                   if ($unit['amount'] > 0) {
                       $model = Asset::query()->where('unit', $unit['unit'])->where('user_id', $user->id)->first();
                       $model->amount += $unit['amount'];
                       $model->save();
                       $model->transaction()->create([
                           'tracking_code' => $unit['txID'],
                           'user_id' => $user->id,
                           'transact_type' => $transaction->id,
                           'amount' => $unit['amount'],
                           'type' => 2,
                           'description' => "واریز {$unit['amount']} {$unit['unit']} به کیف پول",
                       ]);
                   }
                   DB::commit();
               }
               $this->addTransactionTron($data);
           } catch (\Exception $e) {
               DB::rollBack();
           }*/
    }

    private function addTransactionTron($data)
    {
        $array = [];
        $user = $this->user;
        DB::beginTransaction();
        try {
            foreach ($data as $unit) {
                array_push($array, ['first_hash' => $unit['txID'], 'user_id' => $user->id, 'amount' => $unit['amount'], 'unit' => $unit['unit'], 'amount_commission' => $unit['commission'], 'status' => 1]);
            }
            TransactionTron::insert($array);
            DB::commit();
            return $this->createParameterJs();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    private function createParameterJs()
    {
        $result = TransactionTron::select([
            '*',
            'token' => Currency::query()->select('type_token')->whereColumn('transaction_trons.unit', 'currencies.unit')->limit(1),
            'network' => Currency::query()->select('network')->whereColumn('transaction_trons.unit', 'currencies.unit')->limit(1)
        ])->where('status', 1)->get();
        foreach ($result as $row) {
            $token = $row->network . $row->token;
            $address = coinex()->get_deposit_address($row->unit, $token);
            $address = json_decode($address)->data->coin_address;
            $address_to_hex = Tron()->toHex($address);
//api.ctrproject.com/convert-data
            $amount = $row->amount * 1000000;
            $id_transact = $row->id;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://convert-data.rootix.io/convert-data",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "address=" . $address_to_hex . "&amount=" . $amount . "",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            curl_close($curl);
            if ($httpcode == 200) {
                $row->ethers_data = $response;
                $row->save();
            }
        }
    }

    private function commission($trc = 10)
    {
        $fee = 0;
        switch ($trc) {
            case 10;
                $fee = (!empty($this->fee_withdraw_trc10)) ? $this->fee_withdraw_trc10 : 3;
                break;
            case 20;
                $fee = (!empty($this->fee_withdraw_trc20)) ? $this->fee_withdraw_trc20 : 12;
                break;
        }
        return $fee;
    }

    public function getAmountsAdmin($address, $trc = 'all', $unit = null)
    {
        $amount = [];
        if ($trc == 'all') {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {
                if (isset($this->unitsTrc10[$item['key']])) {
                    $amount[$this->unitsTrc10[$item['key']]] = $item['value'];
                }
            }
            //// create array trc20
            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                if (isset($this->unitsTrc20[array_keys($item)[0]])) {
                    $amount[$this->unitsTrc20[array_keys($item)[0]]] = (int)$item[array_keys($item)[0]];
                }
            }
        } elseif ($trc == 10) {
            //// create array trc10
            foreach ($this->getInformationAccount($address)['data'][0]['assetV2'] as $item) {

                if (isset($this->unitsTrc10[$item['key']])) {
                    $amount[$unit] = $item['value'];
                }
            }
        } elseif ($trc == 20) {

            foreach ($this->getInformationAccount($address)['data'][0]['trc20'] as $item) {
                $contract = array_keys($item)[0];

                if ($this->unitsTrc20[$unit]['contract'] == $contract) {
                    $amount[$unit] = (int)$item[$contract];
                }
            }
        }
        return $amount;
    }

    public function amount($amount, int $precision, $mode = 'division')
    {
        $decimal = 1;
        for ($i = 0; $i < $precision; $i++) {
            $decimal .= 0;
        }
        if ($mode == 'division')
            return $amount / $decimal;
        elseif ($mode == 'multiplication')
            return $amount * $decimal;
        return 0;
    }
}
