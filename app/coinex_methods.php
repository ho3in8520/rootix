<?php
//    public function new_trade(Request $request)
//    {
//        $usdt_price=get_specific_currencies('usdt');
//        $usdt_price=$usdt_price['USDT']['rls'];
//        $unit=$request['unit'];
//
//        $type = $request['type'] == 'buy' ? 'buy' : 'sell';
//        $market = strtoupper($request['market']);
//        if ($type == 'buy') {
//            $this->validate($request, [
//                'price' => 'required|numeric',
//                'amount' => 'required|numeric',
//            ]);
//            $amount = $request['amount'];
//            $price = $request['price'];
//            $usdt_asset = auth()->user()->assets->where('unit', 'usdt')->first();
//            if ($amount * $price > $usdt_asset->amount)
//                return response()->json(['status' => 500, 'msg' => 'موجودی تتر شما کافی نمی باشد.']);
//            $trad_fee = BaseData::query()->where('type', 'trad_fee')->first();
//            $total_amount = ($amount * $price);
//            $coinex_amount = ($total_amount) - ($total_amount * $trad_fee->extra_field1 / 100);
//            $new_amount = $coinex_amount / $price;
//        } else {
//            $this->validate($request, [
//                'sell_price' => 'required|numeric',
//                'sell_amount' => 'required|numeric',
//            ]);
//            $amount = $new_amount = $request['sell_amount'];
//            $price = $request['sell_price'];
//            $total_amount = $amount * $price;
//            $des_asset = auth()->user()->assets->where('unit', $request['des_asset'])->first();
//            if ($amount > $des_asset->amount)
//                return response()->json(['status' => 500, 'msg' => 'موجودی ارز شما کمتر از مقدار وارد شده می باشد.']);
//            if ($des_asset->amount == 0)
//                return response()->json(['status' => 500, 'msg' => 'موجودی ارز شما 0 می باشد.']);
//        }
//        $response = coinex()->new_trade($market, $type, $new_amount, $price, auth()->user()->id);
//        $response = json_decode($response);
//        if ($response->code == 0) {
//            $type = $type == 'buy' ? 1 : 0;
//
//            try {
//                $trade = new Trade();
//                $trade->user_id = auth()->user()->id;
//                $trade->id_trade = (string)$response->data->id;
//                $trade->status = $response->data->status;
//                $trade->market = $response->data->market;
//                $trade->type = $type;
//                $trade->finished_time = $response->data->finished_time;
//                $trade->trade_info = json_encode($response);
//                $trade->trade_info = json_encode($response);
//                $trade->amount = $response->data->amount;
//                $trade->old_amount = $amount;
//                $trade->price = $response->data->price;
//                $trade->total_amount = $total_amount;
//                $trade->fee_tx = 0;
//                $trade->save();
//
//                if ($type == 1) {
//                    $usdt_asset->amount -= $total_amount;
//                    $usdt_asset->save();
//                } else {
//                    $des_asset->amount -= $amount;
//                    $des_asset->save();
//                }
//
//                if ($trade->status == 1)
//                    trade_logs($trade);
//                return response()->json(['status' => 100, 'msg' => 'سفارش شما با موفقیت ثیت شد.']);
//            } catch (Exception $exception) {
//                return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
//            }
//        } elseif ($response->code == 107) {
//            return response()->json(['status' => 500, 'msg' => 'در حال حاضر امکان ثبت سفارش وجود ندارد. 30 دقیقه دیگر امتحان کنید.']);
//        } elseif ($response->code == 602) {
//            return response()->json(['status' => 500, 'msg' => 'مقدار وارد شده ارز کمتر از حد مجاز می باشد.']);
//        } else {
//            return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
//        }
//
//    }

//    public function cancel_trade($id)
//    {
//        $trade = Trade::query()->where('id_trade', $id)->first();
//        $response = coinex()->cancel_trade($trade->id_trade, $trade->market);
//        $response = json_decode($response);
//        if ($response->code == 0) {
//            try {
//                $trade->update([
//                    'status' => 'cancel',
//                    'trade_info' => json_encode($response),
//                ]);
//                if ($trade->type == 1) {
//                    $usdt_asset = auth()->user()->assets->where('unit', 'usdt')->first();
//                    $usdt_asset->amount += $trade->total_amount;
//                    $usdt_asset->save();
//                } else {
//                    $unit = strtolower(str_replace('USDT', '', $trade->market));
//                    $currency_asset = auth()->user()->assets->where('unit', $unit)->first();
//                    $currency_asset->amount += $trade->amount;
//                    $currency_asset->save();
//                }
//
//                return response()->json(['status' => 100, 'msg' => 'سفارش شما با موفقیت لغو شد.']);
//            } catch (Exception $exception) {
//                return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. بعدا دوباره امتحان کنید.']);
//            }
//
//        } else {
//            return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
//        }
//    }
