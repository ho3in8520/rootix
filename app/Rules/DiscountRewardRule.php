<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DiscountRewardRule implements Rule
{
    protected $request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (isset($this->request['type']) && $this->request['type'] == 0) {
            if ($value > 100) {
               return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'مقدار در حالت درصدی نمی تواند بیشتر از 100 باشد.';
    }
}
