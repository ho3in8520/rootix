<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LoginMail extends Mailable
{
    use Queueable, SerializesModels;

    private $token, $ip, $date_time;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $ip, $date_time)
    {
        $this->token = $token;
        $this->ip = $ip;
        $this->date_time = $date_time;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.login')->with(['token' => $this->token, 'ip' => $this->ip, 'date_time' => $this->date_time]);
    }
}
