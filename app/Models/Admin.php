<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable, HasFactory;

    protected $guard = 'admin';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ticketMaster()
    {
        return $this->hasMany(TicketMaster::class, 'accept_id', 'id');
    }

    public function userGuard(){
        return $this->morphOne(UserGuard::class, 'guardable');
    }

    public function getUserGuardIdAttribute()
    {
        $user_id = auth("admin")->user()->id;
        $user_guard = UserGuard::where("guardable_id", $user_id)->where("guardable_type",
            Admin::class)->first();
        return $user_guard->id;
    }

    public function getFullNameAttribute()
    {
        if ($this->first_name || $this->last_name)
            return $this->first_name . ' ' . $this->last_name;
        return null;
    }
}
