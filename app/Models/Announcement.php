<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    use HasFactory;

    protected $guarded= [];
    protected $casts = [
        'visited' => 'array',
    ];
    private $roles_count=0;


    public function __construct(array $attributes = [])
    {
        $this->roles_count= Role::query()->count();
        parent::__construct($attributes);
    }

    public function users()
    {
        return $this->morphedByMany(User::class, 'announcementable');
    }

    public function roles()
    {
        return $this->morphedByMany(Role::class, 'announcementable');
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function getGroupTextAttribute()
    {
        $ann_roles= count($this->roles);
        if ($ann_roles > 0 && $this->roles_count == $ann_roles)
            return 'همه';
        else if ($ann_roles > 0)
            return 'نقش‌ها';
        else
            return 'کاربران';
    }
    public function getGroupAttribute()
    {
        $ann_roles= count($this->roles);
        if ($ann_roles > 0 && $this->roles_count == $ann_roles)
            return 'all';
        else if ($ann_roles > 0)
            return 'roles';
        else
            return 'users';
    }
}
