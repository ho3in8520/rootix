<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    use HasFactory;

    protected $guarded;

    public function setStatusAttribute($val)
    {
        $status = 0;
        switch ($val) {
            case 'not_deal':
                $status = 0;
                break;
            case 'part_deal';
                $status = 0;
                break;
            case 'cancel';
                $status = 2;
                break;
            case 'done';
                $status = 1;
                break;
        }
        $this->attributes['status'] =$status;
}

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
