<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'title',
        'data',
        'priority',
        'status',
        'required',
    ];

    public function scopeForms($q,$id_completed_form){
        $q->select(['type','title','required'])->where('status',1)->whereNotIn('id',$id_completed_form)->orderBy('priority','asc');
    }

    public function userAuthForms()
    {
        return $this->hasMany(UserAuthForm::class,'auth_form_id');
    }
}
