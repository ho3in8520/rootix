<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawRial extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $hidden = ['admin_id','bank_id','user_id'];
    protected $table= 'withdraw_rial';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function getStatusInfoAttribute()
    {
        switch ($this->attributes['status']) {
            case 0:
                return ['text' => 'در حال بررسی', 'color' => 'warning', 'type' => 'Baresi' ];
            case 1:
                return ['text' => 'واریز شده', 'color' => 'success', 'type' => 'Acc'];
            case 2:
                return ['text' => 'رد شده', 'color' => 'danger', 'type' => 'Reject'];
        }
    }

    public function getDepositTypeAttribute()
    {
        switch ($this->attributes['deposit_type']) {
            case '0':
                return 'شتاب';
            case '1':
                return 'پایا';
            case '2':
                return 'سانتا';
            default:
                return null;
        }
    }
}
