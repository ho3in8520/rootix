<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'amount', 'fee', 'unit', 'address', 'network', 'status', 'reject_reason'];

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class, 'financeable');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function status($mode = 'span', $tooltip = false)
    {
        $html = '';
        $class = [0 => 'warning', 1 => 'warning', 2 => 'success', 3 => 'danger', 4 => 'danger'];
        $class_border = [0 => 'Baresi', 1 => 'Baresi', 2 => 'Acc', 3 => 'Reject', 4 => 'Reject'];
        $text = [0 => 'درحال بررسی', 1 => 'درحال بررسی', 2 => 'تایید شده', 3 => 'رد شده', 4 => 'رد شده'];
        $tooltip_tag = '';
        if ($tooltip == true && $this->status == 3) {
            $tooltip_tag = "<span class='attention animate__animated animate__flash animate__infinite infinite animate__slower 3s' data-toggle='tooltip' title='{$this->reject_reason}'>!</span>";
        }

        switch ($mode) {
            case 'p':
                $class = "text-{$class[$this->status]}";
                $html = "<p class='$class'>{$text[$this->status]} {$tooltip_tag}</p>";
                break;
            case 'span';
                $class = "text-{$class[$this->status]}";
                $html = "<span class='$class'>{$text[$this->status]} {$tooltip_tag}</span>";
                break;
            case 'border';
                $class = "withdraw-rls border-{$class_border[$this->status]}";
                $html = "{$tooltip_tag} <span style='width: max-content' class='$class mx-1'>{$text[$this->status]}</span>";
                break;
            default:
                $html = $text[$this->status];
                break;
        }
        return $html;
    }


    public function statusAdmin($mode = 'span', $tooltip = false)
    {
        $html = '';
        $class = [0 => 'warning', 1 => 'warning', 2 => 'success', 3 => 'danger', 4 => 'danger'];
        $class_border = [0 => 'Baresi', 1 => 'Baresi', 2 => 'Acc', 3 => 'Reject', 4 => 'Reject'];
        $text = [0 => 'درحال بررسی', 1 => 'تایید ادمین', 2 => 'تایید صرافی', 3 => 'رد شده از سمت ادمین', 4 => 'رد شده از سمت صرافی'];
        $tooltip_tag = '';
        if ($tooltip == true && $this->status == 3) {
            $tooltip_tag = "<span class='attention animate__animated animate__flash animate__infinite infinite animate__slower 3s' data-toggle='tooltip' title='{$this->reject_reason}'>!</span>";
        }

        switch ($mode) {
            case 'p':
                $html = '<p class="text-' . $class[$this->status] . '">' . $text[$this->status] . '</p>';
                break;
            case 'span';
                $html = '<span class="text-' . $class[$this->status] . '">' . $text[$this->status] . '</span>';
                break;
            case 'input';
                $html = '<input class="form-control text-' . $class[$this->status] . '" value="' . $text[$this->status] . '" disabled>';
                break;
            case 'border';
                $class = "withdraw-rls border-{$class_border[$this->status]}";
                $html = "{$tooltip_tag} <span style='width: max-content' class='$class mx-1'>{$text[$this->status]}</span>";
                break;
            default:
                $html = $text[$this->status];
                break;
        }
        return $html;
    }
}
