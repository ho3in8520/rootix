<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GlobalMarketApi extends Model
{
    use HasFactory;

    protected $guarded = [];

    //get usdt to ril
    public function getUsdtToRilAttribute()
    {
        $data = json_decode($this->attributes['value'], true);
        return $data['stats']['usdt-rls']['bestSell'];
    }

    // *************************************
    // *************** Scope ***************
    // *************************************

    // گرفتن قیمت ارزها
    public function scopePrice_currency($query)
    {
        return $query->where('type', 'price_currency');
    }

    // ****************************************
    // *************** Relations ***************
    // ****************************************


    // ****************************************
    // *************** Functions ***************
    // ****************************************

    // تبدیل قیمت ارز ها به ریال
    public function currency_to_rial($currency, $fields = ['bestSell'])
    {
        $data = json_decode($this->attributes['value'], true);
        $states = $data['stats'];
        $converted = [];
        if (is_array($currency)) {
            foreach ($currency as $item)
                if (isset($states["$item-rls"])) {
                    foreach ($fields as $field)
                        $converted["$item"][$field] = $states["$item-rls"][$field];
                }
            return $converted;
        } else {
            if (isset($states["$currency-rls"]))
                foreach ($fields as $field)
                    $converted["$currency"][$field] = $states["$currency-rls"][$field];
            return $converted;
        }
    }

    // تبدیل ارز ها به usdt
    public function currency_to_usdt($currency, $fields = ['bestSell'])
    {
        $data = json_decode($this->attributes['value'], true);
        $binance = $data['global']['binance'];

        if (is_array($currency)) {
            $converted = [];
            foreach ($currency as $item)
                if (isset($binance["$item"]))
                    $converted["$item"] = $binance["$item"];
            return $converted;
        } else
            if (isset($binance["$currency"]))
                return $binance["$currency"];
    }

    // get trx to usdt
    public function getTrxToUsdtAttribute()
    {
        $data = json_decode($this->attributes['value'], true);
        return $data['global']['binance']['trx'];
    }

    // get trx to usdt
    public function getUsdtToUsdtAttribute()
    {
        $data = json_decode($this->attributes['value'], true);
        return $data['global']['binance']['usdt'];
    }

    // get trx to usdt
    public function getBttToUsdtAttribute()
    {
        $data = json_decode($this->attributes['value'], true);
        return $data['global']['binance']['btt'];
    }
}
