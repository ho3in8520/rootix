<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $guarded;

    public function announcements(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(Announcement::class, 'announcementable');
    }
}
