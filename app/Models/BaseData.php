<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseData extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'name',
        'extra_field1',
        'extra_field2',
        'extra_field3',
        'status'
    ];

    public function scopeWithdraw($q)
    {
        return $q->where('type','transactions')->where('name','withdraw')->where('extra_field1',3);
    }

    public function scopeDeposit($q)
    {
        return $q->where('type','transactions')->where('name','deposit')->where('extra_field1',4);
    }
}
