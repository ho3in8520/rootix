<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketDetail extends Model
{
    use HasFactory;
    protected $table = 'ticket_detail';

    protected $fillable = ['user_id', 'type', 'master_id', 'description'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function ticket_master()
    {
        return $this->belongsTo(TicketMaster::class,'master_id');
    }

}
