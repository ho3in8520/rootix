<?php

namespace App\Models;

use App\Models\Scopes\MergeAssetsCurrencies;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    use HasFactory;

    protected $fillable = ['currency_id', 'user_id', 'amount', 'token'];

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub
        static::addGlobalScope(new MergeAssetsCurrencies());
    }

    public function transaction()
    {
        return $this->morphMany(Finance_transaction::class, 'financeable');
    }

    public function getAddressHexAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_hex;
    }

    public function getAddressAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_base58;
    }

    public function getPrivateKeyAttribute()
    {
        $address = $this->attributes['token'];
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->private_key;
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class,'currency_id');
    }

}
