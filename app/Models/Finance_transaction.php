<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Finance_transaction extends Model
{
    protected $table = 'finance_transactions';

    protected $guarded;

    public function financeable()
    {
        return $this->morphTo(__FUNCTION__, 'financeable_type', 'financeable_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setTheterRlsAttribute($val){
        $theter_price=get_specific_currencies($val);
        $theter_price=$theter_price['USDT']['rls'];
        $this->attributes['theter_rls']=$theter_price;
    }

    public function base_data()
    {
        return $this->belongsTo(BaseData::class,'transact_type');
    }
}
