<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable=[
        'refer_id',
        'user_id',
        'type',
        'is_viewed',
        'details',
    ];

    protected $casts = [
        'details' => 'array',
    ];

    public function user_guard(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(UserGuard::class,'user_id');
    }

    public function notification_view(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(NotificationView::class,'notification_id');
    }
}
