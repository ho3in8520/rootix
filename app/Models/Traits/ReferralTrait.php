<?php

namespace App\Models\Traits;

use App\Models\Asset;
use App\Models\BaseData;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

trait ReferralTrait
{
    private $transact_id;
    private $percent;
    private $status;

    public function __construct()
    {
        $query = BaseData::select([
            'id',
            'percent' => BaseData::select('extra_field1')->where('type', 'referral_percent')->limit(1),
            'status' => BaseData::select('status')->where('type', 'referral_percent')->limit(1),
        ])->where('type', 'transactions')->where('extra_field1', 9)->first();
        if ($query ==  null){
            $this->transact_id = 0;
            $this->percent = 0;
            $this->status = 0;
        }else{
            $this->transact_id = $query->id;
            $this->percent = $query->percent;
            $this->status = $query->status;
        }

    }

    public function referral($amount, $unit)
    {
        if ($this->status) {
            $referral = $this->checkReferralUser();
            if ($referral) {
                return $this->calculations($referral, $amount, $unit);
            }
        }
    }

    private function checkReferralUser()
    {
        return $referral = $this->referral_user;
    }

    private function calculations($user, $amount, $unit)
    {
        $amount = ($this->percent * $amount) / 100;
        $amount_to_usdt = convert_currency($unit, 'usdt', $amount);
        DB::beginTransaction();
        try {
            $asset = Asset::where('user_id', $user->id)->where('unit', 'usdt')->first();
            $asset->amount += $amount_to_usdt;
            $asset->save();
            $asset->transaction()->create([
                'user_id' => $user->id,
                'transact_type' => $this->transact_id,
                'amount' => $amount_to_usdt,
                'type' => 2,
                'description' => "واریز سود کاربر {$user->email}",
            ]);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            return false;
        }
    }
}
