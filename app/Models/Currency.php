<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable= ['name','name_fa','unit', 'type_token','network','color','priority','status','withdraw_status','deposit_status'];

    public function files()
    {
        return $this->morphMany('App\Models\UploadedFile', 'uploadable');
    }

    public function getAmountAttribute()
    {
        return $this->pivot->amount;
    }
    public function getTokenAttribute()
    {
        return $this->pivot->token;
    }

    public function getAddressHexAttribute()
    {
        $address = $this->token;
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_hex;
    }

    public function getAddressAttribute()
    {
        $address = $this->token;
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->address_base58;
    }

    public function getPrivateKeyAttribute()
    {
        $address = $this->token;
        if (empty($address))
            return '';
        $address = json_decode($address);

        return $address->private_key;
    }
}
