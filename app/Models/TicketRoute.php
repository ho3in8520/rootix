<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketRoute extends Model
{
    protected $table = 'ticket_route';

    protected $fillable = ['master_id', 'from_id', 'to_id', 'mode'];
    use HasFactory;
}
