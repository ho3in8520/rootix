<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAuthForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'auth_form_id',
        'user_id',
        'value',
        'status',
        'reject_reason',
    ];

    protected $appends = ['user_level'];

    public function auth_form()
    {
        return $this->belongsTo(AuthForm::class, 'auth_form_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeCompleteForms($q, $user_id)
    {
        return $q->where('user_id', $user_id)->whereIn('status', [0, 1, 2]);
    }

    public function scopeUserLevel($q, $user_id = null)
    {
        if ($user_id)
            return $q->where('user_id', $user_id)->where('status', 2)->orderBy('id', 'desc');
        return $q->where('status', 2)->orderBy('id', 'desc');
    }

    public function getUserLevelAttribute()
    {
        $array = [1 => 'یک', 2 => 'دو', 3 => 'سه', 4 => 'چهار', 5 => 'پنج'];
        return (isset($this->auth_form) && !empty($this->auth_form)) ? $array[$this->auth_form->priority] : 'نامشخص';
    }
}
