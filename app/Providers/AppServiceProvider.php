<?php

namespace App\Providers;

use App\Models\Bank;
use App\Models\Notification;
use App\Models\TicketMaster;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::defaultView('vendor.pagination.custom-pagination');
//        Paginator::useBootstrap();
        Schema::defaultStringLength(191);
        view()->composer('*', function ($view) {
            $admin_notifications = [];
            $notifications = [];
            $announcements = [];

            // مخصوص کسی که لاگین کردس
            if (\auth()->check()) {
                /*
                 * // احتمالا برای نوتیفیکیشن های ادمین بدرد بخوره
                 * $notifications = \auth()->user()
                    ->notifications()
                    ->where('is_viewed',0)
                    ->select(DB::raw('notifications.type'), DB::raw('count(*) as count'), DB::raw('MAX(notifications.created_at) as last_update'), 'extra_field1','extra_field2')
                    ->groupBy('type')
                    ->join('base_data', 'base_data.id', '=', 'notifications.type')
                    ->get();*/

                $admin_notifications = get_admin_notification();
                $notifications = get_notification();
                $announcements = get_announcements();
                $number_ticket_user = TicketMaster::where('user_id', Auth::user()->id)->where('status', 2)->where('accept_id', '!=', null)->count();
            }
            $number_ticket_admin = TicketMaster::where('accept_id', null)->count();
            $view->with([
                'number_ticket_admin' => $number_ticket_admin ?? '',
                'number_ticket_user' => $number_ticket_user ?? '',
                'notifications' => $notifications,
                'admin_notifications' => $admin_notifications,
                'announcements' => $announcements,
            ]);
        });


        // مربوط به پنل ادمین
        if (request()->is('admin/*')) {
            $sidebar_count['new_bank'] = Bank::query()->whereIn('status', [0, 2])->count();
            view()->share(['new_bank_count' => $sidebar_count['new_bank']]);
        }

    }
}
