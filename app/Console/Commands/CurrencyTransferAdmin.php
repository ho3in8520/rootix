<?php

namespace App\Console\Commands;

use App\Library\tron\Tron;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Currency;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CurrencyTransferAdmin extends Command
{
    use Tron;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $result = \App\Models\TransactionTron::select(['user_id', 'status', 'unit', 'id', 'amount_commission', 'commission_hash', 'transfer_hash', 'ethers_data', 'amount', 'blockchain_type',
            'address' => DB::table('assets')->select('token')->whereColumn('assets.user_id', 'transaction_trons.user_id')->whereNotNull('token')->limit(1),
            'token' => Currency::query()->select('type_token')->whereColumn('transaction_trons.unit', 'currencies.unit')->limit(1),
            'network' => Currency::query()->select('network')->whereColumn('transaction_trons.unit', 'currencies.unit')->limit(1)
        ])->whereIn('status', [0, 1, 2, 3, 4, 5, 6])->get();

        foreach ($result as $item) {
            if ($item->status == 0) {

            } elseif ($item->status == 1) {
                \Illuminate\Support\Facades\DB::beginTransaction();
                try {
                    $transfer = Tron()->sendTransaction(json_decode($item->address)->address_hex, $item->amount_commission, '', env('ADDRESS_TRON_BASE'));
                    $item->commission_hash = $transfer['txID'];
                    $item->status = 2;
                    $item->save();
                    \Illuminate\Support\Facades\DB::commit();
                } catch (\Exception $e) {
                    \Illuminate\Support\Facades\DB::rollBack();
                }
            } elseif ($item->status == 2) {
                try {
                    $result = \Tron()->getTransaction($item->commission_hash);
                    if ($result['ret'][0]['contractRet'] == 'SUCCESS') {
                        $item->status = 4;
                    } else {
                        $item->status = 3;
                    }
                } catch (\Exception $e) {
                    $item->status = 3;
                }
                $item->save();
            } elseif ($item->status == 3) {
                /// fail fail withdraw admin
                $item->status = 3;
                $item->save();
            } elseif ($item->status == 4) {
                $token = $item->network . $item->token;
                $address = coinex()->get_deposit_address($item->unit,$token);
                $address=json_decode($address)->data->coin_address;
                $address_to_hex = Tron()->toHex($address);
                \Illuminate\Support\Facades\DB::beginTransaction();
                try {
                    if ($item->unit == 'trx') {
                        $transfer = Tron(json_decode($item->address)->private_key)->sendTransaction($address_to_hex, $item->amount, '', json_decode($item->address)->address_hex);
                    } else {
                        switch (contractUnit($item->unit)['type_token']) {
                            case 10;
                                $transfer = Tron(json_decode($item->address)->private_key)->sendToken($address_to_hex, $this->amount($item->amount, contractUnit($item->unit)['precision'], 'multiplication'), contractUnit($item->unit)['contract'], json_decode($item->address)->address_hex);
                                break;
                            case 20;
                                $transfer = Tron(json_decode($item->address)->private_key)->triggerSmartContractWithdraw($item->ethers_data, contractUnit($item->unit)['contract'], json_decode($item->address)->address_hex);
                                break;
                        }
                    }
                    $item->transfer_hash = $transfer['txID'];
                    $item->status = 5;
                    $item->save();
                    \Illuminate\Support\Facades\DB::commit();
                } catch (\Exception $e) {
                    \Illuminate\Support\Facades\DB::rollBack();
                }
            } elseif ($item->status == 5) {
                $transaction = BaseData::where('type', 'transactions')->where('extra_field1', 4)->first();
                DB::beginTransaction();
                try {
//                    $result = \Tron()->getTransaction($item->transfer_hash);
                    if (coinex()->inquiry_deposit_currency_to_coinex($item->unit, $item->transfer_hash, $item->blockchain_type) == 'finish') {
                        $item->status = 7;
                        $model = Asset::query()->where('unit', $item->unit)->where('user_id', $item->user_id)->first();
                        $model->amount += $item->amount;
                        $model->save();
                        $model->transaction()->create([
                            'tracking_code' => $item->transfer_hash,
                            'user_id' => $item->user_id,
                            'transact_type' => $transaction->id,
                            'amount' => $item->amount,
                            'type' => 2,
                            'description' => "واریز {$item->amount} {$item->amount} به کیف پول",
                        ]);
                    } elseif(coinex()->inquiry_deposit_currency_to_coinex($item->unit, $item->transfer_hash, $item->blockchain_type) != 'cancel') {
                        $item->status = 5;
                    }else{
                        $item->status = 6;
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    $item->status = 6;
                }
                $item->save();
            }
        }
        return 1;
    }
}
