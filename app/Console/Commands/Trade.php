<?php

namespace App\Console\Commands;

use App\Jobs\TradeTransaction;
use Illuminate\Console\Command;

class Trade extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:trade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update List Queue Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model=\App\Models\Trade::select(['id','id_trade','status','market'])->where('status',0)->get();
        foreach ($model as $item) {
            dispatch(new TradeTransaction($item->id,$item->id_trade,$item->status,$item->market));
            }
        return Command::SUCCESS;
    }
}
