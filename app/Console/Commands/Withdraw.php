<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Withdraw extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'withdraw:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $array = ['confirming', 'finish'];
        $withdrawals = \App\Models\Withdraw::where('status', 1)->whereNotNull('coin_withdraw_id')->where('coin_withdraw_id', '!=', 0)->get();
        foreach ($withdrawals as $withdrawal) {
            $result = coinex()->inquire_withdrawal_list($withdrawal->coin_withdraw_id);
            $result = json_decode($result, true);
            if ($result['code'] == 0) {
                if (in_array($result['data']['data'][0]['status'], $array)) {
                    $withdrawal->tx_id = $result['data']['data'][0]['tx_id'];
                    $withdrawal->status = 2;
                    $withdrawal->save();
                } elseif ($result['data']['data'][0]['status'] == 'fail') {
                    $withdrawal->status = 4;
                    $withdrawal->save();
                }
            }
        }
        return Command::SUCCESS;
    }
}
