<?php

namespace App\Observers;

use App\Models\Asset;
use App\Models\Currency;
use App\Models\Role;
use App\Models\User;
use App\Models\UserGuard;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function created(User $user)
    {
        $currencies = Currency::all(['id']);
        $array = [];
        $token = Tron()->generateAddress();
        foreach ($currencies as $currency) {
            array_push($array,
                [
                    'user_id' => $user->id,
                    'currency_id' => $currency->id,
                    'amount' => 0, 'token' => json_encode($token->getRawData()),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
        }
        Asset::insert($array);
        $user->userGuard()->create();
        $role_user=Role::query()->where('name','user')->first();
        $user->assignRole($role_user->id);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param \App\Models\User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
