<?php

namespace App\Jobs;

use App\Mail\LoginMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class AfterLogin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email, $token, $mobile, $ip, $date_time;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $mobile, $token, $ip, $date_time)
    {
        $this->email = $email;
        $this->token = $token;
        $this->mobile = $mobile;
        $this->ip = $ip;
        $this->date_time = $date_time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new LoginMail($this->token, $this->ip, $this->date_time));
        if ($this->mobile)
            smsVerify($this->date_time, $this->mobile, 'notification-user-login-panel');
    }
}
