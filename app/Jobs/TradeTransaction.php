<?php

namespace App\Jobs;

use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\Trade;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TradeTransaction implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $id_trade;
    private $id_database;
    private $status;
    private $market;

    public function __construct($id_database, $id_trade, $status, $market)
    {
        $this->id_database = $id_database;
        $this->id_trade = $id_trade;
        $this->status = $status;
        $this->market = $market;
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return $this->id_database;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $response = coinex()->get_trade('order_status', ['id' => $this->id_trade, 'market' => $this->market]);
        $response = json_decode($response);
        if ($response != null){
            if ($response->message == 'Success' || $response->code == 0) {
                $model = Trade::find($this->id_database);
                if (coinex()->trade_status($response->data->status) != $model->status) {
                    $model->status = $response->data->status;
                    $model->save();
                }
                if ($model->status == 1){
                    trade_logs($model);
                }
            }
        }
    }
}
