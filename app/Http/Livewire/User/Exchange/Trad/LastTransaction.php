<?php

namespace App\Http\Livewire\User\Exchange\Trad;

use Illuminate\Support\Facades\Http;
use Livewire\Component;
use Mockery\Exception;

class LastTransaction extends Component
{
    public $markets;
    public $market;
    public $readyToLoad = false;
    public function render()
    {
        $this->market = strtoupper(implode('', $this->markets));
        return view('livewire.user.exchange.trad.last-transaction', ['result' => $this->readyToLoad ? $this->get_request_transaction() : []]);
    }

    public function loadRequestTransaction()
    {
        $this->readyToLoad = true;
    }

    private function get_request_transaction()
    {
        try {
            $result = Http::get('https://api.coinex.com/v1/market/deals?limit=15&market=' . $this->market);
            if ($result->status() == 200) {
                $data = json_decode($result->body(), true)['data'];
                foreach ($data as $key => $item) {
                    if ($item['type'] == 'buy') {
                        $data[$key]['color'] = 'green';
                    } else {
                        $data[$key]['color'] = 'red';
                    }
                }
                return $data;
            } else
                return false;
        }catch (Exception $e){
            return 'موردی یافت نشد';
        }
}
}
