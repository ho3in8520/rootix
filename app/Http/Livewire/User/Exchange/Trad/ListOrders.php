<?php

namespace App\Http\Livewire\User\Exchange\Trad;

use Livewire\Component;

class ListOrders extends Component
{
    public function render()
    {
        return view('livewire.user.exchange.trad.list-orders');
    }
}
