<?php

namespace App\Http\Livewire\User\Exchange\Trad;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class ListRequest extends Component
{
    public $markets;
    public $market;
    public $readyToLoad = false;
    private $max;


    public function loadRequestSell()
    {
        $this->readyToLoad = true;
    }

    public function render()
    {
        $this->market = strtoupper(implode('', $this->markets));
        return view('livewire.user.exchange.trad.list-request', ['result' => $this->readyToLoad ? $this->get_request() : ['sells' => [], 'buys' => [], 'last' => 0]]);
    }

    private function get_request()
    {
        try {
            $result = Http::get('https://api.coinex.com/v1/market/depth?market=' . $this->market . '&limit=50&merge=0');
            if ($result->status() == 200) {
                $data = json_decode($result->body(), true)['data'];

                foreach ($data['asks'] as $key => $item) {
                    $data['asks'][$key]['total'] = $item[0] * $item[1];
                }
                foreach ($data['bids'] as $key => $item) {
                    $data['bids'][$key]['total'] = $item[0] * $item[1];
                }
                return array_merge($this->percent_background($data), ['last' => $data['last']]);
            }
        } catch (\Exception $e) {
            return ['sells' => [], 'buys' => []];
        }
    }

    private function percent_background($data)
    {
        $buys = [$data['bids']];
        $sells = [$data['asks']];

        $sell = $this->percent_background_mode($this->multisort($sells, "0", 'sell'), 'sell');
        $buy = $this->percent_background_mode($this->multisort($buys, "0", 'buy'), 'buy');
        return ['sells' => $sell, 'buys' => $buy];
    }

    private function percent_background_mode($data, $mode)
    {
        $max = $this->max($data);
        if ($mode == 'sell') {
            foreach ($data as $k => $row) {
                $percent = ($row['total'] / $max) == 0 ? 0 : ($row['total'] / $max);
                $data[$k] = array_merge($data[$k], ['percent_background' => ($percent * 100) + 13]);
            }
        } else {
            foreach ($data as $k => $row) {
                $percent = ($row['total'] / $max) == 0 ? 0 : ($row['total'] / $max);
                $data[$k] = array_merge($data[$k], ['percent_background' => ($percent * 100) + 13]);
            }
        }
        return $data;
    }

    private function multisort(&$array, $key, $mode = 'sell')
    {
        $valsort = array();
        $ret = array();
        reset($array);
        foreach ($array[0] as $ii => $va) {
            $valsort[$ii] = $va[$key];
        }
        asort($valsort);
        foreach ($valsort as $ii => $va) {
            $ret[$ii] = $array[0][$ii];
        }
//        return $ret;
        return $mode == 'sell' ? $ret : array_reverse($ret);
    }

    private function max(array $data)
    {
        $array=[];
        foreach ($data as $key => $item) {
           $array[]=$item['total'];
        }
        return max($array);
    }
}

