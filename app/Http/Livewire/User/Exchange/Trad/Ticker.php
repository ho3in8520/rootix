<?php

namespace App\Http\Livewire\User\Exchange\Trad;

use Illuminate\Support\Facades\Http;
use Livewire\Component;

class Ticker extends Component
{
    public $markets;
    public $market;
    public $readyToLoad = false;
    private $ticker = [
        'btc',
        'eth',
        'trx',
        'bnb',
        'shib',
        'doge',
        'xrp',
        'ada',
        'sol',
        'btt',
        'jst',
        'win',
    ];

    public function render()
    {
//        $this->market = strtoupper(implode('', $this->markets));
        return view('livewire.user.exchange.trad.ticker', ['result' => $this->readyToLoad ? $this->get_request() : []]);
    }

    public function loadRequestTicker()
    {
        $this->readyToLoad = true;
    }

    private function get_request()
    {
        try {
            $result = get_specific_currencies(implode(',', $this->ticker));
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }
//
//    private function selectUnitTicker(array $data)
//    {
//        $array=[];
//
//        foreach ($data as $key=>$item)
//        {
//            if (in_array($key,array_keys($this->ticker))){
//                $array[$this->ticker[$key]][$key]=$data[$key];
//            }
//        }
//        return $array;
//    }
}
