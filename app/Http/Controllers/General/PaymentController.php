<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\transactionGateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Shetabit\Multipay\Invoice;

use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Payment\Facade\Payment;

class PaymentController extends Controller
{
    public function payment(Request $request)
    {
        $request->validate([
            'amount' => ['required', function ($attr, $val, $fail) {
                $amount = (int)str_replace(',', '', $val);
                if (preg_match("/^[0-9]$/", $amount)) {
                    $fail('مقدار باید از نوع عدد باشد');
                }
                if (amountTransactionDays()->total + unit_to_rial($amount) > transactionLimit()) {
//                    $fail("درخواست بیشتر از حد مجاز سقف تراکنش روزانه شما " . number_format(transactionLimit()));
                    $fail("درخواست بیشتر از حد مجاز سقف تراکنش روزانه شما " . rial_to_unit(transactionLimit(),'rls',true));
                }
            }],
            'mode' => 'required|in:asset',
            'gateway' => 'required|in:zarinpal,idpay'
        ]);
        $gateway = $request->gateway;
        $amount = (int)str_replace(',', '', $request->amount);
//        $amount = in_array($request->gateway, ['zarinpal']) ? $amount / 10 : $amount;
        if (get_unit() == 'rls')
            $amount = in_array($request->gateway, ['zarinpal']) ? rial_to_unit($amount, 'rls') : $amount;
        else
            $amount = in_array($request->gateway, ['zarinpal']) ? $amount : unit_to_rial($amount);

        try {
            $invoice = (new Invoice)->amount($amount);
            return Payment::via($gateway)->purchase($invoice, function ($driver, $transactionId) use ($invoice, $request, $amount, $gateway) {
                $amount = in_array($request->gateway, ['zarinpal']) ? unit_to_rial($amount) : $amount;
                $trans = new transactionGateway();
                $trans->user_id = auth()->user()->id;
                $trans->gateway = $gateway;
                $trans->amount = $amount;
                $trans->transaction_id = $transactionId;
                $trans->mode = $request->mode;
                $trans->status = 0;
                $trans->save();
            })->pay()->render();
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'danger', 'msg' => $e->getMessage()]);
        }
    }

    public function paymentSuccess(Request $request)
    {
        $transaction_id = $request->has('Authority') ? $request->Authority : $request->id;
        $trans = transactionGateway::where('transaction_id', $transaction_id)->firstOrFail();
//        $amount3 = in_array($trans->gateway, ['zarinpal']) ? rial_to_unit($trans->amount,'rial')  : $trans->amount;
//        $amount2 = $trans->amount;
        $amount=$amount2=$trans->amount;
        if (in_array($trans->gateway, ['zarinpal']))
            $amount2=$amount/10;
        $trans_type = BaseData::where('type', 'transactions')->where('extra_field1', 1)->first();
        try {
            $receipt = Payment::via($trans->gateway)->amount((int)$amount2)->transactionId($transaction_id)->verify();
            $amount = (int)$trans->amount;
            $unit=get_unit();
            $trans->reference_id = $receipt->referenceId;
            $trans->status = 1;
            $trans->save();
            $asset = Asset::where('user_id', $trans->user_id)->where('unit', 'rls')->first();
            $asset->amount = $asset->amount + $amount;
            $asset->save();
//            $amount2=rial_to_unit($amount,'rial');
            $ReferenceId = $receipt->getReferenceId();
            $asset->transaction()->create([
                'user_id' => $trans->user_id,
                'refer_id' => $trans->id,
                'amount' => (int)$amount,
                'type' => 2,
                'transact_type' => $trans_type->id,
                'tracking_code' => $ReferenceId,
                'description' => 'شارژ کیف پول به مبلغ: '.rial_to_unit($amount,'rls',true)
//                'description' => "شارژ کیف پول $unit به مبلغ $amount2"
            ]);
            $msg = "شارژ کیف پول به مبلغ " .rial_to_unit($amount,'rls',true);
            $msg .= " | ";
            $msg .= "کد پیگیری شما: " . $ReferenceId;
            Auth::loginUsingId($trans->user_id);
            return redirect()->route('user.dashboard')->with('flash', ['type' => 'success', 'msg' => $msg]);

        } catch (InvalidPaymentException $exception) {
            $trans->status = 2;
            $trans->save();
            return redirect()->route('user.dashboard')->with('flash', ['type' => 'danger', 'msg' => $exception->getMessage()]);
        }
    }

    public function paymentShow($type)
    {
        return view('user.dashboard');
    }

    public function chargeWallet()
    {
        $asset_rial = Asset::where('user_id', \auth()->user()->id)->where('unit', 'rls')->value('amount');
        return view('user.wallet.charge', compact('asset_rial'));
    }
}
