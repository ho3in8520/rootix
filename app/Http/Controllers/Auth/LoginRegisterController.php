<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\User\LoggedInDeviceManager;
use App\Jobs\AfterLogin;
use App\Jobs\ForceLogout;
use App\Mail\ForgetPassword;
use App\Mail\StepMail;
use App\Models\Country;
use App\Models\User;
use App\Rules\StrengthPasswordFormat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use RealRashid\SweetAlert\Facades\Alert;

class LoginRegisterController extends Controller
{
    public function form($type = 'login')
    {
        $countries = Country::all();
        return view("landing.auth.login_register", compact('countries', 'type'));
    }

    /**
     * ویو مربوط به فراموشی رمز عبور
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function forgetPasswordForm()
    {
        $message = '';
        if (\request()->has('token')) { // اگر توکن ست شده بود
            $user = User::query()->where('verification', \request()->token)
                ->where('time_verification', '>', now())
                ->first();
            if ($user) {
                $message = [
                    'type' => 'change-pass',
                    'text' => 'لینک فعالسازی معتبر میباشد',
                    'token' => \request()->token,
                ];
            } else {
                $message = [
                    'type' => 'error',
                    'text' => 'لینک بازیابی رمز عبور معتبر نمیباشد'
                ];
            }
        }
        return view('landing.auth.main_forget-password', compact('message'));
    }

    /**
     * ارسال ایمیل فراموشی رمز عبور
     */
    public function forgetPasswordSendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);
        $token = Str::random(100);
        $link = route('login.forget_password.form', ['token' => $token]);
        Mail::to(\request()->email)->send(new ForgetPassword($link));
        $user = User::query()->where('email', $request->email)->first();
        $user->verification = $token;
        $user->time_verification = now()->addMinutes(15);
        $user->save();
        Session::flash('success', 'لینک بازیابی رمز با موفقیت به ایمیل شما ارسال شد');
        return redirect()->back();
    }

    /**
     * تغییر رمز بعد از ارسال لینک بازیابی رمز عبور
     */
    public function forgetChangePassword(Request $request)
    {
        $request->validate([
            'password' => ['required', 'min:8', 'confirmed', new StrengthPasswordFormat()],
            'token' => 'required|exists:users,verification'
        ]);
        $user = User::query()->where('verification', \request()->token)
            ->where('time_verification', '>', now())
            ->first();
        $user->verification = '';
        $user->time_verification = '';
        $user->password = Hash::make($request->password);
        $user->save();
        \alert()->success('', 'رمز عبور شما با موفقیت بازیابی شد.');
        return redirect()->route('login.form');
    }

    public function loginForm()
    {
        $countries = Country::all();
        return view("landing.auth.main_login", compact('countries'));
    }

    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ], [
            'g-recaptcha-response.required' => 'فیلد ریکپچا الزامی میباشد',
            'g-recaptcha-response.recaptcha' => 'در تایید ریکپچا مشکلی پیش آمده است'
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $token = bin2hex(base64_encode(random_bytes(50)));
            \auth()->user()->login_token = $token;
            \auth()->user()->save();
            $date = jdate_from_gregorian(date('Y/m/d H:i:s'), 'Y/m/d-H:i:s');
            AfterLogin::dispatch($request->email, \auth()->user()->mobile, $token, user_ip(), $date);
            return redirect()->route('user.dashboard');
        }
        return redirect()->back()->with('flash', ['type' => 'error', 'msg' => 'اطلاعات شما با سوابق ما مطابقت ندارد.'])->withInput();
//        return response()->json(['status' => 500, 'msg' => 'اطلاعات شما با سوابق ما ما مطاقبت ندارد']);
    }

    public function register()
    {
        $countries = Country::all();
        return view("landing.auth.main_register", compact('countries'));
    }

    public function registerStore(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users',
            'password' => ['required', 'min:8', 'confirmed', new StrengthPasswordFormat()],
            'laws' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
            'referral_code' => 'nullable|exists:users,code'
        ], [
            'g-recaptcha-response.required' => 'فیلد ریکپچا الزامی میباشد',
            'g-recaptcha-response.recaptcha' => 'در تایید ریکپچا مشکلی پیش آمده است'
        ]);
        $laws = $request->laws === 'on' ? 1 : 0;
//        dd($laws,$request->laws);
        try {
            $user = User::create([
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'country_id' => $request->country,
                'accept_laws' => $laws,
                'code' => rand(1000, 9999),
                'referral_id' => $request->referral_code
            ]);
            Auth::login($user);
            return redirect()->route('user.dashboard');
        } catch (\Exception $e) {
            return redirect()->back()->with('flash', ['type' => 'error', 'msg' => 'ثبت اطلاعات با خطا مواجه شد'])->withInput();
        }
    }

    /*
     * در صورتی که کاربر بعد از لاگین از ایمیل خودش روی خروج از پنل کلیک کرد
    همه کاربر هارو خارج میکنه و به صورت sms یک پیام جدید حاوی رمز عبور برای کاربر ارسال میکنه
    */
    public function force_logout(Request $request)
    {
        $new_password = random_int(10000, 999999);
        $token = $request->token;
        $user = User::query()
            ->where('login_token', $token)
            ->firstOrFail(['id', 'password', 'login_token']);
        $user->password = Hash::make($new_password);
        $user->login_token = null;
        $user->save();
        $user->fresh();
        $user = Auth::loginUsingId($user->id);
        (new LoggedInDeviceManager())->logoutAllDevices(new Request());
        $text = 'کاربر مورد نظر با موفقیت از پنل شما خارج شد' . "\n\r";
        $text .= 'رمز عبور جدید شما: ' . $new_password;
        Alert::success('موفقیت آمیز!', $text);
        ForceLogout::dispatch($user->email, $user->mobile, $new_password);
        return redirect(route('user.dashboard'));
    }

}
