<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use RealRashid\SweetAlert\Facades\Alert;

class GoogleController extends Controller
{
    public function loginWithGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackFromGoogle()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();

            // Check Users Email If Already There
            $is_user = User::where('email', $user->email)->first();
            if(!$is_user){

                $saveUser = User::updateOrCreate([
                    'google_id' => $user->getId(),
                ],[
                    'email' => $user->getEmail(),
                    'code' => rand(1000, 9999),
                    'password' => Hash::make($user->getName().'@'.$user->getId())
                ]);
            }else{
                $saveUser = User::where('email',  $user->getEmail())->first();
                $saveUser->google_id = $user->getId();
                $saveUser->save();
            }


            Auth::loginUsingId($saveUser->id);

            return redirect()->route('user.dashboard');
        } catch (\Throwable $th) {
            \alert()->error('','خطایی رخ داده است. لطفا بعدا دوباره امتحان کنید.');
            return redirect()->route('login.form');
            throw $th;
        }
    }
}
