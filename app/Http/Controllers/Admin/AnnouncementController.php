<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AnnouncementController extends Controller
{
    public function index()
    {
        $announcement = Announcement::query()->paginate(20);
        return view('admin.announcement.index', compact('announcement'));
    }

    public function create()
    {
        $roles = Role::all();
        $users = User::all(); // این قسمت بعدا تبدیل میشه به سرچ ایجکسی
        return view('admin.announcement.create', compact('roles', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'text' => 'required',
            'group' => 'required|in:all,roles,users',
            'annable_id' => "required|exists:" . ($request['group'] == 'all' ? 'roles' : $request['group']) . ",id",
            'started_at' => 'required',
            'end_at' => 'nullable',
            'img' => 'required|mimes:jpeg,png,jpg',
        ]);
        $request['started_at'] = jalali_to_gregorian($request->started_at . " 00:00:00");
        $request['end_at'] = $request->end_at ? jalali_to_gregorian($request->end_at . " 00:00:00") : null;
        $request['visited'] = [];

        $announcement = Announcement::query()->create($request->only(['title', 'excerpt', 'text', 'started_at', 'end_at', 'visited']));
        $announcement->files()->create([
            'path' => upload($request->file('img'), 'announcement'),
            'type' => 'announcement',
            'user_id' => 1
        ]);

        $request['group'] = $request['group'] == 'all' ? 'roles' : $request['group'];
        $announcement->{$request->group}()->attach($request->annable_id);
        return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت ذخیره شدند']);
    }

    /**
     * Display the specified resource.
     */
    public function show(Announcement $announcement)
    {
        $annable = $announcement->group == 'all' ? [] : $announcement->{$announcement->group}()->get();
        return view('admin.announcement.show', compact('announcement', 'annable'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Announcement $announcement
     */
    public function edit(Announcement $announcement)
    {
        $roles = Role::all();
        $users = User::all();
        $group = $announcement->group == 'all' ? 'roles' : $announcement->group;
        $annable_id = $announcement->{$group}()->get(['id'])->pluck('id')->toArray();
        return view('admin.announcement.edit', compact('roles', 'users', 'announcement', 'annable_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Models\Announcement $announcement
     */
    public function update(Request $request, Announcement $announcement)
    {
        $request->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'text' => 'required',
            'group' => 'required|in:all,roles,users',
            'annable_id' => "required|exists:" . ($request['group'] == 'all' ? 'roles' : $request['group']) . ",id",
            'started_at' => 'required',
            'end_at' => 'nullable',
            'img' => 'nullable|mimes:jpeg,png,jpg',
        ]);
        $request['started_at'] = jalali_to_gregorian($request->started_at . " 00:00:00");
        $request['end_at'] = $request->end_at ? jalali_to_gregorian($request->end_at . " 00:00:00") : null;
        $request['visited'] = [];

        $announcement->update($request->only(['title', 'excerpt', 'text', 'started_at', 'end_at', 'visited']));
        if ($request->img) {
            $announcement->files()->delete();
            $announcement->files()->create([
                'path' => upload($request->file('img'), 'announcement'),
                'type' => 'announcement',
                'user_id' => 1
            ]);
        }

        $group = $announcement->group == 'all' ? 'roles' : $announcement->group;
        $request['group'] = $request['group'] == 'all' ? 'roles' : $request['group'];

        $announcement->{$group}()->detach();
//        dd("Sdfsdf");
        $announcement->{$request->group}()->attach($request->annable_id);
        return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت ذخیره شدند']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Announcement $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
