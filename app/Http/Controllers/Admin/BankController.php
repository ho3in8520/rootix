<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:banks_list_menu'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:bank_accept_btn'])->only('confirm');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:bank_reject_btn'])->only('reject');
    }

    public function index(Request $request)
    {
        $banks= Bank::query();

        $date_from = \request()->date_from ? jalali_to_gregorian(request()->date_from . " 00:00:00") : false;
        $date_to = \request()->date_to ? jalali_to_gregorian(request()->date_to . " 23:59:59") : false;

        \request()->fullname? $banks->whereHas('user', function ($q) use ($request){
            $q->where('first_name',  \request()->fullname)
                ->orWhere('last_name',\request()->fullname)
                ->orWhereRaw("concat(first_name, ' ', last_name) like '%{$request->fullname}%' ");
        }):'';

        \request()->code? $banks->whereHas('user', function ($q){
            $q->where('code',\request()->code);
        }):'';
        \request()->name? $banks->where('name','like',"%{$request->name}%"):'';
        \request()->card_number? $banks->where('card_number','like',"%{$request->card_number}%"):'';
        \request()->account_number? $banks->where('account_number','like',"%{$request->account_number}%"):'';
        \request()->sheba_number? $banks->where('sheba_number','like',"%{$request->sheba_number}%"):'';
        in_array(\request()->status,['0',1,2,3])? $banks->where('status',$request->status):'';
        $date_from ? $banks->where('created_at', '>=', $date_from) : '';
        $date_to ? $banks->where('created_at', '<=', $date_to) : '';
        $banks= $banks->paginate(10);

        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export($banks)), 'banks.xlsx');
            else {
                $data = $this->export($banks);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('users.pdf');
            }
        }

        return showData(view('admin.banks.index',compact('banks')));
    }

    public function reject(Request $request,$bank_id)
    {
        $request->validate([
            'reject_reason'=>'required'
        ]);
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 3, 'reject_reason'=>$request->reject_reason]);
            $bank=Bank::findOrFail($bank_id);
            $notif_array=[
                'type' => 'bank',
                'refer_id' => $bank_id,
                'user_id' =>$bank->user_id,
                'details' => [
                    'status' => 0,
                    'card_number' => $bank->card_number
                ]
            ];
            insert_notif($notif_array);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر رد صلاحیت شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }

    public function confirm($bank_id)
    {
        try {
            Bank::query()
                ->where('id',$bank_id)
                ->update(['status' => 1, 'reject_reason'=> '']);
            $bank=Bank::findOrFail($bank_id);
            $notif_array=[
                'type' => 'bank',
                'refer_id' => $bank_id,
                'user_id' =>$bank->user_id,
                'details' => [
                    'status' => 1,
                    'card_number' => $bank->card_number
                ]
            ];
            insert_notif($notif_array);
            return response()->json(['status'=>100,'msg'=>'حساب مورد نظر تایید شد']);
        }catch (\Exception $exception) {
            return response()->json(['status'=>500,'msg'=>'مشکلی پیش آمده است!']);
        }
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            switch ($item->status){
                case 0:
                    $status='جدید';
                    break;
                case 1:
                    $status='تایید شده';
                    break;
                case 2:
                    $status='ویرایش شده';
                    break;
                case 3:
                    $status='رد شده';
                    break;
            }
            array_push($result, [
                'نام' => $item->user->fullname,
                'کد کاربر' => $item->user->code,
                'بانک' => $item->name,
                'شماره حساب' => $item->account_number,
                'شماره کارت' => $item->card_number,
                'شماره شبا' => $item->sheba_number,
                'وضعیت' => $status,
            ]);
        }
        return $result;
    }
}
