<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    private function permissionsList()
    {
        $array = [
            [
                'name' => 'dashboard_menu',
                'label' => 'منوی داشبورد',
            ],
            [
                'name' => 'wallet_menu',
                'label' => 'منوی کیف پول کاربر',
                'childs' => [
                    [
                        'name' => 'rial_deposit',
                        'label' => 'دکمه واریز ریالی',
                        'childs' => [
                            [
                                'name' => 'rial_deposit_btn',
                                'label' => ' واریز ریالی',
                            ],
                        ],
                    ],
                    [
                        'name' => 'rial_withdraw',
                        'label' => 'دکمه برداشت ریالی',
                        'childs' => [
                            [
                                'name' => 'rial_withdraw_btn',
                                'label' => ' برداشت ریالی',
                            ],
                        ],
                    ],
                    [
                        'name' => 'crypto_deposit',
                        'label' => 'دکمه واریز کریپتو',
                        'childs' => [
                            [
                                'name' => 'crypto_deposit_btn',
                                'label' => ' واریز کریپتو',
                            ],
                        ],
                    ],
                    [
                        'name' => 'crypto_withdraw',
                        'label' => 'دکمه برداشت کریپتو',
                        'childs' => [
                            [
                                'name' => 'crypto_withdraw_btn',
                                'label' => ' برداشت کریپتو',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'banks_menu',
                'label' => 'منوی بانک ها',
                'childs' => [
                    [
                        'name' => 'banks_list_menu',
                        'label' => 'لیست بانک ها',
                        'childs' => [
                            [
                                'name' => 'bank_accept_btn',
                                'label' => 'تایید حساب بانکی کاربر',
                            ],
                            [
                                'name' => 'bank_reject_btn',
                                'label' => 'رد کردن حساب بانکی کاربر',
                            ],
                        ],
                    ],
                    [
                        'name' => 'user_bank_menu',
                        'label' => 'منوی حساب بانکی',
                        'childs' => [
                            [
                                'name' => 'bank_create',
                                'label' => 'ایجاد کردن بانک',
                            ],
                            [
                                'name' => 'bank_edit',
                                'label' => 'ویرایش کردن بانک',
                            ],
                            [
                                'name' => 'bank_accept',
                                'label' => 'تایید کردن بانک',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'swap_menu',
                'label' => 'منوی تبادل',
                'childs' => [
                    [
                        'name' => 'swap_store',
                        'label' => 'انجام تبادل',
                    ],
                ],
            ],
            [
                'name' => 'trade_menu',
                'label' => 'منوی خرید فروش',
                'childs' => [
                    [
                        'name' => 'rial_trade_buy',
                        'label' => 'دکمه خرید ریالی',
                    ],
                    [
                        'name' => 'rial_trade_sell',
                        'label' => 'دکمه فروش ریالی',
                    ],
                    [
                        'name' => 'usdt_trade_buy',
                        'label' => 'دکمه خرید تتری',
                    ],
                    [
                        'name' => 'usdt_trade_sell',
                        'label' => 'دکمه فروش تتری',
                    ],
                    [
                        'name' => 'rial_list',
                        'label' => 'منوی قیمت ارزها به ریال',
                    ],
                    [
                        'name' => 'usdt_list',
                        'label' => 'منوی قیمت ارزها به تتر',
                    ],
                ],
            ],
            [
                'name' => 'user_setting_menu',
                'label' => 'منوی تنظیمات کاربر',
                'childs' => [
                    [
                        'name' => 'authenticate_active',
                        'label' => 'دکمه فعال کردن گوگل اتنتیکتور',
                    ],
                    [
                        'name' => 'authenticate_deactive',
                        'label' => 'دکمه غیرفعال کردن گوگل اتنتیکتور',
                    ],
                ],
            ],
            [
                'name' => 'tickets_menu',
                'label' => 'منوی تیکت ها',
                'childs' => [
                    [
                        'name' => 'admin_new_tickets',
                        'label' => 'منوی تیکت های جدید (ادمین)',
                        'childs' => [
                            [
                                'name' => 'admin_show_ticket',
                                'label' => 'مشاهده تیکت(ادمین)',
                            ],
                            [
                                'name' => 'admin_close_ticket',
                                'label' => 'بستن تیکت(ادمین)',
                            ],
                            [
                                'name' => 'admin_new_ticket',
                                'label' => 'ایجاد تیکت(ادمین)',
                            ],
                        ],
                    ],
                    [
                        'name' => 'admin_my_tickets',
                        'label' => 'منوی تیکت های من',
                    ],
                    [
                        'name' => 'user_ticket_menu',
                        'label' => 'منوی تیکت',
                        'childs' => [
                            [
                                'name' => 'ticket_create',
                                'label' => 'ایجاد کردن تیکت',
                            ],
                            [
                                'name' => 'ticket_delete',
                                'label' => 'حذف کردن تیکت',
                            ],
                            [
                                'name' => 'ticket_show',
                                'label' => 'نمایش تیکت',
                            ],
                            [
                                'name' => 'ticket_close',
                                'label' => 'بستن تیکت',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'list_menu',
                'label' => 'منوی لیست',
                'childs' => [
                    [
                        'name' => 'user_transaction_menu',
                        'label' => 'منوی نمایش تراکنش ها',
                        'childs' => [
                        ],
                    ],
                    [
                        'name' => 'rial_transaction_menu',
                        'label' => 'منوی درخواست برداشت ریالی',
                    ],
                    [
                        'name' => 'crypto_transaction_menu',
                        'label' => 'منوی درخواست برداشت ارز',
                    ],
                ],
            ],
            [
                'name' => 'profile_btn',
                'label' => 'منوی پروفایل',
                'childs' => [
                    [
                        'name' => 'about_me',
                        'label' => 'نمایش باکس درباره من',
                    ],
                    [
                        'name' => 'change_password',
                        'label' => 'تغییر رمز عبور',
                    ],
                    [
                        'name' => 'authenticate_btn',
                        'label' => 'احراز هویت',
                    ],
                ],
            ],
            [
                'name' => 'users_management_menu',
                'label' => 'منوی کاربران',
                'childs' => [
                    [
                        'name' => 'users_index',
                        'label' => 'لیست کاربران',
                        'childs' => [
                            [
                                'name' => 'user_info',
                                'label' => 'اطلاعات شخصی',
                                'childs' => [
                                    [
                                        'name' => 'user_edit',
                                        'label' => 'ویرایش کاربر',
                                    ]
                                ],
                            ],
                            [
                                'name' => 'user_documents',
                                'label' => 'مدارک کاربر',
                            ],
                            [
                                'name' => 'user_roles',
                                'label' => 'نقش های کاربر',
                                'childs' => [
                                    [
                                        'name' => 'user_add_role',
                                        'label' => 'افزودن نقش',
                                    ],
                                ],
                            ],
                            [
                                'name' => 'user_setting',
                                'label' => 'تنظیمات کاربر',
                                'childs' => [
                                    [
                                        'name' => 'user_confirm_sms',
                                        'label' => 'روش تایید پیامک',
                                    ],
                                    [
                                        'name' => 'user_change_password',
                                        'label' => 'تغییر رمز عبور کاربر',
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'name' => 'users_authenticate',
                        'label' => 'احراز هویت',
                        'childs' => [
                            [
                                'name' => 'show_details',
                                'label' => 'مشاهده اطلاعات',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'withdraws_menu',
                'label' => 'برداشت ها',
                'childs' => [
                    [
                        'name' => 'rial_withdraw_index',
                        'label' => 'منوی لیست برداشت ریالی',
                        'childs' => [
                            [
                                'name' => 'rial_withdraw_reject',
                                'label' => 'دکمه رد کردن'
                            ],
                            [
                                'name' => 'rial_withdraw_accept',
                                'label' => 'دکمه تایید کردن'
                            ],
                        ]
                    ],
                    [
                        'name' => 'currency_withdraw_index',
                        'label' => 'منوی لیست برداشت ارزی',
                        'childs' => [
                            [
                                'name' => 'more_info',
                                'label' => 'دکمه اطلاعات تکمیلی',
                                'childs' => [
                                    [
                                        'name' => 'currency_withdraw_reject',
                                        'label' => 'دکمه رد درخواست برداشت'
                                    ],
                                    [
                                        'name' => 'currency_withdraw_accept',
                                        'label' => 'دکمه تایید درخواست برداشت'
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        'name' => 'withdraw_setting',
                        'label' => ' منوی تنظیمات برداشت',
                    ],
                ],
            ],
            [
                'name' => 'fees_management_menu',
                'label' => 'منوی مدیریت کارمزدها',
                'childs' => [
                    [
                        'name' => 'swap_fees',
                        'label' => 'مدیریت کارمزد سواپ ارزها',
                    ],
                    [
                        'name' => 'min_withdraw_fess',
                        'label' => 'مدیریت کارمزد و حداقل برداشت ارزها',
                    ],
                    [
                        'name' => 'transfer_currency_user_to_admin',
                        'label' => 'مدیریت کارمزد انتقال ارز از کاربر به ادمین',
                    ],
                    [
                        'name' => 'trade_fee',
                        'label' => 'مدیریت کارمزد ترید',
                    ],
                ],
            ],
            [
                'name' => 'reports_menu',
                'label' => 'منوی گزارشات',
                'childs' => [
                    [
                        'name' => 'transactions_menu',
                        'label' => 'لیست تراکنش ها',
                    ],
                ],
            ],
            [
                'name' => 'system_setting_menu',
                'label' => 'منوی تنظیمات سامانه',
                'childs' => [
                    [
                        'name' => 'notifications_setting',
                        'label' => 'ارسال اعلان ها',
                    ],
                    [
                        'name' => 'monetary_site',
                        'label' => 'واحد پول سامانه',
                    ],
                    [
                        'name' => 'swap_trade_setting',
                        'label' => 'تنظیمات فعال و غیر فعال کردن سواپ و ترید',
                    ],
                ],
            ],
//            [
//                'name' => 'blog',
//                'label' => 'مقاله',
//                'childs' => [
//                    [
//                        'name' => 'blog_create',
//                        'label' => 'ایجاد کردن',
//                    ],
//                    [
//                        'name' => 'blog_edit',
//                        'label' => 'ویرایش کردن',
//                    ],
//                    [
//                        'name' => 'blog_delete',
//                        'label' => 'حذف کردن',
//                    ],
//                    [
//                        'name' => 'blog_accept',
//                        'label' => 'تایید کردن',
//                    ],
//                    [
//                        'name' => 'blog_reject',
//                        'label' => 'رد کردن',
//                        'childs' => [
//                            [
//                                'name' => 'blog_acceptsa',
//                                'label' => 'تایید کردن',
//                            ],
//                            [
//                                'name' => 'blog_createe',
//                                'label' => 'زیر مجموعه مقاله1',
//                                'childs' => [
//                                    [
//                                        'name' => 'blog_createe2',
//                                        'label' => 'زیر مجموعه مقاله2',
//                                    ],
//                                ],
//                            ],
//                        ],
//                    ],
//                ],
//            ],
        ];
        $this->createPermissions($array, 0);
    }

    private function resetPermissionsAndRoles()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        foreach (config("permission.table_names") as $table) {
            DB::table($table)->truncate();
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return true;
    }

    private function createPermissions($array, $parent)
    {
        foreach ($array as $permission) {
            $perm = Permission::where("name", $permission["name"])->first();
            if (!$perm) {
                $perm = Permission::create(['name' => $permission['name'], 'label' => $permission['label'], 'guard_name' => "web", 'parent' => $parent]);
            }
            if (isset($permission["childs"])) {
                $this->createPermissions($permission["childs"], $perm->id);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $this->resetPermissionsAndRoles();
        $this->permissionsList();
        $roles = Role::orderBy('id', 'DESC')->paginate(10);
        return view('admin.roles.index', compact('roles'));
    }

    private function panelColorArray($lvl)
    {
        $array = [
            1 => "primary",
            2 => "success",
            3 => "info",
        ];

        return $array[$lvl];
    }

    private function buildPermissionsHtml($permission, $parent = 0, $lvl = 0, $rolePermissions = array())
    {
        $lvl++;
        $str = "";
        foreach ($permission as $value):
            if (isset($value['children'])) {
                $str .= '<div class="panel panel-' . $this->panelColorArray($lvl) . '">
                        <div class="panel-heading p-2 mb-2">
                            <h5 class="panel-title"> <label class="text-white"><input ' . ((in_array($value['id'],
                        $rolePermissions)) ? "checked" : "") . ' class="parent" type="checkbox" data-parent-id="' . $parent . '" data-id="' . $value['id'] . '" name="permission[]" value="' . $value['id'] . '">
                                    ' . $value['label'] . '</label></h5>
                        </div>
                        <div class="panel-body p-3 border-grey border mb-3">';
                if (isset($value["children"])) {
                    $str .= $this->buildPermissionsHtml($value["children"], $value['id'], $lvl, $rolePermissions);
                }
                $str .= '</div></div>';
            } else {
                $str .= '<label class="text-black pl-3"><input ' . ((in_array($value['id'],
                        $rolePermissions)) ? "checked" : "") . ' class="child" type="checkbox" data-parent-id="' . $parent . '" data-id="' . $value['id'] . '" name="permission[]" value="' . $value['id'] . '">
                                    ' . $value['label'] . '</label>';
            }
        endforeach;

        return $str;
    }

    private function buildTree(array $elements, $parentId = 0)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get()->toArray();
        $permission = $this->buildPermissionsHtml($this->buildTree($permission, 0));

        return view('admin.roles.form', compact('permission'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
        ]);

        DB::beginTransaction();
        try {
            if ($role = Role::create(['name' => $request->input('name'), 'guard_name' => "web"])) {
                $role->syncPermissions($request->input('permission'));
                DB::commit();

                return response()->json([
                    'status' => 100,
                    'timer' => 500,
                    'url' => route('roles.index'),
                    'msg' => trans('message.success'),
                ]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
            DB::rollBack();
        }

        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=",
            "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();


        return view('admin::roles.show', compact('role', 'rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get()->toArray();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck('role_has_permissions.permission_id', 'role_has_permissions.permission_id')
            ->all();
        $permission = $this->buildPermissionsHtml($this->buildTree($permission, 0), 0, 0, $rolePermissions);

        return view('admin.roles.form', compact('permission', 'role'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:roles,name,' . $id,
        ]);

        DB::beginTransaction();
        try {
            $role = Role::find($id);
            $role->name = request('name');
            $role->save();
            $role->syncPermissions($request->input('permission'));
            DB::commit();

            return response()->json([
                'status' => 100,
                'timer' => 500,
                'url' => route('roles.index'),
                'msg' => trans('message.success'),
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
        }
        return response()->json(['status' => 500, 'msg' => trans('message.fail')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $model = Role::find($request->id);
        if (!$model->users) {
            $model->delete();
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
        }
        return response()->json(['status' => 500, 'msg' => 'به دلیل اطلاعات وابسته امکان حذف وجود ندارد']);
    }
}
