<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Jobs\SendSms;
use App\Models\Asset;
use App\Models\AuthForm;
use App\Models\BaseData;
use App\Models\City;
use App\Models\Currency;
use App\Models\Finance_transaction;
use App\Models\User;
use App\Models\UserAuthForm;
use App\Rules\JalaliDataFormat;
use App\Rules\NationalCodeFormat;
use App\View\Components\ReportList;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use niklasravnsborg\LaravelPdf\Pdf;
use Spatie\Permission\Models\Role;
use function Symfony\Component\String\u;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:users_management_menu'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:users_authenticate'])->only('authenticate');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:user_info'])->only('show');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:show_details'])->only('form_authenticate');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:user_edit'])->only('edit','editInformation');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:user_change_password'])->only('editSetting');
    }

    public function index(Request $request)
    {
        $users = User::query();

        $start_created_at = \request()->start_created_at ? jalali_to_gregorian(request()->start_created_at . " 00:00:00") : false;
        $end_created_at = \request()->end_created_at ? jalali_to_gregorian(request()->end_created_at . " 23:59:59") : false;

        $request->LblName ? $users->where('first_name', 'like', '%' . $request->LblName . '%')->orWhere('last_name', 'like', '%' . $request->LblName . '%') : '';
//        $request->last_name ? $users->where('last_name', 'like', '%' . $request->last_name . '%') : '';
        $request->mobile ? $users->where('mobile', 'like', '%' . $request->mobile . '%') : '';
        $request->email ? $users->where('email', 'like', '%' . $request->email . '%') : '';
        $request->national_code ? $users->where('national_code', 'like', '%' . $request->national_code . '%') : '';
        $request->birth_day ? $users->where('birth_day', $request->birth_day) : '';
        $request->gender ? $users->where('gender_id', $request->gender) : '';
        $request->status ? $users->where('status', $request->status) : '';

        $start_created_at ? $users->where('created_at', '>=', $start_created_at) : '';
        $end_created_at ? $users->where('created_at', '<=', $end_created_at) : '';
        $request->code ? $users->where('code', $request->code) : '';
        if ($request->has('ordering') && !empty($request->ordering)) {
            $ordering = ['alphabet' => 'first_name', 'step' => 'step', 'created' => 'created_at'];
            $users->orderBy($ordering[$request->ordering], 'desc');
        } else {
            $users->orderBy('updated_at', 'desc');
        }


        $users = $users->paginate(10);
        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export_users($users)), 'users.xlsx');
            else {
                $data = $this->export_users($users);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('users.pdf');
            }
        }
        return showData(view('admin.manage.users.index', compact('users')));
    }

    public function form_authenticate($user)
    {
        $images = [];
        $roles = Role::all();
        $user = User::with(array('bank' => function ($q) {
            $q->orderBy('id', 'desc')->with('files');
        }))->findOrFail($user);
        $step_completed = UserAuthForm::select([
            '*',
            'type' => AuthForm::select('type')->whereColumn('user_auth_forms.auth_form_id', 'auth_forms.id')->limit(1)
        ])->where('user_id', $user->id)->orderBy('auth_form_id', 'asc')->get()->toArray();
        $step_forms = AuthForm::where('status', 1)->get()->toArray();
        $images['national'] = $user->files()->where('type', 'national')->latest()->first();
        $images['selfie_image'] = $user->files()->where('type', 'selfie_image')->latest()->first();
        return view('admin.manage.users.authenticate', compact('user', 'images', 'roles', 'step_forms', 'step_completed'));
    }

    public function show(User $user)
    {
        $result['roles'] = Role::all();
        $result['states'] = City::where('parent', 0)->get();
        $result['cities'] = !empty($user->city) ? City::where('parent', $user->state)->get() : [];
        $result['documents'] = $user->files()->whereIn('type', ['national', 'phone_bill'])->get();
        $result['assets'] = $user->assets;
        $result['user_level'] = $user->auth_forms()->userLevel()->first();
        !empty($result['user_level']) ? $result['user_level'] = $result['user_level']->toArray()['user_level'] : $result['user_level'] = 'نامشخص';
        $result['current_role'] = $user->roles()->get()->toArray();
        if (\request()->has('export_report') && !empty(\request()->export_report)) { //به دلیل این که تب گزارشات کاربر کامپوننت هست و نمیشه return حروجی pdf و excel گرفت با این ترفند خروجی های کامپوننت حل میشه
            $export = new ReportList($user);
            return $export->render();
        }
        return view('admin.manage.users.show', compact('user', 'result'));
    }

    public function change_status(): \Illuminate\Http\RedirectResponse
    {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $form_id = $_GET['form_id'];
        $form_auth = UserAuthForm::find($form_id);
        $form_auth->status = 2;
        $form_auth->save();
        $user = User::findOrFail($id);

//        $user->step_complate = $status;
        if ($status == 3) { // اطلاعات کاربر تایید شده
            SendSms::dispatch($user->mobile, 'سوم', 'step-confirmation');
        }
        if ($status == 4) { // بانک تایید شده
            $user->is_complete_steps = 1;
            $user->bank()->first()->update(['status' => 1]);
            SendSms::dispatch($user->mobile, 'چهارم', 'step-confirmation');
        }
        $notif_array = [
            'type' => 'authenticate',
            'user_id' => $user->id,
            'refer_id' => $form_auth->id,
            'details' => [
                'step' => $status,
                'status' => 1,
            ]
        ];
        insert_notif($notif_array);

        if (!empty($user->reject_reason)) {
            $user->reject_reason = null;
            $form_auth->reject_reason = null;
        }
        $user->save();
        $form_auth->save();

        return redirect()->back()->with('flash', ['status' => '100', 'msg' => 'این مرحله با موفقیت تایید شد', 'type' => 'success']);
    }

    public function disable_status()
    {
        $id = $_GET['id'];
        $status = $_GET['status'];
        $value = $_GET['value'];
        $form_id = $_GET['form_id'];
        $form_auth = UserAuthForm::find($form_id);
        $form_auth->status = 3;
        $form_auth->reject_reason = $value;
        $form_auth->save();
        $user = User::findOrFail($id);

//        $user->step = $status;
//        if ($user->step_complate >= $status) {
//            $user->step_complate = $status;
//        }
        $user->is_complete_steps = 0;
//        $user->reject_reason = $value;
        $notif_array = [
            'type' => 'authenticate',
            'user_id' => $user->id,
            'refer_id' => $form_auth->id,
            'details' => [
                'step' => $status + 1, // سجاد گفت به اضافه یک کنم
                'status' => 2,
                'reject_reason' => $value
            ]
        ];
        insert_notif($notif_array);

        $user->save();

        return response()->json(['status' => '100', 'msg' => 'لغو صلاحیت شما با موفقیت انجام شد', 'type' => 'success']);
    }

    // ویرایش اطلاعات کاربری
    public function edit(User $user, Request $request)
    {
        /*      $request->validate([
                  'birth_day' => [new JalaliDataFormat()],
                  'national_code' => [new NationalCodeFormat()],
                  'state' => ['nullable', 'exists:cities,id'],
                  'city' => ['nullable', 'exists:cities,id'],
                  'roles' => ['nullable', 'exists:roles,name'],
                  'gender_id' => ['nullable', 'in:1,2'],
                  'stats' => ['nullable', 'in:1,2'],
              ]);*/
//        try {
        if ($request->tab == 'information') {
            return $this->editInformation($user, $request);
        } elseif ($request->tab == 'documents') {

        } elseif ($request->tab == 'roles') {
            return $this->editRole($user, $request);
        } elseif ($request->tab == 'setting') {
            return $this->editSetting($user, $request);
        } elseif ($request->tab == 'wallet') {

        } elseif ($request->tab == 'reports') {

        }
//        } catch (\Exception $exception) {
//            dd($exception->getMessage());
//            return redirect()->back()->with('flash', ['status' => '500', 'msg' => 'خطایی پیش آمده', 'type' => 'danger'])->withInput(['tab' => $request->tab]);
//        }
    }

    // لاگین کردن با کاربر داخل پنل
    public function login_panel(User $user)
    {
        Auth::logout();
        Auth::login($user);
        return redirect('/dashboard');
    }

    public function export_users($data)
    {
        $result = [];
        foreach ($data as $item) {
            $level = $item->auth_forms()->userLevel()->first();
            !empty($level) ? $level = $level->toArray()['user_level'] : $level = 'نامشخص';
            array_push($result, [
                'کد کاربری' => $item->code,
                'نام , نام خانوادگی' => $item->fullname,
                'ایمیل' => $item->email,
                'کدملی' => $item->national_code,
                'سطح کاربر' => $level,
                'تاریخ عضویت' => jdate_from_gregorian($item->created_at),
            ]);
        }
        return $result;
    }

    public function export_auth($data)
    {
        $result = [];
        foreach ($data as $item) {
            array_push($result, [
                'کد کاربری' => $item->code,
                'نام و نام خانوادگی' => $item->full_name,
                'ایمیل' => $item->email,
                'کدملی' => $item->national_code,
                'سطح کاربر' => $item->step_complate,
                'تاریخ ثبت' => jdate_from_gregorian($item->created_at, '%Y.%m.%d'),
            ]);
        }
        return $result;
    }

    public function wallet(Request $request, $user_id)
    {
        $amount = (float)str_replace(',', '', $request['amount']);
        $request->validate([
            'unit' => 'required',
            'amount' => 'required|between:0,999999.0',
            'transact_type' => 'required',
        ]);

        try {
            $asset = Asset::where('user_id', $user_id)
                ->where('unit', $request->unit)->first();
            if ($amount > $asset->amount && $request['transact_type'] == 'decrease') {
                return response()->json(['status' => 500, 'msg' => 'مقدار کاهش بیشتر از موجودی کاربر می باشد.']);
            }
            if ($request['transact_type'] == 'increase') {
                $type = 2;
                $description = 'افزایش موجودی کیف پول توسط ادمین (' . $request['unit'] . ')';
                $base_data = BaseData::where('type', 'transactions')->where('extra_field1', 7)->first()->id;
                $amount = $asset->amount + unit_to_rial($amount, false, $request['unit']);
            } else {
                $type = 1;
                $description = 'کاهش موجودی توسط ادمین (' . $request['unit'] . ')';
                $base_data = BaseData::where('type', 'transactions')->where('extra_field1', 8)->first()->id;
                $amount = $asset->amount - unit_to_rial($amount, false, $request['unit']);
            }

            $asset->update([
                'amount' => $amount,
            ]);

            $asset->transaction()->create([
                'user_id' => $user_id,
                'transact_type' => $base_data,
                'amount' => $amount,
                'type' => $type,
                'description' => $description,
            ]);
            return redirect()->back()->with('flash', ['status' => '100', 'msg' => 'عملیات با موفقیت انجام شد.', 'type' => 'success'])->withInput(['tab' => $request->tab]);
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->with('flash', ['status' => '500', 'msg' => 'خطا در ثبت اطلاعات', 'type' => 'danger'])->withInput(['tab' => $request->tab]);
        }

    }

    private function editInformation(User $user, Request $request)
    {
        $request->validate([
            'birth_day' => ['nullable', new JalaliDataFormat()],
            'national_code' => ['nullable', new NationalCodeFormat()],
            'email' => ['nullable', 'email'],
            'gender_id' => ['nullable', 'in:1,2,0'],
            'stats' => ['nullable', 'in:1,2'],
        ]);
        try {
            $user->update($request->except(['id', 'code', 'google2fa_secret', 'email_verified_at', 'step', 'step_complate', 'is_complete_steps', 'panel_type', 'verification', 'time_verification', 'referral_id', 'reject_reason', 'remember_token', 'deleted_at', 'created_at', 'updated_at']));
            return redirect()->back()->with('flash', ['status' => '100', 'msg' => 'اطلاعات با موفقیت ثبت گردید', 'type' => 'success'])->withInput(['tab' => $request->tab]);

        } catch (Exception $e) {
            return redirect()->back()->with('flash', ['status' => '500', 'msg' => 'خطا در ثبت اطلاعات', 'type' => 'danger'])->withInput(['tab' => $request->tab]);
        }
    }

    private function editRole(User $user, Request $request)
    {
        $request->validate([
            'id' => 'required|exists:roles,id',
            'type' => 'required|in:add,delete'
        ]);
        try {

            if ($request->type == 'add') {

                $user->assignRole($request->id);
                $role_name = Role::where('id', $request->id)->value('name');
                return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت اعمال شد', 'id' => $request->id, 'name' => $role_name, 'date' => date('Y/m/d')]);
            }
            $user->removeRole($request->id);
            return response()->json(['status' => 100, 'msg' => 'تغییرات با موفقیت اعمال شد']);
        } catch (Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در انجام عملیات']);
        }
    }

    private function editSetting(User $user, Request $request)
    {
        try {
            $request->validate([
                'confirm_type' => 'nullable|in:0,1',
                'password' => 'nullable'
            ]);

            if ($request->has('confirm_type') && !empty($request->confirm_type)) {
                $user->confirm_type = 'sms';
            } else {
                $user->confirm_type = 'google-authenticator';
            }

            if ($request->has('password') && !empty($request->password)) {
                $user->password = Hash::make($request->password);
            }
            $user->save();
            return redirect()->back()->with('flash', ['status' => '100', 'msg' => 'این مرحله با موفقیت تایید شد', 'type' => 'success'])->withInput(['tab' => $request->tab]);
        } catch (Exception $e) {
            return redirect()->back()->with('flash', ['status' => '500', 'msg' => 'خطا در ثبت اطلاعات', 'type' => 'danger'])->withInput(['tab' => $request->tab]);
        }
    }

    private function editWallet($request)
    {

    }

    public function authenticate(Request $request)
    {
        $users = UserAuthForm::whereIn('status', [1, 3])->groupBy('user_id');

        if ($request->has('LblName') && !empty($request->LblName)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('first_name', 'like', '%' . $request->LblName . '%')->orWhere('last_name', 'like', '%' . $request->LblName . '%');
            });

        }
        if ($request->has('mobile') && !empty($request->mobile)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('mobile', $request->mobile);
            });
        }

        if ($request->has('email') && !empty($request->email)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('email', $request->email);
            });
        }

        if ($request->has('national_code') && !empty($request->national_code)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('national_code', $request->national_code);
            });
        }

        if ($request->has('birth_day') && !empty($request->birth_day)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('birth_day', $request->birth_day);
            });
        }

        if ($request->has('gender') && !empty($request->gender)) {
            $users->whereHas('user', function ($q) use ($request) {
                $q->where('gender_id', $request->gender);
            });
        }

        if ($request->has('status') && !empty($request->status)) {
            $users->where('status', $request->status);
        }

        if ($request->has('ordering') && !empty($request->ordering)) {
            $ordering = ['alphabet' => 'first_name', 'step' => 'step', 'created' => 'created_at'];
            $users->orderBy($ordering[$request->ordering], 'desc');
        } else {
            $users->orderBy('updated_at', 'desc');
        }


        $users = $users->paginate(10);

        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export_users($users)), 'users.xlsx');
            else {
                $data = $this->export_users($users);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('users.pdf');
            }
        }
        return showData(view('admin.manage.users.authenticate_index', compact('users')));
    }
}
