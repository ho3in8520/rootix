<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Currency;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class  CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:withdraw_setting'])->only('form');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currencies = Currency::query();

        if (isset($request) && $request->has('full_name'))
            $currencies->where('full_name', 'like', "%{$request->full_name}%");
        if (isset($request) && $request->has('exclusive_name'))
            $currencies->where('exclusive_name', 'like', "%{$request->exclusive_name}%");
        if (isset($request) && $request->has('status'))
            $currencies->where('status', intval($request->status));

        $currencies = $currencies->paginate(1);
        return showData(view('admin.currency.index', compact('currencies')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        try {
        $request->validate([
            'full_name' => 'required',
            'exclusive_name' => 'required',
            'price' => 'required',
            'image' => 'required',
        ]);
        $request['price'] = intval(str_replace(',', '', $request->price));
        $request['status'] = (isset($request['status']) || $request['status'] == 'on') ? 1 : 0;
        $request['add_to_asset'] = (isset($request['add_to_asset']) || $request['add_to_asset'] == 'on') ? 1 : 0;

        if (($request['add_to_asset'] == 1)) {
            $users = User::get();
            foreach ($users as $user) {
                $asset = new Asset();
                $asset->user_id = $user->id;
                $asset->unit = $request['full_name'];
                $asset->amount = 0;
                $asset->logo = $request['full_name'];
                $asset->save();
            }
        }

        $currency = Currency::create($request->only(['full_name', 'exclusive_name', 'status', 'price', 'add_to_asset']));
        $path = upload($request->image, 'currencies');
        $currency->files()->create([
            'user_id' => auth()->user()->id,
            'path' => $path,
            'type' => 'currencies',
        ]);
        return response()->json(['status' => 100, 'msg' => 'عملیات موفقیت آمیز بود', 'url' => route('currencies.index')]);
//        } catch (\Exception $exception) {
//            return response()->json(['status' => 500, 'msg' => $exception->getMessage()]);
//            return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود آمده است']);
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        return view('admin.currency.create', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        try {
            $request->validate([
                'full_name' => 'required',
                'exclusive_name' => 'required',
                'price' => 'required|between:0,99,999',
            ]);
            $request['price'] = intval(str_replace(',', '', $request->price));
            $request['status'] = (isset($request['status']) || $request['status'] == 'on') ? 1 : 0;
            $request['add_to_asset'] = (isset($request['add_to_asset']) || $request['add_to_asset'] == 'on') ? 1 : 0;

            if (($request['add_to_asset'] == 1)) {
                $users = User::get();
                foreach ($users as $user) {
                    $asset = Asset::where('user_id', $user->id)->where('unit', $request['full_name'])->first();
                    if (isset($asset)) {
                        $asset->unit = $request['full_name'];
                        $asset->logo = $request['full_name'];
                        $asset->save();
                    } elseif (!isset($asset)) {
                        $new_asset = new Asset();
                        $new_asset->user_id = $user->id;
                        $asset->unit = $request['full_name'];
                        $asset->amount = 0;
                        $asset->logo = $request['full_name'];
                        $asset->save();
                    }

                }
            }

            if (($request['add_to_asset'] == 0)) {
                $users = User::get();
                foreach ($users as $user) {
                    $assets = Asset::where('unit', $request['full_name'])->where('amount', '!=', 0)->get();

                    if (count($assets) != 0) {
                        return response()->json(['status' => 500, 'msg' => 'نمیتوان این ارز را حذف کرد لطفا دارایی های کاربران را تصفیه کنید.']);
                    } else {
                        Asset::where('unit', $request['full_name'])->where('user_id', $user->id)->delete();
                    }
                }
            }

            $currency->update($request->only(['full_name', 'exclusive_name', 'status', 'price', 'add_to_asset']));
            if ($request->has('image') && $request->image) {
                $currency->files()->delete();
                $path = upload($request->image, 'currencies');
                $currency->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => $path,
                    'type' => 'currencies',
                ]);
            }
            return response()->json(['status' => 100, 'msg' => 'عملیات موفقیت آمیز بود']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود آمده است']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        return response()->json(['status' => 500, 'msg' => 'در حال حاضر این امکان در دسترس نمی باشد']);
    }

    public function form()
    {
        $currencies = Currency::all();
        foreach ($currencies as $currency) {
            $unit=strtoupper($currency['unit']);
            $item = get_specific_currencies($unit);
            $color = $item[$unit]['color'] ?? '#e3ebfc';
            $logo_url = $item[$unit]['logo_url'] ?? '';
            $currency->setAttribute('color' , $color);
            $currency->setAttribute('logo_url' , $logo_url);
        }
        return view('admin.wd_currency_setting', compact('currencies'));
    }

    public function wd_currency_status(Request $request)
    {
//        dd($request['type'],$request['status']);
        try {
            $currency=Currency::findOrFail($request['currency_id']);
            $currency->update([$request['type']=>$request['status']]);
            return response()->json(['status'=>100,'msg'=>'عملیات با موفقیت انجام شد','refresh'=>true]);
        }catch (\Exception $exception){
            dd($exception);
            return response()->json(['status'=>500,'msg'=>'مشکلی رخ داده است. لطفا بعدا امتحان کنید.']);
        }

    }
}
