<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\UploadedFile;
use App\Models\User;
use App\Models\UserGuard;
use Carbon\Carbon;
//use hisorange\BrowserDetect\Exceptions\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = PostCategory::all();

        $posts = Post::select([
            '*',
            'category' => PostCategory::select([DB::raw("name")])->whereColumn('posts.category_id', 'posts_category.id')->limit(1),
        ]);

        $request->title && $request->has('title') && !empty($request->title) ? $posts->where('title', 'LIKE', '%' . $request->title . '%') : '';
        $request->category_id && $request->has('category_id') && !empty($request->category_id) ? $posts->where('category_id', $request->category_id) : '';
        $request->creator && $request->has('creator') && !empty($request->creator) ? $posts->guardFilters() : '';
        $type = $request->type == 'amozesh' ? 0 : 1;
        $posts->where('type',$type);
        $posts->orderBy('status', 'asc');
        $posts = $posts->paginate(10);
        return showData(view('blog.index', compact('posts', 'categories')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = PostCategory::all();
        return view('blog.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'created_at' => 'required_if:receiver,1',
            'file' => 'required|mimes:jpeg,png,jpg|max:5048',
        ]);
        $type = \request()->type == 'amozesh' ? 0 : 1;

        if ($request['receiver'] == 0)
            $time = Carbon::now();
        else
            $time = $request['created_at'];

        $request['tags'] && !empty($request['tags']) ? $tags = $request['tags'] : $tags = "";
        $tags = explode(',', $tags);
        try {
//            dd($tags,json_encode($tags));
            $post = Post::create([
                'title' => $request['title'],
                'description' => $request['description'],
                'tags' => json_encode($tags),
                'status' => 0,
                'reject_reason' => '',
                'creator_id' => getCurrentUser()->user_guard_id,
                'category_id' => $request['category_id'],
                'type' => $type,
                'created_at' => $time,
            ]);
//            $image = $request->file('image');
//            $img = Image::make($image->path());
            $mime_type = $request->file->extension();
            $size = $request->file->getSize();
            $path = \Illuminate\Support\Facades\Storage::disk('uploaded')->put('/blog/' . $post->id, $request->file('file'));
//            $img->resize(920, 460, function ($constraint) {
//                $constraint->aspectRatio();
//            })->save($path);


            UploadedFile::create([
                'user_id' => getCurrentUser()->user_guard_id,
                'uploadable_type' => 'App\Models\Post',
                'uploadable_id' => $post->id,
                'mime_type' => $mime_type,
                'size' => $size,
                'path' => $path,
                'thumbnail_path' => '',
                'type' => 'blog_pic',
                'status' => 0,
                'state' => 1,
                'description' => '',
            ]);
            return response()->json(['status' => 100, 'msg' => 'پست با موفقیت ایجاد شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = PostCategory::all();
        return view('blog.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'created_at' => 'required_if:receiver,1',
        ]);
        $type = \request()->type == 'amozesh' ? 0 : 1;
        if ($request['receiver'] == 0)
            $time = Carbon::now();
        else
            $time = $request['created_at'];

        $request['tags'] && !empty($request['tags']) ? $tags = $request['tags'] : $tags = "";
        $tags = explode(',', $tags);
        try {
            $post = Post::findOrFail($id);

            $post->update([
                'title' => $request['title'],
                'description' => $request['description'],
                'tags' => $tags,
                'status' => 3,
                'reject_reason' => '',
                'category_id' => $request['category_id'],
                'type' => $type,
                'created_at' => $time,
            ]);
            return response()->json(['status' => 100, 'msg' => 'پست با موفقیت ویرایش شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reject(Request $request)
    {
        try {
            $post = Post::findOrFail($request->post_id);
            $post->update([
                'reject_reason' => $request['reject_reason'],
                'status' => 3
            ]);
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی رخ داده است. لطفا بعدا امتحان کنید.']);
        }


    }

    public function accept(Request $request)
    {
        try {
            $post = Post::findOrFail($request->post_id);
            $post->update([
                'status' => 1
            ]);
            return response()->json(['status' => 100, 'msg' => 'پست مورد نظر با موفقیت تایید شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی رخ داده است. لطفا بعدا امتحان کنید.']);
        }


    }
}
