<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Withdraw;
//use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class WDController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:currency_withdraw_index'])->only('index_withdraw');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:more_info'])->only('show_withdraw');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:user_info'])->only('show');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:currency_withdraw_reject|currency_withdraw_accept'])->only('update_status');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_withdraw(Request $request)
    {
        $withdraws = Withdraw::with('user');
        if ($request->has('code') && !empty($request->code)) {
            $withdraws->whereHas('user', function ($q) use ($request) {
                $q->where('code', $request->code);
            });
        }
        if ($request->has('email') && !empty($request->email)) {
            $withdraws->whereHas('user', function ($q) use ($request) {
                $q->where('email', $request->email);
            });
        }
        if ($request->has('amount') && !empty($request->amount)) {
            $withdraws->where('amount', $request->amount);
        }
        if ($request->has('status') && !empty($request->status)) {
            $withdraws->where('status', $request->status);
//            $array = [1 => [2], 2 => [3, 4], 3 => [0, 1]];
//            $withdraws->whereIn('status', $array[$request->status]);
        }
        if ($request->has('date_from') && !empty($request->date_from)) {
            $withdraws->where('created_at', '>=', jalali_to_gregorian($request->date_from . " 00:00:00"));
        }
        if ($request->has('date_to') && !empty($request->date_to)) {
            $withdraws->where('created_at', '<=', jalali_to_gregorian($request->date_to . " 00:00:00", '/', false));
        }
        $withdraws = $withdraws->orderBy('status', 'desc')->paginate(10);

        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export($withdraws)), 'withdraws.xlsx');
            else {
                $data = $this->export($withdraws);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('withdraws.pdf');
            }
        }

        return showData(view('admin.wallet.withdraw.index', compact('withdraws')));
    }

    public function index_deposit()
    {
        return view('admin.wallet.deposit.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show_withdraw($id)
    {
        $result = Withdraw::with('user')->where('id', $id)->firstOrFail();
        return view('admin.wallet.withdraw.show', compact('result'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function balance_admin(Request $request)
    {
        $result = coinex()->get_balances_coin($request->unit);
        return response()->json(['status' => 100, 'amount' => $result]);
    }

    public function update_status(Request $request)
    {
        $request->validate([
            'key' => 'required|exists:withdraws,id',
            'mode' => 'required|in:1,2'
        ]);
        $result = Withdraw::find($request->key);
        $base_data = BaseData::select([
            DB::raw("id as transact_type"),
            'min_withdraw' => BaseData::select('extra_field1')->where('type', 'min_withdraw_' . $result->unit)->limit(1),
            'fee_withdraw' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_' . $result->unit)->limit(1),
        ])->where('type', 'transactions')->where('extra_field1', 3)->first();
//        $base_data = BaseData::where('type', 'transactions')->where('extra_field1', 3)->value('id');
        $amount = $result->amount - $result->fee;
        DB::beginTransaction();
        try {
            if ($request->mode == 1) {
                $asset = Asset::where('user_id', $result->user_id)->where('unit', $result->unit)->first();
                if ($asset->amount < $result->amount) {
                    return response()->json(['status' => 500, 'msg' => 'موجودی کاربر کمتر از مبلغ درخواستی است']);
                }
                $withdrawal = coinex()->withdrawal($result->unit, $result->address, $amount);
                $withdrawal = json_decode($withdrawal, true);
                if ($withdrawal['code'] == 107) {
                    return response()->json(['status' => 500, 'msg' => 'موجودی ادمین کم است']);
                }
                if ($withdrawal['code'] == 310) {
                    return response()->json(['status' => 500, 'msg' => 'آدرس کاربر در آدرس لیست کوینکس ادد نشده است']);
                }
                if ($withdrawal['code'] == 104) {
                    return response()->json(['status' => 500, 'msg' => 'مبلغ کمتر از حداقل برداشت است']);
                }
                if ($withdrawal['code'] != 0) {
                    return response()->json(['status' => 500, 'msg' => 'خطا در ارتباط با شبکه بلاکچین']);
                }
                $result->status = 1;
                $result->save();
                // کاهش موجودی ولت کاربر
                $asset->amount -= $result->amount;
                $asset->save();
                // لاگ تراکنش
                $asset->transaction()->create([
                    'tracking_code' => (isset($withdrawal['tx_id']) && !empty($withdrawal['tx_id']))?$withdrawal['tx_id']:null,
                    'user_id' => $result->user_id,
                    'transact_type' => $base_data->aid,
                    'amount' => $amount,
                    'type' => 1,
                    'extra_field1' => $result->address,
                    'description' => "برداشت ارز {$result->unit} به مقدار {$amount}",
                ]);

                insert_notif([
                    'refer_id' => $result->id,
                    'user_id' => $result->user_id,
                    'type' => 'withdraw',
                    'details' => ['status' => 1, 'type' => 'crypto']
                ]);
                DB::commit();
                return response()->json(['status' => 100, 'msg' => 'عملیات انتقال با موفقیت انجام گردید']);

            } else {
                $reject = $request->has('reject_reason') && !empty($request->reject_reason) ? $request->reject_reason : null;
                $result->status = 3;
                $result->reject_reason = $reject;
                $result->save();
                insert_notif([
                    'refer_id' => $result->id,
                    'user_id' => $result->user_id,
                    'type' => 'withdraw',
                    'details' => ['status' => 0, 'type' => 'crypto']
                ]);
            }
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'url' => route('admin.request.withdraw')]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['status' => 500, 'msg' => 'عملیات انجام نشد']);
        }
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            array_push($result, [
                'کد کاربری' => $item->user->code,
                'تاریخ درخواست' => jdate_from_gregorian($item->created_at,'Y/m/d H:i'),
                'ایمیل' => $item->user->email,
                'ارز' => $item->unit,
                'مبلغ' => $item->amount,
                'کارمزد' => $item->fee,
                'وضعیت' => $item->statusAdmin('text'),
            ]);
        }
        return $result;
    }
}
