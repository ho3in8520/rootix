<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserGuard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::orderBy('id', 'desc')->paginate(5);
        return view('admin.notifications.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        return view('admin.notifications.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'color' => 'required',
            'icon' => 'required',
            'description' => 'required',
            'user_id' => 'required_if:receiver,1',
        ]);

        $started_at = $request['started_at'] && !empty($request['started_at']) ? $started_at = Carbon::parse($request['started_at']) : $started_at = Carbon::now();
        if ($request['user_id'] != 0)
            $user = UserGuard::where('guardable_id', $request['user_id'])->where('guardable_type', 'App\Models\User')->first();
        else
            $user=0;
        try {
            $notif_array=[
                'type' => 'admin_user',
                'refer_id' => null,
                'user_id' => $user ? $user->id : $user,
                'details' => $request['description']
            ];
            insert_notif($notif_array);

            return response()->json(['status' => 100, 'msg' => 'پیغام با موفقیت ذخیره شدند']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = Notification::select([
            '*',
            'user' => User::select([DB::raw("concat(email,' ','(',code,')') as email")])->whereColumn('Notifications.user_id', 'users.id')->limit(1)
        ])->findOrFail($id);
        return view('admin.notifications.edit', compact('notification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'color' => 'required',
            'icon' => 'required',
            'description' => 'required',
            'user_id' => 'required_if:receiver,1',
        ]);

        $started_at = $request['started_at'] && !empty($request['started_at']) ? $started_at = Carbon::parse($request['started_at']) : $started_at = Carbon::now();
        try {
            $notification = Notification::query()->findOrFail($id);
            $notification->update([
                'user_id' => ($request['user_id']) ? $request['user_id'] : 0,
                'title' => $request['title'],
                'description' => $request['description'],
                'color' => $request['color'],
                'icon' => $request['icon'],
                'started_at' => $started_at,
            ]);

            return response()->json(['status' => 100, 'msg' => 'پیغام با موفقیت ویرایش شد.']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است.لطفا بعدا امتحان کنید.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function users(Request $request)
    {
        try {
            $users = User::select(['id', DB::raw("concat(email,' ','(',code,')') as email")])
                ->where('code', 'LIKE', '%' . $request['term'] . '%')
                ->orwhere('email', 'LIKE', '%' . $request['term'] . '%')
                ->get()->toArray();

            return response()->json($users);
        } catch (\Exception $exception) {
            return response()->json($exception);
        }


    }
}
