<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\WithdrawRial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class WithdrawRialController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:rial_withdraw_index'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:rial_withdraw_reject'])->only('reject');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:rial_withdraw_accept'])->only('confirm');
    }
    /**
     * لیست درخواست های برداشت کاربران
     */
    public function index(Request $request)
    {
        $start_deposit_date = \request()->start_deposit_date ? jalali_to_gregorian(\request()->start_deposit_date . " 00:00:00") : false;
        $end_deposit_date = \request()->end_deposit_date ? jalali_to_gregorian(\request()->end_deposit_date . " 23:59:59") : false;


        $withdraw_rial = WithdrawRial::query();
        \request()->user_code ? $withdraw_rial->whereHas('user', function ($q) {
            return $q->where('code', \request()->user_code);
        }) : '';

        \request()->last_name ? $withdraw_rial->whereHas('user', function ($q) {
            return $q->where('last_name', \request()->last_name);
        }) : '';

        \request()->start_amount ? $withdraw_rial->where('amount','>=', \request()->start_amount) : '';
        \request()->end_amount ? $withdraw_rial->where('amount','<=', \request()->end_amount) : '';
        in_array(\request()->status, ['0', '1', '2']) ? $withdraw_rial->where('status', \request()->status) : '';
        \request()->tracking_code ? $withdraw_rial->where('tracking_code', \request()->tracking_code) : '';
        in_array(\request()->deposit_type, ['0', '1', '2']) ? $withdraw_rial->where('deposit_type', \request()->deposit_type) : '';

        $start_deposit_date ? $withdraw_rial->where('deposit_date', '>=', $start_deposit_date) : '';
        $end_deposit_date ? $withdraw_rial->where('deposit_date', '<=', $end_deposit_date) : '';

        $withdraw_rial = $withdraw_rial->orderBy('created_at', 'desc')->paginate(5);
        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export($withdraw_rial)), 'withdraw_rial.xlsx');
            else {
                $data = $this->export($withdraw_rial);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('withdraw_rial.pdf');
            }
        }

        return showData(view('admin.withdraw_rial.index', compact('withdraw_rial')));
    }

    /**
     * رد درخواست برداشت کاربر
     */
    public function reject(Request $request, $id)
    {
        $request->validate([
            'admin_des' => 'required'
        ]);
        try {
            DB::beginTransaction();
            $withdraw_rial= WithdrawRial::query()
                ->where('id', $id)
                ->where('status', 0)
                ->first();
            $withdraw_rial->update([
                'admin_id' => auth()->user()->id,
                'status' => 2,
                'admin_des' => $request->admin_des,
            ]);
            $amount= $withdraw_rial->amount;
            $rial_asset= Asset::query()->where('user_id',$withdraw_rial->user_id)->where('unit','rls')->first();
            $rial_asset->update(['amount' => DB::raw("amount + $amount")]);
            $transact_type= BaseData::query()->where('type','transactions')->where('extra_field1',6)->first()->id;
            $rial_asset->transaction()->create([
                'user_id' => $rial_asset->user_id,
                'refer_id' => $withdraw_rial->id,
                'transact_type' => $transact_type,
                'amount' => $amount,
                'type' => 2,
                'description' => "بازگشت وجه به خاطر کنسل کردن درخواست برداشت ریالی",
            ]);
            DB::commit();
            return response()->json(['status' => 100, 'msg' => 'درخواست مورد نظر با موفقیت رد شد']);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود امده است لطفا با مدیریت در تماس باشید']);
        }
    }

    /**
     * تایید درخواست برداشت کاربر
     */
    public function confirm(Request $request, $id)
    {
        $request->validate([
            'admin_des' => 'required',
            'tracking_code' => 'required',
            'deposit_type' => 'required|in:0,1,2'
        ]);
        try {
            WithdrawRial::query()
                ->where('id', $id)
                ->where('status', 0)
                ->update([
                    'admin_id' => auth()->user()->id,
                    'status' => 1,
                    'tracking_code' => $request->tracking_code,
                    'deposit_type' => $request->deposit_type,
                    'deposit_date' => now(),
                    'admin_des' => $request->admin_des,
                ]);
            return response()->json(['status' => 100, 'msg' => 'درخواست مورد نظر با موفقیت تایید شد']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود امده است لطفا با مدیریت در تماس باشید']);
        }
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            array_push($result, [
                'نام ونام خانوادگی' => $item->user->full_name,
                'ایمیل' => $item->user->email,
                'کد کاربر' => $item->user->code,
                'کد پیگیری' => $item->tracking_code?$item->tracking_code:'-',
                'مبلغ' => number_format($item->amount).' ریال',
                'کارمزد' => number_format($item->fee).' ریال',
                'مبلغ قابل پرداخت' => number_format($item->amount-$item->fee).' ریال',
                'وضعیت' => $item->status_info['text'],
//                'تاریخ' => jdate_from_gregorian($item->created_at,'Y.m.d H:i'),
            ]);
        }
        return $result;
    }

}
