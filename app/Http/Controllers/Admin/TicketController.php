<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TotalExport;
use App\Http\Controllers\Controller;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
use App\Models\TicketRoute;
use App\Models\UploadedFile;
use App\Models\User;
//use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:admin_new_tickets|admin_my_tickets'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:admin_show_ticket'])->only('show');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:admin_close_ticket'])->only('ban');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:admin_new_ticket'])->only('create','store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        $tickets = TicketMaster::query();

        $date_from = \request()->date_from ? jalali_to_gregorian(request()->date_from . " 00:00:00") : false;
        $date_to = \request()->date_to ? jalali_to_gregorian(request()->date_to . " 23:59:59") : false;

        $request->ticket_code && !empty($request->ticket_code) ? $tickets->where('ticket_code', $request->ticket_code) : '';

        if ($request->email && !empty($request->email)) {
            $user = User::where('email', 'like', $request->email)->get();
            $ticketdetial = TicketDetail::whereIn('user_id', $user->pluck('id'))->distinct()->get('master_id');
            $tickets->whereIn('id', $ticketdetial->pluck('master_id'));

        }
        if ($request->code && !empty($request->code)) {
            $user = User::where('code', 'like', $request->code)->get();
            $ticketdetial = TicketDetail::whereIn('user_id', $user->pluck('id'))->distinct()->get('master_id');
            $tickets->whereIn('id', $ticketdetial->pluck('master_id'));
        }
        if (isset($request) && !empty($request->title)) {
            $tickets->where('title', 'like', "%{$request->title}%");
        }
        if (isset($request) && !empty($request->status)) {
            $tickets->where('status', intval($request->status));
        }
        if (isset($request) && !empty($request->priority)) {
            $tickets->where('priority', intval($request->priority));
        }
        if (isset($request) && !empty($request->file)) {
            $ticketdetials = TicketDetail::get();
            $upload = UploadedFile::whereIn('uploadable_id', $ticketdetials->pluck('id'))->where('uploadable_type', 'App\Models\TicketDetail')->first();
            $ticketdetial = TicketDetail::whereIn('id', $upload->pluck('uploadable_id'))->distinct()->get('master_id');
            if ($request->file == 1){
                $tickets = $tickets->whereIn('id', $ticketdetial->pluck('master_id'));

            }else{
                $tickets = $tickets->whereNotIn('id', $ticketdetial->pluck('master_id'));

            }
        }
        $date_from ? $tickets->where('created_at', '>=', $date_from) : '';
        $date_to ? $tickets->where('created_at', '<=', $date_to) : '';

        if ($request->has('type') &&  $request->type == 'my_tickets')
            $tickets = $tickets->where('accept_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);
        else
            $tickets = $tickets->where('accept_id', null)->orderBy('created_at', 'desc')->paginate(10);


        if ($request->has('export') && !empty($request->export)) {
            if ($request->export == 'excel')
                return Excel::download(new TotalExport($this->export($tickets)), 'tickets.xlsx');
            else {
                $data = $this->export($tickets);
                view()->share('data', $data);
                $pdf = \PDF::loadView('admin.pdf_report', $data);
                return $pdf->download('$tickets.pdf');
            }
        }
        return showData(view('admin.ticket.index', compact('tickets')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::all();
        return view('admin.ticket.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'user_id' => 'required',
            'priority' => ['required', Rule::in([1, 2, 3])],
            'description' => 'required|string',
            'file' => 'sometimes|mimes:jpeg,png,jpg,zip,pdf,rar|max:5120',
        ]);

        try {
            $roles=\App\Models\Role::query()->where('name','user')->first();
            $ticket_master = new TicketMaster();
            $ticket_master->user_id = $request['user_id'];
            $ticket_master->title = $request['title'];
            $ticket_master->priority = $request['priority'];
            $ticket_master->role_id = $roles->id;
            $ticket_master->accept_id = auth()->user()->id;
            $ticket_master->status = 1;
            $ticket_master->save();

            $ticket_detail = new TicketDetail();
            $ticket_detail->user_id = $request['user_id'];
            $ticket_detail->type = 2;
            $ticket_detail->master_id = $ticket_master->id;
            $ticket_detail->description = $request['description'];
            $ticket_detail->save();
            if ($request->has('file')) {
                $ticket_detail->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('file'), 'ticket'),
                    'type' => 'ticket'
                ]);
            }
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'url' => route('ticket.admin.show', $ticket_master->id)]);
        } catch (\Exception $e) {
            dd($e);
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = TicketMaster::where('id', $id)->firstOrFail();
        if (empty($ticket->accept_id)) {
            $ticket->accept_id = auth()->user()->id;
            $ticket->save();
        }
        $roles = Role::all();
        $role = Role::where('id', $ticket->role_id)->first();

        return view('admin.ticket.show', compact('ticket', 'roles', 'role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticketDetail = TicketDetail::where('id', $id)->first();
        if (!empty($ticketDetail->files[0]->path)) {
            $ticketDetail->files[0]->delete();
        }
        $ticketDetail->delete();

        return redirect()->back()->with('flash', ['type' => '200', 'msg' => 'با موفقیت انجام شد']);
    }

    public function comment(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|string',
            'file' => 'sometimes|mimes:jpeg,png,jpg,zip,pdf,rar|max:10240',
        ]);

        try {
            $ticket_master = TicketMaster::query()->findOrFail($id);
            $ticket_master->update(['status' => 2]);
            $ticket_detail = new TicketDetail();
            $ticket_detail->user_id = $ticket_master->user_id;
            $ticket_detail->type = 2;
            $ticket_detail->master_id = $id;
            $ticket_detail->description = $request['description'];
            $ticket_detail->save();
            if ($request->has('file')) {
                $ticket_detail->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('file'), 'ticket'),
                ]);
            }
            $notif_array = [
                'refer_id' => $id,
                'user_id' => $ticket_master->user_id,
                'type' => 'ticket',
                'details' => [
                    'subject' => $ticket_master->title,
                ]
            ];
            insert_notif($notif_array);

            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
        } catch (\Exception $exception) {
            dd($exception);
        }

    }

    public function route(Request $request, $id)
    {
        $ticket_route = new TicketRoute();
        $ticket_route->master_id = $id;
        $ticket_route->from_id = $request->input('from_id');
        $ticket_route->to_id = $request->input('to_id');
        if ($request->input('to_id') == 0) {
            $ticket_route->mode = 1;
        } else {
            $ticket_route->mode = 2;
        }
        $ticket_route->save();

        $ticket_mster = TicketMaster::where('id', $id)->first();
        $ticket_mster->role_id = $request->input('from_id');
        if ($request->input('to_id') == 0) {
            $ticket_mster->accept_id = null;
        } else {
            $ticket_mster->accept_id = $request->input('to_id');
        }
        $ticket_mster->save();

        return response()->json(['status' => '100', 'msg' => 'با موفقیت ارجاع داده شد.']);
    }

    public function ban($id)
    {
        $ticket_master = TicketMaster::where('id', $id)->first();
        if ($ticket_master->status != 4) {
            $ticket_master->status = 4;
            $ticket_master->save();
            return response()->json(['status' => 100, 'msg' => 'تیکت با موفقیت بسته شد.']);
        } else {
            return response()->json(['status' => 100, 'msg' => 'این تیکت  بسته شده است.']);
        }
    }

    public function export($data)
    {
        $result = [];
        foreach ($data as $item) {
            switch ($item->status) {
                case 1:
                    $status = 'بررسی نشده';
                    break;
                case 2:
                    $status = 'پاسخ داده شد';
                    break;
                case 3:
                    $status = 'ارجاع داده شد';
                    break;
                case 4:
                    $status = 'بسته شد';
                    break;
            }
            switch ($item->priority) {
                case 1:
                    $priority = 'کم';
                    break;
                case 2:
                    $priority = 'متوسط';
                    break;
                case 3:
                    $priority = 'زیاد';
                    break;
            }

            array_push($result, [
                'تیکت آیدی' => $item->ticket_code,
                'تاریخ' => $item->created_at,
                'ایمیل' => $item->user->first()->email,
                'عنوان' => $item->title,
                'فوریت' => $priority,
                'وضعیت' => $status,
            ]);
        }
        return $result;
    }

    public function users(Request $request)
    {
        $users = [];

        if($request->has('q')){
            $search = $request->q;
            $users =User::select("id", "email")
                ->where('email', 'LIKE', "%$search%")
                ->get();
        }
        return response()->json($users);
    }

}
