<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Currency;

//use Dompdf\Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use function Livewire\str;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:fees_management_menu'])->only('fees_index');
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:swap_fees|min_withdraw_fess|transfer_currency_user_to_admin|trade_fee'])->only('fees_store');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    // ویو صفحه ی ایندکس مدیریت کارمزد ها (کدها کپی شده از swap management , trade management قبلی)
    public function fees_index()
    {
        $units = Currency::select('unit')->get();
        $first_unit = $units->first()->unit;
//        $result = BaseData::where('type', 'manage_swap_rial')->orderBy('id', 'asc')->get();
        $result = BaseData::where('type', 'manage_swap_' . $first_unit)->orderBy('id', 'asc')->get();

        /*$min_withdraw = BaseData::where('type', 'min_withdraw_rial')->value('extra_field1');
        $fee_withdraw = BaseData::where('type', 'fee_withdraw_rial')->value('extra_field1');*/

        $min_withdraw = BaseData::where('type', 'min_withdraw_' . $first_unit)->value('extra_field1');
        $fee_withdraw = BaseData::where('type', 'fee_withdraw_' . $first_unit)->value('extra_field1');

        $fee_withdraw_admin = BaseData::select([
            'trc10' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc10')->limit(1),
            'trc20' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc20')->limit(1),
        ])->first();

        $trad_fee = BaseData::query()->where('type', 'trad_fee')->first();
        return view('admin.manage.fees.index', compact('result', 'units', 'min_withdraw', 'fee_withdraw', 'fee_withdraw_admin', 'trad_fee'));
    }

    // وقتی داخل صفحه مدیریت کارمزد از سلکت باکس ارز قسمت `مدیریت کارمزد سواپ ارزها` رو تغیر میده
    public function change_unit(Request $request)
    {
        $result = BaseData::where('type', 'manage_swap_' . $request->unit)->get();
//        return view('admin.manage.swap-withdraw.form', compact('result'));
        return view('admin.manage.fees.form', compact('result'));
    }

    // وقتی داخل صفحه مدیریت کارمزد از سلکت باکس ارز قسمت `مدیریت کارمزد و حداقل برداشت ارزها` رو تغیر میده
    public function change_unit_for_withdraw(Request $request)
    {
        $min_withdraw = BaseData::where('type', 'min_withdraw_' . $request->unit)->value('extra_field1');
        $fee_withdraw = BaseData::where('type', 'fee_withdraw_' . $request->unit)->value('extra_field1');

//        return view('admin.manage.swap-withdraw.withdraw-form', compact('min_mithdraw', 'fee_mithdraw'));
        return view('admin.manage.fees.withdraw-form', compact('min_withdraw', 'fee_withdraw'));
    }

    // ذخیره کردن تنظیمات کارمزد ها
    public function fees_store(Request $request)
    {
        if ($request->has('name')) {
            switch ($request->name) {
                // مربوط به کارمزد ترید
                case 'trade':
                    $request->validate([
                        'trad_fee' => 'required'
                    ]);
                    try {
                        BaseData::query()->updateOrCreate(
                            ['type' => 'trad_fee', 'name' => 'کارمزد ترید در روتیکس'],
                            ['extra_field1' => $request->trad_fee]
                        );
                        return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

                    } catch (\Exception $exception) {
                        return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
                    }
                    break;

                // کارمزد انتقال ارزها از کابران به کیف پول ادمین
                case 'fee_withdraw_admin_network':
                    $request->validate([
                        'fee_withdraw_admin_trc10' => 'required|numeric|between:0,99.99',
                        'fee_withdraw_admin_trc20' => 'required|numeric|between:0,99.99',
                    ]);
                    try {
                        BaseData::updateOrCreate(
                            ['type' => "fee_withdraw_admin_trc10"],
                            ['extra_field1' => $request->fee_withdraw_admin_trc10, 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین']
                        );
                        BaseData::updateOrCreate(
                            ['type' => "fee_withdraw_admin_trc20"],
                            ['extra_field1' => $request->fee_withdraw_admin_trc20, 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین']
                        );

                        return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
                    } catch (\Exception $e) {
                        return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
                    }
                    break;

                // مدیریت کارمزد و حداقل برداشت ارزها
                case 'fee_min_withdraw':
                    $request->fee_withdraw = str_replace(',', '', $request->fee_withdraw);
                    $request->min_withdraw = str_replace(',', '', $request->min_withdraw);

                    $this->validate($request, [
                        'fee_withdraw' => 'required',
                        'min_withdraw' => 'required',
                    ]);
                    try {
                        BaseData::query()->updateOrCreate(
                            ['type' => 'min_withdraw_' . $request->unit, 'name' => 'حداقل برداشت ' . $request->unit],
                            ['extra_field1' => "$request->min_withdraw"]
                        );
                        BaseData::query()->updateOrCreate(
                            ['type' => "fee_withdraw_$request->unit", 'name' => 'کارمزد برداشت ' . $request->unit],
                            ['extra_field1' => "$request->fee_withdraw"]
                        );

                        return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
                    } catch (\Exception $e) {
                        return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
                    }
                    break;

                // مدیریت کارمزد سواپ ارزها
                case 'swap_fee':
                    $request->percent = str_replace(',', '', $request->percent);
                    $request->from = str_replace(',', '', $request->from);
                    $request->to = str_replace(',', '', $request->to);

                    $request->validate([
                        'unit' => 'required|string',
                        'from.*' => 'required|between:0,999999999.99',
                        'to.*' => 'required|between:0,999999999.99',
                        'percent.*' => 'required|between:0,999999999.99',
                    ]);
                    try {
                        BaseData::where('type', 'manage_swap_' . $request->unit)->delete();
                        foreach ($request->percent as $key => $item)
                            $model = BaseData::create([
                                'type' => 'manage_swap_' . $request->unit,
                                'name' => 'مدیریت سواپ ' . $request->unit,
                                'extra_field1' => $request->from[$key],
                                'extra_field2' => $request->to[$key],
                                'extra_field3' => $item
                            ]);
                        if ($model)
                            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

                    } catch (\Exception $e) {
                        return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات.']);
                    }
                    break;

            }
        } else {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
        }
    }

    public function manage_withdraw_swap_page()
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$result = BaseData::where('type', 'manage_swap_rial')->orderBy('id', 'asc')->get();
        $units = Currency::select('unit')->get();
        $min_mithdraw = BaseData::where('type', 'min_withdraw_rial')->value('extra_field1');
        $fee_mithdraw = BaseData::where('type', 'fee_withdraw_rial')->value('extra_field1');
        $fee_mithdraw_admin = BaseData::select([
            'trc10' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc10')->limit(1),
            'trc20' => BaseData::select('extra_field1')->where('type', 'fee_withdraw_admin_trc20')->limit(1),
        ])->first();
        return view('admin.manage.swap-withdraw.index', compact('result', 'units', 'min_mithdraw', 'fee_mithdraw', 'fee_mithdraw_admin'));*/
    }

    public function manage_swap_update(Request $request)
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$request->percent = str_replace(',', '', $request->percent);
        $request->from = str_replace(',', '', $request->from);
        $request->to = str_replace(',', '', $request->to);

        $request->validate([
            'unit' => 'required|string',
            'from.*' => 'required|between:0,999999999.99',
            'to.*' => 'required|between:0,999999999.99',
            'percent.*' => 'required|between:0,999999999.99',
        ]);
        try {
            BaseData::where('type', 'manage_swap_' . $request->unit)->delete();
            foreach ($request->percent as $key => $item)
                $model = BaseData::create([
                    'type' => 'manage_swap_' . $request->unit,
                    'name' => 'مدیریت سواپ ' . $request->unit,
                    'extra_field1' => $request->from[$key],
                    'extra_field2' => $request->to[$key],
                    'extra_field3' => $item
                ]);
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات.']);
        }*/
    }


    public function manage_withdraw_update(Request $request)
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$request->fee_withdraw = str_replace(',', '', $request->fee_withdraw);
        $request->min_withdraw = str_replace(',', '', $request->min_withdraw);

        $this->validate($request, [
            'fee_withdraw' => 'required',
            'min_withdraw' => 'required',
        ]);
        try {
            BaseData::query()->updateOrCreate(
                ['type' => 'min_withdraw_' . $request->unit, 'name' => 'حداقل برداشت ' . $request->unit],
                ['extra_field1' => "$request->min_withdraw"]
            );
            BaseData::query()->updateOrCreate(
                ['type' => "fee_withdraw_$request->unit", 'name' => 'کارمزد برداشت ' . $request->unit],
                ['extra_field1' => "$request->fee_withdraw"]
            );

            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
        }*/
    }

    public function fee_withdraw_admin_update(Request $request)
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$this->validate($request, [
            'fee_withdraw_admin_trc10' => 'required|numeric|between:0,99.99',
            'fee_withdraw_admin_trc20' => 'required|numeric|between:0,99.99',
        ]);
        try {
            BaseData::updateOrCreate(
                ['type' => "fee_withdraw_admin_trc10"],
                ['extra_field1' => $request->fee_withdraw_admin_trc10, 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین']
            );
            BaseData::updateOrCreate(
                ['type' => "fee_withdraw_admin_trc20"],
                ['extra_field1' => $request->fee_withdraw_admin_trc20, 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین']
            );

            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
        }*/

    }


    public function manage_trade_page()
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*//        dd(json_decode(coinex()->information_withdraw_currency('usdt')));
                $currencies = currentUnits();
                $trad_fee = BaseData::query()->where('type', 'trad_fee')->first();

                $first_currency = array_keys($currencies)[0];
                $first_coinex = json_decode(coinex()->information_withdraw_currency("$first_currency"));
                $first_min_withdraw_rootix = BaseData::query()->where('type', "min_withdraw_$first_currency")->first();
                $first_fee_withdraw_rootix = BaseData::query()->where('type', "fee_withdraw_$first_currency")->first();
                $coinex_key = $first_currency == 'usdt' ? 'USDT-TRC20' : strtoupper($first_currency);
                $first = [
                    'coinex' => $first_coinex->data->$coinex_key,
                    'first_min_withdraw_rootix' => $first_min_withdraw_rootix->extra_field1,
                    'first_fee_withdraw_rootix' => $first_fee_withdraw_rootix->extra_field1
                ];
        //        dd($usdt);
                return view('admin.manage.trade.index', compact('currencies', 'trad_fee', 'first'));*/
    }

    function get_currency_fees(Request $request)
    {
        $currency_upper = strtoupper($request['name']);
        $usdt_trc20 = 'USDT-TRC20';
        $fee_type = $request['fee_type'];
        if (str_contains($fee_type, 'least'))
            $base_data = BaseData::query()->where('type', 'min_withdraw_' . $request['name'])->first();
        else
            $base_data = BaseData::query()->where('type', 'fee_withdraw_' . $request['name'])->first();

        $currency_fee = json_decode(coinex()->information_withdraw_currency($request['name']));
        if ($request['name'] == 'usdt')
            $currency_fee = $currency_fee->data->$usdt_trc20->$fee_type;
        else
            $currency_fee = $currency_fee->data->$currency_upper->$fee_type;
        $result = ['type' => $fee_type, 'coinex' => $currency_fee, 'rootix' => $base_data->extra_field1, 'status' => 200];
        return response()->json($result);
    }

    public function trad_fees_update(Request $request)
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$type = $request['fee_type'];
        if ($type == 'withdraw_least_amount') {
            $request->min_withdraw_rootix = str_replace(',', '', $request->min_withdraw_rootix);
            $this->validate($request, [
                'min_withdraw_rootix' => 'required',
            ]);
            try {
                BaseData::query()->updateOrCreate(
                    ['type' => 'min_withdraw_' . $request['unit'], 'name' => 'حداقل برداشت ' . $request['unit']],
                    ['extra_field1' => $request->min_withdraw_rootix]
                );
                return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
            } catch (\Exception $exception) {
                return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
            }
        } else {
            $request->fee_withdraw_rootix = str_replace(',', '', $request->fee_withdraw_rootix);
            $this->validate($request, [
                'fee_withdraw_rootix' => 'required',
            ]);
            try {
                BaseData::query()->updateOrCreate(
                    ['type' => "fee_withdraw_" . $request['unit'], 'name' => 'کارمزد برداشت ' . $request['unit']],
                    ['extra_field1' => $request->fee_withdraw_rootix]
                );
                return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
            } catch (\Exception $exception) {
                return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);
            }
        }*/
    }

    public function trad_fee_update(Request $request)
    {
        return 'رفته قسمت مدیریریت کارمزد ها';
        /*$this->validate($request, [
            'trad_fee' => 'required',
        ]);

        try {
            BaseData::query()->updateOrCreate(
                ['type' => 'trad_fee', 'name' => 'کارمزد ترید در روتیکس'],
                ['extra_field1' => $request->trad_fee]
            );
            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'خطا در آپدیت اطلاعات']);

        }*/
    }

    public function set_fees()
    {
        $base_data = BaseData::query()->where('type', 'like', '%' . 'fee_withdraw_' . '%')
            ->orWhere('type', 'like', '%' . 'min_withdraw_' . '%')
            ->get();
        $usdt_trc20 = 'USDT-TRC20';
        $eth_ERC20 = 'ETH-ERC20';
        foreach ($base_data as $item) {
            if (str_contains($item->type, 'min_withdraw_') && !str_contains($item->type, 'rial') && !str_contains($item->type, 'admin')) {
                $unit = str_replace('min_withdraw_', '', $item->type);
                $UNIT = strtoupper($unit);
                $coinex = json_decode(coinex()->information_withdraw_currency($unit));
                if ($unit == 'usdt') {
                    $item->update([
                        'extra_field1' => $coinex->data->$usdt_trc20->withdraw_least_amount
                    ]);
                    $item->save();
                } elseif ($unit == 'eth') {
                    $item->update([
                        'extra_field1' => $coinex->data->$eth_ERC20->withdraw_least_amount
                    ]);
                    $item->save();
                } else {
                    $item->update([
                        'extra_field1' => $coinex->data->$UNIT->withdraw_least_amount
                    ]);
                    $item->save();
                }
            } elseif (str_contains($item->type, 'fee_withdraw_') && !str_contains($item->type, 'rial') && !str_contains($item->type, 'admin')) {
                $unit = str_replace('fee_withdraw_', '', $item->type);
                $UNIT = strtoupper($unit);
                $coinex = json_decode(coinex()->information_withdraw_currency($unit));
                if ($unit == 'usdt') {
                    $item->update([
                        'extra_field1' => $coinex->data->$usdt_trc20->withdraw_tx_fee
                    ]);
                    $item->save();
                } elseif ($unit == 'eth') {
                    $item->update([
                        'extra_field1' => $coinex->data->$eth_ERC20->withdraw_tx_fee
                    ]);
                    $item->save();
                } else {
                    $item->update([
                        'extra_field1' => $coinex->data->$UNIT->withdraw_tx_fee
                    ]);
                    $item->save();
                }
            }
//            dd('succeed');
        }

    }

    public function support_form()
    {
        $base_data = BaseData::query()->where('type', 'support_setting')->first();
        $base_data = unserialize($base_data->extra_field3);

        return view('admin.support', compact('base_data'));

    }

    public function support(Request $request)
    {
        $request->validate([
            'emails.*' => ['required', 'exists:users,email',
                function ($attr, $val, $fail) {
                    $model = User::query()->where('email', $val)->whereNotNull('mobile')->exists();
                    $model ? '' : $fail('برای این کاربر موبایل ثبت نشده است.');
                }
            ]
        ]);


        $array = [
            'Saturday' => $request->emails[0],
            'Sunday' => $request->emails[1],
            'Monday' => $request->emails[2],
            'Tuesday' => $request->emails[3],
            'Wednesday' => $request->emails[4],
            'Thursday' => $request->emails[5],
            'Friday' => $request->emails[6],
        ];
        $array = serialize($array);

        BaseData::query()->where('type', 'support_setting')->update(['extra_field3' => $array]);
        return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);

    }

    public function laws_form()
    {
        $laws=BaseData::query()->where('type', 'laws')->first();
        return view('admin.laws',compact('laws'));
    }

    public function laws(Request $request)
    {
        $request->validate([
            'description' => 'required'
        ]);

        try {
            $laws = json_encode($request->description);
//            $laws = serialize($request->description);
            BaseData::query()->where('type', 'laws')->update(['extra_field3' => $laws]);
            return response()->json(['status' => 100, 'msg' => 'اطلاعات با موفقیت آپدیت شد']);
        } catch (\Exception $exception) {
            dd($exception);
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. لطفا بعدا امتحان کنید']);
        }
    }
}
