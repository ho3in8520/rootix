<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Currency;
use App\Models\Finance_transaction;
use App\Models\GlobalMarketApi;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:' . name_roles_string_middleware(), 'permission:dashboard_menu'])->only('dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function test()
    {
        return view('user.exchange.trade');
    }

    public function dashboard()
    {
        $currencies=get_specific_currencies('BTC,ETH,WIN,BTT,JST,USDT,BNB,XRP,ADA,SHIB,TRX,DOGE,SOL,WBTC,LTC,AVAX,DOT,MATIC,
        ATOM,ETC,XLM,VET,HBAR,FIL,ICP,EGLD,THETA,SAND,FTM,AXS,XTZ,WAVES,HNT,AAVE,CAKE,ZEC,EOS,FLOW');
        $currencies=collect($currencies);
        $currencies=$currencies->sortByDesc('market_cap');
        $header_currencies=get_specific_currencies('BTC,ETH,ADA,SOL,SHIB,DOGE');

        $assets=auth()->user()->assets;
        $assets_amount = get_specific_currencies(implode(',', $assets->pluck('unit')->toArray()));
        $usdt_asset=0; // جمع موجودی ارزهای کاربر به دلار
        $usdt_decimal=contractUnit('usdt');
        foreach ($assets as $asset) {
            if ($asset->unit != 'rls')
                $usdt_asset += convert_currency($asset->unit, 'usdt', $asset->amount, false, $assets_amount);
        }
        $usdt_asset_to_rls=convert_currency('usdt','rls',$usdt_asset); // ===> جمع موجودی ارزهای کاربر به تومان (واحد سایت)
        $usdt_asset=number_format($usdt_asset,$usdt_decimal['precision']);

        $rls_asset=$assets->where('unit','rls')->first(); // موجودی ریالی کیف پول کاربر
        $rls_asset_to_usdt=number_format(convert_currency('rls','usdt',$rls_asset->amount),$usdt_decimal['precision']); // موجودی ریالی کیف پول کاربر به دلار

        $total_usdt_asset=(float)$rls_asset_to_usdt+(float)$usdt_asset; // جمع موجودی کل کاربر به دلار (موجودی ارز + موجودی ریال)
        $total_rls_asset=$usdt_asset_to_rls+rial_to_unit($rls_asset->amount,'rls'); // جمع موجودی کل کاربر به ریال (موجودی ارز + موجودی ریال)

        $result=[
            'usdt_asset' =>$usdt_asset,
            'usdt_asset_to_rls' =>$usdt_asset_to_rls,
            'rls_asset' =>rial_to_unit($rls_asset->amount,$rls_asset->unit),
            'rls_asset_to_usdt' =>$rls_asset_to_usdt,
            'total_usdt_asset' =>$total_usdt_asset,
            'total_rls_asset' =>$total_rls_asset,
        ];

        return view('user.dashboard', compact('header_currencies','currencies','result'));
    }

    public function get_theter_price(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://gateway.accessban.com/public/web-service/list/crypto?format=json&limit=30&title=theter",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Accept: application/json",
                'Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZ2F0ZXdheS5hY2Nlc3NiYW4uY29tXC9wdWJsaWMiLCJzdWIiOiJmYjk3YWUxOC02ZjU1LTUyY2ItODg4NS01ODVlNjQ1ZjA3ZTIiLCJpYXQiOjE2MjIyNzMzMDQsImV4cCI6MTc4MDAzOTcwNCwibmFtZSI6InNhamphZCBzdWx0YW5pLTEyMjc5In0.lQSgxebT92IroGbrn_r2HpVrniEDrwytTpkuTxNitWA'
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

    }

    // پاک کردن نوتیف ها در پنل کاربر
    public function clear_notif()
    {
        \auth()->user()
            ->notifications()
            ->delete();
        return response()->json(['status' => 100, 'msg' => 'نوتیف‌ ها با موفقیت پاکسازی شدند']);
    }


}
