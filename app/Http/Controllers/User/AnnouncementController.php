<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Announcement;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    // این تابع برای وقتی هست که کاربر اطلاعیه رو دید
    public function visit()
    {
        $announcements= get_announcements('not-visited');
        $user_id= auth()->user()->id;
        if ($announcements) {
            foreach ($announcements as $ann) {
                $visited= $ann->visited;
                $visited[] = $user_id;
                $ann->visited= $visited;
                $ann->save();
            }
        }
    }
}
