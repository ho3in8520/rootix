<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use App\Models\Currency;
use App\Models\Trade;
use Carbon\Carbon;
//use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;

class ExchangeController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:trade_menu')->only('index');
        $this->middleware('permission:rial_trade_buy|rial_trade_sell|usdt_trade_buy|usdt_trade_sell')->only('new_trade');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($des_unit, $source_unit)
    {
        $markets = ['des' => $des_unit, 'source' => $source_unit];
        $markets_value = [];
        $assets = auth()->user()->assets->whereIn('unit', [$des_unit, ($source_unit == 'rial') ? 'rls' : $source_unit]);

        if (count($assets) < 2) {
            abort(404);
        }
        foreach ($assets as $asset) {
            if ($asset->unit == $markets['des'])
                $markets_value = array_merge($markets_value, ['des_amount' => $asset->amount]);
            else
                $markets_value = array_merge($markets_value, ['source_amount' => $asset->amount]);
        }

        $trades = auth()->user()->trades;

        return view('user.exchange.trade', compact('markets', 'markets_value', 'trades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function buy(Request $request)
    {
        $this->validate($request,[
            'price' => 'required|numeric',
            'amount' => 'required|numeric',
            'type' => ['required',Rule::in(['buy','sell'])],
            'unit' => ['required',Rule::in(['usdt','rial'])],
            'market' => 'required',
        ]);

        $amount = $request['amount'];
        $price = $request['price'];
        $unit=$request['unit']=='rial'?'rls':$request['unit'];
        $unit_asset = auth()->user()->assets->where('unit', $unit)->first();
//        if ($amount * $price > $unit_asset->amount)
//            return response()->json(['status' => 500, 'msg' => 'موجودی شما کافی نمی باشد.']);
//        if ($unit == 'rls' || $unit=='rial') {
//            $rootix_amount = unit_to_rial($price) * $amount;
//            $price = $price / $usdt_price;
//        } else
//            $rootix_amount = $price * $amount;

        $trad_fee = BaseData::query()->where('type', 'trad_fee')->first();
        $coinex_amount = ($amount * $price);
        $coinex_amount = ($coinex_amount) - ($coinex_amount * $trad_fee->extra_field1 / 100);
        $new_amount = $coinex_amount / $price;

    }

    public function sell(Request $request)
    {
        $this->validate($request,[
            'sell_price' => 'required|numeric',
            'sell_amount' => 'required|numeric',
            'type' => ['required',Rule::in(['buy','sell'])],
            'unit' => ['required',Rule::in(['usdt','rial'])],
            'market' => 'required',
        ]);

        $amount = $new_amount = $request['sell_amount'];
        $price = $request['sell_price'];
        $des_asset = auth()->user()->assets->where('unit', $request['des_asset'])->first();

    }

    public function new_trade(Request $request)
    {
        $usdt_price = get_specific_currencies('usdt');
        $usdt_price = $usdt_price['USDT']['rls'];
        $unit=$request['unit']=='rial'?'rls':$request['unit'];

        $type = $request['type'] == 'buy' ? 'buy' : 'sell';
        $market = strtoupper(str_replace('/','',$request['market']));
        $market=str_replace('RIAL','USDT',$market);
        if ($type == 'buy') {
            $this->validate($request, [
                'price' => 'required|numeric',
                'amount' => 'required|numeric',
            ]);
            $amount = $request['amount'];
            $price = $request['price'];
            $unit_asset = auth()->user()->assets->where('unit', $unit)->first();
            if ($amount * $price > $unit_asset->amount)
                return response()->json(['status' => 500, 'msg' => 'موجودی شما کافی نمی باشد.']);
            if ($unit == 'rls' || $unit == 'rial') {
                $rootix_amount = unit_to_rial($price) * $amount;
                $price = $price / $usdt_price;
            } else
                $rootix_amount = $price * $amount;

            $trad_fee = BaseData::query()->where('type', 'trad_fee')->first();
            $coinex_amount = ($amount * $price);
            $coinex_amount = ($coinex_amount) - ($coinex_amount * $trad_fee->extra_field1 / 100);
            $new_amount = $coinex_amount / $price;
        } else {
            $this->validate($request, [
                'sell_price' => 'required|numeric',
                'sell_amount' => 'required|numeric',
            ]);
            $amount = $new_amount = $request['sell_amount'];
            $price = $request['sell_price'];
            if ($unit == 'rls' || $unit == 'rial'){
                $rootix_amount = $amount * unit_to_rial($price);
                $price=unit_to_rial($price)/$usdt_price;
            }else
                $rootix_amount = $amount * $price;

            $des_asset = auth()->user()->assets->where('unit', $request['des_asset'])->first();
            if ($amount > $des_asset->amount)
                return response()->json(['status' => 500, 'msg' => 'موجودی ارز شما کمتر از مقدار وارد شده می باشد.']);
            if ($des_asset->amount == 0)
                return response()->json(['status' => 500, 'msg' => 'موجودی ارز شما 0 می باشد.']);
        }
        $response = coinex()->new_trade($market, $type, $new_amount, $price, auth()->user()->id);
        $response = json_decode($response);
        if ($response->code == 0) {
            $type = $type == 'buy' ? 1 : 0;

            try {
                $trade = new Trade();
                $trade->user_id = auth()->user()->id;
                $trade->id_trade = (string)$response->data->id;
                $trade->status = $response->data->status;
                $trade->market = $response->data->market;
                $trade->type = $type;
                $trade->finished_time = $response->data->finished_time;
                $trade->trade_info = json_encode($response);
                $trade->amount = $response->data->amount;
                $trade->old_amount = $amount;
                $trade->price = $response->data->price;
                $trade->total_amount = $rootix_amount;
                $trade->fee_tx = 0;
                $trade->unit = $unit;
                $trade->save();

                if ($type == 1) {
                    $unit_asset->amount -= $rootix_amount;
                    $unit_asset->save();
                } else {
                    $des_asset->amount -= $amount;
                    $des_asset->save();
                }

                if ($trade->status == 1)
                    trade_logs($trade);
                return response()->json(['status' => 100, 'msg' => 'سفارش شما با موفقیت ثیت شد.']);
            } catch (\Exception $exception) {
                return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
            }
        } elseif ($response->code == 107) {
            return response()->json(['status' => 500, 'msg' => 'در حال حاضر امکان ثبت سفارش وجود ندارد. 30 دقیقه دیگر امتحان کنید.']);
        } elseif ($response->code == 602) {
            return response()->json(['status' => 500, 'msg' => 'مقدار وارد شده ارز کمتر از حد مجاز می باشد.']);
        } else {
            return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
        }

    }

    public function cancel_trade($id)
    {
        $trade = Trade::query()->where('id_trade', $id)->first();
        $response = coinex()->cancel_trade($trade->id_trade, $trade->market);
        $response = json_decode($response);
        if ($response->code == 0) {
            try {
                $trade->update([
                    'status' => 'cancel',
                    'trade_info' => json_encode($response),
                ]);
                if ($trade->type == 1) {
                    $unit_asset = auth()->user()->assets->where('unit', $trade->unit)->first();
                    $unit_asset->amount += $trade->total_amount;
                    $unit_asset->save();
                } else {
                    $unit = strtolower(str_replace('USDT', '', $trade->market));
                    $currency_asset = auth()->user()->assets->where('unit', $unit)->first();
                    $currency_asset->amount += $trade->amount;
                    $currency_asset->save();
                }

                return response()->json(['status' => 100, 'msg' => 'سفارش شما با موفقیت لغو شد.']);
            } catch (\Exception $exception) {
                return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است. بعدا دوباره امتحان کنید.']);
            }

        } else {
            return response()->json(['status' => 500, 'msg' => 'مشکلی به وجود آمده است. لطفا بعدا امتحان کنید.']);
        }
    }


    public function list_request(Request $request)
    {
        return $this->get_request(strtoupper($request->market));
    }

    //// list_request
    private function get_request($market)
    {
        try {
            $ex = explode('/', $market);
            if ($ex[1] == 'RIAL') {
                $market = $ex[0] . '/USDT';
            }
            $market = str_replace('/', '', $market);
            $result = Http::get('https://api.coinex.com/v1/market/depth?market=' . $market . '&limit=50&merge=0');
            if ($result->status() == 200) {
                $data = json_decode($result->body(), true)['data'];

                foreach ($data['asks'] as $key => $item) {
                    $data['asks'][$key]['total'] = $item[0] * $item[1];
                }
                foreach ($data['bids'] as $key => $item) {
                    $data['bids'][$key]['total'] = $item[0] * $item[1];
                }
                return response()->json(array_merge($this->percent_background($data), ['last' => $data['last']]));
            } else {
                return ['sells' => [], 'buys' => [], 'last' => 0];
            }
        } catch (\Exception $e) {
            return ['sells' => [], 'buys' => [], 'last' => 0];
        }
    }

    //// list_request
    private function percent_background($data)
    {
        $buys = [$data['bids']];
        $sells = [$data['asks']];

        $sell = $this->percent_background_mode($this->multisort($sells, "0", 'sell'), 'sell');
        $buy = $this->percent_background_mode($this->multisort($buys, "0", 'buy'), 'buy');
        return ['sells' => $sell, 'buys' => $buy];
    }

    //// list_request
    private function percent_background_mode($data, $mode)
    {
        $max = $this->max($data);
        if ($mode == 'sell') {
            foreach ($data as $k => $row) {
                $percent = ($row['total'] / $max) == 0 ? 0 : ($row['total'] / $max);
                $data[$k] = array_merge($data[$k], ['percent_background' => ($percent * 100) + 13]);
            }
        } else {
            foreach ($data as $k => $row) {
                $percent = ($row['total'] / $max) == 0 ? 0 : ($row['total'] / $max);
                $data[$k] = array_merge($data[$k], ['percent_background' => ($percent * 100) + 13]);
            }
        }
        return $data;
    }

    //// list_request
    private function multisort(&$array, $key, $mode = 'sell')
    {
        $valsort = array();
        $ret = array();
        reset($array);
        foreach ($array[0] as $ii => $va) {
            $valsort[$ii] = $va[$key];
        }
        asort($valsort);
        foreach ($valsort as $ii => $va) {
            $ret[$ii] = $array[0][$ii];
        }
//        return $ret;
        return $mode == 'sell' ? $ret : array_reverse($ret);
    }

    //// list_request
    private function max(array $data)
    {
        $array = [];
        foreach ($data as $key => $item) {
            $array[] = $item['total'];
        }
        return max($array);
    }

    public function last_transaction(Request $request)
    {
        return $this->get_request_transaction(strtoupper($request->market));
    }

    ////last_transaction
    private function get_request_transaction($market)
    {
        try {
            $ex = explode('/', $market);
            if ($ex[1] == 'RIAL') {
                $market = $ex[0] . '/USDT';
            }
            $market = str_replace('/', '', $market);
            $result = Http::get('https://api.coinex.com/v1/market/deals?limit=15&market=' . $market);
            if ($result->status() == 200) {
                $data = json_decode($result->body(), true)['data'];
                foreach ($data as $key => $item) {
                    $data[$key]['date'] = Carbon::parse($data[$key]['date'])->format('H:i:s');
                    if ($item['type'] == 'buy') {
                        $data[$key]['color'] = 'green';
                    } else {
                        $data[$key]['color'] = 'red';
                    }
                }
                return $data;
            } else
                return false;
        } catch (\Exception $e) {
            return 'موردی یافت نشد';
        }
    }

    public function markets(Request $request)
    {
        $this->validate($request, [
            'market' => 'required|string',
        ]);
        try {
            $result = $this->currencies_market_stats($request->market);
            return $result;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function currencies_market_stats($currencies)
    {
        $ticker = [
            'rls',
            'usdt',
            'btc',
            'eth',
            'trx',
            'bnb',
            'shib',
            'doge',
            'xrp',
            'ada',
            'sol',
            'btt',
            'jst',
            'win',
            'wbtc',
            'ltc',
            'avax',
            'dot',
            'matic',
            'atom',
            'etc',
            'xlm',
            'vet',
            'hbar',
            'fil',
            'icp',
            'egld',
            'theta',
            'sand',
            'ftm',
            'axs',
            'xtz',
            'waves',
            'hnt',
            'aave',
            'cake',
            'zec',
            'eos',
            'flow',
        ];
        $array = [];
        $result = get_specific_currencies(implode(',', $ticker));

        $source_unit = explode('/', $currencies)[1];
        foreach ($result as $key => $item) {
            $array[$key] = [
                'currency' => $item['currency'],
                'name' => $item['name'],
                'logo_url' => $item['logo_url'],
//                'price' => convert_currency('usdt', $source_unit, $item['price'], true, $result),
                'price' => $item['price'],
                'price_btc' => $item['price_btc'],
                'rls' => $item['rls'],
                'url' => route('exchange.index', ['des_unit' => strtolower($item['currency']), 'source_unit' => $source_unit]),
            ];
        }
//        unset($array[strtoupper($source_unit) == 'RIAL' ? 'RLS' : strtoupper($source_unit)]);
        return response()->json($array);
    }
}
