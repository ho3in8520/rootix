<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Notification;
use App\Models\TicketDetail;
use App\Models\TicketMaster;
//use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class TicketController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:user_ticket_menu')->only('index');
        $this->middleware('permission:ticket_create|ticket_show');
        $this->middleware('permission:ticket_close')->only('update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = TicketMaster::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(10);

        return view('user.ticket.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('user.ticket.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'role_id' => ['required', Rule::in([1, 2])],
            'priority' => ['required', Rule::in([1, 2, 3])],
            'description' => 'required|string',
            'file' => 'sometimes|mimes:jpeg,png,jpg,zip,pdf,rar|max:5120',
        ]);

        try {
            $ticket_master = new TicketMaster();
            $ticket_master->user_id = auth()->user()->id;
            $ticket_master->title = $request['title'];
            $ticket_master->priority = $request['priority'];
            $ticket_master->role_id = $request['role_id'];
            $ticket_master->status = 1;
            $ticket_master->save();

            $ticket_detail = new TicketDetail();
            $ticket_detail->user_id = auth()->user()->id;
            $ticket_detail->type = 1;
            $ticket_detail->master_id = $ticket_master->id;
            $ticket_detail->description = $request['description'];
            $ticket_detail->save();
            if ($request->has('file')) {
                $ticket_detail->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('file'), 'ticket'),
                    'type' => 'ticket'
                ]);
            }
            support_notif('تیکت');
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'url' => route('tickets.show', $ticket_master->id)]);
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'msg' => 'خطایی رخ داده است']);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = TicketMaster::where('id', $id)->firstOrFail();
        $answers = TicketDetail::query()->where('master_id', $ticket->id)->get();
        $last_ticket = $answers->last();
        $last_ticket = calculate_time($last_ticket->updated_at);
        return view('user.ticket.show', compact('ticket', 'answers', 'last_ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('user.ticket.show');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $ticket = TicketMaster::query()->findOrFail($id);
            $ticket->status = 4;
            $ticket->save();
            return response()->json(['status' => 100, 'msg' => 'تیکت با موفقیت بسته شد.']);
        } catch (\Exception $exception) {
            dd($exception);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function comment(Request $request, $id)
    {
        $this->validate($request, [
            'description' => 'required|string',
            'file' => 'sometimes|mimes:jpeg,png,jpg,zip,pdf,rar|max:10240',
        ]);

        try {
            TicketMaster::where('id', $id)->update(['status' => 1]);
            $ticket_detail = new TicketDetail();
            $ticket_detail->user_id = auth()->user()->id;
            $ticket_detail->type = 1;
            $ticket_detail->master_id = $id;
            $ticket_detail->description = $request['description'];
            $ticket_detail->save();
            if ($request->has('file')) {
                $ticket_detail->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('file'), 'ticket'),
                ]);
            }
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید']);
        } catch (\Exception $exception) {
            dd($exception);
        }

    }
}
