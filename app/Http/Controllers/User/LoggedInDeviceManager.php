<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\UserSession;
use Illuminate\Http\Request;

class LoggedInDeviceManager extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = UserSession::query()
            ->where('user_id', auth()->user()->id)
            ->get(['sessions.*', 'sessions.id as device_id'])->reverse();

        return view('user/logged-in-devices.list')
            ->with('devices', $devices)
            ->with('current_session_id', \Session::getId());
    }

    /**
     * Logout a session based on session id.
     *
     * @return \Illuminate\Http\Response
     */
    public function logoutDevice(Request $request, $device_id)
    {
        \DB::table('sessions')
            ->where('user_id', \Auth::user()->id)
            ->where('id', $device_id)->delete();

        return redirect('/logged-in-devices');
    }



    /**
     * Logouts a user from all other devices except the current one.
     *
     * @return \Illuminate\Http\Response
     */
    public function logoutAllDevices(Request $request)
    {
        UserSession::query()
            ->where('user_id', \Auth::user()->id)
            ->where('id', '!=', \Session::getId())->delete();

        return redirect('/logged-in-devices');
    }

}
