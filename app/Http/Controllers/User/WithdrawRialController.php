<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\BaseData;
use App\Models\WithdrawRial;
use Carbon\Carbon;
//use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WithdrawRialController extends Controller
{
    /**
     * لیست درخواست های برداشت کاربر
     */
    public function index()
    {
        $withdraw_rial = auth()->user()->withdraw_rial()->with('bank', function ($q) {
            $q->select(['id', 'name', 'card_number', 'account_number', 'sheba_number']);
        });

        $start_deposit_date = \request()->start_deposit_date ? jalali_to_gregorian(\request()->start_deposit_date . " 00:00:00") : false;
        $end_deposit_date = \request()->end_deposit_date ? jalali_to_gregorian(\request()->end_deposit_date . " 23:59:59") : false;

//        \request()->amount ? $withdraw_rial->where('amount', \request()->amount) : '';
        \request()->amount ? $withdraw_rial->where('amount', unit_to_rial(\request()->amount)) : '';
        in_array(\request()->status, ['0', '1', '2']) ? $withdraw_rial->where('status', \request()->status) : '';
        \request()->tracking_code ? $withdraw_rial->where('tracking_code', \request()->tracking_code) : '';
        in_array(\request()->deposit_type, ['0', '1', '2']) ? $withdraw_rial->where('deposit_type', \request()->deposit_type) : '';

        $start_deposit_date ? $withdraw_rial->where('deposit_date', '>=', $start_deposit_date) : '';
        $end_deposit_date ? $withdraw_rial->where('deposit_date', '<=', $end_deposit_date) : '';

        $withdraw_rial = $withdraw_rial->latest()->paginate(20);
        return showData(view('user.withdraw_rial.index', compact('withdraw_rial')));
    }

    /**
     * صفحه ی ثبت درخواست برداشت کاربر
     */
    public function create()
    {
        $rial_asset = auth()->user()->assets()->where('unit', 'rls')->first(['amount']);
        $min_withdraw = BaseData::query()->where('type', 'min_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $fee_withdraw = BaseData::query()->where('type', 'fee_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $banks = auth()->user()->bank()->active()->get();
        $today_withdraw = auth()->user()->withdraw_rial()->whereIn('status', [0, 1])->where(DB::raw('DATE(created_at)'), Carbon::today()->toDateString())->sum('amount');
        return view('user.withdraw_rial.create', compact('rial_asset', 'min_withdraw', 'fee_withdraw', 'banks', 'today_withdraw'));
    }

    /**
     * ذخیره درخواست برداشت ریالی
     */
    public function store(Request $request)
    {
        $banks = auth()->user()->bank()->active()->get(['id'])->pluck('id')->toArray();
        $rial_asset = auth()->user()->assets()->where('unit', 'rls')->first(['id', 'amount']);
        $min_withdraw = BaseData::query()->where('type', 'min_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $fee_withdraw = BaseData::query()->where('type', 'fee_withdraw_rial')->first(['extra_field1'])->extra_field1;
        $today_withdraw = auth()->user()->withdraw_rial()->whereIn('status', [0, 1])->where(DB::raw('DATE(created_at)'), Carbon::today()->toDateString())->sum('amount');
        $max_daily_withdraw = max_withdraw_daily();
        if ($max_daily_withdraw == -1)
            $max = $rial_asset->amount; // وقتی سقفی نداره حداکثر میزان برداشتش میشه میزان موجودی کاربر
        else {
            $today_max = $max_daily_withdraw - $today_withdraw;
            $max = $today_max > $rial_asset->amount ? $rial_asset->amount : $today_max;
        }
        $request->validate([
            'amount' => "required|numeric|min:$min_withdraw|max:{$max}",
            'bank' => "required|in:" . implode(',', $banks)
        ]);
        try {
            DB::beginTransaction();
//            $amount_withdrawn = $request->amount - $fee_withdraw;
            $amount_withdrawn = unit_to_rial($request->amount) - $fee_withdraw;
            if ($amount_withdrawn < 0)
                return response()->json(['status' => 500, 'msg' => 'مقدار قابل برداشت شما باید بیشتر از 0 باشد']);

            auth()->user()->withdraw_rial()->create([
//                'amount' => $request->amount,
                'amount' => unit_to_rial($request->amount),
                'user_des' => $request->user_des,
                'fee' => $fee_withdraw,
                'bank_id' => $request->bank
            ]);

//            $rial_asset->amount = $rial_asset->amount - $request->amount;
            $rial_asset->amount = $rial_asset->amount - unit_to_rial($request->amount);
            $rial_asset->save();

            $unit=get_unit();
            $fee_withdraw=rial_to_unit($fee_withdraw,'rls',true);
            $transact_type = BaseData::query()->withdraw()->first(['id'])->id;
            $rial_asset->transaction()->create([
                'user_id' => auth()->user()->id,
                'transact_type' => $transact_type,
//                'amount' => $request->amount,
                'amount' => unit_to_rial($request->amount),
                'type' => 1,
                'description' => "درخواست برداشت $unit با کارمزد $fee_withdraw",
            ]);
            DB::commit();
            return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام شد']);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با مدیریت در تماس باشید']);
        }
    }
}
