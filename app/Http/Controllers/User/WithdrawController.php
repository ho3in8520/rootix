<?php

namespace App\Http\Controllers\User;


use App\Http\Controllers\Controller;
use App\Mail\WithdrawVerify;
use App\Mail\sendMessageAdmin;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Deposit;
use App\Models\Withdraw;
use Carbon\Carbon;
use IEXBase\TronAPI\Support\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use function Symfony\Component\String\s;


class WithdrawController extends Controller
{
    private $admin;
    function __construct()
    {
        $this->admin = DB::table('admin_wallet')->first();
    }

    public function index($unit)
    {
        $usdt_asset = Asset::where('user_id',auth()->user()->id)->where('unit','usdt')->first();
        $token = json_decode($usdt_asset->token,true);
        $min_send_trx = BaseData::where('type','min_send_trx')->where('extra_field1','1')->first();
        $fee_withdraw_usdt = BaseData::where('type','fee_withdraw_usdt')->where('extra_field1','1')->first();
        $min_withdraw_usdt = BaseData::where('type','min_withdraw_usdt')->where('extra_field1','1')->first();

        return view('user.withdraw.index',compact('unit','token','usdt_asset','min_send_trx','fee_withdraw_usdt','min_withdraw_usdt'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'parameters' => 'required|string',
            'parameters2' => 'required|string',
            'amount' => 'required|between:0,99,999',
            'address_wallet' => 'required|string',
        ]);
        $token = Str::random(100);
        $link = route('verify-withdraw.email', $token);
        $link2 = route('reject-withdraw.email', $token);
        $user = auth()->user();

        $asset = Asset::where('unit',$request->input('unit'))->where('user_id',$user->id)->value('token');
        $asset = json_decode($asset,true);
        $tron_balance = Tron($this->admin->private_key)->getBalance($this->admin->base58);

        $min_send_trx = BaseData::where('type','min_send_trx')->where('extra_field1','1')->first();
        $to_tron = $min_send_trx->extra_field2 * 1000000;
        $type_withdraw_trx = BaseData::where('type', 'transactions')->where('extra_field1', 5)->first();

        if ($tron_balance <= $to_tron) {
            Mail::to('info@rootix.io')->send(new sendMessageAdmin('TRX'));
            return response(['error','در این موقع تبدیل ارز در دسترس نمیباشد لطفا 30 دقیقه بعد تست فرمایید']);
        }
        $tron_balance_user = Tron($asset['privateKey'])->getBalance($asset['address']['base58']);
        $withdraw = new Withdraw();
        $withdraw->user_id = $user->id;
        $withdraw->parameter1 = $request->input('parameters');
        $withdraw->parameter2 = $request->input('parameters2');
        $withdraw->amount = $request->input('amount');
        $withdraw->unit = $request->input('unit');
        $withdraw->address_wallet = $request->input('address_wallet');
        $withdraw->verification = $token;
        $withdraw->status = 1;
        $withdraw->time_verification = Carbon::now()->addMinutes(15);
        $withdraw->save();

        if($tron_balance_user < $to_tron) {
            $amount = $to_tron - $tron_balance_user;
            $amount_tron = round($amount / 1000000,2);

            Tron($this->admin->private_key)->sendTransaction($asset['address']['hex'], $amount_tron, null, $this->admin->hex);
            $withdraw->transaction()->create([
                'user_id' => $user->id,
                'transact_type' => (int)$type_withdraw_trx->id,
                'refer_id' => (int)$user->id,
                'amount' => $min_send_trx->extra_field2,
                'type' => 1,
                'description' => "برداشت $min_send_trx->extra_field2 trx از ولت ادمین و ارسال به ولت کاربر برای انجام تراکنش"
            ]);
        }

        if ($withdraw){
            Mail::to($user->email)->send(new WithdrawVerify($link,$link2));
            Withdraw::where('id',$withdraw->id)->update([
                'status'=> 3,
            ]);
            return response(['success','عملیات با موفقیت انجام گردید برای تایید تراکنش یک ایمیل تاییدیه برای شما ارسال شد.']);

        }else{
            return response(['error','لطفا دوباره تلاش نمایید یا برای مشکلتان  به بخش فنی تیکت ارسال نمایید.']);
        }

    }

    public function verifyEmail($token)
    {
        try {
            $withdraw = Withdraw::where('verification',$token)->first();
            $withdraw->amount = $withdraw->amount / 1000000 ;
            $type_withdraw_usdt_fee = BaseData::where('type', 'fee_withdraw_usdt')->where('extra_field1', 1)->first();
            $type_withdraw_usdt_withdraw = BaseData::where('type', 'transactions')->where('extra_field1', 7)->first();

            if ($withdraw->time_verification > Carbon::now()) {
                $asset = Asset::where('unit',$withdraw->unit)->where('user_id',$withdraw->user_id)->value('token');
                $asset = json_decode($asset,true);
                if ($withdraw->verification == $token ) {

                    Tron($asset['privateKey'])->triggerSmartContract($withdraw->parameter2,'41a614f803b6fd780986a42c78ec9c7f77e6ded13c',$asset['address']['hex']);

                    $withdraw->transaction()->create([
                        'user_id' => $withdraw->user_id,
                        'transact_type' => (int)$type_withdraw_usdt_fee->id,
                        'amount' => $type_withdraw_usdt_fee->extra_field2,
                        'type' => 1,
                        'description' => "برداشت $type_withdraw_usdt_fee->extra_field2 usdt به عنوان کارمزد برای ارسال توکن به ولت مقصد"
                    ]);

                    Tron($asset['privateKey'])->triggerSmartContract($withdraw->parameter1,'41a614f803b6fd780986a42c78ec9c7f77e6ded13c',$asset['address']['hex']);
                    $withdraw->transaction()->create([
                        'user_id' => $withdraw->user_id,
                        'transact_type' => (int)$type_withdraw_usdt_withdraw->id,
                        'amount' => $withdraw->amount,
                        'type' => 1,
                        'description' => "برداشت $withdraw->amount  usdt از ولت کاربر و ارسال به ولت مقصد برای انجام تراکنش"
                    ]);

                    $withdraw->status = 4;
                    $withdraw->save();
                    return redirect()->route('user.dashboard');
                } else {
                    return 'لینک اشتباه است';
                }
            } else {
                return 'لینک منقضی شده است';
            }
        }catch (\Exception $e){
            return 'لینک شما موجود نیست';
        }
    }

    public function rejectEmail($token)
    {
        try {
            $withdraw = Withdraw::where('verification',$token)->first();
            if ($withdraw->time_verification > Carbon::now()) {
                if ($withdraw->verification == $token && $withdraw->status == 1) {
                    $withdraw->status = 2;
                    $withdraw->save();
                    return redirect()->route('user.dashboard');
                } else {
                    return 'لینک اشتباه است';
                }
            } else {
                return 'لینک منقضی شده است';
            }
        }catch (\Exception $e){
            return 'لینک شما موجود نیست';
        }
    }

    public function check_withdraw_amount(Request $request)
    {
        $this->validate($request, [
            'unit' => 'required|string',
            'amount' => 'required|between:0,99,999'
        ]);

        $asset_user = Asset::where('user_id', auth()->user()->id)->where('unit', 'rls')->first();

        $tron_balance = Tron($this->admin->private_key)->getBalance($this->admin->base58);



        if ($tron_balance <= 10000000) {
            $data = [$tron_balance, 'TRX'];
            Mail::to('info@rootix.io')->send(new sendMessageAdmin('TRX'));
            return response()->json(['status'=>'500','msg'=>'در این موقع تبدیل ارز در دسترس نمیباشد لطفا 30 دقیقه بعد تست فرمایید']);
        }


        if (isset($asset_user->amount) && $asset_user->amount >= $request->input('amount'))
        {
            if( ( $request->input('unit') == 'usdt'  ) ){
                $balance = '';
                if ($request->input('unit') == 'usdt') {

                    $balance = Tron($this->admin->private_key)->getBalanceUsdt($request->input('owner_address'));

                    $amount = $request->input('amount') ;

                    $address_wallet = Tron($this->admin->private_key)->address2HexString($request->input('address_wallet'));
                }

                return response(['success',$address_wallet,$amount,isset($balance['constant_result'][0]) ? $balance['constant_result'][0] : 'empty']);
            }

        }
        return response()->json(['status'=>'500','msg'=>"مبلغ بیش از حد مجاز میباشد."]);
    }

    public function withdraw_from_wallet(Request $request)
    {

        $owner_address = $request->input('owner_address');
        $unit = $request->input('unit');
        $amount = $request->input('amount');


        $parameters = $request->input('parameters');
       if ($unit == 'usdt') {
            $contract_address = '41a614f803b6fd780986a42c78ec9c7f77e6ded13c';
            $owner_address = $this->admin->hex;
        }

        $rial = Asset::where('user_id', Auth()->user()->id)->where('unit', 'rls')->first();
        if ($rial->amount >= $request->input('rial')) {
            $this->exchangeFromAsset($request,$rial);
            $res = Tron($this->admin->private_key)->triggerSmartContract($parameters, $contract_address, $owner_address);

            if (isset($res['txid'])) {

                $deposit = new Deposit();
                $deposit->user_id = \auth()->user()->id;
                $deposit->hash = $res['txid'];
                $deposit->amount = $amount;
                $deposit->unit = $unit;
                $deposit->mode = 'withdraw';
                $deposit->status = '1';
                $deposit->save();

                //az database az asset hay karbar kam mishavad

                $withdraw_wallet_admin_type = BaseData::where('type','transactions')->where('extra_field1','4')->first();

                $deposit->transact()->create([
                    'user_id'=>auth()->user()->id,
                    'transact_type'=> (int)$withdraw_wallet_admin_type->id,
                    'refer_id'=> (int)\auth()->user()->id,
                    'amount'=> $amount,
                    'type'=> '2',
                    'description'=> "مبلغ $amount $unit از ولت ادمین برای تبدیل ارز کم شد"
                ]);

                return response('success');
            }
        }else{
            return response(['error','مقدار موجودی شما برای تبدیل ارز کافی نمیباشد']);
        }


    }

    public function send_message_admin(Request $request)
    {

        $data = [$request->input('amount'), $request->input('unit')];
        Mail::to('info@rootix.io')->send(new sendMessageAdmin($request->input('unit')));


        return response()->json(['status'=>'500','msg'=>'در این موقع تبدیل ارز در دسترس نمیباشد لطفا 30 دقیقه بعد تست فرمایید']);
    }

    private function exchangeFromAsset($request,$rial)
    {

        $unit = Asset::where('user_id', Auth()->user()->id)->where('unit', $request->input('unit'))->first();
        $json_address_wallet = json_decode($unit->token, true);

        $amount = $request->input('amount');
        $amount_rial = $request->input('rial');
        try {
            $type_withdraw_rial = BaseData::where('type', 'transactions')->where('extra_field1', 3)->first();
            $type_deposit_usdt = BaseData::where('type', 'transactions')->where('extra_field1', 2)->first();
            $rial->amount = $rial->amount - $request->input('rial');

            $rial->save();

            $unit->amount = $unit->amount + $amount;
            $unit->save();


            $rial->transaction()->create([
                'user_id' => auth()->user()->id,
                'transact_type' => (int)$type_withdraw_rial->id,
                'amount' => $amount_rial,
                'type' => '1',
                'description' => "برداشت $amount_rial ریال برای دریافت usdt"
            ]);

            $unit->transaction()->create([
                'user_id' => auth()->user()->id,
                'transact_type' => (int)$type_deposit_usdt->id,
                'refer_id' => (int)auth()->user()->id,
                'amount' => $amount,
                'type' => 2,
                'description' => "واریز $amount usdt به حساب کاربر"
            ]);



        }catch(\Exception $e) {
            echo $e;
        }

    }

    public function get_total_usdt(Request $request)
    {
        $balance = Tron($this->admin->private_key)->getBalanceUsdt($request->input('owner_address'));

        return response()->json(isset($balance['constant_result'][0]) ? $balance['constant_result'][0] : '0');
    }

}
