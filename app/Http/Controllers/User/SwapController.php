<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use App\Models\GlobalMarketApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class SwapController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:swap_menu'])->only('index');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:swap_store']);
    }
    // نمایش صفحه ی swap
    public function index()
    {
        $assets = \auth()->user()->assets;
        $list_currencies = get_specific_currencies(implode(',', $assets->pluck('unit')->toArray()));
//        $list_currencies['RIAL'] = $list_currencies['RLS'];
//        unset($list_currencies['RLS']);
        $swap_data = BaseData::query()->where('type', 'like', 'manage_swap_%')->get(['type', DB::raw('extra_field1 as min'), DB::raw('extra_field2 as max'), DB::raw('extra_field3 as fee')])->groupBy('type')->toArray();
        $swap_assets = $assets->filter(function ($value, $key) use ($swap_data) {
            if (array_key_exists("manage_swap_{$value->unit}", $swap_data)) {
                return $value;
            }
        });
        return view('user.swap.index', compact('assets', 'list_currencies', 'swap_assets', 'swap_data'));
    }


    // تابع سواپ کردن
    public function store(Request $request)
    {
        $request->validate([
            'source_unit' => 'required',
            'des_unit' => 'required',
            'source_amount' => 'required',
        ]);
        try {
            DB::beginTransaction();
            $source_unit = $request->source_unit;
            $des_unit = $request->des_unit;
            $source_amount = $source_unit == 'rls' ? unit_to_rial($request->source_amount) : $request->source_amount;
            $source_asset = \auth()->user()->assets()->where('unit', $source_unit)->where('amount', '>=', $source_amount * 1)->first();
            if (!$source_unit) {
                return response()->json(['status' => 500, 'msg' => 'موجودی شما کافی نمیباشد']);
            }
            $fee = BaseData::query()
                ->where('type', "manage_swap_{$source_unit}")
                ->where(function ($q) use ($source_amount) {
                    $q
                        ->where(function ($q) use ($source_amount) {
                            $q
                                ->where(DB::raw("extra_field1 * 1"), '<=', $source_amount)
                                ->where(DB::raw("extra_field2 * 1"), '>', $source_amount);
                        })
                        ->orWhere(function ($q) use ($source_amount) {
                            $q
                                ->where(DB::raw("extra_field1 * 1"), '<=', $source_amount)
                                ->where(DB::raw("extra_field2 * 1"), -1);
                        });
                })
                ->first(['extra_field3']);
            if (!$fee) {
                return response()->json(['status' => 500, 'msg' => 'در حال حاضر امکان سواپ برای ارز مورد نظر وجود ندارد']);
            }
            $fee = $fee->extra_field3 * 1;
            $swap_amount = $source_amount - $fee;
            $list_currencies = get_specific_currencies("$source_unit,$des_unit,usdt");
//            dd($list_currencies);
            $des_amount = convert_currency($source_unit, $des_unit, $swap_amount, false, $list_currencies);
            $des_amount = $des_unit == 'rls' ? unit_to_rial($des_amount) : $des_amount;
            $des_asset = \auth()->user()->assets()->where('unit', $des_unit)->first();
            $des_asset->amount = $des_asset->amount + $des_amount;
            if ($des_asset->save()) {
                $source_asset->amount = $source_asset->amount - $source_amount;
                $source_asset->save();
                $type_swap = BaseData::where('type', 'transactions')->where('name', 'swap')->value('id');

                // Source Asset Log
                $source_asset->transaction()->create([
                    'user_id' => \auth()->user()->id,
                    'transact_type' => $type_swap,
                    'amount' => $source_amount,
                    'type' => 1,
                    'unit' => $source_unit,
                    'theter_price' => $list_currencies[strtoupper($source_unit)]['price'],
                    'theter_rls' => 'USDT',
                    'extra_field1' => json_encode(['fee' => $fee],true),
                    'description' => "مبلغ " . rial_to_unit($source_amount, $source_unit, true) . " بابت سواپ از شما کسر گردید. " . " کارمزد سواپ: " . rial_to_unit($fee, $source_unit, true)
                ]);

                //Des Asset Log
                $des_asset->transaction()->create([
                    'user_id' => \auth()->user()->id,
                    'transact_type' => $type_swap,
                    'amount' => $des_amount,
                    'type' => 2,
                    'unit' => $des_unit,
                    'theter_price' => $list_currencies[strtoupper($des_unit)]['price'],
                    'theter_rls' => 'USDT',
                    'description' => "مبلغ " . rial_to_unit($des_amount, $des_unit, true) . " از سواپ به حساب شما واریز گردید. " . " کارمزد سواپ: " . rial_to_unit($fee, $source_unit, true)
                ]);
                DB::commit();
                return response()->json(['status' => 200, 'msg' => 'عملیات با موفقیت انجام شد!', 'refresh' => true]);
            } else {
                DB::rollBack();
                return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود آمده است با مدیریت در ارتباط باشید']);
            }
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 500, 'msg' => 'مشکلی بوجود آمده است با مدیریت در ارتباط باشیدssss']);
        }

    }

}
