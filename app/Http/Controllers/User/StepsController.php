<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Mail\StepMail;
use App\Models\Admin;
use App\Models\AuthForm;
use App\Models\Bank;
use App\Models\City;
use App\Models\Country;
use App\Models\Notification;
use App\Models\User;
use App\Models\UserAuthForm;
use App\Rules\JalaliDataFormat;
use App\Rules\NationalCodeFormat;
use App\Rules\ShebaFormat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use function Symfony\Component\Translation\t;

class StepsController extends Controller
{
    public function mobile()
    {
        $result = [];
        $countries = Country::all();
        $user = auth()->user();
        if ($user->step == 0 && !empty($user->verification) && $user->time_verification > Carbon::now()) {
            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
            $result['sms']['time'] = $seconds;
            $result['sms']['user'] = $user;
        }
        return view('user.steps.mobile', compact('countries', 'result', 'user'));
    }

    public function sendVerify(Request $request)
    {
        if ($request->mode == 1) {
            $request['country'] = 'IR';
            $request->validate([
                'number' => 'required|numeric',
                'country' => 'required',
            ]);
            $user = auth()->user();
            if ($user->time_verification >= Carbon::now()) {
                $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
                return response()->json(['status' => 300, 'msg' => 'کد یک بار برای شما ارسال شده', 'time' => $seconds]);
            }
            $country = Country::where('code', $request->country)->first();
            $number = '+' . $country->phonecode . $request->number;
            $code = rand(10000, 99999);
            try {
                $user = auth()->user();
                $user->mobile = $request->number;
                $user->verification = $code;
                $user->time_verification = Carbon::now()->addMinutes(1);
                $user->save();
                smsVerify($code, $number, 'verify');

                return response()->json(['status' => 100, 'msg' => 'کد برای شماره موبایل مورد نظر ارسال گردید', 'time' => 60]);
            } catch (\Exception $e) {
                return response()->json(['status' => 500, 'msg' => 'خطا در ارسال']);
            }
        } elseif ($request->mode == 2) {
            $request->validate([
                'code' => 'required|numeric',
            ]);
            $user = auth()->user();
            if ($user->time_verification > Carbon::now()) {
                if ($user->verification == $request->code) {
                    $user->step = 1;
                    $user->step_complate = 1;
                    $user->reject_reason = '';
                    $user->status = 0;
                    $user->save();
                    AuthForm::where('type', 'step.verify-mobile')->first()->userAuthForms()->create([
                        'user_id' => auth()->user()->id,
                        'status' => 2
                    ]);
                    return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'refresh' => true]);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'کد اشتباه است']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'کد منقضی شده است']);
            }
        }
    }

    public function email()
    {
        $user = auth()->user();
        $countries = Country::all();
        return view('user.steps.email', compact('user', 'countries', 'user'));
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'email' => 'required|exists:users,email'
        ]);
        $token = Str::random(100);
        $link = route('step.verify-token.email', $token);
        $user = auth()->user();
        $user->verification = $token;
        $user->time_verification = Carbon::now()->addMinutes(15);
        $user->save();
        Mail::to($user->email)->send(new StepMail($link));
        return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'refresh' => true]);
    }

    public function verifyEmail($token)
    {
        $user = User::where('verification', $token)->firstOrFail();
        if ($user->time_verification > Carbon::now()) {
            if ($user->verification == $token) {
                $user->step = 2;
                $user->step_complate = 2;
                $user->reject_reason = '';
                $user->verification = null;
                $user->save();
                AuthForm::where('type', 'step.verify-email')->first()->userAuthForms()->create([
                    'user_id' => $user->id,
                    'status' => 2
                ]);
                return redirect()->route('login.form')->with('flash', ['type' => 'success', 'msg' => 'ایمیل با موفقیت تایید شد', 'refresh' => true]);
            } else {
                return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'لینک اشتباه است']);
            }
        } else {
            return redirect()->route('login.form')->with('flash', ['type' => 'danger', 'msg' => 'لینک منقضی شده است']);
        }
    }

    public function information()
    {
        $user = auth()->user();
        $states = City::where('parent', 0)->get();
        $city = [];
        $images = [
            'national' => $user->files()->where('type', 'national')->orderBy('id', 'desc')->first(),
            'phone_bill' => $user->files()->where('type', 'phone_bill')->orderBy('id', 'desc')->first(),
        ];

        if (!empty($user->city)) {
            $city = City::where('parent', $user->state)->get();
        }
        return view('user.steps.information', compact('states', 'user', 'city', 'images'));
    }

    public function informationStore(Request $request)
    {
//        dd($request->all(),$request->user['nationality']);
        if ($request->user['nationality']){
            $request->validate([
                'user.*' => 'required',
                'user.birth_day' => [new JalaliDataFormat()],
                'user.national_code' => 'required|min:12|max:12',
                'user.state' => 'exists:cities,id',
                'user.city' => 'exists:cities,id',
                'user.gender_id' => 'in:1,2',
                'user.nationality' => 'in:0,1',
                'user.phone' => 'numeric',
                'user.postal_code' => 'numeric',
            ]);
        }else{
            $request->validate([
                'user.*' => 'required',
                'user.birth_day' => [new JalaliDataFormat()],
                'user.national_code' => [new NationalCodeFormat()],
                'user.state' => 'exists:cities,id',
                'user.city' => 'exists:cities,id',
                'user.gender_id' => 'in:1,2',
                'user.nationality' => 'in:0,1',
                'user.phone' => 'numeric',
                'user.postal_code' => 'numeric',
            ]);
        }

        try {
            $model = User::where('id', auth()->user()->id)->update(array_merge($request->user, ['step' => 3, 'reject_reason' => '', 'status' => 0]));
            AuthForm::where('type', 'step.information')->first()->userAuthForms()->create([
                'user_id' => auth()->user()->id,
                'status' => 1 // ho3in
            ]);
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'refresh' => true]);
            return response()->json(['status' => 500, 'msg' => 'خطا در ثبت']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
        }

    }

    public function bank()
    {
        $user = auth()->user();
        $bank = Bank::where('user_id', $user->id)->with('files')->first();
        return view('user.steps.bank_information', compact('user', 'bank'));
    }

    public function bankStore(Request $request)
    {
        $request['sheba'] = strpos($request['sheba'], 'IR') == false ? 'IR' . $request['sheba'] : $request['sheba'];
        $request->validate([
            'name_bank' => ['required'],
            'card' => ['required', 'numeric'],
            'sheba' => [new ShebaFormat()],
//            'image' => [Rule::requiredIf(function () {
//                $user = Bank::where('user_id', auth()->user()->id)->first();
//                if (!$user)
//                    return false;
//                $image = $user->files()->orderBy('id', 'desc')->first()->path ?? null;
//                return (!$image) ? true : false;
//            }), 'mimes:png,jpg', 'max:1024'],
        ]);
        try {
            $user = auth()->user();
            $model = Bank::updateOrCreate([
                'user_id' => $user->id,
            ], [
                'user_id' => $user->id,
                'name' => $request->name_bank,
                'account_number' => $request->account_number,
                'sheba_number' => $request->sheba,
                'card_number' => $request->card
            ]);
            $user->step = 4;
            $user->reject_reason = '';
            $user->status = 0;
            $user->save();
            AuthForm::where('type', 'step.bank')->first()->userAuthForms()->create([
                'user_id' => auth()->user()->id,
                'status' => 1 // ho3in
            ]);
            if ($request->has('image')) {
                $model->files()->create([
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image'), 'bank'),
                ]);
            }
            if ($model)
                return response()->json(['status' => 100, 'msg' => 'عملیات با موفقیت انجام گردید', 'refresh' => true]);
            return response()->json(['status' => 500, 'msg' => 'خطا در ثبت']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
        }

    }

    public function uploadDocuments()
    {
        return view('user.steps.upload_documents');
    }

    public function uploadDocumentsStore(Request $request)
    {
        $request->validate([
            'image.selfie_image' => ['required', 'mimes:png,jpg', 'max:5024'],
            'image.national_image' => ['required', 'mimes:png,jpg', 'max:5024'],
        ]);
        try {
            $image = [
                [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.national_image'), 'information'),
                    'type' => 'national'
                ],
                [
                    'user_id' => auth()->user()->id,
                    'path' => upload($request->file('image.selfie_image'), 'information'),
                    'type' => 'selfie_image'
                ]
            ];

            User::find(auth()->user()->id)->files()->createMany($image);
            AuthForm::where('type', 'step.upload')->first()->userAuthForms()->create([
                'user_id' => auth()->user()->id,
                'status' => 1 // ho3in
            ]);
            return response()->json(['status' => 100, 'msg' => 'مدارک با موفقیت بارگذاری شد']);
        } catch (\Exception $exception) {
            return response()->json(['status' => 500, 'msg' => 'مشکلی پیش آمده است لطفا با پشتیبانی سایت در ارتباط باشید']);
        }
    }

    public function finish()
    {
        $user = auth()->user();
//        $admin = Admin::query()->first();
        /*$message = 'احراز هویت' . $user->fullname . 'پایان یافت';
        $notif_array=[
            'type' => 'authenticate',
            'refer_id' => $user->userGuard->id,
            'user_id' => $admin->userGuard->id,
            'details' => $message
        ];
        insert_notif($notif_array);*/

        return view('user.steps.finish', compact('user'));
    }

}
