<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\NotificationView;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    public function clear_all(): \Illuminate\Http\JsonResponse
    {
        auth()->user()->notifications()->delete();
        return response()->json(['status' => 100, 'msg' => 'نوتیفیکیشن ها با موفقیت پاک شدند']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index()
    {
        $user = getCurrentUser();
        $notifications = \App\Models\Notification::select([
            '*',
            'viewed'=>NotificationView::select('id')->whereColumn('notifications.id','notification_views.notification_id')->limit(1)
        ])->    where('user_id', $user->user_guard_id)->whereDoesntHave('notification_view')
            ->orWhere('user_id', 0)->whereDoesntHave('notification_view')
            ->orderBy('id','desc')->paginate(20);
        return view('user.notifications.index', compact('notifications'));
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        //
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    /*public function store(Request $request)
    {
        //
    }*/

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*public function show($id)
    {
        $id = base64_decode($id);
        $user = getCurrentUser();
        $notification = \App\Models\Notification::where('id', $id)->where('user_id', $user->user_guard_id)
            ->orWhere('user_id', 0)->where('id', $id)
            ->firstOrFail();
        $this->check_view($notification, $user);
        return view('user.notifications.form', compact('notification'));
    }*/


    /*private function check_view($data, $user)
    {
        if (!$data->notification_view) {
            $data->notification_view()->create(['user_id' => $user->user_guard_id]);
        }
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        //
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {
        //
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        //
    }*/
}
