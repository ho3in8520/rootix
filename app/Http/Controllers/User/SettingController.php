<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PragmaRX\Google2FALaravel\Support\Authenticator;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:user_setting_menu'])->only('form');
        $this->middleware(['role:'.name_roles_string_middleware(),'permission:authenticate_active|authenticate_deactive'])->only('status_google_authenticator');
    }
    public function form(Request $request)
    {
        $user = auth()->user();
        if (!empty($user->verification) && $user->time_verification > Carbon::now())
            $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
        else
            $seconds = 0;
        if (empty($user->google2fa_secret)) {
            // Initialise the 2FA class
            $google2fa = app('pragmarx.google2fa');

            // Save the registration data in an array
            $registration_data = $request->all();

            // Add the secret key to the registration data
            $registration_data["google2fa_secret"] = $google2fa->generateSecretKey();


            // Save the registration data to the user session for just the next request
            $request->session()->flash('registration_data', $registration_data);

//         Generate the QR image. This is the image the user will scan with their app
//         to set up two factor authentication
            $QR_Image = $google2fa->getQRCodeInline(
                config('app.name'),
                $user->email,
                $registration_data['google2fa_secret']
            );
            return view('user.settings.setting', ['QR_Image' => $QR_Image, 'secret' => $registration_data['google2fa_secret'], 'user' => $user, 'time' => $seconds]);
        }
        return view('user.settings.setting', ['user' => $user, 'time' => $seconds]);
    }

    // تغغیر روش تایید برای کاربر
    public function change_confirmation_type(Request $request)
    {
        if ($request->ajax()) {
            $request->validate(['confirm_type' => 'required', 'in:sms,google-authenticator']);
            try {
                $user = auth()->user();
                if ($request->confirm_type == $user->confirm_type)
                    return response()->json(['status' => 300]);
                switch ($request->confirm_type) {
                    case 'sms':
                        if (empty($user->google2fa_secret))
                            return response()->json(['status' => 300, 'msg' => 'ابتدا از فعالسازی google authenticator خود اطمینان حاصل کنید']);

                        break;
                    case 'google-authenticator':
                        if (empty($user->google2fa_secret))
                            return response()->json(['status' => 300, 'msg' => 'ابتدا از فعالسازی google authenticator خود اطمینان حاصل کنید','refresh'=>true]);

                        if ($user->mobile) {
                            if ($user->time_verification >= Carbon::now()) {
                                $seconds = Carbon::parse($user->time_verification)->diffInSeconds(Carbon::now());
                                return response()->json(['status' => 200, 'msg' => 'کد یک بار برای شما ارسال شده', 'time' => $seconds]);
                            }
                            $code = rand(10000, 99999);
                            $second = 60;
                            $user->verification = $code;
                            if (is_null(smsVerify($code, $user->mobile, 'verify'))) {
                                $user->time_verification = Carbon::now()->addSecond($second);
                                $user->save();
                                return response()->json(['status' => 200, 'msg' => 'کد تایید با موفقیت برای شما ارسال شد', 'time' => $second]);
                            } else
                                return response()->json(['status' => 500, 'msg' => 'ارسال کد تایید موفقیت آمیز نبود با پشتیبانی سایت در ارتباط باشید']);
                        } else {
                            return response()->json(['status' => 500, 'msg' => 'شماره موبایل شما ثبت نشده است']);
                        }
                        break;
                    default:

                        break;
                }
            } catch (\Exception $exception) {

            }
        }

    }

    // تایید تغییر روش تایید
    public function verify_confirmation_type(Request $request)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $request->validate([
                'confirm_type' => ['required', 'in:sms,google-authenticator'],
                'one_time_password' => ['required', 'numeric'],
            ]);
            try {
                $user = auth()->user();
                if ($request->confirm_type == $user->confirm_type)
                    return response()->json(['status' => 300]);

                switch ($request->confirm_type) {
                    case 'sms':
                        if (getCurrentOtp($request->one_time_password) == true) {
                            $user->confirm_type = $request->confirm_type;
                            $user->save();
                            return response()->json(['status' => 100, 'msg' => 'روش تایید شما با موفقیت تغییر کرد!','refresh'=>true]);
                        }
                        return response()->json(['status' => 500, 'msg' => 'کد تایید اتنتیکاتور اشتباه می‌باشد']);
                        break;
                    case 'google-authenticator':
                        if ($user->time_verification < Carbon::now()) {
                            return response()->json(['status' => 300, 'msg' => 'کد تایید شما منقضی شده است','refresh'=>true]);
                        }
                        if ($user->verification == $request->one_time_password) {
                            $user->verification = '';
                            $user->time_verification = '';
                            $user->confirm_type = $request->confirm_type;
                            $user->save();
                            return response()->json(['status' => 100, 'msg' => 'روش تایید شما با موفقیت تغییر کرد!','refresh'=>true]);
                        }
                        return response()->json(['status' => 300, 'msg' => 'کد تایید اشتباه می باشد!']);
                        break;
                    default:

                        break;
                }
            } catch (\Exception $exception) {

            }

        }
    }

    // فعال یا غیرفعال کردن گوگل اتنتیکاتور
    public function status_google_authenticator(Request $request)
    {
        $request->validate([
            'one_time_password' => 'required',
        ]);
        $user = \auth()->user();
        if ($request->type == 'deactivate') {
            if (getCurrentOtp($request->one_time_password) === true) {
                $user->google2fa_secret = null;
                $user->login_2fa = false;
                $user->save();
                return response()->json(['status' => 200, 'msg' => 'اتنتیکاتور با موفقیت غیرفعال شد','refresh'=>true]);
            } else {
                return response()->json(['status' => 500, 'msg' => 'کد تاییدیه اتنتیکاتور اشتباه است']);
            }
        } else {
            if (empty($user->google2fa_secret)) {
                $user->google2fa_secret = $request->input('secret');
                if (getCurrentOtp($request->one_time_password) === true) {
                    $user->save();
                    return response()->json(['status' => 200, 'msg' => 'اتنتیکاتور با موفقیت ساخته شد', 'refresh'=>true]);
                } else {
                    return response()->json(['status' => 500, 'msg' => 'کد تاییدیه اتنتیکاتور اشتباه است']);
                }
            } else {
                return response()->json(['status' => 500, 'msg' => 'اتنتیکاتور شما قبلا ساخته شده است']);
            }
        }
    }

    // فعال یا غیرفعال کردن ورود دومرحله ای
    public function login_2fa(Request $request)
    {
        $request->validate([
            'one_time_password' => 'required',
        ]);
        $user= auth()->user();
        if (empty($user->google2fa_secret))
            return response()->json(['status' => 500, 'msg' => 'ابتدا گوگل اتنتیکاتور را فعال کنید', 'type' => 'error']);

        if (getCurrentOtp($request->one_time_password) === true) {
            $type = $user->login_2fa == false ? true : false;
            $user->login_2fa = $type;
            $user->save();

            $msg = $type == false ? 'غیرفعال' : 'فعال';
            return response()->json(['status' => 100, 'msg' => "ورود دومرحله ای با موفقیت $msg شد", 'type' => 'success']);
        } else {
            return response()->json(['status' => 500, 'msg' => 'کد تاییدیه اتنتیکاتور اشتباه است', 'type' => 'success']);
        }
    }
}
