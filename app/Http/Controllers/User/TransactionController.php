<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\BaseData;
use App\Models\Finance_transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware( 'permission:user_transaction_menu')->only('index');
    }

    public function index(Request $request)
    {

        $currencies = auth()->user()->assets;
        $currencies = $currencies->pluck('unit');
        $date_from = $request->date_from && !empty($request->date_from) ? jalali_to_gregorian($request->date_from . " 00:00:00") : false;
        $date_to = $request->date_to && !empty($request->date_to) ? jalali_to_gregorian($request->date_to . " 23:59:59") : false;


        $transactions = Finance_transaction::query()->where('user_id', auth()->user()->id);

        in_array($request->type, ['1', '2']) ? $transactions->where('type', $request->type) : '';

        $request->unit ?
            $transactions->whereHasMorph('financeable', [Asset::class], function ($query) use ($request) {
                $query->where('unit', $request->unit);
            }) : '';

        $date_from ? $transactions->where('created_at', '>=', $date_from) : '';
        $date_to ? $transactions->where('created_at', '<', $date_to) : '';
//        $request->amount_from && $request->has('amount_from') && !empty($request->amount_from)? $transactions->where('amount', '>=', $request->amount_from) : '';
        $request->amount_from && $request->has('amount_from') && !empty($request->amount_from) ? $transactions->where('amount', '>=', unit_to_rial($request->amount_from)) : '';
//        $request->amount_to && $request->has('amount_to') && !empty($request->amount_to)? $transactions->where('amount', '<', $request->amount_to) : '';
        $request->amount_to && $request->has('amount_to') && !empty($request->amount_to) ? $transactions->where('amount', '<', unit_to_rial($request->amount_to)) : '';
        $request->destination_address && $request->has('destination_address') && !empty($request->destination_address) ? $transactions->where('extra_field1', '=', $request->destination_address) : '';
        $transactions->orderBy('created_at', 'desc');
        $transactions = $transactions->paginate(10);


        return showData(view('user.transactions.index', compact('transactions', 'currencies')));
    }
}
