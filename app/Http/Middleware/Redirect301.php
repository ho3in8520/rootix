<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Redirect301
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($redirect = negasht($request->path())) {
            return redirect($redirect);
        }
        return $next($request);
    }
}
