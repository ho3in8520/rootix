// $('.select2').select2({
//     dir: 'rtl',
//     width: '100%',
// });

var minutes = 1;
var seconds = minutes * 60;

function convertIntToTime(num) {
    var mins = Math.floor(num / 60);
    var secs = num % 60;
    var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
    return (timerOutput);
}

var countdown = setInterval(function () {
    var current = convertIntToTime(seconds);
    $('timer').html(current);
    if (seconds == 0) {
        clearInterval(countdown);
    }
    seconds--;
    if (seconds >= 0) {
    } else {
        $('timer').hide();
        $('#test').show();
        $('#test').html('<span class="ft-refresh-ccw"</span>');
    }
}, 1000);

$(document).on('focus', ".datePicker", function () {
    $(this).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        isRTL: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy/mm/dd",
        showAnim: 'slideDown',
        showButtonPanel: true,
        yearRange: "-100:+10",
    });
});

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function parent_form_submit(elm) {
    $(elm).parent('form').submit();
}

// تابع برای فراخوانی ajax
function ajax(form, func_name = null, params = {}) {
    let action = form.isObject === true ? form.action : form.attr('action');
    let method = form.isObject === true ? form.method : form.attr('method');
    let data = form.isObject === true ? form.data : form.serialize();
    let button = form.isObject === true && form.button ? form.button : null; // حتما باید خود باتن پاس داده بشه نه کلاسش دادا

    let button_text = '';
    if (button) {
        button_text = button.text();
        button.text('درحال ارسال...').attr('disabled');
    }

    $('.help-block').remove();
    $('span[class*="error-"]').text('');

    $.ajax({
        url: httpToHttps(action),
        type: method,
        data: data,
        success: function (response) {
            if (button != null) {
                button.text(button_text).removeAttr('disabled');
            }
            // اگه برای برگشت از ajax خواستیم یه تابعی رو اجرا کنیم
            if (func_name != null)
                func_name(response, params);
            if (response.msg) { //
                if (response.status == 200 || response.status == 100)
                    swal('موفق!', response['msg'], 'success').then(function () {
                        response.refresh === true ? location.reload() : '';
                    });
                else if (response.status == 500)
                    swal('خطا!', response['msg'], 'error');
                else if (response.status == 300)
                    swal('خطا!', response['msg'], 'warning');
            }
        },
        error: function (xhr) {
            if (button != null) {
                button.text(button_text).removeAttr('disabled');
            }
            if (func_name != null)
                func_name(xhr, params);
            errorForms(xhr);
        }
    });
}

// تایمر شمارش معکوس
function timer_count(time, class_name, func_after) {
    if (time > 0) {
        let timer = setInterval(function () {
            if (time > 0) {
                $(class_name).text(--time);
            } else {
                clearInterval(timer);
                if (func_after != null) {
                    func_after();
                }
            }
        }, 1000);
    }

}

var Base64 = {
    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },
    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },
    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}

function swalResponse(status, msg, title = '') {
    switch (status) {
        case 100:
            swal(title, msg, 'success')
            break;
        case 300:
            swal(title, msg, 'warning')
            break;
        case 500:
            swal(title, msg, 'error')
            break;
    }
}

// تابع اختصاصی json parse
function json_parse(json) {
    return JSON.parse(json.replaceAll('&quot;', '"'));
}

//////////// تابع تبدیل اعداد فارسی به انگلیسی //////
function toEnglishNumber(strNum, elm) {
    var pn = ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"];
    var en = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    var cache = strNum;
    for (var i = 0; i < 10; i++) {
        var regex_fa = new RegExp(pn[i], 'g');
        cache = cache.replace(regex_fa, en[i]);
    }
    $(elm).val(cache);
}

$("input[type=number],input[data-type=number]").keyup(function () {
    toEnglishNumber($(this).val(), $(this));
})

///////////////////////////////////////////

function rial_to_unit(amount, unit, has_number_format = false, has_post_fix = true) {
    amount = de_numberFormat(amount);
    unit = unit.toLowerCase();

    let post_fix = get_unit(unit);
    if (unit === 'rial' || unit === 'rls')
        amount /= 10;
    amount = parseFloat(amount).toFixed(8).replace(/\.?0+$/, "");

    if (has_number_format)
        amount = numberFormat(amount);
    if (has_number_format && has_post_fix)
        amount += ' ' + post_fix;

    return amount
}

function unit_to_rial(amount, mode = false) {
    amount = de_numberFormat(amount);
    amount *= 10;

    return mode === true ? numberFormat(amount) + ' ریال' : amount;
}

function get_unit(unit = 'rial', lang = 'fa') {
    if (unit === 'rial' || unit === 'rls') {
        return (lang == 'fa') ? 'تومان' : 'Toman'
    } else {
        return unit;
    }
}


// تبدیل ارزها بهمدیگر
/*
-- آرگومان اول: ارایه مبلغ ارزها بر اساس usdt
-- آرگومان دوم: واحد ارز مبدا
-- آرگومان سوم: واحد ارز مقصد
-- آرگومان چهارم: مقدار ارز مبدا
 */
function swap(list_currencies, source_unit, des_unit, source_amount, has_number_format = false, has_post_fix = true) {
    source_unit = source_unit.toUpperCase();
    des_unit = des_unit.toUpperCase();

    if (source_amount <= 0 || source_unit === des_unit || list_currencies[source_unit] === undefined)
        return false;

    let source_usdt_amount = list_currencies[source_unit]['price'];
    let des_usdt_amount = list_currencies[des_unit]['price'];

    let source_btc_amount = list_currencies[source_unit]['price_btc'];

    let source_rls_amount = list_currencies[source_unit]['rls'];

    if (des_unit === 'USDT')
        return rial_to_unit(source_usdt_amount * source_amount, des_unit, has_number_format, has_post_fix);
    if (des_unit === 'BTC')
        return rial_to_unit(source_btc_amount * source_amount, des_unit, has_number_format, has_post_fix);
    if (des_unit === 'RLS' || des_unit === 'RIAL')
        return rial_to_unit(source_rls_amount * source_amount, des_unit, has_number_format, has_post_fix);

    if (list_currencies[des_unit] === undefined)
        return false;

    return rial_to_unit(source_usdt_amount * source_amount / des_usdt_amount, des_unit, has_number_format, has_post_fix);
}

function de_numberFormat(amount) {
    return typeof amount === 'string' ? amount.replace(/\,/g, '') : amount;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

function httpToHttps(url, https = true) {
    if (url.indexOf('https') === -1 && https) {
        return url.replace('http', 'https');
    }
    return url;
}
