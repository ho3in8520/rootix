classNameErrorSelect2 = "select2-selection--single";
classNameErrorSelect2Multiple = "select2-selection--multiple";
TimeOutActionNextStore = 2000;
buttonLoadingText = 'please wait....';
buttonTextSweetAlert = "success";
textConfirmDelete = "آیا از حذف این مورد اطمینان دارید ؟";
buttonTextConfirm = "بلی";
buttonTextCancel = "خیر";
elemClicked = null;
//for reset modal content and sections
objectId = "";
url = "";
messageMode = "swal";

///////////////////////////////////////////////////////////////////////////////
// باتن کپی کردن
$(".copy-btn").on('click', function () {
    let elm = document.getElementById($(this).data('input'));
    if (copyToClipboard(elm) == true) {
        swal('متن مورد نظر با موفقیت کپی شد', '', 'success');
    }
});

// تابع کپی کردن
function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

///////////////////////////////////////////////////////////////////////////////

$(':input').on('focus', function () {
    $(this).attr('autocomplete', 'off');
});

function resetFormInput(elementClick) {
    $(elementClick).closest('form').find('input[type=text],input[type=number],input[type=password],textarea,.hiddenEmpty').val("");
    $(elementClick).closest('form').find('select').val("").trigger("change");
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function startConfig(showProgress = false) {
    $('.help-block').remove();
    $('span[class*="error-"]').text('');
    $("." + classNameErrorSelect2).attr("style", normalCss);
    if ($(document).hasClass("g-recaptcha-response")) {
        $("#g-recaptcha-response").addClass('d-none')
    }
    $("#progressBar").css('width', 0 + '%');
    $("#progressBar").css('background-color', '#3399ff');
    // $("input[type=text],select,textarea,input[type=password]").attr("style", normalCss);
    (showProgress) ? $("#progressShow").show() : false;

}

function ajaxStore(e) {
    e.preventDefault();
    var elementClick = $(this);
    var reloadPage = $(this).data('reload');
    var callback = $(this).data('callback');
    if ($(elementClick).attr("data-massage-mode") != undefined)
        messageMode = $(elementClick).attr("data-massage-mode");
    startConfig();
    var captionButton = $(elementClick).html();
    $(elementClick).html(' <i class="fa fa-circle-o-notch fa-spin"></i>' + buttonLoadingText);
    if (typeof CKEDITOR != 'undefined') {
        for (instance in CKEDITOR.instances)
            CKEDITOR.instances[instance].updateElement();
    }
    var data = new FormData($(elementClick).closest('form')[0]);
    // var data=$(elementClick).closest('form').serialize()
    // data.append('status', $(elementClick).val());
    $.ajax({
        url: httpToHttps($(elementClick).closest('form').attr('action')),
        type: $(elementClick).closest('form').attr('method'),
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            $(elementClick).html(captionButton)
            resultResponse(response, reloadPage);
            if (elementClick.hasClass('reset-form')) {
                if (objectId != "")
                    getHttpRequestForModal(url, {book_id: objectId}, {size: 'modal-xl'});

            }
            $(function () {
                afterSave();
            });
        },
        error: function (xhr) {
            $(".ajaxMessage").html("");
            $(elementClick).html(captionButton)
            defaultProgress();
            errorForms(xhr);
        },
        complete: function (data) {

        }
    });
}

///////////////////////////////////////////////////////////////////////////////result response
function resultResponse(result, reloadPage = true) {
    switch (parseInt(result.status)) {
        case 100:
            $("#progressBar").css('background-color', "green");
            if ("msg" in result && result.msg != "")
                alertMessage('success', result.msg, messageMode);
            if ("url" in result) {
                setTimeout(function () {
                    window.location.href = result.url;
                }, ("timer" in result) ? result.timer : TimeOutActionNextStore);
            } else if ("data" in result) {
                $(".showHtml").html(result.data)
            } else if (reloadPage) {
                setTimeout(function () {
                    window.location.reload();
                }, ("timer" in result) ? result.timer : TimeOutActionNextStore);
            }
            break;
        case 200:
            $(".message-end-warranty").html(result.msg);
            $(".show-message").slideDown(1000);
            break;
        case 300:
            alertMessage('alert', result.msg, messageMode)
            if ("url" in result) {
                setTimeout(function () {
                    window.location.href = result.url;
                }, TimeOutActionNextStore);
            }
            break;
        case 422:
            alertMessage('error', result.msg, messageMode);
            defaultProgress();
            break;
        case 403:
            alertMessage('warning', result.msg, messageMode);
            break;
        case 500:
            alertMessage('error', result.msg, messageMode);
            defaultProgress();
            break;
    }

}

///////////////////////////////////////////////////////////////////////////////show message
function alertMessage(typeMessage, message, mode = "swal") {
    if (mode == "swal") {
        var messageShow = "";
        switch (typeMessage) {
            case 'success' :
                swal({
                    title: "",
                    text: message,
                    icon: "success",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'error' :
                swal({
                    title: "",
                    text: message,
                    icon: "error",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'warning':
                swal({
                    title: "",
                    text: message,
                    icon: "warinig",
                    button: buttonTextSweetAlert,
                });
                break;
            case 'alert':
                alert('please wait...')
                break;
        }
        $(".ajaxMessage").html(messageShow);
    } else {
        switch (typeMessage) {
            case 'success' :
                toastr.success(message, "موفق", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
            case "error":
                toastr.error(message, "خطا", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
            case "warning":
                toastr.warning(message, "هشدار", {
                    showMethod: "slideDown",
                    hideMethod: "slideUp",
                    timeOut: TimeOutActionNextStore
                });
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////default progrees
function defaultProgress() {
    $("#progressBar").css('width', '0');
}

/////////////////////////////////////////////////////////////////////////////////error validation form
function errorForms(xhr) {
    if (xhr.status === 422) {
        alertMessage('error', 'لطفا اطلاعات فرم را درست وارد کید');
        var errorsValidation = JSON.parse(xhr.responseText).errors;
        $.each(errorsValidation, function (key, value) {
            var errArray = key.split('.');
            var customTagError = $(document).find(".error-" + key)

            var getElement = "";
            var errorHtml = "<div class='help-block text-danger'>" + value + "</div>";
            if ($(document).find(".error-" + key).length >= 1) {
                $(customTagError).show().text(value)
            } else if (typeof errArray[1] === 'undefined') {
                getElement = "[name='" + key + "']";
                showHtmlValidation(getElement, errorHtml)
                getElement = "[name='" + key + "[]" + "']";
                showHtmlValidation(getElement, errorHtml)
            } else if (!$.isNumeric(errArray[1])) {
                getElement = "[name='" + (errArray[0] + "[" + errArray[1] + "]']");
                showHtmlValidation(getElement, errorHtml)
            } else if ($.isNumeric(errArray[1])) {
                getElement = "[name^='" + errArray[0] + "']";
                showHtmlValidation(getElement, errorHtml, errArray[1])
            }
        });
    } else
        swal('خطا!', 'مشکلی بوجود آمده است لطفا مجددا تلاش کنید', 'error');
}

////////////////////////////////////////////////////////////////////////////////show html error validation
function showHtmlValidation(getElement, errorHtml, indexArr = "") {
    var element = $(getElement);
    if (indexArr != "") {
        element = element.eq(indexArr);
    }
    if ($(element).hasClass("g-recaptcha-response")) {
        $("#g-recaptcha-response").addClass('d-none')
    }
    if ($(element).closest(".input-group").length)
        element = element.closest(".input-group");

    if (!$(getElement).parent().hasClass('select2'))
        if ($(getElement).parent().find('.' + classNameErrorSelect2Multiple).length)
            element = element.parent().find('.' + classNameErrorSelect2Multiple);
        else if ($(getElement).parent().find('.' + classNameErrorSelect2).length)
            element = element.parent().find('.' + classNameErrorSelect2);
    // element.attr("style", errorCss);
    element.after(errorHtml);

    /*if (!$(getElement).hasClass('select2')) {
        if (indexArr != "") {
            //indexArr++;
            if ($(getElement).eq(indexArr).closest(".input-group").length) {
                $(getElement).eq(indexArr).closest(".input-group").after(errorHtml);
                $(getElement).eq(indexArr).closest(".input-group").attr("style", errorCss)
            } else {
                $(getElement).eq(indexArr).after(errorHtml);
                $(getElement).eq(indexArr).attr("style", errorCss)
            }
        } else {
            if ($(getElement).closest(".input-group").length) {
                $(getElement).closest(".input-group").after(errorHtml);
                $(getElement).closest(".input-group").attr("style", errorCss)
            } else {
                $(getElement).after(errorHtml);
                $(getElement).attr("style", errorCss)
            }
        }
    } else {
        if ($(getElement).eq(indexArr).closest(".input-group").length) {
            $(getElement).eq(indexArr).closest(".input-group").after(errorHtml);
            $(getElement).eq(indexArr).closest(".input-group").attr("style", errorCss)
        } else {
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2Multiple).attr("style", errorCss)
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2Multiple).after(errorHtml);
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2).attr("style", errorCss)
            $(getElement).eq(indexArr).parent().find('.' + classNameErrorSelect2).after(errorHtml);
        }
    }*/
}

$(document).on('click', '.timeOutChangePage', function (e) {
    e.preventDefault();
    var hrefLink = $(this).attr('href');
    alertMessage('alert', 'please wait...');
    setTimeout(function () {
        window.location.href = hrefLink;
    }, TimeOutActionNextStore);

});

////////////////////////////////////////////////////////////////////////delete ajax
function ajaxDelete(url, elementDelete) {
    swal({
        title: textConfirmDelete,
        text: "",
        icon: "warning",
        buttons: buttonTextConfirm,
    })
        .then((willDelete) => {
            if (willDelete) {
                var id = elementDelete.closest('tr').attr('data-id');
                var path = url.replace('?', id);
                $.ajax({
                    'url': path,
                    type: 'DELETE',
                    success(response) {
                        swal(response.msg, {
                            icon: "success",
                        });
                        elementDelete.closest('tr').slideUp(1000)
                    }
                });

            }
        });
};

///////////////////////////////////////////////////////////////////////ajax store use
$(document).on('click', '.ajaxStore', ajaxStore);

////////////////////////////////////////////////////////////////////////http request and get response and put in modal
function getHttpRequestForModal(url, parameter = {}, modalConfig = {}) {
    modalSize = modalConfig.hasOwnProperty('size') ? modalConfig.size : 'modal-lg';
    modalHeader = modalConfig.hasOwnProperty('header') ? modalConfig.header : '';
    createModal({header: modalHeader, size: modalSize});
    $.get(url, parameter, function (response) {
        if (parseInt(response.status) == 100) {
            $(".modal-body").html(response.htmlForModal);
            if ($("#myModal .modal-body").find('.ck-editor').length) {
                CKEDITOR.replace($('.ck-editor').attr('name'), {
                    filebrowserUploadUrl: '/upload_ck'
                });
            }
            $('#myModal').modal({backdrop: 'static', keyboard: false});

            setTimeout(function () {
                $('#myModal').find('.select2').select2()
            }, 1000)
        }

    });


}

/////////////////////////////////////////////////////////////////////////////////modal creator
function createModal(option = {}) {
    modalSize = option.hasOwnProperty('size') ? option.size : 'modal-sm';
    modalHeader = option.hasOwnProperty('header') ? option.header : '';

    if ($("#myModal").length == 0) {
        var modalHtml = '  <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">\n' +
            '        <div class="modal-dialog ' + option.size + '">\n' +
            '            <div class="modal-content">\n' +
            '                <div class="modal-header">\n' +
            '                    <h4 class="modal-title">' + option.header + '</h4>\n' +
            '                </div>\n' +
            '                <div class="modal-body">\n' +
            '                </div>\n' +
            '               <div class="modal-footer">\n' +
            '                  <button type="button" class="btn btn-danger" data-dismiss="modal">انصراف</button>\n' +
            '               </div>' +
            '            </div>\n' +
            '        </div>\n' +
            '    </div>';
        $("#createModal").html(modalHtml);
        $("#myModal").modal('toggle');
    } else {
        $("#myModal").find('.modal-dialog').addClass(modalSize);
        $("#myModal").find('.modal-header').html(modalHeader);
        if (!$("#myModal").hasClass("show")) {
            $("#myModal").modal('show');
        }

    }

}

/////////////////////////////////////////////////////////////////////////////////css validation form and create div show error
var errorCss = "border-width: 1px; border-color: #d2322d !important; border-style: solid";
var normalCss = "border-width: 1px; border-color: #75787D !important; border-style: solid";

//////////////////////////////////////////////////////////////////////////////// empty content modal next hide modal
$(document).on('hidden.bs.modal', '#myModal', function () {
    $('#myModal .modal-body').html("")
});


$(document).on('keyup', '.number-format', function () {
    var elementKeyUp = $(this);
    elementKeyUp.val(numberFormat(elementKeyUp.val()))

});

function numberFormat(Number) {
    // Number = parseFloat(Number).toFixed(8).replace(/\.?0+$/,"");
    Number += '';
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    Number = Number.replace(',', '');
    x = Number.split('.');
    y = x[0];
    z = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(y))
        y = y.replace(rgx, '$1' + ',' + '$2');
    // return y + z;
    return y + z;
}

$(document).on("click", "a[data-confirm]", function (e) {
    e.preventDefault();
    var elem = $(this);

    swal({
        title: $(elem).data('confirm'),
        text: $(elem).data('text'),
        icon: 'warning',
        buttons: {
            confirm: buttonTextConfirm,
            cancel: buttonTextCancel,
        },
        dangerMode: true
    })
        .then((result) => {
            if (result) {
                if ($(elem).data('method')) {
                    var options = {
                        'action': $(elem).data('action'),
                        'target': '_self'
                    };
                    if ($(elem).data('method').toLowerCase() != "delete")
                        $.extend(options, {
                            method: $(elem).data('method')
                        });
                    else
                        $.extend(options, {
                            method: "post"
                        });

                    var form = $('<form>', options);
                    if ($(elem).data('method').toLowerCase() == "delete") {
                        form.append($('<input>', {
                            'name': '_method',
                            'value': "delete",
                            'type': 'hidden'
                        }));
                    }
                    form.append($('<input>', {
                        'name': '_token',
                        'value': $('meta[name=csrf-token]').attr('content'),
                        'type': 'hidden'
                    }));
                    if ($(elem).data('id')) {
                        form.append($('<input>', {
                            'name': 'id',
                            'value': $(elem).data('id'),
                            'type': 'hidden'
                        }));
                    }
                    form.appendTo(document.body);
                    $(form).submit();
                }
            } else {
                return false;
            }
        });

});

function updateQueryStringParameter(url, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = url.indexOf('?') !== -1 ? "&" : "?";
    if (url.match(re)) {
        return url.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return url + separator + key + "=" + value;
    }
}

$(document).on('keypress', '[chars]', function (event) {
    var $elem = $(this);
    var chars = $elem.attr('chars');
    var arr = chars.split("|");
    if (typeof arr[1] != "undefined")
        if ($elem.val().length >= parseInt(arr[1])) {
            return false;
            event.preventDefault();
        }
    //$elem.val(value.replace(regReplace, ''));

});
$(".english").bind('keypress', function (e) {
    var regex = new RegExp("^[a-zA-Z0-9 ]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) return true;
    e.preventDefault();
    return false;
});
$(document).on('blur change', '[chars]', function (event) {
    var $elem = $(this);
    if ($elem.val() == '')
        return false;
    var chars = $elem.attr('chars');
    var arr = chars.split("|");
    var value = $elem.val(),
        regReplace,
        preset = {
            'int': {
                "regex": /[0-9 -()+]+$/,
                "label": "عدد صحیح"
            },
            'float': {
                "regex": '[-+]?([0-9]*.[0-9]+|[0-9]+)',
                "label": "عدد اعشار"
            },
            'ip': {
                "regex": 'bd{1,3}.d{1,3}.d{1,3}.d{1,3}b',
                "label": "آی پی"
            },
            'url': {
                "regex": /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
                "label": "url"
            },
            'string': {
                "regex": /^([a-z0-9]{5,})$/,
                "label": "رشته"
            },
            'email': {
                "regex": /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                "label": "ایمیل"
            },
            'jdate': {
                "regex": /^[1][1-4][0-9]{2}\/((0[1-6]\/(0[1-9]|[1-2][0-9]|3[0-1]))|(0[7-9]\/(0[1-9]|[1-2][0-9]|30))|(1[0-1]\/(0[1-9]|[1-2][0-9]|30))|(12\/(0[1-9]|[1-2][0-9])))/,
                "label": "تاریخ"
            },
            'time': {
                "regex": /(?:2[0-3]|[01][0-9]):[0-5][0-9]/,
                "label": "ساعت"
            },
            'mobile': {
                "regex": /^(0)?9\d{9}$/,
                "label": "شماره موبایل"
            },
            'nationalcode': {
                "regex": '',
                "label": "کد ملی"
            },
        },
        filter = preset[arr[0]].regex || arr[0];
    var checked = false;
    if (arr[0] == "nationalcode") {
        checked = checkCodeMelli($elem.val());
    } else {
        regReplace = new RegExp(filter, 'ig');
        checked = regReplace.test($elem.val());
    }
    if (!checked) {
        $elem.css({"border-width": "1px", "border-color": "rgb(210, 50, 45)", "border-style": "solid"});
        if (!$elem.parent().find(".help-block").length && !$elem.parent().parent().find(".help-block").length) {
            if ($elem.parent().hasClass('input-group'))
                $elem.parent().after("<div class='help-block text-danger'>ورودی یک " + preset[arr[0]].label + " نامعتبر است!</div>");
            else
                $elem.after("<div class='help-block text-danger'>ورودی یک " + preset[arr[0]].label + " نامعتبر است!</div>");
        }
    } else {
        // if ($elem.parent().hasClass('input-group')){
        //     if($elem.prev("input").length)
        //         $elem.next("input").blur(function(){
        //         }).blur();
        //     if($elem.next("input").length)
        //         $elem.next("input").blur(function(){
        //         }).blur();
        // }


        $elem.css({"border-width": "1px", "border-color": "#A6A9AE", "border-style": "solid"});
        if ($elem.parent().hasClass('input-group'))
            $elem.parent().parent().find(".help-block").remove();
        else
            $elem.parent().find(".help-block").remove();
    }

});
// $("input[chars^='time']").inputmask("99:99");




