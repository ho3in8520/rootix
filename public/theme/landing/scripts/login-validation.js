const email = document.getElementById("email");
const checkEmail = document.getElementById("check-email");
const passwordInput = document.querySelector("#password");
const eyeBtn = document.querySelector(".eye-btn");

eyeBtn.addEventListener("click", () => {
    const eyeIcon = eyeBtn.querySelector("svg");

    if (eyeIcon.classList.contains("open-eye")) {

        eyeBtn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="close-eye" width="30" height="30" viewBox="0 0 35.828 35.829"><g transform="translate(-0.086 -0.086)"><path d="M26.91,26.91A15.105,15.105,0,0,1,18,30C7.5,30,1.5,18,1.5,18A27.675,27.675,0,0,1,9.09,9.09m5.76-2.73A13.68,13.68,0,0,1,18,6C28.5,6,34.5,18,34.5,18a27.75,27.75,0,0,1-3.24,4.785m-10.08-1.6a4.5,4.5,0,1,1-6.36-6.36" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M1.5,1.5l33,33" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        passwordInput.setAttribute("type", "text")
    } else if(eyeIcon.classList.contains("close-eye")) {

        eyeBtn.innerHTML = `<svg class="open-eye" xmlns="http://www.w3.org/2000/svg" width="30" height="23" viewBox="0 0 35 26"><g transform="translate(-0.5 -5)"><path d="M1.5,18S7.5,6,18,6,34.5,18,34.5,18,28.5,30,18,30,1.5,18,1.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M22.5,18A4.5,4.5,0,1,1,18,13.5,4.5,4.5,0,0,1,22.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        passwordInput.setAttribute("type", "password")
    }
});

email.addEventListener("input", emailValidation);

function emailValidation() {
    const emailPattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

    if (email.value.match(emailPattern) && email.value.trim().length !== 0) {
        checkEmail.classList.add("active");
        return true;
    } else {
        checkEmail.classList.remove("active");
        return false;
    }
}

const checkPassword = document.getElementById("check-password");

const form = document.querySelector(".register-form");

// form.addEventListener("submit", () => {
//     if (emailValidation()) {
//         email.value = "";
//
//         checkEmail.classList.remove("active");
//     }
// });
