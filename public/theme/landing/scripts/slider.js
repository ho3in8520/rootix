// --------------------------------------------
$(".customer-owl").owlCarousel({
  // loop:true,
  rtl: true,
  margin: 0,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
      nav: false,
    },
    768: {
      items: 2,
    },
    1100: {
      items: 3,
    },
  },
});

$(".owl-crypto-carts").owlCarousel({
  // center: true,

  loop: false,
  rtl: true,
  dots: false,

  responsive: {
    0: {
      items: 1,
      stagePadding: 0,
    },
    380: {
      items: 1,
      stagePadding: 3,
    },
    680: {
      items: 1,
      stagePadding: 100,
    },
    800: {
      items: 2,
      stagePadding: 70,
    },
  },
});

$(".owl-feature").owlCarousel({
  loop: false,
  rtl: true,
  autoplay: true,

  responsive: {
    0: {
      items: 1,
      stagePadding: 0,
      dots: true,
    },
    500: {
      items: 2,
      stagePadding: 20,
      dots: false,
    },
    650: {
      items: 2,
      stagePadding: 70,
    },
    800: {
      items: 3,
      stagePadding: 40,
    },
  },
});

$(".blog-page-top-slide").owlCarousel({
  // loop:true,
  rtl: true,
  margin: 15,
  responsiveClass: true,
  responsive: {
    0: {
      dots: true,
      items: 1,
      nav: false,
    },
  },
});