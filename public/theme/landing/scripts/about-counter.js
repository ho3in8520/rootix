function counter() {
  const customers = document.getElementById("about-customers");
  const dailyTransaction = document.getElementById("daily-transaction");
  const monthlyTransaction = document.getElementById("monthly-transaction");

  let customersCounter = 0;

  const customersInterval = setInterval(() => {
    customersCounter += 1;
    customers.innerHTML = customersCounter;
    if (customersCounter === 100) {
      clearInterval(customersInterval);
    }
  }, 10);

  let dailyTransactionCounter = 0;

  const dailyTransactionInterval = setInterval(() => {
    dailyTransactionCounter += 1;
    dailyTransaction.innerHTML = dailyTransactionCounter;

    if (dailyTransactionCounter === 350) {
      clearInterval(dailyTransactionInterval);
    }
  }, 10);

  let monthlyTransactionCounter = 0;

  const monthlyTransactionInterval = setInterval(() => {
    monthlyTransactionCounter += 1;
    monthlyTransaction.innerHTML = monthlyTransactionCounter;
    if (monthlyTransactionCounter === 550) {
      clearInterval(monthlyTransactionInterval);
    }
  }, 10);
}

const counterRowOne = document.querySelector(".counter-row-1");
const counterRowTwo = document.querySelector(".counter-row-2");

let c = 0;

document.addEventListener("scroll", () => {
  const distanceCounterToTop =
    counterRowOne.offsetTop + counterRowTwo.offsetTop;

  if (scrollY >= distanceCounterToTop - 700 && c === 0) {
    c += 1;
    console.log("yes");
    counter();
  }
});
