const mymap = L.map("mapid").setView([32.64545, 51.66505], 13);
 // L.marker([32.633259, 51.364031]).addTo(mymap);
L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiaG9zZWluODQyMyIsImEiOiJja3VjYXV0NnoweW1oMnBsOWt6YXE3emhnIn0.3hRiFGa7s5gr0nqKHLH8KQ",
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
    accessToken: "your.mapbox.access.token",
  }
).addTo(mymap);
map.replaceLayer(oldLayer, newLayer)
