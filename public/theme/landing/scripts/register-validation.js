const email = document.getElementById("email");
const checkEmail = document.getElementById("check-email");

if (email != null)
    email.addEventListener("input", emailValidation);

function emailValidation() {
    const emailPattern = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

    if (email.value.match(emailPattern) && email.value.trim().length !== 0) {
        checkEmail.classList.add("active");
        return true;
    } else {
        checkEmail.classList.remove("active");
        return false;
    }
}

const password = document.getElementById("password");
const checkPassword = document.getElementById("password_confirmation");
const strengthMeter = document.getElementById("password-progress");

password.addEventListener("input", updateStrengthMeter);
updateStrengthMeter();

function updateStrengthMeter() {
    const passwordProgress = document.querySelector(".password-progress");
    let strength = 100;
    const weaknesses = calculatePasswordStrength(password.value);

    weaknesses.forEach((weakness) => {
        if (weakness == null) return;
        strength -= weakness.deduction;
        const messageElement = document.createElement("div");
        messageElement.innerText = weakness.message;
    });
    strengthMeter.style.width = `${strength}%`;

    if (strength < 30) {
        passwordProgress.removeAttribute("data-before");
        checkPassword.classList.remove("active");
    }

    if (strength >= 30 && strength <= 50) {
        passwordProgress.setAttribute("data-before", "متوسط");
        checkPassword.classList.add("active");
    }

    if (strength >= 50 && strength <= 100) {
        passwordProgress.setAttribute("data-before", "قوی");
        checkPassword.classList.add("active");
    }
}

function calculatePasswordStrength(password) {
    const weaknesses = [];
    weaknesses.push(lengthWeakness(password));
    weaknesses.push(lowercaseWeakness(password));
    weaknesses.push(uppercaseWeakness(password));
    weaknesses.push(numberWeakness(password));
    weaknesses.push(specialCharactersWeakness(password));
    weaknesses.push(repeatCharactersWeakness(password));
    return weaknesses;
}

function lengthWeakness(password) {
    const length = password.length;

    if (length <= 5) {
        return {
            message: "Your password is too short",
            deduction: 40,
        };
    }

    if (length <= 10) {
        return {
            message: "Your password could be longer",
            deduction: 15,
        };
    }
}

function uppercaseWeakness(password) {
    return characterTypeWeakness(password, /[A-Z]/g, "uppercase characters");
}

function lowercaseWeakness(password) {
    return characterTypeWeakness(password, /[a-z]/g, "lowercase characters");
}

function numberWeakness(password) {
    return characterTypeWeakness(password, /[0-9]/g, "numbers");
}

function specialCharactersWeakness(password) {
    return characterTypeWeakness(
        password,
        /[^0-9a-zA-Z\s]/g,
        "special characters"
    );
}

function characterTypeWeakness(password, regex, type) {
    const matches = password.match(regex) || [];

    if (matches.length === 0) {
        return {
            message: `Your password has no ${type}`,
            deduction: 20,
        };
    }

    if (matches.length <= 2) {
        return {
            message: `Your password could use more ${type}`,
            deduction: 5,
        };
    }
}

function repeatCharactersWeakness(password) {
    const matches = password.match(/(.)\1/g) || [];
    if (matches.length > 0) {
        return {
            message: "Your password has repeat characters",
            deduction: matches.length * 10,
        };
    }
}

const passwordRepeat = document.getElementById("password_confirmation");
const checkPasswordRepeat = document.getElementById("check-password-again");

passwordRepeat.addEventListener("input", passwordRepeatIsValid);

function passwordRepeatIsValid() {
    if (
        password.value === passwordRepeat.value &&
        passwordRepeat.value.trim().length !== 0
    ) {
        checkPasswordRepeat.classList.add("active");
        return true;
    } else {
        checkPasswordRepeat.classList.remove("active");
        return false;
    }
}

const referralCode = document.getElementById("referral-code");
const form = document.querySelector(".register-form");

form.addEventListener("submit", (e) => {
    if (emailValidation() && passwordRepeatIsValid()) {
        // email.value = "";
        // password.value = "";
        // passwordRepeat.value = "";
        // referralCode.value = "";

        strength = 0;

        checkEmail.classList.remove("active");
        checkPassword.classList.remove("active");
        checkPasswordRepeat.classList.remove("active");
    }
});

const passwordInput = document.querySelector("#password");
const eyeBtn = document.querySelector(".eye-btn");

eyeBtn.addEventListener("click", () => {
    const eyeIcon = eyeBtn.querySelector("svg");

    if (eyeIcon.classList.contains("open-eye")) {
        eyeBtn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="close-eye" width="30" height="30" viewBox="0 0 35.828 35.829"><g transform="translate(-0.086 -0.086)"><path d="M26.91,26.91A15.105,15.105,0,0,1,18,30C7.5,30,1.5,18,1.5,18A27.675,27.675,0,0,1,9.09,9.09m5.76-2.73A13.68,13.68,0,0,1,18,6C28.5,6,34.5,18,34.5,18a27.75,27.75,0,0,1-3.24,4.785m-10.08-1.6a4.5,4.5,0,1,1-6.36-6.36" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M1.5,1.5l33,33" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        passwordInput.setAttribute("type", "text");
    } else if (eyeIcon.classList.contains("close-eye")) {
        eyeBtn.innerHTML = `<svg class="open-eye" xmlns="http://www.w3.org/2000/svg" width="30" height="23" viewBox="0 0 35 26"><g transform="translate(-0.5 -5)"><path d="M1.5,18S7.5,6,18,6,34.5,18,34.5,18,28.5,30,18,30,1.5,18,1.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M22.5,18A4.5,4.5,0,1,1,18,13.5,4.5,4.5,0,0,1,22.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        passwordInput.setAttribute("type", "password");
    }
});

const repeatPasswordInput = document.querySelector("#password_confirmation");
const repeatEyeBtn = document.querySelector(".repeat-password-ete-btn");

repeatEyeBtn.addEventListener("click", () => {
    const eyeIcon = repeatEyeBtn.querySelector("svg");

    if (eyeIcon.classList.contains("open-eye")) {
        repeatEyeBtn.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" class="close-eye" width="30" height="30" viewBox="0 0 35.828 35.829"><g transform="translate(-0.086 -0.086)"><path d="M26.91,26.91A15.105,15.105,0,0,1,18,30C7.5,30,1.5,18,1.5,18A27.675,27.675,0,0,1,9.09,9.09m5.76-2.73A13.68,13.68,0,0,1,18,6C28.5,6,34.5,18,34.5,18a27.75,27.75,0,0,1-3.24,4.785m-10.08-1.6a4.5,4.5,0,1,1-6.36-6.36" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M1.5,1.5l33,33" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        repeatPasswordInput.setAttribute("type", "text");
    } else if (eyeIcon.classList.contains("close-eye")) {
        repeatEyeBtn.innerHTML = `<svg class="open-eye" xmlns="http://www.w3.org/2000/svg" width="30" height="23" viewBox="0 0 35 26"><g transform="translate(-0.5 -5)"><path d="M1.5,18S7.5,6,18,6,34.5,18,34.5,18,28.5,30,18,30,1.5,18,1.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path d="M22.5,18A4.5,4.5,0,1,1,18,13.5,4.5,4.5,0,0,1,22.5,18Z" fill="none" stroke="#727272" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/></g></svg>`;
        repeatPasswordInput.setAttribute("type", "password");
    }
});
