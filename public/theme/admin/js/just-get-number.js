const inputJustGetNumber = (inputs) => {
  const numberRegex = /^[\d,]*$/;

  inputs.forEach((input) => {
    input.addEventListener('input', () => {
      const inputSplit = input.value.split('');

      const newInputValue = inputSplit.filter((inputCharacter) =>
        numberRegex.test(inputCharacter)
      );

      input.value = newInputValue.join('');
    });
  });
};
