const tableRefreshBtn = document.querySelector('.table__refresh-btn');
const dashboardTable = document.querySelector('.dashboard-table');

const enableLoading = () => {
  dashboardTable.classList.add('active');
};

const disableLoading = () => {
  dashboardTable.classList.remove('active');
};

tableRefreshBtn.addEventListener('click', enableLoading);
