const getOrCreateTooltip = (chart) => {
    let tooltipEl = chart.canvas.parentNode.querySelector("div");

    if (!tooltipEl) {
        tooltipEl = document.createElement("div");
        tooltipEl.classList.add("tooltip-design");
        tooltipUl = document.createElement("ul");
        tooltipUl.classList.add("tooltip-ul");

        tooltipEl.appendChild(tooltipUl);
        chart.canvas.parentNode.appendChild(tooltipEl);
    }
    return tooltipEl;
};

const externalTooltipHandler = (context) => {
    const { chart, tooltip } = context;
    const tooltipEl = getOrCreateTooltip(chart);
    tooltipEl.classList.add("active");
    if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    if (tooltip.body) {
        const bodyLine = tooltip.body.map((b) => b.lines);
        const tooltipLi = document.createElement("li");

        const myChartData = myChart.data.datasets[0].data;
        const sum = myChartData.reduce((accumulator, curr) => accumulator + curr);

        tooltipBodyEl = document.createElement("p");
        tooltipBodyEl.textContent = bodyLine[0];

        bodyLine.forEach((body, i) => {
            const walletChart = document.querySelector(".wallet-chart");
            const num = body[0].match(/(\d+)/);
            const value = parseInt(num[0]);

            const percent = Math.round((value / sum) * 100);

            walletChart.setAttribute("data-chart", `${percent}%`);
        });

        const ulNode = tooltipEl.querySelector("ul");

        while (ulNode.firstChild) {
            ulNode.firstChild.remove();
        }

        ulNode.appendChild(tooltipLi);
    }
};

// Doughnut 2

var ctx2 = document.getElementById("assets-chart").getContext("2d");

var gradientStroke = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "transparent");
gradientFill3.addColorStop(0, "transparent");

var gradientStroke3 = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill4 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill4.addColorStop(1, "#5834EC");
gradientFill4.addColorStop(0.7, "#8A71F2");
gradientFill4.addColorStop(0, "#8A71F2");

var gradientFill5 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill5.addColorStop(1, "#DD2ABB");
gradientFill5.addColorStop(0.7, "#E664CE");
gradientFill5.addColorStop(0, "#E664CE");

var myChart = new Chart(ctx2, {
    type: "doughnut",
    data: {
        datasets: [
            {
                label: ["کیف ریالی", "کیف رمز ارز"],
                data: [Math.round(chart_data.assets_chart.rial), Math.round(chart_data.assets_chart.usdt)],
                borderColor: gradientStroke3,
                fill: true,
                backgroundColor: ["#DD2ABB", "#5834EC"],
                borderWidth: 1,
                cutout: "68%",
            },
        ],
    },
    options: {
        plugins: {
            tooltip: {
                enabled: false,
                external: externalTooltipHandler,
            },
        },
    },
});

const assetsChartInfo = document.querySelector(".assets-chart__info");
window.addEventListener("load", () => {
    const chartInfo = myChart.data.datasets[0];

    const sum = chartInfo.data.reduce((accumulator, curr) => accumulator + curr);

    chartInfo.data.forEach((label, index) => {
        createElement(chartInfo, index, label, sum);
    });
});

function createElement(chartInfo, index, label, sum) {
    const colorEl = document.createElement("span");
    const color = chartInfo.backgroundColor[index]

    colorEl.classList = "chart-color";
    colorEl.style.border = `3.7px solid ${color}`;

    const chartInfoContainer = document.createElement("div");
    chartInfoContainer.classList = "chartInfoContainer";
    const labelBtnEl = document.createElement("button");
    labelBtnEl.classList = "assets-chart__title w-100";

    const labelTextEl = document.createElement("h5");
    labelTextEl.textContent = chartInfo.label[index];

    const percentEl = document.createElement("span");
    const percent = Math.round((label / sum) * 100);
    percentEl.textContent = percent + "%";

    createTooltip(percent);

    chartInfoContainer.appendChild(colorEl);
    chartInfoContainer.appendChild(labelTextEl);

    labelBtnEl.appendChild(chartInfoContainer);
    labelBtnEl.appendChild(percentEl);

    assetsChartInfo.appendChild(labelBtnEl);
}

const createTooltip = (percent) => {
    const tooltipChartEl = document.createElement("span");
    tooltipChartEl.textContent = percent + "%";
};

// Chart 2

var ctx = document.getElementById("red-chart")
if (ctx) {
    ctx= ctx.getContext("2d");
    var gradientStroke = ctx.createLinearGradient(0, 0, 0, 0);

    var gradientFill = ctx.createLinearGradient(0, 500, 0, 100);
    gradientFill.addColorStop(1, "#ff000009");
    gradientFill.addColorStop(0, "#ff000009");

    var myChart2 = new Chart(ctx, {
        type: "line",
        data: {
            labels: [
                "10AM",
                "11AM",
                "12AM",
                "01AM",
                "02AM",
                "03AM",
                "04AM",
                "10AM",
                "11AM",
                "12AM",
                "01AM",
                "02AM",
                "03AM",
                "04AM",
                "10AM",
                "11AM",
                "12AM",
                "10AM",
                "11AM",
                "12AM",
            ],

            datasets: [
                {
                    label: "",
                    borderColor: "#ff0077ec",
                    pointBackgroundColor: gradientStroke,
                    fill: true,
                    borderWidth: 2,
                    pointBorderWidth: 0,
                    backgroundColor: gradientFill,
                    data: [
                        23, 25, 24, 22, 21, 19, 18, 19, 18, 19, 20, 17, 22, 23, 24, 21, 15.3,
                        15, 16, 17, 17.5, 17.3, 22, 23,
                    ],

                    pointHitRadius: 30,
                },
            ],
        },

        options: {
            scales: {
                x: {
                    grid: {
                        display: false,
                    },
                },
                y: {
                    min: 0,
                },
            },
            responsive: true,
            maintainAspectRatio: false,

            plugins: {
                legend: {
                    labels: {
                        boxWidth: 0,
                    },
                },
            },

            tooltips: {
                callbacks: {
                    label: function (tooltipItem) {
                        return tooltipItem.yLabel;
                    },
                },
            },
        },
    });
}

// Doughnut 3

var ctx3 = document.getElementById("wallet-assets-chart").getContext("2d");

var gradientStroke = ctx3.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx3.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "transparent");
gradientFill3.addColorStop(0, "transparent");

var gradientStroke3 = ctx3.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx3.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "#FF9800");

var gradientFill4 = ctx3.createLinearGradient(0, 500, 0, 100);
gradientFill4.addColorStop(1, "#6563FF");

var gradientFill5 = ctx3.createLinearGradient(0, 500, 0, 100);
gradientFill5.addColorStop(1, "#739BEE");

const secondChartLabel = (ctx) => {
    console.log(ctx);
    return ctx.parsed + "%";
};

var myChart3 = new Chart(ctx3, {
    type: "doughnut",
    data: {
        datasets: [
            {
                label: chart_data.crypto_chart.label,
                data: chart_data.crypto_chart.data,
                borderColor: gradientStroke3,
                fill: true,
                backgroundColor: chart_data.crypto_chart.backgroundColor,
                borderWidth: 1,
                cutout: "55%",
            },
        ],
    },
    options: {
        plugins: {
            tooltip: {
                // external: externalTooltipHandler,
                callbacks: {
                    label: function (ctx) {
                        console.log(ctx);
                        const title = ctx.dataset.label[ctx.dataIndex];
                        const value = ctx.parsed;
                        const sum = ctx.dataset.data.reduce(
                            (ack, currentItem) => ack + currentItem,
                            0
                        );
                        const percent = Math.round((value / sum) * 100);

                        return `${title} : ${percent}%`;
                    },
                },
                displayColors: false,
            },
        },
    },
});
