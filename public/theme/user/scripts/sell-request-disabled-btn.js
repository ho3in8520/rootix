const recordBtn = document.querySelector('.record-btn');
const requestBoxs = [...document.querySelectorAll('.request-box')];
const requestJsBox1 = document.querySelector('.request-js-box-1');
const requestJsBox2 = document.querySelector('.request-js-box-2');
const requestJsBox3 = document.querySelector('.request-js-box-3');
const requestJsBox4 = document.querySelector('.request-js-box-4');
const input1 = document.getElementById('currency-value-input-1');
const input2 = document.getElementById('currency-value-input-2');
const input3 = document.getElementById('currency-value-input-3');

const inputValueIsEmpty = (input) => input.value.trim().length === 0;
const selectedValueIsEmpty = (selected) => selected.value.length === 0;

const handleDisabledBtn = () => {
  const getSelectedValue =
    volumeFormSelectedTitleContainer.getAttribute('data-value');

  if (getSelectedValue) {
    requestJsBox2.classList.add('active');
    requestJsBox2.classList.add('enabled');
    requestJsBox1.classList.remove('active');
    input1.disabled = false;
  } else {
    requestJsBox1.classList.add('active');
    requestJsBox1.classList.add('enabled');
    requestJsBox2.classList.remove('active');
    input1.disabled = true;
  }

  const getNumberRegex = /^\d*\.?\d*$/;
  if (!inputValueIsEmpty(input1) && getNumberRegex.test(input1.value.trim())) {
    requestJsBox3.classList.add('active');
    requestJsBox2.classList.remove('active');
    requestJsBox3.classList.add('enabled');
    input2.disabled = false;
  } else {
    input1.value = '';
    input2.disabled = true;
    requestJsBox3.classList.remove('active');
    requestJsBox3.classList.remove('enabled');
  }

  if (!inputValueIsEmpty(input2) && getNumberRegex.test(input2.value.trim())) {
    requestJsBox4.classList.add('active');
    requestJsBox3.classList.remove('active');
    requestJsBox4.classList.add('enabled');
    input3.disabled = false;
  } else {
    input2.value = null;
    requestJsBox4.classList.remove('active');
    requestJsBox4.classList.remove('enabled');
  }

  if (!getNumberRegex.test(input3.value.trim())) {
    input3.value = null;
  }

  if (
    getSelectedValue &&
    !inputValueIsEmpty(input1) &&
    !inputValueIsEmpty(input2) &&
    !inputValueIsEmpty(input3)
  ) {
    recordBtn.disabled = false;
  } else {
    recordBtn.disabled = true;
  }
};

input1.addEventListener('input', handleDisabledBtn);
input2.addEventListener('input', handleDisabledBtn);
input3.addEventListener('input', handleDisabledBtn);
volumeFormSelected.addEventListener('click', handleDisabledBtn);
