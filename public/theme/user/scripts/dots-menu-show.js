function showWalletMenu(walletMenu, walletBtn) {
  walletBtn.addEventListener('click', () => {
    walletMenu.classList.add('active');
    walletBtn.classList.add('hidden');
  });

  document.addEventListener('click', (e) => {
    if (!walletBtn.contains(e.target) && !walletMenu.contains(e.target)) {
      walletBtn.classList.remove('hidden');
      walletMenu.classList.remove('active');
    }
  });
}

const dotsBtnEl = document.querySelector('.dots-btn');
const walletMenuEl = document.querySelector('.wallet__menu');

showWalletMenu(walletMenuEl, dotsBtnEl);

const dotsBtnTableEl = document.querySelector('.dots-table-btn');
const tableMenuEl = document.querySelector('.wallet__table-menu');

showWalletMenu(tableMenuEl, dotsBtnTableEl);
