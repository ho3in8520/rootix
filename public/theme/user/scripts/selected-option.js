const setLiContentForSelected = (lis, selectBox) => {
  lis.forEach((li) => {
    li.addEventListener('click', () => {
      selectBox.innerHTML = '';
      const dataValue = li.getAttribute('data-value');
      const option = document.createElement('option');
      option.value = dataValue;
      selectBox.appendChild(option);
    });
  });
};
