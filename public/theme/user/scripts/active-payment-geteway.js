const symbols = [...document.querySelectorAll('.symbols__symbol')];

symbols.forEach((symbol, index) => {
  symbol.addEventListener('click', () => {
    symbols.forEach((nestedSymbol) => {
      nestedSymbol.classList.remove('active');
    });
    symbol.classList.add('active');
  });
});
