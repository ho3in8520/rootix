const boxes = [...document.querySelectorAll('.selected-boxs__box')];

boxes.forEach((box) => {
  box.addEventListener('click', () => {
    const currencyInput = document.querySelector('#currency-value-input-1');
    const boxValue = box.querySelector('.total');

    boxes.forEach((boxs) => {
      boxs.classList.remove('active');
    });

    console.log(boxValue);
    currencyInput.value = boxValue.textContent.trim();
    box.classList.add('active');
  });
});
