const toggleSelected = (btn) => {
  btn.addEventListener('click', () => {
    btn.classList.toggle('active');
  });
};
