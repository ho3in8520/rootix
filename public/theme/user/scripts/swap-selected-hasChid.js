const swapSelected = document.querySelector('.first-volume-form__menu-3');
const swapSelectedChildren = [
    ...document.querySelectorAll('.first-volume-form__menu-3 li'),
];

const swapSelected2 = document.querySelector('.second-volume-form__menu-3');
const swapSelectedChildren2 = [
    ...document.querySelectorAll('.second-volume-form__menu-3 li'),
];

const hasChild = (swapSelected, swapSelectedChildren) => {
    if (swapSelectedChildren.length >= 6) {
        swapSelected.style.overflowY = 'scroll';
    } else {
        swapSelected.style.overflowY = 'visible';
        swapSelected.style.height = 'auto';
    }
};

document.addEventListener('DOMContentLoaded', () => {
    hasChild(swapSelected, swapSelectedChildren);
    hasChild(swapSelected2, swapSelectedChildren2);
});
