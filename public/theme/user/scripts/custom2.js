// start-navigation
var owl = $('.owl-carousel');
owl.owlCarousel({
    margin: 10,
    rtl: true,
    nav: true,
    loop: true,

    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 4
        }
    }
});
// end-navigation

// start-notification
$(document).ready(function () {
    $(".container-notification").click(function (event) {
        event.stopPropagation();
    });


    $(".icon-header").click(function (event) {
        event.stopPropagation();
        $(".container-notification").fadeToggle();
    });

    $("body").click(function (event) {
        $(".container-notification").fadeOut()
    })

    $(".notification-tab").click(function () {
        var type = $(this).data('type')
        $(".notification-tab").removeClass('active');
        $(this).addClass('active');
        // --------------------
        var ul = $(this).closest('.container-notification').find('ul')
        ul.hide()
        $('*[data-type=' + type + ']').show()
    })

// continu-navigation
    $(document).on('click', '.btn-next', function () {
        $(".owl-next").click()
    })
    $(document).on('click', '.btn-prev', function () {
        $(".owl-prev").click()
    })
// continu-navigation

});
// end-notification
