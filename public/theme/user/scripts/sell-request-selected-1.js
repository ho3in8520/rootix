const volumeFormSelected = document.querySelector('.volume-form__selected');
const volumeFormSelected8 = document.querySelector('.volume-form__selected-2');
const volumeForm = document.querySelector('.volume-form');
const volumeFormLinks = [...volumeForm.querySelectorAll('.volume-form__link')];
const volumeFormSelectedTitleContainer = document.querySelector(
  '.volume-form__selected-desc'
);

const volumeFormSelected2 = document.querySelector('.volume-form__selected-2');
const volumeForm2 = document.querySelector('.volume-form-2');
const volumeFormLinks2 = [...document.querySelectorAll('.volume-form__link-2')];
const volumeFormSelectedTitleContainer2 = document.querySelector(
  '.volume-form__selected-desc-2'
);

if (volumeFormSelected8 !== null) {
  volumeFormSelected8.addEventListener('click', () => {
    volumeFormSelected8.classList.toggle('active');
  });
}
function selectedToggle(volumeFormSelected, volumeForm) {
  volumeFormSelected.addEventListener('click', () => {
    volumeFormSelected.classList.toggle('active');
    if (volumeFormSelected.classList.contains('active')) {
      volumeForm.style.borderBottomLeftRadius = '0px';
    } else {
      volumeForm.style.borderBottomLeftRadius = '15px';
    }
  });
}

function loopLinks(volumeFormLinks, volumeFormSelectedTitleContainer) {
  volumeFormLinks.forEach((volumeFormLink, index) => {
    volumeFormLink.addEventListener('click', (e) => {
      e.preventDefault();
      const volumeFormLinkChild = {
        imgUrl: volumeFormLink.querySelector('.volume-form__img')
          ? volumeFormLink.querySelector('.volume-form__img').src
          : null,
        title: volumeFormLink.querySelector('.volume-form__selected-title')
          .textContent,
      };

      createHtmlEl(volumeFormLinkChild, volumeFormSelectedTitleContainer);
    });
  });
}

function createHtmlEl(volumeFormLinkChild, volumeFormSelectedTitleContainer) {
  const currentCurrency = `
  ${
    volumeFormLinkChild.imgUrl
      ? `<img src=${volumeFormLinkChild.imgUrl} class="volume-form__img">`
      : ''
  }
  <span class="volume-form__selected-title">${volumeFormLinkChild.title}</span>
  <div class="arr">
    <svg
    xmlns="http://www.w3.org/2000/svg"
    width="13"
    height="11"
    viewBox="0 0 18 11.115"
    >
    <path
    id="Icon_material-expand-more"
    data-name="Icon material-expand-more"
    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
    transform="translate(-9 -12.885)"
    />
    </svg>
    </div>
    `;

  volumeFormSelectedTitleContainer.setAttribute('data-value', 'true');

  volumeFormSelectedTitleContainer.innerHTML = currentCurrency;
}

selectedToggle(volumeFormSelected, volumeForm);
loopLinks(volumeFormLinks, volumeFormSelectedTitleContainer);
