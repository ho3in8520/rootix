const currencyTableLeft = [...document.querySelectorAll(".currency-table-left")];

const getCurrencyElWith = (currencyEl) => currencyEl.getAttribute("bg-size");

const setWidth = (currencyEl, currentCurrencyElWidth) => {
    const bg = currencyEl.querySelector(".bg");
    bg.style.width = `${currentCurrencyElWidth}%`;
};
var currencies = ''
let market = $("input[name='market']").val()
currencyTableLeft.forEach((currencyEl) => {
    const currentCurrencyElWidth = getCurrencyElWith(currencyEl);
    setWidth(currencyEl, currentCurrencyElWidth);
});

$(document).ready(function () {
    reloadTicker()
    reloadListRequest()
    reloadLastTransaction()
    $('#source_amount').on('keyup', function () {
        $("#buy li button").css('background-color', '#eff2f6');
        $("#buy li button").css('color', '4a4a4a');
    })
    $('#des_amount').on('keyup', function () {
        $("#sell li button").css('background-color', '#eff2f6');
        $("#sell li button").css('color', '4a4a4a');
    })
    $('#buy li').on('click', function () {
        let pct = $(this).text();
        let total_usdt = source_amount;
        let buy_price = $('input[name="price"]').val();
        buy_price = buy_price.replace(',', '');
        if (buy_price == null || buy_price.length == 0)
            return false;
        else {
            let cal_usdt = calculate_usdt(total_usdt, pct, 'buy', buy_price);
            if (buy_price)
                $('input[name=amount]').val(cal_usdt);
            else
                $('#source_amount').val(cal_usdt);
        }

        $('input[name="amount"]').change()

    });
    $('#sell li').on('click', function () {
        let pct = $(this).text();
        let total_des = des_amount;
        let cal_des = calculate_usdt(total_des, pct, 'sell');
        $('#des_amount').val(cal_des);
        $('input[name="sell_amount"]').change()
    });

    $(document).on('click', '.ticker tbody tr', function () {
        var url = $(this).data('href');
        window.location.href = url;
    })

    $(document).on('click', '.order-table tbody tr', function () {
        let price = $(this).children('td:first').text();
        $('input[name="sell_price"]').val(price);
        $('input[name="price"]').val(price);
    })

    $('#open_order').on('click', function () {
        $(this).addClass('active');
        $('#open').show();
        $('#close').hide();
        $('#close_order').removeClass('active');
        $('#all').hide();
        $('#all_orders').removeClass('active');
    });

    $('#close_order').on('click', function () {
        $(this).addClass('active')
        $('#open').hide();
        $('#open_order').removeClass('active');
        $('#close').show();
        $('#all').hide();
        $('#all_orders').removeClass('active');
    });

    $('#all_orders').on('click', function () {
        $(this).addClass('active')
        $('#open').hide();
        $('#open_order').removeClass('active');
        $('#close').hide();
        $('#close_order').removeClass('active');
        $('#all').show();
    });

    $('input[name="price"], input[name="amount"]').on('keyup change', function () {


        let unit = source_unit === 'rial' ? ' Toman' : ' usdt';
        let price=$('input[name="price"]').val();
        price=price.replace(',','')
        let total = (price * $('input[name="amount"]').val()) + unit;
        $('#total_buy').text(total);
    });

    $('input[name="sell_price"], input[name="sell_amount"]').on('keyup change', function () {
        let unit = source_unit === 'rial' ? ' Toman' : ' usdt';
        let sell_price=$('input[name="sell_price"]').val();
        sell_price=sell_price.replace(',','');
        let total = (sell_price * $('input[name="sell_amount"]').val()) + unit;
        $('#total_sell').text(total);
    });
    $('.last-price').on('click', function () {
        let price = $(this).text();
        $('input[name="sell_price"]').val(price);
        $('input[name="price"]').val(price);
    })

    $(document).on('click', '.tread-tabs-markets', function () {
        let url = $(this).data('action')
        window.location.href = url;
    })
})

function ajax_trade(action, method, data, func_name) {
    $.ajax({
        url: action,
        type: method,
        data: data,
        success: function (response) {
            if (response != null) {
                if (func_name != null)
                    func_name(response);
            }
        },
        error: function (xhr) {

        }
    });
}

//// list-request
function listRequestCallback(response) {
    listRequestSetForm('sell', response.sells)
    listRequestSetForm('buy', response.buys)
    let array = market.split('/')
    if (response.last > 0) {
        $(".last-price").text(convertPrice(array[1], response.last, currencies))
        $(".last-price-rls").text(convertPrice('rial', response.last, currencies))
    }
}

//// list-request
function reloadListRequest() {
    let csrf = $("meta[name='csrf-token']").attr('content')
    let exchange_list_request_url = $(".list-request").data('url');
    setInterval(function () {
        ajax_trade(httpToHttps(exchange_list_request_url), 'post', {_token: csrf, market: market}, listRequestCallback)
    }, 3000)
}

//// list-request
function listRequestSetForm(mode, data) {
    let classTable = mode == 'sell' ? $(".tbody-list-request-sell") : $(".tbody-list-request-buy");
    let classBg = mode == 'sell' ? 'red-bg' : 'green-bg';
    let array = market.split('/')
    if (data[0]) {
        var html = '';
        $.each(data, (index, item) => {
            html += '<tr class="position-relative currency-table-left" bg-size="90">\n' +
                '<td class="red">' + convertPrice(array[1], item[0], currencies) + '</td>\n' +
                '<td>' + item[1] + '</td>\n' +
                '<td>' + convertPrice(array[1], item['total'], currencies) + '</td>\n' +
                '<td class="bg ' + classBg + '" style="width: ' + item['percent_background'] + '%;"></td>\n' +
                '</tr>'
        });
        $(classTable).html(html)
        $(classTable).closest('.tread-tabs__content').removeClass('position-relative')
        $(classTable).closest('.tread-tabs__content').find('.spinner').remove()
    }
}

function reloadLastTransaction() {
    let csrf = $("meta[name='csrf-token']").attr('content')
    let exchange_last_transaction_url = $(".last-transaction").data('url');
    ajax_trade(httpToHttps(exchange_last_transaction_url), 'post', {_token: csrf, market: market}, LastTransactionSetForm)
    setInterval(function () {
        ajax_trade(httpToHttps(exchange_last_transaction_url), 'post', {_token: csrf, market: market}, LastTransactionSetForm)
    }, 10000)
}

function LastTransactionSetForm(data) {
    let classTable = $(".tbody-last-transaction");
    let array = market.split('/')
    var html = '';
    $.each(data, (index, item) => {
        html += '<tr>\n' +
            '<td>\n' +
            item['date'] +
            '</td>\n' +
            '<td class="' + item['color'] + '">' + convertPrice(array[1], item['price'], currencies) + '</td>\n' +
            '<td>' + item['amount'] + '</td>\n' +
            '</tr>'
    });
    $(classTable).html(html)
    $(classTable).closest('.panel-box').removeClass('position-relative')
    $(classTable).closest('.panel-box').find('.spinner').remove()
}

function reloadTicker() {
    let csrf = $("meta[name='csrf-token']").attr('content')
    let exchange_ticker_url = $(".ticker").data('url');

    ajax_trade(httpToHttps(exchange_ticker_url), 'post', {_token: csrf, market: market}, tickerSetForm)
    setInterval(function () {
        ajax_trade(httpToHttps(exchange_ticker_url), 'post', {_token: csrf, market: market}, tickerSetForm)
    }, 10000)
}

function tickerSetForm(data) {
    let source_unit = market.split('/')[1];
    let classTable = $(".ticker-tbody");
    var html = '';
    currencies = data;
    let units = ['RLS', 'USDT']
    $.each(data, (index, item) => {
        if (!inArray(index, units)) {
            html += '<tr style="cursor: pointer"\n' +
                'data-href="' + item['url'] + '">\n' +
                '<td>\n' +
                '<img src="' + item['logo_url'] + '" alt="' + item['name'] + '" width="25">\n' +
                '</td>\n' +
                '<td>' + item['currency'] + '/' + get_unit(source_unit, 'en').toUpperCase() + '</td>\n' +
                '<td>' + convertPrice(source_unit, item['price'], currencies) + '</td>\n' +
                '</tr>'
        }
    });
    $(classTable).html(html)
    $(classTable).closest('.panel-box').removeClass('position-relative')
    $(classTable).closest('.panel-box').find('.spinner').remove()
}

function calculate_usdt(value, pct, elm_id, price = null) {
    switch (pct.trim()) {
        case "25%":
            change_pct_style(1, elm_id);
            if (price != null)
                return value / (price * 4);
            else
                return value / 4;
        case '50%':
            change_pct_style(2, elm_id);
            if (price != null)
                return value / (price * 2);
            else
                return value / 2;
        case '75%':
            change_pct_style(3, elm_id);
            if (price != null)
                return value * 3 / (price * 4);
            else
                return value * 3 / 4;
        case '100%':
            change_pct_style(4, elm_id);
            if (price != null)
                return value / (price * 1);
            else
                return value / 1;
        default:
            alert('pct id is ' + pct);
            break;
    }
}

function change_pct_style(num, elm_id) {
    let background_color = elm_id === 'buy' ? '#26a69a' : '#ff231f';
    $("#" + elm_id + " li button").css('background-color', '#eff2f6');
    $("#" + elm_id + " li button").css('color', '4a4a4a');
    for (let j = 1; j <= num; j++) {
        $("#" + elm_id + " li:nth-child(" + j + ") button").css('background-color', background_color);
        $("#" + elm_id + " li:nth-child(" + j + ") button").css('color', '#fff');
    }
}

function scrollToBottom(id) {
    var div = document.getElementById(id);
    div.scrollTop = div.scrollHeight - div.clientHeight;
}

function convertPrice(des_unit, price_des, currencies) {
    if (des_unit == 'usdt') {
        return price_des;
    }
    var price = swap(currencies, 'usdt', (des_unit == 'rial') ? 'rls' : des_unit, price_des, false, false);
    if (des_unit == 'rial')
        return (price < 1) ? price : numberFormat(parseFloat(price).toFixed(3))
    return price;

}
