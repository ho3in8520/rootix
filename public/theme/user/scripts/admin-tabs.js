const activeTabs = () => {
  const tabsContent = [...document.querySelectorAll('.tab__content')];
  const tabsBtn = [...document.querySelectorAll('.tab__btn')];

  if (tabsContent && tabsBtn) {
    tabsBtn.forEach((btn, index) => {
      btn.addEventListener('click', () => {
        tabsContent.forEach((content) => {
          content.classList.remove('active');
        });
        tabsBtn.forEach((btn) => {
          btn.classList.remove('active');
        });

        const tabContent = tabsContent[index];
        tabsContent[index] && tabContent.classList.add('active');
        const tabBtn = tabsBtn[index];
        tabsBtn[index] && tabBtn.classList.add('active');
      });
    });
  }
};

window.addEventListener('load', activeTabs);
