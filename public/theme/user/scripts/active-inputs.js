/*
const activeAllInputs = (inputsParents, index) => {
  const currentInputParent = inputsParents[index];
  const inputs = [...currentInputParent.querySelectorAll('input')];
  const selects = [...currentInputParent.querySelectorAll('.disable-select')];
  const radios = [
    ...currentInputParent.querySelectorAll('.radio-container__admin'),
  ];
  const recordBtn = currentInputParent.querySelector('.record-all-change-btn');

  recordBtn.classList.add('d-flex');
  inputs.forEach((input) => {
    input.disabled = false;
  });

  selects.forEach((select) => {
    select.classList.remove('disable-select');
  });

  radios.forEach((radio) => {
    radio.style.pointerEvents = 'auto';
  });

  recordBtn.addEventListener("click", () => {
    recordBtn.classList.remove('d-flex');
    inputs.forEach((input) => {
      input.disabled = true;
    });

    selects.forEach((select) => {
      select.classList.add('disable-select');
    });

    radios.forEach((radio) => {
      radio.style.pointerEvents = 'none';
    });
  })
};
*/
$(document).ready(function (){
    $(document).on('click','.edit-btn',function (){
        $(this).closest('.items-center').find("button[type='submit']").removeClass('d-none')
        $(this).closest('.inputs-box').find("input").removeAttr('disabled');
        $(this).closest('.inputs-box').find(".select-parent-2").removeClass('disable-select')
        $(this).closest('.inputs-box').find(".radio-container__admin").css('pointer-events','auto')
    })
})
