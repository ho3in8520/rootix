const ticketUploadInput = document.querySelector('.ticket-upload__btn');

ticketUploadInput.addEventListener('change', (e) => {
  const photoName = document.querySelectorAll('.photo__name');
  const [file] = e.target.files;

  photoName.forEach((item) => {
    item.innerHTML = file.name;
  });
});
