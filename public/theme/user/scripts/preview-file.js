const gallery = [...document.querySelectorAll('.upload__image')];
let dropAreas = [...document.querySelectorAll('.drop-area')];
const inputsFile = [...document.querySelectorAll('.input__file')];

inputsFile.forEach((input, index) => {
  input.addEventListener('change', (e) => {
    let [file] = e.target.files;
    console.log(file);
    if (file) {
      const img = document.createElement('img');
      img.src = URL.createObjectURL(file);
      console.log(img);
      const currentGallery = gallery[index];
      const i = icon();
      currentGallery.innerHTML = '';
      currentGallery.appendChild(i);
      currentGallery.appendChild(img);

      const parentRemoveIcon = i.parentElement;
      i.addEventListener('click', () => {
        parentRemoveIcon.innerHTML = '';
        file = {};
      });
    }
  });
});

dropAreas.forEach((dropArea, index) => {
  // Prevent default drag behaviors
  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach((eventName) => {
    // console.log(dropArea)
    dropArea.addEventListener(eventName, preventDefaults, false);
    document.body.addEventListener(eventName, preventDefaults, false);
  });

  ['dragenter', 'dragover'].forEach((eventName) => {
    dropArea.addEventListener(eventName, highlight, false);
  });
  ['dragleave', 'drop'].forEach((eventName) => {
    dropArea.addEventListener(eventName, unhighlight, false);
  });

  // Handle dropped files
  dropArea.addEventListener('drop', (e) => {
    handleDrop(e, index);
  });

  function highlight(e) {
    dropArea.classList.add('highlight');
  }

  function unhighlight(e) {
    dropArea.classList.remove('active');
  }

  function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
  }
});

// Highlight drop area when item is dragged over it

function handleDrop(e, index) {
  const dt = e.dataTransfer;
  // const currentBox = dropAreas[index];
  const files = dt.files;
  handleFiles(files, index);
}

function handleFiles(files, index) {
  files = [...files];

  files.forEach((file) => {
    previewFile(file, index);
  });
}

// function handleInputFiles(e, index) {
//   console.log(index);
//   const [file] = e.target.files;
//   console.log(file);
//   if (file) {
//     const img = document.createElement("img");
//     img.src = URL.createObjectURL(file);
//     const currentGallery = gallery[index];
//     const i = icon();
//     currentGallery.innerHTML = "";
//     currentGallery.appendChild(i);
//     currentGallery.appendChild(img);
//     const parentRemoveIcon = i.parentElement;
//     i.addEventListener("click", () => {
//       parentRemoveIcon.innerHTML = "";
//     });
//   }
// }

function icon() {
  const i = document.createElement('i');
  i.classList = 'fa fa-times remove-img';

  return i;
}

function previewFile(file, index) {
  const currentGallery = gallery[index];

  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = function () {
    currentGallery.innerHTML = '';
    let img = document.createElement('img');

    const i = icon();
    img.src = reader.result;
    currentGallery.appendChild(i);
    const parentRemoveIcon = i.parentElement;
    i.addEventListener('click', () => {
      parentRemoveIcon.innerHTML = '';
    });
    currentGallery.appendChild(img);
  };
}
