const menuIcon = document.querySelector('.bars-btn');
const sidebar = document.querySelector('.sidebar');
const sidebarContainer = document.querySelector('.sidebar-container');
const content = document.querySelector('.content');
const menu = document.querySelector('.container-menu');
const body = document.querySelector('body');
const openSidebarBtn = document.querySelector('.open__sidebar');
const responsiveSidebar = document.querySelector(".responsive-sidebar")

openSidebarBtn.addEventListener('click', () => {
  const sidebarBtnAttribute = openSidebarBtn.getAttribute('isClosed');
  if (sidebarBtnAttribute === 'true') {
    openSidebarBtn.setAttribute('isClosed', 'false');

    const arr = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38" height="38" viewBox="0 0 48 48"><defs><filter id="a" x="0" y="0" width="48" height="48" filterUnits="userSpaceOnUse"><feOffset input="SourceAlpha"/><feGaussianBlur stdDeviation="3" result="b"/><feFlood flood-opacity="0.161"/><feComposite operator="in" in2="b"/><feComposite in="SourceGraphic"/></filter></defs><g transform="translate(-1550 -80)"><g transform="matrix(1, 0, 0, 1, 1550, 80)" filter="url(#a)"><circle cx="15" cy="15" r="15" transform="translate(39 39) rotate(180)" fill="#fff"/></g><path d="M19.824,12.885l-4.691,4.681-4.691-4.681L9,14.326l6.133,6.133,6.133-6.133Z" transform="translate(1557.328 119.132) rotate(-90)" opacity="0.6"/></g></svg>`;
    openSidebarBtn.innerHTML = arr;

    $('.sidebar-menu__svg > div').tooltip('disable');
  } else {
    $('.sidebar-menu__svg > div').tooltip('enable');

    const arr = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="38" height="38" viewBox="0 0 48 48"><defs><filter id="a" x="0" y="0" width="48" height="48" filterUnits="userSpaceOnUse"><feOffset input="SourceAlpha"/><feGaussianBlur stdDeviation="3" result="b"/><feFlood flood-opacity="0.161"/><feComposite operator="in" in2="b"/><feComposite in="SourceGraphic"/></filter></defs><g transform="translate(-1796 -80)"><g transform="matrix(1, 0, 0, 1, 1796, 80)" filter="url(#a)"><circle cx="15" cy="15" r="15" transform="translate(9 9)" fill="#fff"/></g><path d="M19.824,12.885l-4.691,4.681-4.691-4.681L9,14.326l6.133,6.133,6.133-6.133Z" transform="translate(1836.672 88.868) rotate(90)" opacity="0.6"/></g></svg>`;
    openSidebarBtn.innerHTML = arr;
    openSidebarBtn.setAttribute('isClosed', 'true');
  }

  sidebar.classList.toggle('active');
  content.classList.toggle('active');
  sidebarContainer.classList.toggle('active');
});

menuIcon.addEventListener('click', () => {
  sidebar.classList.toggle('active');
  content.classList.toggle('active');
  menu.classList.toggle('active');
  body.classList.toggle('active');
});

document.addEventListener('click', (evt) => {
  let targetElement = evt.target;
  const windowWidth = window.innerWidth;
  if (windowWidth <= 992) {
    do {
      if (targetElement == sidebar || targetElement == menuIcon || targetElement == responsiveSidebar) {
        return;
      }

      targetElement = targetElement.parentNode;
    } while (targetElement);

    menu.classList.remove('active');
    content.classList.remove('active');
    sidebar.classList.remove('active');
    body.classList.remove('active');
  }
});

// Sub Menu
const sidebarSubMenuItems = document.querySelectorAll('.sidebar__submenu-item');
const sidebarSubMenuItemDivEl = document.querySelectorAll(
  '.sidebar__submenu-item .sidebar-menu__link'
);

sidebarSubMenuItemDivEl.forEach((item, index) => {
  item.addEventListener('click', () => {
    const sidebarSubMenuItem = sidebarSubMenuItems[index];
    const subMenu = sidebarSubMenuItem.querySelector('.submenu');
    const subMenuAttr = subMenu.getAttribute('data-is-open');

    if (subMenu) {
      sidebar.classList.add('active');
      content.classList.add('active');
      sidebarContainer.classList.add('active');
      openSidebarBtn.setAttribute('isClosed', 'false');
      if (subMenuAttr === 'true') {
        sidebarSubMenuItem.style.height = `${50}px`;
        subMenu.setAttribute('data-is-open', 'false');
        $('.sidebar-menu__svg > div').tooltip('enabled');
      } else {
        $('.sidebar-menu__svg > div').tooltip('disable');
        sidebarSubMenuItem.style.height = `${subMenu.offsetHeight + 60}px`;
        subMenu.setAttribute('data-is-open', 'true');
      }
    }
  });
});
