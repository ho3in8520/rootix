const radios1 = document.getElementsByName('gender');
const content1 = document.getElementById('content-1');
const radios2 = document.getElementsByName('last');
const content2 = document.getElementById('content-2');

const isChecked = (radioBtn) => radioBtn.checked;

const contentShowing = (radioCollection, currentContent) => {
  radioCollection.forEach((radio) => {
    radio.addEventListener('change', () => {
      isChecked(radioCollection[1])
        ? currentContent.classList.add('active')
        : currentContent.classList.remove('active');
    });
  });
};

contentShowing(radios1, content1);
contentShowing(radios2, content2);
