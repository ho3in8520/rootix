const filterBtns = [...document.querySelectorAll(".filter__btn")];

filterBtns.forEach((btn) => {
  btn.addEventListener("click", () => {
    filterBtns.forEach((newBtn) => {
      newBtn.classList.remove("active");
    });
    btn.classList.add("active");
  });
});