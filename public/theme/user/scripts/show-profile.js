const profileBtn = document.querySelector('.profile__btn');
const profileMenu = document.querySelector('.profile__menu');

profileBtn.addEventListener('click', () => {
  profileMenu.classList.toggle('active');
});

document.addEventListener('click', (e) => {
  if (!profileBtn.contains(e.target) && !profileMenu.contains(e.target)) {
    profileMenu.classList.remove('active');
  }
});
