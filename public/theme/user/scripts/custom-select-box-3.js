$.fn.customSelect3 = function (func_name) {
  let elem2 = $(this);
  $(elem2).css('display', 'none');
  $(elem2).attr('id', 'none');
  $(elem2).after(html3);
  let class_name2 = [];
  $('.custom-select-3').each(function (i, v) {
    let class_na2 = '';
    $(v).attr('id', 'custom-select-id-' + i++);
    class_na2 = $(v).attr('class').split(' ');
    class_na2 = class_na2.filter(function (item) {
      return item !== 'form-control' && item !== 'custom-select-3';
    });
    class_name2.push(class_na2.join(' '));
  });

  $('.select-parent-3').each(function (i, v) {
    let elem_custom_select2 = $(this);
    let elem_select2 = $('#custom-select-id-' + i);
    let name2 = $(elem_select2).attr('name');
    let b = 0;
    $(v).addClass(class_name2[i]);
    let text2 = $('#custom-select-id-' + i + ' option:selected').text();
    let image2 = $('#custom-select-id-' + i + ' option:selected').data('image');
    $(elem_custom_select2).find('.custom__select-title-3').text(text2);
    if (image2 && image2 != 'undefined') {
      $(elem_custom_select2).find('.custom__select-img-3').attr('src', image2);
    } else {
      $(elem_custom_select2).find('.custom__select-img-3').remove();
    }
    $('#custom-select-id-' + i + ' option').each(function (i, v) {
      let list2 =
        '<li class="custom-list__item-3" data-value="' +
        $(v).attr('value') +
        '">';
      if ($(v).data('image')) {
        list2 += '<img src="' + $(v).data('image') + '" alt="curreny title"/>';
      }
      list2 += '<span>' + $(v).text() + '</span></li>';
      $(elem_custom_select2).find('.custom-list-3').append(list2);
    });
    $(v).attr('data-name', name2);
    $(v).attr('id', 'custom-select-parent-id-' + i++);
  });
  $(document).on('click', '.custom__select-btn-3', function () {
    $('.custom-list-3').removeClass('active');

    $(this)
      .closest('.custom__select-3')
      .find('.custom-list-3')
      .addClass('active');

    $(this).addClass('active');
  });

  $(document).on('click', '.custom-list__item-3', function () {
    $(this)
      .closest('.custom__select-3')
      .find('.custom-list-3')
      .attr('class', 'custom-list-3');
    $('.custom__select-btn-3').removeClass('active');

    let lable2 = $(this).find('span').text();
    let image2 = $(this).find('img').attr('src');
    $(this)
      .closest('.select-parent-3')
      .find('.custom__select-title-3')
      .text(lable2);
    $(this)
      .closest('.select-parent-3')
      .find('.custom__select-img-3')
      .attr('src', image2);

    let val2 = $(this).data('value');
    let select_name2 = $(this).closest('.select-parent-3').data('name');
    $('select[name=' + select_name2 + '] option').removeAttr('selected');
    $('select[name=' + select_name2 + '] option[value=' + val2 + ']').attr(
      'selected',
      'selected'
    );
    if (func_name != null) {
      func_name(val2);
    }
  });

  $(document).mouseup(function (e) {
    let container2 = $('.select-parent-3');
    if (!container2.is(e.target) && container2.has(e.target).length === 0) {
      $('.custom-list-3').removeClass('active');
      $('.custom__select-btn-3').removeClass('active');
    }
  });
};

let html3 =
  '<div class="select-parent-3" data-name="' +
  name +
  '">\n' +
  '    <div class="custom__select-3">\n' +
  '        <div class="custom__select-btn-3">\n' +
  '            <div class="custom__select-btn__content-3">\n' +
  '                <img\n' +
  '                        src=""\n' +
  '                        alt="BTC"\n' +
  '                        class="custom__select-img-3"\n' +
  '                />\n' +
  '                <span class="custom__select-title-3"></span>\n' +
  '                <svg\n' +
  '                        xmlns="http://www.w3.org/2000/svg"\n' +
  '                        width="13"\n' +
  '                        height="11"\n' +
  '                        viewBox="0 0 18 11.115"\n' +
  '                >\n' +
  '                    <path\n' +
  '                            id="Icon_material-expand-more"\n' +
  '                            data-name="Icon material-expand-more"\n' +
  '                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"\n' +
  '                            transform="translate(-9 -12.885)"\n' +
  '                    />\n' +
  '                </svg>\n' +
  '            </div>\n' +
  '        </div>\n' +
  '        <ul class="custom-list-3">\n' +
  '        </ul>\n' +
  '\n' +
  '    </div>\n' +
  '</div>';
