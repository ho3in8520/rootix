function chart(id,color){
    if (!color)
        color="#627EEA";
    let back_color=colorConvert(color,0.2);
    var ctx = document.getElementById(id).getContext("2d");
    let dataset=[];
    for (let i=0;i<15;i++){
        let num=(Math.random() * (0.004 - 0.001)+0.001).toFixed(4);
        dataset.push(num);
    }
    const labels = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];

    const data = {
        labels: labels,
        datasets: [
            {
                data: dataset,
                backgroundColor: back_color,
                fill: true,
                borderColor: color,
                borderWidth: 1.5,
                tension: 0.2
            },
        ]
    };
    new Chart(ctx, {
        type: 'line',
        data: data,
        options: {
            elements: {
                point: {
                    radius: 0
                },

            },
            plugins: {
                legend: {
                    display: false
                },
            },
            scales: {
                x: {
                    display:false,
                    grid: {
                        display: false
                    }
                },
                y: {
                    display:false,
                    grid: {
                        display: false
                    }
                }
            },
        }
    });
}

function colorConvert(color, transparency) {
    color=color.replace('#','');
    var r = parseInt(color.substring(0,2),16);
    var g = parseInt(color.substring(2,4),16);
    var b = parseInt(color.substring(4,6),16);
    return ('rgba(' + r +', ' + g + ', ' + b + ', ' + transparency + ')');
}
