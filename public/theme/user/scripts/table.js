window.addEventListener('load', () => {
  getTableData();
});

const getTableData = async () => {
  const url = 'http://localhost:5501/panel/scripts/walletTableData.json';
  const response = await fetch(url, {
    headers: {
      ContentType: 'application/json',
    },
  });
  const data = await response.json();

  const nweData = JSON.parse(data);

  console.log(nweData);
};
