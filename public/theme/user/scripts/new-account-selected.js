const swapSelected = document.querySelector('.first-volume-form__menu-3');
const swapSelectedChildren = [
  ...document.querySelectorAll('.first-volume-form__menu-3 li'),
];
const volumeForm9 = document.querySelector('.volume-form');
const volumeForm__selected7 = document.querySelector('.volume-form__selected');

const hasChild = (swapSelected, swapSelectedChildren) => {
  if (swapSelectedChildren.length >= 6) {
    swapSelected.style.overflowY = 'scroll';
  } else {
    swapSelected.style.overflowY = 'visible';
    swapSelected.style.height = 'auto';
  }
};

document.addEventListener('DOMContentLoaded', () => {
  hasChild(swapSelected, swapSelectedChildren);
});

document.addEventListener('click', (evt) => {
  let targetElement = evt.target; // clicked element
  do {
    if (targetElement == swapSelected || targetElement == volumeForm9) {
      return;
    }

    targetElement = targetElement.parentNode;
  } while (targetElement);

  volumeForm__selected7.classList.remove('active');
});
