const filterBox = document.querySelector('.filter__list-container');
const filterList = document.querySelector('.filter__list');
const filterArrowBtnLeft = document.querySelector('.filter__arrow-btn-left');
const filterArrowBtnRight = document.querySelector('.filter__arrow-btn-right');
const filterItem = [...document.querySelectorAll('.filter__item')];

filterArrowBtnLeft.addEventListener('click', counterScrollPlus);
filterArrowBtnRight.addEventListener('click', counterScrollMinus);
filterBox.addEventListener('scroll', checkDisableBtn);

let currentFilterScroll = 0;

function counterScrollPlus() {
  currentFilterScroll += -filterList.clientWidth;
  filterBox.scroll({
    left: currentFilterScroll,
    behavior: 'smooth',
  });

  console.log(filterBox.scrollLeft);
  checkDisableBtn();
}

checkDisableBtn();

function counterScrollMinus() {
  currentFilterScroll -= -filterList.clientWidth;
  filterBox.scroll({
    left: currentFilterScroll,
    behavior: 'smooth',
  });
  console.log(currentFilterScroll);
  checkDisableBtn();
}

let sum = 0;
for (let index = 0; index <= filterItem.length - 1; index++) {
  sum += filterItem[index].offsetWidth;
}

console.log(sum);

function checkDisableBtn() {
  if (currentFilterScroll === 0) {
    filterArrowBtnRight.disabled = true;
    filterArrowBtnRight.classList.add('hide');
  } else {
    filterArrowBtnRight.disabled = false;
    filterArrowBtnRight.classList.remove('hide');
  }

  console.log('yes');
  //   if (currentFilterScroll === 0) {
  //     filterArrowBtnRight.disabled = true;
  //     filterArrowBtnRight.classList.add("hide");
  //   } else {
  //     filterArrowBtnRight.disabled = false;
  //     filterArrowBtnRight.classList.remove("hide");
  //   }
}
