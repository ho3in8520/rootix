$(document).ready(function () {
    $("ul.mr7-select li").click(function () {
        $(this).parent().children('li').removeClass('selected');
        $(this).addClass('selected');
    });

    var clear_notif= function () {
        $("ul.notification").html('<li class="text-center"><h6>چیزی برای نمایش وجود ندارد</h6></li>');
    }

    $(".btn-delete").click(function () {
        ajax({
            action: '/notifications/clear_all',
            isObject: true,
            method: 'delete',
            button: $(this)
        },clear_notif)
    })


    // برای اینکه بگیم کاربر اطلاعیه هارو سین کرده
    $('.notification-tab[data-type=announcement]').click(function () {
        let form= {
            isObject: true,
            action: '/visit-ann',
            method: 'post',
            data: {},
        };
        ajax(form);
    });

    // اضافه کردن بردن در صورت focus شدن روی اینپوت
    $(".input-border-focus input").focus(function () {
        $(this).parent('.input-border-focus').addClass('input-border');
    }).blur(function(){
        $(this).parent('.input-border-focus').removeClass('input-border');
    })

})
