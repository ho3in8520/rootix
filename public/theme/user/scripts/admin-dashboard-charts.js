const getOrCreateTooltip = (chart) => {
  let tooltipEl = chart.canvas.parentNode.querySelector('div');

  if (!tooltipEl) {
    tooltipEl = document.createElement('div');
    tooltipEl.classList.add('tooltip-design');
    tooltipUl = document.createElement('ul');
    tooltipUl.classList.add('tooltip-ul');

    tooltipEl.appendChild(tooltipUl);
    chart.canvas.parentNode.appendChild(tooltipEl);
  }
  return tooltipEl;
};

const externalTooltipHandler = (context) => {
  const { chart, tooltip } = context;
  const tooltipEl = getOrCreateTooltip(chart);

  if (tooltip.opacity === 0) {
    tooltipEl.style.opacity = 0;
    return;
  }

  if (tooltip.body) {
    const bodyLine = tooltip.body.map((b) => b.lines);
    const tooltipLi = document.createElement('li');

    bodyLine.forEach((body, i) => {
      const colors = tooltip.labelColors[i];
      tooltipBodyP = document.createElement('p');
      tooltipBodyP.textContent = body;
      tooltipBodyP.style.color = colors.backgroundColor;
    });

    const ulNode = tooltipEl.querySelector('ul');

    while (ulNode.firstChild) {
      ulNode.firstChild.remove();
    }

    ulNode.appendChild(tooltipLi);
    tooltipLi.appendChild(tooltipBodyP);
    tooltipEl.style.opacity = 1;

    // position of tooltip
    const { offsetLeft: positionX, offsetTop: positionY } = chart.canvas;

    tooltipEl.style.left = `${positionX + tooltip.caretX}px`;
    tooltipEl.style.top = `${positionY + tooltip.caretY}px`;
  }
};

var ctx = document.getElementById('market-volume-chart').getContext('2d');

var gradientStroke = ctx.createLinearGradient(0, 0, 0, 0);

var gradientFill = ctx.createLinearGradient(0, 500, 0, 100);
gradientFill.addColorStop(1, '#bf4cbf9c');
gradientFill.addColorStop(0, '#6E3697');

var gradientStroke2 = ctx.createLinearGradient(0, 0, 0, 0);

var gradientFill2 = ctx.createLinearGradient(0, 500, 0, 100);
gradientFill2.addColorStop(1, '#f9d02ac7');
gradientFill2.addColorStop(0, '#E15921');

var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [
      '10AM',
      '11AM',
      '12AM',
      '01AM',
      '02AM',
      '03AM',
      '04AM',
      '02AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
    ],

    datasets: [
      {
        label: 'My first Chart',
        borderColor: gradientStroke,
        pointBackgroundColor: gradientStroke,
        fill: true,
        backgroundColor: gradientFill,
        data: [0, 12, 5, 17, 9, 15, 14, 8, 17, 13, 18, 9, 14, 20, 10, 8, 0],
        tension: 0.5,
        pointHitRadius: 30,
      },
      {
        label: 'My Second Chart',
        borderColor: gradientStroke,
        pointBackgroundColor: gradientStroke,
        fill: true,
        backgroundColor: gradientFill2,
        data: [0, 14, 7, 18, 13, 21, 14, 30, 22, 25, 15, 22, 16, 15, 14, 16, 0],
        tension: 0.5,
      },
    ],
  },
  options: {
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        grid: {
          borderDash: [3, 3],
        },
      },
    },
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          return tooltipItem.yLabel;
        },
      },
    },
  },
});

// chart 2

var ctx2 = document.getElementById('site-view').getContext('2d');

var gradientStroke = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill = ctx2.createLinearGradient(0, 500, 0, 100);

var gradientStroke2 = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill2 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill2.addColorStop(1, '#5533ff4b');
gradientFill2.addColorStop(0.9, '#20bcff63');
gradientFill2.addColorStop(0, '#a5fecc28');

var myChart2 = new Chart(ctx2, {
  type: 'line',
  data: {
    labels: [
      '10AM',
      '11AM',
      '12AM',
      '01AM',
      '02AM',
      '03AM',
      '04AM',
      '02AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
      '04AM',
      '03AM',
    ],

    datasets: [
      {
        label: 'My Second Chart',
        borderColor: gradientStroke,
        borderWidth: 2,
        borderColor: '#7A89FF',
        pointBackgroundColor: gradientStroke,
        fill: true,
        backgroundColor: gradientFill2,
        data: [0, 14, 7, 18, 13, 21, 14, 30, 22, 25, 15, 22, 16, 15, 14, 16, 0],
        tension: 0.5,
      },
    ],
  },
  options: {
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        grid: {
          borderDash: [3, 3],
        },
      },
    },
    legend: {
      display: false,
    },
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      callbacks: {
        label: function (tooltipItem) {
          return tooltipItem.yLabel;
        },
      },
    },
  },
});

// Bar Chart

const barCTX = document.getElementById('bar-chart');

const getOneWeekAgo = () =>
  Array.from({ length: 7 }).map((_, index) => {
    const ourDate = new Date();
    const pastDate = ourDate.getDate() - index + 1;
    ourDate.setDate(pastDate);

    const currentDate = ourDate.toLocaleDateString('fa-IR');

    return currentDate;
  });

const chartLabels = {
  daily: [
    'شنبه',
    'یکشنبه',
    'دو شنبه',
    'سه شنبه',
    'چهارشنبه',
    'پنجشنبه',
    'جمعه',
  ],
  weekly: getOneWeekAgo(),
  monthly: [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند',
  ],
};

const data3 = {
  labels: [
    'فروردین',
    'اردیبهشت',
    'خرداد',
    'تیر',
    'مرداد',
    'شهریور',
    'مهر',
    'آبان',
    'آذر',
    'دی',
    'بهمن',
    'اسفند',
  ],
  datasets: [
    {
      barPercentage: 0.3,
      label: 'My First Dataset',
      data: [65, 59, 80, 81, 56, 55, 40, 20, 31, 42, 80, 90],
      backgroundColor: '#0062FF',
      borderColor: 'transparent',
      borderWidth: 0,
    },
  ],
};

const config3 = {
  type: 'bar',
  data: data3,
  options: {
    plugins: {
      legend: {
        labels: {
          font: {
            family: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          },
        },
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        beginAtZero: true,
        grid: {
          borderDash: [3, 3],
        },
      },
    },
  },
};

const barChart = new Chart(barCTX, config3);

// doughnut chart

const doughnutCTX = document.getElementById('doughnut-chart').getContext('2d');

var gradientStroke = doughnutCTX.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = doughnutCTX.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, 'transparent');
gradientFill3.addColorStop(0, 'transparent');

var gradientStroke3 = doughnutCTX.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = doughnutCTX.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, '#FF9800');

var gradientFill4 = doughnutCTX.createLinearGradient(0, 500, 0, 100);
gradientFill4.addColorStop(1, '#6563FF');

var gradientFill5 = doughnutCTX.createLinearGradient(0, 500, 0, 100);
gradientFill5.addColorStop(1, '#739BEE');

const secondChartLabel = (ctx) => {
  console.log(ctx);
  return ctx.parsed + '%';
};

var myChart3 = new Chart(doughnutCTX, {
  type: 'doughnut',
  data: {
    datasets: [
      {
        label: ['BTC', 'USD', 'ETH', 'FHO', 'OAP', 'TRX'],
        data: [10, 29, 18, 25, 29],
        borderColor: gradientStroke3,
        fill: true,
        backgroundColor: [
          '#5B5BA4',
          '#6CC1B2',
          '#FFB960',
          '#E94E6D',
          '#18A6E3',
        ],
        borderWidth: 0,
        cutout: '65%',
      },
    ],
  },
  options: {
    plugins: {
      tooltip: {
        callbacks: {
          label: function (ctx) {
            console.log(ctx);
            const title = ctx.dataset.label[ctx.dataIndex];
            const value = ctx.parsed;
            const sum = ctx.dataset.data.reduce(
              (ack, currentItem) => ack + currentItem,
              0
            );
            const percent = Math.round((value / sum) * 100);

            return `${title} : ${percent}%`;
          },
        },
        displayColors: false,
      },
    },
  },
});

const assetsChartInfo = document.querySelector('.currency-result');

window.addEventListener('load', () => {
  const chartInfo = myChart3.data.datasets[0];

  const sum = chartInfo.data.reduce((accumulator, curr) => accumulator + curr);

  chartInfo.data.forEach((label, index) => {
    createElement(chartInfo, index, label, sum);
  });
});

function createElement(chartInfo, index, label, sum) {
  const colorEl = document.createElement('span');
  const color = chartInfo.backgroundColor[index];

  colorEl.classList = 'chart-color';
  colorEl.style.border = `3.7px solid ${color}`;

  const chartInfoContainer = document.createElement('div');
  chartInfoContainer.classList = 'chartInfoContainer';
  const labelBtnEl = document.createElement('button');
  labelBtnEl.classList = 'assets-chart__title w-100';

  const labelTextEl = document.createElement('h5');
  labelTextEl.textContent = ` : ${chartInfo.label[index]}`;

  const percentEl = document.createElement('span');
  const percent = Math.round((label / sum) * 100);
  percentEl.textContent = percent + '%';

  chartInfoContainer.appendChild(percentEl);

  chartInfoContainer.appendChild(labelTextEl);
  labelBtnEl.appendChild(colorEl);
  labelBtnEl.appendChild(chartInfoContainer);

  assetsChartInfo.appendChild(labelBtnEl);
}

// cart-chart

const cartCharts = [...document.querySelectorAll('.cart-chart')];

cartCharts.forEach((ctx) => {
  const ctx2 = ctx.getContext('2d');
  var gradientStroke = ctx2.createLinearGradient(0, 0, 0, 0);

  var gradientFill = ctx2.createLinearGradient(0, 500, 0, 100);

  var gradientStroke2 = ctx2.createLinearGradient(0, 0, 0, 0);

  gradientStroke2.addColorStop(1, 'red');
  gradientStroke2.addColorStop(0, 'blue');

  var gradientFill2 = ctx2.createLinearGradient(0, 500, 0, 100);
  gradientFill2.addColorStop(1, 'transparent');
  gradientFill2.addColorStop(0, 'transparent');

  let width, height, gradient;
  function getGradient(ctx, chartArea) {
    const chartWidth = chartArea.left - chartArea.right;
    const chartHeight = chartArea.bottom - chartArea.top;
    if (!gradient || width !== chartWidth || height !== chartHeight) {
      // Create the gradient because this is either the first render
      // or the size of the chart has changed
      width = chartWidth;
      height = chartHeight;
      gradient = ctx.createLinearGradient(
        0,
        chartArea.left,
        0,
        chartArea.right
      );
      gradient.addColorStop(0, '#E15921');
      gradient.addColorStop(0.3, '#F9CE2A');
    }

    return gradient;
  }

  var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
      labels: [
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
      ],

      datasets: [
        {
          label: '',
          borderColor: function (context) {
            const chart = context.chart;
            const { ctx, chartArea } = chart;

            if (!chartArea) {
              // This case happens on initial chart load
              return;
            }
            return getGradient(ctx, chartArea);
          },
          borderWidth: 4,
          pointBackgroundColor: '#fff',
          fill: true,
          backgroundColor: gradientFill2,
          data: [
            0, 14, 7, 18, 13, 21, 14, 30, 22, 25, 15, 22, 16, 15, 14, 16, 0,
          ],
          tension: 0.5,
        },
      ],
    },
    options: {
      plugins: {
        legend: {
          labels: {
            boxWidth: 0,
          },
        },
      },
      elements: {
        point: {
          radius: 0,
        },
      },
      scales: {
        x: {
          display: false,
          grid: {
            display: false,
          },
          ticks: {
            display: false,
          },
        },
        y: {
          display: false,
          grid: {
            display: false,
          },
          ticks: {
            display: false,
          },
        },
      },

      responsive: true,
      maintainAspectRatio: false,

      tooltips: {
        callbacks: {
          label: function (tooltipItem) {
            return tooltipItem.yLabel;
          },
        },
      },
    },
  });
});

// var ctx2 = document.getElementById('cart-chart').getContext('2d');
