$(document).ready(function () {
    $('.add-a-role-link').click(function () {
        $(this).toggleClass('active');
    });

    $(document).on('click', '.delete-person', function () {
        let elem = $(this)
        let id = $(this).closest('.support').data('id')
        ajax_role(id, 'delete', elem)
    })


    $('.new-persons li button').click(function (e, z) {
        let elem = $(this)
        let id = $(elem).data('id')
        ajax_role(id, 'add', elem)


    });
});

function ajax_role(id, type = 'add', params = {}) {
    swal({
        title: "اطمینان از اضافه کردن نقش به کاربر دارید؟",
        icon: "warning",
        buttons: ['انصراف', 'تایید'],
    })
        .then((willDelete) => {
            if (willDelete) {
                let form = {
                    action: action_role,
                    method: 'post',
                    data: {
                        tab: 'roles',
                        id: id,
                        type: type,
                    },
                    isObject: true
                }
                ajax(form, response_role, {params, type})
            }
        });
}

function response_role(response, elem) {

    if (response.status == 100 && elem.type == 'add') {
        swal(response.msg, '', 'success')
        $(elem.params).attr('disabled', 'true');
        $(".empty-role").remove()
        add_item_role(response.id, response.name, response.date)
    } else if (response.status == 100 && elem.type == 'delete') {
        let id = $(elem.params).closest('.support').data('id')
        $(".support[data-id='" + id + "']").remove()
        $(".new-persons button[data-id='" + id + "']").removeAttr('disabled')
    } else {
        swal(response.msg, '', 'error')
    }
}

function add_item_role(id, name, date) {
    $('#person_list').append(`<div class="support" data-id=${id}>
    <div>
    <h6 class="normal-user">${name}</h6>
    <p class="date">${date}</p>
    </div>
    <div class="delete-person">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16.75"
      height="16.746"
      viewBox="0 0 16.75 16.746"
    >
      <path
        id="Icon_ionic-ios-close"
        data-name="Icon ionic-ios-close"
        d="M21.645,19.661l5.982-5.982A1.4,1.4,0,1,0,25.645,11.7l-5.982,5.982L13.68,11.7A1.4,1.4,0,1,0,11.7,13.679l5.982,5.982L11.7,25.644a1.4,1.4,0,0,0,1.982,1.982l5.982-5.982,5.982,5.982a1.4,1.4,0,0,0,1.982-1.982Z"
        transform="translate(-11.285 -11.289)"
        fill="#f31111"
      />
    </svg>
  </div>
    </div>  `);
}

