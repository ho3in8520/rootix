const volumeFormSelected = document.querySelector('.volume-form__selected-3');
const volumeForm = document.querySelector('.volume-form-3');
const volumeFormLinks = [
  ...volumeForm.querySelectorAll('.volume-form__link-3'),
];
const volumeFormSelectedTitleContainer = document.querySelector(
  '.volume-form__selected-desc-3'
);

const volumeFormSelected2 = document.querySelector('.volume-form__selected-4');
const volumeForm2 = document.querySelector('.volume-form-4');
const volumeFormLinks2 = [...document.querySelectorAll('.volume-form__link-4')];
const volumeFormSelectedTitleContainer2 = document.querySelector(
  '.volume-form__selected-desc-4'
);

function selectedToggle(volumeFormSelected) {
  volumeFormSelected.addEventListener('click', () => {
    volumeFormSelected.classList.toggle('active');
    volumeFormSelected;
    if (volumeFormSelected.classList.contains('active')) {
      volumeForm.style.borderBottomLeftRadius = '0px';
      volumeFormSelected.style.boxShadow = '0 0 10px #aaa';
    } else {
      volumeForm.style.borderBottomLeftRadius = '5px';
      volumeFormSelected.style.boxShadow = 'none';
    }
  });
}

function loopLinks(volumeFormLinks, volumeFormSelectedTitleContainer) {
  volumeFormLinks.forEach((volumeFormLink, index) => {
    volumeFormLink.addEventListener('click', (e) => {
      e.preventDefault();

      const volumeFormLinkChild = {
        imgUrl: volumeFormLink.querySelector('.volume-form__img-3')
          ? volumeFormLink.querySelector('.volume-form__img-3').src
          : null,
        title: volumeFormLink.querySelector('.volume-form__selected-title-3')
          .textContent,
      };

      createHtmlEl(volumeFormLinkChild, volumeFormSelectedTitleContainer);
    });
  });
}

function createHtmlEl(volumeFormLinkChild, volumeFormSelectedTitleContainer) {
  const currentCurrency = `
  ${
    volumeFormLinkChild.imgUrl
      ? `<img src=${volumeFormLinkChild.imgUrl} class="volume-form__img-3">`
      : ''
  }
  <span class="volume-form__selected-title-3">${
    volumeFormLinkChild.title
  }</span>
  <div class="arr">
    <svg
    xmlns="http://www.w3.org/2000/svg"
    width="13"
    height="11"
    viewBox="0 0 18 11.115"
    >
    <path
    id="Icon_material-expand-more"
    data-name="Icon material-expand-more"
    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
    transform="translate(-9 -12.885)"
    />
    </svg>
    </div>
    `;

  volumeFormSelectedTitleContainer.setAttribute('data-value', 'true');

  volumeFormSelectedTitleContainer.innerHTML = currentCurrency;
}

selectedToggle(volumeFormSelected, volumeForm);
loopLinks(volumeFormLinks, volumeFormSelectedTitleContainer);

selectedToggle(volumeFormSelected2, volumeForm2);
loopLinks(volumeFormLinks2, volumeFormSelectedTitleContainer2);
