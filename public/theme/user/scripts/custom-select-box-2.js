$.fn.customSelect2 = function (func_name) {
  var elem = $(this);
  $(elem).css('display', 'none');
  $(elem).attr('id', 'none');
  $(elem).after(html2);
  var class_name = [];
  $('.custom-select-2').each(function (i, v) {
    var class_na = '';
    $(v).attr('id', 'custom-select-id-' + i++);
    class_na = $(v).attr('class').split(' ');
    class_na = class_na.filter(function (item) {
      return item !== 'form-control' && item !== 'custom-select-2';
    });
    class_name.push(class_na.join(' '));
  });

  $('.select-parent-2').each(function (i, v) {
    var elem_custom_select = $(this);
    var elem_select = $('#custom-select-id-' + i);
    var name = $(elem_select).attr('name');
    var b = 0;
    $(v).addClass(class_name[i]);
    var text = $('#custom-select-id-' + i + ' option:selected').text();
    var image = $('#custom-select-id-' + i + ' option:selected').data('image');
    $(elem_custom_select).find('.custom__select-title-2').text(text);
    if (image && image != 'undefined') {
      $(elem_custom_select).find('.custom__select-img-2').attr('src', image);
    } else {
      $(elem_custom_select).find('.custom__select-img-2').remove();
    }
    $('#custom-select-id-' + i + ' option').each(function (i, v) {
      var list =
        '<li class="custom-list__item-2" data-value="' +
        $(v).attr('value') +
        '">';
      if ($(v).data('image')) {
        list += '<img src="' + $(v).data('image') + '" alt="curreny title"/>';
      }
      list += '<span>' + $(v).text() + '</span></li>';
      $(elem_custom_select).find('.custom-list-2').append(list);
    });
    $(v).attr('data-name', name);
    $(v).attr('id', 'custom-select-parent-id-' + i++);
  });
  $(document).on('click', '.custom__select-btn-2', function () {
    $('.custom-list-2').removeClass('active');
    $(this)
      .closest('.custom__select-2')
      .find('.custom-list-2')
      .addClass('active');
  });

  $(document).on('click', '.custom-list__item-2', function () {
    $(this)
      .closest('.custom__select-2')
      .find('.custom-list-2')
      .attr('class', 'custom-list-2');
    var lable = $(this).find('span').text();
    var image = $(this).find('img').attr('src');
    $(this)
      .closest('.select-parent-2')
      .find('.custom__select-title-2')
      .text(lable);
    $(this)
      .closest('.select-parent-2')
      .find('.custom__select-img-2')
      .attr('src', image);

    var val = $(this).data('value');
    var select_name = $(this).closest('.select-parent-2').data('name');
    $('select[name=' + select_name + '] option').removeAttr('selected');
    $('select[name=' + select_name + '] option[value=' + val + ']').attr(
      'selected',
      'selected'
    );
    if (func_name != null) {
      func_name(val);
    }
  });

  $(document).mouseup(function (e) {
    var container = $('.select-parent-2');
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $('.custom-list-2').removeClass('active');
    }
  });
};

var html2 =
  '<div class="select-parent-2" data-name="' +
  name +
  '">\n' +
  '    <div class="custom__select-2">\n' +
  '        <div class="custom__select-btn-2">\n' +
  '            <div class="custom__select-btn__content-2">\n' +
  '                <img\n' +
  '                        src=""\n' +
  '                        alt="BTC"\n' +
  '                        class="custom__select-img-2"\n' +
  '                />\n' +
  '                <span class="custom__select-title-2"></span>\n' +
  '                <svg\n' +
  '                        xmlns="http://www.w3.org/2000/svg"\n' +
  '                        width="13"\n' +
  '                        height="11"\n' +
  '                        viewBox="0 0 18 11.115"\n' +
  '                >\n' +
  '                    <path\n' +
  '                            id="Icon_material-expand-more"\n' +
  '                            data-name="Icon material-expand-more"\n' +
  '                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"\n' +
  '                            transform="translate(-9 -12.885)"\n' +
  '                    />\n' +
  '                </svg>\n' +
  '            </div>\n' +
  '        </div>\n' +
  '        <ul class="custom-list-2">\n' +
  '        </ul>\n' +
  '\n' +
  '    </div>\n' +
  '</div>';
