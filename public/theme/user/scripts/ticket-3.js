const ticketInfo = document.querySelector('.ticket-info__box');
const ticketBtn = document.querySelector('.closed-ticket');

ticketInfo.addEventListener('click', () => {
  ticketInfo.classList.toggle('active');
});

ticketBtn.addEventListener('click', (e) => {
  e.preventDefault();
  const chatBox = document.querySelectorAll('.chat-message__box');

  chatBox.forEach((box) => {
    box.classList.add('hidden');
  });
});
