const input = document.querySelector("#currency-value-input-1");
const input2 = document.querySelector("#currency-value-input-2");
const recordBtn = document.querySelector(".record-btn")

const checkCurrentActiveBtn = () => {
  const box = document.querySelector(".request-js-box-2");
  const box2 = document.querySelector(".request-js-box-3");

  if (input.value) {
    box2.classList.add("active");
    box2.classList.add("enabled");
    box.classList.remove("active")
  }else {
    box2.classList.remove("active");
    box2.classList.remove("enabled");
    box.classList.add("active")
  }

  
  if (input.value, input2.value) {
    recordBtn.disabled = false
  }else {
    recordBtn.disabled = true
  }
};

input.addEventListener("input", checkCurrentActiveBtn);
input2.addEventListener("input", checkCurrentActiveBtn);