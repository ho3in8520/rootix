const getOrCreateTooltip = (chart) => {
    let tooltipEl = chart.canvas.parentNode.querySelector("div");

    if (!tooltipEl) {
        tooltipEl = document.createElement("div");
        tooltipEl.classList.add("tooltip-design");
        tooltipUl = document.createElement("ul");
        tooltipUl.classList.add("tooltip-ul");

        tooltipEl.appendChild(tooltipUl);
        chart.canvas.parentNode.appendChild(tooltipEl);
    }
    return tooltipEl;
};

const externalTooltipHandler = (context) => {
    const { chart, tooltip } = context;
    const tooltipEl = getOrCreateTooltip(chart);

    if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    if (tooltip.body) {
        const bodyLine = tooltip.body.map((b) => b.lines);
        const tooltipLi = document.createElement("li");

        bodyLine.forEach((body, i) => {
            const colors = tooltip.labelColors[i];
            tooltipBodyP = document.createElement("p");
            tooltipBodyP.textContent = body;
            tooltipBodyP.style.color = colors.backgroundColor;
        });

        const ulNode = tooltipEl.querySelector("ul");

        while (ulNode.firstChild) {
            ulNode.firstChild.remove();
        }

        ulNode.appendChild(tooltipLi);
        tooltipLi.appendChild(tooltipBodyP);
        tooltipEl.style.opacity = 1;

        // position of tooltip
        const { offsetLeft: positionX, offsetTop: positionY } = chart.canvas;

        tooltipEl.style.left = `${positionX + tooltip.caretX}px`;
        tooltipEl.style.top = `${positionY + tooltip.caretY}px`;
    }
};

// Doughnut

var ctx2 = document.getElementById("wallet-assets-chart").getContext("2d");

var gradientStroke = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "transparent");
gradientFill3.addColorStop(0, "transparent");

var gradientStroke3 = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "#697AFD");
gradientFill3.addColorStop(0.8, "#A149DD");
gradientFill3.addColorStop(0, "#A149DD");

var gradientFill4 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill4.addColorStop(1, "#F9A826");
gradientFill4.addColorStop(0.8, "#FFEE00");
gradientFill4.addColorStop(0, "#FFEE00");

var myChart2 = new Chart(ctx2, {
    type: "doughnut",
    data: {
        datasets: [
            {
                label: ["دارایی کیف پول", "دارایی رمز ارز"],
                data: [parseFloat(chart_data.assets_chart.rial), parseFloat(chart_data.assets_chart.currency)],
                borderColor: gradientStroke3,
                fill: true,
                backgroundColor: [gradientFill3, gradientFill4],
                borderWidth: 1,
                cutout: "80%",
            },
        ],
    },

    options: {
        plugins: {
            tooltip: {
                callbacks: {
                    label: function (ctx) {
                        const title = ctx.dataset.label[ctx.dataIndex];
                        const value = ctx.parsed;
                        const sum = ctx.dataset.data.reduce(
                            (ack, currentItem) => ack + currentItem,
                            0
                        );
                        const percent = Math.round((value / sum) * 100);

                        return `${title} : ${percent}%`;
                    },
                },
                displayColors: false,
            },
        },
    },
});
