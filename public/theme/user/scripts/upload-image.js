const inputsImage = [...document.querySelectorAll('.input__image')];

inputsImage.forEach((input, index) => {
  input.addEventListener('change', () => {
    const imageContainer = [
      ...document.querySelectorAll('.image-upload-container'),
    ];
    const [file] = input.files;

    const regexImage = /\.(gif|jpe|jpg?|png)$/i;

    if (regexImage.test(file.name)) {
      imageContainer[index].innerHTML = '';
      const imageUpload = document.createElement('img');
      imageUpload.classList.add('image__upload');

      imageUpload.src = URL.createObjectURL(file);
      imageContainer[index].appendChild(imageUpload);
    } else {
      imageContainer[index].innerHTML = '';
      const errorUpload = document.createElement('p');
      errorUpload.classList.add('error');
      errorUpload.textContent = 'لطفا فقط عکس آپلود کنید.';
      imageContainer[index].appendChild(errorUpload);
    }
  });
});
