// Doughnut

var ctx2 = document.getElementById("wallet-assets-chart").getContext("2d");

var gradientStroke = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "transparent");
gradientFill3.addColorStop(0, "transparent");

var gradientStroke3 = ctx2.createLinearGradient(0, 0, 0, 0);

var gradientFill3 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill3.addColorStop(1, "#FF9800");


var gradientFill4 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill4.addColorStop(1, "#6563FF");

var gradientFill5 = ctx2.createLinearGradient(0, 500, 0, 100);
gradientFill5.addColorStop(1, "#739BEE");

var myChart = new Chart(ctx2, {
  type: "doughnut",
  data: {
    datasets: [
      {
        label: "# of Votes",
        data: [35, 30, 25],
        borderColor: gradientStroke3,
        fill: true,
        backgroundColor: [gradientFill3, gradientFill4, gradientFill5],
        borderWidth: 1,
        cutout: "56%",
      },
    ],
  },
  options: {
    plugins: {
    },
  },
});