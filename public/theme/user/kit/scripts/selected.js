const volumeFormSelected = document.querySelector(".volume-form__selected");
const volumeForm = document.querySelector(".volume-form");

volumeFormSelected.addEventListener("click", () => {
  volumeFormSelected.classList.toggle("active");
  if (volumeFormSelected.classList.contains("active")) {
    volumeForm.style.borderBottomLeftRadius = "0px";
  } else {
    volumeForm.style.borderBottomLeftRadius = "15px";
  }
});

const volumeFormLinks = [...document.querySelectorAll(".volume-form__link")];
const volumeFormSelectedDesc = document.querySelector(
  ".volume-form__selected-desc"
);
const arr = document.querySelector(".arr")
const volumeFormSelectedTitleContainer = document.querySelector(".volume-form__selected-title-container")

volumeFormLinks.forEach((volumeFormLink, index) => {
  volumeFormLink.addEventListener("click", () => {
    const volumeFormLinkChild = {
      imgUrl: volumeFormLink.querySelector(".volume-form__img").src,
      title: volumeFormLink.querySelector(".volume-form__selected-title")
        .textContent,
    };

    createHtmlEl(volumeFormLinkChild);
  });
});

function createHtmlEl(volumeFormLinkChild) {
  const img = document.createElement("img");
  img.src = volumeFormLinkChild.imgUrl;
  img.classList.add("volume-form__img");

  const title = document.createElement("span");
  title.textContent = volumeFormLinkChild.title;
  title.classList.add("volume-form__selected-title");

  const arrow = `<svg
  xmlns="http://www.w3.org/2000/svg"
  width="13"
  height="11"
  viewBox="0 0 18 11.115"
  >
  <path
  id="Icon_material-expand-more"
  data-name="Icon material-expand-more"
  d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
  transform="translate(-9 -12.885)"
  />
  </svg>`;
  setInfoToSelected(img, title, arrow);
}

function setInfoToSelected(img, title, arrow) {
  volumeFormSelectedTitleContainer.innerHTML = ""
  volumeFormSelectedTitleContainer.appendChild(img)
  volumeFormSelectedTitleContainer.appendChild(title)
  arr.innerHTML = arrow
}