$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


// گرفتن کیف پول های کاربر + تبدیل ارز ها به ریال + تبدیل ارز ها به تتر
function get_user_assets(callback) {
    $.ajax({
        url: 'get-user-assets',
        type: 'post',
        async: false,
        success: function (response) {
            if (response.status == '100') {
                let data= response.data;
                let assets= data.assets;
                if (assets.length > 0) {
                    callback(data)
                }
                else {
                    callback(false);
                }
            } else {
                swal('ناموفق', 'مشکلی بوجود آمده است لطفا با پشتیبانی سایت در ارتباط باشید', 'danger');
            }

        },
        error: function (data) {
            swal('ناموفق', 'مشکلی بوجود آمده است لطفا با پشتیبانی سایت در ارتباط باشید', 'danger');
        }
    });
}
