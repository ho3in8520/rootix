import {ethers} from "./ethers-5.0.esm.min.js";

$(document).ready(async function () {

    const AbiCoder = ethers.utils.AbiCoder;

    const ADDRESS_PREFIX_REGEX = /^(41)/;
    const ADDRESS_PREFIX = "41";

    async function encodeParams(inputs){
        let typesValues = inputs;
        let parameters = '';

        if (typesValues.length == 0)
            return parameters
        const abiCoder = new AbiCoder();
        let types = [];
        const values = [];

        for (let i = 0; i < typesValues.length; i++) {
            let {type, value} = typesValues[i];
            if (type == 'address')
                value = value.replace(ADDRESS_PREFIX_REGEX, '0x');
            else if (type == 'address[]')
                value = value.map(v => toHex(v).replace(ADDRESS_PREFIX_REGEX, '0x'));
            types.push(type);
            values.push(value);
        }

        try {
            parameters = abiCoder.encode(types, values).replace(/^(0x)/, '');
        } catch (ex) {
            console.log(ex);
        }
        return parameters

    }


    let inputs2 = [
        {type: 'address', value: address_user},
    ];

    let owner_address = await encodeParams(inputs2);


    $.ajax({
        url     : route_get_total_usdt,
        type    : 'POST',
        data    : {owner_address:owner_address,"_token":csrf},
        success : function(data){
            $.data(document,'usdt_amount',parseInt(data.substr(2), 16) / 1000000);

            if ( $('.usdt_balance').data('label')){
                $('.usdt_balance').text('مقدار موجودی تتر شما برابر است با '+  parseInt(data.substr(2), 16) / 1000000);
            }else{
                $('.usdt_balance').text(parseInt(data.substr(2), 16) / 1000000);
            }

        }
    });
});
