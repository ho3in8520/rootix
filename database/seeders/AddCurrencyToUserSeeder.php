<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Currency;
use App\Models\User;
use Illuminate\Database\Seeder;

class AddCurrencyToUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [];
        $users = User::select(['id',
            'token' => Asset::select('token')->whereColumn('users.id', 'assets.user_id')->limit(1)])->get();
        $currencies = Currency::select('id')->get(['id']);
        foreach ($users as $user) {
            if ($user->assets())
                $assets = $user->assets()->pluck('currency_id')->toArray();
            else
                $assets = [];
            $currencies = $currencies->whereNotIn('id', $assets);
            foreach ($currencies as $currency) {
                $asset = ['currency_id' => $currency->id, 'user_id' => $user->id, 'amount' => 0, 'token' => $user->token];
                array_push($array, $asset);
            }
        }

        foreach ($array as $item) {
            Asset::firstOrCreate([
                'user_id' => $item['user_id'],
                'currency_id' => $item['currency_id'],
            ], [
                'user_id' => $item['user_id'],
                'currency_id' => $item['currency_id'],
                'amount' => $item['amount'],
                'token' => $item['token'],
            ]);
        }
    }
}
