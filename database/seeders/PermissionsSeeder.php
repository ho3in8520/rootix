<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $permissions = [
        [
            'name' => 'dashboard_menu',
            'label' => 'منوی داشبورد',
        ],
        [
            'name' => 'wallet_menu',
            'label' => 'منوی کیف پول کاربر',
            'childs' => [
                [
                    'name' => 'rial_deposit',
                    'label' => 'دکمه واریز ریالی',
                    'childs' => [
                        [
                            'name' => 'rial_deposit_btn',
                            'label' => ' واریز ریالی',
                        ],
                    ],
                ],
                [
                    'name' => 'rial_withdraw',
                    'label' => 'دکمه برداشت ریالی',
                    'childs' => [
                        [
                            'name' => 'rial_withdraw_btn',
                            'label' => ' برداشت ریالی',
                        ],
                    ],
                ],
                [
                    'name' => 'crypto_deposit',
                    'label' => 'دکمه واریز کریپتو',
                    'childs' => [
                        [
                            'name' => 'crypto_deposit_btn',
                            'label' => ' واریز کریپتو',
                        ],
                    ],
                ],
                [
                    'name' => 'crypto_withdraw',
                    'label' => 'دکمه برداشت کریپتو',
                    'childs' => [
                        [
                            'name' => 'crypto_withdraw_btn',
                            'label' => ' برداشت کریپتو',
                        ],
                    ],
                ],
            ],
        ],
        [
            'name' => 'banks_menu',
            'label' => 'منوی بانک ها',
            'childs' => [
                [
                    'name' => 'banks_list_menu',
                    'label' => 'لیست بانک ها',
                    'childs' => [
                        [
                            'name' => 'bank_accept_btn',
                            'label' => 'تایید حساب بانکی کاربر',
                        ],
                        [
                            'name' => 'bank_reject_btn',
                            'label' => 'رد کردن حساب بانکی کاربر',
                        ],
                    ],
                ],
                [
                    'name' => 'user_bank_menu',
                    'label' => 'منوی حساب بانکی',
                    'childs' => [
                        [
                            'name' => 'bank_create',
                            'label' => 'ایجاد کردن بانک',
                        ],
                        [
                            'name' => 'bank_edit',
                            'label' => 'ویرایش کردن بانک',
                        ],
                        [
                            'name' => 'bank_accept',
                            'label' => 'تایید کردن بانک',
                        ],
                    ],
                ],
            ],
        ],
        [
            'name' => 'swap_menu',
            'label' => 'منوی تبادل',
            'childs' => [
                [
                    'name' => 'swap_store',
                    'label' => 'انجام تبادل',
                ],
            ],
        ],
        [
            'name' => 'trade_menu',
            'label' => 'منوی خرید فروش',
            'childs' => [
                [
                    'name' => 'rial_trade_buy',
                    'label' => 'دکمه خرید ریالی',
                ],
                [
                    'name' => 'rial_trade_sell',
                    'label' => 'دکمه فروش ریالی',
                ],
                [
                    'name' => 'usdt_trade_buy',
                    'label' => 'دکمه خرید تتری',
                ],
                [
                    'name' => 'usdt_trade_sell',
                    'label' => 'دکمه فروش تتری',
                ],
                [
                    'name' => 'rial_list',
                    'label' => 'منوی قیمت ارزها به ریال',
                ],
                [
                    'name' => 'usdt_list',
                    'label' => 'منوی قیمت ارزها به تتر',
                ],
            ],
        ],
        [
            'name' => 'user_setting_menu',
            'label' => 'منوی تنظیمات کاربر',
            'childs' => [
                [
                    'name' => 'authenticate_active',
                    'label' => 'دکمه فعال کردن گوگل اتنتیکتور',
                ],
                [
                    'name' => 'authenticate_deactive',
                    'label' => 'دکمه غیرفعال کردن گوگل اتنتیکتور',
                ],
            ],
        ],
        [
            'name' => 'tickets_menu',
            'label' => 'منوی تیکت ها',
            'childs' => [
                [
                    'name' => 'admin_new_tickets',
                    'label' => 'منوی تیکت های جدید (ادمین)',
                    'childs' => [
                        [
                            'name' => 'admin_show_ticket',
                            'label' => 'مشاهده تیکت(ادمین)',
                        ],
                        [
                            'name' => 'admin_close_ticket',
                            'label' => 'بستن تیکت(ادمین)',
                        ],
                        [
                            'name' => 'admin_new_ticket',
                            'label' => 'ایجاد تیکت(ادمین)',
                        ],
                    ],
                ],
                [
                    'name' => 'admin_my_tickets',
                    'label' => 'منوی تیکت های من',
                ],
                [
                    'name' => 'user_ticket_menu',
                    'label' => 'منوی تیکت',
                    'childs' => [
                        [
                            'name' => 'ticket_create',
                            'label' => 'ایجاد کردن تیکت',
                        ],
                        [
                            'name' => 'ticket_delete',
                            'label' => 'حذف کردن تیکت',
                        ],
                        [
                            'name' => 'ticket_show',
                            'label' => 'نمایش تیکت',
                        ],
                        [
                            'name' => 'ticket_close',
                            'label' => 'بستن تیکت',
                        ],
                    ],
                ],
            ],
        ],
        [
            'name' => 'list_menu',
            'label' => 'منوی لیست',
            'childs' => [
                [
                    'name' => 'user_transaction_menu',
                    'label' => 'منوی نمایش تراکنش ها',
                    'childs' => [
                    ],
                ],
                [
                    'name' => 'rial_transaction_menu',
                    'label' => 'منوی درخواست برداشت ریالی',
                ],
                [
                    'name' => 'crypto_transaction_menu',
                    'label' => 'منوی درخواست برداشت ارز',
                ],
            ],
        ],
        [
            'name' => 'profile_btn',
            'label' => 'منوی پروفایل',
            'childs' => [
                [
                    'name' => 'about_me',
                    'label' => 'نمایش باکس درباره من',
                ],
                [
                    'name' => 'change_password',
                    'label' => 'تغییر رمز عبور',
                ],
                [
                    'name' => 'authenticate_btn',
                    'label' => 'احراز هویت',
                ],
            ],
        ],
        [
            'name' => 'users_management_menu',
            'label' => 'منوی کاربران',
            'childs' => [
                [
                    'name' => 'users_index',
                    'label' => 'لیست کاربران',
                    'childs' => [
                        [
                            'name' => 'user_info',
                            'label' => 'اطلاعات شخصی',
                            'childs' => [
                                [
                                    'name' => 'user_edit',
                                    'label' => 'ویرایش کاربر',
                                ]
                            ],
                        ],
                        [
                            'name' => 'user_documents',
                            'label' => 'مدارک کاربر',
                        ],
                        [
                            'name' => 'user_roles',
                            'label' => 'نقش های کاربر',
                            'childs' => [
                                [
                                    'name' => 'user_add_role',
                                    'label' => 'افزودن نقش',
                                ],
                            ],
                        ],
                        [
                            'name' => 'user_setting',
                            'label' => 'تنظیمات کاربر',
                            'childs' => [
                                [
                                    'name' => 'user_confirm_sms',
                                    'label' => 'روش تایید پیامک',
                                ],
                                [
                                    'name' => 'user_change_password',
                                    'label' => 'تغییر رمز عبور کاربر',
                                ],
                            ],
                        ],
                        [
                            'name' => 'user_wallet',
                            'label' => 'کیف پول کاربر(ادمین)',
                        ],
                        [
                            'name' => 'user_reports',
                            'label' => ' گزارشات کاربر(ادمین)',
                        ],
                    ],
                ],
                [
                    'name' => 'users_authenticate',
                    'label' => 'احراز هویت',
                    'childs' => [
                        [
                            'name' => 'show_details',
                            'label' => 'مشاهده اطلاعات',
                        ],
                    ],
                ],
            ],
        ],
        [
            'name' => 'withdraws_menu',
            'label' => 'برداشت ها',
            'childs' => [
                [
                    'name' => 'rial_withdraw_index',
                    'label' => 'منوی لیست برداشت ریالی',
                    'childs' => [
                        [
                            'name' => 'rial_withdraw_reject',
                            'label' => 'دکمه رد کردن'
                        ],
                        [
                            'name' => 'rial_withdraw_accept',
                            'label' => 'دکمه تایید کردن'
                        ],
                    ]
                ],
                [
                    'name' => 'currency_withdraw_index',
                    'label' => 'منوی لیست برداشت ارزی',
                    'childs' => [
                        [
                            'name' => 'more_info',
                            'label' => 'دکمه اطلاعات تکمیلی',
                            'childs' => [
                                [
                                    'name' => 'currency_withdraw_reject',
                                    'label' => 'دکمه رد درخواست برداشت'
                                ],
                                [
                                    'name' => 'currency_withdraw_accept',
                                    'label' => 'دکمه تایید درخواست برداشت'
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'name' => 'withdraw_setting',
                    'label' => ' منوی تنظیمات برداشت',
                ],
            ],
        ],
        [
            'name' => 'fees_management_menu',
            'label' => 'منوی مدیریت کارمزدها',
            'childs' => [
                [
                    'name' => 'swap_fees',
                    'label' => 'مدیریت کارمزد سواپ ارزها',
                ],
                [
                    'name' => 'min_withdraw_fess',
                    'label' => 'مدیریت کارمزد و حداقل برداشت ارزها',
                ],
                [
                    'name' => 'transfer_currency_user_to_admin',
                    'label' => 'مدیریت کارمزد انتقال ارز از کاربر به ادمین',
                ],
                [
                    'name' => 'trade_fee',
                    'label' => 'مدیریت کارمزد ترید',
                ],
            ],
        ],
        [
            'name' => 'reports_menu',
            'label' => 'منوی گزارشات',
            'childs' => [
                [
                    'name' => 'transactions_menu',
                    'label' => 'لیست تراکنش ها',
                ],
            ],
        ],
        [
            'name' => 'system_setting_menu',
            'label' => 'منوی تنظیمات سامانه',
            'childs' => [
                [
                    'name' => 'notifications_setting',
                    'label' => 'ارسال اعلان ها',
                ],
                [
                    'name' => 'monetary_site',
                    'label' => 'واحد پول سامانه',
                ],
                [
                    'name' => 'swap_trade_setting',
                    'label' => 'تنظیمات فعال و غیر فعال کردن سواپ و ترید',
                ],
            ],
        ],
        [
            'name' => 'notifications_menu',
            'label' => 'منوی پیغام ها',
            'childs' => [
                [
                    'name' => 'notifications_list',
                    'label' => 'لیست پیغام ها',
                ],
                [
                    'name' => 'notification_create',
                    'label' => 'ابجاد پیغام',
                ],
            ],
        ],
    ];

    public function run()
    {
        $this->createPermissions($this->permissions, 0);
    }

    private function createPermissions($array, $parent)
    {
        foreach ($array as $permission) {
            $perm = Permission::firstOrCreate([
                'name' => $permission["name"]
            ], [
                'name' => $permission['name'],
                'label' => $permission['label'],
                'guard_name' => "web",
                'parent' => $parent
            ]);
            /// اگر زیرمجموعه داشت
            if (isset($permission["childs"])) {
                $this->createPermissions($permission["childs"], $perm->id);
            }
        }
    }
}
