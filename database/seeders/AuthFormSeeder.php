<?php

namespace Database\Seeders;

use App\Models\AuthForm;
use App\Models\BaseData;
use Illuminate\Database\Seeder;

    class AuthFormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $forms = [
            ['type' =>'step.verify-mobile','title'=>'اطلاعات تماس','data' => null,'priority' => 1,'status' => 1,'required' => 1],
            ['type' =>'step.verify-email','title'=>'اطلاعات تماس','data' => null,'priority' => 2,'status' => 1,'required' => 1],
            ['type' =>'step.upload','title'=>'آپلود مدارک','data' => null,'priority' => 3,'status' => 1,'required' => 0],
            ['type' =>'step.information','title'=>'آدرس و تلفن','data' => null,'priority' => 4,'status' => 1,'required' => 0],
            ['type' =>'step.bank','title'=>'اطلاعات بانکی','data' => null,'priority' => 5,'status' => 1,'required' => 0],
            ['type' =>'step.finish','title'=>'در انتظار تایید اپراتور','data' => null,'priority' => 6,'status' => 1,'required' => 0],
        ];
        AuthForm::insert($forms);
    }
}
