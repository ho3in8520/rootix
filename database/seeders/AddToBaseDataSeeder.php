<?php

namespace Database\Seeders;

use App\Models\BaseData;
use Illuminate\Database\Seeder;

class   AddToBaseDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $array = [
            ['type' => 'ticket_notif', 'name' => 'نوتیفیکیشن های مخصوص تیکت', 'extra_field1' => json_encode(['color' => '#54e9903d', 'icon' => 'chatboxes_icon.svg']), 'extra_field2' => 'tickets', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'withdraw_notif', 'name' => 'نوتیفیکیشن های مخصوص برداشت', 'extra_field1' => json_encode(['color' => '#797aff47', 'icon' => 'money$.svg']), 'extra_field2' => 'url', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'bank_notif', 'name' => 'نوتیفیکیشن های مخصوص بانک', 'extra_field1' => json_encode(['color' => '#797aff47', 'icon' => 'money$.svg']), 'extra_field2' => 'url', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'admin_user_notif', 'name' => 'نوتیفیکیشن های ادمین برای کاربر', 'extra_field1' => json_encode(['color' => 'red', 'icon' => 'nobell.svg']), 'extra_field2' => 'url', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'authenticate_notif', 'name' => 'نوتیفیکیشن های احراز هویت', 'extra_field1' => json_encode(['color' => '#f66b6445', 'icon' => 'bell.svg']), 'extra_field2' => 'url', 'extra_field3' => '', 'status' => '1'],


            ['type' => 'transactions', 'name' => 'برداشت از ولت کاربر برای ارسال به ولت دیگر', 'extra_field1' => '5', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'بازگشت وجه به خاطر رد درخواست برداشت', 'extra_field1' => '6', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'افزایش موجودی کیف پول کاربر توسط ادمین', 'extra_field1' => '7', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'transactions', 'name' => 'کاهش موجودی کیف پول کاربر توسط ادمین', 'extra_field1' => '8', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_admin_trc10', 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین', 'extra_field1' => '3', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_admin_trc20', 'name' => 'کارمزد انتقال ارز های کاربران به ولت ادمین', 'extra_field1' => '3', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'trad_fee', 'name' => 'کارمزد ترید در روتیکس', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'trad', 'name' => 'خرید یا قروش ارز (ترید)', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_jst', 'name' => 'کارمزد برداشت jst', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_jst', 'name' => 'حداقل برداشت jst', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_win', 'name' => 'کارمزد برداشت win', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_win', 'name' => 'حداقل برداشت win', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_btt', 'name' => 'کارمزد برداشت btt', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_btt', 'name' => 'حداقل برداشت btt', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_trx', 'name' => 'کارمزد برداشت trx', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_trx', 'name' => 'حداقل برداشت trx', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_btc', 'name' => 'کارمزد برداشت btc', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_btc', 'name' => 'حداقل برداشت btc', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_eth', 'name' => 'کارمزد برداشت eth', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_eth', 'name' => 'حداقل برداشت eth', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_bnb', 'name' => 'کارمزد برداشت bnb', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_bnb', 'name' => 'حداقل برداشت bnb', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_xrp', 'name' => 'کارمزد برداشت xrp', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_xrp', 'name' => 'حداقل برداشت xrp', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_ada', 'name' => 'کارمزد برداشت ada', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_ada', 'name' => 'حداقل برداشت ada', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_shib', 'name' => 'کارمزد برداشت shib', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_shib', 'name' => 'حداقل برداشت shib', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_trx', 'name' => 'کارمزد برداشت trx', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_trx', 'name' => 'حداقل برداشت trx', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_doge', 'name' => 'کارمزد برداشت doge', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_doge', 'name' => 'حداقل برداشت doge', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_sol', 'name' => 'کارمزد برداشت sol', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_sol', 'name' => 'حداقل برداشت sol', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'fee_withdraw_usdt', 'name' => 'کارمزد برداشت usdt', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'min_withdraw_usdt', 'name' => 'حداقل برداشت usdt', 'extra_field1' => '', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],

            ['type' => 'monetary_unit', 'name' => 'واحد پول سامانه', 'extra_field1' => 'toman', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'trade_rial_status', 'name' => 'فعال بودن یا نبودن ترید ریالی سامانه', 'extra_field1' => '1', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'trade_usdt_status', 'name' => 'فعال بودن یا نبودن ترید تتری سامانه', 'extra_field1' => '1', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'swap_status', 'name' => 'فعال بودن یا نبودن سواپ', 'extra_field1' => '1', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],

            ['type' => 'admin_sms_notifs', 'name' => 'لیست اعلان های سامانه به صورت پیامک برای ادمین', 'extra_field1' => '0', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'admin_email_notifs', 'name' => 'لیست اعلان های سامانه به صورت ایمیل برای ادمین', 'extra_field1' => '0', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
            ['type' => 'support_setting', 'name' => 'تنظیمات پشتیبان ها', 'extra_field1' => '0', 'extra_field2' => '', 'extra_field3' => '', 'status' => '1'],
        ];
//        \App\Models\BaseData::insert($array);
        foreach ($array as $item) {
            BaseData::firstOrCreate([
                'type' => $item['type'],
                'extra_field1' => $item['extra_field1'],
            ], [
                'type' => $item['type'],
                'name' => $item['name'],
                'extra_field1' => $item['extra_field1'],
                'extra_field2' => $item['extra_field2'],
                'extra_field3' => $item['extra_field3'],
                'status' => $item['status'],
            ]);
        }
    }
}
