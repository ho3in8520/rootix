<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'code' => rand(0, 99999),
            'first_name' => 'ادمین',
            'last_name' => 'سامانه',
            'email' => 'admin@user.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make('123456'),
            'status' => 1,
            'is_admin' => 1,
            'is_complete_steps' => 1,
        ]);
    }
}
