<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            ['name' => 'Rial', 'name_fa' => 'تومن', 'unit' => 'rls', 'type_token' => 10, 'network' => 'TRC', 'color' => '', 'priority' => 1, 'status' => 1,],
            ['name' => 'Usdt', 'name_fa' => 'تتر', 'unit' => 'usdt', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 2, 'status' => 1,],
            ['name' => 'BitTorrent', 'name_fa' => 'بیت تورنت', 'unit' => 'btt', 'type_token' => 10, 'network' => 'TRC', 'color' => '', 'priority' => 3, 'status' => 1,],
            ['name' => 'JUST', 'name_fa' => 'جاست', 'unit' => 'jst', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 4, 'status' => 1,],
            ['name' => 'WINkLink', 'name_fa' => 'وینک لینک', 'unit' => 'win', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 5, 'status' => 1,],
            ['name' => 'Bitcoin', 'name_fa' => 'بیتکوین', 'unit' => 'btc', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 6, 'status' => 1,],
            ['name' => 'Ethereum', 'name_fa' => 'اتریوم', 'unit' => 'eth', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 7, 'status' => 1,],
            ['name' => 'Binance Coin', 'name_fa' => 'بایننس', 'unit' => 'bnb', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 8, 'status' => 1,],
            ['name' => 'Ripple', 'name_fa' => 'ریپل', 'unit' => 'xrp', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 9, 'status' => 1,],
            ['name' => 'Cardano', 'name_fa' => 'کاردانو', 'unit' => 'ada', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 10, 'status' => 1,],
            ['name' => 'Shiba Inu', 'name_fa' => 'شیبا', 'unit' => 'shib', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 11, 'status' => 1,],
            ['name' => 'Tron', 'name_fa' => 'ترون', 'unit' => 'trx', 'type_token' => 10, 'network' => 'TRC', 'color' => '', 'priority' => 12, 'status' => 1,],
            ['name' => 'Dogecoin', 'name_fa' => 'دوج کوین', 'unit' => 'doge', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 13, 'status' => 1,],
            ['name' => 'Solana', 'name_fa' => 'سولانا', 'unit' => 'sol', 'type_token' => 20, 'network' => 'TRC', 'color' => '', 'priority' => 14, 'status' => 1,],
            ['name' => 'Wrapped Bitcoin', 'name_fa' => 'رپد بیت کوین', 'unit' => 'wbtc', 'type_token' => 20, 'network' => 'TRC', 'color' => '#eaeaea', 'priority' => 15, 'status' => 1,],
            ['name' => 'Litecoin', 'name_fa' => 'لایت کوین', 'unit' => 'ltc', 'type_token' => 20, 'network' => 'TRC', 'color' => '#345d9d', 'priority' => 16, 'status' => 1,],
            ['name' => 'Avalanche', 'name_fa' => 'آوالانچ', 'unit' => 'avax', 'type_token' => 20, 'network' => 'TRC', 'color' => '#e84142', 'priority' => 17, 'status' => 1,],
            ['name' => 'Polkadot', 'name_fa' => 'پولکادات', 'unit' => 'dot', 'type_token' => 20, 'network' => 'TRC', 'color' => '#E6007A', 'priority' => 18, 'status' => 1,],
            ['name' => 'Polygon', 'name_fa' => 'ماتیک', 'unit' => 'matic', 'type_token' => 20, 'network' => 'TRC', 'color' => '#8247E5', 'priority' => 19, 'status' => 1,],
            ['name' => 'Cosmos', 'name_fa' => 'کاز ماز', 'unit' => 'atom', 'type_token' => 20, 'network' => 'TRC', 'color' => '#2e3148', 'priority' => 20, 'status' => 1,],
            ['name' => 'Ethereum Classic', 'name_fa' => 'اتریوم کلاسیک', 'unit' => 'etc', 'type_token' => 20, 'network' => 'TRC', 'color' => '#328332', 'priority' => 21, 'status' => 1,],
            ['name' => 'Stellar', 'name_fa' => 'استلار', 'unit' => 'xlm', 'type_token' => 20, 'network' => 'TRC', 'color' => '#1A1B1B', 'priority' => 22, 'status' => 1,],
            ['name' => 'VeChain', 'name_fa' => 'وی چین', 'unit' => 'vet', 'type_token' => 20, 'network' => 'TRC', 'color' => '#15bdff', 'priority' => 23, 'status' => 1,],
            ['name' => 'Hedera', 'name_fa' => 'هدرا هش گراف', 'unit' => 'hbar', 'type_token' => 20, 'network' => 'TRC', 'color' => '#373636', 'priority' => 24, 'status' => 1,],
            ['name' => 'Filecoin', 'name_fa' => 'فایل کوین', 'unit' => 'fil', 'type_token' => 20, 'network' => 'TRC', 'color' => '#0090ff', 'priority' => 25, 'status' => 1,],
            ['name' => 'Internet Computer', 'name_fa' => 'دفینیتی', 'unit' => 'icp', 'type_token' => 20, 'network' => 'TRC', 'color' => '#6BB1E4', 'priority' => 26, 'status' => 1,],
            ['name' => 'Elrond', 'name_fa' => 'الروند', 'unit' => 'egld', 'type_token' => 20, 'network' => 'TRC', 'color' => '#343434', 'priority' => 27, 'status' => 1,],
            ['name' => 'Theta Network', 'name_fa' => 'تیتا توکن', 'unit' => 'theta', 'type_token' => 20, 'network' => 'TRC', 'color' => '#26a17b', 'priority' => 28, 'status' => 1,],
            ['name' => 'The Sandbox', 'name_fa' => 'سند باکس', 'unit' => 'sand', 'type_token' => 20, 'network' => 'TRC', 'color' => '#01AFEF', 'priority' => 29, 'status' => 1,],
            ['name' => 'Fantom', 'name_fa' => 'فانتوم', 'unit' => 'ftm', 'type_token' => 20, 'network' => 'TRC', 'color' => '#1969FF', 'priority' => 30, 'status' => 1,],
            ['name' => 'Axie Infinity', 'name_fa' => 'اکسی اینفینیتی', 'unit' => 'axs', 'type_token' => 20, 'network' => 'TRC', 'color' => '#0445C8', 'priority' => 31, 'status' => 1,],
            ['name' => 'Tezos', 'name_fa' => 'تزوس', 'unit' => 'xtz', 'type_token' => 20, 'network' => 'TRC', 'color' => '#0062FE', 'priority' => 32, 'status' => 1,],
            ['name' => 'Waves', 'name_fa' => 'ویوز', 'unit' => 'waves', 'type_token' => 20, 'network' => 'TRC', 'color' => '#0054ff', 'priority' => 33, 'status' => 1,],
            ['name' => 'Helium', 'name_fa' => 'هلیوم', 'unit' => 'hnt', 'type_token' => 20, 'network' => 'TRC', 'color' => '#464DFE', 'priority' => 34, 'status' => 1,],
            ['name' => 'Aave', 'name_fa' => 'آی وی', 'unit' => 'aave', 'type_token' => 20, 'network' => 'TRC', 'color' => '#6c8ab3', 'priority' => 35, 'status' => 1,],
            ['name' => 'PancakeSwap', 'name_fa' => 'پنکیک سواپ', 'unit' => 'cake', 'type_token' => 20, 'network' => 'TRC', 'color' => '#d1884f', 'priority' => 36, 'status' => 1,],
            ['name' => 'Zcash', 'name_fa' => 'زی کش', 'unit' => 'zec', 'type_token' => 20, 'network' => 'TRC', 'color' => '#EDB245', 'priority' => 37, 'status' => 1,],
            ['name' => 'EOS', 'name_fa' => 'ایاس', 'unit' => 'eos', 'type_token' => 20, 'network' => 'TRC', 'color' => '#000000', 'priority' => 38, 'status' => 1,],
            ['name' => 'Flow', 'name_fa' => 'فلوو', 'unit' => 'flow', 'type_token' => 20, 'network' => 'TRC', 'color' => '#05ef8b', 'priority' => 39, 'status' => 1,],
        ];
        foreach ($array as $item)
            Currency::query()->firstOrCreate($item);
    }
}
