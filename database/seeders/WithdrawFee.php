<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\BaseData;
use Illuminate\Database\Seeder;

class WithdrawFee extends Seeder
{
    /**
     * این سیدر برای وقتی هست که یه ارز اضافه میکنیم میخواییم به صورت خوکار حداقل برداشت و کارمزد برداشت رو براش ست کنیم
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // اینجا unit ارزها رو اضافه میکنیم
        $assets = ['btt'];
        foreach ($assets as $asset) {
            BaseData::query()->firstOrCreate(
                ['type' => 'min_withdraw_' . $asset, 'name' => 'حداقل برداشت ' . $asset],
                ['extra_field1' => 1]
            );
            BaseData::query()->firstOrCreate(
                ['type' => "fee_withdraw_$asset", 'name' => 'کارمزد برداشت ' . $asset],
                ['extra_field1' => 1]
            );
            BaseData::query()->firstOrCreate(
                ['type' => "manage_swap_$asset", 'name' => 'مدیریت سواپ ' . $asset],
                ['extra_field1' => 1, 'extra_field2' => 1, 'extra_field3' => 1]
            );
        }
    }
}
