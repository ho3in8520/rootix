<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::create([
            'code' => '123123',
            'first_name' => 'user1',
            'last_name' => 'test1',
            'email' => 'user1@user.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make('123456'),
            'status' => 1,
        ]);
       User::create([
            'code' => '4444',
            'first_name' => 'user2',
            'last_name' => 'test2',
            'email' => 'user2@user.com',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make('123456'),
            'status' => 1,
        ]);

//       Artisan::call('update:amount');
    }
}
