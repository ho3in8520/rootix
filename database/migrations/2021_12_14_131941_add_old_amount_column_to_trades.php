<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOldAmountColumnToTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->float('old_amount',20,8)->default(0)->after('amount')->comment('
            این فیلد شامل تعداد ارزی که کاربر وارد میکند می باشد.(تعداد ارزی که برای کوینکس میفرستیم با کسر کارمزد روتیکس است و با تعدادی که کاربر وارد میکند متفاوت است)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            //
        });
    }
}
