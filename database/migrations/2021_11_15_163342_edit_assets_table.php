<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $assets=\App\Models\Asset::all();
        Schema::dropIfExists('assets');
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('currency_id');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->float('amount', 20, 8)->default(0);
            $table->text('token')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        foreach ($assets as $asset){
            $unit= $asset->unit=='rial'?'rls':$asset->unit;
            \App\Models\Asset::create([
                'currency_id'=>\App\Models\Currency::query()->where('unit',$unit)->first('id')->id,
                'user_id'=>$asset->user_id,
                'amount'=>$asset->amount,
                'token'=>$asset->token,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
