<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadedFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");
            $table->morphs("uploadable");
            $table->string('mime_type')->nullable();
            $table->float('size', 20, 2)->default(0)->comment("hajme file");
            $table->string("path", 1000);
            $table->string("thumbnail_path", 1000)->nullable();
            $table->string("type", 250)->nullable();
            $table->tinyInteger("status")->default(0)->comment("0=>uploaded, 1=>accepted, 2=>rejected");
            $table->boolean("state")->default(1);
            $table->text("description")->collation("utf8_general_ci")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_files');
    }
}
