<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawRialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_rial', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedFloat('amount',20,8);
            $table->unsignedFloat('fee');
            $table->unsignedBigInteger('bank_id');
            $table->tinyInteger('status')->comment('0=>pending  1=>deposited  2=> rejected')->default(0);
            $table->text('user_des')->nullable();
            $table->text('admin_des')->nullable();
            $table->string('tracking_code')->nullable();
            $table->tinyInteger('deposit_type')->nullable()->comment('0=>shetab  1=>paya  2=> santa');
            $table->dateTime('deposit_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->on('users')
                ->references('id');

            $table->foreign('admin_id')
                ->on('admins')
                ->references('id');

            $table->foreign('bank_id')
                ->on('banks')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraw_rial');
    }
}
