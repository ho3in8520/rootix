<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNationalityColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('nationality')->default(0)->after('language')->comment('ایرانی باشد 0 غیر ایرانی باشد 1');
            $table->boolean('accept_laws')->default(0)->after('remember_token')->comment('قبول کرده باشد 0 قبول نکرده باشد 1');
            $table->string('national_code', 12)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nationality');
        });
    }
}
