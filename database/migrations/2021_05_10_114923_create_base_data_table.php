<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_data', function (Blueprint $table) {
            $table->id();
            $table->string('type', 255);
            $table->string('name', 255)->collation("utf8_general_ci");
            $table->string('extra_field1', 255)->collation("utf8_general_ci")->nullable();
            $table->string('extra_field2', 255)->collation("utf8_general_ci")->nullable();
            $table->string('extra_field3', 255)->collation("utf8_general_ci")->nullable();
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('base_data');
    }
}
