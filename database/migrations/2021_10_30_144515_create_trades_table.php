<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('id_trade',20)->default(0);
            $table->tinyInteger('status')->default(0)->comment('0->unexecuted , 1 ->executed , 2 ->cancel');
            $table->string('market');
            $table->boolean('type')->default(0)->comment('0->sell , 1->buy');
            $table->dateTime('finished_time')->nullable();
            $table->text('trade_info');
            $table->float('amount',20,8)->default(0);
            $table->float('price',20,8)->default(0);
            $table->float('fee_tx')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
