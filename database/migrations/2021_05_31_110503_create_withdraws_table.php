<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->foreign("user_id")->references("id")->on("users");
            $table->string('parameter1')->comment('پارامتر ورودی برای ارسال تتر به ولت مقصد');
            $table->string('parameter2')->comment('پارامتر ورودی برای برداشت تتر از ولت کاربر برای کارمزد');
            $table->string("amount");
            $table->string('unit');
            $table->string('address_wallet');
            $table->boolean('status')->default(0)->comment('1 => send trx,2 => get tether admin,3 => send token to destination wallet');
            $table->string('verification', 300)->nullable();
            $table->string('time_verification',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
