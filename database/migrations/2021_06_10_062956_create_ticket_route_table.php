<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_route', function (Blueprint $table) {
            $table->id();
            $table->integer('master_id');
            $table->unsignedInteger('from_id');
            $table->unsignedInteger('to_id');
            $table->tinyInteger('mode')->comment(' 1=>role 2=>user')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_route');
    }
}
