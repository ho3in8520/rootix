<?php

return[

    //landing . home

    'aboutUs-header'=>'یک وب سایت تجاری که کیف پول ، مبادله و سایر اطلاعات مربوط به بیت کوین را لیست می کند',
    'aboutUs'=>'مکانی برای هرکسی که می خواهد بیت کوین ها را به سادگی بخرد و بفروشد.
     با استفاده از Visa / MasterCard یا انتقال بانکی وجه خود را واریز کنید. خرید و فروش فوری بیت کوین
     با قیمت منصفانه تضمین شده است. هیچ چیز اضافی با بیش از 700000 کاربر از سراسر جهان از خدمات ما راضی شوید',

    'BTC-wallet'=>'برای ایجاد ، ارسال و دریافت بیت کوین آن را از طریق رایانه شخصی یا موبایل دریافت کنید.',

    'coinWallet'=>'بیت کوین هایی که ایجاد کرده اید یا از طریق کارت اعتباری رد و بدل شده است.',

    'buySellWallet'=>'آدرس گیرنده را وارد کنید ، مبلغ را مشخص کنید و ارسال کنید.',

    'ourWork'=>'بیت کوین براساس پروتکل معروف به blockchain است که امکان ایجاد داده
    ، انتقال و تأیید صحت داده های مالی بسیار مطمئن و بدون دخالت شخص ثالث را فراهم می کند..',

    'ourBenefits'=>'مأموریت ما به عنوان شریک رسمی بنیاد بیت کوین کمک به شما در ورود و درک بهتر جهان
     # cryptocurrency و جلوگیری از هرگونه مشکلی است که ممکن است با آن روبرو شوید..',

    'ourGuarantees'=>'ما در اینجا هستیم زیرا ما علاقه مند به بازارهای شفاف و باز هستیم و هدف این هستیم
    که یک نیروی محرک اصلی در اتخاذ گسترده باشد ، ما اولین و بهترین در cryptocurrency هستیم.',

    'security'=>''


];
