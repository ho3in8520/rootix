<?php

return [
    'title-profile'  =>'پروفایل',
    'title-mobile'   =>'تآیید شماره تلفن',
    'title-minePage' =>'داشبورد',
    'title-exchange' =>'صفحه تبدیل ارز',
    'title-Payment' =>'وضعیت پرداخت',
    'title-Authentication' =>'احراز هویت',
    'title-buySell' =>'صفحه واریز / برداشت وجه',
    'title-withdraw' =>'صفحه برداشت ارز',
    'title-ticket' =>'تیکت',
    'title-charge' =>'صفحه واریز وجه',
    'title-sellCurrency' =>'فروش ارز دیجیتال',
    'title-MyWallet' =>'کیف پول من',
    'title-bankNew' =>'افزودن حساب بانکی جدید',
    'title-bankEdit' =>'ویرایش حساب بانکی',
    'title-accountBank' =>' حساب های بانکی',
    'title-ticket' =>'تیکت',
    'title-withdraw' =>'صفحه برداشت ارز',



];
