@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-Authentication')}}
@endsection
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="https://nobitex.ir/styles.64d4e435.css?__uncache=5%2F13%2F2021%2C%209%3A31%3A12%20PM" />--}}
    <style>
        .progress-container[data-v-822b901a] {
            width: 100%;
            margin: 0 auto 2rem
        }

        .progressbar[data-v-822b901a] {
            counter-reset: step;
            padding: 0
        }

        .progressbar li[data-v-822b901a] {
            list-style-type: none;
            width: 20%;
            float: right;
            font-size: 14px;
            font-weight: 700;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            color: #7d7d7d
        }

        .progressbar li[data-v-822b901a]:before {
            width: 30px;
            height: 30px;
            content: counter(step);
            counter-increment: step;
            line-height: 30px;
            border: 2px solid #7d7d7d;
            display: block;
            text-align: center;
            margin: 0 auto 10px;
            border-radius: 50%;
            background-color: #fff
        }

        .progressbar li[data-v-822b901a]:after {
            width: 100%;
            height: 2px;
            content: "";
            position: absolute;
            background-color: #7d7d7d;
            top: 15px;
            right: -50%;
            z-index: -1
        }

        .progressbar li[data-v-822b901a]:first-child:after {
            content: none
        }

        .progressbar li.active[data-v-822b901a] {
            color: #55b776
        }

        .progressbar li.active[data-v-822b901a]:before {
            border-color: #55b776
        }

        .progressbar li.active + li[data-v-822b901a]:after {
            background-color: #55b776
        }

        .progressbar li.pending[data-v-822b901a] {
            color: #ff7f30
        }

        .progressbar li.pending[data-v-822b901a]:before {
            border-color: #fbc133
        }

        .progressbar li.pending + li[data-v-822b901a]:after {
            background: #fbc133
        }

        .image-infomation .card-body {
            padding: 0.50rem;
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid body mt-3"><!---->
        <div data-v-7fcc824e="" class="page-content">
            <div data-v-7fcc824e="" class="row">
                <div data-v-822b901a="" data-v-7fcc824e="" class="progress-container clearfix">
                    <ul data-v-822b901a="" class="progressbar d-flex justify-content-center">
                        <li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.phoneNum')}}</p> <!----></li>
                        <li data-v-822b901a="" class="active">
                            <img data-v-822b901a=""
                                 src="{{ asset('theme/user/assets/img/step/verified.png') }}"> <p
                                data-v-822b901a="" class="">{{__('attributes.Authentication.bank')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.further')}}</p> <!----></li>

                        {{--<li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.uploadDocuments')}}</p> <!----></li>--}}
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.identity')}}</p> <!----></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="alert alert-warning m-2">
                    <p>لطفا تمامی اطلاعات را واردکنید وشماره ثابت خود را ثبت کنید وعکس کارت ملی وقبض تلفن ثابت خودتان را اپلود کنیدکه باید کاملا واضح ومشخص باشد ودراخر روی گزینه ثبت کلیک کنید و منتظر بمانید تاهویتتان توسط مدیریت سامانه ثبت شود</p>
                    <P></P>
                </div>
                <div class="card-body">
                    @if(!empty($user->reject_reason))
                        <div class="col-md-12">
                            <div class="alert alert-danger text-right">
                                {{ $user->reject_reason }}
                            </div>
                        </div>
                    @endif
                    <form method="post" action="{{ route('step.information.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.name')}}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="user[first_name]"
                                           value="{{ (!empty($user))?$user->first_name:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.lastName')}}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="user[last_name]"
                                           value="{{ (!empty($user))?$user->last_name:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.gender')}}</label>
                                <div class="form-group">
                                    <select type="text" class="form-control" name="user[gender_id]">
                                        <option value="">{{__('attributes.Authentication.select')}}</option>
                                        <option value="1" {{ (!empty($user) && $user->gender_id==1)?'selected':'' }}>
                                            {{__('attributes.Authentication.male')}}
                                        </option>
                                        <option value="2" {{ (!empty($user) && $user->gender_id==2)?'selected':'' }}>
                                            {{__('attributes.Authentication.female')}}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.birthday')}}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control datePicker" name="user[birth_day]"
                                           value="{{ (!empty($user))?$user->birth_day:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.nationalCode')}}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" data-type="number" name="user[national_code]"
                                           value="{{ (!empty($user))?$user->national_code:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.state')}}</label>
                                <div class="form-group">
                                    <select type="text" class="form-control state" name="user[state]">
                                        <option value="">{{__('attributes.Authentication.select')}}</option>
                                        @foreach($states as $item)
                                            <option
                                                value="{{ $item->id }}" {{ (!empty($user) && $user->state==$item->id)?'selected':'' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.city')}}</label>
                                <div class="form-group">
                                    <select type="text" class="form-control city" name="user[city]">
                                        <option value="">{{__('attributes.Authentication.select')}}</option>
                                        @foreach($city as $item)
                                            <option
                                                value="{{ $item->id }}" {{ (!empty($user) && $user->city==$item->id)?'selected':'' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.landlinePhone')}}</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" data-type="number" name="user[phone]"
                                           value="{{ (!empty($user))?$user->phone:'' }}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label>{{__('attributes.Authentication.address')}}</label>
                                <div class="form-group">
                                    <textarea rows="3" class="form-control"
                                              name="user[address]">{{ (!empty($user))?$user->address:'' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>{{__('attributes.Authentication.cart')}}</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['national']))?getImage($images['national']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[national_image]" class="imgInp d-none"
                                               type="file"
                                               style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="card image-infomation">
                                    <div class="card-body">
                                        <label>{{__('attributes.Authentication.bill')}}</label>
                                        <div id="deleteImage">
                                        </div>
                                        <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px" id="blah"
                                             src="{{ (!empty($images['phone_bill']))?getImage($images['phone_bill']->path):asset('theme/user/assets/img/no-image.png') }}"
                                             alt="your image" height="172">
                                        <input accept="image/jpeg" name="image[phone_bill]" type="file"
                                               class="imgInp d-none" style="display: none">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-2" style="display: flex">
                                <button type="button" class="btn btn-success ajaxStore">{{__('attributes.Authentication.submit')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // $('#blah').attr('src', e.target.result);
                    $(input).closest('.card-body').find("img").attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(".imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest('.card-body').find("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });
        $(document).on('click', '#blah', function () {
            $(this).closest('.card-body').find("input").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest('.card-body').find(".imgInp").val('')
            $(this).closest('.card-body').find("#blah").attr('src', "{{ asset('theme/user/assets/img/no-image.png') }}");
            $(this).closest('.card-body').find("#deleteImage").html('')
        });
    </script>
@endsection
