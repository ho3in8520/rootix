@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-Authentication')}}
@endsection
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="https://nobitex.ir/styles.64d4e435.css?__uncache=5%2F13%2F2021%2C%209%3A31%3A12%20PM" />--}}
    <style>
        .progress-container[data-v-822b901a] {
            width: 100%;
            margin: 0 auto 2rem
        }

        .progressbar[data-v-822b901a] {
            counter-reset: step;
            padding: 0
        }

        .progressbar li[data-v-822b901a] {
            list-style-type: none;
            width: 20%;
            float: right;
            font-size: 14px;
            font-weight: 700;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            color: #7d7d7d
        }

        .progressbar li[data-v-822b901a]:before {
            width: 30px;
            height: 30px;
            content: counter(step);
            counter-increment: step;
            line-height: 30px;
            border: 2px solid #7d7d7d;
            display: block;
            text-align: center;
            margin: 0 auto 10px;
            border-radius: 50%;
            background-color: #fff
        }

        .progressbar li[data-v-822b901a]:after {
            width: 100%;
            height: 2px;
            content: "";
            position: absolute;
            background-color: #7d7d7d;
            top: 15px;
            right: -50%;
            z-index: -1
        }

        .progressbar li[data-v-822b901a]:first-child:after {
            content: none
        }

        .progressbar li.active[data-v-822b901a] {
            color: #55b776
        }

        .progressbar li.active[data-v-822b901a]:before {
            border-color: #55b776
        }

        .progressbar li.active + li[data-v-822b901a]:after {
            background-color: #55b776
        }

        .progressbar li.pending[data-v-822b901a] {
            color: #ff7f30
        }

        .progressbar li.pending[data-v-822b901a]:before {
            border-color: #fbc133
        }

        .progressbar li.pending + li[data-v-822b901a]:after {
            background: #fbc133
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid body mt-3"><!---->
        <div data-v-7fcc824e="" class="page-content">
            <div data-v-7fcc824e="" class="row">
                <div data-v-822b901a="" data-v-7fcc824e="" class="progress-container clearfix">
                    <ul data-v-822b901a="" class="progressbar d-flex justify-content-center">
                        <li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!---->
                            <p data-v-822b901a="" class="activeColor">{{__('attributes.Authentication.phoneNum')}}</p> <!----></li>
                        <li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!---->
                            <p data-v-822b901a="" class="activeColor">{{__('attributes.Authentication.bank')}}</p> <!----></li>
                        <li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!----> <p
                                data-v-822b901a="" class="activeColor">{{__('attributes.Authentication.further')}}</p> <!----></li>
                        {{--<li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!---->
                            <p data-v-822b901a="" class="activeColor">{{__('attributes.Authentication.uploadDocuments')}}</p> <!----></li>--}}
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.identity')}}</p> <!----></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                @if($user->status==2)
                    <div class="col-md-12">
                        <div class="alert alert-danger text-left">
                            {{ $user->reject_reason }}
                        </div>
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-warning text-left">
                                <p>
                                    اطلاعات شما تکمیل شد و در انتظار تایید توسط مدیریت سامانه هستید
                                </p>
                                <p>
                                    توجه کنید: تا زمانی که هویت شما تایید نشود اهراز هویت شما ثبت نخواهدشد
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
