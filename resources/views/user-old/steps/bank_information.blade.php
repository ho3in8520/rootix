@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-Authentication')}}
@endsection
@section('style')
    {{--    <link rel="stylesheet" type="text/css" href="https://nobitex.ir/styles.64d4e435.css?__uncache=5%2F13%2F2021%2C%209%3A31%3A12%20PM" />--}}
    <style>
        .progress-container[data-v-822b901a] {
            width: 100%;
            margin: 0 auto 2rem
        }

        .progressbar[data-v-822b901a] {
            counter-reset: step;
            padding: 0
        }

        .progressbar li[data-v-822b901a] {
            list-style-type: none;
            width: 20%;
            float: right;
            font-size: 14px;
            font-weight: 700;
            position: relative;
            text-align: center;
            text-transform: uppercase;
            color: #7d7d7d
        }

        .progressbar li[data-v-822b901a]:before {
            width: 30px;
            height: 30px;
            content: counter(step);
            counter-increment: step;
            line-height: 30px;
            border: 2px solid #7d7d7d;
            display: block;
            text-align: center;
            margin: 0 auto 10px;
            border-radius: 50%;
            background-color: #fff
        }

        .progressbar li[data-v-822b901a]:after {
            width: 100%;
            height: 2px;
            content: "";
            position: absolute;
            background-color: #7d7d7d;
            top: 15px;
            right: -50%;
            z-index: -1
        }

        .progressbar li[data-v-822b901a]:first-child:after {
            content: none
        }

        .progressbar li.active[data-v-822b901a] {
            color: #55b776
        }

        .progressbar li.active[data-v-822b901a]:before {
            border-color: #55b776
        }

        .progressbar li.active + li[data-v-822b901a]:after {
            background-color: #55b776
        }

        .progressbar li.pending[data-v-822b901a] {
            color: #ff7f30
        }

        .progressbar li.pending[data-v-822b901a]:before {
            border-color: #fbc133
        }

        .progressbar li.pending + li[data-v-822b901a]:after {
            background: #fbc133
        }

    </style>
@endsection
@section('content')
    <div class="container-fluid body mt-3"><!---->
        <div data-v-7fcc824e="" class="page-content">
            <div data-v-7fcc824e="" class="row">
                <div data-v-822b901a="" data-v-7fcc824e="" class="progress-container clearfix">
                    <ul data-v-822b901a="" class="progressbar d-flex justify-content-center">
                        <li data-v-822b901a="" class="active"><img data-v-822b901a=""
                                                                   src="{{ asset('theme/user/assets/img/step/verified.png') }}">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.phone')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!---->
                            <!----> <p
                                data-v-822b901a="" class="">{{__('attributes.Authentication.bank')}}</p> <!----></li>
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="activeColor">{{__('attributes.Authentication.further')}}</p> <!----></li>
                        {{--<li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.uploadDocuments')}}</p> <!----></li>--}}
                        <li data-v-822b901a="" class="">
                            <!---->
                            <p data-v-822b901a="" class="">{{__('attributes.Authentication.identity')}}</p> <!----></li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            @if(!empty($user->reject_reason))
                                <div class="col-md-12">
                                    <div class="alert alert-danger text-left">
                                        {{ $user->reject_reason }}
                                    </div>
                                </div>
                            @endif
                            <form method="post" action="{{ route('step.bank.store') }}">
                                @csrf
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <label>{{__('attributes.Authentication.bankName')}}  <span class="text-danger">*</span></label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name_bank" required
                                                   value="{{ (!empty($bank))?$bank->name:'' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>شماره کارت <span class="text-danger">*</span></label>
                                        <div class="form-group">
                                            <input type="text" data-type="number" class="form-control" name="card" required
                                                   value="{{ (!empty($bank))?$bank->card_number:'' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{__('attributes.Authentication.shabaNum')}}  <span class="text-danger">*</span></label>
                                        <div class="form-group">
                                            <input type="text" data-type="number" class="form-control" name="sheba" required
                                                   value="{{ (!empty($bank))?$bank->sheba_number:'' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>{{__('attributes.Authentication.accountNumber')}} </label>
                                        <div class="form-group">
                                            <input type="text" data-type="number" class="form-control" name="account_number"
                                                   value="{{ (!empty($bank))?$bank->account_number:'' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="alert alert-warning text-left">
                                            <p>
                                                تایید حساب بانکی از ده  دقیقه تا دو ساعت زمان بر هست
                                            </p>
                                            <p>
                                                حساب بانکی باید به نام خودتان باشد و درغیراین صورت خلاف قوانین سایت است.
                                            </p>
                                            <p>
                                                واریز و برداشت فقط از طریق حساب های بانکی تائید شده مقدوراست.
                                            </p>
                                        </div>
                                    </div>
                                    {{--<div class="col-md-5">
                                        <div class="card image-infomation mb-0">
                                            <div class="card-body">
                                                <label>{{__('attributes.Authentication.imageUploadLocation')}}</label>
                                                <div id="deleteImage">
                                                </div>
                                                <img title="{{__('attributes.Authentication.imageUpload')}}" style="cursor:pointer;width:100%;height: 172px"
                                                     id="blah"
                                                     src="{{ (!empty($bank->files[0]))?getImage($bank->files[0]->path):asset('theme/user/assets/img/no-image.png') }}"
                                                     alt="your image" height="172">
                                                <input accept="image/jpeg" name="image" type="file"
                                                       class="imgInp d-none" style="display: none">
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                                <div class="row justify-content-center mt-3">
                                    <div class="col-md-1">
                                        <button type="button" class="btn btn-success ajaxStore">{{__('attributes.Authentication.submit')}}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // $('#blah').attr('src', e.target.result);
                    $(input).closest('.card-body').find("img").attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest('.card-body').find("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });

        $(document).on('click', '#blah', function () {
            $(this).closest('.card-body').find("input").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest('.card-body').find(".imgInp").val('')
            $(this).closest('.card-body').find("#blah").attr('src', "{{ asset('theme/user/assets/img/no-image.png') }}");
            $(this).closest('.card-body').find("#deleteImage").html('')
        });
    </script>
@endsection
