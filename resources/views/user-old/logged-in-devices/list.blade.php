@extends('templates.user.master_page')
@section('title_browser') Login Activity @endsection

@section('content')
    <div class="row">
        <div class="col-lg">
            <div class="card m-1 p-2">
                <div class="card-header d-flex justify-content-between">
                    دستگاه های موجود
                    <a href="{{ count($devices) > 1 ? '/logout/all' : '#' }}" class="btn btn-danger {{ count($devices) == 1 ? 'disabled' : '' }}">حذف همه دستگاه ها</a>
                </div>
                <table class="table table-hover mt-1">
                    <thead class="thead-dark">
                    <tr>
                        <th>دستگاه</th>
                        <th>IP</th>
                        <th style="width:12%" >آخرین فعالیت</th>
                        <th style="width:12%" >عملیات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($devices as $device)
                        <tr>
                            <td>{{ $device->user_agent }}</td>
                            <td>{{ $device->ip_address }}</td>
                            <td>{{ Carbon\Carbon::parse($device->last_activity)->diffForHumans() }}</td>
                            @if ($current_session_id == $device->id)
                                <td><span class="btn btn-info">همین دستگاه</span></td>
                            @else
                                <td><a href="/logout/{{$device->device_id}}" class="btn btn-danger">حذف</a></td>                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
