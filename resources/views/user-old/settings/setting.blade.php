@extends('templates.user.master_page')
@section('title_browser')
    تنظیمات
@endsection
@section('style')
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/css-rtl/pages/app-user.min.css')}}">

    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/menu/menu-types/horizontal-menu.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/css-rtl/plugins/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('theme/user/app-assets/vendors/css/extensions/swiper.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/user/app-assets/vendors/css/vendors-rtl.min.css')}}">
@endsection
@section('content')

    <!-- Nav Filled Starts -->
    <section id="nav-filled">
        <div class="row">
            <div class="col-sm-12">
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h5>
                            <i class="fa fa-bell"></i>
                            <span>ارسال اعلان ها <span style="font-size: 13px;"
                                                       class="text-danger"> (به زودی)</span></span>
                        </h5>
                    </div>
                    <hr>
                    <div class="card-content">
                        <div class="card-body">
                            <form>
                                <ul class="list-unstyled mb-0">
                                    <li class="d-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="customCheck"
                                                       id="customCheck1" disabled>
                                                <label class="custom-control-label" for="customCheck1">دریافت اعلان از
                                                    طریق پست الکترونیک </label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="customCheck"
                                                       id="customCheck1" disabled>
                                                <label class="custom-control-label" for="customCheck1">دریافت اعلان از
                                                    طریق پیامک </label>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-block mr-2 mb-1">
                                        <fieldset>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="customCheck"
                                                       id="customCheck1" disabled>
                                                <label class="custom-control-label" for="customCheck1">به صورت نوتیفیکشن
                                                    در اپلیکیشن </label>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h5>
                            <i class="fa fa-mobile"></i>
                            <span>  google authenticator </span>
                        </h5>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="mb-2">
                            <ul class="list-unstyled">
                                <li class="row mb-1">
                                    <label class="col-lg-3 col-md-5 col-sm-7 d-flex align-items-center">google authenticator: </label>
                                    <div>
                                        @if(!empty(auth()->user()->google2fa_secret))
                                            <button type="button"
                                                    class="btn btn-danger waves-effect waves-light"
                                                    data-toggle="modal"
                                                    data-target="#default">غیرفعال کردن
                                            </button>
                                            <div class="modal fade text-left"
                                                 id="default" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="myModalLabel1"
                                                 aria-hidden="true">
                                                <div
                                                    class="modal-dialog modal-dialog-scrollable"
                                                    role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title"
                                                                id="myModalLabel1">Modal
                                                                پایه</h4>
                                                            <button type="button"
                                                                    class="close"
                                                                    data-dismiss="modal"
                                                                    aria-label="Close">
                                                                                                <span
                                                                                                    aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-8 col-md-offset-2 mx-auto">
                                                                        <div class="panel panel-default">
                                                                            <h5 class="panel-heading "
                                                                                style="text-align: center;">غیرفعالسازی گوگل اتنتیکاتور</h5>

                                                                            <div class="panel-body"
                                                                                 style="text-align: center;">
                                                                                <form method="post"
                                                                                      action="{{ route('setting.status_google_authenticator') }}">
                                                                                    @csrf
                                                                                    <input type="hidden" value="deactivate"
                                                                                           name="type">
                                                                                    <div class="form-group">
                                                                                        <label>Authenticator
                                                                                            Code</label>
                                                                                        <input type="text"
                                                                                               name="one_time_password"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                    <div>
                                                                                        <button
                                                                                            class="btn btn-primary ajaxStore"
                                                                                            type="submit">ثبت
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <button type="button"
                                                    class="btn btn-success waves-effect waves-light"
                                                    data-toggle="modal"
                                                    data-target="#default"> فعال سازی
                                            </button>
                                            <div class="modal fade text-left"
                                                 id="default" tabindex="-1"
                                                 role="dialog"
                                                 aria-labelledby="myModalLabel1"
                                                 aria-hidden="true">
                                                <div
                                                    class="modal-dialog modal-dialog-scrollable"
                                                    role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title"
                                                                id="myModalLabel1">Modal
                                                                پایه</h4>
                                                            <button type="button"
                                                                    class="close"
                                                                    data-dismiss="modal"
                                                                    aria-label="Close">
                                                                                                <span
                                                                                                    aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-8 col-md-offset-2 mx-auto">
                                                                        <div class="panel panel-default">
                                                                            <h5 class="panel-heading "
                                                                                style="text-align: center;">ثبت گوگل
                                                                                اتینتیکاتور</h5>

                                                                            <div class="panel-body"
                                                                                 style="text-align: center;">
                                                                                <p>کد شخصی اتینتیکاتور شما برابر است
                                                                                    با : {{$secret}}</p>
                                                                                <p> یا میتونید از طریق اسکن Qr کد
                                                                                    زیر آن را به اتینتیکاتور خود
                                                                                    اضافه کنید .</p>
                                                                                <div>
                                                                                    <img src="{{$QR_Image}}">
                                                                                </div>
                                                                                <p>برای ثبت اتینتیکاتور خود باید نرم
                                                                                    افزار google authenticator را در
                                                                                    تلفن همراه خود نصب بفرمایید</p>
                                                                                <p>
                                                                                    بعد از اسکن بارکد کد اتنتیکاتور را
                                                                                    وارد کنید
                                                                                </p>
                                                                                <form method="post"
                                                                                      action="{{ route('setting.status_google_authenticator') }}">
                                                                                    @csrf
                                                                                    <input type="hidden"
                                                                                           value="{{ $secret }}"
                                                                                           name="secret">
                                                                                    <input type="hidden" value="active"
                                                                                           name="type">
                                                                                    <div class="form-group">
                                                                                        <label>Authenticator
                                                                                            Code</label>
                                                                                        <input type="text"
                                                                                               name="one_time_password"
                                                                                               class="form-control">
                                                                                    </div>
                                                                                    <div>
                                                                                        <button
                                                                                            class="btn btn-primary ajaxStore"
                                                                                            type="submit">ثبت
                                                                                        </button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                                <li class="row mb-1">
                                        <label class="col-lg-3 col-md-5 col-sm-7 d-flex align-items-center"> ورود دو
                                            مرحله ای  google authenticator
                                        </label>
                                        <div>
                                            @if (!empty(auth()->user()->google2fa_secret))
                                                @if(auth()->user()->login_2fa == true)
                                                    <button type="button"
                                                            class="btn btn-danger waves-effect waves-light"
                                                            data-toggle="modal"
                                                            data-target="#login_2fa">غیرفعال کردن
                                                    </button>
                                                @else
                                                    <button type="button"
                                                            class="btn btn-success waves-effect waves-light"
                                                            data-toggle="modal"
                                                            data-target="#login_2fa">فعال کردن
                                                    </button>
                                                @endif
                                                <div class="modal fade text-left"
                                                     id="login_2fa" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="login_2faLabel1"
                                                     aria-hidden="true">
                                                    <div
                                                        class="modal-dialog modal-dialog-scrollable"
                                                        role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title"
                                                                    id="login_2faLabel1">Modal
                                                                    پایه</h4>
                                                                <button type="button"
                                                                        class="close"
                                                                        data-dismiss="modal"
                                                                        aria-label="Close">
                                                                                                <span
                                                                                                    aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="container">
                                                                    <div class="row">
                                                                        <div class="col-md-8 col-md-offset-2 mx-auto">
                                                                            <div class="panel panel-default">
                                                                                <h5 class="panel-heading "
                                                                                    style="text-align: center;">غیرفعالسازی گوگل اتنتیکاتور</h5>

                                                                                <div class="panel-body"
                                                                                     style="text-align: center;">
                                                                                    <form method="post"
                                                                                          action="{{ route('setting.login_2fa') }}">
                                                                                        @csrf
                                                                                        <input type="hidden" value="deactivate"
                                                                                               name="type">
                                                                                        <div class="form-group">
                                                                                            <label>Authenticator
                                                                                                Code</label>
                                                                                            <input type="text"
                                                                                                   name="one_time_password"
                                                                                                   class="form-control">
                                                                                        </div>
                                                                                        <div>
                                                                                            <button
                                                                                                class="btn btn-primary ajaxStore"
                                                                                                type="submit">ثبت
                                                                                            </button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <span class="text-danger">لطفا اول گوگل اتنتیکاتور را فعال کنید</span>
                                            @endif
                                        </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="card overflow-hidden">
                    <div class="card-header">
                        <h5>
                            <i class="fa fa-shield"></i>
                            <span>امنیت </span>
                        </h5>
                    </div>
                    <hr>
                    <div class="card-content">
                        <div class="card-body">
                            <p>
                                کاربر گرامی در این بخش می توانید موارد امنیتی حساب کاربری خود را بررسی و اصلاح نمایید
                            </p>

                            <form method="post" action="{{ route('setting.change_confirmation_type') }}"
                                  class="select_confirm_type">
                                @csrf
                                <ul class="list-unstyled mb-0 mt-2 d-flex flex-row align-items-center">
                                    <li class="d-inline-block mr-2">
                                        <p class="mb-0">روش تایید: </p>
                                    </li>
                                    <li class="d-inline-block mr-2">
                                        <fieldset>
                                            <div class="vs-radio-con">
                                                <input type="radio" name="confirm_type"
                                                       @if($user->confirm_type == 'sms') checked @endif value="sms">
                                                <span class="vs-radio">
                      <span class="vs-radio--border"></span>
                      <span class="vs-radio--circle"></span>
                    </span>
                                                <span class="">SMS</span>
                                            </div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2">
                                        <fieldset>
                                            <div class="vs-radio-con">
                                                <input type="radio" name="confirm_type"
                                                       @if($user->confirm_type == 'google-authenticator') checked
                                                       @endif value="google-authenticator">
                                                <span class="vs-radio">
                      <span class="vs-radio--border"></span>
                      <span class="vs-radio--circle"></span>
                    </span>
                                                <span class="">Google Authenticator</span>
                                            </div>
                                        </fieldset>
                                    </li>
                                </ul>
                            </form>
                            <p class="waiting" style="display: none">لطفا منتظر بمانید ...</p>
                            <form method="post" action="{{ route('setting.verify_confirmation_type') }}"
                                  class="confirmation-code" style="display: none">
                                @csrf
                                <input type="hidden" name="confirm_type" value="">
                                <ul class="list-unstyled mb-0 mt-2 d-flex flex-row align-items-center">
                                    <li class="d-inline-block mr-2">
                                        <label class="mb-0">کد تایید: </label>
                                    </li>
                                    <li class="d-inline-block mr-2">
                                        {{--<fieldset>
                                            <input class="form-control" type="number" placeholder="مثال: 123456"
                                                   name="one_time_password">
                                        </fieldset>--}}
                                        <fieldset class="form-group position-relative input-divider-right mb-0">
                                            <div class="code-verify">
                                                <input type='text' class='form-control text-right'
                                                       name='one_time_password'
                                                       placeholder="مثال: 12345"
                                                       style='letter-spacing: 5px'>
                                            </div>
                                            <div class="form-control-position time "
                                                 style="border-right: solid 1px gainsboro"></div>
                                        </fieldset>
                                    </li>
                                    <li class="d-inline-block mr-2">
                                        <fieldset>
                                            <button type="button" class="btn btn-success ajaxStore">تایید</button>
                                        </fieldset>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{asset('theme/user/app-assets/js/scripts/pages/app-user.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/navs/navs.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/js/scripts/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/extensions/swiper.min.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js')}}"></script>
    <script src="{{asset('theme/user/app-assets/vendors/js/vendors.min.js')}}"></script>

    <script>

        var confirm_type = "{{ $user->confirm_type }}"

        function hide_timer() {
            $(".confirmation-code").hide();
            $(".select_confirm_type input[name='confirm_type']:not(:checked)").click();
        }

        function show_confirm_code(response) {
            $(".select_confirm_type").show();
            $(".waiting").hide();
            if (response.status == 200) {
                let confirm_type = $(".select_confirm_type input[name='confirm_type']:checked").val();
                $(".confirmation-code").show(100);
                $(".confirmation-code input[name='confirm_type']").val(confirm_type);
            }
            if (response.time) {
                $(".confirmation-code .time").show();
                timer_count(response.time, '.confirmation-code .time', hide_timer);
            } else {
                $(".confirmation-code .time").hide().text('');
            }
        }

        $(".select_confirm_type input[name='confirm_type']").change(function () {

            if (confirm_type == $(this).val()) {
                $(".confirmation-code").hide(100);
                return;
            }
            if ($(this).val() == 'google-authenticator') {
                $(".select_confirm_type").hide();
                $(".waiting").show();
                $(".confirmation-code label").text('کد تاییدیه SMS')
                let form = $(this).parents('form');
                ajax(form, show_confirm_code);
            } else {
                $(".confirmation-code label").text('کد دریافتی از google authenticator: ')
                show_confirm_code({'status': 200});
            }

        })
    </script>
@endsection
