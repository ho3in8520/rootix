@extends('templates.user.master_page')
@section('title_browser')

    {{__('titlePage.title-minePage')}}

@endsection
@section('style')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css-rtl/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css-rtl/pages/dashboard-ecommerce.min.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/vendors/css/charts/apexcharts.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css-rtl/core/menu/menu-types/horizontal-menu.min.css') }}">
    <link rel="stylesheet" type="text/css"
          href="{{ asset('theme/user/app-assets/css-rtl/core/colors/palette-gradient.min.css') }}">
    <!-- END: Page CSS-->

@endsection
@section('content')
    <div class="row settings">
        <div class="col-md-12 col-lg-12">
        @if(!auth()->user()->is_complete_steps)
            <!-- Level User Alert -->
                <div class="col-md-12">
                    <div class="alert alert-warning py-2 text-center">
                        <span>{{__('attributes.level')}} {{ convertStepToPersian(auth()->user()->step_complate) }}</span>
                        <a class="btn btn-warning text-white"
                           href="{{ route('step.verify-mobile') }}">{{__('attributes.authentication')}}</a>
                    </div>
                </div>
                <!--## Level User Alert -->
        @endif
        <!-- Top Widget -->
            <div class="row">
                @foreach($price_currency as $key=>$item)
                    <div class="col-xl-2 col-md-3 col-sm-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-header py-0" style="background-color: {{ currency_color($key) }}">
                                    <img class="card-img img-fluid mx-auto p-2" style="width: 85%;"
                                         src="{{ asset('/theme/user/assets/img/'.strtolower($key).'.png') }}"
                                         alt="Card image cap">
                                </div>
                                <div class="card-body py-1 text-center" style="font-size: small">
                                    <h5 class="">{{ number_format($item['bestSell']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">تومان</span>
                                    </h5>
                                    @if ($item['dayChange'] >0 )
                                        <p class="badge badge-success mb-0">{{ $item['dayChange'] }}%</p>
                                    @else
                                        <p class="badge badge-danger mb-0">{{ $item['dayChange'] }}%</p>
                                    @endif
                                    <hr style="margin: 5px auto;">
                                    <h5 class="">{{ number_format($item['dayLow']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">تومان</span>
                                    </h5>
                                    <p class="text-black-50">تومان حداقل</p>
                                    <hr style="margin: 5px auto;">
                                    <h5 class="">{{ number_format($item['dayHigh']/10) }}
                                        <span class="text-black-50" style="font-size: 13px;">تومان</span>
                                    </h5>
                                    <p class="text-black-50">تومان حداکثر</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <!--## Top Widget -->

            <div class="row">
                <!-- Wallet Chart -->
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>کیف های پول</h4>
                        </div>
                        <div class="card-body row">{{--
                            <div class="col-md-6">
                                <div id="wallet-chart" class="mx-auto"></div>
                            </div>--}}
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        @foreach($assets as $item)
                                            <tr>
                                                <td><img
                                                        src="{{ asset('theme/user/assets/img/'.strtolower($item['unit']).'.png') }}"
                                                        width="45"></td>
                                                <td class="font-weight-bold">{{ $item['unit'] }}</td>
{{--                                                <td>{{ num_format($item['amount']) }} {{ $item['unit'] }}</td>--}}
                                                <td>{{ rial_to_unit($item['amount'],$item['unit'],true) }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{--<div class="col-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td rowspan="3" class="font-weight-bold">ارزش تخمینی دارایی ها</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">پیشنهاد های خرید</td>
                                            <td>150,000,000 تومان</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">پیشنهاد های فروش</td>
                                            <td>98,000,000 تومان</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
                <!--## Wallet Chart -->

                <!-- profile Status -->
                <div class=" col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>وضعیت حساب کاربری</h4>
                        </div>
                        <div class="card-body row">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td class="font-weight-bold">سطح کاربری:</td>

                                        <td>تایید شده سطح {{ convertStepToPersian(auth()->user()->step_complate) }} <a
                                                href="{{ route('step.verify-mobile') }}"
                                                class="px-1 text-info">ارتقا</a></td>
                                        <td><a class="fa fa-question-circle text-info" style="font-size: 20px;"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">برداشت روزانه ریال:</td>
                                        <td>0 از {{ number_format(transactionLimit()) }} تومان</td>
                                        <td></td>
                                    </tr>
                                    {{--<tr>
                                        <td class="font-weight-bold">برداشت روزانه رمزارز:</td>
                                        <td>0 از 200,000,000 تومان</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">مجموع برداشت روزانه:</td>
                                        <td>0 از 500,000,000 تومان</td>
                                        <td><a class="fa fa-question-circle text-info" style="font-size: 20px;"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">مجموع برداشت ماهانه:</td>
                                        <td>0 از 3,000,000,000 تومان</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">کارمزد معاملات:</td>
                                        <td>0.3% (پله بعد از 0.3%)</td>
                                        <td><a class="fa fa-question-circle text-info" style="font-size: 20px;"></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="font-weight-bold">ارزش معاملات 30 روز:</td>
                                        <td>- (پله بعد از 10 میلیون تومان)</td>
                                        <td></td>
                                    </tr>--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--## profile Status -->

                <!-- Open Your Orders -->
            {{--<div class=" col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>سفارشات باز شما</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="reload"><i class="feather icon-rotate-cw"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body row">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>سمت</th>
                                        <th>قیمت سفارش</th>
                                        <th>مقدار</th>
                                        <th>ارز</th>
                                        <th>مبلغ کل</th>
                                        <th>پر شده</th>
                                        <th>اقدامات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="7" class="text-center">سفارشی ثبت نشده</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!--## Open Your Orders -->

                <!-- Last Transactions -->
            {{--<div class=" col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>معاملات اخیر شما</h4>
                    </div>
                    <div class="card-body row">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>زمان</th>
                                    <th>بازار</th>
                                    <th>نوع</th>
                                    <th>قیمت</th>
                                    <th>حجم</th>
                                    <th>مبلغ کل</th>
                                    <th>کارمزد</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($finance_transactions) > 0)
                                    @foreach($finance_transactions as $item)
                                        <tr>
                                            <td>1400/05/05 22:10</td>
                                            <td>بازار 1</td>
                                            <td>نوع 1</td>
                                            <td>50,000 تومان</td>
                                            <td>500 عدد</td>
                                            <td>55,000 تومان</td>
                                            <td>10%</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">شما معامله ای انجام نداده اید</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!--## Last Transactions -->
            </div>

        </div>
    </div>
@endsection
@section('script')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('theme/user/app-assets/vendors/js/ui/jquery.sticky.js') }}"></script>
    <script src="{{ asset('theme/user/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('theme/user/app-assets/js/scripts/charts/chart-apex.min.js') }}"></script>
    <!-- END: Page JS-->

    <script>
        var walletChartOptions = {
            chart: {
                type: 'donut',
                height: 350
            },
            colors: ['#01c19c', '#fe9e56'],
            series: [44, 55],
            labels: ['Team A', 'Team B'],
            legend: {
                show: false
            },
            responsive: [
                {
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 350
                        },
                        legend: {
                            position: 'bottom'
                        }
                    }
                },
                {
                    breakpoint: 380,
                    options: {
                        chart: {
                            width: 200,
                            height: 260
                        },
                    }
                },
            ]
        }
        var WalletChart = new ApexCharts(
            document.querySelector("#wallet-chart"),
            walletChartOptions
        );
        WalletChart.render();
    </script>


    <script>
        $(document).ready(async function () {
            var usdt = document.getElementsByClassName('usdt_balance');
            if (usdt && "{{!empty($token)}}") {
                var contract = await tronWeb.contract()
                    .at("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");

                var result = await contract.balanceOf("{{!empty($token['address']['base58']) ? $token['address']['base58'] : ' '}}").call();
                console.log(result / 1000000);
                $('.usdt_balance').html(result / 1000000);
            }
        });

        function separate(Number) {
            Number += '';
            Number = Number.replace(',', '');
            x = Number.split('.');
            y = x[0];
            z = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(y))
                y = y.replace(rgx, '$1' + ',' + '$2');
            return y + z;
        }

        $('#tag').on('click', function () {
            if ("{{isset($token['address']['base58'])}}") {
                //$(this).attr('href', "{{--{{route('swap.show',['unit'=>'usdt'])}}--}}");
            } else {
                swal('ناموفق', 'لطفا ابتدا کیف پول خود را بسازید!! سپس میتوانید اقدام به تبدیل ارز نمایید.', 'error')
            }
        });


        function create_account(unit) {
            const fullNode = 'https://api.trongrid.io';
            const solidityNode = 'https://api.trongrid.io';
            const eventServer = 'https://api.trongrid.io';

            const tronWeb = new TronWeb(fullNode, solidityNode, eventServer);
            var account = tronWeb.createAccount();
            var res = Promise.resolve(account);
            res.then(function (v) {
                var account = v;

                $.ajax({
                    url: '{{ route('createAccount', '') }}' + "/" + unit,
                    type: 'post',
                    data: {account: account, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                        if (data.status == '100') {
                            swal('موفق', data.msg, 'success');
                            setTimeout(function () {
                                window.location.reload()
                            }, 2000)
                        } else {
                            swal('ناموفق', data.msg, 'danger');
                        }

                    }
                });
            }, function (e) {
                console.error(e); // TypeError: Throwing
            });
        }

        function copy(input) {
            var copyText = document.getElementById(input);
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            swal('موفق', 'ادرس شما با موفقیت کپی شد', 'success');
        }


        $(document).ready(function () {
            $(document).on('keyup', '.amount-rial', function () {
                var val = $(this).val();
                $(this).val(numberFormat(val))
            })
        })
    </script>
@endsection
