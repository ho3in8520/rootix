@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-accountBank')}}
@endsection
@section('style')
    <style>
        .question-icon {
            display: inline-block;
            vertical-align: text-top;

        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css-rtl/pages/data-list-view.min.css') }}">
@endsection
@section('content')
    <section id="data-list-view" class="data-list-view-header">
        <div class="action-btns d-none">
            <div class="btn-dropdown mr-1 mb-1">
                <div class="btn-group dropdown actions-dropodown">
                    <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
                            data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"> {{__('attributes.bank.actions')}}
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#"><i
                                class="feather icon-trash"></i>{{__('attributes.bank.delete')}}</a>
                        <a class="dropdown-item" href="#"><i
                                class="feather icon-archive"></i>{{__('attributes.bank.archive')}}</a>
                        <a class="dropdown-item" href="#"><i
                                class="feather icon-file"></i>{{__('attributes.bank.print')}}</a>
                        <a class="dropdown-item" href="#"><i
                                class="feather icon-save"></i>{{__('attributes.bank.other')}}</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row my-2 col-12">
            <a href="{{ route('banks.create') }}" class="btn btn-info">{{__('attributes.bank.addBank')}}</a>
        </div>

        <!-- DataTable starts -->
        <div class="table-responsive card card-body ">
            <table class="table table-striped table-hover-animation">
                <thead>
                <tr>
                    <th></th>
                    <th>{{__('attributes.bank.name')}}</th>
                    <th>{{__('attributes.bank.cardNum')}}</th>
                    <th>{{__('attributes.bank.accountNumber')}}</th>
                    <th>{{__('attributes.bank.shaba')}}</th>
                    <th>وضعیت</th>
                    <th>{{__('attributes.bank.date')}}</th>
                    <th>{{__('attributes.bank.actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @if (count($banks) > 0)
                    @foreach($banks as $i=>$bank)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $bank->name }}</td>
                            <td>{{ $bank->card_number }}</td>
                            <td>{{ $bank->account_number }}</td>
                            <td>{{ $bank->sheba_number }}</td>
                            <td>
                                <span class="badge badge-{{ $bank->status_color }}">
                                    {{ $bank->status_text }}
                                </span>
                                @if ($bank->status == 3)
                                    <sub  data-toggle="tooltip" title="{{ $bank->reject_reason }}" class="badge badge-pill badge-danger animate__animated animate__fadeOut animate__infinite infinite question-icon">
                                        <i class="fa fa-question text-white"></i>
                                    </sub>
                                @endif
                            </td>
                            <td>{{ jdate_from_gregorian($bank->created_at,'Y/m/d') }}</td>
                            <td class="product-action">
                                <span class="action-edit" style="cursor: pointer"><a
                                        href="{{ route('banks.edit',$bank) }}" class="feather icon-edit"></a></span>
                                <span style="cursor: pointer"><a data-icon="warning"
                                                                 data-text="{{__('attributes.bank.errDelete')}}"
                                                                 data-confirm="{{__('attributes.bank.deleteFunction')}}"
                                                                 data-action="{{ route('banks.destroy', $bank) }}"
                                                                 data-method="delete"
                                                                 class="feather icon-trash text-danger"></a></span>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="15" class="text-center">{{__('attributes.bank.display')}}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <!-- DataTable ends -->
    </section>
@endsection
