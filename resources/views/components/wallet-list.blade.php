<div class="dashboard-table wallet-table-container-self">
    <div class="wallet-table-container">
        <div class="table-loading">
            <img src="{{ asset('theme/user/images/wallet/spinner.svg') }}" class="table-loding-spinner">
        </div>
        <div class="row">
            <div class="col-12">
                <button class="table__refresh-btn" style="float: left;margin-bottom: 18px;">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="28"
                        viewBox="0 0 26.997 31.499"
                        class="refresh-icon"
                    >
                        <path
                            d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                            transform="translate(-4.5 -2.251)"
                            fill="#4a4a4a"
                        />
                    </svg>
                    بروزرسانی
                </button>
                <table class="wallet-table">
                    <thead>
                    <tr>
                        <th>
                            <div>
                                نام ارز / میزان دارایی
                                <button class="sort-table-btn">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="10"
                                        height="20.383"
                                        viewBox="0 0 12.203 20.383">
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"/>
                                    </svg>
                                </button>
                            </div>
                        </th>
                        @if(in_array('global-price',$fields))
                            <th>
                                <div>
                                    قیمت جهانی
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383">
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"/>
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('24h',$fields))
                            <th>
                                <div>
                                    24h%
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('7d',$fields))
                            <th>
                                <div>
                                    7d%
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('market-volume',$fields))
                            <th>
                                <div>
                                    حجم کل بازار
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('daily-market-volume',$fields))
                            <th>
                                <div>
                                    حجم بازار
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('inventory',$fields))
                            <th>
                                <div>
                                    موجودی
                                    <button class="sort-table-btn">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383">
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"/>
                                        </svg>
                                    </button>
                                </div>
                            </th>
                        @endif
                        @if(in_array('deposit',$fields) || in_array('withdraw',$fields))
                            <th>
                                <div class="table__refresh-btn-container">
                                    واریز/برداشت
                                </div>
                            </th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($assets as $asset)
                        @php
                            $unit= $asset->unit == 'rial'?
                            (isset($list_currencies['RLS'])?'RLS':'RIAL'):
                            strtoupper($asset->unit);
                            if (!isset($list_currencies[$unit])) continue;
                            $currency= $list_currencies[$unit];
                            $currency_1d= $currency['1d'];
                            $currency_7d= $currency['7d'];
                            $market_volume= market_cap_volume($currency['market_cap']);
                            $daily_market_volume= $currency['volume']?market_cap_volume($currency['volume']):null;
                            $amount=blocked_asset($asset);
                        @endphp
                        <tr>
                            <!-- LOGO -->
                            <td>
                                <div class="wallet-table__curency-desc">
                                    <a href="#" class="wallet-table__currency-img-container">
                                        <img src="{{ $currency['logo_url'] }}"
                                             alt="{{ $currency['name_fa'] }}"/>
                                    </a>

                                    <div class="wallet-table__curency-title-container">
                                        <span>{{ $currency['currency'] }}
                                            @if($amount['des'] != null)
                                                <span
                                                    class="attention animate__animated animate__flash animate__infinite infinite animate__slower 3s"
                                                    data-toggle="tooltip" title=""
                                                    data-original-title="{{$amount['des']}}">!</span>
                                            @endif</span>
                                        <p>{{ rial_to_unit($amount['amount'],strtolower($unit),true) }}</p>

                                        {{--                                        <p>{{ rial_to_unit($asset->amount,strtolower($unit),true) }}</p>--}}
                                    </div>
                                </div>
                            </td><!--# LOGO-->

                        @if(in_array('global-price',$fields))
                            <!-- GLOBAL PRICE -->
                                <td>
                                    @if($asset->unit != 'rls')
                                        <div>
                                            <span class="wallet-table-currency-price">
                                                {{ rial_to_unit($currency['rls'],'rls',true) }}
                                            </span>
                                            <p>
                                                {{ num_format($currency['price'],contractUnit('usdt')['precision']) }}
                                                $
                                            </p>
                                        </div>
                                    @else
                                        <hr class="empty-line">
                                    @endif
                                </td><!--# GLOBAL PRICE -->
                        @endif

                        @if(in_array('24h',$fields))
                            <!-- 24h -->
                                <td>
                                    <div>
                                        @if (!is_null($currency_1d))
                                            <span
                                                class="change-percents {{ $currency_1d >= 0?'green':'red' }}-currency"
                                                dir="ltr">
                                                    {{ $currency_1d > 0 ? '+':''}}{{ number_format((float)$currency_1d,2) }}
                                            </span>
                                            {{--                                            <p>24H</p>--}}
                                        @else
                                            <hr class="empty-line">
                                        @endif
                                    </div>
                                </td><!--# 24h -->
                        @endif

                        @if(in_array('7d',$fields))
                            <!-- 7d -->
                                <td>
                                    <div>
                                        @if (!is_null($currency_7d))
                                            <span
                                                class="change-percents {{ $currency_7d >= 0?'green':'red' }}-currency"
                                                dir="ltr">
                                                    {{ $currency_7d > 0 ? '+':''}}{{ number_format((float)$currency_7d,2) }}
                                            </span>
                                            {{--                                            <p>7d</p>--}}
                                        @else
                                            <hr class="empty-line">
                                        @endif
                                    </div>
                                </td><!--# 7d -->
                        @endif

                        @if(in_array('market-volume',$fields))
                            <!-- MARKET VOLUME -->
                                <td>
                                    <div>
                                  <span class="wallet-table-currency-price">
                                      {{ isset($market_volume[0])?$market_volume[0]:'-' }}
                                  </span>
                                        <p>
                                            {{ isset($market_volume[1])?$market_volume[1]:'-' }}
                                        </p>
                                    </div>
                                </td><!--# MARKET VOLUME -->
                        @endif

                        @if(in_array('daily-market-volume',$fields))
                            <!-- DAILY MARKET VOLUME -->
                                <td>
                                    <div>
                                  <span class="wallet-table-currency-price">
                                      {{ isset($daily_market_volume[0])?$daily_market_volume[0]:'-' }}
                                  </span>
                                        <p>
                                            {{ isset($daily_market_volume[1])?$daily_market_volume[1]:'-' }}
                                        </p>
                                    </div>
                                </td><!--# DAILY MARKET VOLUME -->
                        @endif

                        @if(in_array('inventory',$fields))
                            <!-- INVENTORY -->
                                <td>
                                    <div>
                                  <span class="wallet-table-currency-price">
{{--                                      {{ convert_currency($asset->unit,'rls',$asset->amount,true,$list_currencies) }}--}}
                                      {{ convert_currency($asset->unit,'rls',$amount['amount'],true,$list_currencies) }}
                                  </span>
                                        <p>
                                            {{--                                            {{ num_format(convert_currency($asset->unit,'usdt',$asset->amount,false,$list_currencies),contractUnit('usdt')['precision']) }}--}}
                                            {{ num_format(convert_currency($asset->unit,'usdt',$amount['amount'],false,$list_currencies),contractUnit('usdt')['precision']) }}
                                            $
                                        </p>
                                    </div>
                                </td><!--# INVENTORY -->
                        @endif

                        @if(in_array('deposit',$fields) || in_array('withdraw',$fields))
                            @php
                                $can_deposit=$unit=='RLS'?'rial_deposit':'crypto_deposit';
                                $can_withdraw=$unit=='RLS'?'rial_withdraw':'crypto_withdraw';
                            @endphp
                            <!-- OPERATION -->
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        @if(in_array('withdraw',$fields))
                                            @can($can_withdraw)
                                                {{--                                                <a href="{{ $unit=='RLS'?route('wallet.withdraw.rial.create'):route('withdraw-form',$unit) }}"--}}
                                                {{--                                                   class="btn wallet-btn harvest-btn">--}}
                                                {{--                                                    برداشت--}}
                                                {{--                                                </a>--}}
                                                @if($unit=='RLS')
                                                    <a href="{{ route('wallet.withdraw.rial.create') }}" class="btn wallet-btn harvest-btn flex-column disabled">
                                                        برداشت
                                                    </a>
                                                @else
                                                    <a href="{{ route('withdraw-form',$unit) }}"
                                                       class="btn wallet-btn harvest-btn disabled">
                                                        برداشت
                                                    </a>
                                                @endif

                                            @endcan
                                        @endif

                                        @if(in_array('deposit',$fields))
                                            @if (in_array(strtolower($unit),['usdt','trx','rls']))
                                                @can($can_deposit)
                                                    <a href="{{ $unit=='RLS'?route('wallet.deposit.rial.create'):route('deposit-form',$unit) }}"
                                                       class="btn wallet-btn deposit-btn disabled">
                                                        واریز
                                                    </a>
                                                @endcan
                                            @else
                                                @can($can_deposit)
                                                    <button disabled class="btn wallet-btn deposit-btn flex-column">
                                                        واریز
                                                        <span class="soon__btn">(به زودی)</span>
                                                    </button>
                                                @endcan
                                            @endif
                                        @endif
                                    </div>
                                </td><!--# OPERATION -->
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
