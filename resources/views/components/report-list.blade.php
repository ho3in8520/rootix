<div class="system-reports" id="basic-form-layouts">
    <div class="filters-container">
        <button
            class="col-12 filter-btn"
            data-toggle="modal"
            data-target="#toggle-filters-inputs"
        >
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width="23.814"
                height="20.15"
                viewBox="0 0 23.814 20.15"
            >
                <g transform="translate(-3.375 -5.625)">
                    <path
                        d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                        transform="translate(0 -3.554)"
                    />
                    <path
                        d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                        transform="translate(0 -1.777)"
                    />
                    <path
                        d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"
                    />
                </g>
            </svg>
            <span class="pr-1">فیلترها</span>
        </button>
        <div
            id="toggle-filters-inputs"
            class="up-animation modal"
        >
            <div class="modal-content pt-0">
                <div class="modal-header">
                    <h6 class="modal-title">فیلترهای جدول</h6>
                    <button
                        type="button"
                        class="close ml-0 mr-auto"
                        data-dismiss="modal"
                    >
                        &times;
                    </button>
                </div>

                <div class="modal-body p-md-0">
                    <form class="search-report" method="get" action="">
                        <div class="form-controlers pb-2">
                            <div class="header-transactionrls header-transactionrls-top mb-3">
                                <div class="w-100 bg-white">
                                    <label class="mb-2 mt-3 d-block LblName fs-16"
                                    >کد تراکنش</label
                                    >
                                    <input
                                        type="text"
                                        name="code"
                                        id="LblName"
                                        placeholder="..."
                                    />
                                </div>

                                <div class="w-100 bg-white">
                                    <label
                                        class="mb-2 mt-3 d-block LblEmail fs-16"
                                    >نوع تراکنش</label
                                    >
                                    <select class="custom-selected" name="type">
                                        <option value="">همه</option>
                                        <option value="2">واریز</option>
                                        <option value="1">برداشت</option>
                                    </select>
                                </div>

                                <div class="w-100 bg-white">
                                    <label
                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                    >ارز</label
                                    >
                                    <select class="custom-selected" name="currency">
                                        <option value="">همه</option>
                                        @foreach($assets as $asset)
                                            <option value="{{ $asset->unit }}">{{ $asset->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="w-100 bg-white">
                                    <label
                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                    >مبلغ</label
                                    >
                                    <input
                                        type="text"
                                        name="amount"
                                        id="LblEmail"
                                        placeholder="..."
                                    />
                                </div>
                            </div>
                            <div class="header-transactionrls row mb-3 justify-content-start">
                                <div class="col-12 col-md-3">
                                    <div class="w-100 bg-white">
                                        <label
                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                        >تاریخ از</label
                                        >
                                        <input
                                            type="text"
                                            class="datepicker datePicker"
                                            name="start_created_at"
                                            id="LblDateMoney"
                                            placeholder="..."
                                        />
                                    </div>
                                </div>

                                <div class="col-12 col-md-3">
                                    <div class="w-100 bg-white">
                                        <label
                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                        >تاریخ تا</label
                                        >
                                        <input
                                            class="last-input datepicker datePicker"
                                            type="text"
                                            name="end_created_at"
                                            id="LblDateMoney2"
                                            placeholder="..."
                                        />
                                    </div>
                                </div>

                                <div class="col-12 col-md-3">
                                    <button
                                        type="button"
                                        class="btn fixed-bottom transaction__btn2 search-report-button"
                                        data-dismiss="modal" href="{{ request()->url() }}"
                                    >
                                        ثبت
                                    </button>
                                </div>


                            </div>


                            <!-- <div
                              class="header-transactionrls mb-3 justify-content-start"
                            >
                              <div class="bg-white bottom-transactions">
                                <label
                                  class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                  >تاریخ از</label
                                >
                                <input
                                  type="text"
                                  class="datepicker"
                                  id="LblDateMoney"
                                  placeholder="..."
                                />
                              </div>

                              <div
                                class="date-transaction bg-white bottom-transactions"
                              >
                                <label
                                  class="mb-2 mt-3 d-block transaction__filter-title fs-16"
                                  >تاریخ تا</label
                                >
                                <input
                                  class="last-input datepicker"
                                  type="text"
                                  id="LblDateMoney2"
                                  placeholder="..."
                                />
                              </div>

                              <div
                                class="date-transaction bg-white bottom-transactions"
                              >
                                <button
                                  type="button"
                                  class="btn fixed-bottom transaction__btn2"
                                  data-dismiss="modal"
                                >
                                  ثبت
                                </button>
                              </div>
                              <div class="bottom-transactions"></div>
                            </div> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="component-report">
        <div class="row">
            <div class="report-table col-12">
                <div class="overflow-auto">
                    <div class="table-responsive specification">
                        <table class="container Specification-table mt-5">
                            <thead>
                            <tr>
                                <th class="headline">
                                    <div class="code">کد تراکنش</div>
                                </th>
                                <th class="header headline">
                                    <div>نوع تراکنش</div>
                                </th>
                                <th class="header headline">
                                    <div>ارز</div>
                                </th>
                                <th class="header headline">
                                    <div>مبلغ</div>
                                </th>
                                <th class="header headline">
                                    <div>توضیحات</div>
                                </th>
                                <th class="headline">
                                    <div class="date">تاریخ عضویت</div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reports as $report)
                                @php
                                    $type=[1=>'برداشت',2=>'واریز'];
                                    $class=[1=>'deposit',2=>'deposit'];
                                @endphp

                                <tr>
                                    <td class="numbers pt-4">
                                        @if($report->tracking_code)
                                            {{ \Illuminate\Support\Str::limit($report->tracking_code,10) }}
                                            <a href="https://tronscan.org/#/transaction/{{ $report->tracking_code }}"
                                               title="نمایش وضعیت" target="_blank">
                                                <img class="arrow" src="{{ asset('theme/user/images/Group 1779.svg') }}"
                                                     alt="" width="30">
                                            </a>
                                        @else
                                            -
                                        @endif

                                    </td>
                                    <td class="header user-level pt-4">
                                        <span class="{{ $class[$report->type] }}">{{ $type[$report->type] }}</span>
                                    </td>
                                    <td class="currency header pt-4">{{ $report->financeable->unit }}</td>
                                    <td class="amount header pt-4">
                                        {{ $report->amount }}
                                    </td>
                                    <td
                                        class="description header pt-4"
                                        style="max-width: 250px"
                                    >
                                        <p>
                                            {{ $report->description }}
                                        </p>
                                    </td>
                                    <td class="more header pt-4">
                                        <svg
                                            data-toggle="modal"
                                            data-target="#exampleModal"
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="27"
                                            height="5.344"
                                            viewBox="0 0 27 5.344"
                                        >
                                            <g transform="translate(-4.5 -15.328)">
                                                <path
                                                    d="M17.986,15.328A2.672,2.672,0,1,0,20.658,18a2.671,2.671,0,0,0-2.672-2.672Z"
                                                />
                                                <path
                                                    d="M7.172,15.328A2.672,2.672,0,1,0,9.844,18a2.671,2.671,0,0,0-2.672-2.672Z"
                                                />
                                                <path
                                                    d="M28.828,15.328A2.672,2.672,0,1,0,31.5,18a2.671,2.671,0,0,0-2.672-2.672Z"
                                                />
                                            </g>
                                        </svg>
                                        <div
                                            class="modal fade"
                                            tabindex="-1"
                                            role="dialog"
                                            aria-labelledby="exampleModalLabel"
                                            aria-hidden="true"
                                        >
                                            <div
                                                class="modal-dialog"
                                                role="document"
                                            >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">
                                                            توضیحات
                                                        </h5>
                                                        <button
                                                            type="button"
                                                            class="close"
                                                            data-dismiss="modal"
                                                            aria-label="Close"
                                                        >
                                                <span aria-hidden="true"
                                                >&times;</span
                                                >
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        مبلغ 1,932.46242971 از سواپ به
                                                        حساب شما واریز گردید trx مبلغ
                                                        1,932.46242971 کارمزد سواپ: 3,000
                                                        تومان
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button
                                                            type="button"
                                                            class="btn btn-secondary"
                                                            data-dismiss="modal"
                                                        >
                                                            Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="date pt-5">
                                        {{ jdate_from_gregorian($report->created_at,'%A, %d %B %Y') }}
                                        <span class="time">{{ jdate_from_gregorian($report->created_at,'H:i') }}</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $reports->withQueryString()->links() }}

                <div
                    class="pdf-exel mt-5"
                    style="margin-top: 43px !important"
                >
                    <button class="btn-pdf mt-2">
                        <p class="card-text">
                            <a href="{{ request()->fullUrlWithQuery(['export_report'=>'pdf']) }}" target="_blank" class="pdf-text">خروجی PDF</a>
                        </p>
                        <img
                            class="pdf-image"
                            src="{{ asset('theme/user/images/Icon metro-file-pdf.svg') }}"
                        />
                    </button>

                    <button class="btn-exel mt-2 mr-2">
                        <p class="card-text">
                            <a href="{{ request()->fullUrlWithQuery(['export_report'=>'excel']) }}" target="_blank" class="exel-text">خروجی Excel</a>
{{--                        </p>--}}
                        <img
                            class="excel-image"
                            src="{{ asset('theme/user/images/Icon simple-microsoftexcel.svg') }}"
                        />
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@section('script')
    <script>
        $(document).on('click', '.pagination a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href')
            $.get(url, {}, function (data) {
                let val = $(data).find(".component-report")
                $('.component-report').html(val);
            });
        })
        $(document).on('click', '.search-report-button', function () {
            var url = $(this).attr('href')
            var filter = $("form.search-report").serialize()
            if (window.location.href.split('?').length >= 2) {
                url = url + '&' + filter

            } else {
                url = url + '?' + filter
            }
            $.get(url, {}, function (data) {
                let val = $(data).find(".component-report")
                $('.component-report').html(val);
            });
        })
    </script>

@endsection
