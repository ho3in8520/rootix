<div class="panel-box panel-box__padding-0 h-1027 overflow-y-auto position-relative" wire:init="loadRequestTicker">
    <div class="d-flex justify-center align-items-center h-100 spinner">
        <div class="spinner-border text-primary mx-auto" role="status">
        </div>
    </div>
{{--    <div class="panel-box__padding-1">--}}
{{--        <form class="trad-seachbox">--}}
{{--                      <span class="trad-searchbox__icon">--}}
{{--                        <i class="fa fa-search"></i>--}}
{{--                      </span>--}}
{{--            <input type="text" placeholder="سرچ کنید..."/>--}}
{{--        </form>--}}
{{--    </div>--}}

    <div class="tread-tabs">
        <div class="tread-tabs__content tread-tabs__content-2">
            <table class="tread-table ticker">
                <thead>
                <tr>
                    <th>نماد</th>
                    <th>مارکت</th>
                    <th>قیمت</th>
                </tr>
                </thead>

                <tbody>
                @foreach($result as $key=>$item)
                    <tr style="cursor: pointer"
                        data-href="{{ route('exchange.index',['des_unit'=>strtolower($item['currency']),'source_unit'=>'usdt']) }}">
                        <td>
                            <img src="{{ $item['logo_url'] }}" alt="{{ $item['name'] }}" width="25">
                        </td>
                        <td>{{ $key }}/USDT</td>
                        <td>{{ $item['price'] }}</td>
{{--                        <td class="{{ ($item['1d']<0)?'red':'green' }}">{{ $item['1d'] }}--}}
{{--                        </td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
