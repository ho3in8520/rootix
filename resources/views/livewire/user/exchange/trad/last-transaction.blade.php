<table class="tread-table" wire:init="loadRequestTransaction" wire:poll.3000ms>
    <thead>
    <tr>
        <th>زمان ها</th>
        <th>قیمت ({{ strtoupper($markets['des']) }})</th>
        <th>مقدار</th>
    </tr>
    </thead>

    <tbody>
    @foreach($result as $item)
    <tr>
        <td>{{ Carbon\Carbon::parse($item['date'])->format('H:i:s') }}</td>
        <td class="{{ $item['color'] }}">{{ $item['price'] }}</td>
        <td>{{ number_format($item['amount'],4,'.','') }}</td>
    </tr>
    @endforeach
    </tbody>
</table>
