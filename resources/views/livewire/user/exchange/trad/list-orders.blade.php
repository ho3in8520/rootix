<div class="col-md-9">
    <div class="market-history market-order mt15">
        <ul class="nav nav-pills" role="tablist">
            <li class="nav-item">
                <a
                    class="nav-link active"
                    data-toggle="pill"
                    href="#open-orders"
                    role="tab"
                    aria-selected="true"
                >سفارشات باز</a
                >
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    data-toggle="pill"
                    href="#stop-orders"
                    role="tab"
                    aria-selected="false"
                >سفارشات انجام شده</a
                >
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    data-toggle="pill"
                    href="#order-history"
                    role="tab"
                    aria-selected="false"
                >تاریخچه سغارشات</a
                >
            </li>
            <li class="nav-item">
                <a
                    class="nav-link"
                    data-toggle="pill"
                    href="#trade-history"
                    role="tab"
                    aria-selected="false"
                >Balance</a
                >
            </li>
        </ul>
        <div class="tab-content">
            <div
                class="tab-pane fade show active"
                id="open-orders"
                role="tabpanel"
            >
                <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>زمان سفارش</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سقارش</li>
                    <li>مفدار</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>  <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>زمان سفارش</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سقارش</li>
                    <li>مفدار</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>
                <span class="no-data">
                        <i class="icon ion-md-document"></i>
                        No data
                      </span>
            </div>
            <div class="tab-pane fade" id="stop-orders" role="tabpanel">
                <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>زمان</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سفارش</li>
                    <li>نقدار</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>
                <span class="no-data">
                        <i class="icon ion-md-document"></i>
                        No data
                      </span>
            </div>
            <div
                class="tab-pane fade"
                id="order-history"
                role="tabpanel"
            >
                <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>زمان سقارش</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سقارش</li>
                    <li>حجم</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>
                <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>5445455</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سقارش</li>
                    <li>حجم</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>
                <span class="no-data">
                        <i class="icon ion-md-document"></i>
                        No data
                      </span>
            </div>
            <div
                class="tab-pane fade"
                id="trade-history"
                role="tabpanel"
            >
                <ul
                    class="d-flex justify-content-between market-order-item"
                >
                    <li>زمان سفارش</li>
                    <li>All pairs</li>
                    <li>All Types</li>
                    <li>خرید/فروش</li>
                    <li>قیمت سقارش</li>
                    <li>مفدار</li>
                    <li>انجام شده</li>
                    <li>انجام نشده</li>
                </ul>
                <span class="no-data">
                        <i class="icon ion-md-document"></i>
                        No data
                      </span>
            </div>
        </div>
    </div>

</div>
