@extends('templates.user.master_page')
@section('title_browser')
    {{__('titlePage.title-exchange')}}
@endsection

@section('content')
    <div class="settings mtb15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 mx-auto col-12 row px-0">
                    <div class="row col-12 bg-dark-2 text-white"
                         style="min-height: 350px; margin-bottom: -100px; padding-bottom: 140px; margin-left: 0; margin-right: 0; border-radius: 3px;">
                        <div class="col-12 d-flex flex-row align-items-end">
                            <span class="btn btn-warning ml-auto text-white">معامله حرفه ای</span>
                        </div>
                        <div class="col-sm-6 row align-items-end">
                            <div class="m-auto">
                                <img src="{{ asset('theme/landing/images/tether.svg') }}" style="width: 50px;">
                            </div>
                            <div class="col row  m-auto">
                                <h4 class="col-12 text-white"><span class="text-secondary">USDT</span> Tether</h4>
                                <h4 class="col-12 text-white">تتر</h4>
                            </div>
                        </div>
                        <div class="col-sm-6 d-flex flex-column justify-content-end align-items-end">
                            <div class="h4 text-white" style="direction: ltr" dir="ltr"><span
                                    class="text-secondary">$</span><span> 1</span></div>
                            <div class="d-flex flex-row text-white" dir="ltr" style="direction: ltr">
                                <h4 class="text-secondary">ریال</h4>
                                <h4 class="px-3 text-white">{{$usdt_amount->usdtToRil}}</h4>
                                <h4 class="badge badge-pill badge-info btn" style="font-size: 18px; padding: 8px 20px;">0.82
                                    %</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-8 text-dark ">
                        <div class="card">
                            <div class="card-body">
                                <h4>اطلاعات بازار جهانی</h4>
                                <div class="row mt-sm-4">
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم بازار</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M</p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم معاملات 24h</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M
                                        </p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">بالاترین قیمت تا کنکون</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 1.5 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h4>اطلاعات بازار جهانی</h4>
                                <div class="row mt-sm-4">
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم بازار</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M</p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم معاملات 24h</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M
                                        </p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">بالاترین قیمت تا کنکون</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 1.5 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h4>اطلاعات بازار جهانی</h4>
                                <div class="row mt-sm-4">
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم بازار</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M</p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">حجم معاملات 24h</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 628M
                                        </p>
                                    </div>
                                    <div class="col-sm-4 py-2">
                                        <p class="text-secondary d-inline d-md-block">بالاترین قیمت تا کنکون</p>
                                        <p class="h5 d-inline d-md-block float-left float-md-none" style="direction: ltr"><span class="text-secondary">$ </span> 1.5 </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 card ">
                        <div class="card-body">
                            <div class="d-flex flex-row buy-sell-btns row mb-4">
                                <div class="col-6 bg-info py-2 text-white text-center btn buy">خرید</div>
                                <div class="col-6 bg-secondary py-2 text-center text-white btn sell disabled">فروش</div>
                            </div>
                            <form class="buy-section">
                                <div class="form-group row">
                                    <label class="col-12">من می‌خواهم</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button type="button" class="btn btn-primary total-amount">کل موجودی</button>
                                        </div>
                                        <input id="rial" type="text" onkeyup="($(this).val(numberFormat($(this).val())),getPrice())"   name="rial" class="form-control " placeholder="مبلغ خود را به ریال وارد کنید">
                                    </div>
                                    <span class="float-right "> مقدار موجودی  {{ number_format($rial_asset->amount) }} ریال</span>
                                </div>
                                <div class="form-group row">


                                </div>
                                <div class="form-group row">
                                    <label class="col-12">بپردازم تا معادل</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">تتر</span>
                                        </div>
                                        <input  type="text" id="amount" name="amount" class="form-control" placeholder="تعداد تتر های دریافتی" readonly>
                                    </div>
                                    <span  class="float-right usdt_balance col-12 " data-label="usdt">0</span>

                                </div>
                                <button type="button" id="my_button" data-unit="{{$unit}}"
                                        data-route-withdraw="{{ route('check-withdraw-amount') }}"
                                        data-address-wallet="{{$token['address']['base58']}}"
                                        data-csrf="{{ csrf_token()}}"
                                        data-fee-swap="{{$fee_swap_usdt->extra_field2}}"
                                        data-min-withdraw-swap="{{$min_swap_withdraw->extra_field2}}"
                                        data-route-dashboard="{{route('user.dashboard')}}"
                                        data-route-send-message="{{ route('send-message-admin') }}" data-route-withdraw-from="{{ route('withdraw-from-wallet') }}"  class="form-group btn btn-info col-12 py-2" >همین الان خرید میکنم
                                </button>
                            </form>
                            <form class="sell-section" style="display: none">
                                <div class="form-group row">
                                    <label class="col-12">من می‌خواهم</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">تتر</span>
                                        </div>
                                        <input type="text" class="form-control py-4 price-input"
                                               data-type="tether" aria-label="تتر">
                                    </div>
                                </div>
                                <div class="form-group row">
                                <span class="btn btn-warning btn-sm mx-auto" onclick="convert_price('sell')">
                                    تبدیل
                                </span>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12">بفروشم تا معادل</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">تومان</span>
                                        </div>
                                        <input type="text" class="form-control py-4 toman-price" readonly
                                               aria-label="قیمت تومان">
                                    </div>
                                </div>
                                <button class="form-group btn btn-danger col-12 py-2" type="submit">همین الان می فروشم</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="module" src="{{asset('theme/user/assets/js/withdrawController.js')}}" ></script>
    <script>
        $(".buy-sell-btns .btn").click(function () {
            if ($(this).hasClass('bg-info') || $(this).hasClass('sell'))
                return;
            $(".buy-section,.sell-section").toggle();
            $(".buy-sell-btns .btn").removeClass('bg-info').addClass('bg-secondary');
            $(this).addClass('bg-info');
        });

        function convert_price(type) {
            if (type == 'buy') {
                let val = $(".buy-section .price-input").val();
                $(".buy-section .tether-price").val(val * 0.236);
            }
            else {
                let val = $(".sell-section .price-input").val();
                $(".sell-section .toman-price").val(val * 236);
            }
        }

        $(document).ready(async function () {
            var usdt = document.getElementsByClassName('usdt_balance');

            if(usdt && "{{$token['address']['base58']}}")
            {

                var contract = await tronWeb.contract()
                    .at("TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t");

                var result = await contract.balanceOf("{{$token['address']['base58']}}").call();
                console.log(result / 1000000);
                $('.usdt_balance').html('مقدار موجودی'+" {{ $unit }} " +'شما برابر است با : ' + result / 1000000) ;
            }
        });

        function separate(Number)
        {
            Number+= '';
            Number= Number.replace(',', '');
            x = Number.split('.');
            y = x[0];
            z= x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(y))
                y= y.replace(rgx, '$1' + ',' + '$2');
            return y+ z;
        }
        function getPrice(){
            var dollar = '';

            var rial = $('#rial').val() ;
            var rial = rial.replace(/,/g,'');
            var rial = rial - '{{$fee_swap_usdt->extra_field2}}';
            $('.main_amount').css('display','block');
            $('.main_amount').html('مقدار نهایی دارایی شما پس از کم شدن کارمزد برابر است با : '+ numberFormat(rial) + 'ریال');

            var amount = rial / "{{$usdt_amount->usdtToRil}}";
            var amount2 = amount.toLocaleString('en-US', { maximumFractionDigits: 2, minimumFractionDigits: 2 });

            // console.log(amount2);
            $('#amount').val(amount2);

        }

        $(document).ready(function () {
            $(document).on('click','.total-amount',function () {
                $(this).closest('.input-group').find("input").val(`{{ $rial_asset->amount }}`)
            })
        })

    </script>

@endsection

