@if ($paginator->hasPages())
    <div class="pagination mt-5 mb-5">
        @if ($paginator->onFirstPage())
            <a style="pointer-events: none" class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">❯</a>
        @else
            <a class="" href="{{ $paginator->previousPageUrl() }}">❯</a>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <a href="" class="disabled" aria-disabled="true">{{$element}}</a>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="page-item active" aria-current="page">{{ $page }}</a>
                    @else
                        <a href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" rel="next"
               aria-label="@lang('pagination.next')">❮</a>
        @else
            <a style="pointer-events: none" class="disabled" rel="next"
               aria-label="@lang('pagination.next')">❮</a>
        @endif
        <p class="choices mt-2 ml-4">صفحه {{$paginator->currentPage()}} از {{$paginator->lastPage()}}</p>
    </div>

@endif
