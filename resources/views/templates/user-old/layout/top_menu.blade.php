<!-- BEGIN: Header-->
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed navbar-shadow navbar-brand-center">
    <div class="navbar-header d-xl-block d-none">
        <ul class="nav navbar-nav flex-row">
            {{--<li class="nav-item"><a class="navbar-brand" href="../../../html/rtl/horizontal-menu-template/index.html">
                    <div class="brand-logo"></div>
                </a></li>--}}
        </ul>
    </div>
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                    class="ficon feather icon-menu"></i></a></li>
                    </ul>
                    {{--<ul class="nav navbar-nav bookmark-icons">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html"
                                                                  data-toggle="tooltip" data-placement="top"
                                                                  title="{{__('attributes.userMenu.listWork')}}"><i
                                    class="ficon feather icon-check-square"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html"
                                                                  data-toggle="tooltip" data-placement="top"
                                                                  title="{{__('attributes.userMenu.conversation')}}"><i
                                    class="ficon feather icon-message-square"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html"
                                                                  data-toggle="tooltip" data-placement="top"
                                                                  title="{{__('attributes.email')}}"><i class="ficon feather icon-mail"></i></a>
                        </li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calender.html"
                                                                  data-toggle="tooltip" data-placement="top"
                                                                  title="{{__('attributes.userMenu.calendar')}}"><i
                                    class="ficon feather icon-calendar"></i></a></li>
                    </ul>--}}
                    {{--<ul class="nav navbar-nav">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link bookmark-star"><i
                                    class="ficon feather icon-star warning"></i></a>
                            <div class="bookmark-input search-input">
                                <div class="bookmark-input-icon"><i class="feather icon-search primary"></i></div>
                                <input class="form-control input" type="text" placeholder="{{__('attributes.userMenu.search')}} Vuexy ..."
                                       tabindex="0" data-search="template-list">
                                <ul class="search-list search-list-bookmark"></ul>
                            </div>
                        </li>
                    </ul>--}}
                </div>
                <ul class="nav navbar-nav float-right">
                    {{--<li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link"
                                                                       id="dropdown-flag" href="#"
                                                                       data-toggle="dropdown" aria-haspopup="true"
                                                                       aria-expanded="false"><i
                                class="flag-icon flag-icon-ir"></i><span class="selected-language">{{__('attributes.userMenu.farsi')}}</span></a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-flag"><a class="dropdown-item"
                                                                                      href="../../ltr/horizontal-menu-template"
                                                                                      data-language="en"><i
                                    class="flag-icon flag-icon-us"></i> {{__('attributes.userMenu.english')}}</a><a class="dropdown-item" href="#"
                                                                                                                    data-language="fa"><i
                                    class="flag-icon flag-icon-ir"></i>{{__('attributes.userMenu.farsi')}}</a><a class="dropdown-item" href="#"
                                                                                                                 data-language="de"><i
                                    class="flag-icon flag-icon-de"></i>آلمان</a><a class="dropdown-item" href="#"
                                                                                   data-language="pt"><i
                                    class="flag-icon flag-icon-pt"></i>پرتغال</a></div>
                    </li>--}}
                    <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i
                                class="ficon feather icon-maximize"></i></a></li>
                    <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i
                                class="ficon feather icon-search"></i></a>
                        <div class="search-input">
                            <div class="search-input-icon"><i class="feather icon-search primary"></i></div>
                            <input class="input" type="text"
                                   placeholder="{{__('attributes.userMenu.search')}} Vuexy ..." tabindex="-1"
                                   data-search="template-list">
                            <div class="search-input-close"><i class="feather icon-x"></i></div>
                            <ul class="search-list search-list-main"></ul>
                        </div>
                    </li>
                    <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#"
                                                                           data-toggle="dropdown"><i
                                class="ficon feather icon-bell"></i><span
                                class="badge badge-pill badge-primary badge-up">{{ countNotificationsNotView() }}</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header m-0 p-2">
                                    <h3 class="white">{{ countNotificationsNotView() }} اعلان </h3><span
                                        class="notification-title">جدید برنامه</span>
                                </div>
                            </li>
                            <li class="scrollable-container media-list">
                                @if(countNotificationsNotView() >= 1)
                                @foreach(getNotificationsNotView() as $item)
                                    <a class="d-flex justify-content-between"
                                       href="{{ route('notification.show',base64_encode($item->id)) }}">
                                        <div class="media d-flex align-items-start">
                                            <div class="media-left"><i
                                                    class="{{ $item->icon }} {{ $item->color }}"
                                                    style="font-size: 20px"></i></div>
                                            <div class="media-body">
                                                <h6 class="{{ $item->color }} media-heading">
                                                    {{ $item->title }}
                                                </h6>
                                                <small
                                                    class="notification-text">{!! \Illuminate\Support\Str::limit($item->description,30) !!}
                                                </small>
                                            </div>
                                            <small>
                                                <time class="media-meta"
                                                      datetime="2015-06-11T18:29:20+08:00">{{ jdate_from_gregorian($item->created_at,'Y/m/d') }}
                                                </time>
                                            </small>
                                        </div>
                                    </a>
                                @endforeach
                                @else
                                <div class="d-flex justify-content-between">
                                    <div class="media d-flex align-items-start">
                                        <div class="media-body">
                                            <h6 class="media-heading text-center primary">
                                                موردی یافت نشد!
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                    @endif
                            </li>
                            <li class="dropdown-menu-footer"><a class="dropdown-item p-1 text-center"
                                                                href="{{ route('notification.index') }}">دیدن تمام اعلان ها</a></li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none"><span
                                    class="user-name text-bold-600">{{ auth()->user()->full_name }}</span><span
                                    class="user-status">{{__('attributes.userMenu.access')}}</span></div>
                            <span><img class="round"
                                       src="{{ auth()->user()->avatar }}"
                                       alt="avatar" height="40" width="40"></span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('user.profile.form') }}"><i
                                    class="feather icon-user"></i>{{__('attributes.userMenu.profile')}}</a>
                            <a class="dropdown-item" href="{{ route('change_pass.index') }}"><i
                                    class="feather icon-user"></i>{{__('attributes.userMenu.changePass')}}</a>


                            <a class="dropdown-item" href="{{route('setting.form')}}"><i
                                    class="feather icon-settings"></i> تنظیمات </a>
                            {{--<a class="dropdown-item" href="app-email.html"><i class="feather icon-mail"></i> صندوق ورودی من</a>
                            <a class="dropdown-item" href="app-todo.html"><i class="feather icon-check-square"></i> برنامه ها</a>
                            <a class="dropdown-item"  href="app-chat.html"><i class="feather icon-message-square"></i> گفتگوها</a>--}}
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"><i
                                    class="feather icon-power"></i> {{__('attributes.userMenu.logOut')}}</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<ul class="main-search-list-defaultlist d-none">
    <li class="d-flex align-items-center"><a class="pb-25" href="#">
            <h6 class="text-primary mb-0">{{__('attributes.userMenu.file')}}</h6></a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="mr-50"><img src="{{ asset('theme/user/app-assets/images/icons/xls.png') }}" alt="png"
                                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">دو مورد جدید ثبت شد</p><small
                        class="text-muted">{{__('attributes.userMenu.manege')}}</small>
                </div>
            </div>
            <small class="search-data-size mr-50 text-muted">&apos;17kb</small></a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="mr-50"><img src="{{ asset('theme/user/app-assets/images/icons/jpg.png') }}" alt="png"
                                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">52 فایل عکس تهیه شده است</p><small class="text-muted">توسعه دهنده
                        FrontEnd</small>
                </div>
            </div>
            <small class="search-data-size mr-50 text-muted">&apos;11kb</small></a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="mr-50"><img src="{{ asset('theme/user/app-assets/images/icons/pdf.png') }}" alt="png"
                                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">25 فایل PDF بارگذاری شده است</p><small class="text-muted">مدیر
                        دیجیتال مارکتینگ</small>
                </div>
            </div>
            <small class="search-data-size mr-50 text-muted">&apos;150kb</small></a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between w-100" href="#">
            <div class="d-flex">
                <div class="mr-50"><img src="{{ asset('theme/user/app-assets/images/icons/doc.png') }}" alt="png"
                                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">Anna_Strong</p><small class="text-muted">طراح وب سایت</small>
                </div>
            </div>
            <small class="search-data-size mr-50 text-muted">&apos;256kb</small></a></li>
    <li class="d-flex align-items-center"><a class="pb-25" href="#">
            <h6 class="text-primary mb-0">اعضا</h6></a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img
                        src="{{ asset('theme/user/app-assets/images/portrait/small/avatar-s-8.jpg') }}" alt="png"
                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">جواد محمدی</p><small class="text-muted">طراح رابط کاربری</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img
                        src="{{ asset('theme/user/app-assets/images/portrait/small/avatar-s-1.jpg') }}" alt="png"
                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">مسعود اصغرزاده</p><small class="text-muted">توسعه دهنده
                        FrontEnd</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img
                        src="{{ asset('theme/user/app-assets/images/portrait/small/avatar-s-14.jpg') }}" alt="png"
                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">محمد نوریان</p><small class="text-muted">مدیر دیجیتال
                        مارکتینگ</small>
                </div>
            </div>
        </a></li>
    <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a
            class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
            <div class="d-flex align-items-center">
                <div class="avatar mr-50"><img
                        src="{{ asset('theme/user/app-assets/images/portrait/small/avatar-s-6.jpg') }}" alt="png"
                        height="32"></div>
                <div class="search-data">
                    <p class="search-data-title mb-0">هانیه برخوردار</p><small class="text-muted">طراح وب سایت</small>
                </div>
            </div>
        </a></li>
</ul>
<ul class="main-search-list-defaultlist-other-list d-none">
    <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer"><a
            class="d-flex align-items-center justify-content-between w-100 py-50">
            <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>نتیجه ای پیدا نشد</span>
            </div>
        </a></li>
</ul>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="horizontal-menu-wrapper">
    <div
        class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-without-dd-arrow navbar-shadow menu-border"
        role="navigation" data-menu="menu-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                {{--                --}}{{--<li class="nav-item mr-auto"><a class="navbar-brand"--}}
                {{--                                                href="../../../html/rtl/horizontal-menu-template/index.html">--}}
                {{--                        <div class="brand-logo"></div>--}}
                {{--                        <h2 class="brand-text mb-0">Vuexy</h2></a></li>--}}
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                            class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                            class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                            data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <!-- Horizontal menu content-->
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <!-- include ../../../includes/mixins-->
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                {{--                <li class="nav-item">--}}
                {{--                    <a class="nav-link" href="/">{{__('attributes.userMenu.minePage')}}</a>--}}
                {{--                </li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('user.dashboard') }}">{{__('attributes.userMenu.dashboard')}}</a>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link"
                       href="{{ route('swap.show',['unit'=>'usdt']) }}">{{__('attributes.userMenu.exchange')}}</a>
                </li>--}}
                {{--                <li class="nav-item">--}}
                {{--                    <a class="nav-link" href="{{ route('charge_wallet.view') }}">{{__('attributes.userMenu.payRial')}}</a>--}}
                {{--                </li>--}}
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('sell_currency.view') }}">{{__('attributes.userMenu.sellCurrency')}}</a>
                </li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('banks.index') }}">{{__('attributes.userMenu.bank')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('wallet.index') }}">{{__('attributes.userMenu.myWallet')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('setting.form') }}">تنظیمات</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('ticket.index') }}"> {{__('attributes.ticket.tickets')}} <span
                            class="count badge badge-danger">{{$number_ticket_user}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('transactions.index') }}">لیست تراکنش ها</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('withdraw-rial.index') }}">درخواست‌های برداشت ریالی</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logged-in-devices.list') }}">Login Activity</a>
                </li>

            </ul>
        </div>
    </div>
</div>
<!-- END: Main Menu-->
