<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta
        name="google-signin-client_id"
        content="894253294344-lre3h5mtr8cpcp54rl1ibm5a7v1rl3fe.apps.googleusercontent.com"
    />
    <title>@yield('title_browser')</title>
    <link rel="icon" type="image/x-icon" href="{{asset('theme/landing/images/logo.png')}}">
    @include('templates.landing_page.layout.header')
    {!! htmlScriptTagJsApi() !!}
    <style>
        .register-google-btn {
            border-radius: 30px !important;
            height: 44px !important;
            box-shadow: none !important;
            direction: rtl !important;
            text-align: center;
            width: 48%;
            border: 2px solid #c5c3c3;
            background-color: white;
            color: black;
            display: flex;
            align-items: center;
        }

        .register-google-btn span {
            font-size: 16px;
        }
    </style>
@yield('style')
{{--    <script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o+"?href="+window.location.href;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="ab96fde8-18ba-4828-a1ed-8189027d5fcf";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>--}}
<!---start GOFTINO code--->
    <script type="text/javascript">
        !function () {
            var i = "tk2ftK", a = window, d = document;

            function g() {
                var g = d.createElement("script"), s = "https://www.goftino.com/widget/" + i,
                    l = localStorage.getItem("goftino_" + i);
                g.async = !0, g.src = l ? s + "?o=" + l : s;
                d.getElementsByTagName("head")[0].appendChild(g);
            }

            "complete" === d.readyState ? g() : a.attachEvent ? a.attachEvent("onload", g) : a.addEventListener("load", g, !1);
        }();
    </script>
    <!---end GOFTINO code--->
</head>

<body class="main-form-body">
<div class="loader">
    <img src="{{asset('theme/landing/images/loader.svg')}}">
</div>
@include('general.flash_message_desktop')
<main class="main-register">
    <section class="register-info register login-info">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="register-form-container">
                        <div class="register-form__title">
                            <h1>خوش آمدید</h1>
                            <p>برای ورود به پنل اطلاعات خود را وارد کنید</p>
                        </div>

                        <div class="register-mark-btns">
                            {{--                            <a href="{{route('login')}}"--}}
                            {{--                                class=" register-mark-btn register-google-btn">--}}
                            {{--                                <div class="facebook-container">--}}
                            {{--                                    <i class="fa fa-google"></i>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="text-center w-100">--}}
                            {{--                                    <span>ورود با گوگل</span>--}}
                            {{--                                </div>--}}
                            {{--                            </a>--}}
                            <a href="{{route('login')}}" class="register-mark-btn register-google-btn">
                                <div class="facebook-container">
                                    <img width="28px" height="28px" style="margin-right: 8px;margin-top: 4px"
                                         src="{{asset('theme/landing/images/google.png')}}" alt="">
                                </div>
                                <div class="text-center w-100 sign-in-with-facebook">
                                    <span>ورود با گوگل</span>
                                </div>
                            </a>
                            <a class="register-mark-btn register-facebook-btn">
                                <div class="facebook-container">
                                    <i class="fa fa-facebook-f"></i>
                                </div>
                                <div class="text-center w-100 sign-in-with-facebook">
                                    <span>ورود با فیسبوک</span>
                                </div>
                            </a>
                        </div>
                        @yield('form')
                        <h5 class="development-rootix development-rootix-login">توسعه یافته در روتیکس</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="register-img register">
        <div class="main-register-img">
            <div class="register-logo">
                <a href="{{route('landing.home')}}">
                    <img
                        class="register-logo"
                        src="{{asset('theme/landing/images/logo.png')}}"
                        alt="روتیکس لوگو"
                    />
                </a>

            </div>
            <div class="rootix-register-image-info-container">
                <div class="register-img__container">
                    <img
                        src="{{asset('theme/landing/images/register/Group 1497.png')}}"
                        alt="ثبت نام در روتیکس"
                    />
                </div>
            </div>

            <div class="register-img__bottom">
                <hr class="register-hr"/>
                <div>
                    <h3>
                        به راحتی در پنل روتیکس ثبت نام کنید و خرید و فروش در دنیای
                        ارز دیجیتال
                    </h3>
                    <h3>در یکی از بهترین صرافی ها را تجربه کنید</h3>
                </div>
            </div>
        </div>
    </section>
</main>
<script src="{{asset('theme/landing/scripts/Jquery.min.js')}}"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="{{asset('theme/landing/scripts/app.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.loader').hide();
        $("body").css({"overflow": "visible"});
    });
</script>
@yield('script')
</body>
</html>
