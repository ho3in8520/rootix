{{--<header class="header about-header">--}}
{{--    <div class="container">--}}
{{--        <nav class="nav">--}}
            <div>
                <a href="{{route('landing.home')}}" class="logo">
                    <img src="{{asset('theme/landing/images/logo.png')}}" alt="لوگو"/>
                </a>
                <ul>
                    <li><a href="{{route('landing.home')}}"> خانه </a></li>
                    <li><a href="{{route('landing.about')}}"> درباره ما </a></li>
                    <li><a href="{{route('landing.services')}}"> خدمات </a></li>
                    <li><a href="{{route('landing.prices')}}"> قیمت ها </a></li>
                    <li><a href="{{route('landing.faq')}}">سوالات متداول</a></li>
                    <li><a href="https://blog.rootix.io"> آموزش </a></li>
                    <li><a href="https://blog.rootix.io"> مجله خبری </a></li>
                    <li><a href="{{route('landing.contact_us')}}"> تماس با ما </a></li>
                </ul>
            </div>

            <div class="login-container">
                <a href="#" class="language">
{{--                    <i class="fas fa-angle-down"></i>--}}
                    <p>Fa</p>
                    <div>
                        <img src="{{asset('theme/landing/images/iran_flag.png')}}" alt="پرچم"/>
                    </div>
                </a>
                <a href="{{route('login.form')}}" class="login-btn">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="9"
                        viewBox="0 0 18 9"
                    >
                        <image
                            id="icons8-long_arrow_up_filled"
                            width="18"
                            height="9"
                            xlink:href="{{asset('theme/landing/images/login-right-arrow.png')}}"
                        />
                    </svg>
                    ورود
                </a>
            </div>
{{--        </nav>--}}
{{--        @yield('header')--}}
{{--    </div>--}}
{{--</header>--}}
