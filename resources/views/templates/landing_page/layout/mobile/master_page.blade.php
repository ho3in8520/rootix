<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>@yield('title_browser')</title>
    <link rel="icon" type="image/x-icon" href="{{asset('theme/landing/images/logo.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="روتیکس بازار امن خرید و فروش بیت کوین،اتریوم و ارزهای دیجیتال، با تسهیل ترید و دارای ویژگی های استاندارد صرافی های ارز دیجیتال برتر دنیا در محیطی حرفه ای مفتخر به خدمت رسانی شما عزیزان می باشد.">
@include('templates.landing_page.layout.header')
    @yield('style')
    {{--<script type="text/javascript">
        CRISP_RUNTIME_CONFIG = {
            locale: "{{ \Illuminate\Support\Facades\App::currentLocale() }}"
        };
        window.$crisp = [];
        window.CRISP_WEBSITE_ID = "0d388766-bc01-4ba9-bb6b-b79f274e0180";
        (function () {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.chat/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();
    </script>--}}
{{--    <script type="text/javascript">!function(){function t(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,localStorage.getItem("rayToken")?t.src="https://app.raychat.io/scripts/js/"+o+"?rid="+localStorage.getItem("rayToken")+"&href="+window.location.href:t.src="https://app.raychat.io/scripts/js/"+o+"?href="+window.location.href;var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=document,a=window,o="ab96fde8-18ba-4828-a1ed-8189027d5fcf";"complete"==e.readyState?t():a.attachEvent?a.attachEvent("onload",t):a.addEventListener("load",t,!1)}();</script>--}}

    <!---start GOFTINO code--->
<!--    <script type="text/javascript">
        !function(){var i="tk2ftK",a=window,d=document;function g(){var g=d.createElement("script"),s="https://www.goftino.com/widget/"+i,l=localStorage.getItem("goftino_"+i);g.async=!0,g.src=l?s+"?o="+l:s;d.getElementsByTagName("head")[0].appendChild(g);}"complete"===d.readyState?g():a.attachEvent?a.attachEvent("onload",g):a.addEventListener("load",g,!1);}();
    </script>-->
    <!---end GOFTINO code--->

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="96a851e6-997a-47ea-a987-108ce9047ba8";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>

</head>
<body>
<div class="loader">
    <img src="{{asset('theme/landing/images/loader.svg')}}">
</div>
@include('templates.landing_page.layout.mobile.top_menu')

<main class="@yield('main_class')">
    @yield('content')
</main>
<!-- Footer Starts -->
<footer>
    <div class="container-fluid">

        <div class="footer-logo">

            <h2>
                R
            </h2>
            <h2>
                OOTIX</h2>
        </div>

        <div class="row footer-row">
            <div class="col-6 col-lg-3">
                <div class="footer-contact footer-item">
                    <h3 class="footer-title">روتیکس</h3>
                    <div>
                        <h5>rootix@company.com</h5>
                        <h5>648-145-8360</h5>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-sign-in footer-item">
                    <h3 class="footer-title">عضو شوید</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="{{route('register.form')}}"> ثبت نام </a>
                        </li>
                        <li>
                            <a href="{{route('login.form')}}"> ورود </a>
                        </li>
                        <li>
                            <a href="{{route('landing.faq')}}"> سوالات متداول </a>
                        </li>

                        <li>
                            <a href="{{route('landing.contact_us')}}"> تماس با ما </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-about-us footer-item">
                    <h3 class="footer-title">درباره ما</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="{{route('landing.about')}}"> درباره روتیکس </a>
                        </li>
                        <li>
                            <a href="#"> تیم های ما </a>
                        </li>
                        <li>
                            <a href="#"> فرصت های شغلی </a>
                        </li>
                        <li>
                            <a href="#"> نظرات کاربران </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-6 col-lg-3">
                <div class="footer-quick-access footer-item">
                    <h3 class="footer-title">دسترسی سریع</h3>
                    <ul class="footer-menu">
                        <li>
                            <a href="#"> روش کار روتیکس </a>
                        </li>
                        <li class="footer-rootix-menu-item">
                            <a href="#"> ویژگی های روتیکس </a>
                        </li>
                        <li>
                            <a href="{{route('landing.blog')}}"> بلاگ روتیکس </a>
                        </li>
                        <li>
                            <a href="{{route('landing.prices')}}"> قیمت ها </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr />

        <div class="footer-bottom">
            <div class="social-networks">
                <a href="https://www.instagram.com/rootix.io/" class="social-network instagram">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="18"
                        viewBox="0 0 18 18"
                    >
                        <image
                            id="instagram_copy"
                            data-name="instagram copy"
                            width="18"
                            height="18"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAABHNCSVQICAgIfAhkiAAAAaNJREFUOE+dlD0ohVEYx12fyVcYFDZiM2Bj8rExKZOPKHV9LAYp3A9yFxkpJEooMZFFschoMEgRE2IgRWLQ9fvXOfW6vfd6eerX85xznvffeZ/znOOLRqNJxlrxo1BnJ37xZ6zPwrryfEZohHgGvmAXniBZ6y5iysmDJsiHeRiSUAPBISxDn8fd2LQQQRj8EjonKIcMjyLaaRVcwCdcQ4mEVKRtaI8jlM58Kryb9WL8HXTABkxBwAotMOiPESoyiY1mXjvohFOoNztSLccgYoVWGfQ6hHKIbyEX5uADhiHFiJw4csPEoXhCByw2QxncmI/S8C8mzvIqpLqtQXfM7/YwXgEdjoosS7gjCbm1gw5kCyrh0ovQsalFKV4nZO2BoADUKvZKJNyROvbZfB3Bv8EEqDY6xSOH+A+hRRb8jkWFFbAJNWb+Ht8FugVOG2cwbU9NjaUGc7NCJtWQj3HWg8xPSkg1yAT9+39MDVotoTaCHalC+I9KuuRLELTPiJ6CAbiCPVDjqYvdTM9INujVqIV9aLFC+mAQAqA75sVeSdIbNq3kb1Ymla7Wdx0PAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
                <a href="https://twitter.com/rootixexchange" class="social-network twitter">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink"
                        width="18"
                        height="14"
                        viewBox="0 0 18 14"
                    >
                        <image
                            id="twitter_copy"
                            data-name="twitter copy"
                            width="18"
                            height="14"
                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAOCAYAAAAi2ky3AAAABHNCSVQICAgIfAhkiAAAAUBJREFUKFNj/P//PxMDBPyD0rioEKBEHRDLAfFTIG4F4rVAHATEDxiBBmkCGauBOAqIL+EwJRoovgSL3HOg2B8gTgUZZAdkHATiv0AcAcRr0DQIAPmvgJgVhyUguWyQQbJAxiMkRRuA7HYgPgUVEwTS7/B4ewJQrgRkEEhNBhBPBmIWJA0gb54EYlDYpQIxLCzRzQQFzQ2QQcYgpwGxNxCL4bEZlxRI/zmQQTJAxmMyDABp+QjEUkD8Dea1SCBnGRmGLQDqSQTpgxkEYncBcSwQS5BgoANQLSjGUQwCJbAqEgw5DVRrBlOP7CJhoOA2ZEkChoKSzRNsBsHEHIGM+UAsj8cgZ6DcPmR5kIskgQKgaGcHYnsgDgBiKxyGgBJmGBDvRZeH5bUyoEQoEHNDFXwF0siZ+BOQvwiIe4AYayoHAAP+W7zpyrnuAAAAAElFTkSuQmCC"
                        />
                    </svg>
                </a>
{{--                <a href="#" class="social-network face-book">--}}
{{--                    <svg--}}
{{--                        xmlns="http://www.w3.org/2000/svg"--}}
{{--                        xmlns:xlink="http://www.w3.org/1999/xlink"--}}
{{--                        width="8"--}}
{{--                        height="18"--}}
{{--                        viewBox="0 0 8 18"--}}
{{--                    >--}}
{{--                        <image--}}
{{--                            id="facebook-logo_copy"--}}
{{--                            data-name="facebook-logo copy"--}}
{{--                            width="8"--}}
{{--                            height="18"--}}
{{--                            xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAASCAYAAABmQp92AAAABHNCSVQICAgIfAhkiAAAALFJREFUKFNj/P//PwMSSAeyg4HYEIjZgfgLI5KCEqBAN7JqEBtZwTsgXxBNwS+YAkWgxD0kyfNAdgqyFUpAzl0kBW1AdjXMCh4gwwCIDyMpmA9k54OcALLiIZAhAsRcSAp+A9k/gPgrSAGKP7H5AmSCMBBzYzHhM8gEkBv0gPgokoIFQHYecjjIAzkPkBRMRlegDxS4gKRgNpCdhmzCYFBgDHTQGSRHLgeyo5AdqYsrHACDnVzHH+anLgAAAABJRU5ErkJggg=="--}}
{{--                        />--}}
{{--                    </svg>--}}
{{--                </a>--}}
                <a href="https://t.me/rootix" class="social-network telegram">
                    <img src="{{asset('theme/landing/images/telegram_mobile
.png')}}" width="55px"  height="55px" alt="">
                </a>
            </div>

            <h5>© 2018 Rootix All Right Reserved</h5>
            <h5 class="inf">info@rootix.io</h5>
{{--            <h5 class="inf">648-145-8360</h5>--}}
        </div>
    </div>
</footer>
<!-- Footer Ends -->
@include("templates.landing_page.layout.footer")
<script>
    $(document).ready(function (){
        $('.loader').hide();
        $("body").css({"overflow-y":"visible"});
    });
</script>
@yield('script')
</body>
</html>
