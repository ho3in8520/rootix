<footer class="page-footer">
    <div class="footer-content">
        <div class="container-fluid">

        </div>
    </div>
</footer>

<script src="{{asset('theme/admin/js/vendor/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('theme/admin/js/persian-date.min.js')}}"></script>
<script src="{{asset('theme/admin/js/persian-datepicker.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/Chart.bundle.min.js')}}"></script>
<script src="{{asset('theme/admin/js/cleave.min.js')}}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{asset('theme/admin/js/vendor/chartjs-plugin-datalabels.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/moment.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/fullcalendar.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/datatables.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/progressbar.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/jquery.barrating.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/select2.full.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/nouislider.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/Sortable.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/mousetrap.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/glide.min.js')}}"></script>
<script src="{{asset('theme/admin/js/vendor/dropzone.min.js')}}"></script>
<script src="{{asset('theme/admin/js/jquery.multi-select.js')}}"></script>
<script src="{{asset('theme/admin/js/dore.script.js')}}"></script>
<script src="{{ asset('theme/plugins/bootstrap-iconpicker/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
<script src="{{ asset('theme/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('theme/plugins/form-builder/form-builder.min.js') }}"></script>
<script src="{{ asset('theme/plugins/form-builder/form-render.min.js') }}"></script>


<script>


    var app_url = `{{ asset('/') }}`
    $('.date-picker').persianDatepicker({
        observer: true,
        format: 'YYYY/MM/DD',
        altField: '.datepicker-alt',
        initialValue: false,
    });
</script>
<script src="{{asset('theme/admin/js/scripts.js')}}"></script>
<script src="{{ asset('general/js/popup.js') }}"></script>
<script src="{{ asset('general/js/func.js') }}"></script>
<script src="{{ asset('general/js/project.js') }}"></script>
