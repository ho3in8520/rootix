<div class="sidebar-container">
    <div class="sidebar">
        <div class="sidebar__logo">
            <a href="#">
                <img src="{{asset('theme/landing/images/logo.png')}}" alt="لوگو"/>
            </a>
        </div>

        <ul class="sidebar-menu">
            @can('dashboard_menu')
                <li class="sidebar-menu__item {{request()->url() == route('user.dashboard')?'active':''}}">
                    <a href="{{route('user.dashboard')}}" class="sidebar-menu__link">
                        <div class="sidebar-menu__svg">
                            <div data-toggle="tooltip" data-placement="top" title="داشبورد">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="20"
                                    height="23"
                                    viewBox="0 0 24.597 27.107">
                                    <g transform="translate(1 1)">
                                        <path
                                            d="M4.5,11.788,15.8,3l11.3,8.788V25.6a2.511,2.511,0,0,1-2.511,2.511H7.011A2.511,2.511,0,0,1,4.5,25.6Z"
                                            transform="translate(-4.5 -3)"
                                            fill="none"
                                            stroke="#000"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="2"
                                        />
                                        <path
                                            d="M13.5,30.554V18h7.532V30.554"
                                            transform="translate(-5.968 -5.446)"
                                            fill="none"
                                            stroke="#000"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="2"
                                        />
                                    </g>
                                </svg>
                            </div>
                        </div>
                        داشبورد
                    </a>
                </li>
            @endcan

            @can('wallet_menu')
                <li class="sidebar-menu__item {{ request()->url()==route('wallet.index')?'active':'' }}">
                    <a href="{{ route('wallet.index') }}" class="sidebar-menu__link">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="کیف پول">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="20"
                                        height="19"
                                        viewBox="0 0 24.228 23.372"
                                    >
                                        <g transform="translate(-2.494 -2.737)" opacity="0.6">
                                            <path
                                                d="M22.713,11.25H6.634a3.262,3.262,0,0,0-3.259,3.259v9.126a3.262,3.262,0,0,0,3.259,3.259H22.713a3.262,3.262,0,0,0,3.259-3.259V14.509A3.262,3.262,0,0,0,22.713,11.25Z"
                                                transform="translate(0 -1.535)"
                                                fill="none"
                                                stroke="#000"
                                                stroke-width="1.5"
                                            />
                                            <path
                                                d="M19.65,4.554,6.89,7.047c-.978.217-2.39,1.2-2.39,2.4A3.423,3.423,0,0,1,7.162,8.411h15.59V7.3A3.119,3.119,0,0,0,22,5.255h0A2.569,2.569,0,0,0,19.65,4.554Z"
                                                transform="translate(-1.256 -1)"
                                                fill="none"
                                                stroke="#000"
                                                stroke-width="1.5"
                                            />
                                        </g>
                                    </svg>
                                </div>
                            </div>

                            کیف پول
                        </div>
                    </a>
                </li>
            @endcan

            @can('swap_menu')
                <li class="sidebar-menu__item {{request()->url() == route('swap.index')?'active':''}}">
                    <a href="{{ route('swap.index') }}" class="sidebar-menu__link">
                        <div class="sidebar-menu__svg">
                            <div data-toggle="tooltip" data-placement="top" title="تبادل">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22"
                                    height="14"
                                    viewBox="0 0 26.379 18.465"
                                >
                                    <path
                                        d="M12.246,19.371H3v2.638h9.246v3.957l5.263-5.276-5.263-5.276v3.957Zm7.887-1.319V14.095h9.246V11.457H20.133V7.5l-5.263,5.276Z"
                                        transform="translate(-3 -7.5)"
                                        opacity="0.6"
                                    />
                                </svg>
                            </div>
                        </div>
                        تبادل
                    </a>
                </li>
            @endcan

            @can('trade_menu')
                <li class="sidebar-menu__item {{request()->url() == route('exchange.index',['des_unit'=>'btc','source_unit'=>'usdt'])?'active':''}}">
                    <a href="{{route('exchange.index',['des_unit'=>'btc','source_unit'=>'usdt'])}}"
                       class="sidebar-menu__link">
                        <div class="sidebar-menu__svg">
                            <div data-toggle="tooltip" data-placement="top"
                                 title="خرید و فروش">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22"
                                    height="22"
                                    viewBox="0 0 26.51 26.51"
                                >
                                    <path
                                        d="M0,20.985V23.2c0,1.828,4.453,3.314,9.941,3.314s9.941-1.486,9.941-3.314V20.985C17.744,22.492,13.835,23.2,9.941,23.2S2.138,22.492,0,20.985ZM16.569,6.627c5.488,0,9.941-1.486,9.941-3.314S22.057,0,16.569,0,6.627,1.486,6.627,3.314,11.08,6.627,16.569,6.627ZM0,15.554v2.672c0,1.828,4.453,3.314,9.941,3.314s9.941-1.486,9.941-3.314V15.554c-2.138,1.76-6.053,2.672-9.941,2.672S2.138,17.314,0,15.554Zm21.539.57c2.967-.575,4.971-1.641,4.971-2.868V11.044a12.715,12.715,0,0,1-4.971,1.786ZM9.941,8.284C4.453,8.284,0,10.138,0,12.426s4.453,4.142,9.941,4.142,9.941-1.854,9.941-4.142S15.43,8.284,9.941,8.284ZM21.3,11.2C24.4,10.64,26.51,9.542,26.51,8.284V6.073c-1.838,1.3-5,2-8.321,2.164A5.8,5.8,0,0,1,21.3,11.2Z"
                                        fill="#666"
                                    />
                                </svg>
                            </div>
                        </div>

                        خرید و فروش
                    </a>
                </li>
            @endcan

            @can('user_setting_menu')
                <li class="sidebar-menu__item {{request()->url() == route('setting.form')?'active':''}}">
                    <a href="{{route('setting.form')}}" class="sidebar-menu__link">
                        <div class="sidebar-menu__svg">
                            <div data-toggle="tooltip" data-placement="top"
                                 title="تنظیمات">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22"
                                    height="18"
                                    viewBox="0 0 26.51 22.431"
                                >
                                    <g transform="translate(-3.375 -5.625)" opacity="0.6">
                                        <path
                                            d="M19.389,26.279a2.553,2.553,0,0,1,4.677,0h4.8a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02h-4.8a2.553,2.553,0,0,1-4.677,0H4.395a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                            transform="translate(0 -1.792)"
                                        />
                                        <path
                                            d="M9.193,16.717a2.553,2.553,0,0,1,4.677,0H28.865a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02H13.871a2.553,2.553,0,0,1-4.677,0h-4.8a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                            transform="translate(0 -0.896)"
                                        />
                                        <path
                                            d="M19.389,7.154a2.553,2.553,0,0,1,4.677,0h4.8a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02h-4.8a2.553,2.553,0,0,1-4.677,0H4.395a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                        />
                                    </g>
                                </svg>
                            </div>
                        </div>
                        تنظیمات
                    </a>
                </li>
            @endcan

            @can('list_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div
                        href="#"
                        class="
                sidebar-menu__link
                d-flex
                justify-between
                align-items-center
                pl-2
              "
                    >
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="لیست ها"
                                >
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="22"
                                        height="16"
                                        viewBox="0 0 26.02 20.599"
                                    >
                                        <g transform="translate(-4.5 -7.313)">
                                            <path
                                                d="M8.295,17.929a1.9,1.9,0,1,1-1.9-1.9A1.9,1.9,0,0,1,8.295,17.929Z"
                                                transform="translate(0 -0.317)"
                                                fill="#666"
                                            />
                                            <path
                                                d="M8.295,9.21a1.9,1.9,0,1,1-1.9-1.9A1.9,1.9,0,0,1,8.295,9.21Z"
                                                fill="#666"
                                            />
                                            <path
                                                d="M8.295,26.647a1.9,1.9,0,1,1-1.9-1.9,1.9,1.9,0,0,1,1.9,1.9Z"
                                                transform="translate(0 -0.633)"
                                                fill="#666"
                                            />
                                            <path
                                                d="M29.72,16.875h-16.3a1.084,1.084,0,1,0,0,2.168h16.3a1.084,1.084,0,0,0,0-2.168Z"
                                                transform="translate(-0.285 -0.347)"
                                                fill="#666"
                                            />
                                            <path
                                                d="M29.72,25.594h-16.3a1.084,1.084,0,1,0,0,2.168h16.3a1.084,1.084,0,0,0,0-2.168Z"
                                                transform="translate(-0.285 -0.664)"
                                                fill="#666"
                                            />
                                            <path
                                                d="M13.424,10.325h16.3a1.084,1.084,0,0,0,0-2.168h-16.3a1.084,1.084,0,1,0,0,2.168Z"
                                                transform="translate(-0.285 -0.031)"
                                                fill="#666"
                                            />
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <span> لیست ها </span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('user_transaction_menu')
                            <li {{request()->url() == route('transactions.index')?'active':''}}>
                                <a href="{{route('transactions.index')}}"> تراکنش ها</a>
                            </li>
                        @endcan
                        @can('rial_transaction_menu')
                            <li>
                                <a href="{{route('wallet.withdraw.rial.index')}}"> درخواست برداشت ریالی </a>
                            </li>
                        @endcan
                        @can('crypto_transaction_menu')
                            <li>
                                <a href="{{route('withdraw.index')}}"> درخواست برداشت ارز </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            {{--                منوهای مربوط به ادمین (منوی بانک ها و تیکت ها بین کاربر و ادمین مشترک است.           )--}}

            @can('users_management_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#" class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="کاربران">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="25.5"
                                         viewBox="0 0 25.5 25.5">
                                        <path
                                            d="M18,18a6,6,0,1,0-6-6A6,6,0,0,0,18,18Zm0,3c-4.005,0-12,2.01-12,6v3H30V27C30,23.01,22.005,21,18,21Z"
                                            transform="translate(-5.25 -5.25)" fill="none" stroke="#000"
                                            stroke-width="1.5"/>
                                    </svg>
                                </div>
                            </div>
                            <span>کاربران</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('users_index')
                            <li {{request()->url() == route('user.index')?'active':''}}>
                                <a href="{{route('user.index')}}">لیست کاربران </a>
                            </li>
                        @endcan
                        @can('users_authenticate')
                            <li {{request()->url() == route('user.authenticate.index')?'active':''}}>
                                <a href="{{ route('user.authenticate.index') }}">احراز هویت</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('withdraws_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="برداشت ها">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="32" viewBox="0 0 32 32">
                                        <g transform="translate(-2 -2)">
                                            <path d="M33,18A15,15,0,1,1,18,3,15,15,0,0,1,33,18Z" fill="none"
                                                  stroke="#000"
                                                  stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                            <path d="M24,18l-6-6-6,6" fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="2"/>
                                            <path d="M18,24V12" fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-width="2"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <span>برداشت ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('rial_withdraw_index')
                            <li>
                                <a href="{{route('admin.withdraw-rial.index')}}">لیست برداشت ریالی </a>
                            </li>
                        @endcan

                        @can('currency_withdraw_index')
                            <li>
                                <a href="{{route('admin.request.withdraw')}}">لیست برداشت ارزی </a>
                            </li>
                        @endcan

                        @can('withdraw_setting')
                            <li>
                                <a href="{{route('admin.wd-currency-setting')}}">تنظیمات برداشت </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('fees_management_menu')
                <li class="sidebar-menu__item {{request()->url() == route('admin.manage.fees.store')?'active':''}}">
                    <a href="{{route('admin.manage.fees.store')}}" class="sidebar-menu__link">
                        <div class="sidebar-menu__svg">
                            <div data-toggle="tooltip" data-placement="top" title="مدیریت کارمزدها">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23" height="32" viewBox="0 0 23 32">
                                    <path
                                        d="M24.987,7.435a.455.455,0,0,0-.012-.061.492.492,0,0,0-.12-.225h0l-7-7a.492.492,0,0,0-.229-.122.47.47,0,0,0-.058-.012A.484.484,0,0,0,17.5,0H4.5A2.5,2.5,0,0,0,2,2.5v27A2.5,2.5,0,0,0,4.5,32h18A2.5,2.5,0,0,0,25,29.5V7.5a.484.484,0,0,0-.013-.065ZM18,1.707,23.293,7H19.5A1.5,1.5,0,0,1,18,5.5ZM17.513,7H6.5A.5.5,0,0,1,6,6.5v-2A.5.5,0,0,1,6.5,4H17V5.5A2.476,2.476,0,0,0,17.513,7ZM24,29.5A1.5,1.5,0,0,1,22.5,31H4.5A1.5,1.5,0,0,1,3,29.5V2.5A1.5,1.5,0,0,1,4.5,1H17V3H6.5A1.5,1.5,0,0,0,5,4.5v2A1.5,1.5,0,0,0,6.5,8H24ZM12.5,13.8h2c.286,0,.5.159.5.3a.5.5,0,0,0,1,0,1.413,1.413,0,0,0-1.5-1.3H14v-.3a.5.5,0,0,0-1,0v.3h-.5A1.413,1.413,0,0,0,11,14.1v1.6A1.412,1.412,0,0,0,12.5,17h2c.286,0,.5.158.5.3v1.6c0,.142-.214.3-.5.3h-2c-.286,0-.5-.159-.5-.3a.5.5,0,0,0-1,0,1.413,1.413,0,0,0,1.5,1.3H13v.3a.5.5,0,0,0,1,0v-.3h.5A1.413,1.413,0,0,0,16,18.9V17.3A1.412,1.412,0,0,0,14.5,16h-2c-.286,0-.5-.158-.5-.3V14.1C12,13.958,12.214,13.8,12.5,13.8Zm9,14.2H5.5a.5.5,0,0,0,0,1h16a.5.5,0,0,0,0-1ZM5,25.5a.5.5,0,0,0,.5.5h16a.5.5,0,0,0,0-1H5.5A.5.5,0,0,0,5,25.5ZM13.5,9A7.5,7.5,0,1,0,21,16.5,7.5,7.5,0,0,0,13.5,9Zm0,14A6.5,6.5,0,1,1,20,16.5,6.5,6.5,0,0,1,13.5,23Z"
                                        transform="translate(-2)"/>
                                </svg>
                            </div>
                        </div>
                        مدیریت کارمزدها
                    </a>
                </li>
            @endcan

            @can('reports_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="گزارشات">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24.716" height="28.821"
                                         viewBox="0 0 24.716 28.821">
                                        <g transform="translate(-38 -0.1)">
                                            <path d="M345.614,70.623h2.928L345,67.2v2.888A.6.6,0,0,0,345.614,70.623Z"
                                                  transform="translate(-289.715 -63.322)" fill="none"/>
                                            <path
                                                d="M93.646,47.3a2.83,2.83,0,0,1-2.866-2.793V40.134L78.586,40.1a.592.592,0,0,0-.586.563V63.848a.629.629,0,0,0,.614.574H97.7a.543.543,0,0,0,.512-.574V47.3Zm-2.021,4.448a1.126,1.126,0,1,1,0,2.252H81.772a1.126,1.126,0,0,1,0-2.252Zm-9.909-4.5h6.531a1.126,1.126,0,1,1,0,2.252H81.716a1.126,1.126,0,1,1,0-2.252ZM94.44,58.505H81.772a1.126,1.126,0,0,1,0-2.252H94.44a1.126,1.126,0,1,1,0,2.252Z"
                                                transform="translate(-37.748 -37.748)" fill="none"/>
                                            <path
                                                d="M62.165,7.4,55.313.77A2.263,2.263,0,0,0,53.742.134L40.838.1A2.828,2.828,0,0,0,38,2.915V26.1a2.883,2.883,0,0,0,2.866,2.821H59.952a2.8,2.8,0,0,0,2.764-2.826V8.748A1.852,1.852,0,0,0,62.165,7.4Zm-6.88-3.524L58.826,7.3H55.9a.6.6,0,0,1-.614-.54Zm4.667,22.791H40.866a.624.624,0,0,1-.614-.574V2.915a.592.592,0,0,1,.586-.563l12.195.034V6.761A2.834,2.834,0,0,0,55.9,9.553h4.566V26.095A.543.543,0,0,1,59.952,26.669Z"/>
                                            <path
                                                d="M125,248.126a1.129,1.129,0,0,0,1.126,1.126h9.853a1.126,1.126,0,0,0,0-2.252h-9.853A1.129,1.129,0,0,0,125,248.126Z"
                                                transform="translate(-82.102 -232.999)"/>
                                            <path
                                                d="M125.126,169.252h6.531a1.126,1.126,0,1,0,0-2.252h-6.531a1.126,1.126,0,1,0,0,2.252Z"
                                                transform="translate(-81.158 -157.503)"/>
                                            <path
                                                d="M138.794,327H126.126a1.126,1.126,0,1,0,0,2.252h12.668a1.126,1.126,0,1,0,0-2.252Z"
                                                transform="translate(-82.102 -308.495)"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <span>گزارشات</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('transactions_menu')
                            <li>
                                <a href="{{route('admin.report.index')}}">لیست تراکنش ها</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('banks_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="بانک ها">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="29.821"
                                         viewBox="0 0 29.82 29.821">
                                        <g transform="translate(-1.5 -1.499)">
                                            <g transform="translate(15.004 5.828)">
                                                <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"
                                                      transform="translate(-23.658 -8.716)" fill="none" stroke="#000"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10"
                                                      stroke-width="1"/>
                                                <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"
                                                      transform="translate(-23.658 -10.584)" fill="none" stroke="#000"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10"
                                                      stroke-width="1"/>
                                                <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)"
                                                      fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)"
                                                      fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                            <g transform="translate(2 1.999)">
                                                <path d="M30.82,14.967,16.41,2,2,14.967Z"
                                                      transform="translate(-2 -1.999)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                            <g transform="translate(15.004 5.828)">
                                                <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"
                                                      transform="translate(-23.658 -8.716)" fill="none" stroke="#000"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10"
                                                      stroke-width="1"/>
                                                <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"
                                                      transform="translate(-23.658 -10.584)" fill="none" stroke="#000"
                                                      stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10"
                                                      stroke-width="1"/>
                                                <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)"
                                                      fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)"
                                                      fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                            <g transform="translate(2 1.999)">
                                                <path d="M30.82,14.967,16.41,2,2,14.967Z"
                                                      transform="translate(-2 -1.999)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"
                                                      stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                      stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                                <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"
                                                      fill="none" stroke="#000" stroke-linecap="round"
                                                      stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <span>بانک ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('banks_list_menu')
                            <li>
                                <a href="{{route('admin.bank.index')}}">لیست بانک ها </a>
                            </li>
                        @endcan
                        @can('user_bank_menu')
                            <li>
                                <a href="{{route('banks.index')}}">حساب بانکی </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('notifications_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="پیغام ها">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22.476" height="28.132"
                                         viewBox="0 0 22.476 28.132">
                                        <g transform="translate(-6.775 -3.93)">
                                            <path
                                                d="M20.37,28.336a.911.911,0,0,0-.893.717,1.762,1.762,0,0,1-.352.766,1.329,1.329,0,0,1-1.132.415,1.351,1.351,0,0,1-1.132-.415,1.762,1.762,0,0,1-.352-.766.911.911,0,0,0-.893-.717h0a.917.917,0,0,0-.893,1.118,3.142,3.142,0,0,0,3.27,2.609,3.136,3.136,0,0,0,3.27-2.609.92.92,0,0,0-.893-1.118Z"/>
                                            <path
                                                d="M28.969,24.764c-1.083-1.427-3.213-2.264-3.213-8.655,0-6.56-2.9-9.2-5.6-9.83-.253-.063-.436-.148-.436-.415v-.2a1.725,1.725,0,0,0-1.687-1.73h-.042a1.725,1.725,0,0,0-1.687,1.73v.2c0,.26-.183.352-.436.415-2.707.64-5.6,3.27-5.6,9.83,0,6.391-2.13,7.221-3.213,8.655A1.4,1.4,0,0,0,8.177,27H27.872A1.4,1.4,0,0,0,28.969,24.764Zm-2.742.408H9.83a.308.308,0,0,1-.232-.513,8.518,8.518,0,0,0,1.477-2.348,15.934,15.934,0,0,0,1.005-6.2,10.783,10.783,0,0,1,1.47-6.1A4.512,4.512,0,0,1,16.27,8.065a2.464,2.464,0,0,0,1.308-.738.556.556,0,0,1,.837-.014,2.547,2.547,0,0,0,1.322.752,4.512,4.512,0,0,1,2.721,1.941,10.783,10.783,0,0,1,1.47,6.1,15.934,15.934,0,0,0,1.005,6.2,8.615,8.615,0,0,0,1.512,2.384A.291.291,0,0,1,26.227,25.172Z"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <span>پیغام ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('notifications_list')
                            <li>
                                <a href="{{ route('admin.ann.index') }}">لیست پیغام ها </a>
                            </li>
                        @endcan
                        @can('notification_create')
                            <li>
                                <a href="{{ route('admin.ann.create') }}">ایجاد پیغام جدید</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('tickets_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="تیکت ها">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="26" height="28.985"
                                         viewBox="0 0 32.206 28.985">
                                        <path
                                            d="M22.862,0a9.407,9.407,0,0,1,9.344,9.413h0v2.353l-.01.156a1.142,1.142,0,0,1-1.129.992h0l-.014-.029-.179-.014a1.137,1.137,0,0,1-.626-.322,1.152,1.152,0,0,1-.334-.812h0V9.413a7.148,7.148,0,0,0-7.051-7.1H9.344a7.148,7.148,0,0,0-7.051,7.1h0V19.572a7.148,7.148,0,0,0,7.051,7.1H22.862a7.148,7.148,0,0,0,7.051-7.1,1.153,1.153,0,0,1,2.293,0,9.407,9.407,0,0,1-9.344,9.413H9.344A9.393,9.393,0,0,1,0,19.572H0V9.413A9.379,9.379,0,0,1,9.344,0H22.862ZM6.915,8.674a1.12,1.12,0,0,1,.833.251h0l6.695,5.338a2.294,2.294,0,0,0,2.849,0h0l6.623-5.338h.014l.14-.1a1.145,1.145,0,0,1,1.285,1.89h0l-6.623,5.352a4.575,4.575,0,0,1-5.769,0h0L6.324,10.719,6.2,10.6a1.169,1.169,0,0,1-.048-1.5A1.123,1.123,0,0,1,6.915,8.674Z"/>
                                    </svg>
                                </div>
                            </div>
                            <span>تیکت ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('admin_new_tickets')
                            <li>
                                <a href="{{route('ticket.admin.index')}}">تیکت های جدید</a>
                            </li>
                        @endcan
                        @can('admin_my_tickets')
                            <li>
                                <a href="{{route('ticket.admin.index',['type'=>'my_tickets'])}}">تیکت های من</a>
                            </li>
                        @endcan
                        @can('user_ticket_menu')
                            <li>
                                <a href="{{route('tickets.index')}}">تیکت پشتیبانی</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('system_setting_menu')
                <li class="sidebar-menu__item">
                    <a href="{{route('admin.setting')}}"
                       class="sidebar-menu__link d-flex justify-between justify-between pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="تنظیمات سامانه">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                                        <path
                                            d="M18,15a3,3,0,1,0,3,3A3.009,3.009,0,0,0,18,15ZM28.5,4.5H7.5a3,3,0,0,0-3,3v21a3,3,0,0,0,3,3h21a3,3,0,0,0,3-3V7.5A3,3,0,0,0,28.5,4.5ZM25.875,18a7.591,7.591,0,0,1-.075,1.02l2.22,1.74a.53.53,0,0,1,.12.675l-2.1,3.63a.531.531,0,0,1-.645.225l-2.61-1.05a8.044,8.044,0,0,1-1.77,1.035l-.39,2.775a.544.544,0,0,1-.525.45H15.9a.546.546,0,0,1-.525-.435l-.39-2.775a7.713,7.713,0,0,1-1.77-1.035l-2.61,1.05a.531.531,0,0,1-.645-.225l-2.1-3.63a.53.53,0,0,1,.12-.675l2.22-1.74A8.006,8.006,0,0,1,10.125,18a7.591,7.591,0,0,1,.075-1.02L7.98,15.24a.53.53,0,0,1-.12-.675l2.1-3.63a.531.531,0,0,1,.645-.225l2.61,1.05a8.044,8.044,0,0,1,1.77-1.035l.39-2.775A.544.544,0,0,1,15.9,7.5h4.2a.546.546,0,0,1,.525.435l.39,2.775a7.713,7.713,0,0,1,1.77,1.035l2.61-1.05a.531.531,0,0,1,.645.225l2.1,3.63a.53.53,0,0,1-.12.675l-2.22,1.74A8.006,8.006,0,0,1,25.875,18Z"
                                            transform="translate(-4 -4)" fill="none" stroke="#000" stroke-width="1"/>
                                    </svg>
                                </div>
                            </div>
                            تنظیمات سامانه
                        </div>
                    </a>
                </li>
            @endcan

            @can('system_setting_menu')
                <li class="sidebar-menu__item">
                    <a href="{{route('admin.support')}}"
                       class="sidebar-menu__link d-flex justify-between justify-between pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="پشتیبان">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                                        <path
                                            d="M18,15a3,3,0,1,0,3,3A3.009,3.009,0,0,0,18,15ZM28.5,4.5H7.5a3,3,0,0,0-3,3v21a3,3,0,0,0,3,3h21a3,3,0,0,0,3-3V7.5A3,3,0,0,0,28.5,4.5ZM25.875,18a7.591,7.591,0,0,1-.075,1.02l2.22,1.74a.53.53,0,0,1,.12.675l-2.1,3.63a.531.531,0,0,1-.645.225l-2.61-1.05a8.044,8.044,0,0,1-1.77,1.035l-.39,2.775a.544.544,0,0,1-.525.45H15.9a.546.546,0,0,1-.525-.435l-.39-2.775a7.713,7.713,0,0,1-1.77-1.035l-2.61,1.05a.531.531,0,0,1-.645-.225l-2.1-3.63a.53.53,0,0,1,.12-.675l2.22-1.74A8.006,8.006,0,0,1,10.125,18a7.591,7.591,0,0,1,.075-1.02L7.98,15.24a.53.53,0,0,1-.12-.675l2.1-3.63a.531.531,0,0,1,.645-.225l2.61,1.05a8.044,8.044,0,0,1,1.77-1.035l.39-2.775A.544.544,0,0,1,15.9,7.5h4.2a.546.546,0,0,1,.525.435l.39,2.775a7.713,7.713,0,0,1,1.77,1.035l2.61-1.05a.531.531,0,0,1,.645.225l2.1,3.63a.53.53,0,0,1-.12.675l-2.22,1.74A8.006,8.006,0,0,1,25.875,18Z"
                                            transform="translate(-4 -4)" fill="none" stroke="#000" stroke-width="1"/>
                                    </svg>
                                </div>
                            </div>
                            پشتیبان
                        </div>
                    </a>
                </li>
            @endcan

            @can('system_setting_menu')
                <li class="sidebar-menu__item">
                    <a href="{{route('admin.laws')}}"
                       class="sidebar-menu__link d-flex justify-between justify-between pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <div data-toggle="tooltip" data-placement="top" title="قوانین">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">
                                        <path
                                            d="M18,15a3,3,0,1,0,3,3A3.009,3.009,0,0,0,18,15ZM28.5,4.5H7.5a3,3,0,0,0-3,3v21a3,3,0,0,0,3,3h21a3,3,0,0,0,3-3V7.5A3,3,0,0,0,28.5,4.5ZM25.875,18a7.591,7.591,0,0,1-.075,1.02l2.22,1.74a.53.53,0,0,1,.12.675l-2.1,3.63a.531.531,0,0,1-.645.225l-2.61-1.05a8.044,8.044,0,0,1-1.77,1.035l-.39,2.775a.544.544,0,0,1-.525.45H15.9a.546.546,0,0,1-.525-.435l-.39-2.775a7.713,7.713,0,0,1-1.77-1.035l-2.61,1.05a.531.531,0,0,1-.645-.225l-2.1-3.63a.53.53,0,0,1,.12-.675l2.22-1.74A8.006,8.006,0,0,1,10.125,18a7.591,7.591,0,0,1,.075-1.02L7.98,15.24a.53.53,0,0,1-.12-.675l2.1-3.63a.531.531,0,0,1,.645-.225l2.61,1.05a8.044,8.044,0,0,1,1.77-1.035l.39-2.775A.544.544,0,0,1,15.9,7.5h4.2a.546.546,0,0,1,.525.435l.39,2.775a7.713,7.713,0,0,1,1.77,1.035l2.61-1.05a.531.531,0,0,1,.645.225l2.1,3.63a.53.53,0,0,1-.12.675l-2.22,1.74A8.006,8.006,0,0,1,25.875,18Z"
                                            transform="translate(-4 -4)" fill="none" stroke="#000" stroke-width="1"/>
                                    </svg>
                                </div>
                            </div>
                            قوانین
                        </div>
                    </a>
                </li>
            @endcan

            {{--                          پایان منوی ادمین                     --}}

        </ul>
    </div>

    <div class="open__sidebar" isClosed="true">
        <svg
            xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            width="38"
            height="38"
            viewBox="0 0 48 48"
        >
            <defs>
                <filter
                    id="a"
                    x="0"
                    y="0"
                    width="48"
                    height="48"
                    filterUnits="userSpaceOnUse"
                >
                    <feOffset input="SourceAlpha"/>
                    <feGaussianBlur stdDeviation="3" result="b"/>
                    <feFlood flood-opacity="0.161"/>
                    <feComposite operator="in" in2="b"/>
                    <feComposite in="SourceGraphic"/>
                </filter>
            </defs>
            <g transform="translate(-1796 -80)">
                <g transform="matrix(1, 0, 0, 1, 1796, 80)" filter="url(#a)">
                    <circle
                        cx="15"
                        cy="15"
                        r="15"
                        transform="translate(9 9)"
                        fill="#fff"
                    />
                </g>
                <path
                    d="M19.824,12.885l-4.691,4.681-4.691-4.681L9,14.326l6.133,6.133,6.133-6.133Z"
                    transform="translate(1836.672 88.868) rotate(90)"
                    opacity="0.6"
                />
            </g>
        </svg>
    </div>
</div>

<div class="responsive-sidebar">
    <div class="container-menu">
        <ul class="ul-res-menu">
            <h2>روتیکس</h2>
            @can('dashboard_menu')
                <li>
                    <a href="{{route('user.dashboard')}}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="23"
                            viewBox="0 0 24.597 27.107">
                            <g transform="translate(1 1)">
                                <path
                                    d="M4.5,11.788,15.8,3l11.3,8.788V25.6a2.511,2.511,0,0,1-2.511,2.511H7.011A2.511,2.511,0,0,1,4.5,25.6Z"
                                    transform="translate(-4.5 -3)"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                />
                                <path
                                    d="M13.5,30.554V18h7.532V30.554"
                                    transform="translate(-5.968 -5.446)"
                                    fill="none"
                                    stroke="#000"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                />
                            </g>
                        </svg>
                        داشبورد
                    </a>
                </li>
            @endcan
            @can('wallet_menu')
                <li>
                    <a href="{{ route('wallet.index') }}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="19"
                            viewBox="0 0 24.228 23.372"
                        >
                            <g transform="translate(-2.494 -2.737)" opacity="0.6">
                                <path
                                    d="M22.713,11.25H6.634a3.262,3.262,0,0,0-3.259,3.259v9.126a3.262,3.262,0,0,0,3.259,3.259H22.713a3.262,3.262,0,0,0,3.259-3.259V14.509A3.262,3.262,0,0,0,22.713,11.25Z"
                                    transform="translate(0 -1.535)"
                                    fill="none"
                                    stroke="#000"
                                    stroke-width="1.5"
                                />
                                <path
                                    d="M19.65,4.554,6.89,7.047c-.978.217-2.39,1.2-2.39,2.4A3.423,3.423,0,0,1,7.162,8.411h15.59V7.3A3.119,3.119,0,0,0,22,5.255h0A2.569,2.569,0,0,0,19.65,4.554Z"
                                    transform="translate(-1.256 -1)"
                                    fill="none"
                                    stroke="#000"
                                    stroke-width="1.5"
                                />
                            </g>
                        </svg>
                        کیف پول
                    </a>
                </li>
            @endcan
            @can('swap_menu')
                <li>
                    <a href="{{ route('swap.index') }}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="22"
                            height="14"
                            viewBox="0 0 26.379 18.465"
                        >
                            <path
                                d="M12.246,19.371H3v2.638h9.246v3.957l5.263-5.276-5.263-5.276v3.957Zm7.887-1.319V14.095h9.246V11.457H20.133V7.5l-5.263,5.276Z"
                                transform="translate(-3 -7.5)"
                                opacity="0.6"
                            />
                        </svg>
                        تبادل
                    </a>
                </li>
            @endcan
            @can('trade_menu')
                <li>
                    <a href="{{route('exchange.index',['des_unit'=>'btc','source_unit'=>'usdt'])}}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="22"
                            height="22"
                            viewBox="0 0 26.51 26.51"
                        >
                            <path
                                d="M0,20.985V23.2c0,1.828,4.453,3.314,9.941,3.314s9.941-1.486,9.941-3.314V20.985C17.744,22.492,13.835,23.2,9.941,23.2S2.138,22.492,0,20.985ZM16.569,6.627c5.488,0,9.941-1.486,9.941-3.314S22.057,0,16.569,0,6.627,1.486,6.627,3.314,11.08,6.627,16.569,6.627ZM0,15.554v2.672c0,1.828,4.453,3.314,9.941,3.314s9.941-1.486,9.941-3.314V15.554c-2.138,1.76-6.053,2.672-9.941,2.672S2.138,17.314,0,15.554Zm21.539.57c2.967-.575,4.971-1.641,4.971-2.868V11.044a12.715,12.715,0,0,1-4.971,1.786ZM9.941,8.284C4.453,8.284,0,10.138,0,12.426s4.453,4.142,9.941,4.142,9.941-1.854,9.941-4.142S15.43,8.284,9.941,8.284ZM21.3,11.2C24.4,10.64,26.51,9.542,26.51,8.284V6.073c-1.838,1.3-5,2-8.321,2.164A5.8,5.8,0,0,1,21.3,11.2Z"
                                fill="#666"
                            />
                        </svg>
                        خرید و فروش
                    </a>
                </li>
            @endcan
            @can('user_setting_menu')
                <li>
                    <a href="{{route('setting.form')}}">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 26.51 22.431"
                        >
                            <g transform="translate(-3.375 -5.625)" opacity="0.6">
                                <path
                                    d="M19.389,26.279a2.553,2.553,0,0,1,4.677,0h4.8a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02h-4.8a2.553,2.553,0,0,1-4.677,0H4.395a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                    transform="translate(0 -1.792)"
                                />
                                <path
                                    d="M9.193,16.717a2.553,2.553,0,0,1,4.677,0H28.865a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02H13.871a2.553,2.553,0,0,1-4.677,0h-4.8a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                    transform="translate(0 -0.896)"
                                />
                                <path
                                    d="M19.389,7.154a2.553,2.553,0,0,1,4.677,0h4.8a1.023,1.023,0,0,1,1.02,1.02h0a1.023,1.023,0,0,1-1.02,1.02h-4.8a2.553,2.553,0,0,1-4.677,0H4.395a1.023,1.023,0,0,1-1.02-1.02h0a1.023,1.023,0,0,1,1.02-1.02Z"
                                />
                            </g>
                        </svg>
                        تنظیمات
                    </a>
                </li>
            @endcan

            @can('list_menu')
                <li class="sidebar-menu__item sidebar__submenu-item" data-is-open="false">
                    <div
                        class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="20"
                                    height="20"
                                    viewBox="0 0 26.02 20.599"
                                >
                                    <g transform="translate(-4.5 -7.313)">
                                        <path
                                            d="M8.295,17.929a1.9,1.9,0,1,1-1.9-1.9A1.9,1.9,0,0,1,8.295,17.929Z"
                                            transform="translate(0 -0.317)"
                                            fill="#666"
                                        />
                                        <path
                                            d="M8.295,9.21a1.9,1.9,0,1,1-1.9-1.9A1.9,1.9,0,0,1,8.295,9.21Z"
                                            fill="#666"
                                        />
                                        <path
                                            d="M8.295,26.647a1.9,1.9,0,1,1-1.9-1.9,1.9,1.9,0,0,1,1.9,1.9Z"
                                            transform="translate(0 -0.633)"
                                            fill="#666"
                                        />
                                        <path
                                            d="M29.72,16.875h-16.3a1.084,1.084,0,1,0,0,2.168h16.3a1.084,1.084,0,0,0,0-2.168Z"
                                            transform="translate(-0.285 -0.347)"
                                            fill="#666"
                                        />
                                        <path
                                            d="M29.72,25.594h-16.3a1.084,1.084,0,1,0,0,2.168h16.3a1.084,1.084,0,0,0,0-2.168Z"
                                            transform="translate(-0.285 -0.664)"
                                            fill="#666"
                                        />
                                        <path
                                            d="M13.424,10.325h16.3a1.084,1.084,0,0,0,0-2.168h-16.3a1.084,1.084,0,1,0,0,2.168Z"
                                            transform="translate(-0.285 -0.031)"
                                            fill="#666"
                                        />
                                    </g>
                                </svg>
                            </div>
                            <span> لیست ها </span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('user_transaction_menu')
                            <li {{request()->url() == route('transactions.index')?'active':''}}>
                                <a href="{{route('transactions.index')}}"> تراکنش ها</a>
                            </li>
                        @endcan
                        @can('rial_transaction_menu')
                            <li>
                                <a href="{{route('wallet.withdraw.rial.index')}}"> درخواست برداشت ریالی </a>
                            </li>
                        @endcan
                        @can('crypto_transaction_menu')
                            <li>
                                <a href="{{route('withdraw.index')}}"> درخواست برداشت ارز </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            {{--                            منوهای مربوط به ادمین (منوی بانک ها و تیکت ها بین کاربر و ادمین مشترک است.           )--}}

            @can('users_management_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#" class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 25.5 25.5">
                                    <path
                                        d="M18,18a6,6,0,1,0-6-6A6,6,0,0,0,18,18Zm0,3c-4.005,0-12,2.01-12,6v3H30V27C30,23.01,22.005,21,18,21Z"
                                        transform="translate(-5.25 -5.25)" fill="none" stroke="#000"
                                        stroke-width="1.5"/>
                                </svg>
                            </div>
                            <span>کاربران</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('users_index')
                            <li {{request()->url() == route('user.index')?'active':''}}>
                                <a href="{{route('user.index')}}">لیست کاربران </a>
                            </li>
                        @endcan
                        @can('users_authenticate')
                            <li {{request()->url() == route('user.authenticate.index')?'active':''}}>
                                <a href="{{ route('user.authenticate.index') }}">احراز هویت</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('withdraws_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32">
                                    <g transform="translate(-2 -2)">
                                        <path d="M33,18A15,15,0,1,1,18,3,15,15,0,0,1,33,18Z" fill="none"
                                              stroke="#000"
                                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                        <path d="M24,18l-6-6-6,6" fill="none" stroke="#000" stroke-linecap="round"
                                              stroke-linejoin="round" stroke-width="2"/>
                                        <path d="M18,24V12" fill="none" stroke="#000" stroke-linecap="round"
                                              stroke-linejoin="round" stroke-width="2"/>
                                    </g>
                                </svg>
                            </div>
                            <span>برداشت ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('rial_withdraw_index')
                            <li>
                                <a href="{{route('admin.withdraw-rial.index')}}">لیست برداشت ریالی </a>
                            </li>
                        @endcan

                        @can('currency_withdraw_index')
                            <li>
                                <a href="{{route('admin.request.withdraw')}}">لیست برداشت ارزی </a>
                            </li>
                        @endcan

                        @can('withdraw_setting')
                            <li>
                                <a href="{{route('admin.wd-currency-setting')}}">تنظیمات برداشت </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('fees_management_menu')
                <li class="sidebar-menu__item {{request()->url() == route('admin.manage.fees.store')?'active':''}}">
                    <a href="{{route('admin.manage.fees.store')}}" class="sidebar-menu__link">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 23 32">
                            <path
                                d="M24.987,7.435a.455.455,0,0,0-.012-.061.492.492,0,0,0-.12-.225h0l-7-7a.492.492,0,0,0-.229-.122.47.47,0,0,0-.058-.012A.484.484,0,0,0,17.5,0H4.5A2.5,2.5,0,0,0,2,2.5v27A2.5,2.5,0,0,0,4.5,32h18A2.5,2.5,0,0,0,25,29.5V7.5a.484.484,0,0,0-.013-.065ZM18,1.707,23.293,7H19.5A1.5,1.5,0,0,1,18,5.5ZM17.513,7H6.5A.5.5,0,0,1,6,6.5v-2A.5.5,0,0,1,6.5,4H17V5.5A2.476,2.476,0,0,0,17.513,7ZM24,29.5A1.5,1.5,0,0,1,22.5,31H4.5A1.5,1.5,0,0,1,3,29.5V2.5A1.5,1.5,0,0,1,4.5,1H17V3H6.5A1.5,1.5,0,0,0,5,4.5v2A1.5,1.5,0,0,0,6.5,8H24ZM12.5,13.8h2c.286,0,.5.159.5.3a.5.5,0,0,0,1,0,1.413,1.413,0,0,0-1.5-1.3H14v-.3a.5.5,0,0,0-1,0v.3h-.5A1.413,1.413,0,0,0,11,14.1v1.6A1.412,1.412,0,0,0,12.5,17h2c.286,0,.5.158.5.3v1.6c0,.142-.214.3-.5.3h-2c-.286,0-.5-.159-.5-.3a.5.5,0,0,0-1,0,1.413,1.413,0,0,0,1.5,1.3H13v.3a.5.5,0,0,0,1,0v-.3h.5A1.413,1.413,0,0,0,16,18.9V17.3A1.412,1.412,0,0,0,14.5,16h-2c-.286,0-.5-.158-.5-.3V14.1C12,13.958,12.214,13.8,12.5,13.8Zm9,14.2H5.5a.5.5,0,0,0,0,1h16a.5.5,0,0,0,0-1ZM5,25.5a.5.5,0,0,0,.5.5h16a.5.5,0,0,0,0-1H5.5A.5.5,0,0,0,5,25.5ZM13.5,9A7.5,7.5,0,1,0,21,16.5,7.5,7.5,0,0,0,13.5,9Zm0,14A6.5,6.5,0,1,1,20,16.5,6.5,6.5,0,0,1,13.5,23Z"
                                transform="translate(-2)"/>
                        </svg>
                        مدیریت کارمزدها
                    </a>
                </li>
            @endcan

            @can('reports_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 24.716 28.821">
                                    <g transform="translate(-38 -0.1)">
                                        <path d="M345.614,70.623h2.928L345,67.2v2.888A.6.6,0,0,0,345.614,70.623Z"
                                              transform="translate(-289.715 -63.322)" fill="none"/>
                                        <path
                                            d="M93.646,47.3a2.83,2.83,0,0,1-2.866-2.793V40.134L78.586,40.1a.592.592,0,0,0-.586.563V63.848a.629.629,0,0,0,.614.574H97.7a.543.543,0,0,0,.512-.574V47.3Zm-2.021,4.448a1.126,1.126,0,1,1,0,2.252H81.772a1.126,1.126,0,0,1,0-2.252Zm-9.909-4.5h6.531a1.126,1.126,0,1,1,0,2.252H81.716a1.126,1.126,0,1,1,0-2.252ZM94.44,58.505H81.772a1.126,1.126,0,0,1,0-2.252H94.44a1.126,1.126,0,1,1,0,2.252Z"
                                            transform="translate(-37.748 -37.748)" fill="none"/>
                                        <path
                                            d="M62.165,7.4,55.313.77A2.263,2.263,0,0,0,53.742.134L40.838.1A2.828,2.828,0,0,0,38,2.915V26.1a2.883,2.883,0,0,0,2.866,2.821H59.952a2.8,2.8,0,0,0,2.764-2.826V8.748A1.852,1.852,0,0,0,62.165,7.4Zm-6.88-3.524L58.826,7.3H55.9a.6.6,0,0,1-.614-.54Zm4.667,22.791H40.866a.624.624,0,0,1-.614-.574V2.915a.592.592,0,0,1,.586-.563l12.195.034V6.761A2.834,2.834,0,0,0,55.9,9.553h4.566V26.095A.543.543,0,0,1,59.952,26.669Z"/>
                                        <path
                                            d="M125,248.126a1.129,1.129,0,0,0,1.126,1.126h9.853a1.126,1.126,0,0,0,0-2.252h-9.853A1.129,1.129,0,0,0,125,248.126Z"
                                            transform="translate(-82.102 -232.999)"/>
                                        <path
                                            d="M125.126,169.252h6.531a1.126,1.126,0,1,0,0-2.252h-6.531a1.126,1.126,0,1,0,0,2.252Z"
                                            transform="translate(-81.158 -157.503)"/>
                                        <path
                                            d="M138.794,327H126.126a1.126,1.126,0,1,0,0,2.252h12.668a1.126,1.126,0,1,0,0-2.252Z"
                                            transform="translate(-82.102 -308.495)"/>
                                    </g>
                                </svg>
                            </div>
                            <span>گزارشات</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('transactions_menu')
                            <li>
                                <a href="{{route('admin.report.index')}}">لیست تراکنش ها</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('banks_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 29.82 29.821">
                                    <g transform="translate(-1.5 -1.499)">
                                        <g transform="translate(15.004 5.828)">
                                            <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"
                                                  transform="translate(-23.658 -8.716)" fill="none" stroke="#000"
                                                  stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10"
                                                  stroke-width="1"/>
                                            <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"
                                                  transform="translate(-23.658 -10.584)" fill="none" stroke="#000"
                                                  stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10"
                                                  stroke-width="1"/>
                                            <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)"
                                                  fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)"
                                                  fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <g transform="translate(2 1.999)">
                                            <path d="M30.82,14.967,16.41,2,2,14.967Z"
                                                  transform="translate(-2 -1.999)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <g transform="translate(15.004 5.828)">
                                            <path d="M26.47,10.628a1.406,1.406,0,1,0-1.406,1.406"
                                                  transform="translate(-23.658 -8.716)" fill="none" stroke="#000"
                                                  stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10"
                                                  stroke-width="1"/>
                                            <path d="M23.658,15.308A1.406,1.406,0,1,0,25.064,13.9"
                                                  transform="translate(-23.658 -10.584)" fill="none" stroke="#000"
                                                  stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10"
                                                  stroke-width="1"/>
                                            <path d="M26,18.577v.511" transform="translate(-24.594 -12.452)"
                                                  fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M26,8.376v.512" transform="translate(-24.594 -8.376)"
                                                  fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                        <g transform="translate(2 1.999)">
                                            <path d="M30.82,14.967,16.41,2,2,14.967Z"
                                                  transform="translate(-2 -1.999)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M2,50H30.82" transform="translate(-2 -21.179)" fill="none"
                                                  stroke="#000" stroke-linecap="round" stroke-linejoin="round"
                                                  stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M43.867,39.452V23.6" transform="translate(-18.73 -10.631)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M8.127,39.452V23.6" transform="translate(-4.448 -10.631)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M32.047,23.6V39.45" transform="translate(-14.006 -10.63)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                            <path d="M19.953,39.451V23.6" transform="translate(-9.174 -10.63)"
                                                  fill="none" stroke="#000" stroke-linecap="round"
                                                  stroke-linejoin="round" stroke-miterlimit="10" stroke-width="1"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <span>بانک ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('banks_list_menu')
                            <li>
                                <a href="{{route('admin.bank.index')}}">لیست بانک ها </a>
                            </li>
                        @endcan
                        @can('user_bank_menu')
                            <li>
                                <a href="{{route('banks.index')}}">حساب بانکی </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('notifications_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 22.476 28.132">
                                    <g transform="translate(-6.775 -3.93)">
                                        <path
                                            d="M20.37,28.336a.911.911,0,0,0-.893.717,1.762,1.762,0,0,1-.352.766,1.329,1.329,0,0,1-1.132.415,1.351,1.351,0,0,1-1.132-.415,1.762,1.762,0,0,1-.352-.766.911.911,0,0,0-.893-.717h0a.917.917,0,0,0-.893,1.118,3.142,3.142,0,0,0,3.27,2.609,3.136,3.136,0,0,0,3.27-2.609.92.92,0,0,0-.893-1.118Z"/>
                                        <path
                                            d="M28.969,24.764c-1.083-1.427-3.213-2.264-3.213-8.655,0-6.56-2.9-9.2-5.6-9.83-.253-.063-.436-.148-.436-.415v-.2a1.725,1.725,0,0,0-1.687-1.73h-.042a1.725,1.725,0,0,0-1.687,1.73v.2c0,.26-.183.352-.436.415-2.707.64-5.6,3.27-5.6,9.83,0,6.391-2.13,7.221-3.213,8.655A1.4,1.4,0,0,0,8.177,27H27.872A1.4,1.4,0,0,0,28.969,24.764Zm-2.742.408H9.83a.308.308,0,0,1-.232-.513,8.518,8.518,0,0,0,1.477-2.348,15.934,15.934,0,0,0,1.005-6.2,10.783,10.783,0,0,1,1.47-6.1A4.512,4.512,0,0,1,16.27,8.065a2.464,2.464,0,0,0,1.308-.738.556.556,0,0,1,.837-.014,2.547,2.547,0,0,0,1.322.752,4.512,4.512,0,0,1,2.721,1.941,10.783,10.783,0,0,1,1.47,6.1,15.934,15.934,0,0,0,1.005,6.2,8.615,8.615,0,0,0,1.512,2.384A.291.291,0,0,1,26.227,25.172Z"/>
                                    </g>
                                </svg>
                            </div>
                            <span>پیغام ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('notifications_list')
                            <li>
                                <a href="">لیست پیغام ها </a>
                            </li>
                        @endcan
                        @can('notification_create')
                            <li>
                                <a href="">ایجاد پیغام جدید</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('tickets_menu')
                <li class="sidebar-menu__item sidebar__submenu-item"
                    data-is-open="false">
                    <div href="#"
                         class="sidebar-menu__link d-flex justify-between align-items-center pl-2">
                        <div class="d-flex align-items-center">
                            <div class="sidebar-menu__svg">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 32.206 28.985">
                                    <path
                                        d="M22.862,0a9.407,9.407,0,0,1,9.344,9.413h0v2.353l-.01.156a1.142,1.142,0,0,1-1.129.992h0l-.014-.029-.179-.014a1.137,1.137,0,0,1-.626-.322,1.152,1.152,0,0,1-.334-.812h0V9.413a7.148,7.148,0,0,0-7.051-7.1H9.344a7.148,7.148,0,0,0-7.051,7.1h0V19.572a7.148,7.148,0,0,0,7.051,7.1H22.862a7.148,7.148,0,0,0,7.051-7.1,1.153,1.153,0,0,1,2.293,0,9.407,9.407,0,0,1-9.344,9.413H9.344A9.393,9.393,0,0,1,0,19.572H0V9.413A9.379,9.379,0,0,1,9.344,0H22.862ZM6.915,8.674a1.12,1.12,0,0,1,.833.251h0l6.695,5.338a2.294,2.294,0,0,0,2.849,0h0l6.623-5.338h.014l.14-.1a1.145,1.145,0,0,1,1.285,1.89h0l-6.623,5.352a4.575,4.575,0,0,1-5.769,0h0L6.324,10.719,6.2,10.6a1.169,1.169,0,0,1-.048-1.5A1.123,1.123,0,0,1,6.915,8.674Z"/>
                                </svg>
                            </div>
                            <span>تیکت ها</span>
                        </div>

                        <span class="sidebar-item__arrow"></span>
                    </div>

                    <ul class="submenu">
                        @can('admin_new_tickets')
                            <li>
                                <a href="{{route('ticket.admin.index')}}">تیکت های جدید</a>
                            </li>
                        @endcan
                        @can('admin_my_tickets')
                            <li>
                                <a href="{{route('ticket.admin.index',['type'=>'my_tickets'])}}">تیکت های من</a>
                            </li>
                        @endcan
                        @can('user_ticket_menu')
                            <li>
                                <a href="{{route('tickets.index')}}">تیکت پشتیبانی</a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan

            @can('system_setting_menu')
                <li class="sidebar-menu__item">
                    <a href="{{route('admin.setting')}}"
                       class="sidebar-menu__link d-flex justify-between justify-between pl-2">
                        <div class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 28 28">
                                <path
                                    d="M18,15a3,3,0,1,0,3,3A3.009,3.009,0,0,0,18,15ZM28.5,4.5H7.5a3,3,0,0,0-3,3v21a3,3,0,0,0,3,3h21a3,3,0,0,0,3-3V7.5A3,3,0,0,0,28.5,4.5ZM25.875,18a7.591,7.591,0,0,1-.075,1.02l2.22,1.74a.53.53,0,0,1,.12.675l-2.1,3.63a.531.531,0,0,1-.645.225l-2.61-1.05a8.044,8.044,0,0,1-1.77,1.035l-.39,2.775a.544.544,0,0,1-.525.45H15.9a.546.546,0,0,1-.525-.435l-.39-2.775a7.713,7.713,0,0,1-1.77-1.035l-2.61,1.05a.531.531,0,0,1-.645-.225l-2.1-3.63a.53.53,0,0,1,.12-.675l2.22-1.74A8.006,8.006,0,0,1,10.125,18a7.591,7.591,0,0,1,.075-1.02L7.98,15.24a.53.53,0,0,1-.12-.675l2.1-3.63a.531.531,0,0,1,.645-.225l2.61,1.05a8.044,8.044,0,0,1,1.77-1.035l.39-2.775A.544.544,0,0,1,15.9,7.5h4.2a.546.546,0,0,1,.525.435l.39,2.775a7.713,7.713,0,0,1,1.77,1.035l2.61-1.05a.531.531,0,0,1,.645.225l2.1,3.63a.53.53,0,0,1-.12.675l-2.22,1.74A8.006,8.006,0,0,1,25.875,18Z"
                                    transform="translate(-4 -4)" fill="none" stroke="#000" stroke-width="1"/>
                            </svg>
                            تنظیمات سامانه
                        </div>
                    </a>
                </li>
            @endcan

            {{--                          پایان منوی ادمین                     --}}

            @can('profile_btn')
                <li>
                    <a href="{{route('user.profile.form')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 36 31.5">
                            <path d="M13.5,0V4.5h18V27h-18v4.5H36V0ZM9,9,0,15.75,9,22.5V18H27V13.5H9Z" fill="#1b274c"/>
                        </svg>
                        پروفایل
                    </a>
                </li>
            @endcan
            <li>
                <a href="{{route('logout')}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                        <path
                            d="M18,3A15,15,0,1,0,33,18,15.005,15.005,0,0,0,18,3Zm0,4.5A4.5,4.5,0,1,1,13.5,12,4.494,4.494,0,0,1,18,7.5Zm0,21.3a10.8,10.8,0,0,1-9-4.83c.045-2.985,6-4.62,9-4.62s8.955,1.635,9,4.62a10.8,10.8,0,0,1-9,4.83Z"
                            transform="translate(-2 -2)" fill="none" stroke="#1b274c" stroke-width="2"/>
                    </svg>
                    خروج
                </a>
            </li>
        </ul>
    </div>
</div>
