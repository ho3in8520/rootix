<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="{{ asset('general/js/jquery-3.6.0.min.js') }}"></script>
<script src="https://unpkg.com/@popperjs/core@2"></script>

<script src="{{asset('theme/user/scripts/popper.min.js')}}"></script>
<script src="{{asset('theme/user/scripts/bootstrap-old.min.js')}}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.min.js") }}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.fa.min.js") }}"></script>
<script src="{{asset('theme/user/scripts/show-profile.js?v=1.0.0')}}"></script>
<script src="{{asset('theme/user/scripts/dots-menu-show.js?v=1.0.0')}}"></script>
<script src="{{asset('theme/user/scripts/panel-menu.js?v=1.0.0')}}"></script>
<script src="{{ asset('general/sweetalert/sweetalert.min.js')}}"></script>

<script src="{{  asset("general/datepicker/bootstrap-datepicker.min.js") }}"></script>
<script src="{{  asset("general/datepicker/bootstrap-datepicker.fa.min.js") }}"></script>


<script src="{{ asset('general/js/popup.js') }}"></script>
<script src="{{ asset('general/js/func.js?v=1.0.0') }}"></script>
<script src="{{ asset('general/js/project.js?v=1.0.1') }}"></script>
{{--<script src="{{asset('theme/user/scripts/jquery-3.2.1.slim.min.js')}}"></script>--}}
<script src="{{ asset('theme/user/scripts/owl.carousel.min.js') }}"></script>
<script src="{{ asset('theme/user/scripts/custom2.js?v=1.0.0') }}"></script>
<script src="{{ asset('theme/user/scripts/mr7.js?v=1.0.0') }}"></script>
<script src="{{ asset('theme/user/scripts/selected-swap.js?v=1.0.0') }}"></script>
<script src="{{ asset('theme/user/scripts/custom-select-box-2.js?v=1.0.0') }}"></script>
