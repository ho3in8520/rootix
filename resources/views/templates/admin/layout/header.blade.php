<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
<link rel="stylesheet" href="{{asset('theme/user/styles/bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{ asset("general/datepicker/bootstrap-datepicker.min.css") }}">
<link rel="stylesheet" href="{{asset('theme/user/styles/grid.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/custom.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/icon.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/font.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/app.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/app2.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/responsive.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/custom2.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/owl.carousel.min.css')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/mr7-custom.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/styles/app3.css?v=1.0.4')}}" />
<link rel="stylesheet" href="{{asset('theme/user/assets/css/select2.min.css')}}" />

{{--@livewireStyles--}}
