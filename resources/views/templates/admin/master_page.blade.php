<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>@yield('title_browser')</title>
    <link rel="icon" type="image/x-icon" href="{{asset('theme/landing/images/logo.png')}}">
    @include('templates.admin.layout.header')
    @yield('style')
</head>
<body>
@include('sweetalert::alert')
{{--<section class="section">--}}
{{--    <div class="d-flex">--}}
<!-- Side Bar -->
@include('templates.user.layout.sidebar')

<!-- Content -->
<div class="content @yield('content_class')">
@yield('extra_div')
<!-- header -->
@include('templates.user.layout.top_menu')

<!-- Main Content -->
    <main style="flex: 1">
        @include('general.flash_message')
        @yield('content')
    </main>

    <h5 class="text-center footer-title"> 2020. All Rights Reserved</h5>

</div>
{{--    </div>--}}
{{--</section>--}}
@include('templates.admin.layout.footer')
@yield('script')
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    $.get("{{route('get-cities')}}", {id: id}, function (data) {
        $("select.city").html(JSON.parse(data));
    })
</script>
</body>
</html>
