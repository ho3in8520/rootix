@extends('templates.admin.master_page')
@section('title_browser')
    ویرایش {{ request()->type == 'amozesh'?'آموزش':'خبر' }}
@endsection
@section('style')
    <style>
        #ckeditor {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('posts.update',$post->id)}}" method="post">
                        @csrf
                        @method('put')
                        <input type="hidden" name="type" value="{{ request()->type }}">
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="title" class="">عنوان</label>
                                <input type="text" name="title" class="form-control @error('title') error @enderror" value="{{$post->title}}">
                                @error('title') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                <label for="description" class="">توضیحات</label>
                                <textarea id="ckeditor" name="description" rows="10" cols="125" class="@error('description') error @enderror">{{$post->description}}
            </textarea>
                                @error('description') <span class="text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="form-group col-lg-2 col-md-4 col-sm-6">
                                <label for="category_id">دسته بندی</label>
                                <select name="category_id" class="form-control select2-single">
                                    <option value="">انتخاب کنید</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if($post->category_id == $category->id) selected @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6">
                                <label for="tags">تگ ها</label>
                                <input id="tags" name="tags" class="" data-role="tagsinput" value="{{ implode(',',json_decode($post->tags)) }}">
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-6 mt-3">
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios1" value="0">
                                    <label class="form-check-label" for="exampleRadios1">
                                        تاریخ انتشار پست(همین حالا)
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="receiver" type="radio" name="receiver" id="exampleRadios2" value="1" checked>
                                    <label class="form-check-label" for="exampleRadios2">
                                        انتخاب تاریخ
                                    </label>
                                </div>
                            </div>
                            @if($post->created_at)
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="example-text-input" class="">انتخاب تاریخ</label>
                                    <input name="created_at" class="form-control datepicker date" value="{{\Carbon\Carbon::parse($post->created_at)->toDateString()}}" readonly>
                                </div>
                            @endif
                        </div>
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-info btn-lg ajaxStore">ویرایش مقاله</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('theme/plugins/tags-input/bootstrap-tagsinput.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('change','.receiver',function (){
                var val=$(this).val();
                var elem=$(".date").closest(".form-group");
                if (val==1) {
                    elem.removeClass('d-none');
                }else {
                    elem.addClass('d-none');
                }
            })
        });
        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            });

        $('#tags').tagsinput({
            confirmKeys: [13, 188]
        });

        $('#tags input').on('keypress', function(e){
            if (e.keyCode == 13){
                e.keyCode = 188;
                e.preventDefault();
            }
        });

    </script>
@endsection
