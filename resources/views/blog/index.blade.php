@extends('templates.admin.master_page')
@section('title_browser')
    لیست {{ request()->type == 'amozesh'?'آموزش ها':'خبرها' }}
@endsection
{{--@can('blog_create')--}}
@section('after-title')
    <div class="text-zero top-right-button-container">
        <a href="{{route('posts.create',['type'=>request()->get('type')])}}"
           class="btn btn-primary btn-lg top-right-button mr-1">افزودن جدید</a>
    </div>
@stop
{{--@endcan--}}
@section('content')
    <section id="basic-form-layouts">
        <form method="post" action="{{route('posts.reject')}}">
        @csrf
        <!-- مدال مربوط به عدم تایید پست کاربران -->
            <div id="reject_post_modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="change_pass_modalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="BlockModalLabel">عدم تایید پست کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <input id="reject" type="hidden" value="" name="post_id">
                        <div class="modal-body">
                            <div class="form-group text-left">
                                <textarea name="reject_reason" rows="5" cols="100"
                                          placeholder="دلیل عدم تایید پست را شرح دهید..."></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">کنسل</button>
                            <button type="submit" class="btn btn-info waves-effect waves-light ajaxStore">تایید</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.مدال مربوط به عدم تایید پست کاربران -->
        </form>
        <form method="post" action="{{route('posts.accept')}}">
            @csrf
            <input type="hidden" name="type" value="{{ request()->type }}">
            <!-- مدال مربوط به تایید پست کاربران -->
            <div id="accept_post_modal" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="change_pass_modalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title mt-0" id="BlockModalLabel">عدم تایید پست کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <input id="accept" type="hidden" value="" name="post_id">
                        <h5 class="text-center">این پست تایید شود؟</h5>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">کنسل</button>
                            <button type="submit" class="btn btn-info waves-effect waves-light ajaxStore">تایید</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.مدال مربوط به تایید پست کاربران -->
        </form>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="get" action="">
                            @csrf
                            <div class="row mt-4">
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="title">عنوان</label>
                                    <input name='title' class="form-select form-control"
                                           value="{{ request()->title?request()->title:'' }}">
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label for="category_id">دسته بندی</label>
                                    <select name="category_id" class="form-control select2-single">
                                        <option value="">همه</option>
                                        @foreach($categories as $category)
                                            <option
                                                value="{{$category->id}}" {{ request()->category_id==$category->id?'selected':'' }}>{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                    <label>نویسنده</label>
                                    <input name='creator' class="form-select form-control"
                                           value="{{ request()->creator?request()->creator:'' }}">
                                </div>
                                <div class="form-group col-lg-3 col-md-4 col-sm-6 mt-4 text-left">
                                    <button type="button" class="btn btn-info search-ajax">جست وجو</button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive text-center">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <td>#</td>
                                    <td>عنوان</td>
                                    <td>متن</td>
                                    <td>تگ</td>
                                    <td>دسته بندی</td>
                                    <td>نویسنده</td>
                                    <td>وضعیت</td>
                                    <td>عملیات</td>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($posts)>0)
                                    @foreach($posts as $item)
                                        <tr>
                                            <td>{{ index($posts,$loop) }}</td>
                                            <td>{{$item->title}}</td>
                                            <td>{!! \Illuminate\Support\Str::limit($item->description,20) !!}</td>
                                            @if($item->tags == null)
                                                <td>
                                                    <i class="fa fa-times text-danger"></i>
                                                </td>
                                            @else
                                                <td>
                                                    <i class="fa fa-check text-success"></i>
                                                </td>
                                            @endif
                                            <td>{{$item->category}}</td>
                                            <td>
                                                <a href="{{route('user.show',$item->user_guard->user->id)}}">{{$item->user_guard->user->email}}</a>
                                            </td>
                                            @php
                                                switch ($item->status){
                                                    case "0":
                                                        $status='جدید';
                                                    break;
                                                    case "1":
                                                        $status='تایید شده';
                                                    break;
                                                    case "2":
                                                        $status='رد شده';
                                                    break;
                                                    case "3":
                                                        $status='ویرایش شده';
                                                    break;
        }
                                            @endphp
                                            <td>{{$status}}</td>
                                            <td>
                                                <a href="{{route('posts.edit',$item->id)}}"
                                                   class="btn btn-sm btn-info m-1"><i class="fa fa-edit"></i></a>
                                                <button class="btn btn-sm btn-danger m-1 reject"
                                                        data-post-id="{{$item->id}}" data-target="#reject_post_modal"
                                                        data-toggle="modal" title="رد پست"><i
                                                        class="fa fa-window-close"></i></button>
                                                @if($item->status != 1)
                                                    <button class="btn btn-sm btn-success m-1 accept"
                                                            data-post-id="{{$item->id}}"
                                                            data-target="#accept_post_modal"
                                                            data-toggle="modal" title="تایید پست"><i
                                                            class="fa fa-check"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">مقاله ای جهت نمایش وجود ندارد.</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $posts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('theme/plugins/tags-input/bootstrap-tagsinput.min.js')}}"></script>

    <script>
        $('.reject').on('click', function () {
            $('#reject').val($(this).data('post-id'));
        });
        $('.accept').on('click', function () {
            $('#accept').val($(this).data('post-id'));
        })
        $('.tags').tagsinput({
            typeahead: {
                source: ['Amsterdam', 'Washington']
            }
        });
        // $('#category_id').select2();
    </script>
@endsection
