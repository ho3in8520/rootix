@if(\Illuminate\Support\Facades\Session::has('flash'))
    <div class="register-res-notification register-res-notification-{{ session()->get('flash')['type'] }} alert alert-{{ session()->get('flash')['type'] }} active">
        <span>
            <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 59.873 59.873">
                @if(session()->get('flash')['type'] == 'success')
                    <path
                        id="Icon_metro-notification"
                        data-name="Icon metro-notification"
                        d="M32.507,7.541a24.323,24.323,0,0,0-17.2,41.522,24.323,24.323,0,0,0,34.4-34.4A24.164,24.164,0,0,0,32.507,7.541Zm0-5.613A29.936,29.936,0,1,1,2.571,31.864,29.936,29.936,0,0,1,32.507,1.928ZM28.765,43.09h7.484v7.484H28.765Zm0-29.936h7.484V35.606H28.765Z"
                        transform="translate(-2.571 -1.928)"
                        fill="#00af80"
                    />
                @else
                    <path id="Icon_metro-notification"
                          data-name="Icon metro-notification"
                          d="M32.507,7.541a24.323,24.323,0,0,0-17.2,41.522,24.323,24.323,0,0,0,34.4-34.4A24.164,24.164,0,0,0,32.507,7.541Zm0-5.613A29.936,29.936,0,1,1,2.571,31.864,29.936,29.936,0,0,1,32.507,1.928ZM28.765,43.09h7.484v7.484H28.765Zm0-29.936h7.484V35.606H28.765Z"
                          transform="translate(-2.57 -1.928)" fill="red"/>
                @endif
            </svg>
        </span>

        <span>
            {{ session()->get('flash')['msg'] }}
        </span>
    </div>
@endif
