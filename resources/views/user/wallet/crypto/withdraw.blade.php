@extends('templates.user.master_page')
@section('title_browser','کیف پول-صرافی ارز دیجیتال روتیکس')
@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-title desktop-title mb-3">درخواست برداشت {{ $asset->name_fa }}</h1>
            <div class="panel-box h-auto">
                <div>
                    <div class="d-flex items-center my-1">
                        <span class="icon-Group-1728 font-size__icon"></span>


                        <p class="sell-request-p">
                            برای برداشت موجودی کیف پول‌های خود، درخواست خود را اینجا
                            ثبت نمایید.
                        </p>

                        <button class="create-new-bank withdraw-btn">
                            آمورش برداشت
                        </button>
                    </div>
                </div>
                <form method="post" class="form-withdraw" action="{{ route('withdraw.security') }}">
                    @csrf
                    <input type="hidden" value="{{ $unit }}" name="unit">
                    <div class="row request-row withdraw withdraw-2">
                        <div class="col-12 col-lg-5">
                            <div class="request-boxs">
                                <button type="button"
                                        dir="rtl"
                                        class="
                        request-box request-box-2 request-js-box-2
                        d-flex
                        active
                        enabled
                        justify-between
                        items-center
                      "
                                >

                                    <span class="request-box__title text-right">
                                        تعداد
                                    <br>
                                    <span class="text-danger error-amount"></span>
                                    </span>
                                    <input
                                        type="text"
                                        dir="ltr"
                                        name="amount"
                                        class="currency-value-input"
                                        id="currency-value-input-1"
                                        placeholder="100"
                                    />
                                </button>

                                <div>
                                    <ul class="d-flex items-center wallet-address my-2">
                                        <li class="filter__item">
                                            <button class="filter__btn active" type="button">
                                                <span class="filter__btn-text">TRON</span>
                                            </button>
                                            <input type="hidden" name="network" value="tron">
                                        </li>
                                        <li class="filter__item">
                                            <button class="filter__btn" disabled data-toggle="tooltip" type="button"
                                                    data-placement="top"
                                                    title="به زودی">
                                                <span class="filter__btn-text">ETHEREUM</span>
                                            </button>
                                        </li>
                                        <li class="filter__item">
                                            <button class="filter__btn" disabled data-toggle="tooltip" type="button"
                                                    data-placement="top"
                                                    title="به زودی">
                                                <span class="filter__btn-text">BITCOIN</span>
                                            </button>
                                        </li>
                                        <li class="filter__item">
                                            <button class="filter__btn" disabled data-toggle="tooltip" type="button"
                                                    data-placement="top"
                                                    title="به زودی">
                                                <span class="filter__btn-text">BINANCE</span>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                                <div>
                                    <div>
                                        <button type="button"
                                                dir="rtl"
                                                class="
                            request-box request-box-2 request-js-box-3
                            enabled
                            d-flex
                            justify-center
                            items-center
                          "
                                        >
                                            <span class="request-box__title text-right">
                                       آدرس کیف پول مقصد
                                    <br>
                                    <span class="text-danger error-address"></span>
                                    </span>
                                            <input
                                                class="request-box__title request-box__input" dir="ltr"
                                                name="address"
                                                value=""
                                            />

                                        </button>
                                        <button type="button"
                                                dir="rtl"
                                                class="
                            request-box request-box-2 request-js-box-3
                            d-flex
                            justify-between
                            items-center
                          "
                                        >
                          <span class="request-box__title"
                          >کارمزد انتقال</span
                          >

                                            <input
                                                type="text"
                                                dir="ltr"
                                                disabled
                                                class="currency-value-input w-100"
                                                id="currency-value-input-2"
                                                placeholder="{{ ($fee_currency)?$fee_currency:0 }}"
                                            />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 withdraw__col-responsive">
                            <div class="request-info h-100">
                                <div class="request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>


                                    <div>
                                        <div class="d-flex items-center">
                                            <p class="d-flex items-center">
                                                موجودی قابل برداشت شما :
                                                <span class="green-currency measure-value"
                                                >{{ strtoupper($asset->unit).' '.rial_to_unit($asset->amount,$asset->unit) }} </span>
                                            </p>

                                            <button type="button" class="create-new-bank withdraw-btn select-total"
                                                    data-amount="{{ $asset->amount }}">
                                                انتخاب
                                            </button>
                                        </div>
                                        <p> </p>
                                        <p> </p>
                                    </div>
                                </div>
                                <div class="request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>


                                    <p>نوع شبکه انتقال خود را مشخص کنید.</p>
                                </div>
                                <div class="request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>

                                    <p>
                                        آدرس کیف پول مقصد را وارد کنید توجه: وارد کردن آدرس
                                        نادرست ممکن است منجر به از دست رفتن منابع مالی شما شود.
                                    </p>
                                </div>
                                {{--                                <div class="d-flex items-center mt-5">--}}
                                {{--                                    <div class="ml-3">--}}
                                {{--                                    <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span--}}
                                {{--                                            class="path2"></span></span>--}}
                                {{--                                    </div>--}}

                                {{--                                    --}}{{--                                <p>--}}
                                {{--                                    --}}{{--                                    کارمزد انتقال ارز های دیجیتال در ولت داخلی روتیکس صفر--}}
                                {{--                                    --}}{{--                                    است--}}
                                {{--                                    --}}{{--                                </p>--}}

                                {{--                                    --}}{{--                                <button class="create-new-bank withdraw-btn">--}}
                                {{--                                    --}}{{--                                    مشاهده کارمزد ها--}}
                                {{--                                    --}}{{--                                </button>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <div class="btns-exit">
        <button type="button" class="create-new-bank continue-btn withdraw-security">ادامه</button>
    </div>
    <div class="modal fade text-left security-modal" id="staticBackdrop" data-backdrop="static" tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel4"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel4">تایید تراکنش</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body body-security">

                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/active-withdraw.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/active-selected-address.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.withdraw-security', function () {
                // $(this).attr('disabled', 'disabled')
                let form = $(".form-withdraw");
                form.button = $(this);
                ajax(form, security_html_view);
            })

            function security_html_view(response) {

                if (response.status != 200) {
                    return false;
                }
                let form = response.view;
                let unit = $("input[name='unit']").val();
                let amount = $("input[name='amount']").val();
                let address = $("input[name='address']").val();
                let network = $("input[name='network']").val();
                form = $(form).append("<input type='hidden' value='" + amount + "' name='amount'><input type='hidden' value='" + address + "' name='address'><input type='hidden' value='" + unit + "' name='unit'><input type='hidden' value='" + network + "' name='network'>")
                $(".body-security").html(form)
                $(".security-modal").modal('show');
                $(".modal-withdraw").modal('hide');
            }

            $(document).on("click", '.sendCode', function () {
                var elem = $(this);
                elem.text('در حال ارسال SMS ...');
                var number = $("input[name='number']").val();
                var characterReg = /^(0)?9\d{9}$/;
                if (!characterReg.test(number)) {
                    $('.error-number').text('فرمت شماره موبایل صحیح نیست');
                    $(".sendCode").attr('disabled', true);
                    return;
                }
                const form_obj = {
                    action: '{{ route('withdraw.send-sms') }}',
                    method: 'post',
                    data: {
                        _token: `{{ csrf_token() }}`,
                        number: number,
                        mode: 1,
                    },
                    isObject: true,
                }
                ajax(form_obj, sendCodeCallback, {type: 'send'});
            });

            $(document).on("click", '.resend-btn', function () {
                var number = $("input[name='number']").val();
                var characterReg = /^(0)?9\d{9}$/;
                if (!characterReg.test(number)) {
                    $('.error-number').text('فرمت شماره موبایل صحیح نیست');
                    $(".sendCode").attr('disabled', true);
                    return;
                }
                const form_obj = {
                    action: '{{ route('withdraw.send-sms') }}',
                    method: 'post',
                    data: {
                        _token: `{{ csrf_token() }}`,
                        number: number,
                        mode: 1,
                    },
                    button: $(this),
                    isObject: true,
                }
                ajax(form_obj, sendCodeCallback, {type: 'resend'});
            });

            $(document).on('click', '.button-verify', function () {
                let form = $(this).closest('form');
                form.button = $(this);
                ajax(form, security_html);
            })

            let security_html = function (response) {
                if (response.status == 100) {
                    swalResponse(response.status, response.msg, 'موفق')
                    setTimeout(function () {
                        window.location.href = `{{ route('wallet.index') }}`
                    }, 3000)
                } else {
                    swalResponse(response.status, response.msg, 'خطا')
                }
            }
            let sendCodeCallback = function (response, params) {
                if (response.status == 100) {
                    swal(response.msg, '', 'success');
                    $("input[name='number']").attr('disabled', true);
                    if (params.type == 'send') {
                        $('.sendCode').removeClass("sendCode").addClass("button-verify").text("تایید کد");
                        $('.time-expire').show();
                        $('.resend-btn').attr('disabled', true);
                    } else {
                        $('.resend-btn').text("ارسال مجدد").attr('disabled', true);
                    }
                    time(response.time);
                } else if (response.status == 300) {
                    $('.sendCode').removeClass("sendCode").addClass("button-verify").text("تایید کد");
                    $('.time-expire').show();
                    $('.resend-btn').attr('disabled', true);
                    time(response.time);
                }
            }

            function time(time) {
                var minutes = 1;
                var seconds = minutes * time;

                function convertIntToTime(num) {
                    var mins = Math.floor(num / 60);
                    var secs = num % 60;
                    var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                    return (timerOutput);
                }

                var countdown = setInterval(function () {
                    var current = convertIntToTime(seconds);
                    $('.timer').text(current);

                    if (seconds == 0) {
                        clearInterval(countdown);
                    }
                    seconds--;
                    if (seconds < 0) {
                        $(".resend-btn").remove();
                        $(".bottom-footer-sms").append("<button type='button' class='btn btn-primary resend-btn mr-1'>ارسال مجدد</button>");
                    }
                }, 1000);
            }

            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        })
    </script>
@endsection
