<form method="post" action="{{ route('withdraw.store') }}">
    <input type="hidden" value="{{ $token }}" name="token">
    <div class="row justify-content-center">
        <div class="col-md-7">
            <div class="input-group">
                <label class="m-auto">{{__('attributes.Authentication.phoneNum')}}
                    :</label>
                <fieldset class="form-group position-relative input-divider-right mb-0">
                    <input type="text" class="form-control text-right float-right"
                           id="iconLeft4" name="number" value="{{ $user->mobile }}"
                           placeholder="مثال: 9131234569" readonly>
                </fieldset>
                <span class="input-group-text" id="basic-addon2">98+</span>
            </div>
        </div>
        <div class="col-md-7 row justify-content-center time-expire mt-2" style="display: none">
            <div class="input-group">
                <label class="m-auto">کد تایید: </label>
                <fieldset class="form-group position-relative input-divider-right mb-0">
                    <div class="code-verify">
                        <input class="form-control" name="code">
                    </div>
                </fieldset>
                <div class="input-group-append">
                    <span class="input-group-text timer" id="basic-addon2"></span>
                </div>
            </div>
        </div>
        <div class="col-md-7 row justify-content-center mt-1 bottom-footer-sms">
            <button class="btn btn-success sendCode"
                    type="button">{{__('attributes.Authentication.sendCode')}}
            </button>
        </div>
    </div>
</form>
