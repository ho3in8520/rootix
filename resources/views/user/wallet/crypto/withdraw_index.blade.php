@extends('templates.user.master_page')
@section('title_browser','ليست درخواست برداشت ارز-صرافی ارز دیجیتال روتیکس')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <style>
        table td {
            text-align: right !important;
        }

        table th {
            padding: 20px;
        }
    </style>
@endsection
@section('content')
    <section class="wallet-cart pt-2" id="basic-form-layouts">
        <div class="container">
            <h1 class="dashboard-title bank-title transaction-subtitle desktop-title mb-4">
                لیست درخواست برداشت
            </h1>
            <div>
                <div class="dashboard-table transaction-table-container">
                    <form method="get" action="">
                        <div class="d-flex align-items-end justify-between header-transaction mb-5">
                            <div class="selected-transaction d-flex">
                                <div class="w-100 bg-white">
                                    <span class="mb-2 d-block transaction__filter-title">
                                        شبکه
                                    </span>
                                    <select class="custom-selected" name="network">
                                        <option value="">
                                            همه
                                        </option>
                                        @foreach($networks as $network)
                                            <option value="{{ $network }}" {{ (request()->network == $network)?' selected':'' }}>{{ $network }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="w-100 mr-2 bg-white">
                                    <span class="mb-2 d-block transaction__filter-title">
                                        ارز
                                    </span>
                                    <select class="custom-selected" name="unit">
                                        <option value="">
                                            همه
                                        </option>
                                        @foreach($currencies as $currency)
                                            <option value="{{ $currency->unit }}" {{ (request()->unit == $currency->unit)?' selected':'' }}>{{ $currency->name_fa }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="volume-transaction bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    مقدار
                                </span>
                                <div class="d-flex">
                                    <input type="text" placeholder="مقدار از" name="start_amount" value="{{ request()->start_amount }}"
                                           class="transaction__value-right">
                                    <input type="text" placeholder="مقدار تا" name="end_amount" value="{{ request()->end_amount }}"
                                           class="transaction__value-left">
                                </div>
                            </div>
                            <div class="date-transaction bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    تاریخ
                                </span>
                                <div class="d-flex">
                                    <input type="text" placeholder="از تاریخ" name="start_date" value="{{ request()->start_date }}"
{{--                                           onfocus="(this.type='date')"--}}
                                           class="date-right datePicker">
                                    <input type="text" placeholder="تا تاریخ" name="end_date" value="{{ request()->end_date }}"
{{--                                           onfocus="(this.type='date')"--}}
                                           class="date-left datePicker">
                                </div>
                            </div>
                            <div class="destination-address-input bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    آدرس
                                </span>
                                <input class="address" name="address" type="text" placeholder="آدرس مقصد..." value="{{ request()->address }}">
                            </div>
                            <button class="transaction__btn search-ajax">جستجو</button>
                        </div>
                    </form>
                    <div class="table-loading">
                        <img src="{{ asset('theme/user/images/wallet/spinner.svg') }}" class="table-loding-spinner">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="overflow-auto">
                                <div class="wallet-table-container">
                                    <table class="wallet-table bank-table transaction-table text-right">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>آدرس</th>
                                            <th>مبلغ</th>
                                            <th>کارمزد</th>
                                            <th>ارز</th>
                                            <th>شبکه</th>
                                            <th>وضعیت</th>
                                            <th>تاریخ ایجاد</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($withdraws) > 0)
                                            @foreach($withdraws as $key=>$withdraw)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>

                                                    <td class="">
                                                        <span>{{ $withdraw->address }}</span>
                                                        <a href="#"><img
                                                                src="{{ asset('theme/user/images/Group 1779.svg') }}"
                                                                alt=""></a>
                                                    </td>
                                                    <td>{{ $withdraw->amount }}</td>
                                                    <td>{{ $withdraw->fee }}</td>
                                                    <td>{{ $withdraw->unit }}</td>
                                                    <td>{{ $withdraw->network }}</td>
                                                    <td>{!! $withdraw->status('span',true) !!}</td>
                                                    <td dir="ltr">{{ jdate_from_gregorian($withdraw->created_at,'Y-m-d H:i') }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="100" class="text-center">موردی برای نمایش وجود ندارد</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {{ $withdraws->withQueryString()->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/selected-box.js') }}"></script>
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
