@extends('templates.user.master_page')
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-title desktop-title">
                واریز کیف {{ $asset->name_fa }}
            </h1>

            <div class="panel-box real-box-deposit">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="d-flex items-center mb-1-5">
                            <div>
                                <svg
                                    id="Group_1729"
                                    data-name="Group 1729"
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="25"
                                    height="25"
                                    viewBox="0 0 29.574 29.574"
                                >
                                    <path
                                        id="Path_8613"
                                        data-name="Path 8613"
                                        d="M17.489,18.995H9.938a.753.753,0,0,1,0-1.506h7.523a.753.753,0,0,1,0,1.506Zm3.54-5.018H9.938a.753.753,0,1,1,0-1.506h11.09a.753.753,0,1,1,0,1.506Z"
                                        transform="translate(-0.696 -0.945)"
                                        fill="#f39c12"
                                    />
                                    <path
                                        id="Path_8614"
                                        data-name="Path 8614"
                                        d="M28.826,29.574a.749.749,0,0,1-.749-.749V14.787a13.281,13.281,0,1,0-13.29,13.281H24.815a.753.753,0,0,1,0,1.506H14.787A14.787,14.787,0,1,1,29.574,14.787V28.826a.749.749,0,0,1-.749.749Z"
                                        fill="#222f3e"
                                    />
                                </svg>
                            </div>

                            <p class="sell-request-p">
                                {{ strtoupper($asset->unit) }} موجودی {{ $asset->name_fa }} شما : {{ rial_to_unit($asset->amount,$asset->unit) }}
                            </p>
                        </div>

                        <div class="d-flex items-center mb-1-5">
                            <div>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="25"
                                    height="25"
                                    viewBox="0 0 29.375 29.375"
                                >
                                    <g
                                        id="Group_1728"
                                        data-name="Group 1728"
                                        transform="translate(-8.1 -8.1)"
                                    >
                                        <g
                                            id="Icon-Exclamation"
                                            transform="translate(8.1 8.1)"
                                        >
                                            <path
                                                id="Fill-49"
                                                d="M-205.213-240.525A14.687,14.687,0,0,1-219.9-255.213,14.687,14.687,0,0,1-205.213-269.9a14.687,14.687,0,0,1,14.687,14.687,14.687,14.687,0,0,1-14.687,14.687Zm0-27.777a13.14,13.14,0,0,0-13.09,13.09,13.14,13.14,0,0,0,13.09,13.09,13.14,13.14,0,0,0,13.09-13.09,13.14,13.14,0,0,0-13.09-13.09Z"
                                                transform="translate(219.9 269.9)"
                                                fill="#134563"
                                            />
                                            <path
                                                id="Fill-50"
                                                d="M-197.4-236.1h1.721v1.721H-197.4Z"
                                                transform="translate(211.227 256.871)"
                                                fill="#134563"
                                            />
                                            <path
                                                id="Fill-51"
                                                d="M-196.048-246.532h-.983l-.369-6.944V-258.7h1.721v5.224l-.369,6.944"
                                                transform="translate(211.227 265.583)"
                                                fill="#134563"
                                            />
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <p class="sell-request-p">
                                .آدرس این کیف پول شما در کادر زیر قابل مشاهده است.
                                برای واریز ارزهای دیجیتال به این کیف، می‌توانید از این
                                آدرس استفاده کنید
                            </p>
                        </div>

                        <div>
                            <div class="d-flex items-center mb-1-5">
                                <div>
                                    <svg
                                        id="Group_1729"
                                        data-name="Group 1729"
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="25"
                                        height="25"
                                        viewBox="0 0 29.574 29.574"
                                    >
                                        <path
                                            id="Path_8613"
                                            data-name="Path 8613"
                                            d="M17.489,18.995H9.938a.753.753,0,0,1,0-1.506h7.523a.753.753,0,0,1,0,1.506Zm3.54-5.018H9.938a.753.753,0,1,1,0-1.506h11.09a.753.753,0,1,1,0,1.506Z"
                                            transform="translate(-0.696 -0.945)"
                                            fill="#f39c12"
                                        />
                                        <path
                                            id="Path_8614"
                                            data-name="Path 8614"
                                            d="M28.826,29.574a.749.749,0,0,1-.749-.749V14.787a13.281,13.281,0,1,0-13.29,13.281H24.815a.753.753,0,0,1,0,1.506H14.787A14.787,14.787,0,1,1,29.574,14.787V28.826a.749.749,0,0,1-.749.749Z"
                                            fill="#222f3e"
                                        />
                                    </svg>
                                </div>

                                <p class="sell-request-p">
                                    شبکه انتقال : شبکه انتقال برای ارز خود را مشخص کنید
                                    ، آدرس این کیف پول شما در کادر زیر قابل مشاهده است
                                    <br />
                                    .برای واریز ارزهای دیجیتال به این کیف، می‌توانید از
                                    این آدرس استفاده کنید
                                </p>
                            </div>
                        </div>

                        <div class="d-flex mt-2 right-deposit-container">
                            <div class="d-flex flex-column ml-1">
                                <div>
                                    <div>
                                        <ul class="d-flex items-center wallet-address">
                                            <li class="filter__item">
                                                <button class="filter__btn active">
                                    <span class="filter__btn-text"
                                    >TRON
                                    </span>
                                                </button>
                                            </li>
                                            <li class="filter__item">
                                                <button class="filter__btn" disabled data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="به زودی">
                                    <span class="filter__btn-text"
                                    > ETHEREUM
                                    </span>
                                                </button>
                                            </li>
                                            <li class="filter__item">
                                                <button class="filter__btn" disabled data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="به زودی">
                                    <span class="filter__btn-text"
                                    >BITCOIN
                                    </span>
                                                </button>
                                            </li>
                                            <li class="filter__item">
                                                <button class="filter__btn" disabled data-toggle="tooltip"
                                                        data-placement="top"
                                                        title="به زودی">
                                                    <span class="filter__btn-text">BINANCE</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="real-deposit-amount mt-4">
                                    <h3 class="real-deposit-title mb-2">
                                        آدرس کیف پول
                                    </h3>

                                    <div class="d-flex items-center desired-box">
                                        <div
                                            class="
                                  request-box request-box-2
                                  wallet-box-address
                                  d-flex
                                  active
                                  enabled
                                  justify-between
                                  items-center
                                "
                                        >
                                            <div class="wallet-address-container">
                                  <span
                                      class="
                                      request-box__title
                                      wallet-address-character
                                    "
                                  >{{ $asset->address }}</span
                                  >

                                                <div class="tooltip-address">
                                    <span class="tooltip-address__text">
                                      کپی شد
                                    </span>
                                                </div>
                                            </div>

                                            <button type="button" class="copy-btn">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width="50"
                                                    height="50"
                                                    viewBox="0 0 60 55"
                                                >
                                                    <g
                                                        id="Group_1768"
                                                        data-name="Group 1768"
                                                        transform="translate(-1195 -556)"
                                                    >
                                                        <rect
                                                            id="Input"
                                                            width="60"
                                                            height="55"
                                                            rx="5"
                                                            transform="translate(1195 556)"
                                                            fill="#54e9c3"
                                                        />
                                                        <path
                                                            id="Icon_awesome-copy"
                                                            data-name="Icon awesome-copy"
                                                            d="M15.625,21.875v1.953A1.172,1.172,0,0,1,14.453,25H1.172A1.172,1.172,0,0,1,0,23.828V5.859A1.172,1.172,0,0,1,1.172,4.688H4.688V19.141a2.737,2.737,0,0,0,2.734,2.734Zm0-16.8V0h-8.2A1.172,1.172,0,0,0,6.25,1.172V19.141a1.172,1.172,0,0,0,1.172,1.172H20.7a1.172,1.172,0,0,0,1.172-1.172V6.25H16.8A1.175,1.175,0,0,1,15.625,5.078Zm5.907-1.515L18.312.343A1.172,1.172,0,0,0,17.483,0h-.3V4.688h4.688v-.3a1.172,1.172,0,0,0-.343-.829Z"
                                                            transform="translate(1213.962 571)"
                                                        />
                                                    </g>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                {!!  QrCode::size(190)->generate($address); !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="real-deposit-video">
                            <h3 class="real-deposit-title">آموزش واریز</h3>

                            <video
                                src="#"
                                controls
                            ></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button class="create-new-bank mx-auto mt-3" onclick="window.location.href='{{ route('wallet.index') }}'">
            بازگشت به لیست کیف ها
        </button>
    </section>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/real-deposit.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/copyText.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/active-selected-address.js') }}"></script>

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
