@extends('templates.user.master_page')
@section('title_browser','کیف پول-صرافی ارز دیجیتال روتیکس')
@section('style')
    <style>
        .wallet-table__curency-title-container span {
             text-align: right;
            font-size: 1.3rem;
            display: flex;
            margin-right: 20px;
        }
    </style>
@endsection
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <h1 class="dashboard-title desktop-title">کیف پول</h1>
            <div class="row">
                @foreach([$list_currencies['BTC'],$list_currencies['ETH'],$list_currencies['ADA'],$list_currencies['BNB']] as $item)
                    @php $currency_1d= $item['1d']; @endphp
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="cart-currency-2 cart-currency-3">
                            <div class="cart-currency" style="border-color: {{ $item['color'] }}">
                                <div class="d-flex cart-currency__info justify-between">
                                    <div class="cart-currency__about-price">
                                        <p>{{ num_format($item['price'],contractUnit('usdt')['precision']) }}$</p>
                                        @if (!is_null($currency_1d))
                                            <span
                                                class=" {{ $currency_1d*100 >= 0?'green':'red' }}-currency"
                                                dir="ltr">
                                                {{ $currency_1d > 0 ? '+':''}}{{ num_format($currency_1d,2) }}
                                            </span>
                                        @endif
                                    </div>

                                    <div class="cart-currency__about-name d-flex items-center ">
                                        <div class="cart-currency__names d-flex flex-column items-end">
                                            <h5 class="cart-currency__abbreviation">{{ $item['currency'] }}</h5>
                                            <span class="cart-currency__name">{{ $item['name'] }}</span>
                                        </div>
                                        <img src="{{ $item['logo_url'] }}" alt="{{ $item['name_fa'] }}"
                                             class="cart-currency__img"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="dashboard-chart distance__box">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="panel-box recent-transactions">
                        <div
                            class="d-flex justify-between items-center recent-transactions__title">
                            <h2 class="market-volume-title">تراکنش های اخیر</h2>
                            <a href="{{ route('transactions.index') }}"> مشاهده کامل </a>
                        </div>

                        <div>
                            @foreach($transactions as $item)
                                @php $unit= $item->financeable->unit=='rial'?'RLS':strtoupper($item->financeable->unit) @endphp
                                <div class="d-flex justify-between recent-transactions__content items-center">
                                    <div class="d-flex items-center recent-transactions__desc">
                                        <img src="{{ $list_currencies[$unit]['logo_url'] }}"
                                             class="recent-transactions__img"
                                             alt="{{ $list_currencies[$unit]['name_fa'] }}"/>
                                        <div>
                                            <h5>{{ $item->financeable->name }}</h5>
                                            <span style="white-space: nowrap;">
                                                کیف {{ $unit=='RLS'?'ریال':'ارز' }}-{{ $item->type=='1'?'برداشت':'واریز' }}
                                            </span>
                                        </div>
                                    </div>

                                    <div
                                        class="recent-transactions__name d-flex flex-column items-end" style="overflow: overlay; margin-right: 8px">
                                        <h5 style="white-space: nowrap;">{{ rial_to_unit($item->amount,$unit,true) }}</h5>
                                        @if($unit != 'RLS')
                                            <span style="white-space: nowrap;">{{ convert_currency($unit,'rls',$item->amount,true,$list_currencies) }}</span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-9">
                    <div class="panel-box wallet-asset distance__box-responsive h-auto">
                        <div class="assets d-flex">
                            <div style="{{ $data['total_asset_usdt'] == 0?'width:100%':'' }}">
                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        کل دارایی شما
                                    </h2>
                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">{{ convertFa(num_format($data['total_asset_rial'],contractUnit('rls')['precision']))." ".get_unit() }}</h2>

                                        <div class="assets-value">
                                            {{--                                            <span class="plus change-percents">+12.03</span>--}}
                                            <p>{{ strtoupper(rial_to_unit($data['total_asset_usdt'],'usdt',true)) }}</p>
                                        </div>
                                    </div>
                                </div>

                                <hr class="assets-line"/>

                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        دارایی موجود در کیف ریالی
                                    </h2>

                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">{{ convertFa(rial_to_unit($data['rial_asset'],'rls',true)) }}</h2>

                                        <div class="assets-value">
                                            <p>{{ strtoupper(rial_to_unit($data['rial_asset_usdt'],'usdt',true)) }}</p>
                                        </div>
                                    </div>
                                </div>

                                <hr class="assets-line"/>

                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        دارایی موجود در کیف ارز
                                    </h2>

                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">{{ convertFa(num_format($data['currency_asset_rial'],contractUnit('rls')['precision']))." ".get_unit() }}</h2>

                                        <div class="assets-value">
                                            <p>{{ strtoupper(rial_to_unit($data['currency_asset_usdt'],'usdt',true)) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($data['total_asset_usdt'] > 0)
                                <div class="mr-auto">
                                    <div class="wallet-chart" data-chart="4/45%">
                                        <canvas id="assets-chart"></canvas>
                                    </div>

                                    <div class="assets-chart__info-container">
                                        <div class="assets-chart__info"></div>

                                        <hr class="chart-assets-line"/>

                                        <div class="total-chart">
                                            <span class="total-chart__title-2"> جمع کل: </span>

                                            <span class="total-chart__percent-2"> %100 </span>
                                        </div>

                                        <hr class="chart-assets-line"/>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="distance__box">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                {{--                    <div class="panel-box red-chart-box">--}}
                {{--                        <h2 class="market-volume-title">نمودار</h2>--}}

                {{--                        <div class="red-chart-container" style="height: 310px;">--}}
                <!-- TradingView Widget BEGIN -->
                    <div class="tradingview-widget-container">
                        <div class="tradingview-widget-container__widget"></div>
                        <div style="pointer-events: none">
                            <script type="text/javascript"
                                    src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                    async>
                                {
                                    "symbol"
                                :
                                    "BTCSTUSDT",
                                        "width"
                                :
                                    "100%",
                                        "height"
                                :
                                    "100%",
                                        "locale"
                                :
                                    "en",
                                        "dateRange"
                                :
                                    "12M",
                                        "colorTheme"
                                :
                                    "light",
                                        "trendLineColor"
                                :
                                    "rgba(41, 98, 255, 1)",
                                        "underLineColor"
                                :
                                    "rgba(41, 98, 255, 0.3)",
                                        "underLineBottomColor"
                                :
                                    "rgba(41, 98, 255, 0)",
                                        "isTransparent"
                                :
                                    false,
                                        "autosize"
                                :
                                    true,
                                        "largeChartUrl"
                                :
                                    ""
                                }
                            </script>
                        </div>
                    </div>
                    <!-- TradingView Widget END -->
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>

                <div class="col-12 col-md-6 col-lg-5">
                    <div class="panel-box distance__box-responsive-768">
                        <h2 class="market-volume-title">دارایی رمز ارز</h2>
                        @if (count($crypto_chart) > 0)
                            <div class="wallet-chart-2-container">
                                <div
                                    class="doughnut-chart doughnut-chart-2 wallet-chart-2"
                                    dir="ltr"
                                    style="--crypto-chart-color: {{ $property_crypto_chart>0?'#3cceef':'#ef3c3c' }}"
                                    user-property=" {{ $property_crypto_chart>0?"+":'' }}{{ num_format( ($property_crypto_chart/count($crypto_chart)) ,2) }}%">
                                    <canvas id="wallet-assets-chart"></canvas>
                                    <p class="wallet-assets-chart-title">میزان تغییرات کلی</p>
                                </div>
                            </div>
                        @else
                            <div class="wallet-chart-2-container text-dark">
                                <span>دارایی رمز ارز شما صفر میباشد</span>
                            </div>
                        @endif

                    </div>
                </div>

                <div class="col-12 col-lg-3">
                    <div class="left-wallet-chart distance__box-responsive">
                        {{--                        <div class="panel-box">--}}
                        {{--                            <h2 class="left-wallet-chart-title">BTC Hashribon</h2>--}}

                        <div>
                            <div class="left-wallet-chart-self-top-container">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container" style="pointer-events: none">
                                    <script type="text/javascript"
                                            src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                            async>
                                        {
                                            "symbol"
                                        :
                                            "USDT.D",
                                                "width"
                                        :
                                            "100%",
                                                "height"
                                        :
                                            "100%",
                                                "locale"
                                        :
                                            "en",
                                                "dateRange"
                                        :
                                            "12M",
                                                "colorTheme"
                                        :
                                            "light",
                                                "trendLineColor"
                                        :
                                            "rgb(44,143,3)",
                                                "underLineColor"
                                        :
                                            "rgba(44, 143, 3, 0.3)",
                                                "underLineBottomColor"
                                        :
                                            "rgba(44, 143, 3, 0)",
                                                "isTransparent"
                                        :
                                            false,
                                                "autosize"
                                        :
                                            true,
                                                "largeChartUrl"
                                        :
                                            ""
                                        }
                                    </script>
                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        </div>
                        {{--                        </div>--}}
                        {{--                        <div class="panel-box">--}}
                        {{--                            <h2 class="left-wallet-chart-title">BTC Hashribon</h2>--}}
                        <div>
                            <div class="left-wallet-chart-self-top-container">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container" style="pointer-events: none">
                                    <script type="text/javascript"
                                            src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                            async>
                                        {
                                            "symbol"
                                        :
                                            "BTC.D",
                                                "width"
                                        :
                                            "100%",
                                                "height"
                                        :
                                            "100%",
                                                "locale"
                                        :
                                            "en",
                                                "dateRange"
                                        :
                                            "12M",
                                                "colorTheme"
                                        :
                                            "light",
                                                "trendLineColor"
                                        :
                                            "rgb(211,93,0)",
                                                "underLineColor"
                                        :
                                            "rgba(211,93,0, 0.3)",
                                                "underLineBottomColor"
                                        :
                                            "rgba(211,93,0, 0)",
                                                "isTransparent"
                                        :
                                            false,
                                                "autosize"
                                        :
                                            true,
                                                "largeChartUrl"
                                        :
                                            ""
                                        }
                                    </script>
                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        </div>
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="distance__box">
        <div class="container">
            <div id="wallet-list">
                <x-wallet-list :assets="$assets" :list-currencies="$list_currencies"
                               :fields="['global-price','inventory','24h','deposit','withdraw']"/>
            </div>
        </div>
    </section>
@endsection
@section('script')

    @if ($data['total_asset_usdt'] > 0)
        <script>
            const chart_data = {
                assets_chart: {
                    'rial': '{{ $data['rial_asset_usdt'] * 100/$data['total_asset_usdt'] }}',
                    'usdt': '{{ $data['currency_asset_usdt'] * 100/$data['total_asset_usdt'] }}',
                },
                crypto_chart: {
                    label: [],
                    data: [],
                    backgroundColor: [],
                }
            }
        </script>
    @endif
    @foreach ($crypto_chart as $item):
    <script>
        chart_data["crypto_chart"]['label'].push("{{ $item->unit }}");
        chart_data["crypto_chart"]['data'].push({{ convert_currency($item->unit,'usdt',$item->amount,false,$list_currencies) }});
        chart_data["crypto_chart"]['backgroundColor'].push("{{ $list_currencies[strtoupper($item->unit)]["color"] }}");
    </script>
    @endforeach
    <script src="{{ asset('theme/user/scripts/getTableDataRefresh.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/wallet-charts.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/filter-table.js') }}"></script>

    <!-- Refresh Wallet -->
    <script>
        const refresh_wallet = function (response, params) {
            if (response)
                $("#wallet-list").html(response);
        }
        $("#wallet-list").delegate(".table__refresh-btn", 'click', function (e) {
            $(this).parents('.dashboard-table').addClass('active');
            e.preventDefault();
            const form = {
                action: '{{ route('wallet.refresh') }}',
                method: 'post',
                data: {
                    _token: '{{ csrf_token() }}',
                    fields: ['global-price', 'inventory', '24h', 'deposit', 'withdraw'],
                },
                isObject: true,
            };
            ajax(form, refresh_wallet);
        });
    </script>
    <!--## Refresh Wallet -->
@endsection
