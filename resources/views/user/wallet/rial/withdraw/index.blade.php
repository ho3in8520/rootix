@extends('templates.user.master_page')
@section('title_browser','ليست درخواست برداشت ریالی-صرافی ارز دیجیتال روتیکس')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <style>
        table td {
            text-align: right !important;
        }

        table th {
            padding: 20px;
        }
    </style>
@endsection
@section('content')
    <section class="wallet-cart pt-2" id="basic-form-layouts">
        <div class="container">
            <h1 class="dashboard-title bank-title transaction-subtitle desktop-title mb-4">
                لیست درخواست برداشت ریالی
            </h1>
            <div>
                <div class="dashboard-table transaction-table-container">
                    <form method="get" action="">
                        <div class="d-flex align-items-end header-transaction mb-5">
                            <div class="selected-transaction d-flex">
                                <div class="w-100 bg-white">
                                    <span class="mb-2 d-block transaction__filter-title">
                                        نوع
                                    </span>
                                    <select class="custom-selected" name="deposit_type">
                                        <option value="" {{ request()->deposit_type=='' ?' selected':'' }}>همه</option>
                                        <option value="0" {{ request()->deposit_type=='0' ?' selected':'' }}>شتاب</option>
                                        <option value="1" {{ request()->deposit_type=='1' ?' selected':'' }}>پایا</option>
                                        <option value="2" {{ request()->deposit_type=='2' ?' selected':'' }}>ساتنا</option>
                                    </select>
                                </div>
                                <div class="w-100 mr-2 bg-white">
                                    <span class="mb-2 d-block transaction__filter-title">
                                        وضعیت
                                    </span>
                                    <select class="custom-selected" name="status">
                                        <option value="" {{ request()->status=='' ?' selected':'' }}>همه</option>
                                        <option value="0" {{ request()->status=='0' ?' selected':'' }}>در حال بررسی</option>
                                        <option value="1" {{ request()->status=='1' ?' selected':'' }}>واریز شده</option>
                                        <option value="2" {{ request()->status=='2' ?' selected':'' }}>رد شده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="destination-address-input bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    کد پیگیری
                                </span>
                                <input class="address" name="tracking_code" type="text" placeholder="کد پیگیری" value="{{ request()->tracking_code }}">
                            </div>
                            <div class="volume-transaction bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    مقدار
                                </span>
                                <div class="d-flex">
                                    <input type="text" placeholder="مقدار از" name="start_amount"
                                           value="{{ request()->start_amount }}"
                                           class="transaction__value-right">
                                    <input type="text" placeholder="مقدار تا" name="end_amount"
                                           value="{{ request()->end_amount }}"
                                           class="transaction__value-left">
                                </div>
                            </div>
                            <div class="date-transaction bg-white">
                                <span class="mb-2 d-block transaction__filter-title">
                                    تاریخ واریز
                                </span>
                                <div class="d-flex">
                                    <input type="text" placeholder="از تاریخ" name="start_deposit_date"
                                           value="{{ request()->start_deposit_date }}"
                                           class="date-right datePicker">
                                    <input type="text" placeholder="تا تاریخ" name="end_deposit_date"
                                           value="{{ request()->end_deposit_date }}"
                                           class="date-left datePicker">
                                </div>
                            </div>
                            <button class="transaction__btn search-ajax">جستجو</button>
                        </div>
                    </form>
                    <div class="table-loading">
                        <img src="{{ asset('theme/user/images/wallet/spinner.svg') }}" class="table-loding-spinner">
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="overflow-auto">
                                <div class="wallet-table-container">
                                    <table class="wallet-table bank-table transaction-table text-right">
                                        <thead>
                                        <tr>
                                            <th>ردیف</th>
                                            <th>مبلغ</th>
                                            <th>کارمزد</th>
                                            <th>کد پیگیری</th>
                                            <th>نوع</th>
                                            <th>وضعیت</th>
                                            <th>تاریخ واریز</th>
                                            <th>تاریخ ایجاد</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($withdraws) > 0)
                                            @foreach($withdraws as $key=>$withdraw)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ rial_to_unit($withdraw->amount,'rls',true) }}</td>
                                                    <td>{{ rial_to_unit($withdraw->fee,'rls',true) }}</td>
                                                    <td>{{ $withdraw->tracking_code }}</td>
                                                    <td>{{ $withdraw->deposit_type }}</td>
                                                    <td>
                                                        <span
                                                            class="badge badge-{{ $withdraw->status_info['color'] }}">{{ $withdraw->status_info['text'] }}</span>
                                                        @if ($withdraw->status == 2)
                                                            <span
                                                                class='attention animate__animated animate__flash animate__infinite infinite animate__slower 3s'
                                                                data-toggle='tooltip'
                                                                title='{{ $withdraw->admin_des }}'>!</span>
                                                        @endif
                                                    </td>
                                                    <td dir="ltr">{{ $withdraw->status==1 ? jdate_from_gregorian($withdraw->deposit_date,'Y-m-d H:i'):"واریز نشده" }}</td>
                                                    <td dir="ltr">{{ jdate_from_gregorian($withdraw->created_at,'Y-m-d H:i') }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="100" class="text-center">موردی برای نمایش وجود ندارد</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {{ $withdraws->withQueryString()->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/selected-box.js') }}"></script>
    <script src="{{ asset('general/js/popup.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
