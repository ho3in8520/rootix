@extends('templates.user.master_page')
@section('title_browser','درخواست برداشت ریال-صرافی ارز دیجیتال روتیکس')
@section('content')
    @if ($banks->count() > 0)
        <section class="pt-2">
            <div class="container">
                <h1 class="dashboard-title bank-title desktop-title mb-3">درخواست برداشت ریالی</h1>
                <div class="panel-box h-auto">
                    <div>
                        <div class="d-flex items-center my-1">
                            <span class="icon-Group-1728 font-size__icon"></span>
                            <p class="sell-request-p">
                                .برای برداشت موجودی کیف پول‌های خود، درخواست خود را اینجا ثبت نمایید
                            </p>
                            <button class="create-new-bank withdraw-btn">
                                آمورش برداشت
                            </button>
                        </div>

                        <div class="d-flex items-center my-1">
                        <span class="icon-Group-1745 font-size__icon">
                            <span class="path1"></span>
                            <span class="path2"></span>
                        </span>
                            <p class="sell-request-p">
                                .لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک
                                است
                            </p>
                        </div>
                    </div>

                    <div class="row request-row withdraw">
                        <div class="col-12 col-lg-4">
                            <div class="request-boxs">
                                <button dir="rtl"
                                        class="request-box request-box-2 request-js-box-2 d-flex enabled justify-between items-center">
                                <span class="request-box__title text-right">مبلغ برداشت ({{ get_unit() }}) <br>
                                <span class="text-danger error-amount"></span>
                                </span>
                                    <input type="text" dir="ltr" name="amount" class="currency-value-input"
                                           onkeyup="$(this).val(numberFormat($(this).val()))"
                                           id="currency-value-input-1" placeholder="100">
                                </button>

                                <button
                                    dir="rtl"
                                    class="request-box request-box-2 request-box-3 enabled request-js-box-4 d-flex justify-between items-center">
                                    <span class="request-box__title text-right">
                                        حساب بانکی
                                        <br>
                                        <span class="text-danger error-bank_id"></span>
                                    </span>
                                    <div class="d-flex items-center">
                                        <div class="volume-form volume-form-2 w-auto">
                                            <div class="volume-form__selected volume-form__selected-2">
                                                <div
                                                    class="volume-form__selected-desc-container d-flex items-center justify-between">
                                                    <div
                                                        class="volume-form__selected-desc volume-form__selected-desc-2  justify-between">
                                                        <span class="volume-form__selected-title">کارت بانکی خود را انتخاب کنید</span>
                                                        <div class="arr">
                                                            <svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="13"
                                                                height="10"
                                                                viewBox="0 0 18 11.115"
                                                            >
                                                                <path
                                                                    id="Icon_material-expand-more"
                                                                    data-name="Icon material-expand-more"
                                                                    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                    transform="translate(-9 -12.885)"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                                <ul class="volume-form__menu mr7-select banks-select">
                                                    @foreach($banks as $bank)
                                                        <li class="volume-form__item" data-val="{{ $bank->id }}">
                                                            <a href="#" class="volume-form__link volume-form__link-2">
                                                        <span class="volume-form__selected-title">
                                                            {{ $bank->name }} ({{ $bank->card_number }})
                                                        </span>
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="0"
                                                                    height="0"
                                                                    viewBox="0 0 18 11.115"
                                                                >
                                                                    <path
                                                                        id="Icon_material-expand-more"
                                                                        data-name="Icon material-expand-more"
                                                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                        transform="translate(-9 -12.885)"
                                                                    />
                                                                </svg>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </button>

                                <div dir="rtl"
                                        class="request-box request-box-2 request-js-box-3 d-flex justify-between items-center">

                                    <span class="request-box__title">مبلغ نهایی</span>
                                    <input type="text" dir="rtl" disabled class="currency-value-input text-left"
                                           id="currency-value-input-2" value="--">
                                </div>


                            </div>

                        </div>
                        <div class="col-12 col-lg-8">

                            <div class="request-info h-100">
                                <div class="request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </span>

                                    <div>
                                        <div class="d-flex items-center">
                                            <p>موجودی قابل برداشت شما :
                                                <span
                                                    class="green-currency measure-value">{{ rial_to_unit($rial_asset->amount,'rls',true) }}</span>
                                            </p>
                                            <button class="create-new-bank withdraw-btn total-inventory"
                                                    data-inventory="{{ rial_to_unit($rial_asset->amount,'rls',false) }}">
                                                انتخاب
                                            </button>
                                        </div>
                                        <p class="">مجموع برداشت روزانه:
                                            {{ rial_to_unit($today_withdraw,'rls',true) }} از
                                            {{ max_withdraw_daily()==-1 ? 'نامحدود' :rial_to_unit(max_withdraw_daily(),'rls',true) }}
                                        </p>
                                        <p class="">
                                            سطح شما:
                                            {{ convertStepToPersian(auth()->user()->step_complate) }}
                                        </p>
                                    </div>
                                </div>
                                <div class="request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </span>

                                    <p>.می‌توانید هر یک از حساب‌های بانکی تایید شده‌ی خود را برای دریافت وجه انتخاب
                                        نمایید</p>

                                    <a href="{{ route('banks.index') }}" class="create-new-bank withdraw-btn">
                                        افزودن حساب
                                    </a>
                                </div>
                                <div class="request-info__desc withdraw-request-info__desc d-flex items-center">
                                <span class="icon-Group-1745 font-size__icon">
                                    <span class="path1"></span>
                                    <span class="path2"></span>
                                </span>
                                    <p class="">
                                        کارمزد برداشت
                                        {{ rial_to_unit($fee_withdraw,'rls',true) }}
                                        جهت انجام عملیات بانکی است
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="btns-exit">
            <button class="create-new-bank continue-btn submit-request">
                ثبت درخواست
            </button>
        </div>
    @else
        <section class="pt-2">
            <div class="row">
                <div class="card col-12">
                    <div class="card-body">
                        <div class="alert alert-warning">
                            لطفا ابتدا از بخش حساب های بانکی، حساب بانکی مورد تایید برای خود ثبت کرده و بعد از تایید شدن
                            توسط مدیریت
                            اقدام به ثبت درخواست برداشت کنید
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/sell-request-selected-1.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/just-get-number.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/toggleSelectedBox.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/withdraw-real.js') }}"></script>
{{--    <script src="{{ asset('theme/user/scripts/show-profile.js') }}"></script>--}}
    <script>
        let fee_withdraw = '{{ rial_to_unit($fee_withdraw,'rls') }}' * 1;
        let min_withdraw = '{{ rial_to_unit($min_withdraw,'rls') }}' * 1;
        let inventory = '{{ rial_to_unit($rial_asset->amount,'rls') }}' * 1;
        let max_daily_withdraw = '{{rial_to_unit( max_withdraw_daily(),'rls') }}';
        let today_withdraw = '{{ rial_to_unit($today_withdraw,'rls') }}';
        let today_max = max_daily_withdraw - today_withdraw;


        $(".total-inventory").click(function () {
            let inventory = $(".total-inventory").data('inventory');
            $("#currency-value-input-1").val(inventory);
            $("#currency-value-input-1").keyup();
        });

        $("#currency-value-input-1").keyup(function () {

            // $(".submit-request").attr('disabled', true);
            $(".error-amount").hide();
            $("#currency-value-input-2").val('--');

            let amount = de_numberFormat($(this).val());

            if (!amount)
                return false;

            let check_amount = true;
            if (amount > inventory) {
                check_amount = false;
                $(".error-amount").show().text('مقدار بیشتر از موجودی میباشد');
            } else if (max_daily_withdraw !={{rial_to_unit(-1,'rls')}} && amount > today_max) {
                check_amount = false;
                $(".error-amount").show().text('مقدار بیشتر از حداکثر امروز شما میباشد');
            } else if (amount < min_withdraw) {
                $(".error-amount").show().text(`حداقل مقدار برداشت ${rial_to_unit(min_withdraw * 10, 'rls', true)} میباشد`);
                check_amount = false;
            }
            if (check_amount === true) {
                // $(".submit-request").removeAttr('disabled');
                $("#currency-value-input-2").val(numberFormat(amount - fee_withdraw));
            }
        })

        $(".submit-request").click(function () {
            let amount = de_numberFormat($("#currency-value-input-1").val());
            let bank_id = $(".banks-select li.selected").data('val');
            let form_data = {
                isObject: true,
                action: '{{ route('wallet.withdraw.rial.store') }}',
                method: 'post',
                button: $(this),
                data: {
                    amount: amount,
                    bank_id: bank_id
                }
            }
            ajax(form_data);
        })
    </script>
@endsection
