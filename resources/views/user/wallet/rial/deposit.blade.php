@extends('templates.user.master_page')
@section('title_browser','واریز ریالی-صرافی ارز دیجیتال روتیکس')
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-title desktop-title">درخواست واریز ریالی</h1>

            <div class="panel-box real-box-deposit">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="d-flex items-center my-1">
                            <span class="icon-Group-1728 font-size__icon"></span>

                            <p class="sell-request-p">
                                .لطفا پیش از واریز وجه، توضیحات زیر را به دقت مطالعه
                                نمایید. مسئولیت مشکلات ناشی از عدم توجه به این موارد
                                بر عهده‌ی مشتری خواهد بود
                            </p>
                        </div>

                        <div class="d-flex items-center my-1">
                            <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                    class="path2"></span></span>

                            <p class="sell-request-p">
                                جهت افزایش اعتبار کیف پول ریالی خود با استفاده از کارت های بانکی عضو شبکه شتاب و از طریق
                                دیگاه پرداخت اینترنتی اقدام نمایید
                            </p>
                        </div>

                        <div>
                            <div class="d-flex items-center my-1">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>

                                <p class="sell-request-p">
                                    :در هنگام پرداخت به نکات زیر دقت نمایید
                                </p>
                            </div>

                            <ul class="deposit-list">
                                <li class="sell-request-p">
                                    حتماً به آدرس صفحه‌ی درگاه بانکی دقت نموده و تنها پس
                                    از اطمینان از حضور در سایت‌های . سامانه‌ی شاپرک
                                    مشخصات کارت بانکی خود را وارد نمایید
                                </li>

                                <li class="sell-request-p">
                                    .در صفحه درگاه دقت کنید که حتما مبلغ نمایش داده شده
                                    درست باشد
                                </li>

                                <li class="sell-request-p">
                                    .در تعیین مبلغ واریز به این موضوع دقت نمایید که
                                    حداقل مبلغ واریز ده هزار تومان میباشد.
                                </li>
                            </ul>
                        </div>

                        <div>
                            <h3 class="real-deposit-title">واریز سریع</h3>
                            <div class="selected-boxs fast-charge-cointaner">
                                <button class="selected-boxs__box fast-charge-item">
                                    <span class="selected-boxs__title">مبلغ واریز</span>
                                    <div class="selected-boxs__price">
                                        <span class="selected-boxs__price fast-amount"
                                              data-amount="{{ rial_to_unit('500000','rls',false) }}">{{ rial_to_unit('500000','rls',true) }}</span>
                                    </div>
                                </button>
                                <button class="selected-boxs__box fast-charge-item">
                                    <span class="selected-boxs__title">مبلغ واریز</span>
                                    <div class="selected-boxs__price">
                                        <span class="selected-boxs__price fast-amount"
                                              data-amount="{{ rial_to_unit('1000000','rls',false) }}">{{ rial_to_unit('1000000','rls',true) }}</span>
                                    </div>
                                </button>
                                <button class="selected-boxs__box fast-charge-item">
                                    <span class="selected-boxs__title">مبلغ واریز</span>
                                    <div class="selected-boxs__price">
                                        <span class="selected-boxs__price fast-amount"
                                              data-amount="{{ rial_to_unit('2000000','rls',false) }}">{{ rial_to_unit('2000000','rls',true) }}</span>
                                    </div>
                                </button>
                            </div>
                        </div>

                        <div class="real-deposit-amount">
                            <h3 class="real-deposit-title">واریز دلخواه</h3>
                            <form method="post" action="{{ route('payment') }}">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>

                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @csrf
                                <div class="d-flex items-center desired-box">
                                    <div
                                        class=" request-box request-box-2 d-flex active enabled justify-between items-center">
                                        <span class="request-box__title">مبلغ واریز ({{ get_unit() }})</span>
                                        <input type="text" dir="ltr" name="amount" class="currency-value-input"
                                               id="currency-value-input-1" placeholder="100"/>
                                        <input type="hidden" name="gateway" value="zarinpal">
                                        <input type="hidden" name="mode" value="asset">
                                    </div>
                                    <button type="submit" class="create-new-bank" id="deposit-btn" disabled>واریز</button>
                                    <div class="symbols gateways">
                                    <span class="symbols__symbol item-gateway active" data-gateway="zarinpal">
                                        <img
                                            class="symbols__img"
                                            src="{{ asset('theme/user/images/real-deposit/icon-128x128.png') }}"
                                            alt="زرین پال"
                                        />
                                    </span>
                                        <span class="symbols__symbol item-gateway" data-gateway="idpay">
                                        <img
                                            src="{{ asset('theme/user/images/real-deposit/icon-180.png') }}"
                                            alt="آی دی پی"
                                        />
                                        <p>آی دی پی</p>
                                    </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="real-deposit-video">
                            <h3 class="real-deposit-title">آموزش واریزی ریالی</h3>
                            <video src="" controls></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        // وقتی روی باکس های پرداخت سریع کلیک کرد
        $(".fast-charge-item").click(function () {
            let amount = $(this).find('.fast-amount').data('amount');
            $("#currency-value-input-1").val(de_numberFormat(amount)).keyup();
            $(this).addClass('active').siblings('.fast-charge-item').removeClass('active');
        })

        // وقتی یکی از درگاه هارو انتخاب کرد
        $(".gateways .item-gateway").click(function () {
            $(this).addClass('active').siblings('.gateways .item-gateway').removeClass('active');
            $("input[name=gateway]").val($(this).data('gateway'));
        });

        // وقتی مبلیغی وارد میکنه
        $("#currency-value-input-1").keyup(function () {
            $(".fast-charge-item").removeClass('active');
            $("#deposit-btn").removeAttr('disabled');
            let amount = $(this).val();
            $("#currency-value-input-1").val(numberFormat($(this).val()));
            if (!amount) {
                $("#deposit-btn").attr('disabled', true);
            }
        });
    </script>
@endsection
