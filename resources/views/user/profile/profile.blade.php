@extends('templates.user.master_page')
@section('title_browser')
    پروفایل-صرافی ارز دیجیتال روتیکس
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('theme/user/styles/circular-upload.scss')}}">
@endsection
@section('content')

    <section class="profile-cart ">
        <div class="container">
            <h1 class="profile-title desktop-title color-232323">پروفایل کاربر</h1>
            <div class="row justify-center">
                <div class="col-12 col-xl-8 col-lg-9 position-relative bg-light">
                    <div class="upload-container">
                        {{--                        <div class="progress1" data-percentage="50">--}}
                        {{--                                    <span class="progress1-left">--}}
                        {{--                                    <span class="progress1-bar"></span>--}}
                        {{--                            </span>--}}
                        {{--                            <span class="progress1-right">--}}
                        {{--			                    <span class="progress1-bar"></span>--}}
                        {{--		                    </span>--}}
                        {{--                            <div class="progress1-value">--}}
                        {{--                                <div>--}}
                        {{--                                    74%<br>--}}
                        {{--                                    <span>completed</span>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>

                    <div id="avatar-div" class="">

                        <img src="{{ auth()->user()->avatar }}" width="230" height="230" style="cursor: pointer"
                             class="profile-img rounded-circle" alt="user-avatar" id="open-upload-avatar"
                             title="تغییر عکس پروفایل">
                        <button type="button" id="open-upload-avatar-2" class="position-absolute profile-img-edit"
                                title="تغییر عکس پروفایل">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="50" height="50" viewBox="0 0 150 150">
                                <image width="150" height="150"
                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAYAAABxLuKEAAAGNElEQVR4nO2cXYhVVRTH/2vtqxnYh0glCBnThBQ+DYQpOhWpMX40FIQPE1hRLz71IET01oNv0VskUSo9aBZhUjMWhGQ5fUj6IpFkU5GINtQYBmqevVasfc6dGd333rl3PHe89575MXu4nL3P2Wv97z5n77v23of6x4ZRi1LiAckKMOCdg0s8lAkKgCyJQjh8gh1lr/COACKQl1CWROcoU5fzulKJlgLaTYplAG4G4ADcktVyAYAHcFEJJwA6RaonvaNhEh1RpitWnzoGNK3LEJfWzyKRbb7k4PwkPxwhsfNrUKqelQuLCHiKRB8kRS+8dgV9zTGqevlbyx9IsZQ0LetSAUZU9DCAowp8BOBsdHZO5N5iADUn+pSwmRT9AG6PLpoP55XwMRTvAxjKu8XUzm0Isqb9DIt+A2CQFFuaKIpxu9VBVhdwGIrNUYnrIC9hHmGRYSf6HikeinKbz2onutdsMFturDDpXbOIBDtZcIgUK6IyM4zZEGwR7DTbUP051hxhlMju1/VO9DipPlvjQTrjmC1mk9lmNgZbZ0IYTVvKdif6qfU6UYHWYVGwUbF9Ol9cQ8IowC6Rfaz6ika5rYfZaLaazdqgr40UvpMFw+z16SinxTGbSfAlaGKMlJcwC0OXCCy/ngfaDYPC3ypSHAFhYT1m1CPMTaT4HMDSKKf9WJb5MuXXO7UwiiEAPdHxdkXRA9HPpiWMdXGsijmXkzdI8Whb3j7VCN051ppvHH6HVXauojABQT8BL0XHO4Tgm4TfchUplbyMHycdH6fcTaJ7q6nZCZhvzsteVr0XhDNRi/HMKKcrJYZQiJ28BWBex6oywTyIvm1xHcjVidNhUDmF/+ug6Isu0aEQsB6KtUjjauOJ7QFUTs6Lc4ns6KiHbT0o3lS7axxDOE18zW00wIp7Wt+TfGFFtwADiQWwOE1pr0Qh01rMy53jbmM4L6+aBpSOksEW+rPkEtnAggdaKYQwU5jPLLjfJdJX1iNtMRpio1uKIEItSPT5rA8CO2stogtIsamIraVMGuDCRtPCxnbZyFc3FWTcMhWmwUbrncI4hgSbpjihEFA6nOsTSifcrNXc8EB2K5DFi3vnJDaqAVlIYXHRRZnEYiXqYRXtnFhLTpgmDEddNj88S0rQwlEXs9fuTg4vNEoI0nnttgfvkvYyfUZYYsLUFTUvGAtNmLlFV6ECc02Y+fHxwjPfhEmKrkIFEhPmYny88FycFaYyl0yY8xWzis0ogzCC2YHvBOmqxhFrMb9EmbP8YbMmvxduuqQWYfEpfmUwHatRrJgwHbMInglzuuhaTOK0acJI10sfjbKLi2khFsGDMg4WXY0yqRblmUjQPgCXo1LF41KmBdgiVqRqg7yhIiuSrQ36xDOd9zZ3bXMoYZbf0Y6odMFQpnfLQxe2DVeWkhIftI1TVMBRsPksjBO+xENhS4+1GDtYFkOYX4vOKgjme5ChPHdtk0zlOWvv6ANP9FORWo35aj6b78gm3cLqB9tTOJ5s6oCwtUh3U/CVsJV1kg6WnBeMp0RApIeUMRhdoUMxX81n832yFvZbCVeldHXeixN3W2frEnylazQYX2p2LYQzyjTQ0dJo6J4HKq3xRc2V4cAeJbwTHe0QMt/2VPOmujAUtuG+AOB4lNfmKPBD8K1GHKq6MBlKeBjAj1FG+3IMNPVO23r2K11Q4DEAJ6Oc9uOkEtYB+DcPYYyzCvS2edzm+8yHv6KcCjSyJ/JPZfQqwg6xtkIJg8pYaT7Ua3dj24sVl6TEjwvR6zWeWy2D2Wi2iuMN2RtG6mZ6O/UJ2zyTVXYuymsdzgUbCdumY9H0hEknpQY9U48S7WqlH51pwIl2mW1m43QHqdf30oswQsZzwlilhK+j/BlGCV8IY4XZVG1EWy95vQ3kiDCv9ky2Wf2rKLfJKHBYgX5hWgPg2zxqy/H9MaEFfWhdogJPKoUX3oxGZfJj1OowQYAwCD2Q58Wb9Sqm/cK0nxQLSPUJAGtIsQqYtElM69gWHpf5zVqEEg4q0QEljJXfUZU3TX1HFQFjAO0Wxm4SnZu9vGu5ErrBdAeJ3gfgtmy5213ZaX8D+AfAmDr6GaqjpDjlHX1HghGC/tdMmwMA/gd49I1rJAuSHAAAAABJRU5ErkJggg=="/>
                                <image x="32" y="34" width="87" height="83"
                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACkAAAAnCAYAAACSamGGAAACn0lEQVRYhcWYS2xNQRjHfx4LIRGJR6lq61JSTdhIJBY2HlHxLomVjb0Nm9pbsWIlkVhYiAgRXLpBCLES4lHcetT7VZuKRxBkYk76ne9LnXPSmXP/yUnuvfM7M//7fTPfnDPUUV1AD/AJ+AbcB/YC0+ppSuoo8GeY6yOw2NxRsk4oc8+AW8BX8dtLYFK9DB5XBneJtg6gJtoOmrvrYLDLELAQ+CEiPN4QJRrcbIghVQXXZlojSc/B/xl0ulq2yZM5Uiy1QrC9wBhDBNapggabgdeC32OIwDqtDG7K6H4W8ErwNwwROYIbDZFWk6+LCf8UaDFURIMbDGENvhD8E5/2aNKLJI/BfhXBJkMFlI7guoyum32xTnhndoahAkovkvUZXc9WEXwONBoqoM4UTHFFzUH3eaahAuosxSJYUXWwP7bBc8rgWkOk5VL8Vq3iqItER3BNBt+qDNZiG9RzMMtgi0pxLXaKixpsVVtdX+wyo1PcaYi05qit7nHsF6xqwQi2qQi6naTBUAF1oaDBecCbMg32FEyxM/hepbhUg6sNkZZL8QfBP4q9SHSKVxkirbnKoHvsn26ogDqvDK7M6LqiUtwbO8WHCxp0EXwn+AfAVEMFlJs/n/1gv3MYnK+2OjcHJxsqsJaLAY9ldN2uIliKQad9YtAdpnVIHWqR9JURwUSy5Cwxrf+0ABgQ3ENgiqEiqcEfYrqBbw8zRLs/7EwM3ou9SLQ6xeCHRFujf2fe7w82E+Zu2QbHAkvF99HAbmALsAgYp/g7fpENmJ4iahRwBViWY4iLwLYMg9uBbuA78NO0FpM7rJoIHHB3DaointTKmx7YmvNp2mXFzVXd10ivQdfxEWCnT+Vl4DpwzRfrIvoFXPJl6ov/oyORm3oTgOpfP/E6hsKW4CMAAAAASUVORK5CYII="/>
                            </svg>
                        </button>
                    </div>
                    <div id="uploadStatus"></div>
                    <form id="avatar-form" action="{{route('profile.update.picture',auth()->user())}}"
                          enctype="multipart/form-data" method="POST">
                        @csrf
                        <input type="file" name="file" style="display: none" id="avatar">
                    </form>
                    <div class="cart-currency pr-lg--5">
                        <div class="row mt-4 mb-3">
                            <div class="col-md-6 d-flex align-items-center">
                                <h4 class=" bolder">{{auth()->user()->fullname ?? 'بدون نام'}}</h4>
                                {{--                                <a href="#" class="color-f66b64 mx-3">ویرایش</a>--}}
                            </div>
                            {{--                            <div class="col-md-6 d-flex align-items-center justify-end">--}}
                            {{--                                        <span>--}}
                            {{--                                            <svg xmlns="http://www.w3.org/2000/svg" class=" mr-1" width="25"--}}
                            {{--                                                 height="29.53" viewBox="0 0 31 29.53">--}}
                            {{--                                                <path data-name="Icon feather-star"--}}
                            {{--                                                      d="m18 3 4.635 9.39L33 13.9l-7.5 7.3 1.77 10.32L18 26.655 8.73 31.53l1.77-10.32L3 13.9l10.365-1.51z"--}}
                            {{--                                                      transform="translate(-2.5 -2.5)"--}}
                            {{--                                                      style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round" />--}}
                            {{--                                            </svg>--}}
                            {{--                                            <svg xmlns="http://www.w3.org/2000/svg" class=" mr-1" width="25"--}}
                            {{--                                                 height="29.53" viewBox="0 0 31 29.53">--}}
                            {{--                                                <path data-name="Icon feather-star"--}}
                            {{--                                                      d="m18 3 4.635 9.39L33 13.9l-7.5 7.3 1.77 10.32L18 26.655 8.73 31.53l1.77-10.32L3 13.9l10.365-1.51z"--}}
                            {{--                                                      transform="translate(-2.5 -2.5)"--}}
                            {{--                                                      style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round" />--}}
                            {{--                                            </svg>--}}
                            {{--                                            <svg xmlns="http://www.w3.org/2000/svg" class=" mr-1" width="25"--}}
                            {{--                                                 height="29.53" viewBox="0 0 31 29.53">--}}
                            {{--                                                <path data-name="Icon feather-star"--}}
                            {{--                                                      d="m18 3 4.635 9.39L33 13.9l-7.5 7.3 1.77 10.32L18 26.655 8.73 31.53l1.77-10.32L3 13.9l10.365-1.51z"--}}
                            {{--                                                      transform="translate(-2.5 -2.5)"--}}
                            {{--                                                      style="fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round" />--}}
                            {{--                                            </svg>--}}
                            {{--                                            <svg xmlns="http://www.w3.org/2000/svg" class=" mr-1" width="25"--}}
                            {{--                                                 height="28.53" viewBox="0 0 30 28.53">--}}
                            {{--                                                <path data-name="Icon feather-star"--}}
                            {{--                                                      d="m18 3 4.635 9.39L33 13.9l-7.5 7.3 1.77 10.32L18 26.655 8.73 31.53l1.77-10.32L3 13.9l10.365-1.51z"--}}
                            {{--                                                      transform="translate(-3 -3)" style="fill:#f8bf1a" />--}}
                            {{--                                            </svg>--}}
                            {{--                                            <svg xmlns="http://www.w3.org/2000/svg" class=" mr-1" width="25"--}}
                            {{--                                                 height="28.53" viewBox="0 0 30 28.53">--}}
                            {{--                                                <path data-name="Icon feather-star"--}}
                            {{--                                                      d="m18 3 4.635 9.39L33 13.9l-7.5 7.3 1.77 10.32L18 26.655 8.73 31.53l1.77-10.32L3 13.9l10.365-1.51z"--}}
                            {{--                                                      transform="translate(-3 -3)" style="fill:#f8bf1a" />--}}
                            {{--                                            </svg>--}}
                            {{--                                        </span>--}}

                            {{--                            </div>--}}
                        </div>
                        <span class="line-w"></span>
                        <div class="row mt-4 mb-3">
                            <div class="col-12">
                                <div class="row mb-2">
                                    <label for="#" class="col-6 col-md-3 color-4a4a4a">کد کاربری شما :</label>
                                    <p class=" col-md-3 col-6 color-707070">{{auth()->user()->code}}</p>
                                </div>
                                <div class="row mb-2">
                                    <label for="#" class="col-6 col-md-3 color-4a4a4a">سطح کاربری شما :</label>
                                    <p class=" col-md-3 col-6 color-707070">
                                        سطح {{convertStepToPersian(auth()->user()->step_complate)}}</p>
                                </div>
                                <div class="row">
                                    @can('authenticate_btn')
                                        @if(auth()->user()->is_complete_steps ==0)
                                            <a href="{{ route('step.verify-mobile') }}"
                                               class="btn p-0 text-white btnsize1 bg-f66b64 text-center radius-5">
                                                احراز هویت
                                            </a>
                                        @endif
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="profile-cart2 ">
        <div class="container">
            <div class="row justify-center">
                <div class="col-12 col-xl-8 col-lg-9">
                    <div class="cart-currency px-md-4">
                        <div class="row my-3">
                            <div class="col-12 d-flex align-items-center">
                                <h6 class=" bolder color-232323">پروفایل کاربر</h6>
                            </div>
                        </div>
                        <span class="line-w"></span>
                        {{--                        <form action="#" class="row my-3 align-items-center justify-center">--}}
                        <div class="row my-3 align-items-center justify-center">
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">نام و نام خانوادگی</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->fullname}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">کد ملی</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->national_code}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">کد معرف</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       value="{{auth()->user()->referral_id}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">لینک معرفی به دوستان</label>
                                <div class="d-flex align-items-center">
                                    <input type="text" id="referral-link"
                                           class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3 border-left-0 rounded-0 radius-input"
                                           placeholder="..." aria-describedby="basic-addon2"
                                           value=" {{ route('register.form',['referral_code' => auth()->user()->code]) }}"
                                           disabled>
                                    <div class="input-group-append shadow-none mb-3 border-right-0 radius-label">
                                        <span class="btn border-right-0 bg-54e9c3 rounded-0 radius-label copy-btn"
                                              data-input="referral-link"
                                              style="height: 39px"><i
                                                class="fa fa-copy mx-0 text-dark"></i></span>
                                        {{--                                                <span--}}
                                        {{--                                                    class="input-group-text border-right-0 bg-54e9c3 rounded-0 radius-label"--}}
                                        {{--                                                    id="basic-addon2">--}}
                                        {{--                                                    <button type="button" class="copy-btn">--}}
                                        {{--                                                        <svg xmlns="http://www.w3.org/2000/svg" width="21.875"--}}
                                        {{--                                                             height="25" viewBox="0 0 21.875 25">--}}
                                        {{--                                                            <path id="Icon_awesome-copy" data-name="Icon awesome-copy"--}}
                                        {{--                                                                  d="M15.625,21.875v1.953A1.172,1.172,0,0,1,14.453,25H1.172A1.172,1.172,0,0,1,0,23.828V5.859A1.172,1.172,0,0,1,1.172,4.688H4.688V19.141a2.737,2.737,0,0,0,2.734,2.734Zm0-16.8V0h-8.2A1.172,1.172,0,0,0,6.25,1.172V19.141a1.172,1.172,0,0,0,1.172,1.172H20.7a1.172,1.172,0,0,0,1.172-1.172V6.25H16.8A1.175,1.175,0,0,1,15.625,5.078Zm5.907-1.515L18.312.343A1.172,1.172,0,0,0,17.483,0h-.3V4.688h4.688v-.3a1.172,1.172,0,0,0-.343-.829Z"/>--}}
                                        {{--                                                        </svg>--}}
                                        {{--                                                    </button>--}}
                                        {{--                                                </span>--}}
                                    </div>

                                </div>
                            </div>
                            <form action="{{route('profile.update.about',auth()->user()->id)}}" method="post">
                                @csrf
                                <div class="col-sm-12 mb-2 ">
                                    <label class="color-777 mb-1">درباره من</label>
                                    <textarea name="about" type="text"
                                              class="form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                              rows="3"
                                              cols="100">{{auth()->user()->about}}</textarea>
                                </div>
                                <button type="button"
                                        class="btn form-control shadow-none bg-2f31d0 btnsize2 p-0 mx-auto ajaxStore">
                                    ثبت
                                    تغییرات
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="profile-cart2 ">
        <div class="container">
            <div class="row justify-center">
                <div class="col-12 col-xl-8 col-lg-9">
                    <div class="cart-currency px-md-4">
                        <div class="row my-3">
                            <div class="col-12 d-flex align-items-center">
                                <h6 class=" bolder color-232323">اطلاعات شخصی</h6>
                            </div>
                        </div>
                        <span class="line-w"></span>
                        <form action="#" class="row my-3 align-items-center justify-center">
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">تاریخ تولد</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->birth_day}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">تلفن ثابت</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->phone}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">تلفن همراه</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->mobile}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">کد پستی</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..." value="{{auth()->user()->postal_code}}" disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">شهر</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..."
                                       value="{{auth()->user()->city_name?auth()->user()->city_name->name:''}}"
                                       disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">ایمیل</label>
                                <input type="text"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"
                                       placeholder="..."
                                       value="{{auth()->user()->email}}"
                                       disabled>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1 d-block">جنسیت</label>
                                <div class="row justify-start align-items-center h-40">
                                    <div
                                        class="form-check form-check-inline p-0 m-0 col-md-3 col-4 justify-start align-items-center">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                               id="inlineRadio1" value="false"
                                               @if(auth()->user()->gender_id == 1 ) checked
                                               @else disabled @endif>
                                        <label class="form-check-label" for="inlineRadio1"> مذکر</label>
                                    </div>
                                    <div
                                        class="form-check form-check-inline p-0 m-0 col-md-3 col-4 justify-start align-items-center">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                               id="inlineRadio2" value="false"
                                               @if(auth()->user()->gender_id == 2 ) checked
                                               @else disabled @endif>
                                        <label class="form-check-label" for="inlineRadio2">مونث</label>
                                    </div>
                                    <div
                                        class="form-check form-check-inline p-0 m-0 col-md-4 col-4 justify-start align-items-center">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                               id="inlineRadio3" value="false"
                                               @if(empty(auth()->user()->gender_id)  ) checked
                                               @else disabled @endif>
                                        <label class="form-check-label" for="inlineRadio3">هیچکدام</label>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                {{--                                <label for="#" class="color-777 mb-1">ایمیل</label>--}}
                                {{--                                <input type="text"--}}
                                {{--                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3"--}}
                                {{--                                       placeholder="..."--}}
                                {{--                                       value="{{auth()->user()->email}}"--}}
                                {{--                                       disabled>--}}
                            </div>
                            {{--                            <button class=" btn form-control shadow-none bg-2f31d0 btnsize2 p-0">ثبت تغییرات</button>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="profile-cart2 ">
        <div class="container">
            <div class="row justify-center">
                <div class="col-12 col-xl-8 col-lg-9">
                    <div class="cart-currency px-md-4">
                        <div class="row my-3">
                            <div class="col-12 d-flex align-items-center">
                                <h6 class=" bolder color-232323">تغییر رمز عبور</h6>
                            </div>
                        </div>
                        <span class="line-w"></span>
                        <form method="post" action="{{ route('change_pass.store') }}"
                              class="row my-3 align-items-center justify-center">
                            @csrf
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">رمز عبور فعلی</label>
                                <input type="password" name="old_pass"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3">
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">رمز عبور جدید</label>
                                <input type="password" name="password"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3">
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                <label for="#" class="color-777 mb-1">تکرار رمز عبور جدید</label>
                                <input type="password" name="password_confirmation"
                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3">
                            </div>
                            <div class="col-sm-6 mb-2 ">
                                {{--                                <label for="#" class="color-777 mb-1">تکرار رمز عبور جدید</label>--}}
                                {{--                                <input type="password" name="password_confirmation"--}}
                                {{--                                       class=" form-control bg-f8f8fa border-e2e6eb shadow-none mb-3">--}}
                            </div>
                            @can('change_password')
                                <button type="button"
                                        class="btn form-control shadow-none bg-2f31d0 btnsize2 p-0 ajaxStore">
                                    ثبت تغییرات
                                </button>
                            @endcan
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="profile-cart2 ">
        <div class="container">
            <div class="row justify-center">
                <div class="col-12 col-xl-8 col-lg-9">
                    <div class="cart-currency px-md-4">
                        <div class="row my-3">
                            <div class="col-12 d-flex align-items-center">
                                <h6 class=" bolder color-232323">نشست های فعال</h6>
                            </div>
                        </div>
                        <span class="line-w"></span>
                        @isset($devices)
                            @foreach ($devices as $device)
                                <div class="row my-3 align-items-center justify-between">
                                    <div class="col-md-4">
                                        <div class="d-flex justify-start align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" width="49.995"
                                                 height="49.995" viewBox="0 0 150 150">
                                                <image width="150" height="150"
                                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAEQ0lEQVRogd1aT4hVVRz+vt95hrQII7JFC2MsDAXNDFuEYNQi0IqcgRJiFrYKily0a1e0iFZW2EqjjCxwQEhqFRlmUEwFwZiVgQqtRhjbxID3ni9+593ne3bee7xxfM69fcxl3ju/c8/v+975c3/n/C6fXvgOvRABi0LrSvRvKFshWRnV/g8gkqCUrhgIxmS6j9LDALYDWE/hfgC3AlhdNb8I4B8RZwH8CeAHkd8D+ENGWNluJJrBJKi6yW2OUJQAiaJliOb+r6GNFq4TLpjCBkZMWtSzADaP0hKFie7nxOaXCHwGYAbAb9kNI8Ku875tFvUhhbNW6s1RRQzAZm/D24JwyNvuX204lirkDkgHQxlnGTGdSpjVWTqqNgjs87bdR/I1DiFUGkJnKLx4wwQMgPuofE32r5FjJCGMetuijgFYmxnHh7Xu032P4mHgZK/WKFoRZwg8ozH2wCBUK+iripoQOTmMwuAeYfo75SIy200GgT0UvtYQt32FVEvrlwAeyYwrh50UTgwaGaZqGKkrAK0rehfAE1ntlccu5+YcnWsvdwtR6Fz+RLUiTlmpl2ooIsG5WRH3JK49V6vTU64wFFpjpT5V3wFXDzi3UOoooLsAXL4qMLItwuMnCe97WFNfGRWIWxRxME3+agVIIZl/DqW2hqjnBk2musGEvdFsS7HKUATzHiFKlyO93gwJXTDqDecuEubjKZS6x0rtbkpvoPOwLPVkKLTONZjvPUIZnx9n7DQ2+AJVxum04oa0QcLeBspIoOB7IZ8zvBfAxqxGc7BJwITPkZ0NFpEQSj3mIcrWzNIwROIBX3jXN12IazAK6zJLw+AaPKq6relCXIP1nDs1GautG3Y1GilgX/wfCFl0IX9nxc3DgoE433gZxAXfJTZeiISL3iM/Z5amgfjJQJ5svhCe9Ml+bjnH+TXA74TOWZWj+KSpKkQcKX2r69mfGOyjrEYTIKAMdsQ1mJ9AFC2ej+Tx/6az6gznGo3HY+AFpMMHtk8hylX2WpNiFeeaOFf8zZOQrSL60coZER9nd9QUztU5O3fXwKn5Uz0quYZR88PyJjVBIeOdhLpHpp2u8QvEZZHT9e8NTldcr3LPj6sNR0V8kJXXBCIOO8ecdj+Q+wB808ey0vgK5Av9OPQX0l4VHgVwOjOsHE6LeHyQ94FC2kEldgj4IrPcZAj4XMSOYV6HCWmLCdwl8sBKPCxTis14QIFPQdCwTflIuSkZ9kfjFID5zDg+zLtPEftH8TBykk3ETDRuFPFeZrzBEPFO5Wtm1JaXmi28BPLlMtiDAg5n1uXjUDRuEflK8rUEcPLSt3ltH4vV+1kK7Jb1vK9lMXrRJpK7GeXD7qGsndEwK+MxCSdMmvNI9hp0Xrgpe5KFfXI5yw1F5kTMgXxLxAYK2yFtA3F3dRR7e3V5zy/4ryziIoS/QP4IaBbArylTtpzFBMC/fFu3MH5LwdkAAAAASUVORK5CYII="/>
                                                <image x="42" y="42" width="66" height="66"
                                                       xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAWCAYAAADEtGw7AAABNklEQVQ4jdXVuUpDQRSA4S8xaLBQVNz3vRFEfABfwMrGB7cTXAoLtVJBUYxcOTCCySQ316XQH4aBM2cdzpypFUXhmwxjBluYxDbWsIKxRqbeznha1zjCHuaxjFVMoJ5ZITKe+BQpMtjBNDaxhDsc4Cyz7s19ZHyC2Z4qPOIVFynLStT7OA2eMkkFwvH9dwz70fXi/7zjWiZt5+O8n95natFuVV5ItON5Ji2hkRq/ibcuagN4wA0OMdJDr9PmuZEaP/ZumUf5L2jhEkM99DptWlWuopXmwWlKoBL1VGoZV8n5VYlOJw/1CqUVHXsViv/5QEYz6S85vs2k7TQzSQWifXaxgQWsp9aaSvtcCj74lVkclP154TACxIohf4z9NL8jgcWya/zJZxrPO/6/qDYqiwoj2AZG3gEkpT266KgCIwAAAABJRU5ErkJggg=="/>
                                            </svg>
                                            <div class=" mr-2">
                                                <p class=" d-block color-4a4a4a">{{ $device->user_agent }}</p>
                                                <small
                                                    class="color-707070 d-inline-block">{{ $device->ip_address }}</small>
                                                @if ($current_session_id == $device->id)
                                                    <small class="color-707070 d-inline-block"> ( نشست فعلی )</small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="d-flex justify-end align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20.427" height="20.432"
                                                 viewBox="0 0 20.427 20.432">
                                                <g id="Icon_ionic-ios-timer" data-name="Icon ionic-ios-timer"
                                                   transform="translate(-3.938 -3.938)">
                                                    <path id="Path_8608" data-name="Path 8608"
                                                          d="M14.153,24.369A10.216,10.216,0,0,1,7.064,6.8.82.82,0,0,1,8.2,7.978a8.574,8.574,0,1,0,6.768-2.36v3.2a.822.822,0,1,1-1.645,0V4.76a.822.822,0,0,1,.822-.822,10.216,10.216,0,0,1,.005,20.432Z"
                                                          transform="translate(0 0)"/>
                                                    <path id="Path_8609" data-name="Path 8609"
                                                          d="M12.063,11.348,17.145,15a1.538,1.538,0,1,1-1.788,2.5A1.485,1.485,0,0,1,15,17.145l-3.652-5.082a.512.512,0,0,1,.715-.715Z"
                                                          transform="translate(-2.001 -2.001)"/>
                                                </g>
                                            </svg>
                                            <div class=" mr-2">
                                                <small class="color-707070 d-inline-block">آخرین مشاهده :</small>
                                                <small
                                                    class="color-707070 d-inline-block">{{ Carbon\Carbon::parse($device->last_activity)->diffForHumans() }}</small>
                                                @if ($current_session_id != $device->id)
                                                    <a href="/logout/{{$device->device_id}}" class=" text-left"><small
                                                            class="color-f66b64 d-block">حذف
                                                            نشست</small></a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#avatar").change(function () {
                let form = new FormData($("#avatar-form")[0]);
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                $("#avatar-div").addClass('d-none');
                                $(".progress").removeClass('d-none');
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                $('.upload-container').append(`<div class="progress1" data-percentage="${percentComplete}">
                                    <span class="progress1-left">
                                    <span class="progress1-bar"></span>
                            </span>
                                <span class="progress1-right">
			                    <span class="progress1-bar"></span>
		                    </span>
                                <div class="progress1-value">
                                    <div>
                                        درحال <br>
                                        <span>آپلود</span>
                                    </div>
                                </div>
                            </div>`)
                            }
                        }, false);
                        return xhr;
                    },
                    url: httpToHttps("{{route('profile.update.picture',auth()->user())}}"),
                    type: "POST",
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,

                    success: function (data) {
                        $("#avatar-div").removeClass('d-none');
                        $(".progress1").addClass('d-none');
                        window.location.href = window.location.href;
                    },
                    error: function (error) {
                        let data = JSON.parse(error.responseText);
                        if (data.errors.file[0]){
                            $("#avatar-div").removeClass('d-none');
                            $(".progress1").addClass('d-none');
                            swal('ناموفق', data.errors.file[0], 'error').then(function () {
                                    window.location.href = window.location.href;
                                }
                            );
                        }
                    }

                });
            });

            $('#open-upload-avatar').click(function () {
                $('#avatar').trigger('click');
            });
            $('#open-upload-avatar-2').click(function () {
                $('#avatar').trigger('click');
            });
        })


        function set_authenticator(secret) {
            $.ajax({
                url: httpToHttps("{{route('complete-registration')}}"),
                type: 'POST',
                data: {secret: secret, "_token": csrf},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    if (data.status == 100) {
                        swal('موفق', data['msg'], 'success');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    } else {
                        swal('ناموفق', data['msg'], 'error');
                        setTimeout(function () {
                            location.reload();
                        }, 5000)
                    }
                }
            });
        }
    </script>
@endsection
