@extends('templates.user.master_page')
@section('title_browser')
    خرید و فروش {{strtoupper($markets['des'])}}-صرافی ارز دیجیتال روتیکس
@endsection
@section('content')
    <section class="mt-1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-3 px-8">
                    <div class="panel-box panel-box__padding-0 h-1027 overflow-y-auto position-relative">
                        <div class="d-flex justify-center align-items-center h-100 spinner">
                            <div class="spinner-border text-primary mx-auto" role="status">
                            </div>
                        </div>
                        <div class="tread-tabs__btns panel-box__padding-1">
                            <ul>
                                @can('usdt_list')
                                    <li>
                                        <button
                                            class="tread-tabs-markets {{ ($markets['source']=='usdt')?'active':'' }}"
                                            data-action="{{route('exchange.index',['des_unit'=>'btc','source_unit'=>'usdt'])}}">
                                            USDT
                                        </button>
                                    </li>
                                @endcan
                                @can('rial_list')
                                    <li>
                                        <button
                                            class="tread-tabs-markets {{ ($markets['source']=='rial')?'active':'' }}"
                                            data-action="{{route('exchange.index',['des_unit'=>'btc','source_unit'=>'rial'])}}">{{ get_unit('rial','en') }}</button>
                                    </li>
                                @endcan
                            </ul>
                        </div>
                        <div class="tread-tabs">
                            <div class="tread-tabs__content tread-tabs__content-2 currency-list">
                                <table class="tread-table ticker" data-url="{{ route('exchange.list-markets') }}">
                                    <thead>
                                    <tr>
                                        <th>نماد</th>
                                        <th>مارکت</th>
                                        <th>قیمت</th>
                                    </tr>
                                    </thead>

                                    <tbody class="ticker-tbody">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 px-8">
                    <div class="main-chart mb15">
                        <!-- TradingView Widget BEGIN -->
                        <div id="tv_chart_container">
                            <div id="tradingview_e8053"></div>
                            <script src="https://s3.tradingview.com/tv.js"></script>
                            <script>
                                new TradingView.widget({
                                    width: "100%",
                                    height: 550,
                                    symbol: "{{ strtoupper(implode('',$markets)) }}",
                                    interval: "D",
                                    timezone: "Etc/UTC",
                                    theme: "Light",
                                    style: "1",
                                    locale: "en",
                                    toolbar_bg: "#f1f3f6",
                                    enable_publishing: false,
                                    withdateranges: true,
                                    hide_side_toolbar: false,
                                    allow_symbol_change: true,
                                    show_popup_button: true,
                                    popup_width: "1000",
                                    popup_height: "650",
                                    container_id: "tradingview_e8053",
                                });
                            </script>
                        </div>
                        <!-- TradingView Widget END -->
                    </div>

                    <div class="panel-box panel-box__padding-0 my-1 h-auto">
                        <div class="order">
                            <div class="order__header">
                                <ul class="order__menu">
                                    <li class="order__menu__item">
                                        <button class="order__menu__btn">قیمت دلخواه</button>
                                    </li>
                                    {{--                                    <li class="order__menu__item">--}}
                                    {{--                                        <button class="order__menu__btn">بازار</button>--}}
                                    {{--                                    </li>--}}
                                    {{--                                    <li class="order__menu__item">--}}
                                    {{--                                        <button class="order__menu__btn">توقف بازار</button>--}}
                                    {{--                                    </li>--}}
                                    {{--                                    <li class="order__menu__item">--}}
                                    {{--                                        <button class="order__menu__btn">توقف اندازه</button>--}}
                                    {{--                                    </li>--}}
                                </ul>
                            </div>

                            <div class="d-flex items-center justify-between flex-wrap order__body">
                                <div class="order__card order__card-buy">
                                    <div
                                        class="d-flex align-items-end justify-content-between market-caption">
                                        <p>آخرین قیمت :</p>
                                        <span style="cursor: pointer" class="last-price"></span>
                                    </div>
                                    <form action="{{route('exchange.new-trade')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="type" value="buy">
                                        <input type="hidden" name="unit" value="{{$markets['source']}}">
                                        <input type="hidden" name="market"
                                               value="{{$markets['des']}}/{{$markets['source']}}">
                                        <div class="input-group-container">
                                            <div class="input-group">
                                                <input type="text" name="amount" placeholder="مقدار">
                                                <div>
                                                    <span>{{strtoupper($markets['des'])}}</span>
                                                </div>
                                            </div>
                                            <p class="error error-amount"></p>
                                        </div>

                                        <div class="input-group-container">
                                            <div class="input-group">
                                                <input type="text" id="source_amount" name="price" placeholder="قیمت">
                                                <div>
                                                    <span>{{($markets['source']=='rial')?'Toman':strtoupper($markets['source'])}}</span>
                                                </div>
                                            </div>
                                            <p class="error error-price"></p>
                                        </div>

                                        <ul id="buy" class="market-trade-list">
                                            <li>
                                                <button type="button">25%</button>
                                            </li>
                                            <li>
                                                <button type="button">50%</button>
                                            </li>
                                            <li>
                                                <button type="button">75%</button>
                                            </li>
                                            <li>
                                                <button type="button">100%</button>
                                            </li>
                                        </ul>
                                        <div>
                                            <div
                                                class="d-flex align-items-center justify-content-between market-caption">
                                                <p>دارایی موجود:</p>
                                                <span>{{strtoupper($markets['des'])}} = {{$markets_value['des_amount']}}</span>
                                            </div>
                                            <div
                                                class="d-flex align-items-center justify-content-between market-caption">
                                                <p>دارایی موجود:</p>
                                                <span>{{strtoupper($markets['source'])}} = {{number_format(rial_to_unit($markets_value['source_amount'],$markets['source']))}}</span>
                                            </div>
                                            <div id="buy_box"
                                                 class="d-flex align-items-center justify-content-between market-caption bolder">
                                                <p>مبلغ نهایی:</p>
                                                <span id="total_buy" dir="ltr"></span>
                                            </div>
                                        </div>

                                        @can($markets['source'].'_trade_buy')
                                            <button class="order__btn buy ajaxStore">خرید</button>
                                        @endcan
                                    </form>
                                </div>

                                <div class="order__card order__card-sell">
                                    <div
                                        class="d-flex align-items-end justify-content-between market-caption">
                                        <p>آخرین قیمت :</p>
                                        <span style="cursor: pointer" class="last-price"></span>
                                    </div>
                                    <form action="{{route('exchange.new-trade')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="type" value="sell">
                                        <input type="hidden" name="unit" value="{{$markets['source']}}">
                                        <input type="hidden" name="des_asset" value="{{$markets['des']}}">
                                        <input type="hidden" name="market"
                                               value="{{$markets['des']}}/{{$markets['source']}}">
                                        <div class="input-group-container">
                                            <div class="input-group">
                                                <input type="text" name="sell_price" placeholder="قیمت">
                                                <div>
                                                    <span>{{($markets['source']=='rial')?'Toman':strtoupper($markets['source'])}}</span>
                                                </div>
                                            </div>
                                            <p class="error error-sell_price"></p>
                                        </div>

                                        <div class="input-group-container">
                                            <div class="input-group">
                                                <input type="text" id="des_amount" name="sell_amount"
                                                       placeholder="مقدار">
                                                <div>
                                                    <span>{{strtoupper($markets['des'])}}</span>
                                                </div>
                                            </div>
                                            <p class="error error-sell_amount"></p>
                                        </div>

                                        <ul id="sell" class="market-trade-list">
                                            <li>
                                                <button type="button">25%</button>
                                            </li>
                                            <li>
                                                <button type="button">50%</button>
                                            </li>
                                            <li>
                                                <button type="button">75%</button>
                                            </li>
                                            <li>
                                                <button type="button">100%</button>
                                            </li>
                                        </ul>
                                        <div>
                                            <div
                                                class="d-flex align-items-center justify-content-between market-caption">
                                                <p>دارایی موجود:</p>
                                                <span>{{strtoupper($markets['des'])}} = {{$markets_value['des_amount']}}</span>
                                            </div>
                                            <div
                                                class="d-flex align-items-center justify-content-between market-caption">

                                                <p>دارایی موجود:</p>
                                                <span>{{strtoupper($markets['source'])}} = {{number_format(rial_to_unit($markets_value['source_amount'],$markets['source']))}}</span>
                                            </div>
                                            <div id="sell_box"
                                                 class="d-flex align-items-center justify-content-between market-caption bolder">
                                                <p>مبلغ نهایی:</p>
                                                <span id="total_sell" dir="ltr"></span>
                                            </div>
                                        </div>

                                        @can($markets['source'].'_trade_sell')
                                            <button class="order__btn sell ajaxStore">فروش</button>
                                        @endcan
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-3 px-8">
                    <div class="panel-box panel-box__padding-0 h-550 overflow-y-auto">
                        <div class="tread-tabs">
                            <div class="tread-tabs__btns panel-box__padding-1 list-request"
                                 data-url="{{ route('exchange.list-request') }}">
                                سفارش های باز
                            </div>
                            <div class="tread-tabs__content position-relative">
                                <div class="d-flex justify-center align-items-center h-100 spinner">
                                    <div class="spinner-border text-primary mx-auto" role="status">
                                    </div>
                                </div>
                                <div>
                                    <div class="table-header red-table tread-tabs__content-2">
                                        <p class="text-center mt-2">پیشنهادات فروش</p>
                                        <ul>
                                            <li>قیمت ({{ strtoupper($markets['des']) }})</li>
                                            <li>مقدار ({{ strtoupper($markets['des']) }})</li>
                                            <li>جمع ({{ strtoupper($markets['des']) }})</li>
                                        </ul>
                                    </div>

                                    <div class="tbody-currency tread-tabs__content-2 position-relative">
                                        <table class="tread-table bg-table order-table">
                                            <tbody class="tbody-list-request-sell">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div>
                                    <div class="table-header green-table">
                                        <ul class="tread-tabs__content-2">
                                            <li>
                                                <div>
                                                    <p>آخرین قیمت</p>
                                                    <p style="cursor: pointer" class="last-price">0</p>
                                                </div>
                                            </li>
                                            @if($markets['source']!='rial')
                                                <li>
                                                    <div>
                                                        <p>تومان</p>
                                                        <p class="last-price-rls">0</p>
                                                    </div>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="tbody-currency tread-tabs__content-2 position-relative">
                                        <p class="text-center mt-2 mb-2">پیشنهادات خرید</p>
                                        <table class="tread-table bg-table order-table">
                                            <tbody class="tbody-list-request-buy">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel-box my-1 panel-box__padding-0 h-461 overflow-y-auto position-relative">
                        <div class="d-flex justify-center align-items-center h-100 spinner">
                            <div class="spinner-border text-primary mx-auto" role="status">
                            </div>
                        </div>
                        <div class="tread-tabs">
                            <div class="tread-tabs__btns panel-box__padding-1">
                                <ul>
                                    <li>
                                        <button>آخرین خرید و فروش ها</button>
                                    </li>
                                </ul>
                            </div>

                            <div
                                class="tread-tabs__content tread-tabs__content-last tread-tabs__content-2 last-transaction"
                                data-url="{{ route('exchange.list-transaction') }}">
                                <table class="tread-table">
                                    <thead>
                                    <tr>
                                        <th>زمان ها</th>
                                        <th>قیمت ({{ strtoupper($markets['des']) }})</th>
                                        <th>مقدار</th>
                                    </tr>
                                    </thead>

                                    <tbody class="tbody-last-transaction">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard-chart mt-1">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12 px-8">
                    <div class="panel-box panel-box__padding-0 show-data-table">
                        <div class="tread-tabs__btns">
                            <ul class="justify-start">
                                <li>
                                    <button class="active" type="button" id="open_order">سفارش های باز</button>
                                </li>
                                <li>
                                    <button type="button" id="close_order">سفارش های بسته</button>
                                </li>
                                <li>
                                    <button type="button" id="all_orders">تاریخچه سفارشات</button>
                                </li>

                            </ul>
                        </div>
                        <div class="table-header tread-tabs__content-2">

                        </div>
                        <div class="table-responsive">
                            <input type="hidden" name="market" value="{{ implode(',',$markets) }}">
                            <div class="tread-tabs__content-2 tread-tabs__content-last-3">
                                <table id="orders_table" class="main-tread__table">
                                    <tr>
                                        <th>زمان</th>
                                        <th>بازار</th>
                                        <th>نوع</th>
                                        <th>انجام شده</th>
                                        <th>انجام نشده</th>
                                        <th>مقدار</th>
                                        <th>قیمت</th>
                                        <th>عملیات</th>

                                    </tr>
                                    <tbody id="open">
                                    @foreach($trades as $trade)
                                        @if($trade->status == 0)
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'%d %B %Y')}}</p>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'H:i:s')}}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?strtoupper(str_replace('USDT','RIAL',$trade->market)):strtoupper($trade->market)}}
                                                </td>
                                                <td>
                                                    {{$trade->type==1?'خرید':'فروش'}}
                                                </td>
                                                <td>
                                                    0
                                                </td>
                                                <td>{{($trade->old_amount)}}</td>
                                                <td>{{($trade->old_amount)}}</td>
                                                <td>{{($trade->unit =='rial'||$trade->unit=='rls')?rial_to_unit($trade->total_amount/$trade->old_amount,$trade->unit):$trade->price}}
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?get_unit():$trade->unit}}
                                                </td>
                                                <td>
                                                    <form action="{{route('exchange.cancel-trade',$trade->id_trade)}}"
                                                          method="get">
                                                        @csrf
                                                        <button style="padding: 5px" type="button"
                                                                class="badge badge-danger ajaxStore">کنسل
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>

                                    <tbody id="close" style="display: none">
                                    @foreach($trades as $trade)
                                        @if($trade->status == 1)
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'%d %B %Y')}}</p>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'H:i:s')}}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?strtoupper(str_replace('USDT','RIAL',$trade->market)):strtoupper($trade->market)}}
                                                </td>
                                                <td>
                                                    {{$trade->type==1?'خرید':'فروش'}}
                                                </td>
                                                <td>
                                                    {{$trade->amount}}
                                                </td>
                                                <td>0</td>
                                                <td>{{$trade->amount}}</td>
                                                <td>{{($trade->unit =='rial'||$trade->unit=='rls')?rial_to_unit($trade->total_amount/$trade->old_amount,$trade->unit):$trade->price}}
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?get_unit():$trade->unit}}</td>
                                                <td>
                                                    -
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>

                                    <tbody id="all" style="display: none">
                                    @foreach($trades as $trade)
                                        @if($trade->status == 1 || $trade->status==2)
                                            <tr>
                                                <td>
                                                    <div>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'%d %B %Y')}}</p>
                                                        <p>{{jdate_from_gregorian($trade->created_at,'H:i:s')}}</p>
                                                    </div>
                                                </td>
                                                <td>
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?strtoupper(str_replace('USDT','RIAL',$trade->market)):strtoupper($trade->market)}}
                                                </td>
                                                <td>
                                                    {{$trade->type==1?'خرید':'فروش'}}
                                                </td>
                                                <td>
                                                    {{$trade->status==1?$trade->amount:'-'}}
                                                </td>
                                                <td>{{$trade->status==1?'-':$trade->amount}}</td>
                                                <td>{{$trade->amount}}</td>
                                                <td>{{($trade->unit =='rial'||$trade->unit =='rls')?rial_to_unit($trade->total_amount/$trade->old_amount,$trade->unit):$trade->price}}
                                                    {{($trade->unit=='rial'||$trade->unit=='rls')?get_unit():$trade->unit}}</td>
                                                <td>
                                                    {{$trade->status==1?"انجام شده":"کنسل شده"}}
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    {{--    <script src="{{asset('theme/user/scripts/show-profile.js')}}"></script>--}}
    {{--    <script src="{{asset('theme/user/scripts/panel-menu.js')}}"></script>--}}
    <script>
        {{--        var currencies = `{{ json_encode(get_all_currencies()) }}`--}}
        let source_unit = `{{ $markets['source'] }}`
        let source_amount = `{{ $markets_value['source_amount'] }}`
        let des_amount = `{{ $markets_value['des_amount'] }}`
    </script>
    <script src="{{asset('theme/user/scripts/bg-tread.js?v=1.0.1')}}"></script>
@endsection
