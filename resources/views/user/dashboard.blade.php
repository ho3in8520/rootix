@extends('templates.user.master_page')
@section('title_browser')
    داشبورد روتیکس-صرافی ارز دیجیتال روتیکس
@endsection
@section('style')
    <style>
        .price-chart-table {
            width: 95px;
            height: 57px;
            margin: 0 auto;
        }

        .label-39Xi8p6T {
            display: none;
        !important;
        }
    </style>
@endsection
@section('content')
    <section class="dashboard-cart navigation">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-6">
                    <h1 class="dashboard-title desktop-title">داشبورد صرافی</h1>
                </div>
                <div class="col-6 owl-button" dir="ltr">
                    <button class="btn-next nav-button" type="button">
                        <span class="next">❮</span>
                    </button>
                    <button class="btn-prev nav-button" type="button">
                        <span class="prev">❯</span>
                    </button>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-12">
                    <div class="owl-carousel owl-theme chart-slider" dir="ltr">
                        @foreach($header_currencies as $key=>$item)
                            @php
                                $color=$item['color']?$item['color']:'#627EEA';
                                $color=str_split(str_replace('#','',$color),2);
                                $r=hexdec($color[0]);
                                $g=hexdec($color[1]);
                                $b=hexdec($color[2]);
                            @endphp
                            <div class="chart-slider-item">
                                <!-- TradingView Widget BEGIN -->
                                <div class="tradingview-widget-container"
                                     data-href="{{route('landing.price_detail',$key)}}" style="cursor: pointer">
                                    <div class="tradingview-widget-container__widget"></div>
                                    <div style="pointer-events: none">
                                        <script type="text/javascript"
                                                src="https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js"
                                                async>
                                            {
                                                "symbol"
                                            :
                                                "{{$key}}USDT",
                                                    "width"
                                            :
                                                "100%",
                                                    "height"
                                            :
                                                "100%",
                                                    "locale"
                                            :
                                                "en",
                                                    "dateRange"
                                            :
                                                "1D",
                                                    "colorTheme"
                                            :
                                                "light",
                                                    "trendLineColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 1)",
                                                    "underLineColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 0.3)",
                                                    "underLineBottomColor"
                                            :
                                                "rgba({{$r}}, {{$g}}, {{$b}}, 0)",
                                                    "isTransparent"
                                            :
                                                false,
                                                    "autosize"
                                            :
                                                true,
                                                    "largeChartUrl"
                                            :
                                                ""
                                            }
                                        </script>
                                    </div>

                                </div>
                                <!-- TradingView Widget END -->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!--        end-navigation-->
        </div>
    </section>

    <section class="dashboard-chart">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">حجم کل بازار</h2>
                        </div>

                        <div class="chart">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container" style="pointer-events: none">
                                <div id="tradingview_81a31"></div>
                                <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                <script type="text/javascript">
                                    new TradingView.MediumWidget(
                                        {
                                            "symbols": [
                                                [
                                                    "CRYPTOCAP:TOTAL",
                                                    "CRYPTOCAP:TOTAL"
                                                ],
                                            ],
                                            "chartOnly": false,
                                            "width": '100%',
                                            "height": 464,
                                            "locale": "en",
                                            "colorTheme": "light",
                                            "gridLineColor": "rgba(240, 243, 250, 0)",
                                            "trendLineColor": "#2962FF",
                                            "fontColor": "#787B86",
                                            "underLineColor": "rgba(41, 98, 255, 0.3)",
                                            "underLineBottomColor": "rgba(41, 98, 255, 0)",
                                            "isTransparent": false,
                                            "autosize": true,
                                            "showFloatingTooltip": false,
                                            "scalePosition": "right",
                                            "scaleMode": "Normal",
                                            "container_id": "tradingview_81a31"
                                        }
                                    );
                                </script>
                            </div>
                            <!-- TradingView Widget END -->
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">کیف پول کاربر</h2>
                        </div>

                        <div class="doughnut-chart">
                            @if ($result['total_rls_asset'] > 0)
                                <canvas id="wallet-assets-chart"></canvas>
                            @else
                                <h4 class="text-center">موجودی کیف پول شما صفر می باشد.</h4>
                            @endif
                        </div>

                        <div class="all-assets">
                            <div
                                class="
                            d-flex
                            all-assets-container
                            items-center
                            justify-between
                          "
                            >
                                <h5 class="all-assets-title">مقدار دارایی کل:</h5>
                                <div class="d-flex">
                                    <h5 class="all-assets-tooman">{{number_format($result['total_rls_asset'])}}</h5>
                                    <h5 class="all-assets-dollar">({{$result['total_usdt_asset']}}$)</h5>
                                </div>
                            </div>
                            <div
                                class="
                            d-flex
                            all-assets-container
                            items-center
                            justify-between
                          "
                            >
                                <h5 class="all-assets-title">
                                    مقدار دارایی کیف پول:
                                </h5>
                                <div class="d-flex">
                                    <h5 class="all-assets-tooman">{{number_format($result['rls_asset'])}}</h5>
                                    <h5 class="all-assets-dollar">({{$result['rls_asset_to_usdt']}}$)</h5>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            all-assets-container
                            items-center
                            justify-between
                          "
                            >
                                <h5 class="all-assets-title">
                                    مقدار دارایی رمز ارز:
                                </h5>
                                <div class="d-flex">
                                    <h5 class="all-assets-tooman">
                                        {{number_format($result['usdt_asset_to_rls'])}}
                                    </h5>
                                    <h5 class="all-assets-dollar">({{$result['usdt_asset']}}$)</h5>
                                </div>
                            </div>

                            <div class="d-flex all-assets-container items-center justify-between">
                                {{--                                <h5 class="all-assets-title">خرید و فروش امروز:</h5>--}}
                                {{--                                <div class="d-flex">--}}
                                {{--                                    <h5 class="all-assets-tooman">۲.۵۰۰.۰۰ تومان</h5>--}}
                                {{--                                    <h5 class="all-assets-dollar">(1000$)</h5>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="price-table-currency">
        <div class="container">
            <div class="dashboard-table">
                <div class="d-flex justify-between position-relative">
                    <h2 class="market-volume-title">
                        جدول رمز ارز های موجود در صرافی
                    </h2>
                    <h3 class="table-bottom-title">
                        برای مشاهده جدول همه رمزارز ها از
                        <a href="{{route('landing.prices')}}">صفحه قیمت ها</a>
                        بازدید کنید
                    </h3>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="dashboard-table wallet-table-container-self">
                            <div class="wallet-table-container">
                                <table class="currency-table active dashboard-table-1">
                                    <tr>
                                        <td>
                                            <div class="td-row">ارز دیجیتال</div>
                                        </td>

                                        <td>
                                            <div class="td-row">قیمت</div>
                                        </td>

                                        <td>
                                            <div class="td-row">% 24h</div>
                                        </td>

                                        <td>
                                            <div class="td-row">% 7d</div>
                                        </td>

                                        <td>
                                            <div class="td-row">حجم بازار</div>
                                        </td>

                                        <td>
                                            <div class="td-row">حجم معاملات روزانه</div>
                                        </td>

                                        <td>
                                            <div class="td-row">نمودار تغییرات لحظه ای</div>
                                        </td>
                                    </tr>

                                    @foreach($currencies as $currency)
                                        @php
                                            $key=$currency['currency'];
                                        @endphp
                                        <tr>
                                            <td class="currency-title">
                                                <div class="currency-right-title">
                                                    <img
                                                        src="{{$currency['logo_url']?$currency['logo_url']:asset("theme/landing/images/currency/$key.png")}}"
                                                        alt="{{$currency['currency']}}_logo"
                                                    />
                                                    <h5>{{$currency['name']}}</h5>
                                                </div>
                                            </td>
                                            <td class="currency-last-price">
                                                <div class="currency-doloar-price">
                                                    <h5>@if($currency['price']>=1){{number_format($currency['price'],2)}}
                                                        $ @else{{number_format($currency['price'],7)}}$ @endif</h5>
                                                    <p>{{rial_to_unit($currency['rls'],'rls')}} T</p>
                                                </div>
                                            </td>
                                            <td>
                                                @if($currency['1d'] != null)
                                                    @php
                                                        $img=(float)$currency['1d']<0?'red_arrow':'green_arrow';
                                                    @endphp
                                                    <span
                                                        class="currency-present-change {{(float)$currency['1d']<0?'red':'green'}}-currency">
                                                    <img style="padding-left: 5px;"
                                                         src="{{asset("theme/user/images/$img.svg")}}" alt="">
                                                {{(float)$currency['1d']<0?number_format((float)substr($currency['1d'],1),2):number_format((float)$currency['1d'],2)}}
                            </span>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td class="currency-most-price">
                                                @if($currency['7d'] != null)
                                                    @php
                                                        $img=(float)$currency['7d']<0?'red_arrow':'green_arrow';
                                                    @endphp
                                                    <span
                                                        class="currency-present-change {{(float)$currency['7d']<0?'red':'green'}}-currency">

                                                    <img style="padding-left: 5px;"
                                                         src="{{asset("theme/user/images/$img.svg")}}" alt="">
                                                    {{(float)$currency['7d']<0?number_format((float)substr($currency['7d'],1),2):number_format((float)$currency['7d'],2)}}


                            </span>
                                                @else - @endif
                                            </td>
                                            <td class="cur rency-chart">
                                                <div class="currency-doloar-price">
                                                    @php
                                                        $volume_info=market_cap_volume($currency['market_cap']);
                                                    @endphp
                                                    @if($volume_info[0] != 0)
                                                        <h5>{{$volume_info[0]}}</h5>
                                                        <p>{{$volume_info[1]}}</p>
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="currency-buying-btn">
                                                <div class="currency-doloar-price">
                                                    @php
                                                        $volume_info=market_cap_volume($currency['volume']);
                                                    @endphp
                                                    @if($volume_info[0] != 0)
                                                        <h5>{{$volume_info[0]}}</h5>
                                                        <p>{{$volume_info[1]}}</p>
                                                    @else
                                                        -
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <div class="price-chart-table">
                                                    <canvas id="{{$currency['currency']}}_chart" width="95"
                                                            height="30"></canvas>
                                                    <script type="text/javascript">
                                                        setTimeout(function () {
                                                            chart("{{$currency['currency']}}_chart", "{{$currency['color']}}");
                                                        }, 1000);
                                                    </script>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="d-flex items-center justify-between">
                            <h3 class="table-bottom-title">
                                برای مشاهده جدول همه رمزارز ها از
                                <a href="{{route('landing.prices')}}">صفحه قیمت ها</a>
                                بازدید کنید
                            </h3>

                            {{--                            <div class="d-flex items-center">--}}
                            {{--                                <span class="pages-length-title">صفحه ۱ از ۱۰</span>--}}

                            {{--                                <div class="table-pagination">--}}
                            {{--                                    <button type="button">--}}
                            {{--                                        <svg--}}
                            {{--                                            xmlns="http://www.w3.org/2000/svg"--}}
                            {{--                                            width="8"--}}
                            {{--                                            height="12"--}}
                            {{--                                            viewBox="0 0 9.531 15.424"--}}
                            {{--                                        >--}}
                            {{--                                            <path--}}
                            {{--                                                id="Icon_metro-expand-more"--}}
                            {{--                                                data-name="Icon metro-expand-more"--}}
                            {{--                                                d="M13.605,0,7.712,5.893,1.819,0,0,1.819,7.712,9.531l7.712-7.712Z"--}}
                            {{--                                                transform="translate(0 15.424) rotate(-90)"--}}
                            {{--                                                fill="#777"--}}
                            {{--                                            />--}}
                            {{--                                        </svg>--}}
                            {{--                                    </button>--}}
                            {{--                                    <button type="button">۳</button>--}}
                            {{--                                    <button type="button">۲</button>--}}
                            {{--                                    <button type="button">۱</button>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(".tradingview-widget-container").each(function () {
                $(this).on('click', function () {
                    window.location.href = $(this).data('href');
                })
            });
        })

        @if ($result['total_rls_asset'] > 0)
        const chart_data = {
            assets_chart: {
                'rial': '{{ $result['rls_asset'] * 100/$result['total_rls_asset']}}',
                'currency': '{{ $result['usdt_asset_to_rls'] * 100/$result['total_rls_asset'] }}',
            }
        }
        @endif
    </script>
    <script src="{{asset('theme/user/scripts/dashboard-charts.js')}}"></script>
    <script src="{{asset('theme/user/scripts/currency_chart.js')}}"></script>
@endsection
