@extends('templates.user.master_page')
@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <h1 class="dashboard-title">کیف پول</h1>
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="cart-currency-2 cart-currency-3">
                        <div class="cart-currency">
                            <div class="d-flex cart-currency__info justify-between">
                                <div class="cart-currency__about-price">
                                    <p>26.325$</p>
                                    <span class="plus">+12.03</span>
                                </div>

                                <div
                                    class="
                              cart-currency__about-name
                              d-flex
                              items-center
                            "
                                >
                                    <div
                                        class="
                                cart-currency__names
                                d-flex
                                flex-column
                                items-end
                              "
                                    >
                                        <h5 class="cart-currency__abbreviation">ETH</h5>
                                        <span class="cart-currency__name">Ethereum</span>
                                    </div>
                                    <img
                                        src="images/wallet/Group 292.png"
                                        alt="تایتل ارز"
                                        class="cart-currency__img"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="cart-currency-2 cart-currency-4">
                        <div class="cart-currency">
                            <div class="d-flex cart-currency__info justify-between">
                                <div class="cart-currency__about-price">
                                    <p>26.325$</p>
                                    <span class="plus">+12.03</span>
                                </div>

                                <div
                                    class="
                              cart-currency__about-name
                              d-flex
                              items-center
                            "
                                >
                                    <div
                                        class="
                                cart-currency__names
                                d-flex
                                flex-column
                                items-end
                              "
                                    >
                                        <h5 class="cart-currency__abbreviation">ETH</h5>
                                        <span class="cart-currency__name">Ethereum</span>
                                    </div>
                                    <img
                                        src="images/wallet/Group 292.png"
                                        alt="تایتل ارز"
                                        class="cart-currency__img"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="cart-currency-2 cart-currency-5">
                        <div class="cart-currency">
                            <div class="d-flex cart-currency__info justify-between">
                                <div class="cart-currency__about-price">
                                    <p>26.325$</p>
                                    <span class="plus">+12.03</span>
                                </div>

                                <div
                                    class="
                              cart-currency__about-name
                              d-flex
                              items-center
                            "
                                >
                                    <div
                                        class="
                                cart-currency__names
                                d-flex
                                flex-column
                                items-end
                              "
                                    >
                                        <h5 class="cart-currency__abbreviation">ETH</h5>
                                        <span class="cart-currency__name">Ethereum</span>
                                    </div>
                                    <img
                                        src="images/wallet/Group 292.png"
                                        alt="تایتل ارز"
                                        class="cart-currency__img"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-3">
                    <div class="cart-currency-2 cart-currency-6">
                        <div class="cart-currency">
                            <div class="d-flex cart-currency__info justify-between">
                                <div class="cart-currency__about-price">
                                    <p>26.325$</p>
                                    <span class="plus">+12.03</span>
                                </div>

                                <div
                                    class="
                              cart-currency__about-name
                              d-flex
                              items-center
                            "
                                >
                                    <div
                                        class="
                                cart-currency__names
                                d-flex
                                flex-column
                                items-end
                              "
                                    >
                                        <h5 class="cart-currency__abbreviation">ETH</h5>
                                        <span class="cart-currency__name">Ethereum</span>
                                    </div>
                                    <img
                                        src="images/wallet/Group 292.png"
                                        alt="تایتل ارز"
                                        class="cart-currency__img"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dashboard-chart pt-2">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3">
                    <div class="panel-box recent-transactions">
                        <div
                            class="
                          d-flex
                          justify-between
                          items-center
                          recent-transactions__title
                        "
                        >
                            <h2 class="market-volume-title">تراکنش های اخیر</h2>
                            <a href="#"> مشاهده کامل </a>
                        </div>

                        <div>
                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/Group 1686.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>Ethereum</h5>
                                        <span>کیف ارز-برداشت</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5>0.0023ETH-</h5>
                                    <span>تومان 500.000</span>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/tabadol.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>تبادل</h5>
                                        <span>کیف پول-واریز</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5 class="recent-transactions__money">
                                        تومان 500.000+
                                    </h5>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/Group 1686.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>Ethereum</h5>
                                        <span>کیف ارز-برداشت</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5>0.0023ETH-</h5>
                                    <span>تومان 500.000</span>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/tabadol.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>تبادل</h5>
                                        <span>کیف پول-برداشت</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5 class="recent-transactions__money">
                                        تومان 500.000-
                                    </h5>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/deposite.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>واریز نقدی</h5>
                                        <span>کیف پول-واریز</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5 class="recent-transactions__money">
                                        تومان 500.000+
                                    </h5>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/harvest.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>برداشت نقدی</h5>
                                        <span>کیف پول-برداشت</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5 class="recent-transactions__money">
                                        تومان 500.000+
                                    </h5>
                                </div>
                            </div>

                            <div
                                class="
                            d-flex
                            justify-between
                            recent-transactions__content
                            items-center
                          "
                            >
                                <div
                                    class="
                              d-flex
                              items-center
                              recent-transactions__desc
                            "
                                >
                                    <img
                                        src="images/wallet/Group 1686.png"
                                        class="recent-transactions__img"
                                        alt="تایتل ارز"
                                    />
                                    <div>
                                        <h5>Ethereum</h5>
                                        <span>کیف ارز-برداشت</span>
                                    </div>
                                </div>

                                <div
                                    class="
                              recent-transactions__name
                              d-flex
                              flex-column
                              items-end
                            "
                                >
                                    <h5>0.0023ETH-</h5>
                                    <span>تومان 500.000</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-9">
                    <div class="panel-box wallet-asset">
                        <div class="assets d-flex">
                            <div>
                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        کل دارایی شما
                                    </h2>
                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">۵۰.۰۰۰.۰۰۰ تومان</h2>

                                        <div class="assets-value">
                                            <span class="plus change-percents">+12.03</span>
                                            <p>60.000.000USDT</p>
                                        </div>
                                    </div>
                                </div>

                                <hr class="assets-line" />

                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        دارایی موجود در کیف ریالی
                                    </h2>

                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">۵۰.۰۰۰.۰۰۰ تومان</h2>

                                        <div class="assets-value">
                                            <p>60.000.000USDT</p>
                                        </div>
                                    </div>
                                </div>

                                <hr class="assets-line" />

                                <div>
                                    <h2 class="market-volume-title assets-title">
                                        دارایی موجود در کیف ارز
                                    </h2>

                                    <div class="d-flex justify-between items-end">
                                        <h2 class="assets-price">۵۰.۰۰۰.۰۰۰ تومان</h2>

                                        <div class="assets-value">
                                            <p>60.000.000USDT</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="mx-auto">
                                <div class="wallet-chart">
                                    <canvas id="assets-chart"></canvas>
                                </div>

                                <div class="assets-chart__info-container">
                                    <div class="assets-chart__info"></div>

                                    <hr class="chart-assets-line" />

                                    <div class="total-chart">
                              <span class="total-chart__title-2">
                                جمع کل:
                              </span>

                                        <span class="total-chart__percent-2"> %100 </span>
                                    </div>

                                    <hr class="chart-assets-line" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-2">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="panel-box red-chart-box">
                        <h2 class="market-volume-title">نمودار</h2>

                        <div class="red-chart-container" style="height: 310px;">
                            <canvas id="red-chart"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-5">
                    <div class="panel-box">
                        <h2 class="market-volume-title">دارایی رمز ارز</h2>

                        <div class="wallet-chart-2-container">
                            <div
                                class="doughnut-chart doughnut-chart-2 wallet-chart-2"
                            >
                                <canvas id="wallet-assets-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-3">
                    <div class="left-wallet-chart">
                        <div class="panel-box">
                            <h2 class="left-wallet-chart-title">BTC Hashribon</h2>

                            <div>
                                <div class="left-wallet-chart-self-top-container">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="135"
                                        height="110"
                                        viewBox="0 0 161.929 120.791"
                                    >
                                        <defs>
                                            <linearGradient
                                                id="linear-gradient2"
                                                x1="0.5"
                                                y1="1"
                                                x2="0.5"
                                                gradientUnits="objectBoundingBox"
                                            >
                                                <stop offset="0" stop-color="#fff" />
                                                <stop offset="0.089" stop-color="#fdf6fb" />
                                                <stop offset="0.23" stop-color="#fbdff0" />
                                                <stop offset="0.407" stop-color="#f6b9e0" />
                                                <stop offset="0.613" stop-color="#ef84c8" />
                                                <stop offset="0.84" stop-color="#e741ab" />
                                                <stop offset="1" stop-color="#e10e95" />
                                            </linearGradient>
                                        </defs>
                                        <path
                                            id="Path_7"
                                            data-name="Path 7"
                                            d="M1543.4,981.5l-16.792-25.757-15.714-34.639-34.269-24.869-27.089,28.421-38.119,7.994-16.961,37.3L1381.472,981.5v35.527H1543.4Z"
                                            transform="translate(-1381.472 -896.24)"
                                            fill="url(#linear-gradient2)"
                                        />
                                    </svg>
                                    <h2
                                        class="
                                left-wallet-chart-self-top-container-perecent
                              "
                                    >
                                        85%
                                    </h2>
                                </div>
                            </div>
                        </div>
                        <div class="panel-box">
                            <h2 class="left-wallet-chart-title">BTC Hashribon</h2>
                            <div>
                                <div class="left-wallet-chart-self-top-container">
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="135"
                                        height="110"
                                        viewBox="0 0 168.8 115.301"
                                    >
                                        <defs>
                                            <linearGradient
                                                id="linear-gradient"
                                                x1="0.5"
                                                y1="1"
                                                x2="0.5"
                                                gradientUnits="objectBoundingBox"
                                            >
                                                <stop offset="0" stop-color="#fff" />
                                                <stop offset="0.089" stop-color="#f6fdfb" />
                                                <stop offset="0.232" stop-color="#dff8f1" />
                                                <stop offset="0.41" stop-color="#b9f0e2" />
                                                <stop offset="0.617" stop-color="#84e5cc" />
                                                <stop offset="0.845" stop-color="#41d8b0" />
                                                <stop offset="1" stop-color="#10ce9c" />
                                            </linearGradient>
                                        </defs>
                                        <path
                                            id="Path_8608"
                                            data-name="Path 8608"
                                            d="M1864.9,985.576l-20.242-45.137-21.389-3.575L1791.8,906.028l-26.6,22.345-35.138,1.788-17.406,28.155L1696.1,985.576v35.752h168.8Z"
                                            transform="translate(-1696.101 -906.028)"
                                            fill="url(#linear-gradient)"
                                        />
                                    </svg>

                                    <h2
                                        class="
                                left-wallet-chart-self-top-container-perecent
                              "
                                    >
                                        6M
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-2">
        <div class="container">
            <div class="dashboard-table">
                <div class="row">
                    <div class="col-12">
                        <table class="wallet-table">
                            <tr>
                                <th>
                                    <div>
                                        نام ارز / میزان دارایی

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </div>
                                </th>
                                <th>
                                    <div>
                                        قیمت جهانی

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </div>
                                </th>
                                <th>
                                    <div>
                                        موجودی

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </div>
                                </th>
                                <th>
                                    <div>
                                        تغییرات

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="10"
                                            height="20.383"
                                            viewBox="0 0 12.203 20.383"
                                        >
                                            <path
                                                id="Icon_material-unfold-more"
                                                data-name="Icon material-unfold-more"
                                                d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                transform="translate(-11.115 -4.5)"
                                                opacity="0.7"
                                            />
                                        </svg>
                                    </div>
                                </th>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/Tooman-currency.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <span>ریال</span>
                                    </div>
                                </td>
                                <td></td>

                                <td>
                            <span class="wallet-table-currency-price">
                              ۴,۰۵۶.۲۳تومان
                            </span>
                                </td>

                                <td></td>

                                <td class="wallet-table-btns-td">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-bitcoin.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Bitcoin</span>
                                            <p>0.00231 BTC</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-trx.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ripple</span>
                                            <p>0.00231 RIP</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-eth.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ethereum</span>
                                            <p>0.00231 ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-bitcoin.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Bitcoin</span>
                                            <p>0.00231 BTC</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-trx.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ripple</span>
                                            <p>0.00231 RIP</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-eth.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ethereum</span>
                                            <p>0.00231 ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-bitcoin.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Bitcoin</span>
                                            <p>0.00231 BTC</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-trx.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ripple</span>
                                            <p>0.00231 RIP</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="images/wallet/wallet-table-eth.png"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div
                                            class="wallet-table__curency-title-container"
                                        >
                                            <span>Ethereum</span>
                                            <p>0.00231 ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                              <span class="wallet-table-currency-price">
                                ۴,۰۵۶.۲۳تومان
                              </span>
                                        <p>300.000$</p>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <span class="change-percents plus">+12.03</span>
                                        <p>24H</p>
                                    </div>
                                </td>
                                <td class="wallet-table-btns-td" colspan="2">
                                    <div class="wallet-table-btns">
                                        <a href="#">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="26.997"
                                                height="31.499"
                                                viewBox="0 0 26.997 31.499"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-refresh"
                                                    data-name="Icon ionic-ios-refresh"
                                                    d="M30.445,20.306a1.049,1.049,0,0,0-1.048.956,11.415,11.415,0,1,1-11.672-12.3.275.275,0,0,1,.288.281v3.537a1.127,1.127,0,0,0,1.73.949L26.508,9a1.124,1.124,0,0,0,0-1.905L19.751,2.426a1.127,1.127,0,0,0-1.73.949V6.588a.276.276,0,0,1-.274.281A13.443,13.443,0,1,0,31.493,21.438a1.045,1.045,0,0,0-1.048-1.132Z"
                                                    transform="translate(-4.5 -2.251)"
                                                />
                                            </svg>
                                        </a>

                                        <a href="#" class="btn wallet-btn harvest-btn">
                                            برداشت
                                        </a>

                                        <a href="#" class="btn wallet-btn deposit-btn">
                                            واریز
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <h5 class="text-center footer-title">
                 2020. All Rights Reserved
            </h5>
        </div>
    </section>
@endsection
