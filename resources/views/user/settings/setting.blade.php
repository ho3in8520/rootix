@extends('templates.user.master_page')
@section('title_browser')
    تنظیمات-صرافی ارز دیجیتال روتیکس
@endsection

@section('content')
    <section class="setting-cart pb-0">
        <div class="container">
            <h1 class="setting-title desktop-title color-232323">تنظیمات داشبورد</h1>
        </div>
    </section>

    <section class="setting-cart pb-0">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-11 bg-white cart-currency">
                <h6 class=" d-block text-center bolder">ارسال اعلان ها (به زودی)</h6>
                <div class="row ">

                    <div
                        class="col-12 d-flex align-items-start align-items-md-center my-1 justify-content-center justify-content-sm-start pr-3 pr-lg-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                             viewBox="0 0 29.375 29.375">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #134563
                                    }
                                </style>
                            </defs>
                            <g id="Group_1730" data-name="Group 1730" transform="translate(-8.1 -8.1)">
                                <g id="Icon-Exclamation" transform="translate(8.1 8.1)">
                                    <path id="Fill-49" class="cls-1"
                                          d="M-205.213-240.525a14.687 14.687 0 0 1-14.687-14.688 14.687 14.687 0 0 1 14.687-14.687 14.687 14.687 0 0 1 14.687 14.687 14.687 14.687 0 0 1-14.687 14.687zm0-27.777a13.14 13.14 0 0 0-13.09 13.09 13.14 13.14 0 0 0 13.09 13.09 13.14 13.14 0 0 0 13.09-13.09 13.14 13.14 0 0 0-13.09-13.09z"
                                          transform="translate(219.9 269.9)"/>
                                    <path id="Fill-50" class="cls-1" d="M-197.4-236.1h1.721v1.721h-1.721z"
                                          transform="translate(211.227 256.871)"/>
                                    <path id="Fill-51" class="cls-1"
                                          d="M-196.048-246.532h-.983l-.369-6.944v-5.224h1.721v5.224l-.369 6.944"
                                          transform="translate(211.227 265.583)"/>
                                </g>
                            </g>
                        </svg>
                        <p class="color-4a4a4a mr-2">روش ارسال اعلان ورود و تراکنش ها را مشخص کنید</p>
                    </div>

                    <div class="col-12 d-flex flex-column justify-content-center align-items-start px-lg-0">
                        <div class="form-check p-0 d-flex justify-start align-items-center my-1">
                            <input class="form-check-input pl-2 d-none" type="radio" name="settingRadio1"
                                   id="settingRadio1" value="option1" disabled>
                            <label class="form-check-label pr-2 color-4a4a4a" for="settingRadio1">دریافت اعلان از طریق
                                پست
                                الکترونیک</label>
                        </div>
                        <div class="form-check p-0 d-flex justify-start align-items-center my-1">
                            <input class="form-check-input pl-2 d-none" type="radio" name="settingRadio2"
                                   id="settingRadio2" value="option1" disabled>
                            <label class="form-check-label pr-2 color-4a4a4a" for="settingRadio2">دریافت اعلان از طریق
                                پیامک</label>
                        </div>
                        <div class="form-check p-0 d-flex justify-start align-items-center my-1">
                            <input class="form-check-input pl-2 d-none" type="radio" name="settingRadio3"
                                   id="settingRadio3" value="option1" disabled>
                            <label class="form-check-label pr-2 color-4a4a4a" for="settingRadio3">به صورت نوتیفیکشن در
                                اپلیکیشن</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="setting-cart pb-0">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-11 bg-white cart-currency">
                <h6 class=" d-block text-center bolder">ورود دو مرحله ای</h6>
                <div class="row align-items-center justify-start flex-column">

                    <div
                        class="col-12 d-flex align-items-start align-items-md-center my-1 justify-content-center justify-content-sm-start pr-3 pr-lg-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                             viewBox="0 0 29.375 29.375">
                            <defs>
                                <style>
                                    .cls-1 {
                                        fill: #134563
                                    }
                                </style>
                            </defs>
                            <g id="Group_1730" data-name="Group 1730" transform="translate(-8.1 -8.1)">
                                <g id="Icon-Exclamation" transform="translate(8.1 8.1)">
                                    <path id="Fill-49" class="cls-1"
                                          d="M-205.213-240.525a14.687 14.687 0 0 1-14.687-14.688 14.687 14.687 0 0 1 14.687-14.687 14.687 14.687 0 0 1 14.687 14.687 14.687 14.687 0 0 1-14.687 14.687zm0-27.777a13.14 13.14 0 0 0-13.09 13.09 13.14 13.14 0 0 0 13.09 13.09 13.14 13.14 0 0 0 13.09-13.09 13.14 13.14 0 0 0-13.09-13.09z"
                                          transform="translate(219.9 269.9)"/>
                                    <path id="Fill-50" class="cls-1" d="M-197.4-236.1h1.721v1.721h-1.721z"
                                          transform="translate(211.227 256.871)"/>
                                    <path id="Fill-51" class="cls-1"
                                          d="M-196.048-246.532h-.983l-.369-6.944v-5.224h1.721v5.224l-.369 6.944"
                                          transform="translate(211.227 265.583)"/>
                                </g>
                            </g>
                        </svg>
                        <p class="color-4a4a4a mr-2">روش ورود دو مرحله ای را مشخص کنید</p>
                    </div>

                    <div class="col-12 d-flex flex-column justify-content-center align-items-start px-lg-0 my-1">
                        <div class="form-check p-0 m-0 d-flex justify-start align-items-center flex-wrap">
                            <input class="form-check-input pl-2 d-none" type="radio" name="settingRadio"
                                   id="settingRadio4" value="option1"
                                   @if(auth()->user()->login_2fa == true) checked @endif>
                            <label class="form-check-label pr-2 color-4a4a4a d-inline-block text-nowrap"
                                   for="settingRadio4">ورود دو مرحله ای google
                                authenticator </label>
                            @if(!empty(auth()->user()->google2fa_secret))
                                @can('authenticate_deactive')
                                    <button type="button" data-toggle="modal" data-target="#default"
                                            class="d-block d-md-inline-block text-white bg-f66b64 p-0 btn border-0 shadow-none mr-md-3 ml-0 mr-auto mt-2 mt-sm-0 btnsize3">
                                        <small>غیر فعال سازی</small>
                                    </button>
                                @endcan
                                <div class="modal fade mt-5" id="default" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header mx-auto border-0">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="130" height="140"
                                                     viewBox="0 0 147 150">
                                                    <g id="Group_1783" data-name="Group 1783"
                                                       transform="translate(-833 -230)">
                                                        <g id="Group_1782" data-name="Group 1782"
                                                           transform="translate(278 180)">
                                                            <g id="Ellipse_118" data-name="Ellipse 118"
                                                               transform="translate(555 50)" fill="none"
                                                               stroke="#ff0c00" stroke-width="3">
                                                                <circle cx="73.5" cy="73.5" r="73.5" stroke="none"/>
                                                                <circle cx="73.5" cy="73.5" r="72" fill="none"/>
                                                            </g>
                                                            <text id="_" data-name="!" transform="translate(651 157)"
                                                                  fill="red" font-size="80" font-family="IRANSans"
                                                                  letter-spacing="0.01em">
                                                                <tspan x="-11.309" y="0">!</tspan>
                                                            </text>
                                                        </g>
                                                    </g>
                                                </svg>

                                            </div>
                                            <div class="modal-body border-0">
                                                <h3 class="text-center my-3">غیر فعالسازی Google Authenticator</h3>
                                                <p class="my-4">
                                                    کد google authenticator را وارد کنید

                                                </p>
                                            </div>
                                            <form method="post"
                                                  action="{{ route('setting.status_google_authenticator') }}">
                                                @csrf
                                                <input type="hidden" value="deactivate"
                                                       name="type">
                                                <div>
                                                    <div dir="rtl"
                                                         class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-cente active">

                                                        <input type="text" dir="ltr" name="one_time_password"
                                                               class="currency-value-input text-center"
                                                               id="currency-value-input-1">
                                                    </div>
                                                </div>
                                                <div class="modal-footer border-0">
                                                    <button type="button" id="deactivate"
                                                            class="btn signin-btn deposit-btn mx-auto">
                                                        تایید
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @can('authenticate_active')
                                    <button type="button" data-toggle="modal" data-target="#default"
                                            class="d-block d-md-inline-block text-white bg-54e9c3 p-0 btn border-0 shadow-none mr-md-3 ml-0 mr-auto mt-2 mt-sm-0 btnsize3">
                                        <small>فعال سازی</small>
                                    </button>
                                @endcan
                                <div class="modal fade mt-5" id="default" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-body border-0">
                                                <h4 class="text-center my-3">ثبت گوگل اتینتیکاتور</h4>
                                                <p>
                                                    کد شخصی اتینتیکاتور شما برابر است
                                                    با : {{$secret}}
                                                    ،یا میتونید از طریق اسکن Qr کد
                                                    زیر آن را به اتینتیکاتور خود
                                                    اضافه کنید
                                                </p>
                                                <div class="text-center">
                                                    <img src="{{$QR_Image}}">
                                                </div>
                                                <p>
                                                    برای ثبت اتینتیکاتور خود باید نرم
                                                    افزار google authenticator را در
                                                    تلفن همراه خود نصب بفرمایید،
                                                    بعد از اسکن بارکد کد اتنتیکاتور را
                                                    وارد کنید
                                                </p>
                                            </div>
                                            <form method="post"
                                                  action="{{ route('setting.status_google_authenticator') }}">
                                                @csrf
                                                <input type="hidden"
                                                       value="{{ $secret }}"
                                                       name="secret">
                                                <input type="hidden" value="active"
                                                       name="type">
                                                <div>
                                                    <div dir="ltr"
                                                         class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">

                                                        <span class="request-box__title">Authenticator Code</span>

                                                        <input type="text" dir="rtl" class="currency-value-input"
                                                               id="currency-value-input-1" name="one_time_password">
                                                    </div>
                                                </div>
                                                <div class="modal-footer border-0">
                                                    <button
                                                        id="submit" type="button"
                                                        class="btn signin-btn deposit-btn mx-auto">
                                                        تایید
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div
                        class="col-12 d-flex align-items-start align-items-md-center justify-content-center justify-content-sm-start pr-3 pr-lg-2 my-1">
                        <div>
                            <svg data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" width="25"
                                 height="25" viewBox="0 0 29.574 29.574">
                                <path data-name="Path 8613"
                                      d="M17.489 18.995H9.938a.753.753 0 0 1 0-1.506h7.523a.753.753 0 0 1 0 1.506zm3.54-5.018H9.938a.753.753 0 1 1 0-1.506h11.09a.753.753 0 1 1 0 1.506z"
                                      transform="translate(-.696 -.945)" style="fill:#f39c12"/>
                                <path data-name="Path 8614"
                                      d="M28.826 29.574a.749.749 0 0 1-.749-.749V14.787a13.281 13.281 0 1 0-13.29 13.281h10.028a.753.753 0 0 1 0 1.506H14.787a14.787 14.787 0 1 1 14.787-14.787v14.039a.749.749 0 0 1-.749.749z"
                                      style="fill:#222f3e"/>
                            </svg>
                        </div>
                        <p class="color-4a4a4a mr-2">
                            با فعال سازی این گزینه، جهت ورود به حساب کاربری خود علاوه بر نام کاربری و کلمه عبور، باید کد
                            یکبار مصرفی که از برنامه Google Authenticator دریافت کرده اید را وارد کنید
                        </p>
                    </div>

                    <div class="col-12 d-flex flex-column justify-content-center align-items-start px-lg-0 my-1">
                        <div class="form-check p-0 m-0 d-flex justify-start align-items-center">
                            <input class="form-check-input pl-2 d-none" type="radio" name="settingRadio"
                                   id="settingRadio5" value="option1" disabled>
                            <label class="form-check-label pr-2 color-4a4a4a" for="settingRadio5">ورود با پیامک (به
                                زودی)</label>
                        </div>
                    </div>

                    <div
                        class="col-12 d-flex align-items-start align-items-md-center justify-content-center justify-content-sm-start pr-3 pr-lg-2 my-1">
                        <div>
                            <svg data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" width="25"
                                 height="25" viewBox="0 0 29.574 29.574">
                                <path data-name="Path 8613"
                                      d="M17.489 18.995H9.938a.753.753 0 0 1 0-1.506h7.523a.753.753 0 0 1 0 1.506zm3.54-5.018H9.938a.753.753 0 1 1 0-1.506h11.09a.753.753 0 1 1 0 1.506z"
                                      transform="translate(-.696 -.945)" style="fill:#f39c12"/>
                                <path data-name="Path 8614"
                                      d="M28.826 29.574a.749.749 0 0 1-.749-.749V14.787a13.281 13.281 0 1 0-13.29 13.281h10.028a.753.753 0 0 1 0 1.506H14.787a14.787 14.787 0 1 1 14.787-14.787v14.039a.749.749 0 0 1-.749.749z"
                                      style="fill:#222f3e"/>
                            </svg>
                        </div>
                        <p class="color-4a4a4a mr-2">
                            با فعال سازی این گزینه، جهت ورود به حساب کاربری خود علاوه بر نام کاربری و کلمه عبور، باید کد
                            یکبار مصرفی که پیامک میشود را وارد کنید
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="setting-cart">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-11 bg-white cart-currency">
                <h6 class=" d-block text-center bolder">تائید تراکنش ها</h6>
                <form method="post" action="{{ route('setting.change_confirmation_type') }}"
                      class="select_confirm_type">
                    @csrf
                    <div class="row align-items-center justify-start flex-column">

                        <div
                            class="col-12 d-flex align-items-start align-items-md-center my-1 justify-content-center justify-content-sm-start pr-3 pr-lg-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                                 viewBox="0 0 29.375 29.375">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #134563
                                        }
                                    </style>
                                </defs>
                                <g id="Group_1730" data-name="Group 1730" transform="translate(-8.1 -8.1)">
                                    <g id="Icon-Exclamation" transform="translate(8.1 8.1)">
                                        <path id="Fill-49" class="cls-1"
                                              d="M-205.213-240.525a14.687 14.687 0 0 1-14.687-14.688 14.687 14.687 0 0 1 14.687-14.687 14.687 14.687 0 0 1 14.687 14.687 14.687 14.687 0 0 1-14.687 14.687zm0-27.777a13.14 13.14 0 0 0-13.09 13.09 13.14 13.14 0 0 0 13.09 13.09 13.14 13.14 0 0 0 13.09-13.09 13.14 13.14 0 0 0-13.09-13.09z"
                                              transform="translate(219.9 269.9)"/>
                                        <path id="Fill-50" class="cls-1" d="M-197.4-236.1h1.721v1.721h-1.721z"
                                              transform="translate(211.227 256.871)"/>
                                        <path id="Fill-51" class="cls-1"
                                              d="M-196.048-246.532h-.983l-.369-6.944v-5.224h1.721v5.224l-.369 6.944"
                                              transform="translate(211.227 265.583)"/>
                                    </g>
                                </g>
                            </svg>
                            <p class="color-4a4a4a mr-2">روش تائید تراکنش را مشخص کنید</p>
                        </div>

                        <div class="col-12 d-flex flex-column justify-content-center align-items-start px-lg-0 my-1">
                            <div class="form-check p-0 m-0 d-flex justify-start align-items-center flex-wrap">
                                <input class="form-check-input pl-2 d-none" type="radio" name="confirm_type"
                                       id="settingRadio6" value="google-authenticator"
                                       @if($user->confirm_type == 'google-authenticator') checked @endif>
                                <label class="form-check-label pr-2 color-4a4a4a d-inline-block text-nowrap"
                                       for="settingRadio6">تائید با google authenticator </label>
                            </div>
                        </div>
                        <div class="col-12 px-lg-0 my-1 mr-3 confirmation-code" style="display: none">
                            <form method="post" action="{{ route('setting.verify_confirmation_type') }}" class="col-3">
                                @csrf
                                <div class="form-check p-0 m-0 d-flex justify-start align-items-center">
                                    <input type="hidden" name="confirm_type" value="google-authenticator">

                                    <div dir="rtl"
                                         class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-content-center  align-items-center active ">

                                        <span class="request-box__title">کد تائید</span>

                                        <input type="text" dir="ltr" name="one_time_sms"
                                               class="currency-value-input"
                                               id="currency-value-input-1" placeholder="100">
                                    </div>


                                    <button type="button" id="code-submit"
                                            class="d-md-inline-block text-white bg-54e9c3 p-0 btn border-0 shadow-none mr-md-3 ml-0 mr-auto mt-2 mt-sm-0 btnsize3">
                                        <small>تائید</small>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div
                            class="col-12 d-flex align-items-start align-items-md-center justify-content-center justify-content-sm-start pr-3 pr-lg-2 my-1">
                            <div>
                                <svg data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" width="25"
                                     height="25" viewBox="0 0 29.574 29.574">
                                    <path data-name="Path 8613"
                                          d="M17.489 18.995H9.938a.753.753 0 0 1 0-1.506h7.523a.753.753 0 0 1 0 1.506zm3.54-5.018H9.938a.753.753 0 1 1 0-1.506h11.09a.753.753 0 1 1 0 1.506z"
                                          transform="translate(-.696 -.945)" style="fill:#f39c12"/>
                                    <path data-name="Path 8614"
                                          d="M28.826 29.574a.749.749 0 0 1-.749-.749V14.787a13.281 13.281 0 1 0-13.29 13.281h10.028a.753.753 0 0 1 0 1.506H14.787a14.787 14.787 0 1 1 14.787-14.787v14.039a.749.749 0 0 1-.749.749z"
                                          style="fill:#222f3e"/>
                                </svg>
                            </div>
                            <p class="color-4a4a4a mr-2">
                                با فعال سازی این گزینه، جهت تائید تراکنش خود، باید کد یکبار مصرفی که از برنامه Google
                                Authenticator دریافت کرده اید را وارد کنید.
                            </p>
                        </div>

                        <div class="col-12 d-flex flex-column justify-content-center align-items-start px-lg-0 my-1">
                            <div class="form-check p-0 m-0 d-flex justify-start align-items-center">
                                <input class="form-check-input pl-2 d-none" type="radio" name="confirm_type"
                                       id="settingRadio7" value="sms"
                                       @if($user->confirm_type == 'sms') checked @endif>
                                <label class="form-check-label pr-2 color-4a4a4a" for="settingRadio7">ورود با
                                    پیامک </label>
                            </div>
                        </div>
                        <div class="col-12 px-lg-0 my-1 mr-3 confirmation-code1" style="display: none">
                            <form method="post" action="{{ route('setting.verify_confirmation_type') }}" class="col-4">
                                @csrf
                                <div class="form-check p-0 m-0 d-flex justify-start align-items-center ">
                                    <input type="hidden" name="confirm_type1" value="sms">

                                    <div dir="ltr"
                                         class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-content-center  align-items-center active ">

                                        <span class="request-box__title">Authenticator Code:
                                        <p class="text-danger error-one_time_password1"></p>
                                        </span>

                                        <input type="text" dir="rtl" name="one_time_password1"
                                               class="currency-value-input"
                                               id="currency-value-input-1" placeholder="100">

                                    </div>


                                    <button type="button" id="code-submit2"
                                            class="d-md-inline-block text-white bg-54e9c3 p-0 btn border-0 shadow-none mr-md-3 ml-0 mr-auto mt-2 mt-sm-0 btnsize3">
                                        <small>تائید</small>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div
                            class="col-12 d-flex align-items-start align-items-md-center justify-content-center justify-content-sm-start pr-3 pr-lg-2 my-1">
                            <div>
                                <svg data-name="Group 1739" xmlns="http://www.w3.org/2000/svg" width="25"
                                     height="25" viewBox="0 0 29.574 29.574">
                                    <path data-name="Path 8613"
                                          d="M17.489 18.995H9.938a.753.753 0 0 1 0-1.506h7.523a.753.753 0 0 1 0 1.506zm3.54-5.018H9.938a.753.753 0 1 1 0-1.506h11.09a.753.753 0 1 1 0 1.506z"
                                          transform="translate(-.696 -.945)" style="fill:#f39c12"/>
                                    <path data-name="Path 8614"
                                          d="M28.826 29.574a.749.749 0 0 1-.749-.749V14.787a13.281 13.281 0 1 0-13.29 13.281h10.028a.753.753 0 0 1 0 1.506H14.787a14.787 14.787 0 1 1 14.787-14.787v14.039a.749.749 0 0 1-.749.749z"
                                          style="fill:#222f3e"/>
                                </svg>
                            </div>
                            <p class="color-4a4a4a mr-2">
                                با فعال سازی این گزینه، جهت تائید تراکنش خود، باید کد یکبار مصرفی که پیامک میشود را وارد
                                کنید.
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

@endsection
@section('script')

    <script>
        $('#code-submit').on('click', function () {
            let confirm_type = $('input[name="confirm_type"]').val();
            let one_time_password = $('input[name="one_time_sms"]').val();
            let formData = {
                isObject: true,
                action: "{{route('setting.verify_confirmation_type')}}",
                method: 'post',
                button: $(this),
                data: {
                    confirm_type: confirm_type,
                    one_time_password: one_time_password,
                }
            }
            ajax(formData);
        });

        $('#code-submit2').on('click', function () {
            let confirm_type = $('input[name="confirm_type1"]').val();
            let one_time_password = $('input[name="one_time_password1"]').val();
            let formData = {
                isObject: true,
                action: "{{route('setting.verify_confirmation_type')}}",
                method: 'post',
                button: $(this),
                data: {
                    confirm_type: confirm_type,
                    one_time_password: one_time_password,
                }
            }
            ajax(formData);
        });

        $('#submit').on('click', function () {
            let secret = $('input[name="secret"]').val();
            let type = $('input[name="type"]').val();
            let one_time_password = $('input[name="one_time_password"]').val();
            let formData = {
                isObject: true,
                action: "{{route('setting.status_google_authenticator')}}",
                method: 'post',
                button: $(this),
                data: {
                    secret: secret,
                    type: type,
                    one_time_password: one_time_password,
                }
            }
            ajax(formData);
        });

        $('#deactivate').on('click', function () {
            let type = $('input[name="type"]').val();
            let one_time_password = $('input[name="one_time_password"]').val();
            let formData = {
                isObject: true,
                action: "{{route('setting.status_google_authenticator')}}",
                method: 'post',
                button: $(this),
                data: {
                    type: type,
                    one_time_password: one_time_password,
                }
            }
            ajax(formData);
        });

        var confirm_type = "{{ $user->confirm_type }}"

        function hide_timer() {
            $(".confirmation-code").hide();
            $(".select_confirm_type input[name='confirm_type']:not(:checked)").click();
        }

        let show_confirm_code = function (response, params) {
            $(".select_confirm_type").show();
            $(".waiting").hide();
            if (response.status == 300) {

            }
            if (response.status == 200) {
                let confirm_type = $(".select_confirm_type input[name='confirm_type']:checked").val();
                if (confirm_type == 'sms') {
                    $(".confirmation-code1").show(100);
                    $(".confirmation-code1 input[name='confirm_type']").val(confirm_type);
                } else {
                    $(".confirmation-code").show(100);
                    $(".confirmation-code input[name='confirm_type']").val(confirm_type);
                }

            }
            if (response.time) {
                $(".confirmation-code .time").show();
                timer_count(response.time, '.confirmation-code .time', hide_timer);
            } else {
                $(".confirmation-code .time").hide().text('');
            }
        }

        $(".select_confirm_type input[name='confirm_type']").change(function () {
            if (confirm_type == $(this).val()) {
                $(".confirmation-code").hide(100);
                return;
            }
            if ($(this).val() == 'google-authenticator') {
                $(".select_confirm_type").hide();
                $(".waiting").show();
                $(".confirmation-code label").text('کد تاییدیه SMS')
                let form = $(this).parents('form');
                let params = {};
                ajax(form, show_confirm_code, params);
            } else {
                show_confirm_code({'status': 200});
            }

        })

    </script>
@endsection
