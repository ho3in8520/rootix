@extends('templates.user.master_page')
@section('title_browser')
    حساب جدید-صرافی ارز دیجیتال روتیکس
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title desktop-title">
                حساب جدید
            </h1>

        </div>
    </section>
    <form action=" {{ isset($bank) ? route('banks.update',$bank):route('banks.store') }}"
          method="post">
        @csrf
        @if(isset($bank))
            @method('put')
        @endif
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto">
                    <h5 class="singing-currency-title">
                        اطلاعات حساب بانکی
                    </h5>

                    <div class="row request-row">
                        <div class="col-12 col-lg-5" dir="ltr">
                            <div class="request-boxs">
                                <button type="button" dir="rtl"
                                        class="request-box request-js-box-1 enabled active d-flex justify-between items-center">
                                    <span class="request-box__title">
                                        بانک <br>
                                        <span class="text-danger error-name"></span>

                                    </span>

                                    <div class="d-flex items-center">
                                        <div class="volume-form">
                                            <div class="volume-form__selected">
                                                <div
                                                    class="volume-form__selected-desc-container d-flex items-center justify-between">
                                                    <div class="volume-form__selected-desc justify-between">

                                                        @isset($bank)
                                                            @php
                                                                $logo=get_bank_logo($bank->name);
                                                            @endphp
                                                            <img
                                                                src="{{asset("theme/user/images/bank-details/$logo.svg")}}"
                                                                class="volume-form__img">
                                                            <span class="volume-form__selected-title">
                                                           {{$bank->name}}
                                                        </span>
                                                        @else
                                                            <span class="volume-form__selected-title">
                                                           {{old('name')? old('name'):'بانک را انتخاب کنید'}}
                                                        </span>
                                                        @endisset

                                                        <div class="arr">
                                                            <svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="13"
                                                                height="10"
                                                                viewBox="0 0 18 11.115">
                                                                <path
                                                                    id="Icon_material-expand-more"
                                                                    data-name="Icon material-expand-more"
                                                                    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                    transform="translate(-9 -12.885)"/>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                                <ul id="banks_name"
                                                    class="volume-form__menu first-volume-form__menu-3 new-account__selected">
                                                    @foreach(banks_name() as $item)
                                                        @php
                                                            $bank_logo=get_bank_logo($item);
                                                        @endphp
                                                        <li class="volume-form__item">
                                                            <a href="#" class="volume-form__link">
                                                                <img
                                                                    src="{{asset("theme/user/images/bank-details/$bank_logo.svg")}}"
                                                                    class="volume-form__img"
                                                                    alt="{{$item}}"
                                                                />
                                                                <span class="volume-form__selected-title"
                                                                >{{$item}}</span
                                                                >
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="0"
                                                                    height="0"
                                                                    viewBox="0 0 18 11.115"
                                                                >
                                                                    <path
                                                                        id="Icon_material-expand-more"
                                                                        data-name="Icon material-expand-more"
                                                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                        transform="translate(-9 -12.885)"
                                                                    />
                                                                </svg>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </button>
                                @error('name') <span class="text-danger">{{$message}}</span> @enderror

                                <button type="button" dir="rtl"
                                        class="request-box request-box-2 request-js-box-2 d-flex justify-between items-center
                                         {{isset($bank)?'enabled':''}}">

                                    <span class="request-box__title">
                                        شماره کارت<br>
                                        <span class="text-danger error-card_number"></span>
                                    </span>

                                    <input type="number" name="card_number" {{isset($bank) ?'':'disabled'}} dir="ltr"
                                           class="currency-value-input"
                                           id="currency-value-input-1" placeholder="1235414525688745"
                                           value="{{ isset($bank) ? $bank->card_number: '' }}">
                                </button>
                                @error('card_number') <span class="text-danger">{{$message}}</span> @enderror

                                <button type="button" dir="rtl"
                                        class="request-box request-box-2 request-js-box-3 d-flex justify-between items-center
                                            {{isset($bank)?'enabled':''}}">

                                    <span class="request-box__title">
                                        شماره حساب<br>
                                        <span class="text-danger error-account_number"></span>
                                    </span>

                                    <input type="number" name="account_number" dir="ltr" {{isset($bank) ?'':'disabled'}}
                                    class="currency-value-input"
                                           id="currency-value-input-2" placeholder="123456789101"
                                           value="{{ isset($bank) ? $bank->account_number: '' }}">
                                </button>
                                @error('account_number')<span class="text-danger">{{$message}}</span>@enderror

                                <button type="button" dir="rtl"
                                        class="request-box request-box-2 request-js-box-4 d-flex justify-between items-center
                                            {{isset($bank)?'enabled':''}}">
                                    <span class="request-box__title">
                                        شماره شبا<br>
                                        <span class="text-danger error-sheba_number"></span>
                                    </span>

                                    <div class="shaba">
                                        <span>IR</span>
                                        <input type="text" name="sheba_number" dir="ltr" {{isset($bank) ?'':'disabled'}}
                                                   class="currency-value-input" id="currency-value-input-3" placeholder="421234567891234567891255"
                                               value="{{ isset($bank) ? str_replace('IR','',$bank->sheba_number): '' }}">
                                    </div>

                                </button>
                                @error('sheba_number')<span class="text-danger">{{$message}}</span>@enderror

                            </div>
                        </div>
                        <div class="col-12 col-lg-7">

                            <div class="request-info h-91 d-flex flex-column">
                                <div class="request-info__desc d-flex">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>

                                    <div>

                                        <p>حساب بانکی شما میبایست به نام خودتان باشد و استفاده از کارت بانکی به نام غیر
                                            از
                                            خود خلاف قوانین سایت می باشد.</p>
                                        <p>واریز و برداشت تنها از طریق حساب (های) بانکی تائید شده مقدور است.</p>
                                        <p>از این پس تسویه حساب ها تنها به شماره حسابی که به عنوان پیش فرض انتخاب کرده
                                            اید
                                            واریز خواهد شد.</p>
                                        <p>اگر هنوز حساب بانکی پیش فرض خود را انتخاب نکرده اید در اسرع وقت این کار را
                                            انجام
                                            دهید.</p>
                                    </div>

                                </div>

                                <div class="request-info__desc new-accont__desc d-flex items-center">
                                    <div>
                                        <span class="icon-Group-1728 font-size__icon"></span>


                                    </div>
                                    <p>شماره شبا حتما باید با IR شروع شود (حروف بزرگ)</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="btns-exit">
            <button id="submit" type="button" class="btn signin-btn record-btn" {{isset($bank)?'':'disabled'}}>
                ثبت
            </button>

            <a href="{{route('banks.index')}}" class="btn signin-btn authentication-btn">
                انصراف
            </a>
        </div>
    </form>

@endsection

@section('script')
    <script src="{{asset('theme/user/scripts/sell-request-selected-1.js')}}"></script>
    <script src="{{asset('theme/user/scripts/new-account-selected.js')}}"></script>
    @isset($bank)
        <script>
            let value = {!! json_encode($bank->name) !!};
            $(document).ready(function () {
                $('<input>').attr({
                    type: 'hidden',
                    name: 'name',
                    value: value
                }).appendTo('form');
            })
        </script>
    @else
        <script src="{{asset('theme/user/scripts/sell-request-disabled-btn.js')}}"></script>
    @endisset

    <script>
        $('#submit').on('click', function () {
            let name = $('input[name="name"]').val();
            let card_number = $('input[name="card_number"]').val();
            let account_number = $('input[name="account_number"]').val();
            let sheba_number = $('input[name="sheba_number"]').val();
            let formData = {
                isObject: true,
                action: "{{isset($bank)?route('banks.update.bank',$bank):route('banks.store')}}",
                method: '{{isset($bank)?'post':'post'}}',
                button: $(this),
                data: {
                    name: name,
                    card_number: card_number,
                    account_number: account_number,
                    sheba_number: sheba_number,
                }
            }
            console.log(formData)
            ajax(formData);
        });

        $(document).ready(function () {
            $('#banks_name li').on('click', function () {
                $('input[name="name"]').remove()
                let value = $.trim($(this).text());
                $('<input>').attr({
                    type: 'hidden',
                    name: 'name',
                    value: value
                }).appendTo('form');
            })
        })

    </script>
@endsection
