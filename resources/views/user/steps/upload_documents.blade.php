@extends('templates.user.master_page')
@section('title_browser','دریافت مدارک-احراز هویت')
@section('content')
    <!-- Modal Pay With Cash-->
    <div class="col-12 col-md-6 col-lg-3">
        <div class="modal fade mt-5" id="modalTrain" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body border-0">
                        <h3 class="text-center my-3">تصویر احراز هویت </h3>
                        <p class="my-4">
                            مراحل زیر را برای گرفتن تصویر احراز هویت انجام دهید:
                        </p>
                        <p>1- نوشتن متن تعهد‌نامه:</p>
                        <p>- متن تعهد‌نامه زیر را به صورت خوانا و دست‌نویس روی یک کاغذ سفید بنویسید و امضا کنید.</p>
                        <p class="text-danger">(ضمن مطالعه و تایید قوانین استفاده از خدمات روتیکس، مدارک به جهت احراز
                            هویت در این سایت ارسال گردیده است. همچنین متعهد می‌شوم حساب کاربری و مدارک خود را در اختیار
                            اشخاص غیر قرار ندهم.)</p>
                        <p class="text-danger">نام و نام خانوادگی:</p>
                        <p class="text-danger">تاریخ:</p>
                        <p class="text-danger">امضا:</p>
                        <p>2- مدارک زیر را برای گرفتن عکس احراز هویت آماده کنید:</p>
                        <p>- متن تعهدنامه امضا شده</p>
                        <p>- مدرک هویتی (شناسنامه جدید یا کارت ملی هوشمند یا کارت ملی قدیمی, برای اتباع کارت آمایش)</p>
                        <p>- کارت بانکی ثبت شده در روتیکس(برای اتباع نیازی به کارت بانکی به نام شخص نمی باشد.)</p>
                        <p>الف) کارت ملی (یا آمایش) را در دست چپ طوری نگه دارید که اطلاعات روی کارت مشخص باشد.</p>
                        <p>ب) تعهد‌نامه دست‌نوشته و کارت بانکی‌ای را که اطلاعات آن را در سایت ثبت نموده‌اید، در دست راست
                            خود به گونه‌ای نگه دارید که اطلاعات روی آنها، واضح و اتصال دست‌ها به بدن شما مشخص باشد؛ از
                            شخصی بخواهید از شما در این حالت عکس بگیرد.</p>
                        <p>3- بارگذاری تصویر احراز هویت</p>
                        <p>سپس تصویر گرفته شده را در کادر مربوطه بارگذاری کنید.</p>
                        <p class="text-danger">توجه داشته باشید حداکثر حجم قابل قبول برای هر عکس : 5 مگابایت
                            و تنها فرمت های JPG, PNG, Tiff قابل قبول هستند </p>
                    </div>
                    <div class="modal-footer border-0 justify-content-center">
                        <button data-dismiss="modal"
                                class="btn signin-btn deposit-btn ajaxStore">
                            باشه
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Pay With Cash-->

    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        احراز هویت
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 63%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="progress-circle active"></div>
                                    <div class="progress-circle"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">مرحله یک : اطلاعات تماس</h3>
                                <h3 class="desktop-title">مرحله دو : آدرس و تلفن</h3>
                                <h3 class="desktop-title">مرحله سه : دریافت مدارک</h3>
                                <h3 class="desktop-title">مرحله چهارم : اطلاعات بانکی</h3>
                                <h3 class="d-lg-none">مرحله یک</h3>
                                <h3 class="d-lg-none">مرحله دو</h3>
                                <h3 class="d-lg-none">مرحله سوم</h3>
                                <h3 class="d-lg-none">مرحله چهارم</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form method="post" action="{{ route('step.upload.store') }}">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto" style="min-height: 50vh">
                    <h5 class="singing-currency-title mb-5">آپلود مدرک</h5>

                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="upload-card">
                                <div class="upload-card__header">
                                    <h3 class="upload-card__title">{{auth()->user()->nationality?'کارت آمایش':'کارت ملی'}}</h3>
                                    <p class="upload-card__text">
                                        {{auth()->user()->nationality?'کارت آمایش':'کارت ملی'}} خود را بارگزاری کنید
                                    </p>
                                </div>

                                <div class="upload-card__body drop-area">
                                    <div class="upload-card__btn">
                                        <label for="fileElem" style="cursor: pointer">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="45"
                                                height="45"
                                                viewBox="0 0 58 58"
                                            >
                                                <g
                                                    id="Group_1744"
                                                    data-name="Group 1744"
                                                    transform="translate(-88 -790)"
                                                >
                                                    <circle
                                                        id="Ellipse_115"
                                                        data-name="Ellipse 115"
                                                        cx="29"
                                                        cy="29"
                                                        r="29"
                                                        transform="translate(88 790)"
                                                        fill="#666"
                                                        opacity="0.2"
                                                    />
                                                    <g
                                                        id="Icon_feather-upload"
                                                        data-name="Icon feather-upload"
                                                        transform="translate(105.165 807.772)"
                                                    >
                                                        <path
                                                            id="Path_8632"
                                                            data-name="Path 8632"
                                                            d="M27.874,22.5v5.194a2.6,2.6,0,0,1-2.6,2.6H7.1a2.6,2.6,0,0,1-2.6-2.6V22.5"
                                                            transform="translate(-4.5 -6.917)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                        <path
                                                            id="Path_8633"
                                                            data-name="Path 8633"
                                                            d="M23.486,10.993,16.993,4.5,10.5,10.993"
                                                            transform="translate(-5.306 -4.5)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                        <path
                                                            id="Path_8634"
                                                            data-name="Path 8634"
                                                            d="M18,4.5V20.083"
                                                            transform="translate(-6.313 -4.5)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                    </g>
                                                </g>
                                            </svg>
                                        </label>

                                    </div>

                                    <input
                                        type="file"
                                        id="fileElem"
                                        accept="image/*"
                                        name="image[national_image]"
                                        class="input__file"/>
                                    <div>
                        <span class="upload-card__caption"
                        >فایل را انتخاب کنید</span
                        >
                                        <span class="upload-card__caption"
                                        >یا آن را درون کادر بکشید</span
                                        >
                                    </div>
                                    <div id="gallery" class="upload__image"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="upload-card">
                                <div class="upload-card__header">
                                    <h3 class="upload-card__title">عکس سلفی</h3>
                                    <p class="upload-card__text">
                                        عکس سلفی خود را بارگزاری کنید
                                    </p>
                                </div>

                                <div class="upload-card__body drop-area">
                                    <div class="upload-card__btn">
                                        <label for="fileElem2" style="cursor: pointer">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="45"
                                                 height="45"
                                                 viewBox="0 0 58 58">
                                                <g
                                                    id="Group_1744"
                                                    data-name="Group 1744"
                                                    transform="translate(-88 -790)"
                                                >
                                                    <circle
                                                        id="Ellipse_115"
                                                        data-name="Ellipse 115"
                                                        cx="29"
                                                        cy="29"
                                                        r="29"
                                                        transform="translate(88 790)"
                                                        fill="#666"
                                                        opacity="0.2"
                                                    />
                                                    <g
                                                        id="Icon_feather-upload"
                                                        data-name="Icon feather-upload"
                                                        transform="translate(105.165 807.772)"
                                                    >
                                                        <path
                                                            id="Path_8632"
                                                            data-name="Path 8632"
                                                            d="M27.874,22.5v5.194a2.6,2.6,0,0,1-2.6,2.6H7.1a2.6,2.6,0,0,1-2.6-2.6V22.5"
                                                            transform="translate(-4.5 -6.917)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                        <path
                                                            id="Path_8633"
                                                            data-name="Path 8633"
                                                            d="M23.486,10.993,16.993,4.5,10.5,10.993"
                                                            transform="translate(-5.306 -4.5)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                        <path
                                                            id="Path_8634"
                                                            data-name="Path 8634"
                                                            d="M18,4.5V20.083"
                                                            transform="translate(-6.313 -4.5)"
                                                            fill="none"
                                                            stroke="#2f31d0"
                                                            stroke-linecap="round"
                                                            stroke-linejoin="round"
                                                            stroke-width="3"
                                                        />
                                                    </g>
                                                </g>
                                            </svg>
                                        </label>

                                    </div>

                                    <input type="file"
                                           id="fileElem2"
                                           accept="image/*"
                                           name="image[selfie_image]"
                                           class="input__file"/>
                                    <div>
                                        <span class="upload-card__caption">فایل را انتخاب کنید</span>
                                        <span class="upload-card__caption">یا آن را درون کادر بکشید</span>
                                    </div>
                                    <div id="gallery" class="upload__image"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="upload-card upload-card__admin" style="cursor: pointer">
                                <div class="upload-card__header">
                                    <h3 class="upload-card__title">نمونه عکس سلفی</h3>
                                    <p class="upload-card__text">
                                        برای نمایش عکس در سایز اصلی روی آن کلیک کنید.
                                    </p>
                                </div>

                                <div class="upload-card__body drop-area">
                                    <div class="upload__image button-modal-document">
                                        <img src="{{asset('theme/user/images/authenticate.png')}}" alt="user image"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-3">
                            <button type="button" class="create-new-bank withdraw-btn mt-3" data-toggle="modal"
                                    data-target="#modalTrain">
                                متن عکس سلفی
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <div class="btns-exit">
            <button type="button" class="btn signin-btn record-btn ajaxStore">ثبت</button>

            <a href="#" class="btn signin-btn authentication-btn"> انصراف </a>

            <button class="chat__btn">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="45"
                    height="45"
                    viewBox="0 0 55 55"
                >
                    <g
                        id="Group_1700"
                        data-name="Group 1700"
                        transform="translate(-114 -365)"
                    >
                        <circle
                            id="Ellipse_86"
                            data-name="Ellipse 86"
                            cx="27.5"
                            cy="27.5"
                            r="27.5"
                            transform="translate(114 365)"
                            fill="#666"
                        />
                        <g
                            id="Icon_ionic-ios-chatbubbles"
                            data-name="Icon ionic-ios-chatbubbles"
                            transform="translate(123.5 374.5)"
                        >
                            <path
                                id="Path_8611"
                                data-name="Path 8611"
                                d="M30.3,22.542a1.7,1.7,0,0,1,.232-.858,2.368,2.368,0,0,1,.148-.218,11.393,11.393,0,0,0,1.941-6.349A11.961,11.961,0,0,0,20.412,3.375,12.129,12.129,0,0,0,8.438,12.72a11.3,11.3,0,0,0-.26,2.4A11.927,11.927,0,0,0,20.2,27.014a14.48,14.48,0,0,0,3.319-.541c.795-.218,1.582-.506,1.786-.584a1.859,1.859,0,0,1,.654-.12,1.828,1.828,0,0,1,.71.141l3.987,1.413a.951.951,0,0,0,.274.07.56.56,0,0,0,.563-.562.9.9,0,0,0-.035-.19Z"
                                fill="#fff"
                            />
                            <path
                                id="Path_8612"
                                data-name="Path 8612"
                                d="M22.395,27.6c-.253.07-.577.148-.928.225a12.977,12.977,0,0,1-2.391.316A11.927,11.927,0,0,1,7.052,16.249a13.293,13.293,0,0,1,.105-1.5c.042-.3.091-.6.162-.9.07-.316.155-.633.246-.942L7,13.4A10.464,10.464,0,0,0,3.375,21.27a10.347,10.347,0,0,0,1.744,5.766c.162.246.253.436.225.563s-.837,4.359-.837,4.359a.564.564,0,0,0,.19.541.573.573,0,0,0,.359.127.5.5,0,0,0,.2-.042L9.2,31.029a1.1,1.1,0,0,1,.844.014,11.834,11.834,0,0,0,4.268.844,11.043,11.043,0,0,0,8.445-3.874s.225-.309.485-.675C22.985,27.429,22.69,27.52,22.395,27.6Z"
                                fill="#fff"
                            />
                        </g>
                    </g>
                </svg>
            </button>

        </div>
    </form>
    <div class="modal fade modal-document" id="modal1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="d-flex justify-content-end mb-2">
                    <div class="closed__modal" data-dismiss="modal">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="15.356"
                            height="15.353"
                            viewBox="0 0 12.356 12.353"
                        >
                            <path
                                d="M18.927,17.465l4.413-4.413a1.034,1.034,0,1,0-1.462-1.462L17.465,16,13.052,11.59a1.034,1.034,0,1,0-1.462,1.462L16,17.465l-4.413,4.413a1.034,1.034,0,0,0,1.462,1.462l4.413-4.413,4.413,4.413a1.034,1.034,0,0,0,1.462-1.462Z"
                                transform="translate(-11.285 -11.289)"
                                fill="#1c2426"
                            />
                        </svg>
                    </div>
                </div>
                <div class="modal-header mx-auto border-0">
                    <img
                        src=""
                        alt="user image"
                        width="300"
                        height="300"
                    />
                </div>
                <div class="modal-footer border-0">
                    <button
                        class="btn signin-btn deposit-btn mx-auto"
                        data-dismiss="modal"
                    >
                        تایید
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('theme/user/scripts/preview-file.js') }}"></script>
    <script>
        $(document).on('click', '.button-modal-document', function () {
            var val = $(this).find('img').attr('src');
            $('.modal-document .modal-header img').attr('src', val);
            $('.modal-document').modal('show');
        })
    </script>
@endsection
