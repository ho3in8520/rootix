@extends('templates.user.master_page')
@section('title_browser','تکمیل مشخصات-احراز هویت')
@section('style')
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        احراز هویت
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 88%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div>
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            class="checked"
                                            width="40"
                                            height="40"
                                            viewBox="0 0 54.854 54.455"
                                        >
                                            <g
                                                id="Group_1734"
                                                data-name="Group 1734"
                                                transform="translate(0 -0.455)"
                                            >
                                                <circle
                                                    id="Ellipse_96"
                                                    data-name="Ellipse 96"
                                                    cx="27"
                                                    cy="27"
                                                    r="27"
                                                    transform="translate(0.427 0.455)"
                                                    fill="#8592c9"
                                                />
                                                <path
                                                    id="Path_8619"
                                                    data-name="Path 8619"
                                                    d="M27.427,42.341A27.484,27.484,0,0,1,0,15.6v.686a27.427,27.427,0,0,0,54.854,0V15.6A27.368,27.368,0,0,1,27.427,42.341Z"
                                                    transform="translate(0 11.198)"
                                                    fill="#5b6ba1"
                                                />
                                                <path
                                                    id="Path_8620"
                                                    data-name="Path 8620"
                                                    d="M37.33,10.971l-.857-.857A1.482,1.482,0,0,0,35.445,9.6h0a1.222,1.222,0,0,0-1.029.514L19.5,25.028l-8.057-7.885a1.8,1.8,0,0,0-2.228,0L8.361,18a1.564,1.564,0,0,0,0,2.057l9.428,9.428a2.553,2.553,0,0,0,1.543.686,1.861,1.861,0,0,0,1.543-.686L37.33,13.028A1.482,1.482,0,0,0,37.845,12,1.482,1.482,0,0,0,37.33,10.971Z"
                                                    transform="translate(5.696 6.913)"
                                                    fill="#fff"
                                                />
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="progress-circle active"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">مرحله یک : اطلاعات تماس</h3>
                                <h3 class="desktop-title">مرحله دو : آدرس و تلفن</h3>
                                <h3 class="desktop-title">مرحله سه : دریافت مدارک</h3>
                                <h3 class="desktop-title">مرحله چهارم : اطلاعات بانکی</h3>
                                <h3 class="d-lg-none">مرحله یک</h3>
                                <h3 class="d-lg-none">مرحله دوم</h3>
                                <h3 class="d-lg-none">مرحله سوم</h3>
                                <h3 class="d-lg-none">مرحله چهارم</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form method="post" action="{{ route('step.bank.store') }}">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto">
                    <h5 class="singing-currency-title mb-2 mt-3">
                        اطلاعات حساب بانکی
                    </h5>

                    <div class="row request-row">
                        <div class="col-12 col-lg-5" dir="ltr">
                            <div class="request-boxs">
                                <div
                                    dir="rtl"
                                    class="
                                    request-box
                                    request-js-box-1
                                    enabled
                                    active
                                    d-flex
                                    justify-between
                                    items-center
                                  ">
                                    <span class="request-box__title">
                                        بانک
                                        <br><span class="text-danger error-name_bank"></span>
                                    </span>

                                    <select class="selected-bank" name="name_bank"></select>

                                    <div class="d-flex items-center">
                                        <div class="volume-form">
                                            <div
                                                class="volume-form__selected"
                                            >
                                                <div
                                                    class="volume-form__selected-desc-container d-flex items-center justify-between"
                                                >
                                                    <div class="volume-form__selected-desc justify-between">

                                                        <span
                                                            class="volume-form__selected-title ">بانک را انتخاب کنید</span>

                                                        <div class="arr">
                                                            <svg
                                                                xmlns="http://www.w3.org/2000/svg"
                                                                width="13"
                                                                height="10"
                                                                viewBox="0 0 18 11.115"
                                                            >
                                                                <path
                                                                    id="Icon_material-expand-more"
                                                                    data-name="Icon material-expand-more"
                                                                    d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                    transform="translate(-9 -12.885)"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                                <ul class="volume-form__menu first-volume-form__menu-3 new-account__selected"
                                                    style="overflow-y: auto">
                                                    @foreach(banks() as $bank)
                                                        <li class="volume-form__item" data-value="{{ $bank }}">
                                                            <a class="volume-form__link">
                                                                <img
                                                                    src="{{asset("theme/user/images/bank-details/".get_bank_logo($bank).".svg")}}"
                                                                    class="volume-form__img"
                                                                    alt="{{ $bank }}"
                                                                />
                                                                <span
                                                                    class="volume-form__selected-title">{{ $bank }}</span>
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="0"
                                                                    height="0"
                                                                    viewBox="0 0 18 11.115">
                                                                    <path
                                                                        id="Icon_material-expand-more"
                                                                        data-name="Icon material-expand-more"
                                                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                        transform="translate(-9 -12.885)"
                                                                    />
                                                                </svg>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div dir="rtl"
                                     class="
                                  request-box
                                  request-box-2
                                  request-js-box-2
                                  d-flex
                                  enabled
                                  justify-between
                                  items-center
                                ">

                                <span class="request-box__title">
                                    شماره کارت
                                    <br><span class="text-danger error-card"></span>
                                </span>
                                    <input type="text" dir="ltr" class="currency-value-input" name="card"
                                           id="currency-value-input-1" placeholder="100">
                                </div>

                                <div dir="rtl"
                                     class="
                                  request-box
                                  request-box-2
                                  request-js-box-3
                                  enabled
                                  d-flex
                                  justify-between
                                  items-center
                                ">

                                <span class="request-box__title">
                                    شماره حساب
                                    <br><span class="text-danger error-account_number"></span>
                                </span>
                                    <input type="text" dir="ltr" class="currency-value-input" name="account_number"
                                           id="currency-value-input-2" placeholder="100">
                                </div>

                                <div
                                    dir="rtl"
                                    class="
                                  request-box
                                  request-box-2
                                  request-js-box-4
                                  d-flex
                                  enabled
                                  justify-between
                                  items-center
                                "
                                >
                                <span class="request-box__title">
                                    شماره شبا
                                    <br><span class="text-danger error-sheba"></span>
                                </span>
                                    <div class="shaba w-100">
                                        <span> - IR</span>
                                        <input type="text" dir="ltr" class="currency-value-input" name="sheba"
                                               id="currency-value-input-3" placeholder="100">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class="request-info h-91 d-flex flex-column">
                                <div class="request-info__desc d-flex">
                                    <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                            class="path2"></span></span>
                                    <div>

                                        <p>حساب بانکی شما میبایست به نام خودتان باشد و استفاده از کارت بانکی به نام غیر
                                            از خود خلاف قوانین سایت می باشد.</p>
                                        <p>واریز و برداشت تنها از طریق حساب (های) بانکی تائید شده مقدور است.</p>
                                        <p>از این پس تسویه حساب ها تنها به شماره حسابی که به عنوان پیش فرض انتخاب کرده
                                            اید واریز خواهد شد.</p>
                                        <p>اگر هنوز حساب بانکی پیش فرض خود را انتخاب نکرده اید در اسرع وقت این کار را
                                            انجام دهید.</p>
                                    </div>

                                </div>

                                <div class="request-info__desc new-accont__desc d-flex items-center">
                                    <div>
                                        <span class="icon-Group-1728 font-size__icon"></span>


                                    </div>
                                    <p>شماره شبا حتما باید با IR شروع شود (حروف بزرگ)</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="btns-exit">
            <button class="btn signin-btn record-btn ajaxStore" type="submit">
                ثبت
            </button>

            <a href="/" class="btn signin-btn authentication-btn">
                انصراف
            </a>
        </div>
    </form>
@endsection

@section('script')
    <script src="{{ asset('theme/user/scripts/selected-option.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/just-get-number.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/sell-request-selected-1.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/auth-4.js') }}"></script>
@endsection
