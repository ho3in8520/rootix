@extends('templates.user.master_page')
@section('title_browser','تکمیل مشخصات-احراز هویت')
@section('style')
@endsection
@section('content')
    <section class="pt-2 pr-3">
        <h1 class="sel-request-title desktop-title text-nowrap ml-5">
            احراز هویت
        </h1>
    </section>
    <section class="pt-2" style="min-height: 59vh">
        <div class="container">
            <div class="panel-box h-auto">
                <div class="correct">
                    <svg
                        class="checkmark-2"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 52 52"
                    >
                        <circle
                            class="checkmark__circle-2"
                            cx="26"
                            cy="26"
                            r="25"
                            fill="none"
                        />
                        <path
                            class="checkmark__check-2"
                            fill="none"
                            d="M14.1 27.2l7.1 7.2 16.7-16.8"
                        />
                    </svg>
                </div>

                <div class="text-center">
                    <h5 class="singing-currency-title">
                        اطلاعات شما ثبت شد و در انتظار تایید اپراتور است
                    </h5>
                    <h5 class="singing-currency-title">
                        با تشکر از صبوری شما
                    </h5>
                    <h5 class="singing-currency-title">
                        لینک دعوت شما
                    </h5>

                    <a href="https://rootix.io/auth/register?referral_code={{ auth()->user()->code }}" class="transaction-title">
                        https://rootix.io/auth/register?referral_code={{ auth()->user()->code }}
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection
