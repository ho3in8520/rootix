@extends('templates.user.master_page')
@section('title_browser',' شماره تماس و ایمیل-احراز هویت')
@section('style')
    <link href="{{ asset('theme/user/styles/cantactInfo.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <div class="d-flex">
                <div class="">
                    <h1 class="sel-request-title desktop-title text-nowrap ml-5">
                        احراز هویت
                    </h1>
                </div>
                <div class="w-100">
                    <div class="progressbar-container-container">
                        <div class="progressbar-container">
                            <div class="progressbar"></div>
                            <div class="progress" style="width: 13%"></div>
                            <div class="d-flex justify-center">
                                <div class="progress-circles">
                                    <div class="progress-circle active"></div>
                                </div>
                            </div>
                            <div class="progressbar-titles auto-progress-title">
                                <h3 class="desktop-title">مرحله یک : اطلاعات تماس</h3>
                                <h3 class="desktop-title">مرحله دو : آدرس و تلفن</h3>
                                <h3 class="desktop-title">مرحله سه : دریافت مدارک</h3>
                                <h3 class="desktop-title">مرحله چهارم : اطلاعات بانکی</h3>
                                <h3 class="d-lg-none">مرحله یک</h3>
                                <h3 class="d-lg-none">مرحله دوم</h3>
                                <h3 class="d-lg-none">مرحله سوم</h3>
                                <h3 class="d-lg-none">مرحله چهارم</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contactInfo-->
    <section class="contacts mt-3">
        <div class="container">
            <div class="cantacts-box p-3">
                <span class="f-text text-center d-block mt-3 mb-5">اطلاعات تماس خود را وارد کنید</span>
                <div class="">
                    <img alt="" class="i-img ml-1 mb-1 mb-md-0"
                         src="{{ asset('theme/user/images/contactInfo/Group%201728.png') }}">
                    <span class="t-text">شماره همراه خود را وارد کنید،بعد از ثبت شماره کد تایید بری شماپیامک میشود.کدتایید را وارد نماییدو دکمه ثبت را بزنید.</span>
                </div>
                <div style="margin: 100px 0;">
                    <div class="row mb-5 mb-md-2">
                        <div class="col-12 col-lg-5">
                            <div class="item-right d-flex justify-between p-4 mb-1 mb-md-0">
                                <span class="ttext text-right">
                                    شماره تماس
                                    <p class="text-danger error-number"></p>
                                </span>
                                <input placeholder="09177777878" type="number" name="number"
                                       {{ !empty($result['sms'])?'disabled':'' }}
                                       class="ttext mobile-number"
                                       value="{{!empty($result['sms'])? $result['sms']['user']->mobile:'' }}"/>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class="d-flex items-center mx-3 text-con">
                                <img alt="" class="i-img ml-3"
                                     src="{{ asset('theme/user/images/contactInfo/Group%201738.png') }}">
                                <div class="l-text">در صورتی که شماره همراه شما تغییرپیداکرد به پشتیبانی روتیکس
                                    خبربدهید تا شماره شما در سامانه ثبت شود
                                    و نیاز به ذکراست تمامی پیامک های معاملات به خط ثبت شده شما ارسال میشود.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="codeMessage" class="row  mb-5 mb-md-2" style="
                    @if(empty($result['sms'])) display: none @endif">
                        <div class="col-12 col-lg-5">
                            <div class="item-right d-flex justify-between p-4">
                                <span class="ttext">کد تایید</span>
                                <input placeholder="123456" type="number" name="code" class="ttext"/>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7">
                            <div class=" d-flex items-center mx-3 pt-3">
                                <img class="i-img ml-3"
                                     src="{{ asset('theme/user/images/contactInfo/Group%201738.png') }}">
                                <div class="l-text ">
                                    <span>ارسال دوباره کد در :</span>
                                    <span class="stopwatch timer"></span>
                                </div>
                                <button class="resend-btn px-4 mr-3" disabled>ارسال مجدد</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="btns d-flex justify-center my-5">
            <button id="text" class="my-btn @if(!empty($result['sms'])) button-verify @else sendCode @endif" @if(empty($result['sms'])) disabled @endif style="background-color: #2f31d0;">
                @if(!empty($result['sms'])) تایید کد @else دریافت کد @endif
            </button>
            <button class="i-chat mr-4">
                <img src="{{ asset('theme/user/images/contactInfo/Group%201700.png') }}" alt="">
            </button>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".mobile-number").keyup(function () {
            $('.error-number').text('');
            $(".sendCode").removeAttr('disabled');
            $("input[name='number']").css({'border-color': '#A6A9AE'});
            var inputVal = $(this).val();
            if (!inputVal) {
                return false;
                $(".sendCode").attr('disabled', true)
            }
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(inputVal)) {
                $('.error-number').text('فرمت شماره موبایل صحیح نیست');
                $(".sendCode").attr('disabled', true)
            }
        });

        let sendCodeCallback = function (response, params) {
            if (response.status == 100) {
                swal(response.msg, '', 'success');
                $("input[name='number']").attr('disabled', true);
                if (params.type == 'send') {
                    $('.sendCode').removeClass("sendCode").addClass("button-verify").text("تایید کد");
                    $('#codeMessage').show();
                    $('.resend-btn').attr('disabled', true);
                } else {
                    $('.resend-btn').text("ارسال مجدد").attr('disabled', true);
                }
                time(response.time);
            }
        }

        let verifyCodeCallback = function (response, params) {
            $(".button-verify").text('تایید کد')
        }

        $(document).on("click", '.sendCode', function () {
            var elem = $(this);
            var text_btn = elem;
            elem.text('در حال ارسال SMS ...');
            var number = $("input[name='number']").val();
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(number)) {
                $('.error-number').text('فرمت شماره موبایل صحیح نیست');
                $(".sendCode").attr('disabled', true);
                return;
            }
            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    number: number,
                    mode: 1
                },
                isObject: true,
            }
            ajax(form_obj, sendCodeCallback, {type: 'send'});
        });

        $(document).on('click', '.resend-btn', function () {
            var elem = $(this);
            elem.text('در حال ارسال SMS ...');
            var number = $("input[name='number']").val();
            var characterReg = /^(0)?9\d{9}$/;
            if (!characterReg.test(number)) {
                $('.error-number').text('فرمت شماره موبایل صحیح نیست');
                $(".sendCode").attr('disabled', true);
                return;
            }

            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    number: number,
                    mode: 1
                },
                isObject: true,
            }
            ajax(form_obj, sendCodeCallback, {btnClass: 'resend'});
        })

        $(document).on("click", '.button-verify', function () {
            var elem = $(this);
            elem.text('در حال بررسی ...');
            var code = $("input[name='code']").val();
            const form_obj = {
                action: '{{ route('step.send-verify') }}',
                method: 'post',
                data: {
                    _token: `{{ csrf_token() }}`,
                    code: code,
                    mode: 2
                },
                isObject: true,
            }
            ajax(form_obj,verifyCodeCallback);
        })

        function time(time) {
            var minutes = 1;
            var seconds = minutes * time;

            function convertIntToTime(num) {
                var mins = Math.floor(num / 60);
                var secs = num % 60;
                var timerOutput = (mins < 10 ? "0" : "") + mins + ":" + (secs < 10 ? "0" : "") + secs;
                return (timerOutput);
            }

            var countdown = setInterval(function () {
                var current = convertIntToTime(seconds);
                $('.timer').text(current);

                if (seconds == 0) {
                    clearInterval(countdown);
                }
                seconds--;
                if (seconds < 0) {
                    $("input[name='number'], .resend-btn").removeAttr('disabled');
                }
            }, 1000);
        }

        @if($result && !empty($result['sms']))
        time(`{{ $result['sms']['time'] }}`);
        @endif

    </script>
@endsection
