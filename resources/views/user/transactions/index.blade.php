@extends('templates.user.master_page')
@section('title_browser')
    لیست تراکنش ها-صرافی ارز دیجیتال روتیکس
@endsection
@section('content_class','bank-content')
@section('')
    <div style="min-height: 100vh" class="d-flex flex-column justify-center">
        @endsection
        @section('content')
            <section id="basic-form-layouts" class="wallet-cart pt-2">
                <div class="container">
                    <h1 class="dashboard-title bank-title transaction-subtitle desktop-title mb-4">
                        لیست تراکنش ها
                    </h1>
                    <div>
                        <div class="dashboard-table transaction-table-container">
                            <form action="" method="get">
                                @csrf
                                <div class="d-flex align-items-end justify-between header-transaction mb-5">
                                    <div class="selected-transaction d-flex">
                                        <div class="w-100 bg-white">
                                            <span class="mb-2 d-block transaction__filter-title">واریز / برداشت</span>
{{--                                            <div class="custom-selected">--}}
                                                <select id="type" name='type'>
                                                    <option value="">همه</option>
                                                    <option value="1" {{ request()->type=='1'?'selected':'' }}>برداشت
                                                    </option>
                                                    <option value="2" {{ request()->type=='2'?'selected':'' }}>واریز
                                                    </option>
                                                </select>
{{--                                            </div>--}}
                                        </div>
                                        <div class="w-100 mr-2 bg-white">
                                            <span class="mb-2 d-block transaction__filter-title">ارز</span>
{{--                                            <div class="custom-selected">--}}
                                                <select id="unit" name="unit">
                                                    <option value="">همه</option>
                                                    @foreach($currencies as $currency)
{{--                                                        @dd($currency)--}}
                                                    <option value="{{$currency}}" {{ request()->unit==$currency?'selected':'' }}>
                                                        {{$currency}}
                                                    </option>
                                                    @endforeach
                                                </select>
{{--                                            </div>--}}
                                        </div>
                                    </div>

                                    <div class="volume-transaction bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">مقدار <sub class="text-danger">({{get_unit()}})</sub></span>

                                        <div class="d-flex">
                                            <input name="amount_from" type="text" placeholder="از"
                                                   class="transaction__value"
                                                   value="{{ request()->amount_from?request()->amount_from:'' }}"/>
                                            <input name="amount_to" type="text" placeholder="تا"
                                                   class="transaction__value"
                                                   value="{{ request()->amount_to?request()->amount_to:'' }}"/>
                                        </div>
                                    </div>

                                    <div class="date-transaction bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">تاریخ</span>
                                        <div class="d-flex">
                                            <input id="date" name="date_from" class="datePicker" placeholder="از تاریخ"
                                                   value="{{ request()->date_from?request()->date_from:'' }}" readonly/>
                                            <input name="date_to" class="datePicker" placeholder="تا تاریخ"
                                                   value="{{ request()->date_to?request()->date_to:'' }}" readonly/>
                                        </div>
                                    </div>

                                    <div class="destination-address-input bg-white">
                                        <span class="mb-2 d-block transaction__filter-title">آدرس مقصد</span>
                                        <input type="text" name="destination_address" placeholder="آدرس مقصد..."
                                               value="{{ request()->destination_address?request()->destination_address:'' }}"/>
                                    </div>

                                    <button class="transaction__btn search-ajax">جستجو</button>
                                </div>
                            </form>
                            <div class="table-loading">
                                <img
                                    src="{{asset('theme/user/images/loader.svg')}}"
                                    class="table-loding-spinner"
                                />
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="overflow-auto">
                                        <div class="wallet-table-container">
                                            <table class="wallet-table bank-table transaction-table">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        <div>شناسه</div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            نوع

                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            ارز

                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        <div>
                                                            مقدار

                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            توضیحات

                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>
                                                            آدرس مقصد

                                                            <button class="sort-table-btn">
                                                                <svg
                                                                    xmlns="http://www.w3.org/2000/svg"
                                                                    width="10"
                                                                    height="20.383"
                                                                    viewBox="0 0 12.203 20.383"
                                                                >
                                                                    <path
                                                                        id="Icon_material-unfold-more"
                                                                        data-name="Icon material-unfold-more"
                                                                        d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                                                        transform="translate(-11.115 -4.5)"
                                                                        opacity="0.7"
                                                                    />
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </th>

                                                    <th>
                                                        <div>زمان</div>
                                                    </th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @if(count($transactions) > 0)
                                                    @foreach($transactions as $transaction)
                                                        <tr>
                                                            <td>
                                                                @if($transaction->tracking_code)
                                                                    <span id="tracking_{{$transaction->id}}"
                                                                          class="font-12 mr-2 copy-btn"
                                                                          data-copy="tracking_{{$transaction->id}}"
                                                                          data-content="{{$transaction->tracking_code}}"
                                                                          onclick="copy_text('tracking_{{$transaction->id}}')">
                                                        {!! \Illuminate\Support\Str::limit($transaction->tracking_code,12) !!}
                                                    </span>
                                                                @else
                                                                    <span>-</span>
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <div
                                                                    class="{{ $transaction->type==1?'harvest':'deposit' }}-status situation">
                                                                    {{ $transaction->type==1?'برداشت':'واریز' }}
                                                                </div>
                                                            </td>

                                                            @if ($transaction->financeable instanceof \App\Models\Asset)
                                                                <td>{{ $transaction->financeable->unit }}</td>
                                                            @else
                                                                <td>-</td>
                                                            @endif

                                                            <td>{{ rial_to_unit($transaction->amount,$transaction->financeable->unit,true) }}</td>

                                                            <td>{{$transaction->description}}</td>

                                                            <td>
                                                                @if($transaction->extra_field1)
                                                                    <span id="destination_{{$transaction->id}}"
                                                                          class="font-12 mr-2 copy-btn"
                                                                          data-content="{{$transaction->extra_field1}}"
                                                                          data-copy="destination_{{$transaction->id}}"
                                                                          onclick="copy_text('destination_{{$transaction->id}}')">
                                                        {!! \Illuminate\Support\Str::limit($transaction->extra_field1,12) !!}
                                                    </span>
                                                                @else
                                                                    -
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <p>{{ jdate_from_gregorian($transaction->created_at,'%d %B %Y') }}</p>
                                                                <p>{{ jdate_from_gregorian($transaction->created_at,'H:i:s') }}</p>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        @endsection
        @section('script')

{{--            <script src="{{ asset('general/js/popup.js') }}"></script>--}}
            <script src="{{asset('theme/user/scripts/selected-box.js?v=1.0.0')}}"></script>

            <script>
                function copy_text(id) {
                    // // alert(id);
                    var copyText = document.getElementById(id);
                    var input = document.createElement("input");
                    input.value = $.trim(copyText.getAttribute('data-content'));
                    document.body.appendChild(input);
                    input.select();
                    document.execCommand("Copy");
                    input.remove();
                    swal('متن مورد نظر با موفقیت کپی شد', '', 'success');
                }
                $('#date').on('click',function (){
                    // alert('click here')
                })
            </script>


@endsection
