@extends('templates.user.master_page')
@section('title_browser')
    لیست تیکت ها-صرافی ارز دیجیتال روتیکس
@endsection
@section('content_class','bank-content')

@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">تیکت ها</h1>
                @can('ticket_create')
                    <a href="{{route('tickets.create')}}" class="create-new-bank">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18.111"
                            height="18.111"
                            viewBox="0 0 18.111 18.111">
                            <g
                                id="Icon_feather-plus"
                                data-name="Icon feather-plus"
                                transform="translate(-6.5 -6.5)">
                                <path
                                    id="Path_8591"
                                    data-name="Path 8591"
                                    d="M18,7.5V23.611"
                                    transform="translate(-2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"/>
                                <path
                                    id="Path_8592"
                                    data-name="Path 8592"
                                    d="M7.5,18H23.611"
                                    transform="translate(0 -2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"/>
                            </g>
                        </svg>
                        ثبت تیکت جدید
                    </a>
                @endcan
            </div>

            <div>
                <div class="dashboard-table">
                    <div class="row">
                        <div class="col-12">
                            <div class="overflow-auto">
                                <div class="wallet-table-container">
                                    <table class="wallet-table bank-table ticket__table">
                                        <thead>
                                        <tr>
                                            <th>
                                                <div>
                                                    شناسه
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    عنوان
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    دپارتمان
                                                </div>
                                            </th>
                                            <th>
                                                <div>
                                                    وضعیت
                                                </div>
                                            </th>

                                            <th>
                                                <div>
                                                    تاریخ ایجاد
                                                </div>
                                            </th>

                                            <th>
                                            </th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @if(count($tickets)>0)
                                            @foreach($tickets as $ticket)
                                                @php
                                                    switch ($ticket->status){
                                                        case 1 :
                                                        case 3:
                                                            $status=['status'=>'در حال بررسی','class'=>'minus'];
                                                            break;
                                                        case 2:
                                                            $status=['status'=>'پاسخ داده شد','class'=>'plus'];
                                                            break;
                                                        case 4:
                                                            $status=['status'=>'بسته شد','class'=>'plus'];
                                                            break;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="text-right">
                                                            {{$ticket->ticket_code}}
                                                        </div>
                                                    </td>

                                                    <td>{!! \Illuminate\Support\Str::limit($ticket->title,20) !!}</td>

                                                    <td>{{$ticket->role_id==1?'پشتیبانی':'فنی'}}</td>

                                                    <td>

                                    <span class="{{$status['class']}}">
                                        {{$status['status']}}
                                    </span>
                                                    </td>

                                                    <td>{{jdate_from_gregorian($ticket->created_at,'%d %B %Y')}}</td>

                                                    <td dir="ltr">
                                                        @can('ticket_show')
                                                            <a href="{{route('tickets.show',$ticket->id)}}"
                                                               class="create-new-bank withdraw-btn">
                                                                مشاهده
                                                            </a>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6">تیکتی برای نمایش وجود ندارد</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    @if(count($tickets)>0)
                                        <div class="pagination__container">
                                            {!! $tickets->links() !!}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')

@endsection
