@extends('templates.user.master_page')
@section('title_browser','تبادل ارز-صرافی ارز دیجیتال روتیکس')
@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title desktop-title">تبادل سریع ارز ها</h1>
            <div class="panel-box h-auto">
                <div class="swap-container-currency">
                    <div class="d-flex items-center container-seleted-swap">
                        @php
                            $first_swap_assets= $swap_assets->first();
                            $unit= strtoupper($first_swap_assets->unit)
                        @endphp
                        <div class="d-flex items-center justify-between">
                            <label for="send-input" class="swap-label__responsive">مقدار ارسال</label>
                            <p style="white-space: nowrap;width: auto;" class="swap-label__responsive">
                                موجودی :
                                <span dir="ltr"
                                      class="send-inventory">{{ strtoupper(rial_to_unit($first_swap_assets->amount, $first_swap_assets->unit, true)) }}</span>
                            </p>
                        </div>
                        <div class="volume-form-3">
                            <div class="d-flex items-center w-100 bgitems">
                                <div class="volume-form__input-container-3">
                                    <label for="send-input">مقدار ارسال</label>
                                    <input type="text" id="send-input"/>
                                </div>
                                <div class="line-3"></div>
                                <button type="button" class="volume-form__selected-3">
                                    <div class="volume-form__selected-desc-container-3">
                                        <div class="volume-form__selected-desc-3">
                                            <div class="volume-form__selected-title-container-3 d-flex items-center">
                                                <img src="{{ $list_currencies[$unit]['logo_url'] }}"
                                                     class="volume-form__img-3" alt="{{ $first_swap_assets->name }}"/>
                                                <span
                                                    class="volume-form__selected-title-3">{{ $first_swap_assets->name }}</span>
                                            </div>
                                            <div class="arr">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width="10"
                                                    height="10"
                                                    viewBox="0 0 18 11.115">
                                                    <path
                                                        id="Icon_material-expand-more"
                                                        data-name="Icon material-expand-more"
                                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                        transform="translate(-9 -12.885)"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="volume-form__menu-3 mr7-select currencies send-ul">
                                        @foreach($swap_assets as $asset)
                                            @php $unit= strtoupper($asset->unit) @endphp
                                            <li class="volume-form__item-3 {{ $asset->unit === $first_swap_assets->unit?'selected':'' }}"
                                                data-inventory="{{ $asset->amount }}"
                                                data-unit="{{ strtolower($asset->unit) }}">
                                                <a href="#" class="volume-form__link-3">
                                                    <img src="{{ $list_currencies[$unit]['logo_url'] }}"
                                                         class="volume-form__img-3" alt="{{ $asset->name }}"/>
                                                    <span
                                                        class="volume-form__selected-title-3">{{ $asset->name }}</span>
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="0"
                                                        height="0"
                                                        viewBox="0 0 18 11.115">
                                                        <path
                                                            id="Icon_material-expand-more"
                                                            data-name="Icon material-expand-more"
                                                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                            transform="translate(-9 -12.885)"
                                                        />
                                                    </svg>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-between items-center swap-currency">

                            <div class="d-flex swap-volume d-res-none">
                                <p>موجودی : </p>
                                <p dir="ltr"
                                   class="send-inventory">{{ strtoupper(rial_to_unit($first_swap_assets->amount, $first_swap_assets->unit, true)) }}</p>
                            </div>

                            <div class="d-flex items-center swap-wage d-res-none">
                                <p>کارمزد صرافی :</p>
                                <div class="d-flex flex-column items-end">
                                    <h4 class="price fee-on-usdt">-</h4>
                                    <p class="total-value fee-amount">-</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="d-flex justify-between">
                        <div class="swap-related">
                            <div class="related-circle">

                            </div>
                        </div>
                        <div class="d-flex justify-between items-center swap-currency center-swap-currency d-res-none">
                            <p>مقدار ارسال:</p>
                            <div class="d-flex flex-column items-end">
                                <h4 class="price swap-minus send-on-usdt">-</h4>
                                <p class="total-value send-amount">-</p>
                            </div>
                        </div>
                    </div>


                    <div class="d-flex items-center container-seleted-swap">
                        @php
                            $assets->push($assets->shift());
                            $first_assets= $assets->first();
                                $unit= strtoupper($first_assets->unit);
                        @endphp
                        <div class="d-flex items-center justify-between">
                            <label for="receive-input" class="swap-label__responsive">مقدار دریافت</label>
                            <p style="white-space: nowrap;width: auto;" class="swap-label__responsive">
                                موجودی :
                                <span dir="ltr"
                                      class="receive-inventory">{{ strtoupper(rial_to_unit($first_assets->amount, $first_assets->unit, true)) }}</span>
                            </p>

                        </div>
                        <div class="volume-form-3 volume-form-4">
                            <div class="d-flex items-center justify-between w-100 bgitems">
                                <div class="volume-form__input-container-3 volume-form__input-container-4">
                                    <label for="receive-input">مقدار دریافت</label>
                                    <p class="disabled-p" id="receive-input">-</p>
                                    <p class="disabled-p swap-err error"></p>
                                </div>
                                <div class="line-3"></div>
                                <button type="button" class="volume-form__selected-3 volume-form__selected-4">
                                    <div
                                        class="volume-form__selected-desc-container-3 volume-form__selected-desc-container-4">
                                        <div class="volume-form__selected-desc-3 volume-form__selected-desc-4">
                                            <div
                                                class="volume-form__selected-title-container-3 volume-form__selected-title-container-4 d-flex items-center">
                                                <img src="{{ $list_currencies[$unit]['logo_url'] }}"
                                                     class="volume-form__img-3 volume-form__img-4"
                                                     alt="{{ $first_assets->name }}"/>
                                                <span
                                                    class="volume-form__selected-title-3 volume-form__selected-title-4">{{ $first_assets->name }}</span>
                                            </div>
                                            <div class="arr">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    width="10"
                                                    height="10"
                                                    viewBox="0 0 18 11.115"
                                                >
                                                    <path
                                                        id="Icon_material-expand-more"
                                                        data-name="Icon material-expand-more"
                                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                        transform="translate(-9 -12.885)"
                                                    />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="volume-form__menu-3 volume-form__menu-4 mr7-select currencies receive-ul">
                                        @foreach($assets as $asset)
                                            @php $unit= strtoupper($asset->unit) @endphp
                                            <li class="volume-form__item-3 volume-form__item-4 {{ $asset->unit === $first_assets->unit?'selected':'' }}"
                                                data-inventory="{{ $asset->amount }}"
                                                data-unit="{{ strtolower($asset->unit) }}">
                                                <a href="#" class="volume-form__link-3 volume-form__link-4">
                                                    <img src="{{ $list_currencies[$unit]['logo_url'] }}"
                                                         class="volume-form__img-3 volume-form__img-4"
                                                         alt="{{ $asset->name }}"/>
                                                    <span
                                                        class="volume-form__selected-title-3 volume-form__selected-title-4">{{ $asset->name }}</span>
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="0"
                                                        height="0"
                                                        viewBox="0 0 18 11.115"
                                                    >
                                                        <path
                                                            id="Icon_material-expand-more"
                                                            data-name="Icon material-expand-more"
                                                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                            transform="translate(-9 -12.885)"
                                                        />
                                                    </svg>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </button>
                            </div>
                        </div>
                        <div class="d-flex justify-between items-center swap-currency">
                            <div class="d-flex swap-volume d-res-none">
                                <p>موجودی : </p>
                                <p dir="ltr"
                                   class="receive-inventory">{{ strtoupper(rial_to_unit($first_assets->amount, $first_assets->unit, true)) }}</p>
                            </div>

                            <div
                                class="d-flex justify-between items-center swap-currency w-100 center-swap-currency d-lg-none">
                                <p>مقدار ارسال:</p>
                                <div class="d-flex flex-column items-end">
                                    <h4 class="price swap-minus send-on-usdt">-</h4>
                                    <p class="total-value send-amount">-</p>
                                </div>
                            </div>


                            <div class="d-flex align-items-center swap-wage d-lg-none d-res-block">
                                <p>کارمزد صرافی :</p>
                                <div class="d-flex flex-column items-end">
                                    <h4 class="price fee-on-usdt">-</h4>
                                    <p class="total-value fee-amount">-</p>
                                </div>
                            </div>

                            <div class="d-flex items-center swap-wage my-0">
                                <p>مقدار دریافت :</p>
                                <div class="d-flex flex-column items-end">
                                    <h4 class="price swap-plus receive-on-usdt">-</h4>
                                    <p class="total-value receive-amount">-</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @can('swap_store')
                    <button data-toggle="modal" data-target="#swap-modal" id="swap-btn"
                            class="btn signin-btn record-btn swap-record w-100-res">
                        ثبت
                    </button>
                @endcan
            </div>
        </div>
        <div class="modal fade text-left" id="swap-modal" style="display: none;">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header bg-warning white">
                        <h5 class="modal-title" id="myModalLabel140">فرم تایید سواپ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th scope="row">ارز مبدا</th>
                                    <td class="source-unit"></td>
                                </tr>
                                <tr>
                                    <th scope="row">ارز مقصد</th>
                                    <td class="des-unit"></td>
                                </tr>
                                <tr>
                                    <th scope="row">مبلغ وارد شده</th>
                                    <td class="source-amount"></td>
                                </tr>
                                <tr>
                                    <th scope="row">کارمزد</th>
                                    <td class="fee-swap"></td>
                                </tr>
                                <tr>
                                    <th scope="row">واریزی شما :</th>
                                    <td class="des-amount"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success waves-effect waves-light" id="confirm-swap-btn">
                            تایید و
                            تبدیل
                        </button>
                        <button type="button" class="btn btn-warning waves-effect waves-light" data-dismiss="modal">
                            انصراف
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mt-3">
        <div class="container">
            <div id="wallet-list">
                <x-wallet-list :assets="$assets" :list-currencies="$list_currencies"
                               :fields="['global-price','inventory','24h','7d','market-volume','daily-market-volume']"/>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/selected-swap.js?v=1.0.0')}}"></script>

    <!-- Swap -->
    <script>
        $(document).ready(function () {
            let list_currencies = json_parse('{{ json_encode($list_currencies) }}');
            let swap_data = json_parse('{{ json_encode($swap_data) }}');

            // وقتی یکی از ارزهارو انتخاب میکنیم از کامبو باکس
            $(".currencies li").click(function () {
                let ul = $(this).parent();
                let inventory = $(this).data('inventory');
                let unit = $(this).data('unit');
                let inventory_class = ul.hasClass('send-ul') ? '.send-inventory' : '.receive-inventory';
                $(inventory_class).text((rial_to_unit(inventory, unit, true)).toUpperCase());
                check_selected_li();
                $("#send-input").keyup();
            });
            $(".currencies li:first").click().click();

            // وقتی مبلیغ رو برای سواپ وارد کرد
            $("#send-input").keyup(function () {
                // برای نشون دادن به کاربر
                $(this).val(numberFormat($(this).val()));

                reset_text();

                let send_li_sel = $(".send-ul li.selected");
                let receive_li_sel = $(".receive-ul li.selected");
                let receive_input = $("#receive-input");
                let swap_btn = $("#swap-btn");
                swap_btn.attr('disabled', 'disabled');


                let send_unit = send_li_sel.data('unit');
                let receive_unit = receive_li_sel.data('unit');
                let val = de_numberFormat($(this).val());
                val = send_unit.toLowerCase() == 'rls' ? val * 10 : val;
                let all_fee = swap_data[`manage_swap_${send_unit}`];
                let inventory = send_li_sel.data('inventory');

                if (val === '' || val == 0) {
                    reset_text();
                    return false;
                }
                if (inventory < val * 1) {
                    reset_text();
                    show_error('مقدار وارد شده بیشتر از موجودی میباشد');
                    return false;
                }
                let fee = get_fee(all_fee, send_unit, val);

                // اگر کارمزد نداشت
                if (fee === false) {
                    swap_btn.removeAttr('disabled');
                    reset_text();
                    show_error('مقدار مورد نظر خارج از بازه میباشد');
                } else {
                    let send_price_usdt = list_currencies[send_unit.toUpperCase()]['price'];
                    $(".fee-on-usdt").text(rial_to_unit((fee * send_price_usdt), 'usdt', true, false) + '$');
                    $(".fee-amount").text(rial_to_unit(fee, send_unit, true).toUpperCase());

                    $(".send-amount").text(rial_to_unit(val, send_unit, true).toUpperCase());
                    $(".send-on-usdt").text(rial_to_unit((val * send_price_usdt), 'usdt', true, false) + '$');

                    let receive_amount = swap(list_currencies, send_unit, receive_unit, val - fee, true, false);
                    if (receive_amount === false) // اگه عدد برگشتی مشکل داشت
                        show_error('----');
                    else {
                        $(".receive-amount").text(receive_amount + ' ' + get_unit(receive_unit).toUpperCase());
                        receive_input.text(receive_amount);
                        let receive_price_usdt = list_currencies[receive_unit.toUpperCase()]['price'];
                        receive_price_usdt = receive_unit.toLowerCase() == 'rls' ? receive_price_usdt * 10 : receive_price_usdt;
                        $(".receive-on-usdt").text(rial_to_unit((de_numberFormat(receive_amount) * receive_price_usdt), 'usdt', true, false) + '$');
                        swap_btn.removeAttr('disabled');
                    }
                }

            });

            // وقتی روی کلید ثبت کلیک کرد و و مدال تایید رو بهش نشون میدیم
            $("#swap-btn").click(function () {
                let source_unit = $(".send-ul li.selected").data('unit');
                let des_unit = $(".receive-ul li.selected").data('unit');
                let source_amount = $(".send-amount:first").text();
                let des_amount = $(".receive-amount").text();
                let fee_swap = $(".fee-amount:first").text();

                $("#swap-modal .source-unit").text(source_unit.toUpperCase());
                $("#swap-modal .des-unit").text(des_unit.toUpperCase());
                $("#swap-modal .source-amount").text(source_amount);
                $("#swap-modal .des-amount").text(des_amount);
                $("#swap-modal .fee-swap").text(fee_swap);
            });

            // وقتی درخواست ajax سواپ ارسال شد و جواب برگشت
            var after_swap = function (response, params) {
                $("#confirm-swap-btn").removeAttr('disabled').text('ثبت');
            }

            // تایید نهایی سواپ
            $("#confirm-swap-btn").click(function () {
                $(this).attr('disabled', 'disabled').text('منتظر بمانید ...');
                let form = {
                    isObject: true,
                    action: '{{ route('swap.store') }}',
                    method: 'post',
                    data: {
                        source_unit: $(".send-ul li.selected").data('unit'),
                        des_unit: $(".receive-ul li.selected").data('unit'),
                        source_amount: de_numberFormat($("#send-input").val()),
                    }
                }
                ajax(form, after_swap);
            });

            // نشان دادن خطا
            function show_error(msg) {
                $("#receive-input").hide();
                $(".swap-err").text(msg).show();
            }

            // محاسبه ی کارمزد سواپ بر اساس مقدار وارد شده کاربر
            function get_fee(all_fee, send_unit, val) {
                let fee = false;
                all_fee.forEach(function (item) {
                    let min = item.min * 1;
                    let max = item.max * 1;
                    val *= 1;
                    if ((min <= val && max > val) || (min <= val && max === -1)) {
                        fee = item.fee;
                    }
                });
                return fee;
            }

            // ریست کردن مقادیر فرم
            function reset_text(exclude = []) {
                let text_item = ['.fee-on-usdt', '.fee-amount', '.send-on-usdt', '.send-amount', '.receive-on-usdt', '.receive-amount', '#receive-input'];
                if (exclude.length > 0) {
                    exclude.forEach(function (val, index) {
                        // val_item= val_item.filter(item => item !== val)
                        text_item = text_item.filter(item => item !== val)
                    })
                }
                text_item = text_item.join(', ');
                $(`${text_item}`).text('-').show();
                $(".swap-err").hide();
            }

            // بررسی میکنه مقدار ارسالی و مقدار دریافی برای سواپ یکی هست یا ن
            function check_selected_li() {
                $("#swap-btn, #send-input").removeAttr('disabled');
                let send_li_sel = $(".send-ul li.selected").data('unit');
                let receive_li_sel = $(".receive-ul li.selected").data('unit');
                if ((send_li_sel === receive_li_sel) || !send_li_sel || !receive_li_sel) {
                    $("#swap-btn, #send-input").attr('disabled', 'disabled');
                }
            }

            // اپدیت هر 30 ثانیه یکبار usdt_amount
            setInterval(function () {
                $.ajax({
                    url: httpToHttps(`{{ route('asset.get_list_currencies') }}`),
                    type: 'post',
                    data: {
                        currencies: '{{ implode(',', $assets->pluck('unit')->toArray()) }}'
                    },
                    success: function (response) {
                        list_currencies = response;
                        $("#send-input").keyup();
                    }
                })
            }, 30000)

            // وقنی روی بادی کلیک کرد
            $(document).on('click', function (event) {
                let select= $('button.volume-form__selected-3');
                select.hasClass('active')?select.removeClass('active'):'';
            });
            // بعد این خط دیگه نباید روی اون سلکت باکس ها تابعی برای کلیک بنویسیم
            $('button.volume-form__selected-3').on('click', function (event) {
                event.stopPropagation();
            });

        });
    </script>
    <!--## Swap -->


    <!-- Wallet -->
    <script>
        //Refresh Wallet
        const refresh_wallet = function (response, params) {
            if (response)
                $("#wallet-list").html(response);
        }
        $("#wallet-list").delegate(".table__refresh-btn", 'click', function (e) {
            $(this).parents('.dashboard-table').addClass('active');
            e.preventDefault();
            const form = {
                action: '{{ route('wallet.refresh') }}',
                method: 'post',
                data: {
                    _token: '{{ csrf_token() }}',
                    fields: ['global-price', 'inventory', '24h', '7d', 'market-volume', 'daily-market-volume'],
                },
                isObject: true,
            };
            ajax(form, refresh_wallet);
        })
        //## Refresh Wallet
    </script>
    <!--## Wallet -->

@stop
