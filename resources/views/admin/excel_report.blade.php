<table class="table table-bordered">
    @if(isset($data) && count($data)>0)
    <thead>
    <tr>
        @foreach($data[0] as $key => $item)
            <th>{{$key}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        @foreach($data as $item)
            <tr>
            @foreach($item as $value)

                    <td>
                        <p class="list-item-heading">{{$value}}</p>
                    </td>
            @endforeach
            </tr>
        @endforeach
    </tbody>
    @endif
</table>

