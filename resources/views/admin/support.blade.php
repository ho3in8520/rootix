@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت پشتیبان-پنل مدیریت روتیکس
@endsection

@section('content')
    <section class="withdraw pt-2">

        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">
                    مدیریت پشتیبان
                </h1>
            </div>
            <div>
                <div class="dashboard-table transaction-table-container">
                    <div class="filters-container">
                        <form action="{{route('admin.support')}}" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="شنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Saturday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="یکشنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Sunday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label for="exampleFormControlInput1" class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="دوشنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Monday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label for="exampleFormControlInput1" class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="سه شنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Tuesday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label for="exampleFormControlInput1" class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="چهارشنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Wednesday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label for="exampleFormControlInput1" class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="پنجشنبه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Thursday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <label for="exampleFormControlInput1" class="form-label">روز هفته</label>
                                        <input name="day[]" type="text" class="form-control" value="جمعه" readonly>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">ایمیل</label>
                                        <input name="emails[]" type="email" class="form-control" value="{{$base_data['Friday']??''}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center mt-4">
                                <div class="col-3">
                                    <button class="btn btn-primary col-12 ajaxStore">ثبت</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

