@extends('templates.admin.master_page')
@section('title_browser')
    لیست گروه ها-پنل مدیریت روتیکس
@endsection
@section('content')
    <section id="basic-form-layouts" class="dashboard-cart user-list">
        <div class="container">
            <h1 class="dashboard-title desktop-title"> لیست گروه ها</h1>
            <div class="dashboard-table transaction-table-container">

                <div class="row">
                    <div class="col-12">
                        <div class="overflow-auto">
                            <div class="table-responsive specification">
                                <table class="container Specification-table mt-5">
                                    <thead>
                                    <tr>
                                        <th class="user-code">
                                            <div class="flat">
                                                ردیف
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div class="flat">
                                                نام گروه
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div>
                                                تاریخ ایجاد
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td class="code pt-4">{{ index($roles,$loop) }}</td>
                                            <td class="name header pt-4">{{$role->name}}</td>
                                            <td class="gmail-national-code header pt-4">{{ jdate_from_gregorian($role->created_at) }}</td>
                                            <td class="pt-5 pl-3">
                                                <div class="button-success">
                                                    <div class="pb-3">
                                                        <a class="operations-btn" href="{{ route('roles.edit',$role->id) }}">
                                                            ویرایش
                                                        </a>
                                                    </div>
                                                    <div class="pb-2">
                                                        <a class="authentication-btn" href="">
                                                           حذف
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {!! $roles->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete', function () {
                var id = $(this).data('key');
                swal({
                    title: "اطمینان از انجام عملیات",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: "DELETE",
                                url: httpToHttps(`{{ route('roles.destroy') }}`),
                                data: {_token: `{{ csrf_token() }}`, id: id},
                                success: function (response) {
                                    if (response.status == 100) {
                                        swal(response.msg, '', 'success')
                                        setTimeout(function () {
                                            window.location.reload();
                                        }, 3000)
                                    } else {
                                        swal(response.msg, '', 'error')
                                    }
                                }
                            });
                        }
                    });
            })
        })
    </script>
@endsection
