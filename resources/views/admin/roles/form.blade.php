@extends('templates.admin.master_page')
@section('title_browser') دسترسی ها-پنل مدیریت روتیکس @endsection
@section('content')
    <section id="basic-form-layouts" class="dashboard-cart">
        <div class="container">
            <h1 class="dashboard-title desktop-title">مدیریت دسترسی ها</h1>
            <div class="dashboard-table transaction-table-container">
                <form method="post"
                      action="{{ (empty($role))?route("roles.store"):route('roles.update',$role->id) }}">
                    @csrf
                    <div class="card-body">
                        <div class="px-1 py-3">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <label>نام</label>
                                    <input type="text" class="form-control" name="name" placeholder="نام"
                                           value="{{ (!empty($role))?$role->name:'' }}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    {!! $permission !!}
                                </div>
                            </div>
                            <div class="btns-exit">
                                <button href="#" class="btn signin-btn record-btn">
                                    ثبت
                                </button>
                            </div>
{{--                            <div class="form-actions">--}}
{{--                                <button type="button" class="btn btn-success ajaxStore">--}}
{{--                                    <i class="icon-note"></i> ثبت--}}
{{--                                </button>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
@section("script")
    <script>
        $(document).ready(function () {
            $(".parent").change(function (e) {
                var id = $(this).data("id");
                // $("input[data-parent-id="+id+"]").not(this).trigger("click");
                if ($(this).is(":checked"))
                    $("input[data-parent-id=" + id + "]").prop("checked", 1).change();
                else
                    $("input[data-parent-id=" + id + "]").prop("checked", 0).change();
            });

            $(".child").change(function (e) {
                var id = $(this).data("parent-id");

                if ($(this).is(":checked"))
                    $("input[data-id=" + id + "]").prop("checked", 1);
                else if ($("input[data-parent-id=" + id + "]:checked").length == 0)
                    $("input[data-id=" + id + "]").prop("checked", 0);
            });
        })
    </script>
@endsection
