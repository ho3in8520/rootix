@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت قوانین -پنل مدیریت روتیکس
@endsection
@section('style')
    <style>
        #ckeditor {
            display: none;
        }
    </style>
@endsection

@section('content')
    <section class="withdraw pt-2">

        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">
                    مدیریت قوانین
                </h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin.laws')}}" method="post">
                                @csrf
                                <input type="hidden" name="type" value="">
                                <div class="row justify-content-center">
                                    <div class="form-group col-lg-10 col-md-4 col-sm-6">
                                        <label for="example-text-input" class="">قوانین</label>
                                        <textarea id="ckeditor" name="description" rows="10" cols="125">
                                            {{$laws->extra_field3?json_decode($laws->extra_field3):''}}
                                        </textarea>
                                        @error('description') <span class="text-danger"></span>@enderror
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <button type="submit" class="btn btn-info btn-lg ajaxStore">ثبت قوانین</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('theme/plugins/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('ckeditor',
                {
                    customConfig: 'config.js',
                    toolbar: 'simple'
                });
        });
    </script>
@endsection

