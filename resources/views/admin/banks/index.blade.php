@extends('templates.admin.master_page')
@section('title_browser')
    حساب های بانکی کاربران-پنل مدیریت روتیکس
@endsection
@section('style')
    <style>
        .gray1 {
            background-color: gray;
        }
    </style>
@endsection
@section('content')
    <section id="basic-form-layouts" class="withdraw pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                حساب های بانکی کاربران
            </h1>

            <div>
                <div class="dashboard-table transaction-table-container">
                    <div class="filters-container">
                        <button class="col-12 filter-btn" data-toggle="modal" data-target="#toggle-filters-inputs">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="23.814"
                                height="20.15"
                                viewBox="0 0 23.814 20.15">
                                <g transform="translate(-3.375 -5.625)">
                                    <path
                                        d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -3.554)"
                                    />
                                    <path
                                        d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -1.777)"
                                    />
                                    <path
                                        d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"
                                    />
                                </g>
                            </svg>
                            <span class="pr-1">فیلترها</span>
                        </button>
                        <div id="toggle-filters-inputs" class="up-animation modal">
                            <div class="modal-content pt-0">
                                <div class="modal-header">
                                    <h6 class="modal-title">فیلترهای جدول</h6>
                                    <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">
                                        &times;
                                    </button>
                                </div>

                                <form action="">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-controlers pb-2">
                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblName fs-16">نام ونام
                                                        خانوادگی</label>
                                                    <input type="text" id="LblName" placeholder="..." name="fullname"
                                                           value="{{ request()->query('fullname') }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblUserCode fs-16">بانک</label>
                                                    @php
                                                        $banks_list=banks();
                                                    @endphp
                                                    <select class="custom-selected" name="name">
                                                        <option value="">همه</option>
                                                        @foreach($banks_list as $item)
                                                            <option
                                                                value="{{$item}}" {{request()->query('name') == $item?'selected':''}}>{{$item}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label
                                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16">کد
                                                        کاربری</label>
                                                    <input type="text" id="LblBank" placeholder="..." name="code"
                                                           value="{{ request()->query('code') }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">شماره
                                                        کارت</label>
                                                    <input type="text" id="LblCardNumber" name="card_number"
                                                           value="{{ request()->query('card_number') }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">شماره
                                                        حساب</label>
                                                    <input type="text" id="LblAccountNumber" name="account_number"
                                                           value="{{ request()->query('account_number') }}">
                                                </div>
                                            </div>

                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block fs-16">شبا</label>
                                                    <input type="text" id="LblSheba" placeholder="..."
                                                           name="sheba_number"
                                                           value="{{ request()->query('sheba_number') }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">وضعیت
                                                    </label>
                                                    <select class="custom-selected" name="status">
                                                        <option value="">همه</option>
                                                        <option
                                                            value="0" {{ request()->query('status')==='0'?'selected':'' }}>
                                                            جدید
                                                        </option>
                                                        <option
                                                            value="1" {{ request()->query('status')==1?'selected':'' }}>
                                                            تایید
                                                            شده
                                                        </option>
                                                        <option
                                                            value="2" {{ request()->query('status')==2?'selected':'' }}>
                                                            ویرایش شده
                                                        </option>
                                                        <option
                                                            value="3" {{ request()->query('status')==3?'selected':'' }}>
                                                            رد
                                                            شده
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label
                                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16">از
                                                        تاریخ ( ثبت
                                                        )</label>
                                                    <input type="text" name="date_from" class="datePicker"
                                                           id="LblDateMoney"
                                                           value="{{ request()->query('date_from') }}">
                                                </div>

                                                <div class="w-100 date-transaction bg-white">
                                                    <label
                                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16">تا
                                                        تاریخ ( ثبت
                                                        )</label>
                                                    <input class="last-input datePicker" type="text" name="date_to"
                                                           id="LblDateMoney2" value="{{ request()->query('date_to') }}">
                                                </div>

                                                <div class="w-100 date-transaction bg-white">
                                                    <button class="btn fixed-bottom transaction__btn2 search-ajax"
                                                            data-dismiss="modal">جستجو
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="table-responsive mt-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="withdraw-rls">
                                        <thead>
                                        <tr>
                                            <th class="text-right LblNamefamilyall text-nowarp">
                                                نام
                                            </th>
                                            <th>
                                                <div class="">
                                                    کد کاربر
                                                </div>
                                            </th>
                                            <th>
                                                <div>بانک</div>
                                            </th>
                                            <th>
                                                <div>شماره حساب</div>
                                            </th>
                                            <th>
                                                <div>شماره کارت</div>
                                            </th>
                                            <th>
                                                <div>شماره شبا</div>
                                            </th>
                                            <th>
                                                <div>وضعیت</div>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($banks) > 0)
                                            @foreach($banks as $bank)
                                                @php
                                                    $status=$bank->status;
                                                    switch ($status){
                                                        case 0:
                                                        case 2:
                                                                $status_arr=['status'=>'بررسی نشده','class'=>'border-Baresi'];
                                                                break;
                                                        case 1:
                                                                $status_arr=['status'=>'تایید شده','class'=>'border-Acc'];
                                                                break;
                                                        case 3:
                                                                $status_arr=['status'=>'رد شده','class'=>'border-Reject'];
                                                                break;
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="withdraw-rls">
                                                            <span>{{$bank->user->fullname??'بدون نام'}}</span>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="text-center">
                                                            <p class="withdraw-rls text-center">
                                                                {{$bank->user->code}}
                                                            </p>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="text-center">
                                                            <p class="withdraw-rls text-center">
                                                                {{$bank->name}}
                                                            </p>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <span
                                                            class="withdraw-rls"> {{$bank->account_number??'-'}} </span>
                                                    </td>

                                                    <td>
                                                        <span class="withdraw-rls text-nowrap">
                                                            {{$bank->card_number??'-'}}
                                                        </span>
                                                    </td>

                                                    <td>
                                                        <span class="withdraw-rls text-nowrap">
                                                            {{$bank->sheba_number??'-'}}
                                                        </span>
                                                    </td>

                                                    <td @if($bank->status ==3) style="padding-left: 2.6rem" @endif>
                                                        <div class="d-flex justify-content-center m-3">
                                                            @if($bank->status ==3)
                                                                <span
                                                                    class="attention animate__animated animate__flash animate__infinite infinite animate__slower 3s"
                                                                    data-toggle="tooltip" title=""
                                                                    data-original-title="{{$bank->reject_reason}}">!</span>
                                                            @endif
                                                            <span class="withdraw-rls {{$status_arr['class']}}">
                                                        {{$status_arr['status']}}
                                                    </span>
                                                        </div>
                                                    </td>

                                                    <td class="withdraw-rls-btns-td">
                                                        <div
                                                            class="wallet-table-btns d-flex flex-column align-items-end">

                                                            <div class="d-flex justify-content-between">
                                                                @can('bank_accept_btn')
                                                                    <form method="post"
                                                                          action="{{ route('admin.bank.confirm',[$bank]) }}">
                                                                        @csrf
                                                                        <input type="hidden" name="id"
                                                                               value="{{ $bank->id }}">
                                                                        <button {{$bank->status == 1?'disabled':''}}
                                                                                class="btn withdraw-rls-btn {{$bank->status == 1?'gray1':'harvest-btn2'}} ml-1 p-0 ajaxStore">
                                                                            تایید کردن
                                                                        </button>
                                                                    </form>
                                                                @endcan
                                                                @can('bank_reject_btn')
                                                                    <button type="button" data-target="#myModal"
                                                                            data-toggle="modal"
                                                                            data-url="{{route('admin.bank.reject',$bank->id)}}"
                                                                            data-id="{{$bank->id}}"
                                                                            {{$bank->status == 3?'disabled':''}}
                                                                            class="btn withdraw-rls-btn {{$bank->status ==3?'gray1':'deposit-btn2'}} reject-btn">
                                                                        رد کردن
                                                                    </button>
                                                                @endcan
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-danger" colspan="12"><h6> بانکی وجود ندارد.</h6></td>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-between align-items-center flex-row-reverse user-list">
                            <div>
                                {!! $banks->links() !!}
                            </div>
                            <div class="table-result">
                                <div class="pdf-exel mt-0  mr-3 mt-5 mb-5"
                                     style="margin: 0 !important;padding: 0 !important;">
                                    <a class="btn-pdf ml-3"
                                       href="{{route('admin.bank.index',request()->all()+['export'=>'pdf'])}}">خروجی PDF
                                        <img class="pdf-image"
                                             src="{{asset('theme/admin/img/Icon%20metro-file-pdf.svg')}}" alt="">
                                    </a>
                                    <a class="btn-exel"
                                       href="{{route('admin.bank.index',request()->all()+['export'=>'excel'])}}">خروجی
                                        Excel
                                        <img class="excel-image"
                                             src="{{asset('theme/admin/img/Icon%20simple-microsoftexcel.svg')}}" alt="">
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade modal-bank" id="myModal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header mx-auto border-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="130" height="140" viewBox="0 0 147 150">
                        <g id="Group_1783" data-name="Group 1783" transform="translate(-833 -230)">
                            <g id="Group_1782" data-name="Group 1782" transform="translate(278 180)">
                                <g id="Ellipse_118" data-name="Ellipse 118" transform="translate(555 50)" fill="none"
                                   stroke="#ff0c00" stroke-width="3">
                                    <circle cx="73.5" cy="73.5" r="73.5" stroke="none"/>
                                    <circle cx="73.5" cy="73.5" r="72" fill="none"/>
                                </g>
                                <text id="_" data-name="!" transform="translate(651 157)" fill="red" font-size="80"
                                      font-family="IRANSans" letter-spacing="0.01em">
                                    <tspan x="-11.309" y="0">!</tspan>
                                </text>
                            </g>
                        </g>
                    </svg>

                </div>
                <div class="modal-body border-0">
                    <h3 class="text-center my-3">دلیل رد کردن بانک را بنویسید:</h3>
                </div>
                <form id="reject-form" action="" method="post">
                    @csrf
                    <input type="hidden" value="" id="bank_id">
                    <div>
                        <div dir="rtl"
                             class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">

                            <input type="text" dir="rtl" class="currency-value-input" name="reject_reason"
                                   id="currency-value-input-1" placeholder="">
                        </div>
                    </div>
                    <div class="modal-footer border-0">
                        <button class="btn signin-btn deposit-btn mx-auto ajaxStore">
                            تایید
                        </button>
                        <button type="button" class="btn signin-btn btn-danger mx-auto" data-dismiss="modal">
                            بستن
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.reject-btn').on('click', function () {
            let bank_id = $(this).data('id');
            let action = $(this).data('url');
            $('#bank_id').val(bank_id);
            $('#reject-form').attr('action', action);
        })
    </script>
@endsection
