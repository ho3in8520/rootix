@extends('templates.admin.master_page')
@section('title_browser')
    مشاهده اطلاعیه -پنل مدیریت روتیکس
@endsection
@section('content')
    <section class="withdraw start-message pt-2">
        <div class="dashboard-cart requests-withdrawal">
            <div class="container">
                <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                    مشاهده اطلاعیه `{{ $announcement->title }}`
                </h1>
            </div>
        </div>
    </section>
    <section class="pt-1">
        <div class="container">
            <div class="panel-box h-auto">
                <div class="row">
                    <div class="col-md-4 my-1">
                        <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                            <span class="request-box__title">عنوان</span>
                            <input type="text" dir="ltr" class="currency-value-input"
                                   value="{{ $announcement->title }}" disabled/>
                        </div>
                    </div>
                    <div class="col-md-4 my-1">
                        <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                            <span class="request-box__title">تاریخ شروع</span>
                            <input type="text" dir="ltr" class="currency-value-input"
                                   value="{{ jdate_from_gregorian($announcement->started_at,'Y/m/d') }}" disabled/>
                        </div>
                    </div>
                    <div class="col-md-4 my-1">
                        <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                            <span class="request-box__title">تاریخ پایان</span>
                            <input type="text" dir="ltr" class="currency-value-input"
                                   value="{{ jdate_from_gregorian($announcement->end_at,'Y/m/d') }}" disabled/>
                        </div>
                    </div>

                    <div class="col-md-8 my-1">
                                <textarea class="w-100 request-box request-box-2 auth__btn request-js-box-2 enabled
                            border-0 outline-0 ticket__message-box" placeholder="توضیحات کوتابه" style="height: 100%;"
                                          autocomplete="off" disabled>{{ $announcement->excerpt }}</textarea>
                    </div>

                    <div class="col-md-4 my-1 p-3 mx-auto d-flex" style="box-shadow: 0 0 7px #0000003d;">
                        <img src="{{ getImage($announcement->files()->first()->path) }}" class="m-auto"
                             style="max-width: 180px;border: 2px dashed #54e9c3; padding: 8px; border-radius: 5px">
                    </div>

                    <div class="col-12 my-1">
                        <div class="card">
                            <div class="card-body" style="box-shadow: 0 0 7px #0000003d">
                                <p>{!! $announcement->text !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 my-1">
                        <div class="karmozd-traid d-flex ">
                            <p class="p-4">گروه ارسالی</p>
                            <select class="form-control swap-fee-select col-4 swap-fee-select custom-select-2" disabled>
                                <option selected>{{ $announcement->group_text }}</option>
                            </select>
                        </div>
                    </div>
                    @if(count($annable))
                        <div class="col-md-8 ann-list my-1">
                            <label for="roles-list" class="col-12 roles-list">
                                <select class="js-states form-control" id="roles-list"
                                        multiple="multiple" readonly="" disabled>
                                    @foreach($annable as $item)
                                        <option selected>{{ $item->name ?? $item->email }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script src="{{asset('theme/user/assets/js/select2.min.js')}}"></script>
    <script>
        $("#roles-list").select2({
            width: 'resolve'
        });
    </script>
@stop
