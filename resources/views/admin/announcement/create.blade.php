@extends('templates.admin.master_page')
@section('title_browser')
    ایجاد اطلاعیه جدید-پنل مدیریت روتیکس
@endsection
@section('content')
    <section class="withdraw start-message pt-2">
        <div class="dashboard-cart requests-withdrawal">
            <div class="container">
                <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                    ایجاد پیغام جدید
                </h1>
            </div>
        </div>
    </section>

    <form action="{{ route('admin.ann.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto">
                    <div class="row">
                        <div class="col-md-4">
                            <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                                <span class="request-box__title">
                                    عنوان
                                    <br><span class="text-danger error-title"></span>
                                </span>
                                <input type="text" dir="ltr" class="currency-value-input"
                                       id="currency-value-input-1" name="title"
                                       placeholder="عنوان را وارد کنید"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                                <span class="request-box__title">
                                    تاریخ شروع
                                    <br><span class="text-danger error-started_at"></span>
                                </span>
                                <input type="text" dir="ltr" class="currency-value-input datePicker" name="started_at"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div dir="rtl" class="request-box request-box-2 request-js-box-2
                          enabled d-flex justify-between items-center input-border-focus">
                                <span class="request-box__title">
                                    تاریخ پایان
                                    <br><span class="text-danger error-end_at"></span>
                                </span>
                                <input type="text" dir="ltr" class="currency-value-input datePicker" name="end_at"/>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <p class="text-danger error-excerpt"></p>
                                <textarea name="excerpt" class="w-100 request-box request-box-2 auth__btn request-js-box-2 enabled
                            border-0 outline-0 ticket__message-box" placeholder="توضیحات کوتاه"
                                          autocomplete="off"></textarea>
                        </div>

                        <div class="col-md-4">
                            <div class="upload-card py-3">
                                <div class="upload-card__header">
                                    <h3 class="upload-card__title">
                                        آپلود عکس
                                        <br><span class="text-danger error-img"></span>
                                    </h3>
                                </div>
                                <div style="margin-top: 1.2rem; z-index: 1" class="upload-card__body drop-area">
                                    <div class="upload-card__btn">
                                        <label for="fileElem2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="45" height="45"
                                                 viewBox="0 0 58 58">
                                                <g id="Group_1744" data-name="Group 1744"
                                                   transform="translate(-88 -790)">
                                                    <circle id="Ellipse_115" data-name="Ellipse 115" cx="29" cy="29"
                                                            r="29" transform="translate(88 790)" fill="#666"
                                                            opacity="0.2"></circle>
                                                    <g id="Icon_feather-upload" data-name="Icon feather-upload"
                                                       transform="translate(105.165 807.772)">
                                                        <path id="Path_8632" data-name="Path 8632"
                                                              d="M27.874,22.5v5.194a2.6,2.6,0,0,1-2.6,2.6H7.1a2.6,2.6,0,0,1-2.6-2.6V22.5"
                                                              transform="translate(-4.5 -6.917)" fill="none"
                                                              stroke="#2f31d0" stroke-linecap="round"
                                                              stroke-linejoin="round" stroke-width="3"></path>
                                                        <path id="Path_8633" data-name="Path 8633"
                                                              d="M23.486,10.993,16.993,4.5,10.5,10.993"
                                                              transform="translate(-5.306 -4.5)" fill="none"
                                                              stroke="#2f31d0" stroke-linecap="round"
                                                              stroke-linejoin="round" stroke-width="3"></path>
                                                        <path id="Path_8634" data-name="Path 8634" d="M18,4.5V20.083"
                                                              transform="translate(-6.313 -4.5)" fill="none"
                                                              stroke="#2f31d0" stroke-linecap="round"
                                                              stroke-linejoin="round" stroke-width="3"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </label>

                                    </div>

                                    <input type="file" id="fileElem2" accept="image/png,image/jpeg" name="img" class="input__file">
                                    <div>
                                        <span class="upload-card__caption">فایل را انتخاب کنید</span>
                                        <span class="upload-card__caption">یا آن را درون کادر بکشید</span>
                                    </div>
                                    <div id="gallery" class="upload__image"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <textarea id="ckeditor" name="text" rows="10" cols="125"></textarea>
                        </div>
                        <div class="col-md-4">
                            <div class="karmozd-traid d-flex ">
                                <p class="p-4">گروه ارسالی</p>
                                <select class="form-control swap-fee-select col-4 swap-fee-select custom-select-2"
                                        name="group">
                                    <option value="all">همه</option>
                                    <option value="roles" selected>نقش‌ها</option>
                                    <option value="users">کابران</option>
                                </select>
                            </div>
                        </div>

                        <!-- این سلکت باکس وقتیه که روی `همه` کلیک کرده و میخواد (عملا چیزی از این بخش نشون داده نمیشه) -->
                        <select class="all-list d-none" multiple="multiple">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}" selected></option>
                            @endforeach
                        </select> <!-- ## -->

                        <div class="col-md-8 ann-list">
                            <label for="roles-list" class="col-12 roles-list">
                                <select class="js-states form-control roles-list" id="roles-list" name="annable_id[]"
                                        multiple="multiple">
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </label>
                            <label for="users-list" class="col-12 users-list" style="display: none">
                                <select class="js-states form-control users-list" id="users-list" multiple="multiple">
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->email }}</option>
                                    @endforeach
                                </select>
                            </label>
                        </div>

                        <div class="col-12 mt-4">
                            <button type="submit" class="btn transaction__btn2 mx-auto ajaxStore">ارسال</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>

@endsection
@section('script')
    <script src="{{ asset('theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('theme/user/assets/js/select2.min.js')}}"></script>
    <script src="{{asset('theme/user/scripts/preview-file.js')}}"></script>
    <script>
        CKEDITOR.replace('ckeditor',
            {
                customConfig: 'config.js',
                toolbar: 'simple'
            });
        $("#roles-list,#users-list").select2({
            width: 'resolve'
        });
        $('.custom-select-2').customSelect2(function (val) {
            $(".users-list,.roles-list, .all-list").hide().removeAttr('name');
            $(`.${val}-list`).show().attr('name', 'annable_id[]');
        });
    </script>
@stop

