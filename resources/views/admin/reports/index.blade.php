@extends('templates.admin.master_page')
@section('title_browser')
    گزارشات سامانه-پنل مدیریت روتیکس
@endsection
@section('content')
    <section id="basic-form-layouts" class="dashboard-cart system-reports">
        <div class="container">
            <h1 class="dashboard-title desktop-title">گزارشات سامانه</h1>
            <div class="dashboard-table transaction-table-container">

                <div class="filters-container">
                    <button class="col-12 filter-btn" data-toggle="modal" data-target="#toggle-filters-inputs">
                        <svg xmlns="http://www.w3.org/2000/svg" width="23.814" height="20.15"
                             viewBox="0 0 23.814 20.15">
                            <g transform="translate(-3.375 -5.625)">
                                <path
                                    d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                    transform="translate(0 -3.554)"/>
                                <path
                                    d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                    transform="translate(0 -1.777)"/>
                                <path
                                    d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"/>
                            </g>
                        </svg>
                        <span class="pr-1">فیلترها</span>
                    </button>
                    <div id="toggle-filters-inputs" class="up-animation modal">
                        <div class="modal-content pt-0">

                            <div class="modal-header">
                                <h6 class="modal-title">فیلترهای جدول</h6>
                                <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">&times;
                                </button>
                            </div>

                            <div class="modal-body">
                                <div class="form-controlers pb-2">
                                    <form method="get" action="">
                                        <div class="header-transactionrls mb-3">
                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblEmail fs-16">مقدار</label>
                                                <input type="text" name="amount" value="{{ request()->amount }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">ایمیل</label>
                                                <input type="text" name="email" value="{{ request()->email }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblVariz fs-16">واریز/برداشت</label>
                                                <select class="custom-selected" name="type">
                                                    <option value="" {{ !request()->type?'selected':'' }}>همه</option>
                                                    <option value="2" {{ request()->type==2?'selected':'' }}>واریز</option>
                                                    <option value="1"  {{ request()->type==1?'selected':'' }}>برداشت</option>
                                                </select>
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block ">کد
                                                    کاربری</label>
                                                <input type="text" name="code"  value="{{ request()->code }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblVariz fs-16">ارز</label>
                                                <select class="custom-selected" name="currency">
                                                    <option value="">همه</option>
                                                    @foreach($assets as $item)
                                                        <option value="{{ $item->unit }}" {{ request()->currency==$item->unit?'selected':'' }}>{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="header-transactionrls mb-3">
                                            <div class="w-100 input-date  bg-white">
                                                <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تاریخ
                                                    از</label>
                                                <input type="text" name="start_created_at" class="datePicker" value="{{ request()->start_created_at }}">
                                            </div>

                                            <div class="w-100 input-date date-transaction bg-white">
                                                <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تاریخ
                                                    تا</label>
                                                <input class="last-input datePicker" type="text" name="end_created_at" value="{{ request()->end_created_at }}">
                                            </div>

                                            <div class="w-100 date-transaction bg-white">
                                                <button type="button" class="btn fixed-bottom transaction__btn2 search-ajax" data-dismiss="modal">جستجو
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="report-table col-12">
                        <div class="overflow-auto">
                            <div class="table-responsive specification">
                                <table class="container Specification-table mt-5">
                                    <thead>
                                    <tr>
                                        <th class="header-code headline">
                                            <div class="code">کد</div>
                                        </th>
                                        <th class="header headline">
                                            <div>ایمیل</div>
                                        </th>
                                        <th class="header headline">
                                            <div>مقدار</div>
                                        </th>
                                        <th class="header headline">
                                            <div>ارز</div>
                                        </th>
                                        <th class="header headline">
                                            <div>توضیحات</div>
                                        </th>
                                        <th class="header headline">
                                            <div>نوع</div>
                                        </th>
                                        <th class="header headline">
                                            <div class="date">تاریخ</div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($reports) > 0)
                                        @foreach($reports as $key=>$item)
                                            <tr>
                                                <td class="numbers header-code pt-4">{{ $item->user->code }}</td>
                                                <td class="gmail-national-code header pt-4">{{ $item->user->email }}</td>
                                                <td class="amount header pt-4">{{ $item->amount }}</td>
                                                <td class="currency header pt-4">{{ $item->financeable->unit }}</td>
                                                <td class="description header pt-4">{{ $item->description }}</td>
                                                <td class="more header pt-4">
                                                    <svg data-toggle="modal" data-target="#exampleModal-{{ $key+1 }}"
                                                         xmlns="http://www.w3.org/2000/svg" width="27" height="5.344"
                                                         viewBox="0 0 27 5.344">
                                                        <g transform="translate(-4.5 -15.328)">
                                                            <path
                                                                d="M17.986,15.328A2.672,2.672,0,1,0,20.658,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                            <path
                                                                d="M7.172,15.328A2.672,2.672,0,1,0,9.844,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                            <path
                                                                d="M28.828,15.328A2.672,2.672,0,1,0,31.5,18a2.671,2.671,0,0,0-2.672-2.672Z"/>
                                                        </g>
                                                    </svg>
                                                    <div class="modal fade" id="exampleModal-{{ $key+1 }}" tabindex="-1"
                                                         role="dialog" aria-labelledby="exampleModalLabel-{{ $key+1 }}"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title"
                                                                        id="exampleModalLabel-{{ $key+1 }}">توضیحات</h5>
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">{{ $item->description }}</div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary"
                                                                            data-dismiss="modal">بستن
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="header user-level pt-4">
                                                    <span
                                                        class="{{ $item->type==1?"harvest":"deposit" }}">{{ $item->type==1?"برداشت":"واریز" }}</span>
                                                </td>
                                                <td class="date header pt-5">{{ jdate_from_gregorian($item->created_at,'Y.m.d') }}
                                                    <span
                                                        class="time">{{ jdate_from_gregorian($item->created_at,'H:i') }}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="11" class="text-center">موردی یافت نشد</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ $reports->withQueryString()->links() }}

                            <div class="pdf-exel pr-3 mt-5">
                                <button class="btn-pdf mt-2">
                                    <p class="card-text">
                                        <a href="{{ request()->fullUrlWithQuery(['export'=>'pdf']) }}" class="pdf-text">خروجی PDF</a>
                                    </p>
                                    <img class="pdf-image" src="{{ asset('theme/user/images/Icon%20metro-file-pdf.svg') }}">
                                </button>
                                <button class="btn-exel mt-2 mr-2">
                                    <p class="card-text">
                                        <a href="{{ request()->fullUrlWithQuery(['export'=>'excel']) }}" class="exel-text">خروجی Excel</a>
                                    </p>
                                    <img class="excel-image" src="{{ asset('theme/user/images/Icon%20simple-microsoftexcel.svg') }}">
                                </button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
