@extends('templates.admin.master_page')
@section('title_browser')
    تیکت ها-پنل مدیریت روتیکس
@endsection
@section('style')
    <style>
        .view-pager {
            display: none;
        }

        .view-filter {
            display: none;
        }
    </style>
@endsection
@section('content')
    <section class="withdraw pt-2">
        <!--              Reject Modal-->
        <div class="modal fade mt-5" id="rejectModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header mx-auto border-0">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="130"
                            height="140"
                            viewBox="0 0 147 150">
                            <g
                                id="Group_1783"
                                data-name="Group 1783"
                                transform="translate(-833 -230)">
                                <g
                                    id="Group_1782"
                                    data-name="Group 1782"
                                    transform="translate(278 180)">
                                    <g
                                        id="Ellipse_118"
                                        data-name="Ellipse 118"
                                        transform="translate(555 50)"
                                        fill="none"
                                        stroke="#ff0c00"
                                        stroke-width="3">
                                        <circle cx="73.5" cy="73.5" r="73.5" stroke="none"/>
                                        <circle cx="73.5" cy="73.5" r="72" fill="none"/>
                                    </g>
                                    <text
                                        id="_"
                                        data-name="!"
                                        transform="translate(651 157)"
                                        fill="red"
                                        font-size="80"
                                        font-family="IRANSans"
                                        letter-spacing="0.01em"
                                    >
                                        <tspan x="-11.309" y="0">!</tspan>
                                    </text>
                                </g>
                            </g>
                        </svg>
                    </div>
                    <div class="modal-body border-0">
                        <h5 class="text-center my-3">دلیل رد درخواست را بنویسید</h5>
                    </div>
                    <form action="">
                        <div>
                            <div
                                dir="rtl"
                                class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active"
                            >
                                <input
                                    type="text"
                                    dir="ltr"
                                    class="currency-value-input"
                                    id="currency-value-input-1"
                                    placeholder="100"
                                />
                            </div>
                        </div>
                        <div class="modal-footer border-0">
                            <button
                                href="#"
                                type="submit"
                                class="btn signin-btn deposit-btn mx-auto"
                            >
                                تایید و رد کردن
                            </button>
                            <button
                                href="#"
                                type="button"
                                class="btn signin-btn btn-danger mx-auto"
                                data-dismiss="modal"
                            >
                                بستن
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--Reject Modal-->

        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">
                    مدیریت تیکت ها
                </h1>

                @can('admin_new_ticket')
                    <a href="{{route('ticket.admin.create')}}" class="create-new-bank text-white">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18.111"
                            height="18.111"
                            viewBox="0 0 18.111 18.111"
                        >
                            <g
                                id="Icon_feather-plus"
                                data-name="Icon feather-plus"
                                transform="translate(-6.5 -6.5)"
                            >
                                <path
                                    id="Path_8591"
                                    data-name="Path 8591"
                                    d="M18,7.5V23.611"
                                    transform="translate(-2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                />
                                <path
                                    id="Path_8592"
                                    data-name="Path 8592"
                                    d="M7.5,18H23.611"
                                    transform="translate(0 -2.444)"
                                    fill="none"
                                    stroke="#fff"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                />
                            </g>
                        </svg>
                        ثبت تیکت جدید
                    </a>
                @endcan
            </div>
            <div>
                <div class="dashboard-table transaction-table-container">
                    <div class="filters-container">
                        <button class="col-12 filter-btn" data-toggle="modal" data-target="#toggle-filters-inputs">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="23.814"
                                height="20.15"
                                viewBox="0 0 23.814 20.15">
                                <g transform="translate(-3.375 -5.625)">
                                    <path
                                        d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -3.554)"
                                    />
                                    <path
                                        d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -1.777)"
                                    />
                                    <path
                                        d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"
                                    />
                                </g>
                            </svg>
                            <span class="pr-1">فیلترها</span>
                        </button>
                        <div id="toggle-filters-inputs" class="up-animation modal">
                            <div class="modal-content pt-0">
                                <div class="modal-header">
                                    <h6 class="modal-title">فیلترهای جدول</h6>
                                    <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">
                                        &times;
                                    </button>
                                </div>

                                <div class="modal-body p-md-0">
                                    <form action="">
                                        @csrf
                                        @if(request()->has('type') && request()->query('type') =='my_tickets')
                                            <input type="hidden" name="type" value="my_tickets">
                                        @endif
                                        <div class="form-controlers pb-2">
                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblName fs-16">شماره تیکت</label>
                                                    <input type="text" id="LblName" name="ticket_code"
                                                           value="{{ request()->query('ticket_code') }}"/>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblEmail fs-16">ایمیل</label>
                                                    <input type="text" id="LblEmail" name="email"
                                                           value="{{ request()->query('email') }}"/>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">کد
                                                        کاربری</label>
                                                    <input type="text" id="LblEmail" name="code"
                                                           value="{{ request()->query('code') }}"/>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">عنوان</label>
                                                    <input type="text" id="LblEmail" name="title"
                                                           value="{{ request()->query('title') }}"/>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">فوریت</label>
                                                    <select class="custom-selected" name="priority">
                                                        <option
                                                            value="" {{ (request()->query('priority') == '')? 'selected':''  }}>
                                                            همه
                                                        </option>
                                                        <option
                                                            value="1" {{ (request()->query('priority') == '1')? 'selected':''  }}>
                                                            کم
                                                        </option>
                                                        <option
                                                            value="2" {{ (request()->query('priority') == '2')? 'selected':''  }}>
                                                            متوسط
                                                        </option>
                                                        <option
                                                            value="3" {{ (request()->query('priority') == '3')? 'selected':''  }}>
                                                            زیاد
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblName fs-16">همراه فایل</label>
                                                    <select class="custom-selected" name="file">
                                                        <option
                                                            value="" {{ (request()->query('file') == '')? 'selected':''  }}>
                                                            همه
                                                        </option>
                                                        <option
                                                            value="1" {{ (request()->query('file') == '1')? 'selected':''  }}>
                                                            با فایل
                                                        </option>
                                                        <option
                                                            value="2" {{ (request()->query('file') == '2')? 'selected':''  }}>
                                                            بدون فایل
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="w-100 bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">وضعیت</label>
                                                    <select class="custom-selected" name="status">
                                                        <option value="">همه</option>
                                                        <option
                                                            value="1" {{ (request()->query('status') == '1')? 'selected':''  }}>
                                                            بررسی نشده
                                                        </option>
                                                        <option
                                                            value="2" {{ (request()->query('status') == '2')? 'selected':''  }}>
                                                            پاسخ داده شده
                                                        </option>
                                                        <option
                                                            value="3" {{ (request()->query('status') == '3')? 'selected':''  }}>
                                                            ارجاع داده شده
                                                        </option>
                                                        <option
                                                            value="4" {{ (request()->query('status') == '4')? 'selected':''  }}>
                                                            بسته شده
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label
                                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16">از
                                                        تاریخ ( ثبت
                                                        )</label>
                                                    <input type="text" name="date_from" class="datePicker"
                                                           id="LblDateMoney"
                                                           value="{{ request()->query('date_from') }}">
                                                </div>

                                                <div class="w-100 date-transaction bg-white">
                                                    <label
                                                        class="mb-2 mt-3 d-block transaction__filter-title fs-16">تا
                                                        تاریخ ( ثبت
                                                        )</label>
                                                    <input class="last-input datePicker" type="text" name="date_to"
                                                           id="LblDateMoney2" value="{{ request()->query('date_to') }}">
                                                </div>

                                                <div
                                                    class="w-100 date-transaction fixed-bottom bg-white d-flex items-end">
                                                    <button class="btn transaction__btn2 fixed-bottom serach-ajax"
                                                            data-dismiss="modal">
                                                        جستجو
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive mt-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="withdraw-rls">
                                        <thead>
                                        <tr>
                                            <th class="text-right LblNamefamilyall text-nowarp">
                                                تیکت آیدی
                                            </th>
                                            <th>
                                                <div class="">
                                                    تاریخ
                                                </div>
                                            </th>
                                            <th>
                                                <div>ایمیل</div>
                                            </th>
                                            <th>
                                                <div>عنوان</div>
                                            </th>

                                            <th>
                                                <div>فوریت</div>
                                            </th>

                                            <th>
                                                <div>وضعیت</div>
                                            </th>

                                            <th>

                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($tickets) > 0)
                                            @foreach($tickets as $ticket)
                                                @php
                                                    $priority=$ticket->priority;
                                                    $status=$ticket->status;
                                                    switch ($priority){
                                                        case 1:
                                                            $priority_arr=['priority'=>'کم','class'=>'border-Baresi'];
                                                            break;
                                                        case 2:
                                                            $priority_arr=['priority'=>'متوسط','class'=>'border-Acc'];
                                                            break;
                                                        case 3:
                                                            $priority_arr=['priority'=>'زیاد','class'=>'border-Reject'];
                                                            break;
                                                    }
                                                    switch ($status){
                                                        case 1:
                                                            $status=['status'=>' بررسی نشده','class'=>'border-Baresi'];
                                                            break;
                                                        case 2:
                                                            $status=['status'=>'پاسخ داده شد','class'=>'border-Acc'];
                                                            break;
                                                        case 3:
                                                            $status=['status'=>'ارجاع داده شد','class'=>'border-Reject'];
                                                            break;
                                                        case 4:
                                                            $status=['status'=>'بسته شده','class'=>'border-Reject'];
                                                            break;
                                                    }

                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="withdraw-rls">
                                                            <span>{{$ticket->ticket_code}}</span>
                                                        </div>
                                                    </td>

                                                    <td>
                                                        <div class="text-center">
                                                            <p class="withdraw-rls text-center">
                                                                {{jdate_from_gregorian($ticket->created_at,'Y.m.d')}}
                                                            </p>
                                                            <p class="withdraw-rls text-center">
                                                                {{jdate_from_gregorian($ticket->created_at,'H:i')}}
                                                            </p>
                                                        </div>
                                                    </td>

                                                    @foreach($ticket->user as $user)
                                                        <td>
                                                            <span class="withdraw-rls"> {{$user->email}} </span>
                                                        </td>
                                                    @endforeach

                                                    <td>
                                                        <span class="withdraw-rls text-nowrap">
                                                            {!! \Illuminate\Support\Str::limit($ticket->title,30) !!}
                                                        </span>
                                                    </td>

                                                    <td>
                                                        <div class="d-flex justify-content-center m-3">
                                    <span class="withdraw-rls {{$priority_arr['class']}}">
                                    {{$priority_arr['priority']}}
                                    </span>
                                                        </div>
                                                    </td>


                                                    <td>
                                                        <div class="d-flex justify-content-center m-3">
                                    <span class="withdraw-rls {{$status['class']}}">
                                       {{$status['status']}}
                                    </span>
                                                        </div>
                                                    </td>

                                                    <td class="withdraw-rls-btns-td">
                                                        <div
                                                            class="wallet-table-btns d-flex flex-column align-items-end">

                                                            <div class="d-flex justify-content-between">
                                                                @can('admin_show_ticket')
                                                                    <a href="{{route('ticket.admin.show',$ticket->id)}}"
                                                                       class="btn withdraw-rls-btn harvest-btn2 mr-0 p-0">
                                                                        مشاهده
                                                                    </a>
                                                                @endcan

                                                                @can('admin_close_ticket')
                                                                    <form
                                                                        action="{{route('ticket.admin.ban',$ticket->id)}}"
                                                                        method="get">
                                                                        @csrf
                                                                        <button href=""
                                                                                {{$ticket->status==4?'disabled':''}}
                                                                                class="btn withdraw-rls-btn {{$ticket->status==4?'bg-707070':'deposit-btn2'}} ajaxStore">
                                                                            بستن
                                                                        </button>
                                                                    </form>
                                                                @endcan
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-danger" colspan="12"><h6> تیکتی وجود ندارد.</h6></td>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-between align-items-center flex-row-reverse user-list">
                            <div>
                                {!! $tickets->links() !!}
                            </div>
                            <div class="table-result">
                                <div class="pdf-exel mt-0  mr-3 mt-5 mb-5"
                                     style="margin: 0 !important;padding: 0 !important;">
                                    @if(request()->has('type') && request()->type == 'my_tickets')
                                        <a href="{{route('my_ticket.admin.index',request()->all()+['export'=>'pdf'])}}"
                                           class="btn-pdf ml-3">خروجی PDF<i class="far fa-file-pdf"></i>
                                        </a>
                                        <a href="{{route('my_ticket.admin.index',request()->all()+['export'=>'excel'])}}"
                                           class="btn-exel">خروجی Excel<i class="far fa-file-excel"></i>
                                        </a>
                                    @else
                                        <a href="{{route('ticket.admin.index',request()->all()+['export'=>'pdf'])}}"
                                           class="btn-pdf ml-3">خروجی PDF<i class="far fa-file-pdf"></i>
                                        </a>
                                        <a href="{{route('ticket.admin.index',request()->all()+['export'=>'excel'])}}"
                                           class="btn-exel">خروجی Excel<i class="far fa-file-excel"></i>
                                        </a>
                                    @endif

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
@endsection
