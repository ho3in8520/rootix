@extends('templates.admin.master_page')
@section('title_browser')
    ایجاد تیکت-پنل مدیریت روتیکس
@endsection

@section('content_class','bank-content')

@section('content')
    <section class="wallet-cart pt-2">
        <div class="container">
            <div class="bank-header">
                <h1 class="dashboard-title bank-title desktop-title">
                    تیکت جدید
                </h1>
            </div>
        </div>
    </section>

    <form id="ticket-form" action="{{route('ticket.admin.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <section class="pt-1">
            <div class="container">
                <div class="panel-box h-auto">
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div dir="rtl" class="request-box request-box-2 auth__btn request-js-box-2
                          enabled d-flex justify-between items-center">
                                <span class="request-box__title">

                                    عنوان
                                    <br>
                                <span class="text-danger error-title"></span>
                                </span>
                                <input type="text" dir="ltr" class="currency-value-input"
                                       id="currency-value-input-1" name="title"
                                       placeholder="عنوان را وارد کنید"/>
                            </div>
                        </div>


                        <div class="col-12 col-md-6 col-lg-4">
                            <div dir="rtl" class="request-box request-box-2 auth__btn request-js-box-2
                          enabled d-flex justify-between items-center">
                                <span class="request-box__title">کاربر</span>
                                <select class="form-control col-4 users" name="user_id">
                                    <option value="">انتخاب کنید...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div dir="rtl" class="request-box request-box-2 auth__btn request-js-box-2 enabled
                          d-flex justify-between items-center">
                                <span class="request-box__title">فوریت</span>
                                <select class="form-control col-4 js-example-basic-single" name="priority">
                                    <option value="1">کم</option>
                                    <option value="2">متوسط</option>
                                    <option value="3">زیاد</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div>
                        <textarea name="description"
                                  class="w-100 request-box request-box-2 auth__btn request-js-box-2 enabled
                            border-0 outline-0 ticket__message-box"
                                  placeholder="پیام خود را وارد کنید..."
                        ></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-center mt-4">
                        <div class="photo__name"></div>

                        <label for="upload-btn" class="ticket__btns d-flex">
                            <div class="ticket-upload__btn d-flex align-items-center">
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                         viewBox="0 0 26.992 30.849">
                                        <path id="Icon_metro-attachment" data-name="Icon metro-attachment"
                                              d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                              transform="translate(-3.535 -1.928)"/>
                                    </svg>
                                </div>

                                <input name="file" type="file" class="ticket-upload__input d-none" id="upload-btn"
                                       accept="image/*">

                                <span>
                                فایل
                            </span>
                            </div>
                            <button id="" type="button" class="create-new-bank withdraw-btn ajaxStore">
                                ارسال
                            </button>
                        </label>
                    </div>
                </div>
            </div>
        </section>
    </form>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/ticket-2.js')}}"></script>
    <script src="{{asset('theme/user/scripts/custom-select-box-2.js')}}"></script>
    <script src="{{asset('theme/user/assets/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.users').select2({
                width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: `{{ route('admin.users') }}`,
                    user:$(this).val(),
                    dataType: 'json',
                    type: "get",
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.email,
                                    id: item.id,
                                }
                            })
                        };
                    }
                }
            });
            $('.js-example-basic-single').select2();

            $('.custom-select-2').customSelect2(function (val) {});
        });
    </script>
@endsection
