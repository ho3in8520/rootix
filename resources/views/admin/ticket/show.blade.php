@extends('templates.admin.master_page')
@section('style')
    <style>
        .hide {
            display: none;
        }
    </style>
@endsection
@section('title_browser') نمایش تیکت-پنل مدیریت روتیکس@endsection
@section('content')
    <section class="mt-5 ticket-3">
        <div class="container">
            <div class="row align-items-start">
                <div class="col-12 col-lg-9 chat__col">
                    <div class="panel-box p-0 d-flex flex-column justify-between" style="min-height: 575px">
                        <div class="chat-box p-4">
                            @foreach($ticket->ticket_details as $tickets)
                                @php
                                    $date=$tickets->created_at;
                                    if ($date->isToday())
                                        $msg='امروز';
                                    elseif ($date->isYesterday())
                                        $msg='دیروز';
                                    else
                                        $msg='';
                                @endphp
                                <div class="chat-box__chat d-flex dir-{{$tickets->type==1?'rtl':'ltr'}}">
                                    <div class="chat-box__user-img">
                                        <img
                                            src="{{$tickets->type==1?$tickets->user->avatar:asset('theme/landing/images/logo.png')}}"
                                            alt="مشتری روتیکس"/>
                                    </div>
                                    <div class="chat-box__user-info">
                                        @if($tickets->files !=null && count($tickets->files)>0)
                                            <div>
                                                <div class="chat-box__text" style="display: inline-block; !important;">
                                                    <p>
                                                        {{$tickets->description}}
                                                    </p>
                                                </div>
                                                <a href="{{getImage($tickets->files[0]->path,'download')}}"
                                                   title="دانلود فایل">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="24"
                                                         viewBox="0 0 26.992 30.849">
                                                        <path id="Icon_metro-attachment"
                                                              data-name="Icon metro-attachment"
                                                              d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                                              transform="translate(-3.535 -1.928)"/>
                                                    </svg>
                                                </a>

                                            </div>
                                        @else
                                            <div class="chat-box__text">
                                                <p>
                                                    {{$tickets->description}}
                                                </p>
                                            </div>
                                        @endif
                                        <div class="d-flex">
                                            <h5 class="chat-box__name">
                                                {{$tickets->type==1?$tickets->user->fullname:$tickets->ticket_master->admin->fullname}}
                                                ({{$tickets->type==1?'مشتری':'پشتیبان'}})
                                                <span
                                                    class="chat-box__time">- {{$msg}} {{$msg?jdate_from_gregorian($date,'H:i'):jdate_from_gregorian($date,'%Y.%m.%d')}}</span>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
{{--                        <div class="chat-message__input chat-message__box">--}}
{{--                        <textarea--}}
{{--                            class="w-100 border-0 outline-0 ticket__message-box ticket__message-box-2"--}}
{{--                            placeholder="پیام خود را برای پشتیبان بنویسید..."></textarea>--}}
{{--                        </div>--}}
                    </div>
                </div>

                <div class="col-12 col-lg-3">
                    <div class="panel-box p-4 ticket-info__box">
                        <div class="ticket-info d-flex align-items-center justify-between mb-4">
                            <div class="ticket-info d-flex align-items-center">
                                <div>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="19"
                                        height="13"
                                        viewBox="0 0 23.264 15.842">
                                        <path
                                            id="Icon_awesome-ticket-alt"
                                            data-name="Icon awesome-ticket-alt"
                                            d="M4.947,8.211H17.316v7.421H4.947Zm15.461,3.711a1.855,1.855,0,0,0,1.855,1.855v3.711a1.855,1.855,0,0,1-1.855,1.855H1.855A1.855,1.855,0,0,1,0,17.487V13.777a1.855,1.855,0,1,0,0-3.711V6.355A1.855,1.855,0,0,1,1.855,4.5H20.408a1.855,1.855,0,0,1,1.855,1.855v3.711A1.855,1.855,0,0,0,20.408,11.921ZM18.553,7.9a.928.928,0,0,0-.928-.928H4.638a.928.928,0,0,0-.928.928v8.04a.928.928,0,0,0,.928.928H17.625a.928.928,0,0,0,.928-.928Z"
                                            transform="translate(0.5 -4)"
                                            fill="none"
                                            stroke="#000"
                                            stroke-width="1"/>
                                    </svg>
                                </div>
                                <h5 class="ticket-info__title mb-0 mr-2">
                                    اطلاعات تیکت
                                </h5>
                            </div>

                            <span
                                class="icon-Icon-material-expand-more ticket-info__arrow"
                            ></span>
                        </div>

                        <h5 class="ticket-info__title mb-4">
                            موضوع : {{$ticket->title}}
                        </h5>
                        @php
                            $status=[];
                            switch ($ticket->status){
                                case 1:
                                case 3:
                                    $status=['status'=>'در حال بررسی','class'=>'minus'];
                                    break;
                                case 2:
                                    $status=['status'=>'پاسخ داده شده','class'=>'plus'];
                                    break;
                                case 4:
                                    $status=['status'=>'بسته شده','class'=>'plus'];
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">
                            وضعیت : <span class="{{$status['class']}}"> {{$status['status']}}</span>
                        </h5>
                        @php
                            $role='';
                            switch ($ticket->role_id){
                                case 1:
                                    $role='پشتیبانی';
                                    break;
                                case 2:
                                    $role='فنی';
                                    break;
                                case 3:
                                    $role='پشتیبان3';
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">بخش : {{$role}}</h5>

                        <h5 class="ticket-info__title mb-4">
                            ارسال شده در : {{jdate_from_gregorian($ticket->created_at,'%Y.%m.%d')}}
                        </h5>

                        <h5 class="ticket-info__title mb-4">
                            آخرین بروزرسانی : {{calculate_time($ticket->updated_at)}} پیش
                        </h5>
                        @php
                            $priority='';
                            switch ($ticket->priority){
                                case 1:
                                    $priority='کم';
                                    break;
                                case 2:
                                    $priority='متوسط';
                                    break;
                                case 3:
                                    $priority='زیاد';
                                    break;
                            }
                        @endphp
                        <h5 class="ticket-info__title mb-4">اولویت : {{$priority}}</h5>

                        <h5 class="ticket-info__title mb-4">
                            کاربر ارجاع داده شده : فنی
                        </h5>

                        <form action="{{route('ticket.admin.ban',$ticket->id)}}" method="get"
                              class="{{$ticket->status==4?'hide':''}}">
                            @csrf
                            @method('put')
                            <button type="button" class="btn authentication-btn w-100 mt-5 closed-ticket ajaxStore">
                                بستن تیکت
                            </button>
                        </form>

                        <div class="row reference__dropdown">
                            <select class="form-control custom-select-2 col-12" name="currency">
                                <option value="0" selected>ارجاع به</option>
                                <option value="1">تست</option>
                                <option value="2">تست</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-9 chat-message-box__col chat-message__box {{$ticket->status==4?'hide':''}}">
                    <form action="{{route('comment.admin.store',$ticket->id)}}" method="post"
                          class="{{$ticket->status==4?'hide':''}}">
                        @csrf
                        <div class="mt-3 panel-box">
                      <textarea name="description"
                                class="w-100 request-box-2 auth__btn request-js-box-2 enabled border-0 outline-0 ticket__message-box"
                                placeholder="پیام خود را بنویسید..."></textarea>
                            <div class="d-flex align-items-center justify-end mt-4">
                                <div class="photo__name"></div>

                                <label for="upload-btn" class="ticket__btns d-flex">
                                    <div class="ticket-upload__btn d-flex align-items-center">
                                        <div>
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="20"
                                                height="24"
                                                viewBox="0 0 26.992 30.849">
                                                <path
                                                    id="Icon_metro-attachment"
                                                    data-name="Icon metro-attachment"
                                                    d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                                    transform="translate(-3.535 -1.928)"
                                                />
                                            </svg>
                                        </div>

                                        <input type="file" name="file" class="ticket-upload__input d-none"
                                               id="upload-btn"
                                               accept="image/*"/>

                                        <span> فایل </span>
                                    </div>

                                    <button class="create-new-bank withdraw-btn ajaxStore">
                                        ارسال
                                    </button>
                                </label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <form action="{{route('comment.admin.store',$ticket->id)}}" method="post"
                  class="{{$ticket->status==4?'hide':''}}">
                @csrf
                <div class="mt-3 panel-box mobile-panel-box" style="display: none">
                    <div class="chat-message__input chat-message__box">
                        <textarea name="description"
                            class="w-100 border-0 outline-0 ticket__message-box ticket__message-box-2"
                            placeholder="پیام خود را برای مشتری بنویسید..."></textarea>
                    </div>
                    <div class="d-flex justify-center mt-4 flex-column ticket-3__btns">
                        <div class="photo__name text-center ml-0 mb-2"></div>

                        <label for="upload-btn" class="ticket__btns mb-3">
                            <div class="ticket-upload__btn d-flex align-items-center justify-center">
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg"
                                         width="20"
                                         height="24"
                                         viewBox="0 0 26.992 30.849">
                                        <path id="Icon_metro-attachment"
                                              data-name="Icon metro-attachment"
                                              d="M22.629,11.781,20.673,9.825l-9.785,9.783a4.151,4.151,0,0,0,5.871,5.87L28.5,13.737a6.918,6.918,0,0,0-9.784-9.783L6.388,16.282l-.026.025A9.649,9.649,0,0,0,20.008,29.951l.025-.026h0l8.415-8.414-1.958-1.956-8.415,8.413-.025.025a6.881,6.881,0,0,1-9.733-9.73l.027-.025v0L20.673,5.911a4.151,4.151,0,0,1,5.871,5.87L14.8,23.521a1.383,1.383,0,0,1-1.957-1.956l9.785-9.784Z"
                                              transform="translate(-3.535 -1.928)"/>
                                    </svg>
                                </div>

                                <input type="file" name="file" class="ticket-upload__input d-none" id="upload-btn" accept="image/*"/>
                                <span> فایل </span>
                            </div>
                        </label>
                        <button class="create-new-bank withdraw-btn w-100 m-0 mt-4 ajaxStore">
                            ارسال
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/ticket-2.js')}}"></script>
    <script src="{{asset('theme/user/scripts/ticket-3.js')}}"></script>
    <script src="{{asset('theme/user/scripts/custom-select-box-2.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.custom-select-2').customSelect2(function (val) {
            });
        });
    </script>


    <script>
        function upload() {
            var fileInput = document.getElementById('upload_file');
            $('#name_img').text(fileInput.files[0].name);
        }


        // var filename = fileInput.files[0].name;
        // alert(filename);
        $(document).ready(function () {
            $(document).on('change', '#select', function () {
                let select1 = $('#select').val();
                $.data(document, "select", $('.select').val());

                var sub_select = '';
                switch (select1) {
                    case '0':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').addClass('d-none');
                        break;
                    case '1':
                        $('#top1').removeClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document, "sub_select", $('.top1').val());

                        $(document).on('change', '#top1', function () {
                            $.data(document, "sub_select", $('#top1').val());
                            switch (sub_select) {
                                case '0':
                                    $.data(document, "sub_select", $('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    case '2':
                        $('#top1').addClass('d-none');
                        $('#top2').removeClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document, "sub_select", $('.top2').val());
                        $(document).on('change', '#top2', function () {
                            var sub_select1 = $('.top2').val();
                            $.data(document, "sub_select", $('#top2').val());
                            switch (sub_select1) {
                                case '0':
                                    $.data(document, "sub_select", $('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    case '3':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').removeClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');
                        $.data(document, "sub_select", $('.top3').val());
                        $(document).on('change', '#top3', function () {
                            var sub_select1 = $('.top3').val();
                            $.data(document, "sub_select", $('#top3').val());
                            switch (sub_select1) {
                                case '0':
                                    $.data(document, "sub_select", $('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    case '4':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').removeClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document, "sub_select", $('.top4').val());
                        $(document).on('change', '#top4', function () {
                            var sub_select1 = $('.top4').val();
                            $.data(document, "sub_select", $('#top4').val());

                            switch (sub_select1) {
                                case '0':
                                    $.data(document, "sub_select", $('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    default:
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');
                }
            });
            $(document).on('click', '.submit2', function () {
                send_ajax($.data(document, "select"), $.data(document, "sub_select"));
            });

        });


        function send_ajax(id1, id2) {
            console.log(id1, id2)
            $.ajax({
                url: httpToHttps('{{route('ticket.route',$ticket->id)}}'),
                type: 'post',
                data: {from_id: id1, to_id: id2, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    swal('موفق', data.msg, 'success');
                    setTimeout(function () {
                        location.href = "{{route('ticket.admin.index')}}"
                    }, 2000)
                }
            });
        }

    </script>
@endsection
