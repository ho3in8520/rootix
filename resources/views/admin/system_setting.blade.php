@extends('templates.admin.master_page')
@section('title_browser')
    تنظیمات سامانه-پنل مدیریت روتیکس
@endsection
@section('style')
    <style>
        .confirmations {
            width: 25px;
        }
    </style>
@endsection
@section('content')
    <section class="dashboard-cart user-list dashboard-setting">
        <div class="container">
            <h1 class="dashboard-title desktop-title">تنظیمات سامانه</h1>
            <div class="dashboard-table transaction-table-container send-Announcements ">
                <b class="section-title mb-2">ارسال اعلان ها</b>
                <form action="{{route('admin.setting.notifs-status')}}" method="post">
                    @csrf
                    <div>
                        <div class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 fill="currentColor" class="bi bi-exclamation-circle m-1 m-1" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path
                                    d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                            </svg>
                            <p class="mr-2">ارسال اعلان به صورت </p>
                            <b> پیامک </b>
                            <label class="switch m-2">
                                <input id="message-switch" type="checkbox" name="sms"
                                    {{$admin_notifs['admin_sms_notifs']->extra_field1==1?'checked' :''}}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        @php
                            $sms_arr=json_decode($admin_notifs['admin_sms_notifs']->extra_field2);
                        @endphp
                        <div id="message" class="{{$admin_notifs['admin_sms_notifs']->extra_field1==1?'':'d-none'}}">
{{--                            @if($sms_arr != null)--}}
                                <div class="form-check m-1 mt-3 ">
                                    <input class="form-check-input " type="checkbox" name="Message[]" id="ticket"
                                           value="tickets" {{($sms_arr != null && in_array('tickets',$sms_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4 " for="ticket">
                                        <small>
                                            بخش تیکت ها
                                        </small>
                                    </label>
                                </div>
                                <div class="form-check m-1 mt-3 ">
                                    <input class="form-check-input" type="checkbox" name="Message[]"
                                           id="rial-withdrawal"
                                           value="rial_withdraw" {{($sms_arr != null && in_array('rial_withdraw',$sms_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="rial-withdrawal">
                                        <small>
                                            درخواست برداشت ریالی
                                        </small>

                                    </label>
                                </div>
                                <div class="  form-check m-1 mt-3">
                                    <input class="form-check-input" type="checkbox" name="Message[]"
                                           id="currency-withdrawal"
                                           value="currency_withdraw" {{($sms_arr != null && in_array('currency_withdraw',$sms_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="currency-withdrawal">
                                        <small>
                                            درخواست برداشت ارزها

                                        </small>
                                    </label>
                                </div>
                                <div class=" form-check m-1 mt-3">
                                    <input class="form-check-input" type="checkbox" name="Message[]" id="banks"
                                           value="banks" {{($sms_arr != null && in_array('banks',$sms_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="banks">
                                        <small>
                                            بخش بانک ها </small>
                                    </label>
                                </div>
{{--                            @endif--}}
                        </div>
                    </div>
                    <hr class="dashed-hr-dashboard">
                    <div>
                        <div class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 fill="currentColor" class="bi bi-exclamation-circle m-1" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path
                                    d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                            </svg>
                            <p class="mr-2"> ارسال اعلان به صورت</p>
                            <b> ایمیل</b>
                            <label class="switch m-2">
                                <input id="email-switch" type="checkbox"
                                       name="mail" {{$admin_notifs['admin_email_notifs']->extra_field1==1?'checked':''}}>
                                <span class="slider round"></span>
                            </label>
                        </div>
                        @php
                            $email_arr=json_decode($admin_notifs['admin_email_notifs']->extra_field2);
                        @endphp
                        <div id="email" class="{{$admin_notifs['admin_email_notifs']->extra_field1==1?'':'d-none'}}">
{{--                            @if($email_arr != null)--}}
                                <div class="form-check m-1 mt-3 ">
                                    <input class="form-check-input " type="checkbox" name="email[]" id="email-ticket"
                                           value="tickets" {{($email_arr != null && in_array('tickets',$email_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4 " for="email-ticket">
                                        <small>
                                            بخش تیکت ها
                                        </small>
                                    </label>
                                </div>
                                <div class="form-check m-1 mt-3 ">
                                    <input class="form-check-input" type="checkbox" name="email[]"
                                           id="email-rial-withdrawal"
                                           value="rial_withdraw" {{($email_arr != null && in_array('rial_withdraw',$email_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="email-rial-withdrawal">
                                        <small>
                                            درخواست برداشت ریالی
                                        </small>

                                    </label>
                                </div>
                                <div class="  form-check m-1 mt-3">
                                    <input class="form-check-input" type="checkbox" name="email[]"
                                           id="email-currency-withdrawal"
                                           value="currency_withdraw" {{($email_arr != null && in_array('currency_withdraw',$email_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="email-currency-withdrawal">
                                        <small>
                                            درخواست برداشت ارزها

                                        </small>
                                    </label>
                                </div>
                                <div class=" form-check m-1 mt-3">
                                    <input class="form-check-input" type="checkbox" name="email[]" id="email-banks"
                                           value="banks" {{($email_arr != null && in_array('banks',$email_arr))?'checked':''}}>
                                    <label class="form-check-label mr-4" for="email-banks">
                                        <small>
                                            بخش بانک ها </small>
                                    </label>
                                </div>
{{--                            @endif--}}
                        </div>
                    </div>
                    @can('notifications_setting')
                        <div class="btns-exit">
                            <button type="submit" class="btn signin-btn record-btn ajaxStore">
                                ثبت
                            </button>
                        </div>
                    @endcan
                </form>
            </div>

            @can('monetary_site')
                <div class="dashboard-table transaction-table-container send-Announcements ">
                    <b class="section-title mb-2">واحد پول ایران</b>
                    <div>
                        <div class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 fill="currentColor" class="bi bi-exclamation-circle m-1" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path
                                    d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                            </svg>
                            <p class="mr-2">نوع واحد پول سامانه را انتخاب کنید</p>
                        </div>

                        <div class="form-check m-1 mt-3 d-flex">
                            <img class="comment-img" src="{{asset('theme/user/images/Group 1785.svg')}}" alt="comment">
                            <label class="form-check-label mr-2"><small>
                                    توجه داشته باشید با تغییر واحد پولی سامانه، واحد پول در تمام بخش ها از جمله سواپ،
                                    ترید،
                                    واریز و برداشت تغییر خواهد کرد. </small>
                            </label>
                        </div>
                        <form action="">
                            @csrf
                            <div class="form-check m-1 mt-3 ">
                                <input class="form-check-input " type="radio" name="monetary_unit" id="rial" value="rls"
                                    {{$result['monetary_unit']->extra_field1=='rls'?'checked':''}}>
                                <label class="form-check-label mr-4" for="rial">
                                    <small>
                                        ریال(Rial)
                                    </small>
                                </label>
                            </div>

                            <div class="  form-check m-1 mt-3">
                                <input class="form-check-input" type="radio" name="monetary_unit" id="toman"
                                       value="toman"
                                    {{$result['monetary_unit']->extra_field1=='toman'?'checked':''}}>
                                <label class="form-check-label mr-4" for="toman">
                                    <small>
                                        تومان (Toman)
                                    </small>

                                </label>
                            </div>
                        </form>

                    </div>
                </div>
            @endcan

            @can('swap_trade_setting')
                <div class="dashboard-table transaction-table-container send-Announcements ">
                    <div>
                        <div class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                 fill="currentColor" class="bi bi-exclamation-circle m-1" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                <path
                                    d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z"/>
                            </svg>
                            <p class="mr-2">تنظیمات سواپ و ترید </p>
                        </div>

                        <div class="form-check swap-setting m-1 mt-3 d-flex">
                            <p class="mr-2 ">انتقال سواپ</p>
                            <label class="switch ">
                                <input id="move-swap-switch" type="checkbox" name="swap_status"
                                    {{$result['swap_status']->extra_field1==1?'checked':''}}>
                                <span class="slider round"></span>
                            </label>
                            @if($result['swap_status']->extra_field1==0)
                                <div>
                                    <img class="confirmations mr-3"
                                         src="{{asset('theme/user/images/Group%201786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$result['swap_status']->extra_field2}}">
                                </div>
                            @endif
                        </div>

                        <div class="form-check swap-setting m-1 mt-3 d-flex">
                            <p class="mr-2">ترید ریالی</p>
                            <label class="switch ">
                                <input id="rial-traid-switch" type="checkbox"
                                       name="trade_rial_status" {{$result['trade_rial_status']->extra_field1==1?'checked':''}}>
                                <span class="slider round"></span>
                            </label>
                            @if($result['trade_rial_status']->extra_field1==0)
                                <div>
                                    <img class="confirmations mr-3"
                                         src="{{asset('theme/user/images/Group%201786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$result['trade_rial_status']->extra_field2}}">
                                </div>
                            @endif
                        </div>

                        <div class="form-check swap-setting m-1 mt-3 d-flex">
                            <p class="mr-2">ترید USDT</p>
                            <label class="switch ">
                                <input id="USDT-traid-switch" type="checkbox"
                                       name="trade_usdt_status" {{$result['trade_usdt_status']->extra_field1==1?'checked':''}}>
                                <span class="slider round"></span>
                            </label>
                            @if($result['trade_usdt_status']->extra_field1==0)
                                <div>
                                    <img class="confirmations mr-3"
                                         src="{{asset('theme/user/images/Group%201786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$result['trade_usdt_status']->extra_field2}}">
                                </div>
                            @endif
                        </div>

                        <!-- Modal -->
                        <div class="modal fade modal-bank" id="myModal" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header mx-auto border-0">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="130" height="140"
                                             viewBox="0 0 147 150">
                                            <g id="Group_1783" data-name="Group 1783" transform="translate(-833 -230)">
                                                <g id="Group_1782" data-name="Group 1782"
                                                   transform="translate(278 180)">
                                                    <g id="Ellipse_118" data-name="Ellipse 118"
                                                       transform="translate(555 50)" fill="none"
                                                       stroke="#ff0c00" stroke-width="3">
                                                        <circle cx="73.5" cy="73.5" r="73.5" stroke="none"/>
                                                        <circle cx="73.5" cy="73.5" r="72" fill="none"/>
                                                    </g>
                                                    <text id="_" data-name="!" transform="translate(651 157)" fill="red"
                                                          font-size="80"
                                                          font-family="IRANSans" letter-spacing="0.01em">
                                                        <tspan x="-11.309" y="0">!</tspan>
                                                    </text>
                                                </g>
                                            </g>
                                        </svg>

                                    </div>
                                    <div class="modal-body border-0">
                                        <h3 class="text-center my-3">دلیل غیرفعال کردن این آیتم را بنویسید:</h3>
                                    </div>
                                    <form id="deactivate-form" action="{{route('admin.setting.trade-swap-status')}}"
                                          method="post">
                                        @csrf
                                        <input type="hidden" name="status" value="0">
                                        <div>
                                            <div dir="rtl"
                                                 class="request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">

                                                <input type="text" dir="rtl" class="currency-value-input"
                                                       name="deactivate_reason"
                                                       id="currency-value-input-1" placeholder="">
                                            </div>
                                        </div>
                                        <div class="modal-footer border-0">
                                            <button class="btn signin-btn deposit-btn mx-auto ajaxStore">
                                                تایید
                                            </button>
                                            <button type="button" class="btn signin-btn btn-danger mx-auto"
                                                    data-dismiss="modal">
                                                بستن
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endcan
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/dashboardSetting.js')}}"></script>
    <script>
        $('input[name=monetary_unit]').on('change', function () {
            let monetary_unit = $(this).val();
            let formData = {
                isObject: true,
                action: "{{route('admin.setting.monetary-unit')}}",
                method: 'post',
                button: $(this),
                data: {
                    monetary_unit: monetary_unit,
                }
            }
            ajax(formData);
        });


        $('input[name="swap_status"] , input[name="trade_rial_status"],input[name="trade_usdt_status"]').on('change', function (e) {
            let type = $(this).attr('name');
            if (!e.target.checked) {
                $('#deactivate-form').append(`<input type="hidden" name="type" value="${type}"/>`);
                $('#myModal').modal('show');
            } else {
                let status = 1;
                let formData = {
                    isObject: true,
                    action: "{{route('admin.setting.trade-swap-status')}}",
                    method: 'post',
                    button: $(this),
                    data: {
                        type: type,
                        status: status,
                    }
                }
                ajax(formData);
            }
        });

    </script>
@endsection
