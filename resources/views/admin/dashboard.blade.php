@extends('templates.admin.master_page')
@section('title_browser')
    داشبورد
@endsection

@section('content')
    <section class="admin-dashboard dashboard-cart navigation">
        <div class="container">
            <div class="row justify-content-between">
                <div>
                    <h1 class="dashboard-title desktop-title mr-2">
                        داشبورد صرافی
                    </h1>
                </div>
                <div dir="ltr" class="calender__selectbox col-12 col-md-6 col-lg-3">
                    <div dir="rtl" class="w-100">
                        <select class="form-control custom-select-2 w-100"
                            name="currency2">
                            <option
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                value="0"
                                selected>
                                ۲۸ مهر ۱۴۰۰ - ۲۸ آبان ۱۴۰۰
                            </option>
                            <option
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                value="0"
                                selected>
                                ۳۰ مهر ۱۴۰۰ - ۳۰ آبان ۱۴۰۰
                            </option>
                            <option
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                value="0"
                                selected>
                                ۲ مهر ۱۴۰۰ - ۲ آبان ۱۴۰۰
                            </option>
                            <option
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                value="0"
                                selected>
                                ۱۰ مهر ۱۴۰۰ - ۱۰ آبان ۱۴۰۰
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row card-section">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="cart-currency">
                        <div class="d-flex cart-currency__info justify-between">
                            <div class="cart-transaction">
                                <p>درخواست تراکنش های ۲۴ ساعته</p>
                                <div class="d-flex items-center">
                                    <span> ۳۸ درخواست در صف </span>
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/Icon-Exclamation.svg')}}"
                                        width="20"
                                        height="20"
                                        alt="notif"/>
                                </div>
                            </div>

                            <div class="cart-currency__about-name d-flex items-center">
                                <div
                                    class="cart-currency__names d-flex flex-column items-end cart-currency__about-price"
                                >
                                    <h5 class="cart-currency__abbreviation">۳۰۰</h5>
                                    <span class="plus">۳%+</span>
                                </div>
                            </div>
                        </div>

                        <div class="cart-currency__chart mt-4">
                            <canvas id="cart-chart" class="cart-chart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="cart-currency">
                        <div class="d-flex cart-currency__info justify-between">
                            <div class="cart-transaction">
                                <p>درخواست تراکنش های ۲۴ ساعته</p>
                                <div class="d-flex items-center">
                                    <span> ۳۸ درخواست در صف </span>
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/Icon-Exclamation.svg')}}"
                                        width="20"
                                        height="20"
                                        alt="notif"
                                    />
                                </div>
                            </div>

                            <div class="cart-currency__about-name d-flex items-center">
                                <div
                                    class="cart-currency__names d-flex flex-column items-end cart-currency__about-price"
                                >
                                    <h5 class="cart-currency__abbreviation">۳۰۰</h5>
                                    <span class="plus">۳%+</span>
                                </div>
                            </div>
                        </div>

                        <div class="cart-currency__chart mt-4">
                            <canvas id="cart-chart" class="cart-chart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="cart-currency">
                        <div class="d-flex cart-currency__info justify-between">
                            <div class="cart-transaction">
                                <p>درخواست تراکنش های ۲۴ ساعته</p>
                                <div class="d-flex items-center">
                                    <span> ۳۸ درخواست در صف </span>
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/Icon-Exclamation.svg')}}"
                                        width="20"
                                        height="20"
                                        alt="notif"
                                    />
                                </div>
                            </div>

                            <div class="cart-currency__about-name d-flex items-center">
                                <div
                                    class="cart-currency__names d-flex flex-column items-end cart-currency__about-price"
                                >
                                    <h5 class="cart-currency__abbreviation">۳۰۰</h5>
                                    <span class="plus">۳%+</span>
                                </div>
                            </div>
                        </div>

                        <div class="cart-currency__chart mt-4">
                            <canvas id="cart-chart" class="cart-chart"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="cart-currency">
                        <div class="d-flex cart-currency__info justify-between">
                            <div class="cart-transaction">
                                <p>درخواست تراکنش های ۲۴ ساعته</p>
                                <div class="d-flex items-center">
                                    <span> ۳۸ درخواست در صف </span>
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/Icon-Exclamation.svg')}}"
                                        width="20"
                                        height="20"
                                        alt="notif"
                                    />
                                </div>
                            </div>

                            <div class="cart-currency__about-name d-flex items-center">
                                <div
                                    class="cart-currency__names d-flex flex-column items-end cart-currency__about-price"
                                >
                                    <h5 class="cart-currency__abbreviation">۳۰۰</h5>
                                    <span class="plus">۳%+</span>
                                </div>
                            </div>
                        </div>

                        <div class="cart-currency__chart mt-4">
                            <canvas id="cart-chart" class="cart-chart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end-navigation-->

    <section class="admin-dashboard dashboard-chart">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 responsive-chart">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">
                                درآمد کل : ۳.۰۰۰.۰۰۰ تومان
                            </h2>

                            <select
                                class="form-control custom-select-3"
                                name="currency2"
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}">
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="0">
                                    یک ماه گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="1">
                                    یک هفته گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="2"
                                    selected>
                                    یک روز گذشته
                                </option>
                            </select>
                        </div>

                        <div class="chart" style="height: 300px">
                            <canvas id="market-volume-chart"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">بازدید سایت</h2>

                            <select
                                class="form-control custom-select-3"
                                name="currency2"
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}">
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="0">
                                    یک ماه گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="1">
                                    یک هفته گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="2"
                                    selected>
                                    یک روز گذشته
                                </option>
                            </select>
                        </div>

                        <div style="height: 300px">
                            <canvas id="site-view"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="admin-dashboard price-table-currency">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="market-volume">
                        <div class="d-flex flex-column justify-between h-100">
                            <div class="wage-box d-flex items-center justify-between">
                                <img
                                    src="{{asset('theme/admin/img/admin-dashboard/wage.svg')}}"
                                    class="wage__box-img"
                                    alt="wage"
                                />
                                <div class="wage-box__info">
                                    <h3 class="wage-box__title">829<span>$</span></h3>
                                    <p class="wage-box__desc">کارمزد ترید بر ماه</p>
                                </div>
                            </div>
                            <hr class="admin-dashboard-hr" />
                            <div class="wage-box d-flex items-center justify-between">
                                <img
                                    src="{{asset('theme/admin/img/admin-dashboard/wage-swap.svg')}}"
                                    class="wage__box-img"
                                    alt="wage"
                                />
                                <div class="wage-box__info">
                                    <h3 class="wage-box__title">829<span>$</span></h3>
                                    <p class="wage-box__desc">کارمزد سوآپ بر ماه</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-5 mt-sm-1">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">فعالیت کاربران</h2>

                            <select
                                class="form-control custom-select-3"
                                name="currency2"
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                            >
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="daily"
                                >
                                    روزانه
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="weekly"
                                >
                                    هفتگی
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="monthly"
                                    selected
                                >
                                    ماهانه
                                </option>
                            </select>
                        </div>

                        <canvas style="height: 210px" id="bar-chart"></canvas>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-md-1">
                    <div class="market-volume">
                        <div class="d-flex justify-between market-volume__info">
                            <h2 class="market-volume-title">کار ارز</h2>

                            <select
                                class="form-control custom-select-3"
                                name="currency2"
                                data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                            >
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="0"
                                >
                                    یک ماه گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="1"
                                >
                                    یک هفته گذشته
                                </option>
                                <option
                                    data-image="{{asset('theme/admin/img/admin-dashboard/Icon feather-calendar.svg')}}"
                                    value="2"
                                    selected
                                >
                                    یک روز گذشته
                                </option>
                            </select>
                        </div>

                        <div class="d-flex items-center justify-between">
                            <div class="currency-result"></div>

                            <div style="width: 210px" class="doughnut-chart-container">
                                <canvas id="doughnut-chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section style="margin-top: 1rem" class="admin-dashboard user-section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="market-volume table-container">
                        <table class="user-table">
                            <thead>
                            <tr>
                                <th class="pb-4">
                                    <span class="table-title">نام کاربر</span>
                                </th>
                                <th class="text-center pb-4">
                                    <span class="table-title">تاریخ</span>
                                </th>
                                <th class="text-center pb-4">
                                    <span class="table-title">مقدار</span>
                                </th>
                                <th class="text-left pb-4">
                                    <span class="table-title">وضعیت</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-2">
                                    <span class="name">حسین شاهپوری</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="date">1400.02.03</span>
                                </td>
                                <td class="text-center py-2">
                                    <span class="amount">1000 usdt</span>
                                </td>
                                <td class="text-left py-2">
                                    <span class="status plus"> تایید شده </span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4 mt-sm-1">
                    <div class="market-volume table-container">
                        <table class="user-table">
                            <tbody>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="wallet-table__curency-desc">
                                        <a
                                            href="#"
                                            class="wallet-table__currency-img-container"
                                        >
                                            <img
                                                src="{{asset('theme/admin/img/wallet/wallet-table-eth.png')}}"
                                                alt="تایتل ارز"
                                            />
                                        </a>

                                        <div class="wallet-table__curency-title-container">
                                            <span>Ethereum</span>
                                            <p>ETH</p>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <img
                                        src="{{asset('theme/admin/img/admin-dashboard/table-chart.svg')}}"
                                        alt="chart"
                                        class="table-chart"
                                    />
                                </td>

                                <td class="text-left">
                                    <div
                                        class="wallet-table__curency-title-container text-left"
                                    >
                                        <span>$18.111</span>
                                        <p>ETH</p>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-12 col-lg-4 mt-md-1">
                    <div class="market-volume">
                        <table class="user-table currency-info" style="direction: ltr !important">
                            <tbody>
                            <tr>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                                <td class="py-4">
                                    <div class="currency-info">
                                        <h3>BTCdominance</h3>
                                        <h3>$2.5B</h3>
                                        <span class="plus">6.15%+</span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/admin-dashboard-charts.js')}}"></script>
    <script src="{{asset('theme/user/scripts/custom-select-box-3.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.custom-select-2').customSelect2(function (val) {});

            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

            $('.custom-select-3').customSelect3(function (val) {
                if (val === 'daily') {
                    barChart.data.labels = chartLabels.daily;

                    barChart.update();
                } else if (val === 'weekly') {
                    barChart.data.labels = chartLabels.weekly;

                    barChart.update();
                } else if (val === 'monthly') {
                    barChart.data.labels = chartLabels.monthly;

                    barChart.update();
                }
            });
        });

    </script>

@endsection
