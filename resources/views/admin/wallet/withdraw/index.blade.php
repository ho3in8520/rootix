@extends('templates.admin.master_page')
@section('title_browser')
    لیست درخواست های برداشت-پنل مدیریت روتیکس
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
@endsection
@section('content')
    <section class="withdraw pt-2" id="basic-form-layouts">

        <div class="dashboard-cart requests-withdrawal">
            <div class="container">
                <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                    لیست درخواست های برداشت
                </h1>

                <div>
                    <div class="dashboard-table transaction-table-container">

                        <div class="filters-container">
                            <button class="col-12 filter-btn" data-toggle="modal"
                                    data-target="#toggle-filters-inputs">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.814" height="20.15"
                                     viewBox="0 0 23.814 20.15">
                                    <g transform="translate(-3.375 -5.625)">
                                        <path
                                            d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                            transform="translate(0 -3.554)"/>
                                        <path
                                            d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                            transform="translate(0 -1.777)"/>
                                        <path
                                            d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"/>
                                    </g>
                                </svg>
                                <span class="pr-1">فیلترها</span>
                            </button>
                            <div id="toggle-filters-inputs" class="up-animation modal">
                                <div class="modal-content pt-0">

                                    <div class="modal-header">
                                        <h6 class="modal-title">فیلترهای جدول</h6>
                                        <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">
                                            &times;
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        <div class="form-controlers pb-2">
                                            <form action="" method="get" style="display:contents">
                                                <div class="header-transactionrls mb-3">
                                                    <div class="w-100  bg-white">
                                                        <label
                                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16">کد
                                                            کاربری</label>
                                                        <input type="text" name="code"
                                                               value="{{ request()->query('code') }}" id="LblEmail">
                                                    </div>

                                                    <div class="w-100  bg-white">
                                                        <label
                                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16">از
                                                            تاریخ (درخواست)</label>
                                                        <input type="text" name="date_from"
                                                               value="{{ request()->query('date_from') }}"
                                                               class="datePicker">
                                                    </div>

                                                    <div class="w-100  bg-white">
                                                        <label
                                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16">تا
                                                            تاریخ (درخواست)</label>
                                                        <input type="text" class="last-input datePicker"
                                                               name="date_to"
                                                               value="{{ request()->query('date_to') }}">
                                                    </div>

                                                    <div class="w-100  bg-white">
                                                        <label class="mb-2 mt-3 d-block LblEmail fs-16">ایمیل</label>
                                                        <input type="text" name="email"
                                                               value="{{ request()->query('email') }}">
                                                    </div>


                                                    <div class="w-100  bg-white">
                                                        <label
                                                            class="mb-2 mt-3 d-block transaction__filter-title fs-16">
                                                            مبلغ</label>
                                                        <input type="text" name="amount"
                                                               value="{{ request()->query('amount') }}"
                                                               style="font-size: 10.5px">
                                                    </div>


                                                </div>
                                                <div class="header-transactionrls mb-3">
                                                    <div class="w-25  bg-white">
                                                        <label class="mb-2 mt-3 d-block LblVariz fs-16">وضعیت</label>
                                                        <select class="last-input custom-selected" name="status">
                                                            <option
                                                                value="" {{ request()->query('status')==''?'selected':'' }}>
                                                                انتخاب کنید...
                                                            </option>
                                                            <option
                                                                value="0" {{ request()->query('status')=='0'?'selected':'' }}>
                                                                درحال بررسی
                                                            </option>
                                                            <option
                                                                value="1" {{ request()->query('status')=='1'?'selected':'' }}>
                                                                تایید ادمین
                                                            </option>
                                                            <option
                                                                value="2" {{ request()->query('status')=='2'?'selected':'' }}>
                                                                تایید صرافی
                                                            </option>
                                                            <option
                                                                value="3" {{ request()->query('status')=='3'?'selected':'' }}>
                                                                رد ادمین
                                                            </option>
                                                            <option
                                                                value="4" {{ request()->query('status')=='4'?'selected':'' }}>
                                                                رد صرافی
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="w-100 mt-3">
                                                        <button type="button"
                                                                class="btn fixed-bottom transaction__btn2 search-ajax"
                                                                data-dismiss="modal">جستجو
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="withdraw-rls">
                                            <thead>
                                            <tr>
                                                <th class="text-right LblNamefamilyall text-nowarp">
                                                    <div class="title-header">
                                                        کد کاربری
                                                    </div>
                                                </th>
                                                <th style="display: inline">
                                                    <div class="title-header">
                                                        تاریخ درخواست
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        ایمیل
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        ارز
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        مبلغ
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="title-header">
                                                        کارمزد
                                                    </div>
                                                </th>

                                                <th>
                                                    <div class="title-header">
                                                        وضعیت
                                                    </div>
                                                </th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($withdraws->count() > 0)
                                                @foreach($withdraws as $withdraw)
                                                    <tr>
                                                        <td>
                                                            <div class="withdraw-rls">
                                                                <span>{{ $withdraw->user->code }}</span>
                                                            </div>
                                                        </td>

                                                        <td>
                                                              <span class="withdraw-rls d-block text-center">
                                                                  {{ jdate_from_gregorian($withdraw->created_at,'Y/m/d') }}
                                                              </span>
                                                            <span class="withdraw-rls">
                                                                {{ jdate_from_gregorian($withdraw->created_at,'H:i') }}
                                                              </span>
                                                        </td>

                                                        <td>
                                                            <span class="withdraw-rls">
                                                                {{ $withdraw->user->email }}
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <span class="withdraw-rls">{{ $withdraw->unit }}</span>
                                                        </td>
                                                        <td>
                                                            <span class="withdraw-rls">{{ $withdraw->amount }}</span>
                                                        </td>
                                                        <td>
                                                            <span class="withdraw-rls">{{ $withdraw->fee }}</span>
                                                        </td>

                                                        <td class="d-flex justify-content-center m-3">
                                                            {!! $withdraw->statusAdmin('border',true) !!}
                                                        </td>

                                                        <td class="withdraw-rls-btns-td">
                                                            @can('more_info')
                                                                <div class="wallet-table-btns d-flex flex-column">
                                                                    <a href="{{ route('admin.request.withdraw.show',$withdraw->id) }}"
                                                                       class="withdraw-rls-topbtn text-dark"
                                                                       data-target="#moreInfo">اطلاعات تکمیلی
                                                                    </a>
                                                                </div>
                                                            @endcan
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <th colspan="6" class="text-center">موردی یافت نشد!</th>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    {!! $withdraws->links() !!}

                                    <div class="pdf-exel mr-3 mt-5 mb-5">
                                        <a class="btn-pdf ml-3"
                                           href="{{ request()->fullUrlWithQuery(['export'=>'pdf']) }}">خروجی PDF
                                            <img class="pdf-image"
                                                 src="{{ asset('theme/user/images/Icon metro-file-pdf.svg') }}" alt="">
                                        </a>
                                        <a class="btn-exel"
                                           href="{{ request()->fullUrlWithQuery(['export'=>'excel']) }}">خروجی Excel
                                            <img class="excel-image"
                                                 src="{{ asset('theme/user/images/Icon simple-microsoftexcel.svg') }}"
                                                 alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
