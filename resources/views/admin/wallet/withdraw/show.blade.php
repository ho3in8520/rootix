@extends('templates.admin.master_page')
@section('title_browser')
    درخواست برداشتپنل مدیریت روتیکس
@endsection
@section('content')
    <section class="pt-2" id="basic-form-layouts">
        <div class="container">
            <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                درخواست برداشت
            </h1>


            <div class="dashboard-table pb-5 pr-3 pl-3">
                <div class="texttop pr-1">
                    <h5>مشخصات کاربر</h5>
                </div>

                <div class="row pt-3 pb-2 align-items-end">
                    <div class="col-md-3 pb-4 ">
                        <div class="fildforinput d-flex">
                            <p class="p-4">نام</p>
                            <input type="text" readonly value="{{ $result->user->full_name }}"
                                   class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                    <div class="col-md-3 pb-4">
                        <div class="fildforinput d-flex">
                            <p class="p-4">کدکاربر</p>
                            <input type="text" readonly value="{{ $result->user->code }}"
                                   class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                    <div class="col-md-3  d-flex pb-4  ">
                        <div class="fildforinput d-flex">
                            <p class="p-4">ایمیل</p>
                            <input type="text" readonly value="{{ $result->user->email }}"
                                   class="font-weight-bold inputs2 pl-3">
                        </div>
                    </div>

                    <div class="col-md-3  d-flex  pb-4">
                        <div class="fildforinput d-flex">
                            <p class="p-4">سطح</p>
                            <input type="text" readonly value="{{ $result->user->step_complate }}"
                                   class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                </div>
                <div class="dash"></div>

                <div class="row pr-1 pt-3 ">
                    <div class=" col-md-12 texttop pt-3 order-0">
                        <h5>اطلاعات درخواست</h5>
                    </div>

                </div>

                <div class="row  align-items-end">
                    <div class="col-md-3 pb-4 ">
                        <div class="fildforinput d-flex">
                            <p class="p-4">نام ارز</p>
                            <input type="text" readonly value="{{ strtoupper($result->unit) }}"
                                   class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                    <div class="col-md-3 pb-4">
                        <div class="fildforinput d-flex">
                            <p class="p-4">شبکه </p>
                            <input type="text" readonly value="{{ strtoupper($result->network) }}"
                                   class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                    <div class="col-md-3 pb-4">
                        <label class="pb-3">مبلغ درخواست شده با احتساب کارمزد</label>
                        <div class="fildforinput d-flex  text-center">
                            <input type="text" readonly value="{{ $result->amount - $result->fee }}"
                                   class="font-weight-bold inputs2 inputscenter pl-3">
                        </div>
                    </div>

                    <div class="col-md-3   pb-4">
                        <div class="fildforinput d-flex">
                            <p class="p-4">کارمزد</p>
                            <input type="text" readonly value="{{ $result->fee }}" class="font-weight-bold inputs pl-3">
                        </div>
                    </div>
                    <div class="col-md-3 pb-4 ">
                        <label class="pb-3">موجودی ارز در کیف پول ادمین</label>
                        <div class="fildforinput d-flex text-center">
                            <span class="spinner-border spinner-border-sm"></span>
                            <input type="text" readonly value=""
                                   class="font-weight-bold inputscenter inputs2 pl-3 amount-admin">
                        </div>
                    </div>
                    <div class="col-md-3 pb-4">
                        <label class="pb-3">آدرس کیف پول مقصد</label>
                        <div class="fildforinputborder d-flex">
                            <p class="p-4"></p>
                            <input id="address" data-content="{{ $result->address }}" type="text" readonly
                                   value="{{ $result->address }}" class="font-weight-bold inputs2 text-left pl-3">
                            <button type="button" class="copy-btn">
                                <img class="iconcopy" src="{{ asset('theme/user/images/copy2.svg') }}" alt=""
                                     onclick="copy_text('address')">

                            </button>
                            <div class="tooltip-address ">
                                <span class="tooltip-address__text tooltiptop" style="position: absolute">
                                  کپی شد
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 pb-4">
                        <div class="fildforinput d-flex text-center ">
                            <p class="p-4">وضعیت</p>
                            <p class="p-4">{!! $result->statusAdmin('span') !!}</p>
                        </div>
                    </div>

                    <div class="col-md-3   pb-4">
                        <label class="pb-3">تاریخ ایجاد درخواست</label>
                        <div class="fildforinput d-flex">

                            <input type="text" readonly
                                   value="{{ jdate_from_gregorian($result->created_at,'Y.m.d H:i:s') }}"
                                   class="font-weight-bold inputscenter inputs2 pl-3">
                        </div>
                    </div>
                </div>

                @if($result->status==0 || $result->status==3)
                    <div class="row pt-4">
                        @can('currency_withdraw_accept')
                            <div class="col-md-6 d-flex forbtntow pb-4">
                                <button class="btn btnsize deposit-btn confirm text-white">
                                    تایید کردن
                                </button>
                            </div>
                        @endcan
                        @can('currency_withdraw_reject')
                            <div class="col-md-6 d-flex p-0 ">
                                <button class="btn btnsize harvest-btn upload__image"
                                        data-toggle="modal"
                                        data-target="#modal4">
                                    رد کردن
                                </button>
                            </div>
                        @endcan
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="modal fade" id="modal4" role="dialog">
                                <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <form method="post"
                                              action="{{ route("admin.request.withdraw.update-status") }}">
                                            @csrf
                                            <input type="hidden" name="mode" value="2">
                                            <input type="hidden" name="key" value="{{ $result->id }}">

                                            <div class="modal-body border-0">
                                                <h3 class="text-center my-3">علت رد کردن را بنویسید</h3>
                                                <p class="my-4">
                                                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                    استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله
                                                    در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد

                                                </p>
                                            </div>
                                            <div>
                                                <div dir="rtl"
                                                     class=" request-box request-box-2 request-js-box-2 modal__input d-flex enabled justify-between align-items-center active">

                                                    <span class="request-box__title">علت</span>

                                                    <input type="text" dir="ltr" class="currency-value-input"
                                                           name="reject_reason"
                                                           id="currency-value-input-1" placeholder="بنویسید...">
                                                </div>
                                            </div>
                                            <div class="modal-footer border-0 justify-content-center">
                                                <button
                                                    class="btn signin-btn deposit-btn ajaxStore">
                                                    تایید
                                                </button>

                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    انصراف
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('script')

    <script src="{{ asset('theme/user/scripts/copyText.js') }}"></script>
    <script>
        $(document).ready(function () {
            $.post(httpToHttps(`{{ route('admin.request.withdraw.balance-admin') }}`), {
                _token: `{{ csrf_token() }}`,
                unit: `{{ $result->unit }}`
            }, balance_admin)

            function balance_admin(response) {
                if (response.status == 100) {
                    $(".spinner-border").hide();
                    $(".amount-admin").val(response.amount)
                }
            }

            $(".confirm").click(function () {
                swal({
                    title: "اطمینان از تایید درخواست دارید؟",
                    text: "پس از تایید درخواست به سمت بلاکچین ارسال میشود و امکان لغو درخواست وجود ندارد",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.post(httpToHttps(`{{ route('admin.request.withdraw.update-status') }}`), {
                                _token: `{{ csrf_token() }}`
                                , mode: 1, key: `{{ $result->id }}`
                            }, function (response) {
                                if (response.status == 100) {
                                    swalResponse(response.status, response.msg, 'موفق');
                                    setTimeout(function () {
                                        window.location.href = response.url;
                                    }, 3000)
                                } else {
                                    swalResponse(response.status, response.msg, 'خطا');
                                }
                            });
                        }
                    });
            })
        })
    </script>
@endsection
