@extends('templates.admin.master_page')
@section('title_browser')لیست برداشت ریالی-پنل مدیریت روتیکس@endsection

@section('content')
    <section id="basic-form-layouts" class="withdraw pt-2 dashboard-cart system-reports">
        <!-- Confirm Modal -->
        <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="confirmModalLabel">تایید و واریز کردن درخواست برداشت کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>کد پیگیری</label>
                                <input type="text" class="form-control" name="tracking_code">
                            </div>
                            <div class="form-group">
                                <label>روش واریز</label>
                                <select class="form-control" name="deposit_type">
                                    <option value="0">شتاب</option>
                                    <option value="1">پایا</option>
                                    <option value="2">سانتا</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>توضیحات مدیر</label>
                                <textarea class="form-control" rows="3" name="admin_des"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                            <button type="button" class="btn btn-info confirm-withdraw ajaxStore">تایید و واریز</button>
                        </div>
                    </form>

                </div>
            </div>
        </div> <!--## Confirm Modal -->

        <!-- Reject Modal -->
        <div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="rejectModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="">
                        @csrf
                        <div class="modal-header">
                            <h5 class="modal-title" id="rejectModalLabel">رد کردن درخواست برداشت کاربر</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>دلیل رد درخواست</label>
                                <textarea class="form-control mt-1" rows="3" name="admin_des"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                            <button type="button" class="btn btn-info confirm-withdraw ajaxStore">تایید و رد کردن
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div> <!--## Reject Modal -->

        <!-- More Modal -->
        <div class="modal fade" id="moreModal" tabindex="-1" role="dialog" aria-labelledby="moreModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                            <tr class="amount">
                                <th>مبلغ</th>
                                <td></td>
                            </tr>
                            <tr class="fee">
                                <th>کامزد</th>
                                <td></td>
                            </tr>
                            <tr class="total_amount">
                                <th>مبلغ قابل پرداخت</th>
                                <td></td>
                            </tr>
                            <tr class="status">
                                <th>وضعیت</th>
                                <td><span></span></td>
                            </tr>
                            <tr class="user_des">
                                <th>توضیحات کاربر</th>
                                <td></td>
                            </tr>
                            <tr class="admin_des">
                                <th>توضیحات ادمین</th>
                                <td></td>
                            </tr>
                            <tr class="card_number">
                                <th>شماره کارت</th>
                                <td></td>
                            </tr>
                            <tr class="sheba_number">
                                <th>شماره شبا</th>
                                <td></td>
                            </tr>
                            <tr class="account_number">
                                <th>شماره حساب</th>
                                <td></td>
                            </tr>
                            <tr class="tracking_code">
                                <th>کد پیگیری</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_type">
                                <th>نوع واریز</th>
                                <td></td>
                            </tr>
                            <tr class="deposit_date">
                                <th>تاریخ واریز</th>
                                <td></td>
                            </tr>
                            <tr class="created_at">
                                <th>تاریخ ایجاد</th>
                                <td></td>
                            </tr>
                            <tr class="updated_at">
                                <th>تاریخ بروزرسانی</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">بستن</button>
                    </div>
                </div>
            </div>
        </div> <!--## More Modal -->

        <div class="container">
            <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                لیست برداشت ریالی
            </h1>

            <div>
                <div class="dashboard-table transaction-table-container">

                    <div class="filters-container">
                        <button class="col-12 filter-btn" data-toggle="modal" data-target="#toggle-filters-inputs">
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.814" height="20.15"
                                 viewBox="0 0 23.814 20.15">
                                <g transform="translate(-3.375 -5.625)">
                                    <path
                                        d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -3.554)"/>
                                    <path
                                        d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                        transform="translate(0 -1.777)"/>
                                    <path
                                        d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"/>
                                </g>
                            </svg>
                            <span class="pr-1">فیلترها</span>
                        </button>
                        <div id="toggle-filters-inputs" class="up-animation modal">
                            <div class="modal-content pt-0">

                                <div class="modal-header">
                                    <h6 class="modal-title">فیلترهای جدول</h6>
                                    <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">&times;
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="form-controlers pb-2">
                                        <form method="get" action="">
                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblName fs-16">نام خانوادگی</label>
                                                    <input type="text" name="last_name"
                                                           value="{{ request()->last_name }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblEmail fs-16">ایمیل</label>
                                                    <input type="text" name="email" value="{{ request()->email }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">کد
                                                        کاربر</label>
                                                    <input type="text" name="user_code"
                                                           value="{{ request()->user_code }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">از
                                                        مبلغ</label>
                                                    <input type="text" name="start_amount"
                                                           value="{{ request()->start_amount }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تا
                                                        مبلغ</label>
                                                    <input type="text" name="end_amount"
                                                           value="{{ request()->end_amount }}">
                                                </div>
                                            </div>

                                            <div class="header-transactionrls mb-3">
                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblName fs-16">کد پیگیری</label>
                                                    <input type="text" name="tracking_code"
                                                           value="{{ request()->tracking_code }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">نوع واریز</label>
                                                    <select class="custom-selected" name="deposit_type">
                                                        <option value="">همه</option>
                                                        <option
                                                            value="0" {{ request()->deposit_type=='0'?'selected':'' }}>
                                                            شتاب
                                                        </option>
                                                        <option
                                                            value="1" {{ request()->deposit_type==='1'?'selected':'' }}>
                                                            پایا
                                                        </option>
                                                        <option
                                                            value="2" {{ request()->deposit_type==='2'?'selected':'' }}>
                                                            سانتا
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">از
                                                        تاریخ (واریز)</label>
                                                    <input type="text" class="datePicker" name="start_deposit_date"
                                                           value="{{ request()->start_deposit_date }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">تا
                                                        تاریخ (واریز)</label>
                                                    <input type="text" name="end_deposit_date" class="datePicker"
                                                           value="{{ request()->end_deposit_date }}">
                                                </div>

                                                <div class="w-100  bg-white">
                                                    <label class="mb-2 mt-3 d-block LblVariz fs-16">وضعیت</label>
                                                    <select class="last-input custom-selected" name="status">
                                                        <option value="">همه</option>
                                                        <option value="0" {{ request()->status==='0'?'selected':'' }}>
                                                            جدید
                                                        </option>
                                                        <option value="1" {{ request()->status==='1'?'selected':'' }}>
                                                            واریز شده
                                                        </option>
                                                        <option value="2" {{ request()->status==='2'?'selected':'' }}>رد
                                                            شده
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="w-100 date-transaction bg-white">
                                                <button type="button"
                                                        class="btn fixed-bottom transaction__btn2 search-ajax"
                                                        data-dismiss="modal">جستجو
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive mt-4">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="withdraw-rls">
                                    <thead>
                                    <tr>
                                        <th class="text-right LblNamefamilyall text-nowarp">
                                            نام ونام خانوادگی
                                        </th>
                                        <th>
                                            <div class="">
                                                ایمیل
                                            </div>
                                        </th>
                                        <th>
                                            <div>
                                                کد کاربر
                                            </div>
                                        </th>
                                        <th>
                                            <div>
                                                کد پیگیری
                                            </div>
                                        </th>

                                        <th>
                                            <div>
                                                مبلغ
                                            </div>
                                        </th>

                                        <th>
                                            <div>
                                                کارمزد
                                            </div>
                                        </th>

                                        <th class=" text-nowarp LblNamefamilyall">
                                            مبلغ قابل پرداخت
                                        </th>

                                        <th>
                                            <div>
                                                وضعیت
                                            </div>
                                        </th>


                                        <th>
                                            <div>

                                            </div>
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if ($withdraw_rial->count() >0)
                                        @foreach($withdraw_rial as $item)
                                            <tr data-more="{{ json_encode($item) }}"
                                                data-bank="{{ json_encode($item->bank) }}"
                                                data-status="{{ json_encode($item->status_info) }}">
                                                <td>{{ $item->user->full_name }}</td>
                                                <td>{{ $item->user->email }}</td>
                                                <td>{{ $item->user->code }}</td>
                                                <td>{{ $item->tracking_code?$item->tracking_code:'-' }}</td>
                                                <td>{{ number_format($item->amount) }} ریال</td>
                                                <td>{{ number_format($item->fee) }} ریال</td>
                                                <td>{{ number_format($item->amount-$item->fee) }} ریال</td>
                                                <td class="d-flex justify-content-center m-3">
                                                    <span class="withdraw-rls border-{{ $item->status_info['type'] }}">
                                                        {{ $item->status_info['text'] }}
                                                    </span>
                                                </td>


                                                <td class="withdraw-rls-btns-td">
                                                    <div class="wallet-table-btns d-flex flex-column">
                                                        <p class="withdraw-rls-topbtn more-btn" style="cursor: pointer"
                                                           data-toggle="modal"
                                                           data-target="#moreModal">اطلاعات تکمیلی</p>

                                                        @if($item->status==0)
                                                            <div class="d-flex align-items-center">
                                                                @can('rial_withdraw_accept')
                                                                    <button type="button"
                                                                            class="btn withdraw-rls-btn  harvest-btn2 mr-0 p-0 confirm-btn"
                                                                            data-toggle="modal"
                                                                            data-target="#confirmModal"
                                                                            data-action="{{ route('admin.withdraw-rial.confirm',$item->id) }}">
                                                                        تایید کردن
                                                                    </button>
                                                                @endcan
                                                                @can('rial_withdraw_reject')
                                                                    <button type="button"
                                                                            class="btn withdraw-rls-btn  deposit-btn2 reject-btn"
                                                                            data-toggle="modal"
                                                                            data-target="#rejectModal"
                                                                            data-action="{{ route('admin.withdraw-rial.reject',$item->id) }}">
                                                                        رد
                                                                        کردن
                                                                    </button>
                                                                @endcan
                                                            </div>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="20" class="text-center">موردی جهت نمایش وجود ندارد</td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>

                                <div class="row d-flex justify-content-between align-items-center flex-row-reverse">
                                    {{ $withdraw_rial->withQueryString()->links() }}
                                    <div class="pdf-exel pr-3 mt-5">
                                        <button class="btn-pdf mt-2">
                                            <p class="card-text">
                                                <a href="{{ request()->fullUrlWithQuery(['export'=>'pdf']) }}"
                                                   class="pdf-text">خروجی PDF</a>
                                            </p>
                                            <img class="pdf-image"
                                                 src="{{ asset('theme/user/images/Icon%20metro-file-pdf.svg') }}">
                                        </button>
                                        <button class="btn-exel mt-2 mr-2">
                                            <p class="card-text">
                                                <a href="{{ request()->fullUrlWithQuery(['export'=>'excel']) }}"
                                                   class="exel-text">خروجی Excel</a>
                                            </p>
                                            <img class="excel-image"
                                                 src="{{ asset('theme/user/images/Icon%20simple-microsoftexcel.svg') }}">
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@endsection

@section('script')
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script>
        $('body').delegate('.reject-btn,.confirm-btn','click',function () {
            let action = $(this).data('action');
            let target = $(this).data('target');
            $(`${target} form`).attr('action', action);
        })
        $('.more-btn').click(function () {
            let data = $(this).parents('tr').data('more');
            let bank = $(this).parents('tr').data('bank');
            let status = $(this).parents('tr').data('status');
            let table = $("#moreModal table tbody");
            table.find(".amount td:last").text(numberFormat(data.amount) + ' ریال ');
            table.find(".fee td:last").text(numberFormat(data.fee) + ' ریال ');
            table.find(".total_amount td:last").text(numberFormat(data.amount - data.fee) + ' ریال ');
            table.find(".status td:last span").text(status['text']);
            table.find(".status td:last span").addClass('badge badge-' + status['color']);
            table.find(".user_des td:last").text(data.user_des);
            table.find(".admin_des td:last").text(data.admin_des);
            table.find(".card_number td:last").text(bank.name + ' - ' + bank.card_number);
            table.find(".sheba_number td:last").text(bank.sheba_number);
            table.find(".account_number td:last").text(bank.account_number);
            table.find(".tracking_code td:last").text(data.tracking_code);
            table.find(".deposit_type td:last").text(data.deposit_type);
            table.find(".deposit_date td:last").text(new Date(data.deposit_date).toLocaleString('fa-IR'));
            table.find(".created_at td:last").text(new Date(data.created_at).toLocaleString('fa-IR'));
            table.find(".updated_at td:last").text(new Date(data.updated_at).toLocaleString('fa-IR'));
        })
    </script>
@endsection
