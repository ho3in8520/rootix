@extends('templates.admin.master_page')

@section('title_browser')
    مدیریت کارمزدها-پنل مدیریت روتیکس
@endsection

@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                مدیریت کارمزدها
            </h1>
            <div class="dashboard-table pb-5 ">
                <!-- مدیریت کارمزد سواپ ارزها -->
                @can('swap_fees')
                    <div class="row">
                        <form class="row col-12" action="{{route('admin.manage.fees.store')}}" method="post">
                            @csrf
                            <input type="hidden" name="name" value="swap_fee">
                            <div class="col-12">
                                <div class="texttop p-4">
                                    <h5>مدیریت کارمزد سواپ ارزها</h5>
                                </div>
                                <div class="row pb-3">
                                    <div class="col-md-4 col-xs-12 pr-2 respcol2 ">
                                        <div class="karmozd-traid d-flex ">
                                            <p class="p-4"> نوع ارز </p>
                                            <select
                                                class="form-control swap-fee-select col-4 swap-fee-select custom-select-2"
                                                name="unit">
                                                @foreach($units as $row)
                                                    <option value="{{ $row->unit }}"
                                                            @if($row->unit == 'rial' || $row->unit == 'rls') selected @endif>{{ $row->unit }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-5 order-first order-md-12">
                                        <div class="text-karmozd d-flex  ">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>
                                            <p class="p-4 txt-karmozd"> برای ایجاد و یا تغییر بازه های کارمزدی ابتدا ارز
                                                مورد نظر را
                                                انتخاب کنید</p>


                                        </div>
                                    </div>
                                    <div class="col-md-3  d-flex btn-change-div order-last ">
                                        <div class="btn-baze-div d-flex pl-2">
                                            <button type="button" class="btn-baze add"> اضافه کردن
                                                <i class="fa fa-plus" style="font-size:13px"></i>
                                            </button>
                                        </div>
                                        <div class="pl-2 buttons-acc">
                                            <button class="btn-change1 ajaxStore">ثبت تغییرات</button>
                                        </div>

                                    </div>
                                </div>
                                <div class="dash1 "></div>
                            </div>

                            <div id="swap-fee-rows" class="col-12">
                                @if(count($result) > 0)
                                    @foreach($result as $key=>$item)
                                        @if($key != 0)
                                            <div class="dash3"></div>
                                        @endif
                                        <div class="row pl-5 risp-row2 row-btt">
                                            <div class="col-md-4  pr-2 pb-3 min-baze ">
                                                <div class="karmozd-traid2 d-flex ">
                                                    <p class="p-4">حداقل برداشت</p>
                                                    <input type="text" id="min-koinex1"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{ number_format(floor($item->extra_field1 *100)/100, 2) }}"
                                                           name="from[]"
                                                           class="font-weight-bold inputs pl-3 loan_max_amount">
                                                </div>
                                            </div>
                                            <div class="col-md-4  pb-3  pr-2 ">
                                                <div class="min-koinex d-flex">
                                                    <p class="mr-3"> حداکثر برداشت</p>
                                                    <input type="text" id="min-koinex2"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{ number_format(floor($item->extra_field2 *100)/100, 2)}}"
                                                           name="to[]"
                                                           class="font-weight-bold inputs pl-3">
                                                </div>
                                            </div>
                                            <div class="col-md-4 pr-2 karmozdcol">
                                                <div class="min-roitex d-flex  ">
                                                    <p class="mr-3 "> مقدار کارمزد</p>
                                                    <input type="text" id="min-rotix1"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{ is_numeric($item->extra_field3)?$item->extra_field3:'' }}"
                                                           name="percent[]"
                                                           class="font-weight-bold inputs pl-3 inputic loan_max_amount">
                                                    <input type="hidden" name="id[]" value="{{ $item->id }}">
                                                    <div class="foricon trash">
                                                        <p class="foricomtex">حذف بازه</p>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="30"
                                                             class="svgtrash"
                                                             height="22"
                                                             viewBox="0 0 35 28">
                                                            <path
                                                                d="M25.75,27h7v3.5h-7Zm0-14H38v3.5H25.75Zm0,7h10.5v3.5H25.75Zm-21,10.5A3.51,3.51,0,0,0,8.25,34h10.5a3.51,3.51,0,0,0,3.5-3.5V13H4.75ZM24,7.75H18.75L17,6H10L8.25,7.75H3v3.5H24Z"
                                                                transform="translate(-3 -6)" fill="#e62b2b"/>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="row pl-5 risp-row2 row-btt">
                                        <div class="col-md-4  pr-2 pb-3 min-baze ">
                                            <div class="karmozd-traid2 d-flex ">
                                                <p class="p-4">حداقل برداشت</p>
                                                <input type="text" id="min-koinex1"
                                                       onkeyup="($(this).val(numberFormat($(this).val())))"
                                                       name="from[]"
                                                       class="font-weight-bold inputs pl-3 loan_max_amount">
                                            </div>
                                        </div>
                                        <div class="col-md-4  pb-3  pr-2 ">
                                            <div class="min-koinex d-flex">
                                                <p class="mr-3"> حداکثر برداشت</p>
                                                <input type="text" id="min-koinex2"
                                                       onkeyup="($(this).val(numberFormat($(this).val())))"
                                                       name="to[]"
                                                       class="font-weight-bold inputs pl-3">
                                            </div>
                                        </div>
                                        <div class="col-md-4 pr-2 karmozdcol">
                                            <div class="min-roitex d-flex  ">
                                                <p class="mr-3 "> مقدار کارمزد</p>
                                                <input type="text" id="min-rotix1"
                                                       onkeyup="($(this).val(numberFormat($(this).val())))"
                                                       name="percent[]"
                                                       class="font-weight-bold inputs pl-3 inputic loan_max_amount">
                                                <input type="hidden" name="id[]">
                                                <div class="foricon trash">
                                                    <p class="foricomtex">حذف بازه</p>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" class="svgtrash"
                                                         height="22"
                                                         viewBox="0 0 35 28">
                                                        <path
                                                            d="M25.75,27h7v3.5h-7Zm0-14H38v3.5H25.75Zm0,7h10.5v3.5H25.75Zm-21,10.5A3.51,3.51,0,0,0,8.25,34h10.5a3.51,3.51,0,0,0,3.5-3.5V13H4.75ZM24,7.75H18.75L17,6H10L8.25,7.75H3v3.5H24Z"
                                                            transform="translate(-3 -6)" fill="#e62b2b"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-4 d-flex p-0">
                                <div class=" buttons-acc  ">
                                    <button class="btn-changedown ajaxStore">ثبت تغییرات</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="dash pt-3"></div><!-- مدیریت کارمزد سواپ ارزها## -->
                @endcan


            <!-- مدیریت کارمزد و حداقل برداشت ارزها -->
                @can('min_withdraw_fess')
                    <div class="row pt-3">
                        <form class="row col-12" action="{{route('admin.manage.fees.store')}}" method="post">
                            @csrf
                            <input type="hidden" name="name" value="fee_min_withdraw">
                            <div class=" col-md-4 texttop p-4 order-0">
                                <h5>مدیریت کارمزد و حداقل برداشت ارزها</h5>
                            </div>
                            <div class="col-md-4 order-1"></div>
                            <div class="col-md-4  d-flex btn-change-div order-md-2 order-last ">
                                <div class=" buttons-acc">
                                    <button class="btn-change ajaxStore">ثبت تغییرات</button>
                                </div>
                            </div>
                            <div class="col-md-3 mb-4 order-3">
                                <div class="karmozd-traid d-flex  ">
                                    <p class="p-4">نوع ارز</p>
                                    <select class="form-control custom-select-2 col-4 fontoptions unit_for_withdraw"
                                            name="unit">
                                        @foreach($units as $row)
                                            <option value="{{ $row->unit }}"
                                                    @if($row->unit == 'rial' || $row->unit == 'rls') selected @endif>{{ $row->unit }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row col-md-9 order-4 m-0 p-0 withdraw">
                                <div class="col-md-4 mb-4">
                                    <div class="min-koinex d-flex">
                                        <p class="mr-3">حداقل برداشت</p>
                                        <input type="text" id="min-koinex5"
                                               onkeyup="($(this).val(numberFormat($(this).val())))"
                                               value="{{ $min_withdraw }}"
                                               name="min_withdraw"
                                               class="font-weight-bold inputs pl-3">
                                    </div>
                                </div>
                                <div class="col-md-4 mb-4">
                                    <div class="min-roitex d-flex ">
                                        <p class="p-4"> مقدار کارمزد</p>
                                        <input type="text" id="min-rotix4"
                                               onkeyup="($(this).val(numberFormat($(this).val())))"
                                               value="{{  $fee_withdraw }}"
                                               name="fee_withdraw"
                                               class="font-weight-bold inputs pl-3">

                                    </div>
                                </div>
                                <!--                        <div class="col-md-4 mb-4">
                                                            <div class="min-roitex d-flex ">
                                                                <p class="p-4"> کارمزد برداشت کوینکس </p>
                                                                <input type="text" id="min-rotix5" value="20"
                                                                       class="font-weight-bold inputs pl-3">
                                                            </div>
                                                        </div>-->
                            </div>
                        </form>
                    </div>
                    <div class="dash pt-3"></div>
                @endcan
            <!-- مدیریت کارمزد و حداقل برداشت ارزها# -->


                <!-- کارمزد انتقال ارزها از کابران به کیف پول ادمین -->
                @can('transfer_currency_user_to_admin')
                <div class="row pt-3 ">
                    <form class="row col-12" action="{{route('admin.manage.fees.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="name" value="fee_withdraw_admin_network">
                        <div class=" col-md-5 texttop p-4 order-0">
                            <h5>مدیریت کارمزد انتقال ارزها از کابران به کیف پول ادمین</h5>
                        </div>
                        <div class="col-md-3 order-1">
                        </div>
                        <div class="col-md-4  d-flex btncol-change2 order-md-2 order-last ">
                            <div class="buttons-acc p-0">
                                <button class="btn-change2 ajaxStore">ثبت تغییرات</button>
                            </div>
                        </div>
                        <div class="col-md-3 mb-4 order-3">
                            <div class="karmozd-traid d-flex ">
                                <p class="p-4">TRC10 </p>
                                <input type="text" id="min-koinex11" name="fee_withdraw_admin_trc10"
                                       value="{{ (!empty($fee_withdraw_admin))?$fee_withdraw_admin->trc10:'' }}"
                                       class="w-100 font-weight-bold inputs pl-3">
                            </div>
                        </div>
                        <div class="col-md-3 mb-4 order-4 ">
                            <div class="min-koinex d-flex">
                                <p class="p-4"> TRC20 </p>
                                <input type="text" id="min-koinex12" name="fee_withdraw_admin_trc20"
                                       value="{{ (!empty($fee_withdraw_admin))?$fee_withdraw_admin->trc20:'' }}"
                                       class="w-100 font-weight-bold inputs pl-3">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="dash pt-3"></div> <!-- #کارمزد انتقال ارزها از کابران به کیف پول ادمین -->
                @endcan

                <!-- ترید -->
                @can('trade_fee')
                    <div class="texttop p-4">
                        <h5>مدیریت کارمزد ترید</h5>
                    </div>
                    <div class="row">
                        <form class="row col-12" action="{{route('admin.manage.fees.store')}}" method="post">
                            @csrf
                            <input type="hidden" name="name" value="trade">
                            <div class="col-md-4 pr-4 pl-0 pb-4">
                                <div class="karmozd-traid1 d-flex ">
                                    <p class="p-4">کارمزد ترید روتیکس</p>
                                    <input type="text" id="karmozd-traid-rotix" value="{{$trad_fee->extra_field1}}"
                                           name="trad_fee" class="font-weight-bold inputs pl-3">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="text-karmozd d-flex pr-3 ">
                                <span class="icon-Group-1745 font-size__icon"><span class="path1"></span><span
                                        class="path2"></span></span>
                                    <p class="p-4 txt-karmozd">پیام مورد نظر</p>
                                </div>
                            </div>
                            <div class="col-md-3  d-flex btn-change-div ">
                                <div class="buttons-acc">
                                    <button class="btn-change2 ajaxStore">ثبت تغییرات</button>
                                </div>
                            </div>
                        </form>
                    </div> <!-- #ترید -->
                @endcan

            </div>

        </div>

    </section>
@endsection

@section('script')
    <script src="{{ asset('theme/user/scripts/swap-selected-hasChid.js') }}"></script>
    <script>
        $(document).ready(function () {
            // سلکت باکس ها
            $('.custom-select-2').customSelect2();
            $(".swap-fee-select ul li").click(function () {
                changeUnit($(this).data('value'));
            });
            $(".unit_for_withdraw ul li").click(function () {
                changeUnitForWithdraw($(this).data('value'));
            });
            // ## سلکت باکس ها


            // اضافه کردن سطر جدید
            $(document).on('click', '.add', function () {
                var form = $('.row-btt:last-child').clone();
                $(form).find("input").val("");
                $("#swap-fee-rows").append(form);
            });

            // حذف یکی از سطرها
            $("body").delegate('.trash', 'click', function () {
                if ($('.risp-row2').length > 1)
                    $(this).closest('.risp-row2').remove();
                else
                    alert('حداقل یک ردیف باید وجود داشته باشد.')
            })


            $("input").keyup();
        });


        function changeUnit(unit) {
            $.ajax({
                url: httpToHttps('{{route('change-unit')}}'),
                type: 'post',
                data: {unit: unit, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    $("#swap-fee-rows").html(data)
                    $("input").keyup()
                }
            });
        }

        function changeUnitForWithdraw(unit) {
            $.ajax({
                url: httpToHttps('{{route('change-unit-for-withdraw')}}'),
                type: 'post',
                data: {unit: unit, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    $(".withdraw").html(data)
                    $("input").keyup()
                }
            });
        }

    </script>
@endsection
