@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کارمزد و برداشت ترید-پنل مدیریت روتیکس
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-titletransaction-subtitle desktop-titlemb-4">
                مدیریت کارمزد و برداشت ترید
            </h1>
            <div class="dashboard-table pb-5 ">
                <!-- کارمزد ترید -->
                <form action="{{route('trad-fee-update')}}" method="post">
                    @csrf
                    <input type="hidden" id="min" name="fee_type" value="withdraw_least_amount">
                    <div class="texttop p-4">
                        <h5>مدیریت کارمزد ترید</h5>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-4 pl-0 pb-4">
                            <div class="karmozd-traid1 d-flex ">
                                <p class="p-4">کارمزد ترید روتیکس</p>
                                <input type="text" id="karmozd-traid-rotix" name="trad_fee"
                                       value="{{$trad_fee->extra_field1}}"
                                       class="font-weight-bold inputs pl-3">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="text-karmozd d-flex pr-3 ">
                            <span class="icon-Group-1745 font-size__icon">
                                <span class="path1"></span>
                                <span class="path2"></span>
                            </span>
                                <p class="p-4 txt-karmozd">
                                    برای ایجاد و یا تغییر بازه های کارمزدی ابتدا ارز مورد نظر را انتخاب کنید
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3  d-flex btn-change-div ">
                            <div class="pl-2">
                                <button type="button" class="btn-change-traid ajaxStore">ثبت تغییرات</button>
                            </div>
                        </div>
                    </div>
                    <div class="dash pt-3"></div>
                </form><!-- کارمزد ترید ##-->

                <!-- مدیریت حداقل برداشت ارزها -->
                <form action="{{route('trad-fees-update')}}" method="post">
                    @csrf
                    <input type="hidden" id="min" name="fee_type" value="withdraw_least_amount">
                    <div class="row  pt-3">
                        <div class=" col-md-4 texttop p-4">
                            <h5>مدیریت حداقل برداشت ارزها</h5>
                        </div>
                        <div class="col-md-4 "></div>
                        <div class="col-md-4  d-flex btn-change-div ">
                            <div class="pl-2">
                                <button type="button" class="btn-change-traid ajaxStore">ثبت تغییرات</button>
                            </div>
                        </div>
                    </div>
                    <div class="row  p-3 ">
                        <div class="col-md-4 mb-4">
                            <div class="karmozd-traid d-flex  ">
                                <p class="p-4">نوع ارز</p>
                                <div class="volume-form-3">
                                    <button type="button" class="volume-form__selected-3">
                                        <div class="volume-form__selected-desc-container-3">
                                            <div class="volume-form__selected-desc-3">
                                                <div
                                                    class="volume-form__selected-title-container-3 d-flex items-center">
                                                    <span class="volume-form__selected-title-3">{{ ucfirst(array_key_first($currencies)) }}</span>
                                                </div>
                                                <div class="arr">
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="13"
                                                        height="11"
                                                        viewBox="0 0 18 11.115">
                                                        <path
                                                            id="Icon_material-expand-more"
                                                            data-name="Icon material-expand-more"
                                                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                            transform="translate(-9 -12.885)"
                                                        />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <select class="d-none" id="min_currency_name" name="unit"
                                                autocomplete="off"></select>
                                        <ul class="volume-form__menu-3 first-volume-form__menu-3"
                                            id="min_currency_name_ul">
                                            @foreach($currencies as $key =>$value)
                                                <li class="volume-form__item-3" data-val="{{ $key }}">
                                                    <a href="#" class="volume-form__link-3">
                                                        <span class="volume-form__selected-title-3">{{ ucfirst($key) }}</span>
                                                        <svg
                                                            xmlns="http://www.w3.org/2000/svg"
                                                            width="0"
                                                            height="0"
                                                            viewBox="0 0 18 11.115"
                                                        >
                                                            <path
                                                                id="Icon_material-expand-more"
                                                                data-name="Icon material-expand-more"
                                                                d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                                transform="translate(-9 -12.885)"
                                                            />
                                                        </svg>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <div class="min-koinex d-flex">
                                <p class="p-4">حداقل برداشت کوینکس</p>
                                <input type="text" id="min_withdraw" name="min_withdraw"
                                       value="{{ $first['coinex']->withdraw_least_amount }}"
                                       class="font-weight-bold inputs pl-3" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4 ">
                            <div class="min-roitex d-flex ">
                                <p class="p-4">حداقل برداشت روتیکس</p>
                                <input type="text" name="min_withdraw_rootix" id="min_withdraw_rootix"
                                       onkeyup="($(this).val(numberFormat($(this).val())))"
                                       value="{{ $first['first_min_withdraw_rootix'] }}"
                                       class="font-weight-bold inputs pl-3">
                            </div>
                        </div>
                    </div>
                    <div class="dash pt-3 pb-4"></div><!-- مدیریت حداقل برداشت ارزها ##-->
                </form>

                <!-- مدیریت کارمزد برداشت ارزها -->
                <form action="{{route('trad-fees-update')}}" method="post">
                    @csrf
                    <input type="hidden" id="fee_type" name="fee_type" value="withdraw_tx_fee">
                    <div class="row  pt-3">
                        <div class=" col-md-4 texttop p-4">
                            <h5>مدیریت کارمزد برداشت ارزها</h5>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4  d-flex btn-change-div ">
                            <div class="pl-2">
                                <button type="button" class="btn-change-traid ajaxStore">ثبت تغییرات</button>
                            </div>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-md-4 mb-4  ">
                            <div class="karmozd-traid d-flex ">
                                <p class="p-4">نوع ارز</p>
                                <div class="volume-form-3 volume-form-4">
                                    <button
                                        type="button"
                                        class="volume-form__selected-3 volume-form__selected-4">
                                        <div
                                            class="volume-form__selected-desc-container-3 volume-form__selected-desc-container-4">
                                            <div
                                                class="volume-form__selected-desc-3 volume-form__selected-desc-4">
                                                <div
                                                    class="volume-form__selected-title-container-3 volume-form__selected-title-container-4 d-flex items-center">
                                                    <span class="volume-form__selected-title-3 volume-form__selected-title-4">{{ ucfirst(array_key_first($currencies)) }}</span>
                                                </div>
                                                <div class="arr">
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="13"
                                                        height="11"
                                                        viewBox="0 0 18 11.115"
                                                    >
                                                        <path
                                                            id="Icon_material-expand-more"
                                                            data-name="Icon material-expand-more"
                                                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                            transform="translate(-9 -12.885)"
                                                        />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <select class="d-none" id="fee_currency_name" name="unit"
                                                autocomplete="off"></select>
                                        <ul class="volume-form__menu-3 volume-form__menu-4 second-volume-form__menu-3" id="fee_currency_name_ul">
                                            @foreach($currencies as $key =>$value)
                                            <li class="volume-form__item-3 volume-form__item-4" data-val="{{ $key }}">
                                                <a
                                                    href="#"
                                                    class="volume-form__link-3 volume-form__link-4">
                                                    <span class="volume-form__selected-title-3 volume-form__selected-title-4">{{ $key }}</span>
                                                    <svg
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="0"
                                                        height="0"
                                                        viewBox="0 0 18 11.115"
                                                    >
                                                        <path
                                                            id="Icon_material-expand-more"
                                                            data-name="Icon material-expand-more"
                                                            d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                                            transform="translate(-9 -12.885)"
                                                        />
                                                    </svg>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4  ">
                            <div class="min-koinex d-flex">
                                <p class="p-4">کارمزد برداشت کوینکس</p>
                                <input type="text" name="fee_withdraw" id="fee_withdraw"
                                       value="{{$first['coinex']->withdraw_tx_fee}}"
                                       class="font-weight-bold inputs pl-3" readonly>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4  ">
                            <div class="min-roitex d-flex ">
                                <p class="p-4 ">کارمزد برداشت روتیکس</p>
                                <input type="text" name="fee_withdraw_rootix" id="fee_withdraw_rootix"
                                       class="font-weight-bold inputs pl-3"
                                       value="{{$first['first_fee_withdraw_rootix']}}"
                                       onkeyup="($(this).val(numberFormat($(this).val())))">
                            </div>
                        </div>
                    </div>
                </form><!-- مدیریت کارمزد برداشت ارزها ##-->
            </div>
        </div>
    </section>
@endsection


<!-- Button trigger modal -->
@section('script')
    <script src="{{ asset('theme/admin/js/selected-swap.js') }}"></script>
    <script src="{{ asset('theme/admin/js/swap-selected-hasChid.js') }}"></script>
    <script>
        $("#min_currency_name_ul li").click(function () {
            let unit= $(this).data('val');
            $("#min_currency_name").html(`<option value="${unit}" selected></option>`).change();
        });

        $("#fee_currency_name_ul li").click(function () {
            let unit= $(this).data('val');
            $("#fee_currency_name").html(`<option value="${unit}" selected></option>`).change();
        })


        var after_change_func= function (response, params) {
            if (response.status == 100 || response.status == 200) {
                $(`${params.coinex_id}`).val(numberFormat(response.coinex)).removeAttr('placeholder','در حال بارگذاری ...');
                $(`${params.rootix_id}`).val(numberFormat(response.rootix)).removeAttr('placeholder','در حال بارگذاری ...');
            }else {
                $(`${params.rootix_id}, ${params.coinex_id}`).removeAttr('placeholder','در حال بارگذاری ...');
            }
        }

        $('#min_currency_name').on('change', function () {
            let name = $(this).val();
            let fee_type = $('#min').val();
            $('#min_withdraw,#min_withdraw_rootix').val('').attr('placeholder','در حال بارگذاری ...');
            let form= {
                isObject: true,
                action: "{{route('get-currency-fees')}}",
                method: "get",
                data: {
                    name: name,
                    fee_type: fee_type,
                }
            }
            ajax(form,after_change_func,{'rootix_id': '#min_withdraw_rootix','coinex_id':'#min_withdraw'})
        });

        $('#fee_currency_name').on('change', function () {
            let name = $(this).val();
            let fee_type = $('#fee_type').val();
            $('#fee_withdraw,#fee_withdraw_rootix').val('').attr('placeholder','در حال بارگذاری ...');
            let form= {
                isObject: true,
                action: "{{route('get-currency-fees')}}",
                method: "get",
                data: {
                    name: name,
                    fee_type: fee_type,
                }
            }
            ajax(form,after_change_func,{'rootix_id': '#fee_withdraw_rootix','coinex_id':'#fee_withdraw'})

        });
    </script>
@endsection
