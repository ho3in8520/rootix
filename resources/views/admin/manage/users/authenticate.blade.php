@extends('templates.admin.master_page')
@section('title_browser')
    اطلاعات کاربران(احراز حویت)-پنل مدیریت روتیکس
@endsection
@php
    $array_steps=status_steps_auth_user($step_forms,$step_completed, 'all','array');
    $array_steps_notif=status_steps_auth_user($step_forms,$step_completed, 'all','notif');
@endphp
@section('content')
    <section class="dashboard-cart user-authentication">
        <div class="container">
            <h1 class="dashboard-title desktop-title">اطلاعات کاربران-احراز حویت</h1>
            <div class="dashboard-table transaction-table-container">
                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله یک</span>
                                <span class="d-block transaction__filter-title">اطلاعات تماس</span>
                            </div>
                            <div>
                                <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شماره تماس
                                    <span class="text">{{$user->mobile?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">ایمیل
                                    <span class="text">{{$user->email?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4 mt-5">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله دو</span>
                                <span class="d-block transaction__filter-title">دریافت مدارک</span>
                            </div>
                            <div>
                                @if($array_steps['step.upload']['status']==3)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$array_steps['step.upload']['msg_reject']}}">
                                @elseif($array_steps['step.upload']['status']==2)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}"
                                         alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-center mb-4">
                    <div class="reject-confirm">
                        @if($array_steps['step.upload']['status']==1 ||$array_steps['step.upload']['status']==0)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                مرحله دو
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.upload']['status']==3)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                مرحله دو
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.upload']['status']==2)
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد مرحله
                                دو
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-4">
                    @if(count($images)>0)
                        @foreach($images as $key =>$item)
                            @php
                                switch ($item->type){
                                        case "national":
                                            $title="تصویر کارت ملی / آمایش";
                                            break;
                                            case "selfie_image":
                                                $title="تصویر سلفی";
                                                break;
    }
                            @endphp
                            <div class="col-sm-6 col-md-6 col-lg-3">
                                <div class="card-rectangle card mt-3">
                                    <div class="card-body">
                                        <p class="text">{{$title}}</p>
                                        <span class="card-text">{{jdate_from_gregorian($item->created_at,'%Y.%m.%d')}}</span>
                                        <div class="dotted mt-2">
                                            <img
                                                src="{{ getImage($item->path) }}"
                                                alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="row justify-center">
                    <div class="d-flex header-transaction mb-4 mt-5">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله سه</span>
                                <span class="d-block transaction__filter-title">اطلاعات کاربر</span>
                            </div>
                            <div>
                                @if($array_steps['step.information']['status']==3)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top"
                                         title="{{$array_steps['step.information']['msg_reject']}}">
                                @elseif($array_steps['step.information']['status']==2)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}"
                                         alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-center mb-4">
                    <div class="reject-confirm">
                        @if($array_steps['step.information']['status']==1 || $array_steps['step.information']['status']==0)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                مرحله سه
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.information']['status']==3)
                            <a class="confirmation"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                مرحله سه
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.information']['status']==2)
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد
                                مرحله سه
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">نام و نام خانوادگی
                                    <span class="text">{{$user->first_name?:''}} {{$user->last_name?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">کدملی
                                    <span class="text">{{$user->national_code?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">تاریخ تولد
                                    <span class="text">{{$user->birth_day?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">تلفن ثابت (با کد شهر)
                                    <span class="text">{{$user->phone?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">کدپستی
                                    <span class="text">{{$user->postal_code?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شهر
                                    <span
                                        class="text">{{!empty($user->city_name->name) ? $user->city_name->name : ' '}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">آدرس
                                    <span class="text">{{$user->address?:''}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">ملیت</p>
                                <div style="float: left">
                                    <div class="form-check form-check-inline">
                                        <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                            ایرانی
                                            <input type="radio"
                                                {{ $user->nationality==0?'checked':'disabled'}}/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                            غیرایرانی
                                            <input type="radio"
                                                {{ $user->nationality==1?'checked':'disabled'}}/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">جنسیت</p>
                                <div style="float: left">
                                    <div class="form-check form-check-inline">
                                        <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                            آقا
                                            <input type="radio"
                                                {{ $user->gender_id==1?'checked':'disabled'}}/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                            خانم
                                            <input type="radio"
                                                {{ $user->gender_id==2?'checked':'disabled'}}/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="radio-container d-flex ml-4 mb-0 pl-4">
                                            هیچکدام
                                            <input type="radio"
                                                {{ $user->gender_id==3?'checked':'disabled'}}/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row justify-center"
                >
                    <div class="d-flex header-transaction mb-4 mt-5">
                        <div class="selected-transaction d-flex">
                            <div class="w-100 bg-white">
                                <span class="d-block transaction__filter-title">مرحله چهار</span>
                                <span class="d-block transaction__filter-title">اطلاعات بانکی</span>
                            </div>
                            <div>
                                @if($array_steps['step.bank']['status']==3)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1786.svg')}}"
                                         alt="" data-toggle="tooltip"
                                         data-placement="top" title="{{$array_steps['step.bank']['msg_reject']}}">
                                @elseif($array_steps['step.bank']['status']==2)
                                    <img class="confirmations" src="{{asset('theme/user/images/Group 1776.svg')}}"
                                         alt="">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row justify-center mb-4">
                    <div class="reject-confirm">
                        @if($array_steps['step.bank']['status']==1 || $array_steps['step.bank']['status']==0)
                            <a href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.bank']['id'])}}"
                               class="confirmation">تایید
                                مرحله چهار
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد مرحله
                                چهار
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.bank']['status']==3)
                            <a href=" {{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.bank']['id'])}}"
                               class="confirmation">تایید
                                مرحله چهار
                                <img src="{{asset('theme/user/images/Path 8632.svg')}}" alt="" style="width: 11px;">
                            </a>
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد مرحله
                                چهار
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @elseif($array_steps['step.bank']['status']==2)
                            <a class="reject" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد مرحله
                                چهار
                                <img src="{{asset('theme/user/images/Icon ionic-ios-close.svg')}}" alt=""
                                     style="width: 8px;">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="row mb-5">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">بانک
                                    <span class="text">{{ (!empty($user->bank[0]))?$user->bank[0]->name:'' }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شماره کارت
                                    <span
                                        class="text">{{ (!empty($user->bank[0]))?$user->bank[0]->card_number:'' }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شماره حساب
                                    <span
                                        class="text">{{ (!empty($user->bank[0]))?$user->bank[0]->account_number:'' }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="card mt-2">
                            <div class="cards-body">
                                <p class="card-text">شماره شبا
                                    <span
                                        class="text">{{ (!empty($user->bank[0]))?$user->bank[0]->sheba_number:'' }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        function rejectUser(status, form_id) {

            swal("دلیل رد صلاحیت خود را بنویسید:", {
                content: "input",
            })
                .then((value) => {
                    if (value !== null) {
                        $.ajax({
                            url: httpToHttps("{{url('admin/user/disableStatus')}}"),
                            type: 'get',
                            data: {
                                status: status,
                                id: "{{$user->id}}",
                                value: value,
                                form_id: form_id,
                                "_token": "{{csrf_token()}}"
                            },
                            headers:
                                {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                },
                            success: function (data) {
                                swal('موفق', data['msg'], data['type']);
                                setTimeout(function () {
                                    location.reload();
                                }, 2000)
                            }
                        });
                    }
                });
        }
    </script>
@endsection
