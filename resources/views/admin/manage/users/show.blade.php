@extends('templates.admin.master_page')
@section('title_browser')
    اطلاعات کاربر-پنل مدیریت روتیکس
@endsection
@section('content')
    <section class="pt-2">
        <div class="container">
            <h1 class="dashboard-title bank-title desktop-title">
                اطلاعات کاربر
            </h1>

            <div class="user-information mt-4">
                <ul class="useritems">
                    @can('user_info')
                        <li>
                            <button
                                class="tab__btn {{ (!empty(session()->getOldInput('tab')) && session()->getOldInput('tab')=='information')|| empty(session()->getOldInput('tab'))?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="25.5"
                                    height="25.5"
                                    viewBox="0 0 25.5 25.5"
                                >
                                    <path
                                        id="Icon_material-person"
                                        data-name="Icon material-person"
                                        d="M18,18a6,6,0,1,0-6-6A6,6,0,0,0,18,18Zm0,3c-4.005,0-12,2.01-12,6v3H30V27C30,23.01,22.005,21,18,21Z"
                                        transform="translate(-5.25 -5.25)"
                                        fill="none"
                                        stroke="#606060"
                                        stroke-width="1.5"
                                    />
                                </svg>
                                مشخصات کاربر
                            </button>
                        </li>
                    @endcan

                    @can('user_documents')
                        <li>
                            <button class="tab__btn {{ session()->getOldInput('tab')=='documents'?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24.523"
                                    height="25.582"
                                    viewBox="0 0 24.523 25.582"
                                >
                                    <path
                                        id="Icon_feather-paperclip"
                                        data-name="Icon feather-paperclip"
                                        d="M25.715,13.371,14.977,24.109a7.014,7.014,0,1,1-9.92-9.92L15.795,3.451a4.676,4.676,0,0,1,6.613,6.613L11.659,20.8A2.338,2.338,0,0,1,8.352,17.5l9.92-9.908"
                                        transform="translate(-2.252 -1.332)"
                                        fill="none"
                                        stroke="#606060"
                                        stroke-linecap="round"
                                        stroke-linejoin="round"
                                        stroke-width="1.5"
                                    />
                                </svg>
                                مدارک
                            </button>
                        </li>
                    @endcan
                    @can('user_roles')
                        <li>
                            <button class="tab__btn {{ session()->getOldInput('tab')=='roles'?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15.184"
                                    height="24.082"
                                    viewBox="0 0 15.184 24.082"
                                >
                                    <path
                                        id="Icon_simple-openaccess"
                                        data-name="Icon simple-openaccess"
                                        d="M21.237,13.532a7.585,7.585,0,0,0-.542-1.046V6.437A6.44,6.44,0,0,0,14.258,0h0A6.437,6.437,0,0,0,7.818,6.437v.88H10.4v-.88a3.86,3.86,0,0,1,7.72,0v3.52a7.589,7.589,0,1,0,3.12,3.577Zm-7,7.969a5.014,5.014,0,1,1,5.014-5.014A5.02,5.02,0,0,1,14.242,21.5Zm2.152-4.991a2.127,2.127,0,1,1-2.127-2.127,2.127,2.127,0,0,1,2.127,2.127Z"
                                        transform="translate(-6.65 0)"
                                        fill="#606060"
                                    />
                                </svg>
                                نقش ها
                            </button>
                        </li>
                    @endcan
                    @can('user_setting')
                        <li>
                            <button class="tab__btn {{ session()->getOldInput('tab')=='setting'?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22.956"
                                    height="22.956"
                                    viewBox="0 0 22.956 22.956"
                                >
                                    <g
                                        id="Icon_feather-settings"
                                        data-name="Icon feather-settings"
                                        transform="translate(-0.75 -0.75)"
                                    >
                                        <path
                                            id="Path_8621"
                                            data-name="Path 8621"
                                            d="M19.352,16.426A2.926,2.926,0,1,1,16.426,13.5,2.926,2.926,0,0,1,19.352,16.426Z"
                                            transform="translate(-4.198 -4.198)"
                                            fill="none"
                                            stroke="#606060"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="1.5"
                                        />
                                        <path
                                            id="Path_8622"
                                            data-name="Path 8622"
                                            d="M19.445,15.154a1.609,1.609,0,0,0,.322,1.775l.059.059a1.952,1.952,0,1,1-2.76,2.76l-.059-.059a1.622,1.622,0,0,0-2.75,1.151v.166a1.951,1.951,0,1,1-3.9,0v-.088A1.609,1.609,0,0,0,9.3,19.445a1.609,1.609,0,0,0-1.775.322l-.059.059a1.952,1.952,0,1,1-2.76-2.76l.059-.059a1.622,1.622,0,0,0-1.151-2.75H3.451a1.951,1.951,0,1,1,0-3.9h.088A1.609,1.609,0,0,0,5.011,9.3a1.609,1.609,0,0,0-.322-1.775l-.059-.059a1.952,1.952,0,1,1,2.76-2.76l.059.059a1.609,1.609,0,0,0,1.775.322H9.3a1.609,1.609,0,0,0,.975-1.473V3.451a1.951,1.951,0,1,1,3.9,0v.088a1.622,1.622,0,0,0,2.75,1.151l.059-.059a1.952,1.952,0,1,1,2.76,2.76l-.059.059a1.609,1.609,0,0,0-.322,1.775V9.3a1.609,1.609,0,0,0,1.473.975h.166a1.951,1.951,0,1,1,0,3.9h-.088a1.609,1.609,0,0,0-1.473.975Z"
                                            fill="none"
                                            stroke="#606060"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            stroke-width="1.5"
                                        />
                                    </g>
                                </svg>
                                تنظیمات
                            </button>
                        </li>
                    @endcan
                    @can('user_wallet')
                        <li>
                            <button class="tab__btn {{ session()->getOldInput('tab')=='wallet'?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24.243"
                                    height="22.467"
                                    viewBox="0 0 24.243 22.467"
                                >
                                    <g
                                        id="Icon_ionic-ios-wallet"
                                        data-name="Icon ionic-ios-wallet"
                                        transform="translate(-2.875 -3.988)"
                                    >
                                        <path
                                            id="Path_8623"
                                            data-name="Path 8623"
                                            d="M23.266,11.25H6.727A3.355,3.355,0,0,0,3.375,14.6v9.387a3.355,3.355,0,0,0,3.352,3.352H23.266a3.355,3.355,0,0,0,3.352-3.352V14.6A3.355,3.355,0,0,0,23.266,11.25Z"
                                            transform="translate(0 -1.386)"
                                            fill="none"
                                            stroke="#606060"
                                            stroke-width="1"
                                        />
                                        <path
                                            id="Path_8624"
                                            data-name="Path 8624"
                                            d="M20.083,4.556,6.958,7.12C5.953,7.344,4.5,8.355,4.5,9.584A3.52,3.52,0,0,1,7.238,8.523H23.274V7.377a3.208,3.208,0,0,0-.777-2.1h0A2.642,2.642,0,0,0,20.083,4.556Z"
                                            transform="translate(-0.231)"
                                            fill="none"
                                            stroke="#606060"
                                            stroke-width="1"
                                        />
                                    </g>
                                </svg>
                                کیف پول
                            </button>
                        </li>
                    @endcan
                    @can('user_reports')
                        <li>
                            <button class="tab__btn {{ session()->getOldInput('tab')=='reports'?'active':'' }}">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="22.456"
                                    height="22.456"
                                    viewBox="0 0 22.456 22.456"
                                >
                                    <path
                                        id="Icon_ionic-ios-paper"
                                        data-name="Icon ionic-ios-paper"
                                        d="M7.295,5.025V21.633a.724.724,0,0,1-.722.722h0a.724.724,0,0,1-.722-.722V6.676H5.025a1.649,1.649,0,0,0-1.65,1.65V23.181a1.649,1.649,0,0,0,1.65,1.65H23.237a1.6,1.6,0,0,0,1.594-1.594V5.025a1.649,1.649,0,0,0-1.65-1.65l-14.338.1A1.478,1.478,0,0,0,7.295,5.025Zm3.4,1.65h4.332a.724.724,0,0,1,.722.722h0a.724.724,0,0,1-.722.722H10.7A.724.724,0,0,1,9.977,7.4h0A.724.724,0,0,1,10.7,6.676Zm0,8.252h7.633a.724.724,0,0,1,.722.722h0a.724.724,0,0,1-.722.722H10.7a.724.724,0,0,1-.722-.722h0A.724.724,0,0,1,10.7,14.928ZM20.808,20.5H10.7a.724.724,0,0,1-.722-.722h0a.724.724,0,0,1,.722-.722H20.808a.724.724,0,0,1,.722.722h0A.724.724,0,0,1,20.808,20.5Zm0-8.252H10.7a.724.724,0,0,1-.722-.722h0A.724.724,0,0,1,10.7,10.8H20.808a.724.724,0,0,1,.722.722h0A.724.724,0,0,1,20.808,12.246Z"
                                        transform="translate(-2.875 -2.875)"
                                        fill="none"
                                        stroke="#606060"
                                        stroke-width="1"
                                    />
                                </svg>
                                گزارشات
                            </button>
                        </li>
                    @endcan
                </ul>
            </div>
        </div>
    </section>

    <section class="wallet-cart admin-1">
        <div class="container">
            <div class="panel-box" style="min-height: 61vh;height: auto">
                <div class="admin-dropdown">
                    <select class="form-control custom-select-2" name="currency1">
                        @can('user_info')
                            <option data-image="{{ asset('theme/user/images/panel_1.svg') }}" value="information"
                                    selected>
                                مشخصات کاربر
                            </option>
                        @endcan
                        @can('user_documents')
                            <option data-image="{{ asset('theme/user/images/panel_2.svg') }}" value="documents">
                                مدارک
                            </option>
                        @endcan
                        @can('user_roles')
                            <option data-image="{{ asset('theme/user/images/panel_3.svg') }}" value="roles">
                                نقش ها
                            </option>
                        @endcan
                        @can('user_setting')
                            <option data-image="{{ asset('theme/user/images/panel_4.svg') }}" value="setting">
                                تنظیمات
                            </option>
                        @endcan
                        @can('user_wallet')
                            <option data-image="{{ asset('theme/user/images/panel_5.svg') }}" value="wallet">
                                کیف پول
                            </option>
                        @endcan
                        @can('user_reports')
                            <option data-image="{{ asset('theme/user/images/panel_6.svg') }}" value="reports">
                                گزارشات
                            </option>
                        @endcan
                    </select>
                </div>
                @can('user_info')
                    <div
                        class="tab__content {{ (!empty(session()->getOldInput('tab')) && session()->getOldInput('tab')=='information')|| empty(session()->getOldInput('tab'))?'active':'' }}"
                        data-type="information">
                        <form action="{{route('admin.user.edit',$user->id)}}" method="post">
                            @csrf
                            <input type="hidden" name="tab" value="information">
                            <div class="inputs-box">
                                <div
                                    class="d-flex align-items-center justify-content-between mt-4 mb-2"
                                >
                                    <h3 class="admin-title">اطلاعات شخصی</h3>

                                    <div class="d-flex items-center">
                                        <button
                                            type="submit"
                                            class="create-new-bank withdraw-btn ml-2 record-all-change-btn d-none"
                                        >
                                            ثبت
                                        </button>

                                        @can('user_edit')
                                            <button
                                                type="button"
                                                class="create-new-bank withdraw-btn m-0 edit-btn">
                                                ویرایش
                                            </button>
                                        @endcan
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                            <div class="d-flex items-center mr-5 first-admin-box">
                                                <img
                                                    src="{{ $user->avatar }}"
                                                    alt="user profile"
                                                />
                                                <div class="mr-4">
                                                    <h5>سطح کاربر : {{ $result['user_level'] }}</h5>
                                                    <span
                                                        class="text-center d-block {{ $user->is_complete_steps?'text-success':'text-danger' }}">  وضعیت : {{ $user->is_complete_steps?'فعال':'غیر فعال' }}</span
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">نام
                                             @error('first_name')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-1"
                                                name="first_name"
                                                value="{{ !empty($user->first_name)?$user->first_name:'' }}"
                                                placeholder="نام"
                                                disabled/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">نام خانوادگی
                                                   @error('last_name')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-1"
                                                name="last_name"
                                                value="{{ !empty($user->last_name)?$user->last_name:'' }}"
                                                placeholder="نام"
                                                disabled/>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-3">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">موبایل
                                        @error('mobile')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-2"
                                                name="mobile"
                                                value="{{ !empty($user->mobile)?$user->mobile:'' }}"
                                                disabled
                                            />
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">تاریخ تولد
                                        @error('birth_day')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input datePicker"
                                                id="currency-value-input-3"
                                                placeholder="100"
                                                name="birth_day"
                                                value="{{ !empty($user->birth_day)?$user->birth_day:'' }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">کد ملی
                                        @error('national_code')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-4"
                                                placeholder="100"
                                                name="national_code"
                                                value="{{ !empty($user->national_code)?$user->national_code:'' }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">ایمیل
                                        @error('email')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="email"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-5"
                                                placeholder="100"
                                                name="email"
                                                value="{{ !empty($user->email)?$user->email:'' }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                          <span class="request-box__title">تلفن ثابت (با کد شهر)
                          @error('phone')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                          </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-6"
                                                name="phone"
                                                placeholder="100"
                                                value="{{ !empty($user->phone)?$user->phone:'' }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">وضعیت پنل
                                        @error('status')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>
                                            <div class="row">
                                                <select
                                                    class="form-control custom-select-2 col-12 disable-select"
                                                    name="status">
                                                    <option value="0">انتخاب کنید...</option>
                                                    <option
                                                        value="1" {{!empty($user->status) && $user->status==1?'selected':''}}>
                                                        فعال
                                                    </option>
                                                    <option
                                                        value="2" {{!empty($user->status) && $user->status==2?'selected':''}}>
                                                        غیرفعال
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center gener-btn"
                                        >
                                        <span class="request-box__title">جنسیت
                                        @error('gender_id')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <div class="d-flex items-center">
                                                <label
                                                    class="radio-container d-flex ml-4 mb-0 pl-4 radio-container__admin"
                                                >
                                                    آقا
                                                    <input type="radio" value="1"
                                                           name="gender_id" {{!empty($user->gender_id) && $user->gender_id==1?'checked':''}}/>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label
                                                    class="radio-container ml-4 mb-0 pl-4 radio-container__admin"
                                                >خانم
                                                    <input type="radio" value="2"
                                                           name="gender_id" {{!empty($user->gender_id) && $user->gender_id==2?'checked':''}}/>
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label
                                                    class="radio-container ml-4 mb-0 no-know radio-container__admin"
                                                >هیچکدام
                                                    <input type="radio" value="0"
                                                           name="gender_id" {{empty($user->gender_id) || $user->gender_id==0 ?'checked':''}}/>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="inputs-box">
                                <div
                                    class="d-flex align-items-center justify-content-between mt-4 mb-2"
                                >
                                    <h3 class="admin-title">آدرس</h3>

                                    <div class="d-flex items-center">
                                        <button
                                            type="submit"
                                            class="create-new-bank withdraw-btn ml-2 record-all-change-btn d-none"
                                        >
                                            ثبت
                                        </button>

                                        @can('user_edit')
                                            <button
                                                type="button"
                                                class="create-new-bank withdraw-btn m-0 edit-btn">
                                                ویرایش
                                            </button>
                                        @endcan
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">استان
                                        @error('states')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <div class="row">
                                                <select
                                                    class="form-control custom-select-2 col-12 disable-select state"
                                                    name="states"
                                                >
                                                    <option value="0">انتخاب کنید...</option>
                                                    @foreach($result['states'] as $state)
                                                        <option
                                                            value="{{ $state->id }}" {{ $state->id==$user->state?'selected':'' }}>{{ $state->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">شهر
                                        @error('city')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <div class="row">
                                                <select
                                                    class="form-control custom-select-2 col-12 disable-select city"
                                                    name="city"
                                                >
                                                    <option value="0">انتخاب کنید...</option>
                                                    @foreach($result['cities'] as $city)
                                                        <option
                                                            value="{{ $city->id }}" {{ $city->id==$user->city?'selected':'' }}>{{ $city->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">کد پستی
                                        @error('postal_code')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-7"
                                                name="postal_code"
                                                value="{{ $user->postal_code }}"
                                                placeholder="123456789"
                                                disabled
                                            />
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                        >
                                        <span class="request-box__title">آدرس
                                        @error('address')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                        </span>

                                            <input
                                                type="text"
                                                dir="ltr"
                                                class="currency-value-input"
                                                id="currency-value-input-8"
                                                placeholder="اصفهان نجف آباد"
                                                name="address"
                                                value="{{ $user->address }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endcan
                @can('user_documents')
                    <div class="tab__content {{ session()->getOldInput('tab')=='documents'?'active':'' }}"
                         data-type="documents">
                        <div class="row pt-5">
                            @if(count($result['documents'])>0)
                                @foreach($result['documents'] as $key =>$item)
                                    @php
                                        switch ($item->type){
                                                case "national":
                                                    $title="تصویر کارت ملی";
                                                    break;
                                                    case "phone_bill":
                                                        $title="تصویر قبض تلفن";
                                                        break;
            }
                                    @endphp
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div class="upload-card upload-card__admin" style="cursor: pointer">
                                            <div class="upload-card__header">
                                                <h3 class="upload-card__title">{{ $title }}</h3>
                                                <p class="upload-card__text">{{jdate_from_gregorian($item->created_at,'%Y/%m/%d')}}</p>
                                            </div>

                                            <div class="upload-card__body drop-area">
                                                <div class="upload__image button-modal-document">
                                                    <img
                                                        src="{{ getImage($item->path) }}"
                                                        alt="user image"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <h3 class="text-center">موردی برای نمایش وجود ندارد!</h3>
                            @endif
                        </div>
                    </div>
                @endcan
                @can('user_roles')
                    <div class="tab__content pt-5 {{ session()->getOldInput('tab')=='roles'?'active':'' }}"
                         data-type="roles">
                        <div class="panel-box1">
                            <div class="specifications">
                                <a href="#">
                                    <img src="{{ $user->avatar }}" alt="mask"/>
                                </a>
                                <div class="name">
                                    <h6>{{ $user->full_name }}</h6>
                                    <a href="#">{{ $user->email }}</a>
                                </div>
                            </div>
                            <hr/>
                            @if(count($result['current_role']) >0)
                                @foreach($result['current_role'] as $current_role)
                                    <div class="support" data-id={{ $current_role['id'] }}>
                                        <div>
                                            <h6 class="normal-user">{{ $current_role['name'] }}</h6>
                                            <p class="date">{{ jdate_from_gregorian($current_role['updated_at'],'Y/m/d') }}</p>
                                        </div>
                                        <div class="delete-person">
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                width="16.75"
                                                height="16.746"
                                                viewBox="0 0 16.75 16.746"
                                            >
                                                <path
                                                    id="Icon_ionic-ios-close"
                                                    data-name="Icon ionic-ios-close"
                                                    d="M21.645,19.661l5.982-5.982A1.4,1.4,0,1,0,25.645,11.7l-5.982,5.982L13.68,11.7A1.4,1.4,0,1,0,11.7,13.679l5.982,5.982L11.7,25.644a1.4,1.4,0,0,0,1.982,1.982l5.982-5.982,5.982,5.982a1.4,1.4,0,0,0,1.982-1.982Z"
                                                    transform="translate(-11.285 -11.289)"
                                                    fill="#f31111"
                                                />
                                            </svg>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="support empty-role">
                                    <p class="text-center">کاربر هیچ نقشی ندارد</p>
                                </div>
                                <hr>
                            @endif
                            <div id="person_list"></div>
                            @can('user_add_role')
                                <div class="add-a-role-link">
                                    <button type="button" class="add-a-role w-100">

                                        <input type="text" id="myInput" onkeyup="searchRole()"
                                               placeholder=" اضافه کردن نقش"
                                               title="Type in a name" autocomplete="off">

                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            class="Plus"
                                        >
                                            <g
                                                id="Icon_feather-plus"
                                                data-name="Icon feather-plus"
                                                transform="translate(-6 -6)"
                                            >
                                                <path
                                                    id="Path_8625"
                                                    data-name="Path 8625"
                                                    d="M18,7.5v21"
                                                    fill="none"
                                                    stroke="#000"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    stroke-width="3"
                                                />
                                                <path
                                                    id="Path_8626"
                                                    data-name="Path 8626"
                                                    d="M7.5,18h21"
                                                    fill="none"
                                                    stroke="#000"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"
                                                    stroke-width="3"
                                                />
                                            </g>
                                        </svg>
                                    </button>

                                    <ul class="new-persons" id="myUL">
                                        @foreach($result['roles'] as $role)
                                            <li>
                                                <button type="button"
                                                        data-id="{{ $role->id }}" {{ in_array($role->id,array_column($result['current_role'],'id'))?'disabled':'' }}>{{ $role->name }}</button>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endcan
                        </div>
                    </div>
                @endcan
                @can('user_setting')
                    <div class="tab__content {{ session()->getOldInput('tab')=='setting'?'active':'' }}"
                         data-type="setting">
                        <form action="{{ route('admin.user.edit',$user) }}" method="post">
                            @csrf
                            <input type="hidden" name="tab" value="setting">
                            @can('user_confirm_sms')
                                <div class="row">
                                    <span class="request-box__title px-2">روش تایید SMS</span>
                                    <label class="switch">
                                        <input id="switch" class="confirm-type" name="confirm_type" value="1"
                                               type="checkbox" {{ $user->confirm_type == 'sms'?'checked':'' }} >
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            @endcan
                            <div class="pt-5">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div
                                            dir="rtl"
                                            class="request-box request-box-2 active auth__btn request-js-box-2 m-0 enabled d-flex justify-between items-center settings"
                                        >
                                            <span class="request-box__title">تغییر رمز عبور </span>
                                            <input type="password" name="password" autocomplete="current-password"
                                                   id="id_password">
                                            <i class="far fa-eye" id="togglePassword" style=" cursor: pointer;"></i>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6 col-lg-8">
                                        <div class="d-flex items-center mt-5 mt-md-4">
                          <span class="icon-Group-1745 font-size__icon"
                          ><span class="path1"></span
                              ><span class="path2"></span
                              ></span>

                                            <p class="sell-request-p">
                                                در صورت عدم تغییر رمز عبور این فیلد را خالی بگذارید.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <button class="btn signin-btn record-btn mx-auto mt-5">
                                    ثبت
                                </button>
                            </div>
                        </form>
                    </div>
                @endcan
                @can('user_wallet')
                    <div class="tab__content pt-5 {{ session()->getOldInput('tab')=='wallet'?'active':'' }}"
                         data-type="wallet">
                        <form action="{{route('admin.users.wallet',$user)}}" method="post">
                            @csrf
                            <input type="hidden" name="tab" value="wallet">
                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <span style="position: absolute; bottom: 97px" id="inventory">موجودی : -</span>
                                    <div
                                        dir="rtl"
                                        class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                    >
                                <span class="request-box__title"> ارز
                                    @error('unit')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                </span>
                                        <div class="row">
                                            <select
                                                class="form-control custom-select-2 col-12 assets assets-select"
                                                name="unit"
                                            >
                                                <option value="" selected>انتخاب کنید...</option>
                                                @foreach($result['assets'] as $item)
                                                    <option value="{{ $item->unit }}"
                                                            data-amount="{{ $item->amount }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4">
                                    <div
                                        dir="rtl"
                                        class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                    >
                                <span class="request-box__title">مقدار
                                  @error('amount')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                </span>

                                        <input
                                            type="text"
                                            dir="ltr"
                                            name="amount"
                                            class="currency-value-input"
                                            id="currency-value-input-10"
                                            placeholder="100"
                                        />
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4">
                                    <div
                                        dir="rtl"
                                        class="request-box request-box-2 auth__btn request-js-box-2 enabled d-flex justify-between items-center"
                                    >
                                <span class="request-box__title">نوع تراکنش
                                      @error('transact_type')
                                    <p class="error text-danger">{{ $message }}</p>
                                    @enderror
                                </span>

                                        <div class="row">
                                            <select
                                                class="form-control custom-select-2 col-12" name="transact_type">
                                                <option value="" selected>انتخاب کنید...</option>
                                                <option value="increase">افزایش موجودی</option>
                                                <option value="decrease">کاهش موجودی</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn signin-btn record-btn mt-5 mx-auto">
                                ثبت
                            </button>
                        </form>
                    </div>
                @endcan
                @can('user_reports')
                    <div class="tab__content {{ session()->getOldInput('tab')=='reports'?'active':'' }}"
                         data-type="reports">
                        <x-report-list :user="$user"></x-report-list>
                    </div>
                @endcan
            </div>
        </div>
    </section>
    <div class="modal fade modal-document" id="modal1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="d-flex justify-content-end mb-2">
                    <div class="closed__modal" data-dismiss="modal">
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="15.356"
                            height="15.353"
                            viewBox="0 0 12.356 12.353"
                        >
                            <path
                                d="M18.927,17.465l4.413-4.413a1.034,1.034,0,1,0-1.462-1.462L17.465,16,13.052,11.59a1.034,1.034,0,1,0-1.462,1.462L16,17.465l-4.413,4.413a1.034,1.034,0,0,0,1.462,1.462l4.413-4.413,4.413,4.413a1.034,1.034,0,0,0,1.462-1.462Z"
                                transform="translate(-11.285 -11.289)"
                                fill="#1c2426"
                            />
                        </svg>
                    </div>
                </div>
                <div class="modal-header mx-auto border-0">
                    <img
                        src=""
                        alt="user image"
                        width="300"
                        height="300"
                    />
                </div>
                <div class="modal-footer border-0">
                    <button
                        class="btn signin-btn deposit-btn mx-auto"
                        data-dismiss="modal"
                    >
                        تایید
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let get_cities = `{{route('get-cities')}}`;
        let action_role = `{{route('admin.user.edit',$user)}}`;
    </script>
    <script src="{{ asset('theme/user/scripts/active-inputs.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/admin-1.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/admin-tabs.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/active-custom-select.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/custom-select-box-2.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/main.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/add-person.js') }}"></script>
    <script src="{{ asset('theme/user/scripts/newAddPerson.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".state ul li").click(function () {
                var id = $(this).data('value');
                $.get(get_cities, {id: id}, function (data) {
                    data = JSON.parse(data)
                    $("select.city").html(data);
                    $(".city ul").html('')
                    var val = '';
                    var text = '';
                    var title = '';
                    $(data).each(function (i, v) {
                        val = $(v).val();
                        text = $(v).text();
                        $(".city ul").append('<li class="custom-list__item-2" data-value="' + val + '"><span>' + text + '</span></li>')
                    })
                    $(".city .custom__select-title-2").text($($(data)[0]).text())
                })
            })
            $(document).on('click', '.button-modal-document', function () {
                var val = $(this).find('img').attr('src');
                $('.modal-document .modal-header img').attr('src', val);
                $('.modal-document').modal('show');
            })
            $(".assets ul li").click(function () {
                let text = $(this).data('value');
                let val = $(".assets-select").find("option[value='" + text + "']").data('amount')
                $("#inventory").text('موجودی: ' + rial_to_unit(val, text, true))
            });

            $('input[name =amount]').on('keyup', function () {
                let amount = $(this).val();
                $(this).val(numberFormat(amount));
            })

            /*      $(".new-persons li button:not(:disabled)").click(function () {
                      swal({
                          title: "اطمینان از اضافه کردن نقش به کاربر دارید؟",
                          icon: "warning",
                          buttons: ['انصراف','تایید'],
                      })
                          .then((willDelete) => {
                              if (willDelete) {
                                  swal("Poof! Your imaginary file has been deleted!", {
                                      icon: "success",
                                  });
                              }
                          });
                  })*/

        });

    </script>
@append
