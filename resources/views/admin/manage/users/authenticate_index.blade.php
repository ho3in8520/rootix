@extends('templates.admin.master_page')
@section('title_browser')
    احراز هویت-پنل مدیریت روتیکس
@endsection
@section('content')
    <section id="basic-form-layouts" class="dashboard-cart user-list">
        <div class="container">
            <h1 class="dashboard-title desktop-title">مدیریت احراز هویت</h1>
            <div class="dashboard-table transaction-table-container">

                <div class="filters-container">
                    <button class="col-12 filter-btn" data-toggle="modal" data-target="#toggle-filters-inputs">
                        <svg xmlns="http://www.w3.org/2000/svg" width="23.814" height="20.15"
                             viewBox="0 0 23.814 20.15">
                            <g transform="translate(-3.375 -5.625)">
                                <path
                                    d="M17.761,26.124a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                    transform="translate(0 -3.554)"/>
                                <path
                                    d="M8.6,16.561a2.293,2.293,0,0,1,4.2,0h13.47a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H12.8a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0a.919.919,0,0,1,.916-.916Z"
                                    transform="translate(0 -1.777)"/>
                                <path
                                    d="M17.761,7a2.293,2.293,0,0,1,4.2,0h4.311a.919.919,0,0,1,.916.916h0a.919.919,0,0,1-.916.916H21.963a2.293,2.293,0,0,1-4.2,0H4.291a.919.919,0,0,1-.916-.916h0A.919.919,0,0,1,4.291,7Z"/>
                            </g>
                        </svg>
                        <span class="pr-1">فیلترها</span>
                    </button>
                    <div id="toggle-filters-inputs" class="up-animation modal">
                        <div class="modal-content pt-0">

                            <div class="modal-header">
                                <h6 class="modal-title">فیلترهای جدول</h6>
                                <button type="button" class="close ml-0 mr-auto" data-dismiss="modal">&times;
                                </button>
                            </div>

                            <form action="">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-controlers pb-2">
                                        <div class="header-transactionrls mb-3">
                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblName fs-16">نام ونام
                                                    خانوادگی</label>
                                                <input type="text" name="LblName" id="LblName"
                                                       value="{{request()->query('LblName')}}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblEmail fs-16">موبایل</label>
                                                <input type="text" id="LblEmail" name="mobile"
                                                       value="{{ request()->query('mobile') }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label
                                                    class="mb-2 mt-3 d-block transaction__filter-title fs-16">ایمیل</label>
                                                <input type="text" id="LblEmail" name="email"
                                                       value="{{ request()->query('email') }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block transaction__filter-title fs-16">کد
                                                    ملی</label>
                                                <input type="text" id="LblEmail" name="national_code"
                                                       value="{{ request()->query('national_code') }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblVariz fs-16">جنسیت</label>
                                                <select class="custom-selected" name="gender">
                                                    <option value="" {{ !request()->query('gender')?'selected':'' }}>همه</option>
                                                    <option value="1" {{ request()->query('gender')=='1'?'selected':'' }}>خانم
                                                    </option>
                                                    <option value="0" {{ request()->query('gender')=='0'?'selected':'' }}>آقا
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="header-transactionrls mb-3">
                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block" id="LblCodeP fs-16">تاریخ تولد</label>
                                                <input type="text" id="LblCodeP" class="datePicker" name="birth_day"
                                                       value="{{ request()->query('birth_day') }}">
                                            </div>

                                            <div class="w-100  bg-white">
                                                <label class="mb-2 mt-3 d-block LblVariz fs-16">وضعیت پنل</label>
                                                <select class="custom-selected" name="status">
                                                    <option value="0" {{ !request()->query('status')?'selected':'' }}>همه</option>
                                                    <option value="3" {{ request()->query('status')=='3'?'selected':'' }}>رد
                                                        شده
                                                    </option>
                                                    <option value="1" {{ request()->query('status')=='1'?'selected':'' }}>جدید

                                                    </option>
                                                </select>
                                            </div>

                                            <div class="w-100 date-transaction bg-white">
                                                <button class="btn fixed-bottom transaction__btn2 search-ajax"
                                                        data-dismiss="modal">جستجو
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="overflow-auto">
                            <div class="table-responsive specification">
                                <table class="container Specification-table mt-5">
                                    <thead>
                                    <tr>
                                        <th class="user-code">
                                            <div class="flat">
                                                کد کاربری
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div class="flat">
                                                نام و نام خانوادگی
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div>
                                                ایمیل
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div>
                                                کد ملی
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div class="flat">
                                                سطح کاربر
                                            </div>
                                        </th>
                                        <th class="header">
                                            <div class="flat">
                                                تاریخ
                                            </div>
                                        </th>
                                        <th class="">
                                            <div class="flat">
                                                <div class="btn-group">
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        @php
                                            $status_class=['1'=>'info','3'=>'failed'];
                                            $status_text=['1'=>'جدید','3'=>'رد شده'];
                                        @endphp
                                        <tr>
                                            <td class="code pt-4">{{$user->user->code}}</td>
                                            <td class="name header pt-4">{{$user->user->fullname??'بدون نام'}}</td>
                                            <td class="gmail-national-code header pt-4">{{$user->user->email}}</td>
                                            <td class="gmail-national-code header pt-4">{{$user->user->national_code??'-'}}</td>
                                            <td class="header user-level pt-4">
                                                <span
                                                    class="{{$status_class[$user->status]}} flat">{{$status_text[$user->status]}}</span>
                                            </td>
                                            <td class="header pt-5">{{jdate_from_gregorian($user->created_at,'%Y.%m.%d')}}
                                                <span
                                                    class="time">{{jdate_from_gregorian($user->created_at,'H:i')}}</span>
                                            </td>
                                            <td class="pt-5 pl-3">
                                                <div class="button-success">
                                                    <div class="pb-2">
                                                        <a class="authentication-btn"
                                                           href="{{route('user.authenticate.form',$user->user_id)}}">
                                                            احراز هویت
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {!! $users->links() !!}
                        <div class="pdf-exel mr-3 mt-5 mb-5">
                            <a href="{{route('user.index',request()->all()+['export' => 'pdf'])}}" class="btn-pdf ml-3">خروجی
                                PDF
                                <img class="pdf-image" src="{{asset('theme/admin/img/Icon%20metro-file-pdf.svg')}}"
                                     alt="">
                            </a>
                            <a href="{{route('user.index',request()->all()+['export'=>'excel'])}}" class="btn-exel">خروجی
                                Excel
                                <img class="excel-image"
                                     src="{{asset('theme/admin/img/Icon%20simple-microsoftexcel.svg')}}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            } else {
                return uri + separator + key + "=" + value;
            }
        }

        $(document).ready(function () {
            $(document).on('click', '.ordering', function () {
                window.location.href = updateQueryStringParameter(window.location.href, 'ordering', $(this).data('value'))
            });
        })
    </script>
@endsection
