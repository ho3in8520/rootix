@extends('templates.admin.master_page')
@section('title_browser')
    تنظیمات واریز و برداشت-پنل مدیریت روتیکس
@endsection
@section('style')
    <style>
        .currency-image{
            width: 43px;
            height: 44px;
        }
    </style>
@endsection
@section('content')
    <section class="dashboard-cart user-list">
        <div class="container ">
            <div class="d-flex">
                <h1 class="dashboard-title desktop-title">تنظیمات برداشت</h1>
                <form action="#" class="search-box  mr-4 " style=" border: 2px solid black ;
    width: 19%;height: fit-content
">
                    <input
                        type="text"
                        class="search-box__input "
                        placeholder="جست و جو ارز"

                    />
                    <svg
                        class="search-box__icon"
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 28.811 28.811"
                    >
                        <g
                            id="Icon_feather-search"
                            data-name="Icon feather-search"
                            transform="translate(-3.75 -3.75)"
                        >
                            <path
                                id="Path_8606"
                                data-name="Path 8606"
                                d="M28.5,16.5a12,12,0,1,1-12-12A12,12,0,0,1,28.5,16.5Z"
                                fill="none"
                                stroke="#777"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1.5"
                            />
                            <path
                                id="Path_8607"
                                data-name="Path 8607"
                                d="M31.5,31.5l-6.525-6.525"
                                fill="none"
                                stroke="#777"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="1.5"
                            />
                        </g>
                    </svg>
                </form>
            </div>

            <div class="dashboard-table transaction-table-container">
                <div class="filters-container">
                    <div class="container">
                        <!--//edit-->
                        <div class="row">
                            @php
                                $index=0;
                            @endphp
                            @foreach($currencies as $currency)
                                <div class="row col-md-6  text-center">
                                    <div class="table-title col-md-4   align-items-center">
                                        @if($index==0 || $index==1)
                                            <div class="currency-title d-block col-md-5 mb-2 align-items-center">
                                                <p>نام</p>
                                            </div>
                                        @endif
                                        <div class="align-items-center d-flex">
                                            <div class="ripple-img-background">
                                                <img class="currency-image" src="{{$currency->logo_url??''}}">
                                            </div>
                                            <div>
                                                <h5>{{$currency->name}}</h5>
                                                <h6>{{strtoupper($currency->unit)}}</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 table-button align-items-center">
                                        @if($index==0 || $index==1)
                                            <div class="deposit-title d-block col-md-4 mb-5">
                                                <p>واریز</p>
                                            </div>
                                        @endif
                                        <div class="form-check mt-3 d-flex justify-center mb-2 align-items-center">
                                            <p class="mr-2">فعال</p>
                                            <label class="switch mr-2">
                                                <input type="checkbox" name="deposit_status"
                                                       data-currency="{{$currency->id}}" {{$currency->deposit_status==1?'checked':''}}>
                                                <span class="slider round"></span>
                                            </label>
                                            <p class="mr-2">غیر فعال</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 table-button align-items-center">
                                        @if($index==0 || $index==1)
                                            <div class="harvest-title  d-block col-md-2 mb-5">
                                                <p>برداشت</p>
                                            </div>
                                        @endif
                                        <form action="">
                                            @csrf
                                            <div class="form-check mt-3 mb-2 d-flex justify-center">
                                                <p class="mr-2">فعال</p>
                                                <label class="switch mr-2">
                                                    <input type="checkbox" name="withdraw_status"
                                                           data-currency="{{$currency->id}}" {{$currency->withdraw_status==1?'checked':''}}>
                                                    <span class="slider round"></span>
                                                </label>
                                                <p class="mr-2">غیر فعال</p>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <hr class="dashed-hr-response ">
                                @php
                                    $index++;
                                @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('script')
    <script>
        $('input[name=withdraw_status] , input[name=deposit_status]').on('change', function (e) {
            let type = $(this).attr('name');
            let currency_id = $(this).data('currency');
            let status;
            status = e.target.checked ? 1 : 0;

            console.log('status is ', status)
            let formData = {
                isObject: true,
                action: "{{route('admin.wd-currency-status')}}",
                method: 'post',
                button: $(this),
                data: {
                    type: type,
                    currency_id: currency_id,
                    status: status,
                }
            }
            ajax(formData);
        })
    </script>
@endsection
