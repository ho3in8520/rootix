<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <!--[if mso]>
    <noscript>
        <xml>
            <o:OfficeDocumentSettings>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
        </xml>
    </noscript>
    <![endif]-->
    <style>
        table, td, div, h1, p {font-family: Arial, sans-serif;}
        .header-container td {
            padding: 5px 0 5px 0;
            background: #303030;
            display: flex;
            flex-direction: row-reverse;
            justify-content: center;
            align-items: center;
        }
        .header-container td img {
            width: 50px;
            height: 50px;
        }
        .header-container td h2{
            color: #fff;
            margin-right: 11px;
        }
        .footer td {
            padding:15px;
            background:#515151;
        }
    </style>
</head>
<body style="margin:0;padding:0;">
<table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;background:#ffffff;">
    <tr>
        <td align="center" style="padding:0;">
            <table role="presentation" style="width:602px;border-collapse:collapse;border:1px solid #cccccc;border-spacing:0;text-align:left;">
                <tr class="header-container">
                    <td>
                        <img src="https://rootix.io/theme/landing/images/logo.png" class="mb-3">
                        <h2>روتیکس | صرافی ارز دیجیتال</h2>
                    </td>
                </tr>
                <tr>
                    <td style="padding:36px 30px 42px 30px;">
                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;">
                            <tr>
                                <td style="padding:0 0 36px 0;color:#153643; text-align: right">
                                    <h1 style="font-size:24px;margin:0 0 20px 0;font-family:Arial,sans-serif;">ورود به پنل کاربری</h1>
                                    <p>
                                        شما در تاریخ {{ $date_time }} وارد پنل کاربری روتیکس شده اید
                                        در صورت عدم تایید روی لینک زیر کلیک کنید
                                    </p>
                                    <p>ip: {{ $ip }}</p>
                                    <sub>در صورت کلیک روی لینک زیر کاربر از پنل حارج شده و رمز عبور جدیدی برای شما فعال فعال میشود</sub>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="{{ route('force-logout',$token) }}" style="background: red; color: #fff; padding: 15px 30px; border-radius: 5px; text-align: center;
text-decoration: none; display: block; margin: auto">خروج از پنل و تعویض رمز</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="footer">
                    <td>
                        <table role="presentation" style="width:100%;border-collapse:collapse;border:0;border-spacing:0;font-size:9px;font-family:Arial,sans-serif;">
                            <tr>
                                <td style="padding:0;width:50%;" align="left">
                                    <table role="presentation" style="border-collapse:collapse;border:0;border-spacing:0;">
                                        <tr>
                                            <td style="padding:0 0 0 10px;width:38px;">
                                                <a href="http://www.twitter.com/" style="color:#ffffff;"><img src="https://assets.codepen.io/210284/tw_1.png" alt="Twitter" width="38" style="height:auto;display:block;border:0;" /></a>
                                            </td>
                                            <td style="padding:0 0 0 10px;width:38px;">
                                                <a href="http://www.facebook.com/" style="color:#ffffff;"><img src="https://assets.codepen.io/210284/fb_1.png" alt="Facebook" width="38" style="height:auto;display:block;border:0;" /></a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
