@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    جزییات قیمت {{$currency_info[$currency]['name_fa']}}-روتیکس
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="res-currency-details-info">
                <div class="d-flex items-center justify-between">
                    <div class="res-currency-details-info__img">
                        @php
                            $img= $currency_info[$currency]['logo_url']?'':$currency_info[$currency]['currency'];
                        @endphp
                        <img width="72px" height="72px" src="{{$currency_info[$currency]['logo_url']?str_replace('64x64','128x128',$currency_info[$currency]['logo_url']):asset("theme/landing/images/currency/$img.png")}}" alt="{{$currency_info[$currency]['currency']}}"/>
                    </div>
                    <div class="res-currency-details-info__title">
                        <h1>{{$currency_info[$currency]['currency']}}</h1>
                        <span>{{$currency_info[$currency]['name']}}</span>
                    </div>
                    <div class="res-currency-details-info__percent-change-{{$currency_info[$currency]['1d']>0?'green':'red'}}">
                                        <span>
                                          <svg
                                              xmlns="http://www.w3.org/2000/svg"
                                              width="12"
                                              height="8"
                                              viewBox="0 0 13.298 8.916"
                                          >
                                            <path
                                                id="Icon_feather-chevron-up"
                                                data-name="Icon feather-chevron-up"
                                                d="M10.486,6.51,5.243,0,0,6.51"
                                                transform="translate(11.892 7.916) rotate(180)"
                                                fill="none"
                                                stroke="#fff"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"
                                                stroke-width="2"
                                            />
                                          </svg>
                                          {{number_format($currency_info[$currency]['1d'],2)}}
                                        </span>
                    </div>
                </div>

                <div
                    class="d-flex justify-between res-currency-details-prices-info"
                >
                    <div class="res-currency-details-prices">
                        <p class="res-currency-details-prices__title">
                            قیمت لحظه ای {{$currency_info[$currency]['name_fa']}}:
                        </p>
                        <h2 class="res-currency-details-prices__mein-price">
                            <span>USDT</span>
                            @if($currency_info[$currency]['price']>=1)
                                {{number_format($currency_info[$currency]['price'],2)}}
                            @else
                                {{number_format($currency_info[$currency]['price'],7)}}
                            @endif
                        </h2>
                    </div>

                    <div class="currency-details__prices-selected">
                        <div class="title-selected" dir="ltr">
                            <div>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="13"
                                    height="10"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        fill="#1b274c"
                                    />
                                </svg>
                            </div>
                            <h5>USDT</h5>
                        </div>

                        <ul class="h-100 selected-currency">
                            {{--                            <li value="BTC">BTC</li>--}}
                            <li value="TRX">USDT</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="res-details-chart">
                <div class="tradingview-widget-container" style="pointer-events: none">
                    <div id="tradingview_81a31"></div>
                    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                    <script type="text/javascript">
                        new TradingView.MediumWidget(
                            {
                                "symbols": [
                                    [
                                        "{{$currency_info[$currency]['currency']}}USDT",
                                        "{{$currency_info[$currency]['currency']}}USDT"
                                    ],
                                ],
                                "chartOnly": true,
                                "width": 350,
                                "height": 200,
                                "locale": "en",
                                "colorTheme": "light",
                                "gridLineColor": "rgba(240, 243, 250, 0)",
                                "trendLineColor": "#2962FF",
                                "fontColor": "#787B86",
                                "underLineColor": "rgba(41, 98, 255, 0.3)",
                                "underLineBottomColor": "rgba(41, 98, 255, 0)",
                                "isTransparent": false,
                                "autosize": true,
                                "showFloatingTooltip": false,
                                "scalePosition": "right",
                                "scaleMode": "Normal",
                                "container_id": "tradingview_81a31"
                            }
                        );
                    </script>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div>
            {{--            <div class="d-flex market-volume-container">--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="d-flex market-volume-container">--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--                <div class="market-volume">--}}
            {{--                    <p>حجم معاملات بازار: (USD)</p>--}}
            {{--                    <h2>1,047,215,567,481.27</h2>--}}
            {{--                </div>--}}
            {{--            </div>--}}


            <div class="container">
                <div class="market-volume-btns">
                    <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}">
                        خرید
                    </a>
                    <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}">
                        فروش
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <h2 class="res-market-table-title">بازار</h2>
            {{--            <div class="d-flex justify-between items-center res-market-table">--}}
            {{--                <h5 class="res-market-table-name">BTC/USDT</h5>--}}

            {{--                <div class="res-market-table-price">--}}
            {{--                    <p>۵۰۱۷۵.۹۸</p>--}}
            {{--                    <span class="plus">+۳.۱۵</span>--}}
            {{--                </div>--}}

            {{--                <a href="#" class="res-market-table-btn"> خرید و فروش </a>--}}
            {{--            </div>--}}
            <div class="d-flex justify-between items-center res-market-table">
                <h5 class="res-market-table-name">{{$currency_info[$currency]['currency']}}/USDT</h5>

                <div class="res-market-table-price">
                    @if($currency_info[$currency]['price']>=1)
                        <p>{{number_format($currency_info[$currency]['price'],2)}}$</p>
                    @else
                        <p>{{number_format($currency_info[$currency]['price'],7)}}$</p>
                    @endif
                    <span dir="ltr" class="{{$currency_info[$currency]['1d']<0?'minus':'plus'}}">{{$currency_info[$currency]['1d']>=0?'+'.number_format($currency_info[$currency]['1d'],2):number_format($currency_info[$currency]['1d'],2)}}</span>
                </div>

                <a href="{{route('exchange.index',['des_unit'=>strtolower($currency_info[$currency]['currency']),'source_unit'=>'usdt'])}}" class="res-market-table-btn"> خرید و فروش </a>
            </div>
        </div>
    </section>

    <section class="res-details-prices-about-currency">
        {{--        <div class="container">--}}
        {{--            <h2 class="res-market-table-title">معرفی بیت کوین</h2>--}}
        {{--            <p class="res-currency-desc">--}}
        {{--                بیت‌کوین یک رمزارز یا ارز دیجیتال و در حقیقت یک سیستم پرداخت جهانی--}}
        {{--                است که رفتاری مشابه با پول بی‌پشتوانه دارد. به‌عنوان توضیحی ساده‌تر،--}}
        {{--                بیت کوین یک دارایی دیجیتالی است که مبنایی صفر و یک داشته و به‌صورت--}}
        {{--                الکترونیکی تولید، ذخیره و منتقل می‌شود. حساب و کتاب تراکنش‌های--}}
        {{--                بیت‌کوین در یک زنجیره به‌هم‌پیوسته اطلاعاتی به نام بلاک‌چین یا--}}
        {{--                زنجیره‌بلوک نگهداری شده و تراکنش‌های شبکه در هر بلاک توسط ماینر‌ها--}}
        {{--                لیست می‌شود. مهم‌ترین مسئله در مورد بیت‌کوین این است که این ارز--}}
        {{--                دیجیتال محدودیت 21 میلیون واحدی دارد! به بیانی ساده‌تر یعنی بعد از--}}
        {{--                استخراج 21 میلیون سکه، دیگر امکان استخراج بیت‌کوین وجود نداشته و به--}}
        {{--                انتها می‌رسد. بیت کوین بسیاری از ویژگی‌های ارزهای فیزیکی را دارد، با--}}
        {{--                این تفاوت که آنی و بدون مرز بین افراد قابل جابجایی است. ارز دیجیتال--}}
        {{--                بیت کوین در بستر اینترنت به کمک رایانه‌های پیشرفته و با قدرت پردازش--}}
        {{--                بسیار بالا، در نتیجه حل معادلات ریاضیاتی .بسیار پیچیده استخراج--}}
        {{--                می‌شود--}}
        {{--            </p>--}}
        {{--        </div>--}}
    </section>
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/open-market-info.js')}}"></script>
@endsection
