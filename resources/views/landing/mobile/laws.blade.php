@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    قوانین روتیکس
@endsection
@section('style')
    <style>
        ul li{
            list-style: square ;
            line-height: 2rem;
            text-align: justify;
        }
        p{
            line-height: 2rem;
        }

        .laws-container{
            max-width: 800px;
            padding: 2rem 1.5rem;
        }
    </style>
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>قوانین روتیکس</h5>
        </div>
    </div>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container laws-container">
            <div class="row">
                <div class="col-12">
                    {!! json_decode($laws->extra_field3) !!}
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->
@endsection

