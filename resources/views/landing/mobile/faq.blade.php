@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    سوالات متداول-روتیکس
@endsection
@section('header')
    <div>

        <div class="about-header-info-res">
            <h5>سوالی دارید؟</h5>
            <h1>از ما بپرسید</h1>
        </div>

        <form action="#" class="faqs-search__search-box">
            <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 17.302 17.302">
                <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4 -4)">
                    <path id="Path_8028" data-name="Path 8028" d="M18.807,11.653A7.153,7.153,0,1,1,11.653,4.5,7.153,7.153,0,0,1,18.807,11.653Z" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                    <path id="Path_8029" data-name="Path 8029" d="M28.865,28.865l-3.89-3.89" transform="translate(-8.27 -8.27)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                </g>
            </svg>

            <input type="search" placeholder="چه سوالی دارید؟" />
        </form>
    </div>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container faqs-container">
            <div class="row">
                <div class="col-12">
                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">روتیکس چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            روتیکس یک صرافی ارز دیجیتال است که میتوانید از طریق درگاه امن آن اقدام به خرید ارزهای دیجیتال مثل بیت کوین کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">ارز دیجیتال چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در قرن بیست و یکم ارزهای دیجیتال در حال جایگزینی با ارزهای رایج مانند ریال و دلار و یورو و حتی طلا میباشد و از این پس تمام تبادلات مالی در سطح جهان و حتی خریدهای روزانه با ارز دیجیتال انجام خواهد شد. لذا استفاده از ارزهای دیجیتال در اینده نزدیک اجتناب ناپذیر خواهد بود.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور ارز دیجیتال بخرم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            با ثبت نام در سایت صرافی ارز دیجیتال روتیکس به راحتی میتوانید از طریق درگاه بانکی واریز ریالی انجام دهید و با امکانات سایت به راحتی آن را تبدیل به ارز دیجیتال دلخواه خود کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور ارز دیجیتال بفروشم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در صرافی روتیکس با استفاده از امکانات ساده و در عین حال قدرتمند و امن میتوانید انواع ارز دیجیتال را به ریال و یا هر نوع ارز دیجیتال دیگر تبدیل کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">کارمزد خرید و فروش ارز دیجیتال در روتیکس چقدر است؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            کارمزدها بستگی به سیستم بانکی کشور و کارمزد شبکه ارز دیجیتال متفاوت است و در صرافی روتیکس کمترین میزان کارمزدها را در بین تمامی صرافیها تجربه خواهید کرد.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور شروع کنم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            با <a href="{{route('register.form')}}" style="color: deepskyblue">ثبت نام</a> در صفحه مورد نظر به راحتی و فقط با ادرس ایمیل و شماره تلفن همراه میتوانید وارد پنل کاربری خود شده و از همین لحظه اقدام به خرید ارز دیجیتال کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">احراز هویت چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            جهت استفاده از خدمات صرافی روتیکس نیازمند احراز هویت هستید که سطوح متفاوتی دارد و در سطح اول فقط با شماره تلفن همراه و ایمیل میتوانید از امکانات صرافی استفاده کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چه اطلاعاتی برای احراز هویت نیاز دارم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در سطح اول شماره تلفن همراه و ایمیل نیاز است و جهت سطوح بالاتر استفاده از خدمات صرافی نیازمند احراز هویت به صورت اطلاعات حساب بانکی و کد ملی میباشید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چرا باید اطلاعات هویتی خودم را در اختیار روتیکس بگذارم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    class="faq-arrow-down faq-arrow"
                                    height="11"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            روتیکس برای حفظ امنیت سرمایه ریالی و دیجیتالی شما نیازمند احراز هویت میباشد ، احراز هویت باعث میشود شخصی غیر از شما نتواند اقدام به برداشت یا جابجایی سرمایه شما کند.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2>جواب سوالت رو پیدا نکردی؟</h2>
                            <p class="top-form-desc">
                                ...............

                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="فرم" />
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="نام و نام خانوادگی...">
                            <input type="email" placeholder="آدرس ایمیل...">
                            <input type="text" placeholder="آدرس سایت...">
                            <textarea placeholder="متن پیام..."></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn" disabled>
                                ارسال
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
@endsection
