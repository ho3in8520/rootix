@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    قیمت ها-روتیکس
@endsection
@section('main_class','main__prices-responsive')
@section('content')
    <section>
        <div class="bottom-price-menu">
            <div class="container">
                <div
                    class="
                res-menu__bottom
                res-menu-price__bottom
                d-flex
                justify-between
              "
                >
                    {{--                    <div>--}}
                    {{--                        <h5>حجم بازار:</h5>--}}
                    {{--                        <span>۲ ترلیون دلار</span>--}}
                    {{--                    </div>--}}

                    <div>
                        <h5>نرخ تتر :</h5>
                        <span> {{rial_to_unit($tether,'rls',true)}}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{--    <section>--}}
    {{--        <div class="container">--}}
    {{--            <div class="market-informations">--}}
    {{--                <div class="market-information-btn">--}}
    {{--                    <h5>اطلاعات کلی بازار</h5>--}}

    {{--                    <button class="open-market-information">--}}
    {{--                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="10" viewBox="0 0 18 11.115">--}}
    {{--                            <path id="Icon_material-expand-more" data-name="Icon material-expand-more"--}}
    {{--                                  d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"--}}
    {{--                                  transform="translate(27 24) rotate(180)"/>--}}
    {{--                        </svg>--}}
    {{--                    </button>--}}
    {{--                </div>--}}

    {{--                <div class="market-information">--}}
    {{--                    <h5>تعداد ارز های دیجیتال: ۷/۰۰۵</h5>--}}
    {{--                    <h5>معاملات ۲۴ ساعت گذشته: ۵۰/۲۳۰ میلیون دلار</h5>--}}

    {{--                    <div class="d-flex justify-between mb-5">--}}
    {{--                        <h5>بیشترین معامله: تتر</h5>--}}

    {{--                        <h5>کمترین معامله: ارز اکیتا</h5>--}}
    {{--                    </div>--}}

    {{--                    <div>--}}
    {{--                        <div class="d-flex justify-between">--}}
    {{--                            <h5>حجم بازار: <span class="market-volume-title">۱۰۶ میلیون دلار</span></h5>--}}
    {{--                            <h5 class="market-persent">23.5-</h5>--}}
    {{--                        </div>--}}
    {{--                        <img src="images/prices/Line chart.png" alt="چارت" class="w-100">--}}
    {{--                    </div>--}}

    {{--                    <div>--}}
    {{--                        <h5>نرخ تتر: ۳۰/۰۰۰ تومان</h5>--}}
    {{--                        <img src="images/prices/Line chart.png" alt="چارت" class="w-100">--}}
    {{--                    </div>--}}

    {{--                    <div class="share-market">--}}
    {{--                        <h5>سهم بازار</h5>--}}
    {{--                        <div class="d-flex items-center justify-between">--}}
    {{--                            <div class="d-flex items-center">--}}
    {{--                                <span>THTHER: 44.20</span>--}}
    {{--                                <img src="images/prices/trx.d986d6a (1).png" alt="ترون">--}}
    {{--                            </div>--}}
    {{--                            <div class="d-flex items-center">--}}
    {{--                                <span>BTC: 44.20</span>--}}
    {{--                                <img src="images/prices/bitcoin.png" alt="ترون">--}}
    {{--                            </div>--}}
    {{--                            <div class="d-flex items-center">--}}
    {{--                                <span>DOGE: 44.20</span>--}}
    {{--                                <img src="images/prices/doge.png" alt="ترون">--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}

    <section>
        <div class="container">
            <h1 class="price-table-currency__title">
                قیمت لحظه ای ارز های دیجیتال
            </h1>
        </div>

        <div class="res-prices-btns-container">
            <div class="res-prices-btns">
                <button>بر اساس قیمت</button>
                <button disabled>اوج قیمت</button>
                <button disabled>پر نوسان ترین</button>
                <button disabled>محبوب ترین</button>
                <button disabled>مبدل قیمت</button>
                <button disabled>مقایسه ارزها</button>
                <button disabled>بازار ایران</button>
                <button disabled>نمای بازار</button>
            </div>
        </div>

        <div class="container">
            <table class="currency-table prices-res-currency-table">
                <tr>
                    <td>
                        <div class="td-row">
                            ارز دیجیتال
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="12"
                                height="20"
                                viewBox="0 0 12.203 20.383"
                            >
                                <path
                                    id="Icon_material-unfold-more"
                                    data-name="Icon material-unfold-more"
                                    d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                    transform="translate(-11.115 -4.5)"
                                    opacity="0.7"
                                />
                            </svg>
                        </div>
                    </td>
                    <td>
                        <div class="td-row">
                            قیمت
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="12"
                                height="20"
                                viewBox="0 0 12.203 20.383"
                            >
                                <path
                                    id="Icon_material-unfold-more"
                                    data-name="Icon material-unfold-more"
                                    d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                    transform="translate(-11.115 -4.5)"
                                    opacity="0.7"
                                />
                            </svg>
                        </div>
                    </td>
                    <td>
                        <div class="td-row">
                            تغییرات
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="12"
                                height="20"
                                viewBox="0 0 12.203 20.383"
                            >
                                <path
                                    id="Icon_material-unfold-more"
                                    data-name="Icon material-unfold-more"
                                    d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                    transform="translate(-11.115 -4.5)"
                                    opacity="0.7"
                                />
                            </svg>
                        </div>
                    </td>
                </tr>

                @foreach($currencies as $key=>$currency)
                    <tr class="second-after">
                        <td class="currency-title">
                            <a href="{{route('landing.price_detail',$key)}}">
                                <div class="currency-right-title">
                                    <img src="{{$currency['logo_url']?$currency['logo_url']:asset("theme/landing/images/currency/$key.png")}}" alt="{{$currency['currency']}}"/>
                                    <div class="currency-left-title">
                                        <h5>{{$currency['name']}}</h5>
                                        @if(isset($currency['market_cap']))
                                            <span>{{number_format($currency['market_cap']/1000000000,2)}}B</span>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        </td>
                        <td class="currency-last-price">
                            <div class="currency-doloar-price just-for-center">
                                @if($currency['price']>=1)
                                    <h5>{{number_format($currency['price'],3)}}$</h5>
                                    <p>{{number_format(rial_to_unit($currency['rls'],'rls'))}}تومان</p>
                                @else
                                    <h5>{{number_format($currency['price'],7)}}$</h5>
                                    <p style="text-align: left">{{number_format(rial_to_unit($currency['rls'],'rls'),3)}}تومان</p>
                                @endif
                            </div>
                        </td>
                        <td class="currency-present-change">
                            <div class="just-for-center">
                                @if(isset($currency['1d']))
                                    @php
                                        $img=(float)$currency['1d']<0?'red_arrow':'green_arrow';
                                    @endphp
                                    <span
                                        class="currency-present-change {{(float)$currency['1d']<0?'red':'green'}}-currency">
                                                      <img src="{{asset("theme/landing/images/$img.svg")}}" alt="">
                                                    {{(float)$currency['1d']<0?number_format(substr($currency['1d'],1),2):number_format($currency['1d'],2)}}

                                        @else
                                            -
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </section>

    <section class="circle-charts">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>بیشترین معاملات</h2>
                            <h2>در روتیکس</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>مجموع برداشت های</h2>
                            <h2>روزانه</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>بیشترین سود</h2>
                            <h2>در معاملات</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Section Two -->
    <section class="prices-mobile-app prices-mobile-app-res">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6" style="order: 1">
                    <div class="what-currency-info what-currency-info-price">
                        <h5>اپلیکیشن موبایل رو فراموش نکنید</h5>
                        <div class="what-crypto-title what-crypto-title-3">
                            <h2>همیشه و همه جا بازار رو چک کن</h2>
                            <span>با اپلیکیشن موبایل روتیکس</span>
                        </div>
                        <div class="mobile-images price-mobile-image">
                            <img src="{{asset('theme/landing/images/prices/mobile.png')}}" alt="برنامه موبایل"/>
                        </div>
                        <p class="what-crypto-desc prices-what-crypto-desc">
                            پنل روتیکس در تلفن های هوشمند هم قابل استفاده است . به راحتی قابل دسترس است و با رابط کاربری
                            پیشرفته و آسان خود ، تجربه کار با صرافی در هر لحظه و هر مکان را با استفاده از تلفن هوشمند
                            برای کاربران خود ایجاد میکند.
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/open-market-info.js')}}"></script>
@endsection
