@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    آموزش-روتیکس
@endsection

@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container">
            <div class="education-res__title">
                <h1>آموزش مفاهیم پایه</h1>
                <p>
                    ........................
                </p>
            </div>
            <div
                class="
              owl-carousel owl-theme
              blog-page-top-slide blog-page-top-slide-res
            "
            >
                @foreach($newest as $new_post)
                    <div class="blog-image-slide-container">
                        <div class="blog-page-top-slide__img">
                            <img width="398px" height="406px"
                                src="{{$new_post->blogImage}}"
                                alt="ارز دیجیتال"
                            />
                        </div>

                        <div class="blog-page-top-slide__info">
                            <h1>
                                {{$new_post->title}}
                            </h1>
                            <div class="blog-page-top-slide__title">
                                <span>{{$new_post->user_guard->fullname}}</span>
                                <span>۵ نظر</span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <hr class="blog-page-line"/>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Four -->
    <section>
        <div class="container">
            <div class="educations-res">
                @foreach($posts as $post)
                    <a href="{{route('landing.post',$post->slug)}}">
                        <div class="main-news">
                            <img
                                src="{{$post->blogImage}}"
                                alt="تایتل آموزش"
                                class="main-news-res-image"
                            />

                            <div class="main-news__info">
                                <h2 class="main-news__title">
                                    {{$post->title}}
                                </h2>
                                <div class="main-news__writer">
                                    <span>{{$post->post_category->name}}</span>
                                    <span>{{jdate_from_gregorian($post->created_at)}}</span>
                                </div>
                            </div>
                        </div>

                        <hr class="main-news__res-line"/>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    <!-- End Section Four -->
@endsection
@section('script')

@endsection
