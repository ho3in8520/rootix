@extends('templates.landing_page.layout.mobile.master_page')
@section('title_browser')
    روتیکس-صرافی آنلاین ارز دیجیتال
@endsection
@section('detail')
    <div class="res-menu__bottom">
        <div class="res-menu__search">
            <button>
                <svg xmlns="http://www.w3.org/2000/svg" width="17.302" height="17.302" viewBox="0 0 17.302 17.302">
                    <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(-4 -4)">
                        <path id="Path_8028" data-name="Path 8028"
                              d="M18.807,11.653A7.153,7.153,0,1,1,11.653,4.5,7.153,7.153,0,0,1,18.807,11.653Z"
                              fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
                              stroke-width="1"/>
                        <path id="Path_8029" data-name="Path 8029" d="M28.865,28.865l-3.89-3.89"
                              transform="translate(-8.27 -8.27)" fill="none" stroke="#fff" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="1"/>
                    </g>
                </svg>

            </button>

            <input type="text" placeholder="جست و جو...">
        </div>
        <a href="{{route('login.form')}}" class="res-menu__login">
            <svg xmlns="http://www.w3.org/2000/svg" width="20.243" height="13.501" viewBox="0 0 20.243 13.501">
                <path id="Icon_ionic-ios-arrow-round-forward" data-name="Icon ionic-ios-arrow-round-forward"
                      d="M20.784,11.51a.919.919,0,0,0-.007,1.294l4.275,4.282H8.782a.914.914,0,0,0,0,1.828H25.045L20.77,23.2a.925.925,0,0,0,.007,1.294.91.91,0,0,0,1.287-.007l5.794-5.836h0a1.026,1.026,0,0,0,.19-.288.872.872,0,0,0,.07-.352.916.916,0,0,0-.26-.64l-5.794-5.836A.9.9,0,0,0,20.784,11.51Z"
                      transform="translate(-7.875 -11.252)" fill="#fff"/>
            </svg>
            ورود
        </a>
    </div>
@endsection
@section('header')
    <div class="row header-row">
        <div class="col-12">
            <div>
                <div class="header-info">
                    <div class="header-info__desc">
                        <h1>
                            روتیکس
                            <h2>بهترین تجربه در</h2>
                            <h2>صرافی های دیجیتال</h2>
                        </h1>
                    </div>
                    <p>
{{--                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با--}}
{{--                        استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله--}}
{{--                        در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد--}}
{{--                        نیاز--}}
                    </p>


                    <div class="header-currency-image">
                        <img src="{{asset('theme/landing/images/header-img.png')}}" alt="ارز دیجیتال"
                             class="header-currency">
                    </div>
                </div>
                <div class="header-btns">
                    <a href="{{route('register.form')}}" class="btn light-blue-btn animation-btn">
                        ثبت نام
                    </a>
                    <a href="#" class="btn main-btn">
                        <i class="fa fa-play"></i>
                        روتیکس چگونه کار می کند
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="header-currency-header-info">
        <div class="container">
            <h2>میزان معاملات ۲۴ ساعته روتیکس : ۲۵۰/۰۰۰/۰۰۰</h2>
        </div>

        <div class="header-currency-informations-container">
            <div class="container">

                <div class="currencys">

                    <div class="header-currency-informations">
                        @php
                            $currencies1=get_specific_currencies('btc,eth,ltc,usdt,xrp');
                        @endphp
                        @foreach($currencies1 as $key=>$item)
                            @php
                                $sign=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);
                            @endphp
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>

                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">
                                        {{$sign}}  {{number_format((float)$value,2)}}  {{'%'}}
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">
                                        {{number_format($item['price'],2)}}
                                    </h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="header-currency-informations">
                        @php
                            $currencies2=get_specific_currencies('bch,bnb,eos,jst,win');
                        @endphp
                        @foreach($currencies2 as $key=>$item)
                            @php
                                $sign=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);
                            @endphp
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>

                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">
                                        {{$sign}} {{number_format((float)$value,2)}} {{"%"}}
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">
                                        {{number_format($item['price'],2)}}
                                    </h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="header-currency-informations">
                        <div class="header-currency-information header-currency-information-1">
                            <div class="header-currency-desc">
                                <h4 class="header-currency-name">BNB/USDT</h4>

                                <h4 class="down-currency">
                                    -12.5%
                                </h4>
                            </div>
                            <div class="header-currency-price-container">
                                <h3 class="header-currency-price">
{{--                                    {{$currency->currency_to_usdt('bnb')}}--}}
                                </h3>
                                <h4>$0.0256</h4>
                            </div>
                        </div>
                        <div class="header-currency-information">
                            <div class="header-currency-desc">
                                <h4 class="header-currency-name">EOS/USDT</h4>

                                <h4 class="up-currency">
                                    -12.5%
                                </h4>
                            </div>
                            <div class="header-currency-price-container">
                                <h3 class="header-currency-price">
{{--                                    {{$currency->currency_to_usdt('eos')}}--}}
                                </h3>
                                <h4>$0.0256</h4>
                            </div>
                        </div>
                        <div class="header-currency-information">
                            <div class="header-currency-desc">
                                <h4 class="header-currency-name">XLM/USDT</h4>

                                <h4 class="down-currency">
                                    -12.5%
                                </h4>
                            </div>
                            <div class="header-currency-price-container">
                                <h3 class="header-currency-price">
{{--                                    {{$currency->currency_to_usdt('xlm')}}--}}
                                </h3>
                                <h4>$0.0256</h4>
                            </div>
                        </div>
                        <div class="header-currency-information">
                            <div class="header-currency-desc">
                                <h4 class="header-currency-name">ETC/USDT</h4>

                                <h4 class="up-currency">
                                    -12.5%
                                </h4>
                            </div>
                            <div class="header-currency-price-container">
                                <h3 class="header-currency-price">
{{--                                    {{$currency->currency_to_usdt('etc')}}--}}
                                </h3>
                                <h4>$0.0256</h4>
                            </div>
                        </div>
                        <div class="header-currency-information">
                            <div class="header-currency-desc">
                                <h4 class="header-currency-name">DOGE/BUSD</h4>

                                <h4 class="up-currency">
                                    -12.5%
                                </h4>
                            </div>
                            <div class="header-currency-price-container">
                                <h3 class="header-currency-price">0.0002569</h3>
                                <h4>$0.0256</h4>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="go-down-container">
        <a href="#currency-table" class="go-down">
            <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن"/>
        </a>
    </div>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="p-t table-currency">
        <div class="container">
            <a id="currency-table"></a>
            <div class="notifications">
                <div class="notifications__notification">
                    <div>

                        <svg xmlns="http://www.w3.org/2000/svg" width="16.853" height="18.383"
                             viewBox="0 0 16.853 18.383">
                            <g id="notification-line" style="fill: #505050;" transform="translate(-3.05 -1.775)">
                                <path id="Path_8035" data-name="Path 8035"
                                      d="M19.711,16.51a8.144,8.144,0,0,1-1.419-1.657,7.143,7.143,0,0,1-.763-2.72V9.339A6.113,6.113,0,0,0,12.189,3.26V2.53a.755.755,0,1,0-1.51,0v.741A6.113,6.113,0,0,0,5.4,9.339v2.794a7.143,7.143,0,0,1-.763,2.72,8.144,8.144,0,0,1-1.4,1.657.566.566,0,0,0-.192.424V17.7a.566.566,0,0,0,.566.566H19.337A.566.566,0,0,0,19.9,17.7v-.769A.566.566,0,0,0,19.711,16.51Zm-15.484.628a9.145,9.145,0,0,0,1.38-1.7,8.053,8.053,0,0,0,.933-3.308V9.339a4.943,4.943,0,1,1,9.88,0v2.794a8.053,8.053,0,0,0,.933,3.308,9.145,9.145,0,0,0,1.38,1.7Z"/>
                                <path id="Path_8036" data-name="Path 8036"
                                      d="M16.836,33.289A1.51,1.51,0,0,0,18.295,32H15.32A1.51,1.51,0,0,0,16.836,33.289Z"
                                      transform="translate(-5.331 -13.132)"/>
                            </g>
                        </svg>
                    </div>
                    <p>ارزش بازار شیبا اینو برای نخستین بار از دوج کوین پیشی گرفت.</p>

                    <a href="#">
                        ادامه
                        <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261"
                             viewBox="0 0 6.336 10.261">
                            <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                  d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                  transform="translate(19.221 -9) rotate(90)"/>
                        </svg>
                    </a>
                </div>

                <div class="notifications__notification">
                    <div>

                        <svg style="fill: #505050;" xmlns="http://www.w3.org/2000/svg" width="16.853" height="12.921"
                             viewBox="0 0 16.853 12.921">
                            <g id="newspaper" transform="translate(-1.125 -5.063)">
                                <path id="Path_8030" data-name="Path 8030"
                                      d="M3.653,5.063V16.158a.7.7,0,0,1-1.4,0V7.872H1.125v8.286a1.828,1.828,0,0,0,1.826,1.826h13.2a1.828,1.828,0,0,0,1.826-1.826V5.063Zm13.2,11.095a.7.7,0,0,1-.7.7H4.636a1.816,1.816,0,0,0,.14-.7V6.187H16.854Z"
                                      transform="translate(0 0)"/>
                                <path id="Path_8031" data-name="Path 8031"
                                      d="M15.744,9.563H10.688v5.618h5.056ZM14.62,14.057H11.812V10.687H14.62Z"
                                      transform="translate(-4.788 -2.253)"/>
                                <path id="Path_8032" data-name="Path 8032" d="M23.063,9.563h3.652v1.124H23.063Z"
                                      transform="translate(-10.983 -2.253)"/>
                                <path id="Path_8033" data-name="Path 8033" d="M23.063,14.063h3.652v1.124H23.063Z"
                                      transform="translate(-10.983 -4.506)"/>
                                <path id="Path_8034" data-name="Path 8034" d="M23.063,18.563h3.652v1.124H23.063Z"
                                      transform="translate(-10.983 -6.759)"/>
                            </g>
                        </svg>
                    </div>
                    <p>
                        @if(isset($new_post[0])) {{$new_post[0]['title']['rendered']}} @endif
                    </p>

                    <a @if(isset($new_post[0])) href="{{$new_post[0]['link']}}" targer="_blank" @else href=""
                       @endif class="index-more">
                        ادامه
                        <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261"
                             viewBox="0 0 6.336 10.261">
                            <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                  d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                  transform="translate(19.221 -9) rotate(90)"/>
                        </svg>
                    </a>
                </div>
            </div>

            <h2 class="text-center" style="color: #1d1f31;">بازار ارز</h2>
            <div class="row">
                <div class="col-12">
                    <table class="currency-table currency-table-3">
                        <tr>
                            <td class="name-currency-title">نام</td>
                            <td class="last-price-title">آخرین قیمت</td>
                            <td class="change-price-title">تغییر</td>
                            <td>بیشترین قیمت</td>
                        </tr>

                        {{--                        @foreach($currency->currency_to_rial(['btc','eth','usdt','trx','doge'],['latest','dayHigh','dayChange']) as $key=>$item)--}}
                        @foreach($currency as $key=>$item)
                            @php
                                $sing=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);
                            @endphp
                            <tr>
                                <td class="currency-title">
                                    <div class="currency-right-title currency-right-title-2">
                                        <img src="{{asset("theme/landing/images/currency/$key.png" )}}"
                                             alt="{{ $item['name_fa']}}"/>
                                        <h5>{{$item['name']}}</h5>
                                    </div>
                                </td>
                                <td class="currency-last-price">
                                    {{number_format(rial_to_unit($item['rls'],'rls'))}}
                                </td>
                                <td class="currency-present-change">
                                    @if(isset($item['1d']))
                                        <span
                                            class="currency-present-change {{(float)$item['1d']<0?'red':'green'}}-currency">
                           {{number_format((float)$value,2)}} {{$sing}}
                          </span>
                                    @else
                                        -
                                    @endif

                                </td>
                                <td class="currency-most-price">
                                    @if(isset($item['day_high']))
                                        {{number_format($item['dayHigh']/10)}}
                                    @else
                                        -
                                    @endif
                                </td>
                            </tr>

                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        <div class="all-currency-btn">
            <a href="{{route('landing.prices')}}" class="btn all-crypto-currency">
                مشاهده جدول کامل بازار
                <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261" viewBox="0 0 6.336 10.261">
                    <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                          d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                          transform="translate(19.221 -9) rotate(90)"/>
                </svg>
            </a>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="section section-two">
        <div class="container">
            <h2 class="app-title">
                کار کردن با روتیکس
                <span class="gold-title"> واقعا آسونه </span>
            </h2>

            <div class="d-flex auth-container">

                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-1.png')}}" alt="ثبت نام"/>
                    <span> ثبت نام در صرافی</span>
                    <p>
                        وارد کردن اطلاعات هویتی کاربر در مرحله اول
                    </p>
                </div>

                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-2.png')}}" alt="ثبت نام"/>
                    <span> احراز هویت </span>
                    <p>احراز اطلاعات وارد شده و تایید شماره تماس و ایمیل
                    </p>
                </div>

            </div>
        </div>
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
    <section>
        <div class="container">
            <h2 class="app-title app-title-2">
                بلاگ
                <span class="gold-title"> روتیکس </span>
            </h2>
            <div class="row row-padding">
                <div class="col-12">
                    <div class="blog-container">
                        <div class="right-blog top-blog blog-info">
                            <a href="#">
                                <img src="{{asset('theme/landing/images/mobile_title1.png')}}" alt="اتریوم"/>
                                <div class="currency-blog">
                                    <p>
                                        قیمت کاردانو همچنان میتازد؛ آیا منتظر ۳ دلار باشیم؟
                                    </p>
                                    <div class="calender">
                                        <p>‏20 مرداد 1400</p>

                                        <div>
                                            19
                                            <i class="far fa-comment"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="blog-container">
                        <div class="right-blog top-blog blog-info">
                            <a href="#">
                                <img src="{{asset('theme/landing/images/mobile_title2.png')}}" alt="اتریوم"/>
                                <div class="currency-blog">
                                    <p>
                                        ۷ اشتباه پرتکرار در صرافی بایننس و راه حل آنها
                                    </p>
                                    <div class="calender">
                                        <p>‏20 مرداد 1400</p>

                                        <div>
                                            19
                                            <i class="far fa-comment"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="blog-container">
                        <div class="right-blog top-blog blog-info">
                            <a href="#">
                                <img src="{{asset('theme/landing/images/mobile_title3.png')}}" alt="اتریوم"/>
                                <div class="currency-blog">
                                    <p>
                                        قانونگذاری ارزهای دیجیتال؛ ارزهای دیجیتال باید خودشان را با قانون وفق دهند یا
                                        برعکس؟
                                    </p>
                                    <div class="calender">
                                        <p>‏20 مرداد 1400</p>

                                        <div>
                                            19
                                            <i class="far fa-comment"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Three -->

    <!-- Start Section Four -->
    <section class="what-currency section-background">
        <div>
            <div class="currency-talk">
                <div class="information-ramz">
                    <div class="what-currency-info">
                        <h5>رمز ارز چیست؟</h5>
                        <div class="what-crypto-title">
                            <h2>بگذارید کمی از</h2>
                            <span>رمز ارز ها صحبت کنیم</span>
                        </div>
                        <p class="what-crypto-desc">
                            رمزارز (یا ارز رمزپایه) گونه ای پول دیجیتال است که در آن تولید واحد پول و تأیید اصالت تراکنش
                            پول با استفاده از الگوریتمهای رمزگذاری کنترل میشود و معمولاً به طور نامتمرکز (بدون وابستگی
                            به یک بانک مرکزی) کار میکند.
                        </p>

                        <a href="#" class="what-currency-more-btn light-blue-btn btn">
                            بیشتر بخوانید
                        </a>
                    </div>
                </div>

                <div class="owl-carts-container">
                    <div class="owl-carousel owl-theme owl-crypto-carts">
                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/bitcoin.png')}}" alt="پول دیجیتال"/>
                            <div class="crypto-cart-desc">
                                <h3>پول دیجیتالی</h3>
                                <p>
                                    ارزهایی هستند که به صورت الکترونیکی ذخیره و منتقل میشوند
                                </p>
                            </div>
                        </div>

                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/Icon.png')}}" alt="همیشه در دسترس"/>
                            <div class="crypto-cart-desc">
                                <h3>هیمشه در دسترس</h3>
                                <p>
                                    پشتیبانی قوی و 24 ساعته روتیکس برای تجربه بهتر در خرید و فروش
                                </p>
                            </div>
                        </div>

                        <div class="crypto-cart">
                            <img src="{{asset('theme/landing/images/credit-card.png')}}" alt="امنیت تراکنش"/>
                            <div class="crypto-cart-desc">
                                <h3>امنیت تراکنش</h3>
                                <p>
                                    حفظ تمامی اطلاعات کاربران و اطلاعات تراکنش ها
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="feature">
        <div>
            <h2 class="feature-title">
                ویژگی های
                <span class="gold-title gold-feature"> روتیکس </span>
            </h2>

            <div class="owl-carousel owl-theme owl-feature">
                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-1.jpg')}}" alt="ثبت نام سریع و راحت"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>راحت و سریع در ثبت نام</h2>
                            <span>و احراز هویت</span>
                        </div>

                        <div>
                            <p>
                                در روتیکس به سادگی با پر کردن دو فرم تایید هویتی و وارد کردن اطلاعات خود به سادگی و به
                                سرعت در یکی از بهترین صرافی ها ثبت نام کنید
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    بیشتر بخوانید
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261"
                                         viewBox="0 0 6.336 10.261">
                                        <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                              d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                              transform="translate(19.221 -9) rotate(90)"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-2.png')}}" alt="امنیت و حفظ اطلاعات کاربران"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>امنیت و حفظ اطلاعات </h2>
                            <span>کاربران</span>
                        </div>

                        <div>
                            <p>
                                روتیکس خود را موظف میداند که امنیت اطلاعات کاربران خود را تامین نموده تا کاربران با
                                خیالی آسوده خرید و فروش کنند
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    بیشتر بخوانید
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261"
                                         viewBox="0 0 6.336 10.261">
                                        <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                              d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                              transform="translate(19.221 -9) rotate(90)"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main-feature">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-3.png')}}" alt="خرید و فروش آسان"/>
                    </div>

                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>خرید و فروش </h2>
                            <span>آسان</span>
                        </div>

                        <div>
                            <p>
                                روتیکس با امکان رابط کاربری آسان و سریع ، یک تجربه متفاوت در زمینه خرید و فروش ارز های
                                دیجیتال برای کاربران خود ایجاد میکند
                            </p>

                            <div class="all-currency-btn">
                                <a href="#" class="btn all-crypto-currency">
                                    بیشتر بخوانید
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6.336" height="10.261"
                                         viewBox="0 0 6.336 10.261">
                                        <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                              d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                              transform="translate(19.221 -9) rotate(90)"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End Section Five -->

    <!-- Start Section Six -->
    <section class="mobile-app section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6" style="order: 1;">
                    <div class="what-currency-info">
                        <h5>اپلیکیشن موبایل رو فراموش نکنید</h5>
                        <div class="what-crypto-title">
                            <h2>همیشه و همه جا بازار رو چک کن</h2>
                            <span>با اپلیکیشن موبایل روتیکس</span>
                        </div>
                        <p class="what-crypto-desc">
                            پنل روتیکس در تلفن های هوشمند هم قابل استفاده است . به راحتی قابل دسترس است و با رابط کاربری
                            پیشرفته و آسان خود ، تجربه کار با صرافی در هر لحظه و هر مکان را با استفاده از تلفن هوشمند
                            برای کاربران خود ایجاد میکند.
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/mobile-1.png')}}" alt="برنامه موبایل"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Six -->

    <!-- Start Section Seven -->
    <section class="customers">
        <div>
            <h2 class="customer-title">آنچه مشتریان ما می گویند</h2>
            <div class="owl-carousel owl-theme customer-owl">
                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>مریم احمدی</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-4.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                یکی از بهترین صرافی هایی که دیدم امنیت بالا و سرعت سایت باعث شده من سراغ صرافی های دیگه
                                نرم
                            </p>
                        </div>
                    </div>
                </div>

                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>سینا اکبری</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-5.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                قابل اعتماد ، پشتیبانی سریع ، امیدوارم روز به روز بهترو قوی تر بشید من به چند تا از
                                دوستام معرفی کردم و اونا هم راضی بودن
                            </p>
                        </div>
                    </div>
                </div>

                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>بیتا رحیمی</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-6.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                اولین چیزی که منو جذب این سایت کرد طراحی فوق العاده ش بود خیلی راضی م از این صرافی
                            </p>
                        </div>
                    </div>
                </div>

                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>زهرا ضیایی</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-7.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                نکته مثبت سایت اینه که مهم ترین ارز ها رو داره واقعا منو از داشتن صرافی های دیگه بی نیاز
                                کرده
                            </p>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>محمد صادقی </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-8.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                تجربه خوبی داشتم تو این صرافی به شما هم پیشنهاد میکنم استفاده کنید پشیمون نمیشید
                            </p>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div class="d-flex justify-between">
                            <div class="customer-info">
                                <h5>پدرام مومنی</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <div class="customer-img">
                                <img src="{{asset('theme/landing/images/customer-9.png')}}" alt="مشتری"/>
                            </div>
                        </div>

                        <div>
                            <p>
                                اولین چیزی که منو جذب این سایت کرد هر اطلاعاتی از ارز ها بخوام و این سایت داره و
                                منو از داشتن هزاران کانال و سایت دیگه راحت کرده
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Section Seven -->

    <!-- Start Section Eight -->
    <section>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="res-form-container">
                        <div>
                            <h2>با ما در تماس باشید</h2>
                            <p class="top-form-desc">
                                در پیشرفت روتیکس با ما همراه باشید ، نظرات و انتقادات خود را در تجربه کار با روتیکس با
                                ما به
                                اشتراک بگذارید. تیم ما برای هرچه بهتر شدن روتیکس مشتاق نظرات شما هستند

                            </p>
                        </div>

                        <div class="form-img">
                            <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="فرم"/>
                        </div>

                        <form action="#" class="res-form">
                            <input type="text" placeholder="نام و نام خانوادگی...">
                            <input type="email" placeholder="آدرس ایمیل...">
                            <input type="text" placeholder="آدرس سایت...">
                            <textarea placeholder="متن پیام..."></textarea>
                            <button type="submit" class="res-form__btn btn light-blue-btn">
                                ارسال
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Eight -->
@endsection
@section('script')
    <script>
        $('#usdt').on('click', function () {
            $('.rial-table').removeClass('active');
            $('.usdt-table').addClass('active');
        });

        $('#rial').on('click', function () {
            $('.rial-table').addClass('active');
            $('.usdt-table').removeClass('active');
        })
    </script>
@endsection
