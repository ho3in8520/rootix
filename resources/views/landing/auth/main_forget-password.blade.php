@mobile
@include('landing.auth.mobile.forget-password')
@endmobile

@tablet
@include('landing.auth.mobile.forget-password')
@endtablet

@desktop
@include('landing.auth.desktop.forget-password')
@enddesktop
