{{--@extends('templates.landing_page.auth.master_page')--}}
{{--@section('style')--}}
    <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8"/>
    <title>Login - Bayya Bitcoin Crypto Currency Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <!-- Template CSS Files -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="{{ asset("theme/landing/css-old/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("theme/landing/css-old/magnific-popup.css") }}">
    <link rel="stylesheet" href="{{ asset("theme/landing/css-old/select2.min.css") }}">
    <link rel="stylesheet" href="{{ asset("theme/landing/css-old/style.css") }}">
    <link rel="stylesheet" href="{{ asset("theme/landing/css-old/skins/orange.css") }}">

    <!-- Live Style Switcher - demo only -->
    <link rel="alternate stylesheet" type="text/css" title="orange"
          href="{{ asset("theme/landing/css-old/skins/orange.css") }}"/>
    <link rel="alternate stylesheet" type="text/css" title="green"
          href="{{ asset("theme/landing/css-old/skins/green.css") }}"/>
    <link rel="alternate stylesheet" type="text/css" title="blue"
          href="{{ asset("theme/landing/css-old/skins/blue.css") }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset("theme/landing/css-old/styleswitcher.css") }}"/>

    <!-- Template JS Files -->

    <script src="{{ asset("theme/landing/js/modernizr.js") }}"></script>
    {!! htmlScriptTagJsApi() !!}
    <style>
        i {
            margin-top: 14px;
            margin-right: 5px;
            position: absolute;
            cursor: pointer;
        }
    </style>
    {{--    @yield('style')--}}
    <script type="text/javascript">window.$crisp = [];
        window.CRISP_WEBSITE_ID = "0d388766-bc01-4ba9-bb6b-b79f274e0180";
        (function () {
            d = document;
            s = d.createElement("script");
            s.src = "https://client.crisp.chat/l.js";
            s.async = 1;
            d.getElementsByTagName("head")[0].appendChild(s);
        })();</script>

</head>
{{--    <style>--}}
{{--        i {--}}
{{--            margin-top: 14px;--}}
{{--            margin-right: 5px;--}}
{{--            position: absolute;--}}
{{--            cursor: pointer;--}}
{{--        }--}}
{{--    </style>--}}
{{--@endsection--}}
{{--@section('content')--}}
<body class="auth-page">
<!-- SVG Preloader Starts -->
<div id="preloader">
    <div id="preloader-content">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
             x="0px" y="0px" width="150px" height="150px" viewBox="100 100 400 400" xml:space="preserve">
                <filter id="dropshadow" height="130%">
                    <feGaussianBlur in="SourceAlpha" stdDeviation="5"/>
                    <feOffset dx="0" dy="0" result="offsetblur"/>
                    <feFlood flood-color="red"/>
                    <feComposite in2="offsetblur" operator="in"/>
                    <feMerge>
                        <feMergeNode/>
                        <feMergeNode in="SourceGraphic"/>
                    </feMerge>
                </filter>
            <path class="path" fill="#000000" d="M446.089,261.45c6.135-41.001-25.084-63.033-67.769-77.735l13.844-55.532l-33.801-8.424l-13.48,54.068
                    c-8.896-2.217-18.015-4.304-27.091-6.371l13.568-54.429l-33.776-8.424l-13.861,55.521c-7.354-1.676-14.575-3.328-21.587-5.073
                    l0.034-0.171l-46.617-11.64l-8.993,36.102c0,0,25.08,5.746,24.549,6.105c13.689,3.42,16.159,12.478,15.75,19.658L208.93,357.23
                    c-1.675,4.158-5.925,10.401-15.494,8.031c0.338,0.485-24.579-6.134-24.579-6.134l-9.631,40.468l36.843,9.188
                    c8.178,2.051,16.209,4.19,24.098,6.217l-13.978,56.17l33.764,8.424l13.852-55.571c9.235,2.499,18.186,4.813,26.948,6.995
                    l-13.802,55.309l33.801,8.424l13.994-56.061c57.648,10.902,100.998,6.502,119.237-45.627c14.705-41.979-0.731-66.193-31.06-81.984
                    C425.008,305.984,441.655,291.455,446.089,261.45z M368.859,369.754c-10.455,41.983-81.128,19.285-104.052,13.589l18.562-74.404
                    C306.28,314.65,379.774,325.975,368.859,369.754z M379.302,260.846c-9.527,38.187-68.358,18.781-87.442,14.023l16.828-67.489
                    C327.767,212.14,389.234,221.02,379.302,260.846z"/>
            </svg>
    </div>
</div>
<!-- SVG Preloader Ends -->
<!-- Live Style Switcher Starts - demo only -->
<div id="switcher" class="">
    <div class="content-switcher">
        <h4>سبک قالب</h4>
        <ul>
            <li>
                <a id="orange-css" href="#" title="orange" class="color"><img
                        src="images/styleswitcher/colors/orange.png" alt="" width="30" height="30"/></a>
            </li>
            <li>
                <a id="green-css" href="#" title="green" class="color"><img src="images/styleswitcher/colors/green.png"
                                                                            alt="" width="30" height="30"/></a>
            </li>
            <li>
                <a id="blue-css" href="#" title="blue" class="color"><img src="images/styleswitcher/colors/blue.png"
                                                                          alt="" width="30" height="30"/></a>
            </li>
        </ul>

        <p>رنگ قالب</p>

        <label><input class="dark_switch" type="radio" name="color_style" id="is_dark" value="dark" checked="checked"/>
            تیره</label>
        <label><input class="dark_switch" type="radio" name="color_style" id="is_light" value="light"/> روشن</label>

        <hr/>

        <p>طرح سبک</p>
        <label><input class="boxed_switch" type="radio" name="layout_style" id="is_wide" value="wide"
                      checked="checked"/> وسیع</label>
        <label><input class="boxed_switch" type="radio" name="layout_style" id="is_boxed" value="boxed"/> جعبه</label>

        <hr/>

        <a href="https://www.rtl-theme.com/user-profile/davod_taheri/" class="custom-button purchase">خرید</a>
        <div id="hideSwitcher">&times;</div>

    </div>
</div>
{{--<div id="showSwitcher" class="styleSecondColor"><i class="fa fa-cog fa-spin"></i></div>--}}

<div class="wrapper">
    <div class="container-fluid user-auth">
        <div class="hidden-xs col-sm-4 col-md-4 col-lg-4">
            <!-- Logo Starts -->
            <a class="logo" href="index.html">
                <img id="logo-user" class="img-responsive" src="{{ asset("theme/landing/images-old/logo-dark.png") }}"
                     alt="logo">
            </a>
            <!-- Logo Ends -->
            <!-- Slider Starts -->
            <div id="carousel-testimonials" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Indicators Starts -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                    <li data-target="#carousel-testimonials" data-slide-to="2"></li>
                </ol>
                <!-- Indicators Ends -->
                <!-- Carousel Inner Starts -->
                <div class="carousel-inner">
                    <!-- Carousel Item Starts -->
                    <div class="item active item-1">
                        <div>
                            <blockquote>
                                <p>این یک برنامه واقع بینانه است برای هر کسی که به دنبال سرمایه گذاری در سایت است.
                                    مرتباً به من پرداخت می شود ، کار خوب را ادامه دهید!</p>
                                <footer>رشت ,<span>معصومه تاجری</span></footer>
                            </blockquote>
                        </div>
                    </div>
                    <!-- Carousel Item Ends -->
                    <!-- Carousel Item Starts -->
                    <div class="item item-2">
                        <div>
                            <blockquote>
                                <p>بیت کوین در 7 روز دو برابر شد. نباید انتظار بیشتری داشت. خدمات عالی مشتری!</p>
                                <footer>سمنان ,<span>حمید حمیدی</span></footer>
                            </blockquote>
                        </div>
                    </div>
                    <!-- Carousel Item Ends -->
                    <!-- Carousel Item Starts -->
                    <div class="item item-3">
                        <div>
                            <blockquote>
                                <p>من و خانواده من می خواهیم از شما برای کمک به ما در یافتن فرصتی عالی برای کسب درآمد
                                    آنلاین تشکر کنیم. از نحوه کارها بسیار خوشحالم!</p>
                                <footer>ساری ,<span>مریم احمدی</span></footer>
                            </blockquote>
                        </div>
                    </div>
                    <!-- Carousel Item Ends -->
                </div>
                <!-- Carousel Inner Ends -->
            </div>
            <!-- Slider Ends -->
        </div>
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <!-- Logo Starts -->
            <a class="visible-xs" href="index.html">
                <img id="logo" class="img-responsive mobile-logo" width="200"
                     src="{{ asset('theme/landing/images/logo-dark.png') }}" alt="logo">
            </a>
            <!-- Logo Ends -->
            @include('general.flash_message')
            <div class="form-container">
                <div>
                    <div class="row text-center">
                        <h2 class="title-head hidden-xs">{{__('attributes.header3')}}
                            <span>{{__('attributes.header4')}}</span></h2>
                        <p class="info-form">{{__('attributes.header')}}</p>
                    </div>
                    <form method="post" action="{{ route('register.store') }}">
                    @csrf
                    <!-- Input Field Starts -->
                        <div class="form-group">
                            <input class="form-control @error('email')input-error-border @enderror" name="email"
                                   id="email" placeholder="{{__('attributes.email')}}" type="email"
                                   required>
                            @error('email') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <i class="fa fa-eye show-pass"></i>
                            <input class="form-control @error('password')input-error-border @enderror"
                                   style="padding-right: 30px"
                                   name="password"
                                   id="password" placeholder="{{__('attributes.password')}}"
                                   type="password" required>
                            <span style="font-size: 13px"
                                  class="">رمز عبور باید شامل حروف بزرگ، حروف کوچیک و عدد باشد</span> <br>
                            @error('password') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <i class="fa fa-eye show-pass"></i>
                            <input class="form-control @error('password_confirmation')input-error-border @enderror"
                                   style="padding-right: 30px"
                                   name="password_confirmation" id="password_confirmation"
                                   placeholder="{{__('attributes.repeatPassword')}}"
                                   type="password" required>
                            @error('password_confirmation') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            @php($referral_code= isset($_GET['referral_code']) && $_GET['referral_code']?$_GET['referral_code']:'')
                            <label>کد معرف</label>
                            @if (empty($referral_code))
                                <input type="text"
                                       class="form-control"
                                       style="padding-right: 30px;"
                                       name="referral_code"
                                       placeholder="کد معرف">
                            @else
                                <input type="text"
                                       class="form-control"
                                       style="padding-right: 30px; color: #000"
                                       disabled
                                       value="{{ $referral_code }}">
                                <input type="hidden" name="referral_code" value="{{ $referral_code }}">
                            @endif
                            @error('referral_code') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        {{--<div class="form-group">
                            <select class="form-control @error('country')input-error-border @enderror" name="country">
                                <option value="">{{__('attributes.selectCountry')}}</option>
                                @foreach($countries as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('country') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>--}}
                        <div class="form-group text-center">
                            {!! htmlFormSnippet([
                    "theme" => "light",
                    "size" => "normal",
                    "tabindex" => "3",
                    "callback" => "callbackFunction",
                    "expired-callback" => "expiredCallbackFunction",
                    "error-callback" => "errorCallbackFunction",
                    ]) !!}
                            @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary"
                                    type="submit">{{__('attributes.submitRegister')}}
                            </button>
                            <p class="text-center"><a
                                    href="{{ route('login.form') }}">{{__('attributes.href-checkLogin')}}</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Copyright Text Starts -->
        {{--<p class="text-center copyright-text">کپی رایت © 2020 Bayya همه حقوق محفوظ است | راستچین شده توسط <a
                href="#" target="_blank">شاهپوری</a></p>--}}
        <!-- Copyright Text Ends -->
        </div>
    </div>
{{--    @include('templates.landing_page.layout.footer')--}}
    <script src="{{ asset("theme/landing/js/jquery-2.2.4.min.js") }}"></script>
    <script src="{{ asset("theme/landing/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset("theme/landing/js/select2.min.js") }}"></script>
    <script src="{{ asset("theme/landing/js/jquery.magnific-popup.min.js") }}"></script>
    <script src="{{ asset("theme/landing/js/custom.js") }}"></script>

    {{--<!-- Live Style Switcher JS File - only demo -->--}}
    <script src="{{ asset("theme/landing/js/styleswitcher.js") }}"></script>
    <script src="{{ asset('general/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('general/js/func.js') }}"></script>
</div>
<script>
    $(document).ready(function () {
        $(".show-pass").click(function () {
            var type = $(this).closest(".form-group").find("input").attr('type');
            if (type == 'text') {
                $(this).closest(".form-group").find("input").attr('type', 'password')
            } else {
                $(this).closest(".form-group").find("input").attr('type', 'text')
            }
        })
    })
</script>
</body>

</html>


    <div>
        <div class="row text-center">
            <h2 class="title-head hidden-xs">{{__('attributes.header3')}} <span>{{__('attributes.header4')}}</span></h2>
            <p class="info-form">{{__('attributes.header')}}</p>
        </div>
        <form method="post" action="{{ route('register.store') }}">
        @csrf
        <!-- Input Field Starts -->
            <div class="form-group">
                <input class="form-control @error('email')input-error-border @enderror" name="email"
                       id="email" placeholder="{{__('attributes.email')}}" type="email"
                       required>
                @error('email') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <i class="fa fa-eye show-pass"></i>
                <input class="form-control @error('password')input-error-border @enderror"
                       style="padding-right: 30px"
                       name="password"
                       id="password" placeholder="{{__('attributes.password')}}"
                       type="password" required>
                <span style="font-size: 13px" class="">رمز عبور باید شامل حروف بزرگ، حروف کوچیک و عدد باشد</span> <br>
                @error('password') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <i class="fa fa-eye show-pass"></i>
                <input class="form-control @error('password_confirmation')input-error-border @enderror"
                       style="padding-right: 30px"
                       name="password_confirmation" id="password_confirmation"
                       placeholder="{{__('attributes.repeatPassword')}}"
                       type="password" required>
                @error('password_confirmation') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                @php($referral_code= isset($_GET['referral_code']) && $_GET['referral_code']?$_GET['referral_code']:'')
                <label>کد معرف</label>
                @if (empty($referral_code))
                    <input type="text"
                           class="form-control"
                           style="padding-right: 30px;"
                           name="referral_code"
                           placeholder="کد معرف">
                @else
                    <input type="text"
                           class="form-control"
                           style="padding-right: 30px; color: #000"
                           disabled
                           value="{{ $referral_code }}">
                    <input type="hidden" name="referral_code" value="{{ $referral_code }}">
                @endif
                @error('referral_code') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <select class="form-control @error('country')input-error-border @enderror" name="country">
                    <option value="">{{__('attributes.selectCountry')}}</option>
                    @foreach($countries as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('country') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group text-center">
                {!! htmlFormSnippet([
        "theme" => "light",
        "size" => "normal",
        "tabindex" => "3",
        "callback" => "callbackFunction",
        "expired-callback" => "expiredCallbackFunction",
        "error-callback" => "errorCallbackFunction",
        ]) !!}
                @error('g-recaptcha-response') <span class="text-danger">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <button class="btn btn-primary"
                        type="submit">{{__('attributes.submitRegister')}}
                </button>
                <p class="text-center"><a href="{{ route('login.form') }}">{{__('attributes.href-checkLogin')}}</a>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $(".show-pass").click(function () {
                var type = $(this).closest(".form-group").find("input").attr('type');
                if (type == 'text') {
                    $(this).closest(".form-group").find("input").attr('type', 'password')
                } else {
                    $(this).closest(".form-group").find("input").attr('type', 'text')
                }
            })
        })
    </script>
@endsection
