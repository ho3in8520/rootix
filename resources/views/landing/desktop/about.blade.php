@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    درباره روتیکس
@endsection
@section('header')
    <header class="about-header">
        <div class="container">
            <nav>
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>

        </div>
        <div class="d-flex items-center header-row">

            <div class="about-header-image">
                <img src="{{asset('theme/landing/images/about-us/about-header-image.png')}}" alt="درباره ما">
            </div>

            <div class="about-header-info">
                <h5>درباره روتیکس</h5>
                <h1>شرکتی خلاق و کاربردی</h1>
            </div>
        </div>

        <div class="go-down-container">
            <a href="#currency-table" class="go-down about-go-down-container">
                <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن" />
            </a>
        </div>
    </header>
@endsection
@section('content')
    <!-- Start Section One -->
    <section class="history">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="company-history-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-1.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="history-descriptions">
                        <h3>تاریخچه شرکت</h3>
                        <p>واژه روتیکس متشکل از دو لغت روتی به معنای ریشه دار و ایکس کوتاه شده لغت اکسچنچ به معنای صرافی است، صرافی ریشه دار نشات گرفته از همت و غیرت جوانان این مرز و بوم با هدف سهول   انجام معاملات ارز دیجیتال برای هموطنان عزیز می باشد.
                            هدف ما فراهم کردن بستری امن،سریع و راحت به جهت خرید و فروش ارز های دیجیتال شاخصی همچون بیت کوین ، تتر، ترون ، بیت تورنت و... در کمترین زمان و امنیتی بالا بین خریدار و فروشنده است.
                            هدف شرکت آن است که با بهره گیری از تجربه و دانش به روز نیروهای خود در زمینه ارز دیجیتال،با توجه به مشکلات عدیده برای ایرانیان از جمله دسترسی سخت به بازار های خارجی،بعنوان واسطه ای میان خریداران و فروشندگان ، امنیت معاملات و وصول دارایی های مبادله شده را تضمین نماید.
                            اکنون افراد میتوانند در بستر حرفه ای که روتیکس(rootix) فراهم آورده است،قیمت بیت کوین را به صورت آنلاین از پلتفرم خرید و فروش مشاهده کنند و در کوتاه ترین زمان ممکن و در چند قدم بیت کوین بخرند یا بیت کوین خود را
                            بفروشند.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="about-currency">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-5">
                    <div class="about-currency-descriptions">
                        <h5>درباره ارز دیجیتال</h5>
                        <h2>
                            ارز دیجیتال
                            <br>
                            همیشه و همه جا به صرفه
                        </h2>
                        <h3>ارز دیجیتال چطوری به کسب و کارت کمک می کنه؟</h3>
                        <p>ارزهای دیجیتال یک سری ابزار برای مبادله های شخصی بین افراد هستند و توسط هیچ دولتی مدیریت نمیشوند از این رو میتوان تورم را کنترل کرد .یکی از نکات خوب در رابطه با ارزهای دیجیتال، نمایان نشدن هویت افراد است که در این بازار مالی جریان ها به فرد یا افراد خاصی نسبت داده نمیشود و پنهان میماند .
                            از دیگر ویژگی های آن میتوان به فراگیر بودن آن اشاره کرد که هیچ محدودیت ندارد و هر فردی درهرجای دنیا میتواند از آن استفاده کند.
                            حذف واسطه ها از این بین هم نکته مثبتی است که هر فرد بدون نیاز به واسطه یا شخص سوم میتواند معامله خود را انجام دهد .
                            تمام تراکنش هایی که ثبت میشوند به صورت زنجیرهایی در قالب بلاک به بلاکچین  افزوده میشود و همینطور  تمام تراکنش ها باید از سمت ماینرهایی که در این شبکه وجود دارند تایید شوند. این را هم در نظر داشته باشید که هیچ تراکنشی قابل حذف یا تغییرنیست .
                            برای ذخیره این ارزهای دیجیتال یک کیف پول نیاز دارید که انواع مختلفی دارند و در قسمت مقالات به آنها میپردازیم. </p>
                        <a href="{{route('login.form')}}" class="about-btn about-currency-btn">
                            شروع خرید و فروش
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-7">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/about-us/sec-2.png')}}" alt="تاریخچه شرکت">
                    </div>
                </div>

            </div>
        </div>

        <img src="{{asset('theme/landing/images/about-us/left-background-sec-3.png')}}" alt="بک گراند راست" class="back-sec-3 left-back">
        <img src="{{asset('theme/landing/images/about-us/right-background-sec-3.png')}}" alt="بک گراند چپ" class="back-sec-3 right-back">
    </section>
    <!-- End Section Two -->

    <!-- Start Section Three -->
    <section class="rootix-team">
        <div class="container-secondary">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="about-currency-img">
                        <img src="{{asset('theme/landing/images/team.png')}}" alt="team-work">
                    </div>
                </div>
{{--                <div class="col-12 col-md-6">--}}
{{--                    <div class="team-container">--}}
{{--                        <div class="three-team-container">--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-1.png')}}" alt="عضو اول">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-2.png')}}" alt="عضو دوم">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-3.png')}}" alt="عضو سوم">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="three-team-container">--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-4.png')}}" alt="عضو چهارم">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-5.png')}}" alt="عضو پنجم">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                            <div class="team">--}}
{{--                                <img src="{{asset('theme/landing/images/about-us/user-old-team-6.png')}}" alt="عضو ششم">--}}
{{--                                <h5>name name</h5>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

                <div class="col-12 col-md-6">
                    <div class="team-info">
                        <h2>اعضا تیم روتیکس</h2>
                        <p>تیم روتیکس از گرد هم آمدن 10متخصص برتر و با تجربه بالا در زمینه توسعه وب و نرم افزار در قالب تیم توسعه و برنامه نویسی
                            ،5 متخصص و فعال در حوزه کریپتو که در زمینه مقاله های آموزشی،تحلیل و خبرهای برتر روز تا امروز به بالا بردن سطح آگاهی کاربران این حوزه اهتمام ورزیده اند،شکل گرفته است.</p>

                        <a href="#" class="about-btn">
                            به ما بپیوندید!
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Three -->


    <!-- Start Section Four -->
    <section class="rootix-company counter-row-2">
        <div class="container-secondary">
            <div class="company-title">
                <h2>یک شرکت قوی در زمینه ارز دیجیتال</h2>
                <p>روتیکس یک شرکت قوی در زمینه ارز های دیجیتال با هدف ایجاد یک بستر امن برای آینده مطمئن است.امنیت کامل در یک سیستم غیر متمرکز به تبادل مالی خود توسط ارزهای دیجیتال و یک سیستم غیر متمرکز بپردازد که به مواردی از آنها در زیر اشاره میکنیم:1.امکان خرید و فروش مستقیم ارز بدون واسطه بر بستر بلاکچین ها 2. ایجاد بستری برای تبادلات مالی برای تجار محترم واردات و صادرات راحت ترین و امن ترین تبادل مالی خود را یا طرح های معاملاتی خو در کشور های مختلف دنیا انجام دهند.</p>
           <p>برنامه های آتی:ایجاد امکان خرید و فروش در سایتها فروشگاهی توسط ارزاهای دیجیتال بصورت غیرمتمرکز</p>
            </div>
            <div class="row row-sec-5-four">
                <div class="col-12">
                    <div>
                        <button class="about-video">
                            <img src="{{asset('theme/landing/images/about-us/sec-four.png')}}" alt="فیلم">
                            <video src="" controls class="self-video"></video>
                        </button>
                        <hr class="rootix-company-line">
                    </div>
                </div>
            </div>
            <div class="row counter-row-1">
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>مشتری های پنل</h3>
                        <h5 id="about-customers">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های روزانه</h3>
                        <h5 id="daily-transaction">۲۵۶,000</h5>
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-md-4">
                    <div class="about-counter">
                        <h3>تراکنش های ماهیانه</h3>
                        <h5 id="monthly-transaction">۲۵۶,000</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="team-contact">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="team-contact__form-container">
                        <div>
                            <h2 class="team-contact__title">با تیم ما در ارتباط باشید</h2>
                            <p class="team-contact__desc">هدف تیم ما ارائه بهترین خدمات برای کاربران سایت روتیکس است.خوشحال می شویم انتقادات و نظرات خود را برای ما به اشتراک بگذارید.</p>
                        </div>

                        <form action="#" class="team-contact__form">
                            <div class="team-contact__inputs">
                                <input type="email" placeholder="نام خود را وارد کنید..." class="team-contact__input ">
                                <input type="text" placeholder="ایمیل خود را وارد کنید..." class="team-contact__input ">
                            </div>

                            <textarea placeholder="پیام خود را بنویسید..." class="team-contact__textarea"></textarea>

                            <button type="submit" class="team-contact__btn about-btn " disabled>ارسال</button>
                        </form>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="about-form__image">
                        <img src="{{asset('theme/landing/images/about-us/Ai.png')}}" class="form-image" alt="ارتباط با ما">
                    </div>
                </div>
            </div>
        </div>
        <img src="{{asset('theme/landing/images/about-us/back-sec-5.png')}}" alt="بک گراند سکشن آخر" class="back-sec-five">
    </section>
    <!-- End Section Five -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/about-counter.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/about-video-playr.js')}}"></script>
@endsection
