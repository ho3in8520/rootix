@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    روتیکس-صرافی آنلاین ارز دیجیتال
@endsection
@section('style')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js" type="text/javascript"></script>
    <style>
        .currency-chart{
            width: 95px;
            height: 57px;
            margin: 0 auto;
        }
    </style>
@endsection
@section('header')
    <!-- Start Header -->
    <header class="header">
        <div class="container">
            <nav class="nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>
        <div class="notifications index-notifications">
            <div class="container">
                <div class="index-notifications-self">
                    <div class="notifications__notification">
                        <div class="d-flex items-center">
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="25.853" height="30.383"
                                     viewBox="0 0 16.853 18.383">
                                    <g id="notification-line" style="fill: #fff;" transform="translate(-3.05 -1.775)">
                                        <path id="Path_8035" data-name="Path 8035"
                                              d="M19.711,16.51a8.144,8.144,0,0,1-1.419-1.657,7.143,7.143,0,0,1-.763-2.72V9.339A6.113,6.113,0,0,0,12.189,3.26V2.53a.755.755,0,1,0-1.51,0v.741A6.113,6.113,0,0,0,5.4,9.339v2.794a7.143,7.143,0,0,1-.763,2.72,8.144,8.144,0,0,1-1.4,1.657.566.566,0,0,0-.192.424V17.7a.566.566,0,0,0,.566.566H19.337A.566.566,0,0,0,19.9,17.7v-.769A.566.566,0,0,0,19.711,16.51Zm-15.484.628a9.145,9.145,0,0,0,1.38-1.7,8.053,8.053,0,0,0,.933-3.308V9.339a4.943,4.943,0,1,1,9.88,0v2.794a8.053,8.053,0,0,0,.933,3.308,9.145,9.145,0,0,0,1.38,1.7Z"/>
                                        <path id="Path_8036" data-name="Path 8036"
                                              d="M16.836,33.289A1.51,1.51,0,0,0,18.295,32H15.32A1.51,1.51,0,0,0,16.836,33.289Z"
                                              transform="translate(-5.331 -13.132)"/>
                                    </g>
                                </svg>
                            </div>

                            <div class="notification-text-container notification-text-container-right">
                                <p class="notification-text notification-text-right">
                                    ارزش بازار شیبا اینو برای نخستین بار از دوج کوین پیشی گرفت.
                                </p>
                            </div>
                        </div>
                        <a href="#" class="index-more">
                            ادامه
                            <svg xmlns="http://www.w3.org/2000/svg" style="fill: #fff;" width="6.336" height="10.261"
                                 viewBox="0 0 6.336 10.261">
                                <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                      d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                      transform="translate(19.221 -9) rotate(90)"/>
                            </svg>
                        </a>
                    </div>

                    <div class="notifications__notification">
                        <div class="d-flex items-center">
                            <div>

                                <svg style="fill: #fff;" xmlns="http://www.w3.org/2000/svg" width="23.853"
                                     height="24.921"
                                     viewBox="0 0 16.853 12.921">
                                    <g id="newspaper" transform="translate(-1.125 -5.063)">
                                        <path id="Path_8030" data-name="Path 8030"
                                              d="M3.653,5.063V16.158a.7.7,0,0,1-1.4,0V7.872H1.125v8.286a1.828,1.828,0,0,0,1.826,1.826h13.2a1.828,1.828,0,0,0,1.826-1.826V5.063Zm13.2,11.095a.7.7,0,0,1-.7.7H4.636a1.816,1.816,0,0,0,.14-.7V6.187H16.854Z"
                                              transform="translate(0 0)"/>
                                        <path id="Path_8031" data-name="Path 8031"
                                              d="M15.744,9.563H10.688v5.618h5.056ZM14.62,14.057H11.812V10.687H14.62Z"
                                              transform="translate(-4.788 -2.253)"/>
                                        <path id="Path_8032" data-name="Path 8032" d="M23.063,9.563h3.652v1.124H23.063Z"
                                              transform="translate(-10.983 -2.253)"/>
                                        <path id="Path_8033" data-name="Path 8033"
                                              d="M23.063,14.063h3.652v1.124H23.063Z"
                                              transform="translate(-10.983 -4.506)"/>
                                        <path id="Path_8034" data-name="Path 8034"
                                              d="M23.063,18.563h3.652v1.124H23.063Z"
                                              transform="translate(-10.983 -6.759)"/>
                                    </g>
                                </svg>
                            </div>

                            <div class="notification-text-container notification-text-container-left">
                                <p class="notification-text notification-text-left">
                                    @if(isset($new_post[0])) {{$new_post[0]['title']['rendered']}} @endif
                                </p>
                            </div>
                        </div>
                        <a @if(isset($new_post[0])) href="{{$new_post[0]['link']}}" targer="_blank" @else href=""
                           @endif class="index-more">
                            ادامه
                            <svg xmlns="http://www.w3.org/2000/svg" style="fill: #fff;" width="6.336" height="10.261"
                                 viewBox="0 0 6.336 10.261">
                                <path id="Icon_material-expand-more" data-name="Icon material-expand-more"
                                      d="M18.055,12.885,14.13,16.8l-3.925-3.916L9,14.091l5.13,5.13,5.13-5.13Z"
                                      transform="translate(19.221 -9) rotate(90)"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row header-row">
                <div class="col-12 col-md-6">
                    <div class="header-info">
                        <div>
                            <h1>
                                راه آسان و ایمن برای بدست
                                <h2>آوردن بیت کوین</h2>
                            </h1>
                        </div>
                        <ul>
                            <li>امنیت بالا را با ما تجربه کنید.</li>
                            <li> ولت اختصاصی خود را داشته باشید.</li>
                            <li> با کمترین کارمزد ترید کنید.</li>
                            <li> تنوع در معاملات با سرعت بالا فقط در روتیکس</li>
                        </ul>

                        <div class="header-btns">
                            <a href="{{route('register.form')}}" class="btn light-blue-btn animation-btn">
                                ثبت نام
                            </a>
                            <a href="#" class="btn main-btn">
                                <i class="fa fa-play"></i>
                                روتیکس چگونه کار می کند
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="header-currency-image">
                        <img src="{{asset('theme/landing/images/header-img.png')}}" alt="ارز دیجیتال"
                             class="header-currency">
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="header-currency-header-info">
            <div class="container">
                <h2>میزان معاملات ۲۴ ساعته روتیکس : ۲۵۰/۰۰۰/۰۰۰</h2>
            </div>

            <div class="header-currency-informations-container">
                <div class="container">

                    <div class="currencys">

                        <div class="header-currency-informations">
                            @php
                                $currencies1=get_specific_currencies('btc,eth,ltc,usdt,xrp');
                            @endphp
                            @foreach($currencies1 as $key=>$item)
{{--                                @dd($item)--}}
                                @php
                                $sign=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);
                                @endphp
                                <div class="header-currency-information">
                                    <div class="header-currency-desc">
                                        <h4 class="header-currency-name">{{$key}}/USDT</h4>

                                        <h4 class="{{$item['1d']<0?'down':'up'}}-currency">
                                            {{$sign}}  {{number_format((float)$value,2)}}  {{'%'}}
                                        </h4>
                                    </div>
                                    <div class="header-currency-price-container">
                                        <h3 class="header-currency-price">
                                            {{number_format($item['price'],2)}}
                                        </h3>
                                        <h4>$0.0256</h4>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="header-currency-informations">
                            @php
                            $currencies2=get_specific_currencies('bch,bnb,eos,jst,win');
                            @endphp
                            @foreach($currencies2 as $key=>$item)
                                @php
                                    $sign=$item['1d']<0?'-':'+';
                                    $value=str_replace('-','',$item['1d']);
                                @endphp
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">{{$key}}/USDT</h4>

                                    <h4 class="{{$item['1d']<0?'down':'up'}}-currency">
                                        {{$sign}} {{number_format((float)$value,2)}} {{"%"}}
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">
                                        {{number_format($item['price'],2)}}
                                    </h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="header-currency-informations">
                            <div class="header-currency-information header-currency-information-1">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">DOGE/BUSD</h4>

                                    <h4 class="down-currency">
                                        -12.5%
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">0.0002569</h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">DOGE/BUSD</h4>

                                    <h4 class="up-currency">
                                        -12.5%
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">0.0002569</h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">DOGE/BUSD</h4>

                                    <h4 class="down-currency">
                                        -12.5%
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">0.0002569</h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">DOGE/BUSD</h4>

                                    <h4 class="up-currency">
                                        -12.5%
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">0.0002569</h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                            <div class="header-currency-information">
                                <div class="header-currency-desc">
                                    <h4 class="header-currency-name">DOGE/BUSD</h4>

                                    <h4 class="up-currency">
                                        -12.5%
                                    </h4>
                                </div>
                                <div class="header-currency-price-container">
                                    <h3 class="header-currency-price">0.0002569</h3>
                                    <h4>$0.0256</h4>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="go-down-container">
            <a href="#currency-table" class="go-down">
                <img src="{{asset('theme/landing/images/Group 186.png')}}" alt="به پایین رفتن"/>
            </a>
        </div>
    </header>
    <!-- End Header -->
@endsection

@section('content')
    <!-- Start Section One -->
    <section class="p-t table-currency">
        <div class="container">
            <a id="currency-table"></a>
            <div class="currency-tab">
                <span class="currency-tabel-title">انتخاب بازار بر اساس:</span>
                <div class="currency-tabel-btns">
                    <button id="rial" class="active">تومان IRT</button>
                    <button id="usdt">تتر USDT</button>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <table class="currency-table active rial-table ">
                        <tr>
                            <td>نام</td>
                            <td>آخرین قیمت (تومان)</td>
                            <td>تغییر</td>
                            <td>بیشترین قیمت</td>
                            <td>نمودار تغییرات هفته</td>
                            <td></td>
                        </tr>

                        @foreach($currency as $key=>$item)
                            @php
                                $sing=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);
                            @endphp
                            <tr>
                                <td class="currency-title">
                                    <div class="currency-right-title">
                                        <img src="{{$item['logo_url']}}"
                                             alt="{{ $item['name_fa']}}"/>
                                        <h5>{{$item['currency']}}</h5>
                                    </div>
                                    <span>{{ $item['name_fa']}}</span>
                                </td>
                                <td class="currency-last-price">
                                    {{number_format(rial_to_unit($item['rls'],'rls'))}}
                                </td>
                                <td class="currency-present-change">
                                    @if(isset($item['1d']))
                                        <span
                                            class="currency-present-change {{(float)$item['1d']<0?'red':'green'}}-currency">
                           {{number_format((float)$value,2)}} {{$sing}}
                          </span>
                                    @endif

                                </td>
                                <td class="currency-most-price">
                                    @if(isset($item['day_high']))
                                        {{number_format($item['dayHigh']/10)}}
                                    @else
                                        -
                                    @endif
                                </td>

                                <td class="currency-chart">
                                    <canvas id="{{$key}}_chart" width="160" height="40"></canvas>
                                    <script type="text/javascript" >
                                        setTimeout(function(){
                                            chart("{{$key}}_chart", "{{$item['color']}}");
                                        },1000);
                                    </script>
                                </td>
                                <td class="currency-buying-btn">
                                    <a href="{{route('exchange.index',['des_unit'=>strtolower($key),'source_unit'=>'usdt'])}}"
                                       class="btn light-blue-btn">خرید و فروش</a>
                                </td>
                            </tr>

                        @endforeach

                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <table class="currency-table  usdt-table">
                        <tr>
                            <td>نام</td>
                            <td>آخرین قیمت (تتر)</td>
                            <td>تغییر</td>
                            <td>بیشترین قیمت</td>
                            <td>نمودار تغییرات هفته</td>
                            <td></td>
                        </tr>
                        @foreach($currency as $key=>$item)
                            @php

                                $sing=$item['1d']<0?'-':'+';
                                $value=str_replace('-','',$item['1d']);

                            @endphp
                            <tr>
                                <td class="currency-title">
                                    <div class="currency-right-title">
                                        <img src="{{$item['logo_url']}}"
                                             alt="{{ $item['name_fa']}}"/>
                                        <h5>{{$item['currency']}}</h5>
                                    </div>
                                    <span>{{ $item['name_fa']}}</span>
                                </td>
                                <td class="currency-last-price">
                                    @if($item['price']>=1)
                                        {{number_format($item['price'],2)}}
                                    @else
                                        {{number_format($item['price'],5)}}
                                    @endif
                                </td>
                                <td class="currency-present-change">
                                    @if(isset($item['1d']))
                                        <span
                                            class="currency-present-change {{(float)$item['1d']<0?'red':'green'}}-currency">
                           {{number_format((float)$value,2)}} {{$sing}}
                          </span>
                                    @endif

                                </td>
                                <td class="currency-most-price">
                                    @if(isset($item['day_high']))
                                        {{$item['day_high']}}
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="currency-chart">
                                    <canvas id="{{$item['name']}}_chart" width="160" height="40"></canvas>
                                    <script type="text/javascript" >
                                        setTimeout(function(){
                                            chart("{{$item['name']}}_chart", "{{$item['color']}}");
                                        },1000);
                                    </script>
                                </td>
                                <td class="currency-buying-btn">
                                    <a href="{{route('exchange.index',['des_unit'=>strtolower($key),'source_unit'=>'usdt'])}}"
                                       class="btn light-blue-btn">خرید و فروش</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>


        <div class="all-currency-btn">
            <a href="{{route('landing.prices')}}" class="btn all-crypto-currency">
                نمایش همه بازار ها
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="24"
                    height="25.243"
                    viewBox="0 0 24 25.243"
                >
                    <g
                        id="Icon_feather-arrow-left"
                        data-name="Icon feather-arrow-left"
                        transform="translate(-6 -5.379)"
                    >
                        <path
                            id="Path_7756"
                            data-name="Path 7756"
                            d="M28.5,18H7.5"
                            fill="none"
                            stroke="#000"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="3"
                        />
                        <path
                            id="Path_7757"
                            data-name="Path 7757"
                            d="M18,28.5,7.5,18,18,7.5"
                            fill="none"
                            stroke="#000"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="3"
                        />
                    </g>
                </svg>
            </a>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="section section-two">
        <div class="container">
            <h2 class="app-title">
                کار کردن با روتیکس
                <br/>
                <span class="gold-title"> واقعا آسونه </span>
            </h2>

            <div class="auth">
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-1.png')}}" alt="ثبت نام"/>
                    <span> ثبت نام در صرافی </span>
                    <p>
                        وارد کردن اطلاعات هویتی کاربر در مرحله اول
                    </p>
                </div>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="66.65"
                    height="44.452"
                    viewBox="0 0 66.65 44.452"
                    class="auth-arrow"
                >
                    <path
                        id="Icon_ionic-ios-arrow-round-back"
                        data-name="Icon ionic-ios-arrow-round-back"
                        d="M32.028,12.1a3.025,3.025,0,0,1,.023,4.26L18,30.461H71.546a3.01,3.01,0,0,1,0,6.019H18l14.075,14.1a3.047,3.047,0,0,1-.023,4.26,3,3,0,0,1-4.237-.023L8.739,35.6h0a3.38,3.38,0,0,1-.625-.949,2.872,2.872,0,0,1-.232-1.158,3.017,3.017,0,0,1,.857-2.107L27.815,12.172A2.949,2.949,0,0,1,32.028,12.1Z"
                        transform="translate(-7.882 -11.252)"
                        fill="#6bb9f0"
                    />
                </svg>
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-2.png')}}" alt="اهراز هویت"/>
                    <span> احراز هویت </span>
                    <p>
                        احراز اطلاعات وارد شده و تایید شماره تماس و ایمیل
                    </p>
                </div>
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="66.65"
                    height="44.452"
                    viewBox="0 0 66.65 44.452"
                    class="auth-arrow"
                >
                    <path
                        id="Icon_ionic-ios-arrow-round-back"
                        data-name="Icon ionic-ios-arrow-round-back"
                        d="M32.028,12.1a3.025,3.025,0,0,1,.023,4.26L18,30.461H71.546a3.01,3.01,0,0,1,0,6.019H18l14.075,14.1a3.047,3.047,0,0,1-.023,4.26,3,3,0,0,1-4.237-.023L8.739,35.6h0a3.38,3.38,0,0,1-.625-.949,2.872,2.872,0,0,1-.232-1.158,3.017,3.017,0,0,1,.857-2.107L27.815,12.172A2.949,2.949,0,0,1,32.028,12.1Z"
                        transform="translate(-7.882 -11.252)"
                        fill="#6bb9f0"
                    />
                </svg>
                <div class="auth-item">
                    <img src="{{asset('theme/landing/images/auth-3.png')}}" alt="خرید و فروش"/>
                    <span> آغاز خرید و فروش</span>
                    <p>
                        دریافت ایمیل تایید و وارد کردن اطلاعات بانکی. در بخش بعدی منتظر تایید بمانید
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Two -->

    @if(count($new_post) > 5)
        <!-- Start Section Three -->
        <section>
            <div class="container">
                <div class="blog-title text-center">
                    <h2>بلاگ روتیکس</h2>
                </div>
                <div class="row row-padding">
                    <div class="col-12 col-md-6 p-left p-right" dir="ltr">
                        <div class="blog-container" dir="rtl">
                            <div class="right-blog top-blog blog-info" style="margin-bottom: 1rem;">
                                <a href="{{ $new_post[1]['link'] }}">
                                    <img src="{{ $new_post[1]['thumbnail_url'] }}" height="244"
                                         alt="{{ $new_post[1]['title']['rendered'] }}"/>
{{--                                    <div class="currency-blog">--}}
{{--                                        <p>{{ $new_post[1]['title']['rendered'] }}</p>--}}
{{--                                        <div class="calender">--}}
{{--                                            <p>{{ jdate_from_gregorian($new_post[1]['modified'],'%A, %d %B %Y') }}</p>--}}

{{--                                            <div>--}}
{{--                                                19--}}
{{--                                                <i class="far fa-comment"></i>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </a>
                            </div>

                            <div class="right-blog down-blog">
                                <div>
                                    <div class="blog-info blog-desc">
                                        <a href="{{ $new_post[2]['link'] }}">
                                            <img src="{{ $new_post[2]['thumbnail_url'] }}" height="244"
                                                 alt="{{ $new_post[2]['title']['rendered'] }}"/>
{{--                                            <div class="currency-blog">--}}
{{--                                                <p>{{ $new_post[2]['title']['rendered'] }}</p>--}}
{{--                                                <div class="calender">--}}
{{--                                                    <p>{{ jdate_from_gregorian($new_post[2]['modified'],'%A, %d %B %Y') }}</p>--}}

{{--                                                    <div>--}}
{{--                                                        19--}}
{{--                                                        <i class="far fa-comment"></i>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </a>
                                    </div>
                                </div>
                                <div dir="ltr">
                                    <div class="blog-info blog-desc" dir="rtl">
                                        <a href="{{ $new_post[3]['link'] }}">
                                            <img src="{{ $new_post[3]['thumbnail_url'] }}" height="244"
                                                 alt="{{ $new_post[3]['title']['rendered'] }}"/>
{{--                                            <div class="currency-blog">--}}
{{--                                                <p>{{ $new_post[3]['title']['rendered'] }}</p>--}}
{{--                                                <div class="calender">--}}
{{--                                                    <p>{{ jdate_from_gregorian($new_post[3]['modified'],'%A, %d %B %Y') }}</p>--}}

{{--                                                    <div>--}}
{{--                                                        19--}}
{{--                                                        <i class="far fa-comment"></i>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 p-left p-right">
                        <div class="left-blog blog-info">
                            <a href="{{ $new_post[4]['link'] }}">
                                <img src="{{ $new_post[4]['thumbnail_url'] }}"
                                     alt="{{ $new_post[4]['title']['rendered'] }}"/>
{{--                                <div class="currency-blog">--}}
{{--                                    <p>{{ $new_post[4]['title']['rendered'] }}</p>--}}
{{--                                    <div class="calender">--}}
{{--                                        <p>{{ jdate_from_gregorian($new_post[4]['modified'],'%A, %d %B %Y') }}</p>--}}

{{--                                        <div>--}}
{{--                                            19--}}
{{--                                            <i class="far fa-comment"></i>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Section Three -->
    @endif

    <!-- Start Section Four -->
    <section class="what-currency section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="what-currency-info">
                        <h5>رمز ارز چیست؟</h5>
                        <div class="what-crypto-title">
                            <h2>بگذارید کمی از</h2>
                            <span>رمز ارز ها صحبت کنیم</span>
                        </div>
                        <p class="what-crypto-desc">
                            رمزارز (یا ارز رمزپایه) گونه ای پول دیجیتال است که در آن تولید واحد پول و تأیید اصالت تراکنش
                            پول با استفاده از الگوریتمهای رمزگذاری کنترل میشود و معمولاً به طور نامتمرکز (بدون وابستگی
                            به یک بانک مرکزی) کار میکند.
                        </p>

                        <a href="#" class="what-currency-more-btn light-blue-btn btn">
                            بیشتر بخوانید
                        </a>

                        <div class="about-us">
                            <span>کمی درباره ما:</span>
                            <a href="#"> کلیک کنید </a>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="crypto-carts">
                        <div class="crypto-carts-2 crypto-carts-3">
                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/bitcoin.png')}}" alt="پول دیجیتال"/>
                                <div class="crypto-cart-desc">
                                    <h3>پول دیجیتالی</h3>
                                    <p>
                                        ارزهایی هستند که به صورت الکترونیکی ذخیره و منتقل میشوند
                                    </p>
                                </div>
                            </div>

                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/Icon.png')}}" alt="همیشه در دسترس"/>
                                <div class="crypto-cart-desc">
                                    <h3>هیمشه در دسترس</h3>
                                    <p>
                                        پشتیبانی قوی و 24 ساعته روتیکس برای تجربه بهتر در خرید و فروش
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="crypto-carts-2">
                            <div class="crypto-cart">
                                <img src="{{asset('theme/landing/images/credit-card.png')}}" alt="امنیت تراکنش"/>
                                <div class="crypto-cart-desc">
                                    <h3>امنیت تراکنش</h3>
                                    <p>
                                        حفظ تمامی اطلاعات کاربران و اطلاعات تراکنش ها
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Four -->

    <!-- Start Section Five -->
    <section class="feature">
        <div class="container-fluid">
            <h2 class="feature-title">
                ویژگی های
                <span class="gold-title gold-feature"> روتیکس </span>
            </h2>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-1.jpg')}}" alt="ثبت نام سریع و راحت"/>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-info" dir="rtl">
                        <div class="feature-info-title">
                            <h2>راحت و سریع در ثبت نام</h2>
                            <span>و احراز هویت</span>
                        </div>

                        <div>
                            <p>
                                در روتیکس به سادگی با پر کردن دو فرم تایید هویتی و وارد کردن اطلاعات خود به سادگی و به
                                سرعت در یکی از بهترین صرافی ها ثبت نام کنید.
                            </p>

                            <a href="#" class="feature-btn">
                                بیشتر بخوانید
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="25"
                                    height="25"
                                    viewBox="0 0 25 25"
                                >
                                    <image
                                        id="icons8-arrows_long_left_copy"
                                        data-name="icons8-arrows_long_left copy"
                                        width="25"
                                        height="25"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="
                                    />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-sec-five">
                <div class="col-12 col-md-6">
                    <div class="feature-info">
                        <div class="feature-info-title">
                            <h2>امنیت و حفظ اطلاعات کاربران</h2>
                        </div>
                        <p>
                            روتیکس خود را موظف میداند که امنیت اطلاعات کاربران خود را تامین نموده تا کاربران با خیالی
                            آسوده خرید و فروش کنند.
                        </p>

                        <a href="#" class="feature-btn">
                            بیشتر بخوانید
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="25"
                                height="25"
                                viewBox="0 0 25 25"
                            >
                                <image
                                    id="icons8-arrows_long_left_copy"
                                    data-name="icons8-arrows_long_left copy"
                                    width="25"
                                    height="25"
                                    xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="
                                />
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-img-2">
                        <img src="{{asset('theme/landing/images/sec-five-2.jpg')}}" alt="امنیت"/>
                    </div>
                </div>
            </div>

            <div class="row row-sec-five">
                <div class="col-12 col-md-6">
                    <div class="feature-img">
                        <img src="{{asset('theme/landing/images/sec-five-3.jpg')}}" alt="انجام سریع خرید و فروش"/>
                    </div>
                </div>

                <div class="col-12 col-md-6" dir="ltr">
                    <div class="feature-info" dir="rtl">
                        <div class="feature-info-title">
                            <h2>خرید و فروش آسان</h2>
                        </div>

                        <div>
                            <p>
                                روتیکس با امکان رابط کاربری آسان و سریع
                            </p>

                            <a href="#" class="feature-btn">
                                بیشتر بخوانید
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="25"
                                    height="25"
                                    viewBox="0 0 25 25"
                                >
                                    <image
                                        id="icons8-arrows_long_left_copy"
                                        data-name="icons8-arrows_long_left copy"
                                        width="25"
                                        height="25"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABmJLR0QA/wD/AP+gvaeTAAAB4UlEQVR4nO3cv0tWURzH8bc/UomIRKKWcAkaG1ukMRLEIPoTGuwfcMrZIWhoaXNpEYeWIGgIoiFoiCAIQuR5EAyCaAmCkPBxuEnpeRwa4vvF7/sFZ7vDBz5czr3nnnNBkiRJkiRJkiRJkiRJkiRJ0km3CHwBvgF3grOUtwjsAoPf42tsnNpuAD/5U8aA7k5RgOvADw6X8Qu4HRmqqmvAdw6XsQfcjQxV1VW6yftoGfciQ1V1hW6OGBwZy5GhqroMfKYt435kqKouAX3aMh5GhqrqAvCJtoxHkaGqOg98pC1jDRgJzFXSOeAdbRkbwFhgrpLOAm9py3gKjAfmKuk08Iq2jBfAZFysmiaA57RlvASmAnOVdAp4RlvGG+BMYK6SxoB12jLeA9OBuUoaBZ7QlvEBmAnMVdII8Ji2jE3gYmCush7QlrENzEaGqmqVtgzH/x09YH5YGSsJwlUdOwcljA5rRnH+Xn96TfeiNxeUpao+sARsHXeBk3oyPvYm5IthQi6dJOTiYkIuvyfkB6qE/ISbkJscEnIbUEJulEvIraQJudk6IY8jJOSBnYQ80pbQcYc+b0WGqm7YseheaCI1Pw7ox8YRwALdndEDbgZnkSRJkiRJkiRJkiRJkiRJkqR/sg+eUJJlBD5ToQAAAABJRU5ErkJggg=="
                                    />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Five -->

    <!-- Start Section Six -->
    <section class="mobile-app section-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="what-currency-info">
                        <h5>اپلیکیشن موبایل رو فراموش نکنید</h5>
                        <div class="what-crypto-title">
                            <h2>همیشه و همه جا بازار رو چک کن</h2>
                            <span>با اپلیکیشن موبایل روتیکس</span>
                        </div>
                        <p class="what-crypto-desc">
                            پنل روتیکس در تلفن های هوشمند هم قابل استفاده است . به راحتی قابل دسترس است و با رابط کاربری
                            پیشرفته و آسان خود ، تجربه کار با صرافی در هر لحظه و هر مکان را با استفاده از تلفن هوشمند
                            برای کاربران خود ایجاد میکند.
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/mobile-1.png')}}" alt="برنامه موبایل"/>
                        <img src="{{asset('theme/landing/images/mobile-2.png')}}" alt="برنامه موبایل دو"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Six -->

    <!-- Start Section Seven -->
    <section class="customers">
        <div>
            <h2 class="customer-title">آنچه مشتریان ما می گویند</h2>
            <div class="owl-carousel owl-theme customer-owl">
                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>مریم احمدی </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                یکی از بهترین صرافی هایی که دیدم امنیت بالا و سرعت سایت باعث شده من سراغ صرافی های دیگه
                                نرم
                            </p>
                        </div>

                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-4.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>سینا اکبری </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                قابل اعتماد ، پشتیبانی سریع ، امیدوارم روز به روز بهترو قوی تر بشید من به چند تا از
                                دوستام معرفی کردم و اونا هم راضی بودن
                            </p>
                        </div>

                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-5.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>بیتا رحیمی </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                اولین چیزی که منو جذب این سایت کرد طراحی فوق العاده ش بود خیلی راضی م از این صرافی
                            </p>
                        </div>
                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-6.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>

                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>زهرا ضیایی</h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                نکته مثبت سایت اینه که مهم ترین ارز ها رو داره واقعا منو از داشتن صرافی های دیگه بی نیاز
                                کرده
                            </p>
                        </div>
                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-7.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>محمد صادقی </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                تجربه خوبی داشتم تو این صرافی به شما هم پیشنهاد میکنم استفاده کنید پشیمون نمیشید
                            </p>
                        </div>
                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-8.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>
                <div class="customer-container">
                    <div class="customer">
                        <div>
                            <div class="customer-info">
                                <h5>پدرام مومنی
                                </h5>
                                <span>مشتری پنل روتیکس</span>
                            </div>

                            <p>
                                اولین چیزی که منو جذب این سایت کرد هر اطلاعاتی از ارز ها بخوام و این سایت داره و
                                منو از داشتن هزاران کانال و سایت دیگه راحت کرده
                            </p>
                        </div>
                        <div class="customer-img">
                            <img src="{{asset('theme/landing/images/customer-9.png')}}" alt="مشتری"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Seven -->

    <!-- Start Section Eight -->
    <section id="contact-us">
        <div class="forms">
            <div class="form">
                <div>
                    <h2>با ما در تماس باشید</h2>
                    <p style="margin-bottom: 13px;">
                        در پیشرفت روتیکس با ما همراه باشید ، نظرات و انتقادات خود را در تجربه کار با روتیکس با ما به
                        اشتراک بگذارید. تیم ما برای هرچه بهتر شدن روتیکس مشتاق نظرات شما هستند
                    </p>
                    <div style="font-size: 14px;">
                        <i class="fa fa-phone"></i>
                        <span>شماره تماس:</span>
                        <span>91091102-021</span>
                    </div>
                    {{--<div style="font-size: 14px;margin-top: 10px;">
                        <i class="fa fa-map"></i>
                        <span>آدرس:</span>
                        <span>اصفهان-نجف آباد-خیابان شریعتی-مچتمع نیایش-طبقه 4- واحد 21</span>
                    </div>--}}
                </div>

                <form action="#" style="margin-top: 25px;">
                    <div class="inputs">
                        <div class="input">
                            <label for="f-name1">نام</label>
                            <input
                                type="text"
                                id="f-name1"
                                placeholder="نام را وارد کنید"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name1">نام خانوادگی</label>
                            <input
                                type="text"
                                id="l-name1"
                                placeholder="نام خانوادگی را وارد کنید"
                            />
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input">
                            <label for="f-name2">ایمیل</label>
                            <input
                                type="text"
                                id="f-name2"
                                placeholder="ایمیل را وارد کنید"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name2">نام خانوادگی</label>
                            <input
                                type="text"
                                id="l-name2"
                                placeholder="Enter your last name"
                            />
                        </div>
                    </div>

                    <button type="submit" class="btn light-blue-btn send-btn">
                        ارسال پیام
                    </button>
                </form>
            </div>

            <div class="form-img">
                <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="فرم"/>
            </div>
        </div>
    </section>
    <!-- End Section Eight -->
    {{--    <!-- Slider Starts -->--}}
    {{--    <div id="main-slide" class="carousel slide carousel-fade" data-ride="carousel">--}}
    {{--        <!-- Indicators Starts -->--}}
    {{--        <ol class="carousel-indicators visible-lg visible-md">--}}
    {{--            <li data-target="#main-slide" data-slide-to="0" class="active"></li>--}}
    {{--            <li data-target="#main-slide" data-slide-to="1"></li>--}}
    {{--            <li data-target="#main-slide" data-slide-to="2"></li>--}}
    {{--        </ol>--}}
    {{--        <!-- Indicators Ends -->--}}
    {{--        <!-- Carousel Inner Starts -->--}}
    {{--        <div class="carousel-inner">--}}
    {{--            <!-- Carousel Item Starts -->--}}
    {{--            <div class="item active bg-parallax item-1">--}}
    {{--                <div class="slider-content">--}}
    {{--                    <div class="container">--}}
    {{--                        <div class="slider-text text-center">--}}
    {{--                            <h3 class="slide-title"><span>{{__('attributes.headerSafe')}}</span> {{__('attributes.headerAnd')}} <span>{{__('attributes.headerEasy')}}</span><br/> {{__('attributes.headerBTC')}}</h3>--}}
    {{--                            <p>--}}
    {{--                                <a href="about.html" class="slider btn btn-primary">{{__('attributes.moreInfo')}}</a>--}}
    {{--                            </p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Carousel Item Ends -->--}}
    {{--            <!-- Carousel Item Starts -->--}}
    {{--            <div class="item bg-parallax item-2">--}}
    {{--                <div class="slider-content">--}}
    {{--                    <div class="col-md-12">--}}
    {{--                        <div class="container">--}}
    {{--                            <div class="slider-text text-center">--}}
    {{--                                <h3 class="slide-title"><span>{{__('attributes.headerEasyAndSafe'.'')}}</span>--}}
    {{--                                    {{__('attributes.headerGet')}} <br/>--}}
    {{--                                    {{__('attributes.headerBringing')}} <span>{{__('attributes.BitCoin')}}</span> </h3>--}}
    {{--                                <p>--}}
    {{--                                    <a href="pricing.html" class="slider btn btn-primary">{{__('attributes.pricing')}}</a>--}}
    {{--                                </p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Carousel Item Ends -->--}}
    {{--        </div>--}}
    {{--        <!-- Carousel Inner Ends -->--}}
    {{--        <!-- Carousel Controlers Starts -->--}}
    {{--        <a class="left carousel-control" href="index.html#main-slide" data-slide="prev">--}}
    {{--            <span><i class="fa fa-angle-left"></i></span>--}}
    {{--        </a>--}}
    {{--        <a class="right carousel-control" href="index.html#main-slide" data-slide="next">--}}
    {{--            <span><i class="fa fa-angle-right"></i></span>--}}
    {{--        </a>--}}
    {{--        <!-- Carousel Controllers Ends -->--}}
    {{--    </div>--}}
    {{--    <!-- Slider Ends -->--}}
    {{--    <!-- Features Section Starts -->--}}
    {{--    <section class="features">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row features-row">--}}
    {{--                <!-- Feature Box Starts -->--}}
    {{--                <div class="feature-box col-md-4 col-sm-12">--}}
    {{--                        <span class="feature-icon">--}}
    {{--							<img id="download-bitcoin" src="{{ asset('theme/landing/images/icons/orange/download-bitcoin.png') }}" alt="download bitcoin">--}}
    {{--						</span>--}}
    {{--                    <div class="feature-box-content">--}}
    {{--                        <h3>{{__('attributes.BTC-wallet')}}</h3>--}}
    {{--                        <p>{{__('paragraph.BTC-wallet')}}</p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Feature Box Ends -->--}}
    {{--                <!-- Feature Box Starts -->--}}
    {{--                <div class="feature-box two col-md-4 col-sm-12">--}}
    {{--                        <span class="feature-icon">--}}
    {{--							<img id="add-bitcoins" src="{{ asset('theme/landing/images/icons/orange/add-bitcoins.png') }}" alt="add bitcoins">--}}
    {{--						</span>--}}
    {{--                    <div class="feature-box-content">--}}
    {{--                        <h3>{{__('attributes.coinWallet')}}</h3>--}}
    {{--                        <p>{{__('paragraph.coinWallet')}}</p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Feature Box Ends -->--}}
    {{--                <!-- Feature Box Starts -->--}}
    {{--                <div class="feature-box three col-md-4 col-sm-12">--}}
    {{--                        <span class="feature-icon">--}}
    {{--							<img id="buy-sell-bitcoins" src="{{ asset('theme/landing/images/icons/orange/buy-sell-bitcoins.png') }}" alt="buy and sell bitcoins">--}}
    {{--						</span>--}}
    {{--                    <div class="feature-box-content">--}}
    {{--                        <h3>{{__('attributes.buySellWallet')}}</h3>--}}
    {{--                        <p>{{__('paragraph.buySellWallet')}}</p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Feature Box Ends -->--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Features Section Ends -->--}}
    {{--    <!-- About Section Starts -->--}}
    {{--    <section class="about-us">--}}
    {{--        <div class="container">--}}
    {{--            <!-- Section Title Starts -->--}}
    {{--            <div class="row text-center">--}}
    {{--                <h2 class="title-head">{{__('attributes.about')}} <span>{{__('attributes.us')}}</span></h2>--}}
    {{--                <div class="title-head-subtitle">--}}
    {{--                    <p>{{__('paragraph.aboutUs-header')}}</p>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Section Title Ends -->--}}
    {{--            <!-- Section Content Starts -->--}}
    {{--            <div class="row about-content">--}}
    {{--                <!-- Image Starts -->--}}
    {{--                <div class="col-sm-12 col-md-5 col-lg-6 text-center">--}}
    {{--                    <img id="about-us" class="img-responsive img-about-us" src="{{ asset('theme/landing/images/about-us.png') }}" alt="about us">--}}
    {{--                </div>--}}
    {{--                <!-- Image Ends -->--}}
    {{--                <!-- Content Starts -->--}}
    {{--                <div class="col-sm-12 col-md-7 col-lg-6">--}}
    {{--                    <h3 class="title-about">{{__('attributes.company')}}</h3>--}}
    {{--                    <p class="about-text">{{__('paragraph.aboutUs')}}</p>--}}
    {{--                    <ul class="nav nav-tabs">--}}
    {{--                        <li class="active"><a data-toggle="tab" href="#menu1">{{__('attributes.ourWork')}}</a></li>--}}
    {{--                        <li><a data-toggle="tab" href="#menu2">{{__('attributes.ourBenefits')}}</a></li>--}}
    {{--                        <li><a data-toggle="tab" href="#menu3">{{__('attributes.ourGuarantees')}}</a></li>--}}
    {{--                    </ul>--}}
    {{--                    <div class="tab-content">--}}
    {{--                        <div id="menu1" class="tab-pane fade in active">--}}
    {{--                            <p>{{__('paragraph.ourWork')}}</p>--}}
    {{--                        </div>--}}
    {{--                        <div id="menu2" class="tab-pane fade">--}}
    {{--                            <p>{{__('paragraph.ourBenefits')}}</p>--}}
    {{--                        </div>--}}
    {{--                        <div id="menu3" class="tab-pane fade">--}}
    {{--                            <p> {{__('paragraph.ourGuarantees')}}</p>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <a class="btn btn-primary" href="about.html">{{__('attributes.moreInfo')}}</a>--}}
    {{--                </div>--}}
    {{--                <!-- Content Ends -->--}}
    {{--            </div>--}}
    {{--            <!-- Section Content Ends -->--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- About Section Ends -->--}}
    {{--    <!-- Features and Video Section Starts -->--}}
    {{--    <section class="image-block">--}}
    {{--        <div class="container-fluid">--}}
    {{--            <div class="row">--}}
    {{--                <!-- Features Starts -->--}}
    {{--                <div class="col-md-8 ts-padding img-block-left">--}}
    {{--                    <div class="gap-20"></div>--}}
    {{--                    <div class="row">--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="strong-security" src="{{ asset('theme/landing/images/icons/orange/strong-security.png') }}" alt="strong security"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title">{{__('attributes.security')}}</h3>--}}
    {{--                                <p>محافظت در برابر حملات DDoS, <br>رمزگذاری کامل داده ها</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                        <div class="gap-20-mobile"></div>--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="world-coverage" src="{{ asset('theme/landing/images/icons/orange/world-coverage.png') }}" alt="world coverage"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title"> {{__('attributes.global')}}</h3>--}}
    {{--                                <p>ارائه خدمات در 99٪ كشورها<br> در سراسر جهان</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                    </div>--}}
    {{--                    <div class="gap-20"></div>--}}
    {{--                    <div class="row">--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="payment-options" src="{{ asset('theme/landing/images/icons/orange/payment-options.png') }}" alt="payment options"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title">{{__('attributes.payment')}}</h3>--}}
    {{--                                <p>روشهای رایج : <br>ویزا ، مستر کارت ، نقل و انتقال بانکی ، رمزنگاری</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                        <div class="gap-20-mobile"></div>--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="mobile-app" src="{{ asset('theme/landing/images/icons/orange/mobile-app.png') }}" alt="mobile app"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title">{{__('attributes.mobileApp')}}</h3>--}}
    {{--                                <p>تجارت از طریق برنامه تلفن همراه<br> دنلود در پلی استوری</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                    </div>--}}
    {{--                    <div class="gap-20"></div>--}}
    {{--                    <div class="row">--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="cost-efficiency" src="{{ asset('theme/landing/images/icons/orange/cost-efficiency.png') }}" alt="cost efficiency"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title">{{__('attributes.efficiency')}}</h3>--}}
    {{--                                <p>هزینه معاملاتی معقول برای سودجویان<br> و کلیه فروشندگان بازار</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                        <div class="gap-20-mobile"></div>--}}
    {{--                        <!-- Feature Starts -->--}}
    {{--                        <div class="col-sm-6 col-md-6 col-xs-12">--}}
    {{--                            <div class="feature text-center">--}}
    {{--                                    <span class="feature-icon">--}}
    {{--										<img id="high-liquidity" src="{{ asset('theme/landing/images/icons/orange/high-liquidity.png') }}" alt="high liquidity"/>--}}
    {{--									</span>--}}
    {{--                                <h3 class="feature-title">{{__('attributes.liquidity')}}</h3>--}}
    {{--                                <p>دسترسی سریع به سفارش نقدینگی بالا<br> برای جفت ارزهای برتر</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <!-- Feature Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Features Ends -->--}}
    {{--                <!-- Video Starts -->--}}
    {{--                <div class="col-md-4 ts-padding bg-image-1">--}}
    {{--                    <div>--}}
    {{--                        <div class="text-center">--}}
    {{--                            <a class="button-video mfp-youtube" href="https://www.youtube.com/watch?v=0gv7OC9L2s8"></a>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Video Ends -->--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Features and Video Section Ends -->--}}
    {{--    <!-- Pricing Starts -->--}}
    {{--    --}}{{--<section class="pricing">--}}
    {{--        <div class="container">--}}
    {{--            <!-- Section Title Starts -->--}}
    {{--            <div class="row text-center">--}}
    {{--                <h2 class="title-head"> {{__('attributes.packages')}} <span>{{__('attributes.Affordable')}}</span></h2>--}}
    {{--                <div class="title-head-subtitle">--}}
    {{--                    <p>Bitcoin را با استفاده از یک کارت اعتباری یا با حساب بانکی مرتبط خود خریداری کنید</p>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Section Title Ends -->--}}
    {{--            <!-- Section Content Starts -->--}}
    {{--            <div class="row pricing-tables-content">--}}
    {{--                <div class="pricing-container">--}}
    {{--                    <!-- Pricing Switcher Starts -->--}}
    {{--                    <div class="pricing-switcher">--}}
    {{--                        <p>--}}
    {{--                            <input type="radio" name="switch" value="buy" id="buy-1" checked>--}}
    {{--                            <label for="buy-1">{{__('attributes.buy')}} </label>--}}
    {{--                            <input type="radio" name="switch" value="sell" id="sell-1">--}}
    {{--                            <label for="sell-1">{{__('attributes.sell')}} </label>--}}
    {{--                            <span class="switch"></span>--}}
    {{--                        </p>--}}
    {{--                    </div>--}}
    {{--                    <!-- Pricing Switcher Ends -->--}}
    {{--                    <!-- Pricing Tables Starts -->--}}
    {{--                    <ul class="pricing-list bounce-invert">--}}
    {{--                        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3">--}}
    {{--                            <ul class="pricing-wrapper">--}}
    {{--                                <!-- Buy Pricing Table #1 Starts -->--}}
    {{--                                <li data-type="buy" class="is-visible">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 0.007 BTC <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-dollar"></i></span>--}}
    {{--                                            <span class="value">100</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Buy Pricing Table #1 Ends -->--}}
    {{--                                <!-- Sell Pricing Table #1 Starts -->--}}
    {{--                                <li data-type="sell" class="is-hidden">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 100 تومان <span>برای </span></h2>--}}

    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-bitcoin"></i></span>--}}
    {{--                                            <span class="value">0.2</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Sell Pricing Table #1 Ends -->--}}
    {{--                            </ul>--}}
    {{--                        </li>--}}
    {{--                        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3">--}}
    {{--                            <ul class="pricing-wrapper">--}}
    {{--                                <!-- Buy Pricing Table #2 Starts -->--}}
    {{--                                <li data-type="buy" class="is-visible">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 0.015 BTC <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-dollar"></i></span>--}}
    {{--                                            <span class="value">300</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Buy Pricing Table #2 Ends -->--}}
    {{--                                <!-- Sell Pricing Table #2 Starts -->--}}
    {{--                                <li data-type="sell" class="is-hidden">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 1000 تومان <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-bitcoin"></i></span>--}}
    {{--                                            <span class="value">0.5</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Sell Pricing Table #2 Ends -->--}}
    {{--                            </ul>--}}
    {{--                        </li>--}}
    {{--                        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3">--}}
    {{--                            <ul class="pricing-wrapper">--}}
    {{--                                <!-- Buy Pricing Table #3 Starts -->--}}
    {{--                                <li data-type="buy" class="is-visible">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 0.031 BTC <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-dollar"></i></span>--}}
    {{--                                            <span class="value">500</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Buy Pricing Table #3 Ends -->--}}
    {{--                                <!-- Yearlt Pricing Table #3 Starts -->--}}
    {{--                                <li data-type="sell" class="is-hidden">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 3000 تومان <span>برای </span></h2>--}}

    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-bitcoin"></i></span>--}}
    {{--                                            <span class="value">1</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Sell Pricing Table #3 Ends -->--}}
    {{--                            </ul>--}}
    {{--                        </li>--}}
    {{--                        <li class="col-xs-6 col-sm-6 col-md-3 col-lg-3">--}}
    {{--                            <ul class="pricing-wrapper">--}}
    {{--                                <!-- Buy Pricing Table #4 Starts -->--}}
    {{--                                <li data-type="buy" class="is-visible">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 0.081 BTC <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-dollar"></i></span>--}}
    {{--                                            <span class="value">1,000</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Buy Pricing Table #4 Ends -->--}}
    {{--                                <!-- Sell Pricing Table #4 Starts -->--}}
    {{--                                <li data-type="sell" class="is-hidden">--}}
    {{--                                    <header class="pricing-header">--}}
    {{--                                        <h2>دریافت 9000 تومان <span>برای </span></h2>--}}
    {{--                                        <div class="price">--}}
    {{--                                            <span class="currency"><i class="fa fa-bitcoin"></i></span>--}}
    {{--                                            <span class="value">2</span>--}}
    {{--                                        </div>--}}
    {{--                                    </header>--}}
    {{--                                    <footer class="pricing-footer">--}}
    {{--                                        <a href="#" class="btn btn-primary">{{__('attributes.order')}}</a>--}}
    {{--                                    </footer>--}}
    {{--                                </li>--}}
    {{--                                <!-- Sell Pricing Table #4 Ends -->--}}
    {{--                            </ul>--}}
    {{--                        </li>--}}
    {{--                    </ul>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Pricing Ends -->--}}
    {{--    <!-- Bitcoin Calculator Section Starts -->--}}
    {{--    --}}{{--<section class="bitcoin-calculator-section">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row">--}}
    {{--                <!-- Section Heading Starts -->--}}
    {{--                <div class="col-md-12">--}}
    {{--                    <h2 class="title-head text-center"><span>{{__('attributes.computing')}}</span> {{__('attributes.BitCoin')}}</h2>--}}
    {{--                    <p class="message text-center">با مبدل کاربری آسان ما ، مقدار فعلی بیت کوین را بیابید</p>--}}
    {{--                </div>--}}
    {{--                <!-- Section Heading Ends -->--}}
    {{--                <!-- Bitcoin Calculator Form Starts -->--}}
    {{--                <div class="col-md-12 text-center">--}}
    {{--                    <form class="bitcoin-calculator" id="bitcoin-calculator">--}}
    {{--                        <!-- Input #1 Starts -->--}}
    {{--                        <input class="form-input" name="btc-calculator-value" value="1">--}}
    {{--                        <!-- Input #1 Ends -->--}}
    {{--                        <div class="form-info"><i class="fa fa-bitcoin"></i></div>--}}
    {{--                        <div class="form-equal">=</div>--}}
    {{--                        <!-- Input/Result Starts -->--}}
    {{--                        <input class="form-input form-input-result" name="btc-calculator-result">--}}
    {{--                        <!-- Input/Result Ends -->--}}
    {{--                        <!-- Select Currency Starts -->--}}
    {{--                        <div class="form-wrap">--}}
    {{--                            <select id="currency-select" class="form-input select-currency select-primary" name="btc-calculator-currency" data-dropdown-class="select-primary-dropdown"></select>--}}
    {{--                        </div>--}}
    {{--                        <!-- Select Currency Ends -->--}}
    {{--                    </form>--}}
    {{--                    <p class="info"><i>* داده ها هر 15 دقیقه به روز می شوند</i></p>--}}
    {{--                </div>--}}
    {{--                <!-- Bitcoin Calculator Form Ends -->--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Bitcoin Calculator Section Ends -->--}}
    {{--    <!-- Team Section Starts -->--}}
    {{--    --}}{{--<section class="team">--}}
    {{--        <div class="container">--}}
    {{--            <!-- Section Title Starts -->--}}
    {{--            <div class="row text-center">--}}
    {{--                <h2 class="title-head">{{__('attributes.experts')}} <span>{{__('attributes.us')}}</span></h2>--}}
    {{--                <div class="title-head-subtitle">--}}
    {{--                    <p> تیمی با استعداد از کارشناسان Cryptocurrency مستقر درایران</p>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Section Title Ends -->--}}
    {{--            <!-- Team Members Starts -->--}}
    {{--            <div class="row team-content team-members">--}}
    {{--                <!-- Team Member Starts -->--}}
    {{--                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">--}}
    {{--                    <div class="team-member">--}}
    {{--                        <!-- Team Member Picture Starts -->--}}
    {{--                        <img src="{{ asset('theme/landing/images/team/member1.jpg') }}" class="img-responsive" alt="team member">--}}
    {{--                        <!-- Team Member Picture Ends -->--}}
    {{--                        <!-- Team Member Details Starts -->--}}
    {{--                        <div class="team-member-caption social-icons">--}}
    {{--                            <h4>مریم معصومی</h4>--}}
    {{--                            <p>مدیر عامل</p>--}}
    {{--                            <ul class="list list-inline social">--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-facebook"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-twitter"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-google-plus"></a>--}}
    {{--                                </li>--}}
    {{--                            </ul>--}}
    {{--                        </div>--}}
    {{--                        <!-- Team Member Details Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Team Member Ends -->--}}
    {{--                <!-- Team Member Starts -->--}}
    {{--                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">--}}
    {{--                    <div class="team-member">--}}
    {{--                        <!-- Team Member Picture Starts -->--}}
    {{--                        <img src="{{ asset('theme/landing/images/team/member2.jpg') }}" class="img-responsive" alt="team member">--}}
    {{--                        <!-- Team Member Picture Ends -->--}}
    {{--                        <!-- Team Member Details Starts -->--}}
    {{--                        <div class="team-member-caption social-icons">--}}
    {{--                            <h4>مجید جعفری</h4>--}}
    {{--                            <p>مدیر</p>--}}
    {{--                            <ul class="list list-inline social">--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-facebook"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-twitter"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-google-plus"></a>--}}
    {{--                                </li>--}}
    {{--                            </ul>--}}
    {{--                        </div>--}}
    {{--                        <!-- Team Member Details Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Team Member Ends -->--}}
    {{--                <!-- Team Member Starts -->--}}
    {{--                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">--}}
    {{--                    <!-- Team Member-->--}}
    {{--                    <div class="team-member">--}}
    {{--                        <!-- Team Member Picture Starts -->--}}
    {{--                        <img src="{{ asset('theme/landing/images/team/member3.jpg') }}" class="img-responsive" alt="team member">--}}
    {{--                        <!-- Team Member Picture Ends -->--}}
    {{--                        <!-- Team Member Details Starts -->--}}
    {{--                        <div class="team-member-caption social-icons">--}}
    {{--                            <h4>نازنین محمودی</h4>--}}
    {{--                            <p>مشاور بیت کوین</p>--}}
    {{--                            <ul class="list list-inline social">--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-facebook"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-twitter"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-google-plus"></a>--}}
    {{--                                </li>--}}
    {{--                            </ul>--}}
    {{--                        </div>--}}
    {{--                        <!-- Team Member Details Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Team Member Ends -->--}}
    {{--                <!-- Team Member Starts -->--}}
    {{--                <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3">--}}
    {{--                    <div class="team-member">--}}
    {{--                        <!-- Team Member Picture Starts -->--}}
    {{--                        <img src="{{ asset('theme/landing/images/team/member4.jpg') }}" class="img-responsive" alt="team member">--}}
    {{--                        <!-- Team Member Picture Ends -->--}}
    {{--                        <!-- Team Member Details Starts -->--}}
    {{--                        <div class="team-member-caption social-icons">--}}
    {{--                            <h4>محمد حمیدی</h4>--}}
    {{--                            <p>کدنویس بیت کوین</p>--}}
    {{--                            <ul class="list list-inline social">--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-facebook"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-twitter"></a>--}}
    {{--                                </li>--}}
    {{--                                <li>--}}
    {{--                                    <a href="#" class="fa fa-google-plus"></a>--}}
    {{--                                </li>--}}
    {{--                            </ul>--}}
    {{--                        </div>--}}
    {{--                        <!-- Team Member Details Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Team Member Ends -->--}}
    {{--            </div>--}}
    {{--            <!-- Team Members Ends -->--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Team Section Ends -->--}}
    {{--    <!-- Quote and Chart Section Starts -->--}}
    {{--    <section class="image-block2">--}}
    {{--        <div class="container-fluid">--}}
    {{--            <div class="row">--}}
    {{--                <!-- Quote Starts -->--}}
    {{--                <div class="col-md-4 img-block-quote bg-image-2">--}}
    {{--                    <blockquote>--}}
    {{--                        <p>بیت کوین یکی از مهمترین اختراعات در کل تاریخ بشر است. برای اولین بار ، هر کس می تواند با هر کس دیگری ، هرجای کره زمین ، به راحتی و بدون محدودیت ، هر مقدار پول ارسال یا دریافت کند. این طلوع یک دنیای بهتر و آزادتر است.</p>--}}
    {{--                        <footer><img src="{{ asset('theme/landing/images/ceo.jpg') }}" alt="ceo" /> <span>مریم معصومی</span> - مدیر عامل</footer>--}}
    {{--                    </blockquote>--}}
    {{--                </div>--}}
    {{--                <!-- Quote Ends -->--}}
    {{--                <!-- Chart Starts -->--}}
    {{--                <div class="col-md-8 bg-grey-chart">--}}
    {{--                    <div class="chart-widget dark-chart chart-1">--}}
    {{--                        <div>--}}
    {{--                            <div class="btcwdgt-chart" data-bw-theme="dark"></div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                    <div class="chart-widget light-chart chart-2">--}}
    {{--                        <div>--}}
    {{--                            <div class="btcwdgt-chart" bw-theme="light"></div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Chart Ends -->--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Quote and Chart Section Ends -->--}}
    {{--    <!-- Blog Section Starts -->--}}
    {{--    <section class="blog">--}}
    {{--        <div class="container">--}}
    {{--            <!-- Section Title Starts -->--}}
    {{--            <div class="row text-center">--}}
    {{--                <h2 class="title-head">{{__('attributes.news')}} <span>{{__('attributes.BitCoin')}}</span></h2>--}}
    {{--                <div class="title-head-subtitle">--}}
    {{--                    <p>آخرین اخبار مربوط به بیت کوین را در وبلاگ ما ببینید</p>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Section Title Ends -->--}}
    {{--            <!-- Section Content Starts -->--}}
    {{--            <div class="row latest-posts-content">--}}
    {{--                <!-- Article Starts -->--}}
    {{--                <div class="col-sm-4 col-md-4 col-xs-12">--}}
    {{--                    <div class="latest-post">--}}
    {{--                        <!-- Featured Image Starts -->--}}
    {{--                        <a href="blog-post.html"><img class="img-responsive" src="{{ asset('theme/landing/images/blog/blog-post-small-1.jpg') }}" alt="img"></a>--}}
    {{--                        <!-- Featured Image Ends -->--}}
    {{--                        <!-- Article Content Starts -->--}}
    {{--                        <div class="post-body">--}}
    {{--                            <h4 class="post-title">--}}
    {{--                                <a href="blog-post.html">چگونگی آغاز Cryptocurrency و تأثیر آن بر معاملات مالی</a>--}}
    {{--                            </h4>--}}
    {{--                            <div class="post-text">--}}
    {{--                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است...</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-date">--}}
    {{--                            <span>01</span>--}}
    {{--                            <span>مرداد</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="blog-post.html" class="btn btn-primary">{{__('attributes.moreInfo')}}</a>--}}
    {{--                        <!-- Article Content Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Article Ends -->--}}
    {{--                <!-- Article Starts -->--}}
    {{--                <div class="col-sm-4 col-md-4 col-xs-12">--}}
    {{--                    <div class="latest-post">--}}
    {{--                        <!-- Featured Image Starts -->--}}
    {{--                        <a href="blog-post.html"><img class="img-responsive" src="{{ asset('theme/landing/images/blog/blog-post-small-2.jpg') }}" alt="img"></a>--}}
    {{--                        <!-- Featured Image Ends -->--}}
    {{--                        <!-- Article Content Starts -->--}}
    {{--                        <div class="post-body">--}}
    {{--                            <h4 class="post-title">--}}
    {{--                                <a href="blog-post.html">Cryptocurrency - چه کسی با آن درگیر است؟ کلمات در مورد اعضا</a>--}}
    {{--                            </h4>--}}
    {{--                            <div class="post-text">--}}
    {{--                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است...</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-date">--}}
    {{--                            <span>17</span>--}}
    {{--                            <span>فروردین</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="blog-post.html" class="btn btn-primary">{{__('attributes.moreInfo')}}</a>--}}
    {{--                        <!-- Article Content Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <!-- Article Ends -->--}}
    {{--                <!-- Article Start -->--}}
    {{--                <div class="col-sm-4 col-md-4 col-xs-12">--}}
    {{--                    <div class="latest-post">--}}
    {{--                        <!-- Featured Image Starts -->--}}
    {{--                        <a href="blog-post.html"><img class="img-responsive" src="{{ asset('theme/landing/images/blog/blog-post-small-3.jpg') }}" alt="img"></a>--}}
    {{--                        <!-- Featured Image Ends -->--}}
    {{--                        <!-- Article Content Starts -->--}}
    {{--                        <div class="post-body">--}}
    {{--                            <h4 class="post-title">--}}
    {{--                                <a href="blog-post.html">خطرات و پاداش سرمایه گذاری در بیت کوین. مزایا و معایب</a>--}}
    {{--                            </h4>--}}
    {{--                            <div class="post-text">--}}
    {{--                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است...</p>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="post-date">--}}
    {{--                            <span>25</span>--}}
    {{--                            <span>مهر</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="blog-post.html" class="btn btn-primary">{{__('attributes.moreInfo')}}</a>--}}
    {{--                        <!-- Article Content Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <!-- Section Content Ends -->--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Blog Section Ends -->--}}
    {{--    <!-- Call To Action Section Starts -->--}}
    {{--    <section class="call-action-all">--}}
    {{--        <div class="call-action-all-overlay">--}}
    {{--            <div class="container">--}}
    {{--                <div class="row">--}}
    {{--                    <div class="col-xs-12">--}}
    {{--                        <!-- Call To Action Text Starts -->--}}
    {{--                        <div class="action-text">--}}
    {{--                            <h2>{{__('attributes.getStart')}}</h2>--}}
    {{--                            <p class="lead">حساب را به صورت رایگان باز کنید و معاملات بیت کوین را شروع کنید!</p>--}}
    {{--                        </div>--}}
    {{--                        <!-- Call To Action Text Ends -->--}}
    {{--                        <!-- Call To Action Button Starts -->--}}
    {{--                        <p class="action-btn"><a class="btn btn-primary" href="register.html">{{__('attributes.href-noRegister')}}</a></p>--}}
    {{--                        <!-- Call To Action Button Ends -->--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!-- Call To Action Section Ends -->--}}
@endsection

@section('script')
    <script>
        $('#usdt').on('click', function () {
            $('.rial-table').removeClass('active');
            $('.usdt-table').addClass('active');
        });

        $('#rial').on('click', function () {
            $('.rial-table').addClass('active');
            $('.usdt-table').removeClass('active');
        })
    </script>
    <script src="{{asset('theme/user/scripts/currency_chart.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/header-slider.js')}}"></script>
    <script src="{{asset('theme/landing/scripts/index-notification.js')}}"></script>
@endsection
