@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    قوانین روتیکس
@endsection
@section('style')
    <style>
        ul li{
            list-style: square ;
            line-height: 2rem;
            text-align: justify;
        }
        p{
            line-height: 2rem;
        }

        .laws-container{
            max-width: 800px;
            padding: 4rem 0 5rem 0;
        }
    </style>
@endsection
@section('header')
    <header class="header faqs-header">
        <div class="container">
            <nav>
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>

        </div>
        <div class="faqs-search-container">
            <div class="faqs-search">
                <h1 class="faqs-search__title">قوانین روتیکس</h1>
            </div>
        </div>
    </header>
@endsection
@section('content')
    <section>
        <div class="container laws-container">
            <div class="row">
                <div class="col-12">
                    {!! json_decode($laws->extra_field3) !!}
                </div>
            </div>
        </div>
    </section>

@endsection

