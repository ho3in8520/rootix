@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    آموزش-روتیکس
@endsection
@section('header')
    <header class="blog-page-header">
        <div class="container">
            <nav class="nav blog-nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>
    </header>
@endsection
@section('content')
    <!-- Start Section One -->
    <section>
        <div class="container education-container">
            <div class="education-info">
                <h1 class="education__title">آموزش مفاهیم پایه</h1>
                <p class="education__desc">
                    ...........................................
                    {{--                    لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با--}}
                    {{--                    استفاده از طراحان <br />--}}
                    {{--                    گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان--}}
                    {{--                    که لازم است و برای--}}
                </p>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    @if(count($newest)>0)
                        @foreach($newest as $new_post)
                            <div class="right-education-sec-1">
                                <a href="#">
                                    <div class="right-education-sec-1__image">
                                        <img width="340px" height="166px"
                                             src="{{$new_post->blogImage}}"
                                             alt="تایتل آموزش"
                                        />
                                    </div>
                                    <div class="right-education-sec-1__title">
                                        <h3>{{$new_post->title}}</h3>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="col-12 col-md-6">
                    <div class="left-education-sec-1 h-100">
                        <a href="#">
                            <div class="right-education-sec-1__image h-100">
                                <img
                                    src="{{asset('theme/landing/images/education/education-3.png')}}"
                                    alt="تایتل آموزش"
                                    class="h-100"
                                />
                            </div>
                            <div
                                class="
                      right-education-sec-1__title
                      d-flex
                      items-center
                      justify-between
                    "
                            >
                                <h3>تایتل مقاله آموزش در اینجا قرار می گیرد</h3>

                                <div>
                                    <img
                                        src="{{asset('theme/landing/images/education/Play.png')}}"
                                        alt="پلی باتن"
                                        style="width: 33px"
                                    />
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section One -->

    <!-- Start Section Two -->
    <section class="trainings">
        <div class="container education-container">
            <div class="row">
                @if(count($posts)>0)
                    @foreach($posts as $post)
                        <div style="margin-top: 4rem" class="col-12 col-md-6">
                            <div class="training">
                                <a href="{{route('landing.post',$post->slug)}}">
                                    <img width="340px" height="191px"
                                         src="{{$post->blogImage}}"
                                         class="training__img"
                                         alt="تایتل مقاله"
                                    />

                                    <div class="training__info">
                                        <h3 class="training__title">
                                            {{$post->title}}
                                        </h3>
                                        <p class="training__desc">
                                            {!! \Illuminate\Support\Str::limit($post->description,20) !!}
                                        </p>

                                        <hr class="training__line"/>

                                        <div
                                            class="training__tags d-flex justify-between items-center"
                                        >
                      <span class="training__tag training__tag-blue">
                        {{$post->post_category->name}}
                      </span>

                                            <h5 class="training__calender">{{jdate_from_gregorian($post->created_at)}}</h5>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- End Section Two -->
@endsection
@section('script')

@endsection
