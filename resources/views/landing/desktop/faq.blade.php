@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
     سوالات متداول-روتیکس
@endsection
@section('header')
    <header class="header faqs-header">
        <div class="container">
            <nav>
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>

        </div>
        <div class="faqs-search-container">
            <div class="faqs-search">
                <h1 class="faqs-search__title">سوالی دارید؟ از ما بپرسید</h1>
                <form action="#" class="faqs-search__search-box">
                    <input type="search" placeholder="چه سوالی دارید؟" />
                </form>
            </div>
        </div>
    </header>
@endsection
@section('content')
    <section>
        <div class="container faqs-container">
            <div class="row">
                <div class="col-12">
                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">روتیکس چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            روتیکس یک صرافی ارز دیجیتال است که میتوانید از طریق درگاه امن آن اقدام به خرید ارزهای دیجیتال مثل بیت کوین کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">ارز دیجیتال چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در قرن بیست و یکم ارزهای دیجیتال در حال جایگزینی با ارزهای رایج مانند ریال و دلار و یورو و حتی طلا میباشد و از این پس تمام تبادلات مالی در سطح جهان و حتی خریدهای روزانه با ارز دیجیتال انجام خواهد شد. لذا استفاده از ارزهای دیجیتال در اینده نزدیک اجتناب ناپذیر خواهد بود.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور ارز دیجیتال بخرم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            با ثبت نام در سایت صرافی ارز دیجیتال روتیکس به راحتی میتوانید از طریق درگاه بانکی واریز ریالی انجام دهید و با امکانات سایت به راحتی آن را تبدیل به ارز دیجیتال دلخواه خود کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور ارز دیجیتال بفروشم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در صرافی روتیکس با استفاده از امکانات ساده و در عین حال قدرتمند و امن میتوانید انواع ارز دیجیتال را به ریال و یا هر نوع ارز دیجیتال دیگر تبدیل کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">کارمزد خرید و فروش ارز دیجیتال در روتیکس چقدر است؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            کارمزدها بستگی به سیستم بانکی کشور و کارمزد شبکه ارز دیجیتال متفاوت است و در صرافی روتیکس کمترین میزان کارمزدها را در بین تمامی صرافیها تجربه خواهید کرد.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چطور شروع کنم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            با <a href="{{route('register.form')}}" style="color: deepskyblue">ثبت نام</a> در صفحه مورد نظر به راحتی و فقط با ادرس ایمیل و شماره تلفن همراه میتوانید وارد پنل کاربری خود شده و از همین لحظه اقدام به خرید ارز دیجیتال کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">احراز هویت چیست؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            جهت استفاده از خدمات صرافی روتیکس نیازمند احراز هویت هستید که سطوح متفاوتی دارد و در سطح اول فقط با شماره تلفن همراه و ایمیل میتوانید از امکانات صرافی استفاده کنید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چه اطلاعاتی برای احراز هویت نیاز دارم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            در سطح اول شماره تلفن همراه و ایمیل نیاز است و جهت سطوح بالاتر استفاده از خدمات صرافی نیازمند احراز هویت به صورت اطلاعات حساب بانکی و کد ملی میباشید.
                        </p>
                    </div>

                    <div class="faq">
                        <div class="faq-info">
                            <h3 class="faq__title">چرا باید اطلاعات هویتی خودم را در اختیار روتیکس بگذارم؟</h3>

                            <button type="button">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="15"
                                    class="faq-arrow-down faq-arrow"
                                    height="8"
                                    viewBox="0 0 18 11.115"
                                >
                                    <path
                                        id="Icon_material-expand-more"
                                        data-name="Icon material-expand-more"
                                        d="M24.885,12.885,18,19.755l-6.885-6.87L9,15l9,9,9-9Z"
                                        transform="translate(-9 -12.885)"
                                        opacity="0.7"
                                    />
                                </svg>
                            </button>
                        </div>

                        <p class="faq__question">
                            روتیکس برای حفظ امنیت سرمایه ریالی و دیجیتالی شما نیازمند احراز هویت میباشد ، احراز هویت باعث میشود شخصی غیر از شما نتواند اقدام به برداشت یا جابجایی سرمایه شما کند.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Section Eight -->
    <section>
        <div class="forms">
            <div class="form">
                <div>
                    <h2>با ما در تماس باشید</h2>
                    <p>
                        ...............
                    </p>
                </div>

                <form action="#">
                    <div class="inputs">
                        <div class="input">
                            <label for="f-name">نام</label>
                            <input
                                type="text"
                                id="f-name"
                                placeholder="Enter your name"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name">نام خانوادگی</label>
                            <input
                                type="text"
                                id="l-name"
                                placeholder="Enter your last name"
                            />
                        </div>
                    </div>

                    <div class="inputs">
                        <div class="input">
                            <label for="f-name">نام</label>
                            <input
                                type="text"
                                id="f-name"
                                placeholder="Enter your name"
                            />
                        </div>

                        <div class="input">
                            <label for="l-name">نام خانوادگی</label>
                            <input
                                type="text"
                                id="l-name"
                                placeholder="Enter your last name"
                            />
                        </div>
                    </div>

                    <div class="inputs last-input">
                        <div class="input">
                            <label for="f-name">نام</label>
                            <input
                                type="text"
                                id="f-name"
                                placeholder="Enter your name"
                            />
                        </div>
                    </div>

                    <button type="submit" class="btn light-blue-btn send-btn" disabled>
                        ارسال پیام
                    </button>
                </form>
            </div>

            <div class="form-img">
                <img src="{{asset('theme/landing/images/contact-currency.png')}}" alt="فرم" />
            </div>
        </div>
    </section>
    <!-- End Section Eight -->
@endsection
@section('script')
    <script src="{{asset('theme/landing/scripts/faq.js')}}"></script>
@endsection
