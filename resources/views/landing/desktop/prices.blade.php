@extends('templates.landing_page.layout.desktop.master_page')
@section('title_browser')
    قیمت ها-روتیکس
@endsection
@section('style')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js" type="text/javascript"></script>
@endsection
@section('header')
    <header class="blog-page-header">
        <div class="container">
            <nav class="nav blog-nav">
                @include('templates.landing_page.layout.desktop.top_menu')
            </nav>
        </div>
    </header>
@endsection
@section('content')
    <section class="top-table-section">
        <div class="container">
            <div class="prices-top">
                <div class="prices-top__one b-left">
                    <div class="prices-top__one-one">
                        <h3>تعداد ارز های دیجیتال: ۷/۰۰۵</h3>
                        {{--                        <h3>بیشترین معامله تتر</h3>--}}
                    </div>

                    {{--                    <div class="prices-top__one-one">--}}
                    {{--                        <h3>معاملات ۲۴ ساعت گذشته: ۵۰/۲۳۰ میلیون دلار</h3>--}}
                    {{--                        <h3>کمترین معامله: ارز اکیتا</h3>--}}
                    {{--                    </div>--}}
                </div>
                <div class="prices-top__one b-left">
                    <div class="prices-top__one-one">
                        <h3>نرخ تتر: <span class="text-dark">{{rial_to_unit($tether,'rls',true)}} </span></h3>
                    </div>

                </div>

                {{--                <div class="prices-top__two b-left">--}}
                {{--                    <div class="prices-top__two-one prices-top__three-two">--}}
                {{--                        <h3>حجم بازار <span>۱۰۶ میلیون دلار</span></h3>--}}

                {{--                        <h3 class="text-danger">23.5-</h3>--}}
                {{--                    </div>--}}

                {{--                    <div>--}}
                {{--                        <!-- چارت در اینجا قرار می گیرد -->--}}
                {{--                        چارت در اینجا قرار می گیرد چارت در اینجا قرار--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="prices-top__three b-left d-flex">--}}
                {{--                    <h3 class="prices-top__three__title">سهم بازار:</h3>--}}

                {{--                    <div class="prices-top__three__crypto-container">--}}
                {{--                        <div class="prices-top__three__crypto">--}}
                {{--                            <h5>BTC : 44.20</h5>--}}
                {{--                            <img src="{{asset('theme/landing/images/prices/g41.png')}}" alt="بیت کوین"/>--}}
                {{--                        </div>--}}
                {{--                        <div class="prices-top__three__crypto">--}}
                {{--                            <h5>ETH : 44.20</h5>--}}
                {{--                            <img src="{{asset('theme/landing/images/prices/g57.png')}}" alt="بیت کوین"/>--}}
                {{--                        </div>--}}
                {{--                        <div class="prices-top__three__crypto">--}}
                {{--                            <h5>ADA : 44.20</h5>--}}
                {{--                            <img src="{{asset('theme/landing/images/prices/download.png')}}" alt="بیت کوین"/>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

                {{--                <div class="prices-top__three prices-top__three-two">--}}
                {{--                    <h3>تعداد ارز های دیجیتال: ۷/۰۰۵</h3>--}}
                {{--                    <h3>نرخ تتر: <span>{{number_format($usdt_rls/10)}} هزار تومان</span></h3>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>

    <section class="price-table-currency">
        <div class="container">
            <h1 class="price-table-currency__title">
                قیمت لحظه ای ارز های دیجیتال
            </h1>

            <div class="row items-center">
                <div class="col-12 col-md-10">
                    <div class="filter-btns-currency d-flex justify-between">
                        <button class="active">بر اساس قیمت</button>
                        <button disabled>اوج قیمت</button>
                        <button disabled>پر نوسان ترین</button>
                        <button disabled>محبوب ترین</button>
                        <button disabled>مبدل قیمت</button>
                        <button disabled>مقایسه ارزها</button>
                        <button disabled>بازار ایران</button>
                        <button disabled>نمای بازار</button>
                    </div>
                </div>
                {{--                <div class="col-12 col-md-2">--}}
                {{--                    <div class="d-flex items-center choose-currency">--}}
                {{--                        <h5>تومان</h5>--}}
                {{--                        <label class="switch">--}}
                {{--                            <input type="checkbox"/>--}}
                {{--                            <span class="slider round"></span>--}}
                {{--                        </label>--}}
                {{--                        <h5>USDT</h5>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

            <div class="row">
                <div class="col-12">
                    <table class="currency-table active">
                        <tr>
                            <td>
                                <div class="td-row">
                                    ارز دیجیتال
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>
                            <td>
                                <div class="td-row">
                                    قیمت
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    % 24h
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    % 7d
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    حجم بازار
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    حجم معاملات روزانه
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    نمودار تغییرات لحظه ای
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>

                            <td>
                                <div class="td-row">
                                    امکانات
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="9"
                                        height="17"
                                        viewBox="0 0 12.203 20.383"
                                    >
                                        <path
                                            id="Icon_material-unfold-more"
                                            data-name="Icon material-unfold-more"
                                            d="M17.217,8.262l4.214,4.214L23.3,10.6,17.217,4.5l-6.1,6.1L13,12.476Zm0,12.859L13,16.907l-1.874,1.874,6.088,6.1,6.1-6.1L21.43,16.907l-4.214,4.214Z"
                                            transform="translate(-11.115 -4.5)"
                                            opacity="0.7"
                                        />
                                    </svg>
                                </div>
                            </td>
                        </tr>
                        @foreach($currencies as $key=>$currency)
{{--                            @dd($currency['color'])--}}
                            <tr>
                                <td class="currency-title">
                                    <a href="{{route('landing.price_detail',$key)}}">
                                        <div class="currency-right-title">
                                            <img src="{{$currency['logo_url']?$currency['logo_url']:asset("theme/landing/images/currency/$key.png")}}" alt="{{$currency['currency']}}-logo"/>
                                           <div>
                                               <h5>{{$currency['currency']}}</h5>
                                               <p class="text__table-small">{{$currency['name_fa']}}</p>
                                           </div>
                                        </div>
                                    </a>


                                </td>
                                <td class="currency-last-price">
                                    <div class="currency-doloar-price">
                                        @if($currency['price']>=1)
                                            <h5>{{number_format($currency['price'],2)}}$</h5>
                                            <p>{{number_format(rial_to_unit($currency['rls'],'rls'))}} تومان</p>
                                        @else
                                            <h5>{{number_format($currency['price'],7)}}$</h5>
                                            <p>{{number_format(rial_to_unit($currency['rls'],'rls'),3)}} تومان</p>
                                        @endif
                                    </div>
                                </td>
                                <td class="currency-present-change">
                                    @if(isset($currency['1d']))
                                        @php
                                            $img=(float)$currency['1d']<0?'red_arrow':'green_arrow';
                                        @endphp
                                        <span
                                            class="currency-present-change {{(float)$currency['1d']<0?'red':'green'}}-currency">
                                                      <img src="{{asset("theme/landing/images/$img.svg")}}" alt="">
                                                    {{(float)$currency['1d']<0?number_format(substr($currency['1d'],1),2):number_format($currency['1d'],2)}}
                                        </span>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="currency-most-price">
                                    @if(isset($currency['7d']))
                                        @php
                                            $img=(float)$currency['7d']<0?'red_arrow':'green_arrow';
                                        @endphp
                                        <span
                                            class="currency-present-change {{(float)$currency['7d']<0?'red':'green'}}-currency">
                                                     <img src="{{asset("theme/landing/images/$img.svg")}}" alt="">
                                                    {{(float)$currency['7d']<0?number_format(substr($currency['7d'],1),2):number_format($currency['7d'],2)}}

                                            @else
                                                -
                                    @endif
                                </td>
                                <td class="cur rency-chart">
                                    <div class="currency-doloar-price">
                                        @php
                                            $volume_info=market_cap_volume($currency['market_cap']);
                                        @endphp
                                        @if($volume_info[0] != 0)
                                            <h5>{{$volume_info[0]}}</h5>
                                            <p>{{$volume_info[1]}}</p>
                                        @else
                                            -
                                        @endif
                                    </div>
                                </td>
                                <td class="currency-buying-btn">
                                    <div class="currency-doloar-price">
                                        @php
                                            $volume_info=market_cap_volume($currency['volume']);
                                        @endphp
                                        @if($volume_info[0] != 0)
                                            <h5>{{$volume_info[0]}}</h5>
                                            <p>{{$volume_info[1]}}</p>
                                        @else
                                            -
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="price-chart-table">
                                        <canvas id="{{$key}}_chart" width="500" height="150"></canvas>
                                        <script type="text/javascript" >
                                            setTimeout(function(){
                                                chart("{{$key}}_chart", "{{$currency['color']}}");
                                            },1000);
                                        </script>

                                        {{--                                        <svg--}}
                                        {{--                                            xmlns="http://www.w3.org/2000/svg"--}}
                                        {{--                                            width="95"--}}
                                        {{--                                            height="57"--}}
                                        {{--                                            viewBox="0 0 133 80"--}}
                                        {{--                                        >--}}
                                        {{--                                            <g--}}
                                        {{--                                                id="Group_1535"--}}
                                        {{--                                                data-name="Group 1535"--}}
                                        {{--                                                transform="translate(515 -1096.075)"--}}
                                        {{--                                            >--}}
                                        {{--                                                <rect--}}
                                        {{--                                                    id="Rectangle_108"--}}
                                        {{--                                                    data-name="Rectangle 108"--}}
                                        {{--                                                    width="128"--}}
                                        {{--                                                    height="75"--}}
                                        {{--                                                    transform="translate(-510 1096.075)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                />--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="Text"--}}
                                        {{--                                                    transform="translate(-503 1170.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="2"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.9" y="0">Text</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="Text-2"--}}
                                        {{--                                                    data-name="Text"--}}
                                        {{--                                                    transform="translate(-474 1170.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="2"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.9" y="0">Text</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="Text-3"--}}
                                        {{--                                                    data-name="Text"--}}
                                        {{--                                                    transform="translate(-445 1170.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="2"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.9" y="0">Text</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="Text-4"--}}
                                        {{--                                                    data-name="Text"--}}
                                        {{--                                                    transform="translate(-415 1170.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="2"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.9" y="0">Text</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="Text-5"--}}
                                        {{--                                                    data-name="Text"--}}
                                        {{--                                                    transform="translate(-386 1170.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="2"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.9" y="0">Text</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="_0"--}}
                                        {{--                                                    data-name="0"--}}
                                        {{--                                                    transform="translate(-511 1163.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="10"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.615" y="10">0</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="_20"--}}
                                        {{--                                                    data-name="20"--}}
                                        {{--                                                    transform="translate(-513 1146.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="10"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.615" y="10">2</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="_40"--}}
                                        {{--                                                    data-name="40"--}}
                                        {{--                                                    transform="translate(-513 1129.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="10"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.615" y="10">4</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="_60"--}}
                                        {{--                                                    data-name="60"--}}
                                        {{--                                                    transform="translate(-513 1112.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="10"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.615" y="10">6</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <text--}}
                                        {{--                                                    id="_80"--}}
                                        {{--                                                    data-name="80"--}}
                                        {{--                                                    transform="translate(-513 1096.075)"--}}
                                        {{--                                                    fill="#a3a3a3"--}}
                                        {{--                                                    font-size="10"--}}
                                        {{--                                                    font-family="Roboto-Regular, Roboto"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <tspan x="-1.615" y="10">8</tspan>--}}
                                        {{--                                                </text>--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_v_1"--}}
                                        {{--                                                    d="M-480,1101v67.074"--}}
                                        {{--                                                    transform="translate(-22.533 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#f0f0f0"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_v_2"--}}
                                        {{--                                                    d="M-388,1101v67.074"--}}
                                        {{--                                                    transform="translate(-85.353 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_v_3"--}}
                                        {{--                                                    d="M-295,1101v67.074"--}}
                                        {{--                                                    transform="translate(-148.855 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_v_4"--}}
                                        {{--                                                    d="M-203,1101v67.074"--}}
                                        {{--                                                    transform="translate(-211.675 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_v_5"--}}
                                        {{--                                                    d="M-110,1101v67.074"--}}
                                        {{--                                                    transform="translate(-275.177 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#f0f0f0"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_1"--}}
                                        {{--                                                    d="M-480,1312h117.356"--}}
                                        {{--                                                    transform="translate(-22.533 -147.34)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#f0f0f0"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_2"--}}
                                        {{--                                                    d="M-480,1259h117.356"--}}
                                        {{--                                                    transform="translate(-22.533 -111.197)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_3"--}}
                                        {{--                                                    d="M-480,1206h117.356"--}}
                                        {{--                                                    transform="translate(-22.533 -75.041)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_4"--}}
                                        {{--                                                    d="M-480,1153h117.356"--}}
                                        {{--                                                    transform="translate(-22.533 -38.885)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#bfbfbf"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Grid_5"--}}
                                        {{--                                                    d="M-480,1101h117.356"--}}
                                        {{--                                                    transform="translate(-22.533 -3.414)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#f0f0f0"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                />--}}
                                        {{--                                                <path--}}
                                        {{--                                                    id="Line_1"--}}
                                        {{--                                                    d="M-480,1134.37s14.59-12.37,29.18-12.37,14.59,31.718,29.5,31.718c14.59,0,14.59-9.2,29.18-9.2s29.5,18.4,29.5,18.4"--}}
                                        {{--                                                    transform="translate(-22.533 -17.753)"--}}
                                        {{--                                                    fill="none"--}}
                                        {{--                                                    stroke="#e54545"--}}
                                        {{--                                                    stroke-width="2"--}}
                                        {{--                                                />--}}
                                        {{--                                                <g--}}
                                        {{--                                                    id="Dot_11"--}}
                                        {{--                                                    transform="translate(-504 1115.075)"--}}
                                        {{--                                                    fill="red"--}}
                                        {{--                                                    stroke="rgba(0,0,0,0)"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <circle cx="1.5" cy="1.5" r="1.5" stroke="none"/>--}}
                                        {{--                                                    <circle cx="1.5" cy="1.5" r="1" fill="none"/>--}}
                                        {{--                                                </g>--}}
                                        {{--                                                <g--}}
                                        {{--                                                    id="Dot_12"--}}
                                        {{--                                                    transform="translate(-475 1103.075)"--}}
                                        {{--                                                    fill="red"--}}
                                        {{--                                                    stroke="rgba(0,0,0,0)"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1.5"--}}
                                        {{--                                                        cy="1"--}}
                                        {{--                                                        rx="1.5"--}}
                                        {{--                                                        ry="1"--}}
                                        {{--                                                        stroke="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1.5"--}}
                                        {{--                                                        cy="1"--}}
                                        {{--                                                        rx="1"--}}
                                        {{--                                                        ry="0.5"--}}
                                        {{--                                                        fill="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                </g>--}}
                                        {{--                                                <g--}}
                                        {{--                                                    id="Dot_13"--}}
                                        {{--                                                    transform="translate(-445 1135.075)"--}}
                                        {{--                                                    fill="red"--}}
                                        {{--                                                    stroke="rgba(0,0,0,0)"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <circle cx="1" cy="1" r="1" stroke="none"/>--}}
                                        {{--                                                    <circle cx="1" cy="1" r="0.5" fill="none"/>--}}
                                        {{--                                                </g>--}}
                                        {{--                                                <g--}}
                                        {{--                                                    id="Dot_14"--}}
                                        {{--                                                    transform="translate(-416 1126.075)"--}}
                                        {{--                                                    fill="red"--}}
                                        {{--                                                    stroke="rgba(0,0,0,0)"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1.5"--}}
                                        {{--                                                        cy="1"--}}
                                        {{--                                                        rx="1.5"--}}
                                        {{--                                                        ry="1"--}}
                                        {{--                                                        stroke="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1.5"--}}
                                        {{--                                                        cy="1"--}}
                                        {{--                                                        rx="1"--}}
                                        {{--                                                        ry="0.5"--}}
                                        {{--                                                        fill="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                </g>--}}
                                        {{--                                                <g--}}
                                        {{--                                                    id="Dot_15"--}}
                                        {{--                                                    transform="translate(-386 1144.075)"--}}
                                        {{--                                                    fill="red"--}}
                                        {{--                                                    stroke="rgba(0,0,0,0)"--}}
                                        {{--                                                    stroke-width="1"--}}
                                        {{--                                                >--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1"--}}
                                        {{--                                                        cy="1.5"--}}
                                        {{--                                                        rx="1"--}}
                                        {{--                                                        ry="1.5"--}}
                                        {{--                                                        stroke="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                    <ellipse--}}
                                        {{--                                                        cx="1"--}}
                                        {{--                                                        cy="1.5"--}}
                                        {{--                                                        rx="0.5"--}}
                                        {{--                                                        ry="1"--}}
                                        {{--                                                        fill="none"--}}
                                        {{--                                                    />--}}
                                        {{--                                                </g>--}}
                                        {{--                                            </g>--}}
                                        {{--                                        </svg>--}}
                                        {{--                                        <img src="{{asset('theme/landing/images/prices/table-chart.png')}}"--}}
                                        {{--                                             alt="نمودار"/>--}}
                                    </div>
                                </td>
                                <td>
                                    -
                                    {{--                                    <a href="#" class="more-btn-currency">--}}
                                    {{--                                        <svg--}}
                                    {{--                                            xmlns="http://www.w3.org/2000/svg"--}}
                                    {{--                                            width="20"--}}
                                    {{--                                            height="5"--}}
                                    {{--                                            viewBox="0 0 27 6"--}}
                                    {{--                                        >--}}
                                    {{--                                            <g--}}
                                    {{--                                                id="Icon_feather-more-horizontal"--}}
                                    {{--                                                data-name="Icon feather-more-horizontal"--}}
                                    {{--                                                transform="translate(-4.5 -15)"--}}
                                    {{--                                            >--}}
                                    {{--                                                <path--}}
                                    {{--                                                    id="Path_8583"--}}
                                    {{--                                                    data-name="Path 8583"--}}
                                    {{--                                                    d="M19.5,18A1.5,1.5,0,1,1,18,16.5,1.5,1.5,0,0,1,19.5,18Z"--}}
                                    {{--                                                    fill="none"--}}
                                    {{--                                                    stroke="#000"--}}
                                    {{--                                                    stroke-linecap="round"--}}
                                    {{--                                                    stroke-linejoin="round"--}}
                                    {{--                                                    stroke-width="3"--}}
                                    {{--                                                />--}}
                                    {{--                                                <path--}}
                                    {{--                                                    id="Path_8584"--}}
                                    {{--                                                    data-name="Path 8584"--}}
                                    {{--                                                    d="M30,18a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,30,18Z"--}}
                                    {{--                                                    fill="none"--}}
                                    {{--                                                    stroke="#000"--}}
                                    {{--                                                    stroke-linecap="round"--}}
                                    {{--                                                    stroke-linejoin="round"--}}
                                    {{--                                                    stroke-width="3"--}}
                                    {{--                                                />--}}
                                    {{--                                                <path--}}
                                    {{--                                                    id="Path_8585"--}}
                                    {{--                                                    data-name="Path 8585"--}}
                                    {{--                                                    d="M9,18a1.5,1.5,0,1,1-1.5-1.5A1.5,1.5,0,0,1,9,18Z"--}}
                                    {{--                                                    fill="none"--}}
                                    {{--                                                    stroke="#000"--}}
                                    {{--                                                    stroke-linecap="round"--}}
                                    {{--                                                    stroke-linejoin="round"--}}
                                    {{--                                                    stroke-width="3"--}}
                                    {{--                                                />--}}
                                    {{--                                            </g>--}}
                                    {{--                                        </svg>--}}
                                    {{--                                    </a>--}}
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{--                    {!! $currencies->links() !!}--}}
                </div>
            </div>
        </div>
    </section>

    <section class="circle-charts">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>بیشترین معاملات</h2>
                            <h2>در روتیکس</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>مجموع برداشت های</h2>
                            <h2>روزانه</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="circle-chart">
                        <div class="circle-chart__title">
                            <h2>بیشترین سود</h2>
                            <h2>در معاملات</h2>
                        </div>
                        <img src="{{asset('theme/landing/images/prices/chart-1.png')}}" alt="نمودار"/>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Section Six -->
    <section class="prices-mobile-app">
        <div class="container-secondary">
            <div class="row items-center">
                <div class="col-12 col-md-6">
                    <div class="what-currency-info">
                        <h5>اپلیکیشن موبایل رو فراموش نکنید</h5>
                        <div class="what-crypto-title">
                            <h2>همیشه و همه جا بازار رو چک کن</h2>
                            <span>با اپلیکیشن موبایل روتیکس</span>
                        </div>
                        <p class="what-crypto-desc">
                            پنل روتیکس در تلفن های هوشمند هم قابل استفاده است . به راحتی قابل دسترس است و با رابط کاربری
                            پیشرفته و آسان خود ، تجربه کار با صرافی در هر لحظه و هر مکان را با استفاده از تلفن هوشمند
                            برای کاربران خود ایجاد میکند.
                        </p>

                        <div class="download-btns">
                            <button class="btn">
                                Google Play
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="23"
                                    height="24"
                                    viewBox="0 0 23 24"
                                >
                                    <image
                                        id="Google_Play_logo"
                                        width="23"
                                        height="24"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAYCAYAAAARfGZ1AAAABHNCSVQICAgIfAhkiAAABURJREFUSEullGtsU3UYxp///5zTc9pu7bobG4OxMbnjBHSKREQSLkYlwgcMUUwYYBQ/+KGJCUbiZYOFyM0QohGCGITED+OD0URUmBO5GAzgsiGRTXDM0XXt2rVbL6fn5ntOwRgNhMuHrs3a83vf93me92Wvf3Fh7JCU2FIzGyGWNrZVy4XxyZ4i9BlxBAQRfWYY5S4vBEuHy8XggYlyVoqY8ic8ig+XInGsbOuH26MCbg54RaDADTAGtuyjn14JJTN7Jz1qYvwEq63GKmuu95a0d2uD9w+ftfWbTQxCs0UdzZibw7RaEV698DB1u7VIELr+MPox5l47b9jzfVDi2JHLAabFMKdhFJXj0/AYxYZfkPZIorWrSJR7mZW7e1nmfnIsyBnbwbkJVeMwLGBhQwq141KIpzl8kidTrHh2eiy+W5b4oALtzjWfdyAPBwME8iOjc2gGwzMNCcwcl8bQKBkjMhSI7niBy9USsJTtFShD2H0ZXsV/e0OfONQWZCQLJzjoD+cWMjRBzuRYMSeGh8ePYiglwmSGU9wvFnaXCIEmU0oekhUPfovEbp2W+a3tQQHYwZgFy4ZThLg9gcGRpQlWPRTF3HFJxDIUMZLM5DpEiYrwgg7JXdx8Pj16ZM3XFEt35v9RfOrLH4OcMwfOCE5syihJRBXTuoAsTbB6RhgLquKIqi7ne24ZMGQOUZRR2R5qn/iXuVkIWMchU3UvPXgz54uOnggy21CC21BbGrsIqHsuMCpAE1CBtVNDWFI5hKgmQRdFWEzApG97UHI2BFQVAAFvKwoLm1Do6oTXRSBqdvEPJ4PEcAy92bn9bhGciuY9MAWkLREb6q5haekwQvBiwnc98J8bACppG10mIBqARwaKfPvhL2sBl66wp0+fctLiyHFDFjLYAdvvoCk5vbJkckTxYs3kCDacPwV+PAxUKIBCD0oEJx+cAiJ9Dvjp//4l7LlzZxxZqNd/JHHgdiGC2iJzy4LqUxAuKkDjpla8luoEVk4AkloeZoPtFzUOL+2KqSIxqi5mz3edJUNtWUgjx618WpzDY3dNNTN+GemAG+uavsLSj48hViaisrEKnoWloBhRAfq9m8GULKQ0fX9iaKRF19kVtqLnF9KclsjmOR3nJXGK0DTZIgVZr4yX3j2KRZ+dQrKW2sulIaUSqHh1PJTl1UAkBVXXW1VVbxpJ5zoNNUdpk8BWXjuXl8U21DHV1plOGU2S8cnIuWS88E4b5n96FsmJMgQxS9OnIWdG4VKTMF8e024+Xr7Zo/PjnB7O5DRoWZVYItiq8IX8+tNtcXS2I0hFsgUyNC5j+aYTeGx/B0bqRHACB5hGp1hDzIp1qH3h5iviwJHJHy5Ehc9HG0a7of4LvjrxK61/3lBG+y2QyKpMHcOFZ986g9n7ujAyRYIi5lDIVQzqke6+7MD7DW7v4VRkEEOlCdS8Nw9lXsr6f+GNaqdzW27IjCx30d0TsXjjeTy493ekpkpwS3SPrZG4m2daOtK929sSl/BB1SyEB/oxWDJ8a/g662KQxKDO6eQSVKNgLwhexMwD/VCn23FJZkwM79T1xG6fqA32a1H8PNKNN8unIBq+fnt4I8EFgpMFoBUgcD/qP49joC5qmDy+x8OsXSpivbKZQTEtS29u8M7h663Lb+dgbjZI83lvRDHm4FVEamOHBSG6VTPDXVM902j1Q7SEaQQk4+7gL6ZPrlessn31G+OoOTjU1lV9sflS/HT70opH6KZcxwPuOoIP3Bt8WfTI2GknqrfUr02E9OqRbf3oiV8dvoAny6ffN/xvgcG0BE4DDGUAAAAASUVORK5CYII="
                                    />
                                </svg>
                            </button>
                            <button class="btn">
                                Apple Store
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="18"
                                    height="21"
                                    viewBox="0 0 18 21"
                                >
                                    <image
                                        id="Apple-Logo-black"
                                        width="18"
                                        height="21"
                                        xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABHNCSVQICAgIfAhkiAAAAWFJREFUOE+llM8rRFEUx40VVmI3ZEEjNCkLGykbZKMUqbe1UNamKGk2ivwHytoGCyJpsqDGShZIsZiNmcKKyY9E8Tm6p26Pabzj1qd77pnz/c599513Y0EQVEQY09ROwRKs+rpYBKM9hENO3Md8ZDGaQ7TghFfMbeGn+MuOqhC9OuE7cwvcWIyqEZ3DLqRAzH6M8I7GqRiFRsjDDmzBCyRhGEagEs5gDfbF1TfadoXhf7slcQ+dv+2E3CZMqNE6i7ESheXSTxQkxKiHIFuuusTvOfId8CZGKwSTRqNBdBk9o2vZmsHoEU2t6mRHnwYTkUgvNflGBRZxg9kHmnoo6qNtEEjvWMYMomU1miVYtLigkWPphWM5o3aCS6ORytLakKdkuv5hdqBG/Zh894NxNPjfmn9xRfGTe2reN6ohIb1RF8HlhNpufWu+rpWF3ALaVw/Eh3AHze4NyUUn4wIG4FkWX2MdTncYDySxAAAAAElFTkSuQmCC"
                                    />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="mobile-images">
                        <img src="{{asset('theme/landing/images/prices/mobile.png')}}" alt="برنامه موبایل"/>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Six -->
@endsection
@section('script')
    <script src="{{asset('theme/user/scripts/currency_chart.js')}}"></script>
@endsection
