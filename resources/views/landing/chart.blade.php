<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container">
    <div id="tradingview_81a31"></div>
    <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/AAPL/" rel="noopener" target="_blank"><span class="blue-text">ETH</span></a> by TradingView</div>
    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
    <script type="text/javascript">
        new TradingView.MediumWidget(
            {
                "symbols": [
                    [
                        "ETHUSDT",
                        "ETHUSDT"
                    ],
                ],
                "chartOnly": false,
                "width": 1000,
                "height": 400,
                "locale": "en",
                "colorTheme": "light",
                "gridLineColor": "rgba(240, 243, 250, 0)",
                "trendLineColor": "#2962FF",
                "fontColor": "#787B86",
                "underLineColor": "rgba(41, 98, 255, 0.3)",
                "underLineBottomColor": "rgba(41, 98, 255, 0)",
                "isTransparent": false,
                "autosize": true,
                "showFloatingTooltip": false,
                "scalePosition": "right",
                "scaleMode": "Normal",
                "container_id": "tradingview_81a31"
            }
        );
    </script>
</div>
<!-- TradingView Widget END -->
