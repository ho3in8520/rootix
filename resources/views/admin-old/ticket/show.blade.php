@extends('templates.admin.master_page')
@section('content')
    <div class="container-fluid" >
        <div class="row app-row">
            <div class="col-12 chat-app">

                <div class="separator mb-5"></div>

                <div class="scroll">
                    <div class="scroll-content">
                        @foreach($ticket->ticket_details as $tickets)
                        <div class="card d-inline-block mb-3 {{$tickets->type == 1 ? 'float-left' : 'float-right'}} mr-2">
                            <div class="position-absolute pt-1 pr-2 r-0">
                                <span class="text-extra-small text-muted">{{jdate_from_gregorian($ticket->created_at,'H:i     Y/m/d ')}}</span>
                            </div>
                            @if($tickets->type == 2)
                            <div class="position-absolute pt-1 pl-2 ">
                                <form action="{{route('ticket.destroy',$tickets->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button  onclick="return confirm('آیا با حذف این پیام موافقید؟')" type="submit" style="border: none;background-color: none">
                                        <i class="text-extra-small bg-danger text-white text-left glyph-icon simple-icon-ban p-2" style="cursor: pointer"></i>
                                    </button>
                                </form>
                            </div>
                            @endif
                            <div class="card-body">
                                <div class="d-flex flex-row pb-2">
                                    <a class="d-flex" href="#">
                                        <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-1.jpg')}}"
                                             class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                                    </a>
                                    <div class=" d-flex flex-grow-1 min-width-zero">
                                        @if($tickets->type == 2)
                                            <div class="min-width-zero">
                                                <p class="mb-0 truncate list-item-heading">{{auth()->guard('admin')->user()->email}}</p>
                                            </div>
                                        @else
{{--                                        @foreach($tickets->user as $user)--}}
{{--                                            @dd($user)--}}
                                        <div
                                            class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                                <div class="min-width-zero">
                                                    <p class="mb-0 truncate list-item-heading">{{$tickets->user->first_name ? $tickets->user->first_name.' '.$tickets->user->lastname : 'بدون نام'}}</p>
                                                    <span>{{$tickets->user->email}}</span>
                                                </div>
                                        </div>
{{--                                        @endforeach--}}
                                        @endif
                                    </div>
                                </div>

                                <div class="chat-text-left">
                                    <p class="mb-0 text-semi-muted">
                                        {{$tickets->description}}
                                    </p>
                                    @if(isset($tickets->files[0]))
                                    <hr/>
                                    <span>

                                        <div class="glyph">
                                            <a class="btn btn-primary  default" title="نمایش" href="{{getImage($tickets->files[0]->path)}}">
                                                <i class="glyph-icon iconsminds-file"></i>
                                            </a>
                                        </div>
                                    </span>
                                    @endif
                                </div>

                            </div>

                        </div>


                        <div class="clearfix"></div>
                        @endforeach
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="app-menu mb-2">
        <ul class="nav nav-tabs card-header-tabs ml-0 mr-0 mb-1" role="tablist">
            <li class="nav-item w-50 text-center">
                <a class="nav-link active" id="first-tab" data-toggle="tab" href="#firstFull" role="tab"
                   aria-selected="true">اطلاعات تیکت</a>
            </li>
            <li class="nav-item w-50 text-center">
                <a class="nav-link" id="second-tab" data-toggle="tab" href="#secondFull" role="tab"
                   aria-selected="false">جزییات</a>
            </li>
        </ul>

        <div class="p-4 h-100">
            <div class="tab-content h-100">
                <div class="tab-pane fade show active  h-100" id="firstFull" role="tabpanel"
                     aria-labelledby="first-tab">

                    <div class="scroll">

                        <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                            <div class="d-flex flex-grow-1 min-width-zero col-md-12">
                                <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <h6 class=" mb-0 truncate">تاریخ</h6>
                                        </a>
                                        <p class="mb-1 text-muted">{{jdate_from_gregorian($ticket->created_at,'H:i:s  Y-m-d')}}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                            <div class="d-flex flex-grow-1 min-width-zero col-md-12">
                                <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <h6 class=" mb-0 truncate">کد کاربری</h6>
                                        </a>

                                        <p class="mb-1 text-muted">{{$ticket->user[0]->code}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                            <div class="d-flex flex-grow-1 min-width-zero col-md-12">
                                <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <h6 class=" mb-0 truncate">عنوان</h6>
                                        </a>
                                        <p class="mb-1 text-muted">{{$ticket->title}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                            <div class="d-flex flex-grow-1 min-width-zero col-md-6">
                                <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <h6 class=" mb-0 truncate">فوریت</h6>
                                        </a>
                                        @switch($ticket->priority)
                                            @case(1)
                                            کم
                                            @break
                                            @case(2)
                                            متوسط
                                            @break
                                            @case(3)
                                            زیاد
                                            @break
                                            @default
                                            نا معلوم
                                        @endswitch
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <h6 class=" mb-2 truncate">ارجاع دادن</h6>
                            </div>

                            <div class=" mx-auto col-md-6" >
                                <select class="form-control select" id="select">
                                    <option value="0"></option >
                                    <option value="1">1</option >
                                    <option value="2">2</option >
                                    <option value="3">3</option >
                                    <option value="4">4</option >
                                </select>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="  col-md-6 " >
                                <select class="form-control d-none top1" id="top1">
                                    <option value="0"></option >
                                    <option value="1">1</option >
                                    <option value="2">1</option >
                                    <option value="3">1</option >
                                </select>
                                <select class="form-control d-none top2" id="top2">
                                    <option value="0"></option >
                                    <option value="1">2</option >
                                    <option value="2">2</option >
                                    <option value="3">2</option >
                                </select>
                                <select class="form-control d-none top3" id="top3">
                                    <option value="0"></option >
                                    <option value="1">3</option >
                                    <option value="2">3</option >
                                    <option value="3">3</option >
                                </select>
                                <select class="form-control d-none top4" id="top4">
                                    <option value="0"></option >
                                    <option value="1">4</option >
                                    <option value="2">4</option >
                                    <option value="3">4</option >
                                </select>
                            </div>
                        </div>
                        <div class="mt-2">
                            <button type="button" class="btn btn-success d-none submit2" id="submit">ثبت</button>
                        </div>

                    </div>
                </div>

                <div class="tab-pane fade  h-100" id="secondFull" role="tabpanel" aria-labelledby="second-tab">
                    <div class="scroll">
                        <div class="scroll">

                            <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                                <div class="d-flex flex-grow-1 min-width-zero col-md-12">
                                    <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                        <div class="min-width-zero">
                                            <a href="#">
                                                <h6 class=" mb-0 truncate">دپارتمان</h6>
                                            </a>
                                            <p class="mb-1 text-muted">{{$ticket->role_id==1?'پشتیبانی':'فنی'}}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                                <div class="d-flex flex-grow-1 min-width-zero col-md-12">
                                    <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                        <div class="min-width-zero">
                                            <a href="#">
                                                <h6 class=" mb-0 truncate">کاربر ارجاع داده شده</h6>
                                            </a>


                                            <p class="mb-1 text-muted"></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row mb-1 border-bottom pb-3 mb-3 row">

                                <div class="d-flex flex-grow-1 min-width-zero col-md-6">
                                    <div class="pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                        <div class="min-width-zero">
                                            <a href="#">
                                                <h6 class=" mb-0 truncate">فوریت</h6>
                                            </a>
                                            @switch($ticket->priority)
                                                @case(1)
                                                کم
                                                @break
                                                @case(2)
                                                متوسط
                                                @break
                                                @case(3)
                                                زیاد
                                                @break
                                                @default
                                                نا معلوم
                                            @endswitch
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="tab-pane fade  h-100" id="secondFull" role="tabpanel" aria-labelledby="second-tab">
                    <div class="scroll">

                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-1.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">فاطمه کاظمی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-2.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">سینا کرمی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-3.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">امین بدیعی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-4.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">مطهره تقوی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-5.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">سیمین بختیاری</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-2.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">سینا کرمی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-3.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">امین بدیعی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-4.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">مطهره تقوی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-3.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">امین بدیعی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-4.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">مطهره تقوی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-5.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">سیمین بختیاری</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row mb-3 border-bottom pb-3">
                            <a class="d-flex" href="#">
                                <img alt="Profile Picture" src="{{asset('theme/admin/img/profiles/l-2.jpg')}}"
                                     class="img-thumbnail border-0 rounded-circle mr-3 list-thumbnail align-self-center xsmall">
                            </a>
                            <div class="d-flex flex-grow-1 min-width-zero">
                                <div
                                    class="m-2 pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
                                    <div class="min-width-zero">
                                        <a href="#">
                                            <p class="mb-0 truncate">سینا کرمی</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <a class="app-menu-button d-inline-block d-xl-none" href="#">
            <i class="simple-icon-options"></i>
        </a>
    </div>

    <form action="{{route('comment.admin.store',$ticket->id)}}" method="post" >
    @csrf
    <div class="chat-input-container d-flex justify-content-between align-items-center" style="position: sticky;bottom: 0;margin-right: -60px">

        <input class="form-control flex-grow-1"  name="description" type="text" placeholder="نوشتن پیام...">
        <div>
            <label type="button" for="upload_file"  class="btn btn-outline-primary icon-button large mt-2">
                <i class="simple-icon-paper-clip" ></i>
            </label>
            <button type="button" class="btn btn-primary icon-button large ajaxStore">
                <i class="simple-icon-arrow-right"></i>
            </button>
            <input type="file" id="upload_file" onchange="upload()" name="file" class="d-none">
            <span id="name_img" style="color: red"></span>
        </div>

    </div>
    </form>
@endsection
@section('script')
    <script>
        function upload(){
            var fileInput = document.getElementById('upload_file');
            $('#name_img').text(fileInput.files[0].name);
        }


        // var filename = fileInput.files[0].name;
        // alert(filename);
        $(document).ready(function() {
            $(document).on('change','#select',function () {
                let select1 = $('#select').val();
                $.data(document,"select",$('.select').val());

                var sub_select = '';
                switch(select1) {
                    case '0':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').addClass('d-none');
                        break;
                    case '1':
                        $('#top1').removeClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document,"sub_select",$('.top1').val());

                        $(document).on('change','#top1',function () {
                        $.data(document,"sub_select",$('#top1').val());
                        switch(sub_select) {
                            case '0':
                                $.data(document,"sub_select",$('#top1').val());
                                $('#submit').addClass('d-none');
                                break;
                            default:
                                $('#submit').removeClass('d-none');
                        }
                        });
                        break;
                    case '2':
                        $('#top1').addClass('d-none');
                        $('#top2').removeClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document,"sub_select",$('.top2').val());
                        $(document).on('change','#top2',function () {
                            var sub_select1 = $('.top2').val();
                            $.data(document,"sub_select",$('#top2').val());
                            switch(sub_select1) {
                                case '0':
                                    $.data(document,"sub_select",$('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    case '3':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').removeClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');
                        $.data(document,"sub_select",$('.top3').val());
                        $(document).on('change','#top3',function () {
                            var sub_select1 = $('.top3').val();
                            $.data(document,"sub_select",$('#top3').val());
                            switch(sub_select1) {
                                case '0':
                                    $.data(document,"sub_select",$('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    case '4':
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').removeClass('d-none');
                        $('#submit').removeClass('d-none');

                        $.data(document,"sub_select",$('.top4').val());
                        $(document).on('change','#top4',function () {
                            var sub_select1 = $('.top4').val();
                            $.data(document,"sub_select",$('#top4').val());

                            switch(sub_select1) {
                                case '0':
                                    $.data(document,"sub_select",$('#top1').val());
                                    $('#submit').addClass('d-none');
                                    break;
                                default:
                                    $('#submit').removeClass('d-none');
                            }
                        });
                        break;
                    default:
                        $('#top1').addClass('d-none');
                        $('#top2').addClass('d-none');
                        $('#top3').addClass('d-none');
                        $('#top4').addClass('d-none');
                        $('#submit').removeClass('d-none');
                }
            });
            $(document).on('click','.submit2',function () {
                send_ajax($.data(document, "select"),$.data(document, "sub_select"));
            });

        });


        function send_ajax(id1,id2){
            console.log(id1,id2)
            $.ajax({
                url: '{{route('ticket.route',$ticket->id)}}',
                type: 'post',
                data: {from_id: id1,to_id:id2,"_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    swal('موفق',data.msg,'success');
                    setTimeout(function () {
                        location.href = "{{route('ticket.admin.index')}}"
                    },2000)
                }
            });
        }

    </script>
@endsection
