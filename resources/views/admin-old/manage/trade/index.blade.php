@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کارمزد و برداشت ترید
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت کارمزد ترید</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('trad-fee-update')}}" method="post">
                                @csrf
                                <input type="hidden" id="min" name="fee_type" value="withdraw_least_amount">
                                <div class="record">
                                    <div class="row row-btt">

                                        <div class="row withdraw">
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">کارمزد ترید روتیکس</label>
                                                    <input name="trad_fee"
                                                           type="text" class="form-control" style="width: 200px"
                                                           value="{{$trad_fee->extra_field1}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت حداقل برداشت ارزها</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('trad-fees-update')}}" method="post">
                                @csrf
                                <input type="hidden" id="min" name="fee_type" value="withdraw_least_amount">
                                <div class="record">
                                    <div class="row row-btt">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group ">
                                                    <label class="col-12">نوع ارز</label>
                                                    <select class="form-control currency_name" id="min_currency_name"
                                                            name="unit"
                                                            autocomplete="off">
                                                        @foreach($currencies as $key =>$value)
                                                            <option value="{{ $key }}">{{ $key }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row withdraw">
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">حداقل برداشت کوینکس</label>

                                                    <input name="min_withdraw" id="min_withdraw" type="text"
                                                           class="form-control" style="width: 200px"
                                                           value="{{$usdt['coinex']->withdraw_least_amount}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">حداقل برداشت روتیکس</label>
                                                    <input name="min_withdraw_rootix" id="min_withdraw_rootix"
                                                           type="text" class="form-control" style="width: 200px"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{$usdt['usdt_min_withdraw_rootix']}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container fluid -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت کارمزد برداشت ارزها</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('trad-fees-update')}}" method="post">
                                @csrf
                                <input type="hidden" id="fee_type" name="fee_type" value="withdraw_tx_fee">
                                <div class="record">
                                    <div class="row row-btt">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group ">
                                                    <label class="col-12">نوع ارز</label>
                                                    <select class="form-control currency_name" id="fee_currency_name"
                                                            name="unit"
                                                            autocomplete="off">
                                                        @foreach($currencies as $key =>$value)
                                                            <option value="{{ $key }}">{{ $key }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row withdraw">
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">کارمزد برداشت کوینکس</label>

                                                    <input name="fee_withdraw" id="fee_withdraw" type="text"
                                                           class="form-control" style="width: 200px"
                                                           value="{{$usdt['coinex']->withdraw_tx_fee}}" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">کارمزد برداشت روتیکس</label>
                                                    <input name="fee_withdraw_rootix" id="fee_withdraw_rootix"
                                                           type="text" class="form-control" style="width: 200px"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{$usdt['usdt_fee_withdraw_rootix']}}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- Button trigger modal -->
@section('script')

    <script>
        $('#min_currency_name').on('change', function () {
            let name = $(this).val();
            let fee_type = $('#min').val();
            let _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('get-currency-fees')}}",
                type: "get",
                data: {
                    name: name,
                    fee_type: fee_type,
                    _token: _token
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('#min_withdraw').val(numberFormat(response.coinex));
                        $('#min_withdraw_rootix').val(numberFormat(response.rootix));
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $('#fee_currency_name').on('change', function () {
            let name = $(this).val();
            let fee_type = $('#fee_type').val();
            let _token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: "{{route('get-currency-fees')}}",
                type: "get",
                data: {
                    name: name,
                    fee_type: fee_type,
                    _token: _token
                },
                success: function (response) {
                    console.log(response);
                    if (response) {
                        $('#fee_withdraw').val(numberFormat(response.coinex));
                        $('#fee_withdraw_rootix').val(numberFormat(response.rootix));
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    </script>
@endsection
