@extends('templates.admin.master_page')
@section('title_browser')
    مدیریت کارمزد و برداشت ارزها
@endsection

@section('content')
    <div class="page-content-wrapper ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت کارمزد سواپ ارزها</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <!-- end page title -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('manage-swap-update')}}" method="post">
                                @csrf
                                <div class="record">
                                    <div class="row row-btt">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group ">
                                                    <label class="col-12">نوع ارز</label>
                                                    <select class="form-control" id="unit1" onchange="changeUnit()"
                                                            name="unit" autocomplete="off">
                                                        @foreach($units as $row)
                                                            <option value="{{ $row->unit }}"
                                                                    @if($row->unit == 'rial' || $row->unit == 'rls') selected @endif>{{ $row->unit }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form">
                                        @if(count($result) > 0)
                                            @foreach($result as $item)
                                                <div class="row row-btt">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <input type="text" name="percent[]"
                                                                   value="{{ is_numeric($item->extra_field3)?$item->extra_field3:'' }}"
                                                                   onkeyup="($(this).val(numberFormat($(this).val())))"
                                                                   class="form-control loan_max_amount"
                                                                   placeholder="مقدار کارمزد">
                                                            <input type="hidden" name="id[]" value="{{ $item->id }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <hr>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control loan_max_amount"
                                                                   placeholder="از" name="from[]"
                                                                   onkeyup="($(this).val(numberFormat($(this).val())))"
                                                                   value="{{ number_format(floor($item->extra_field1 *100)/100, 2) }}">
                                                            <input type="text" class="form-control loan_max_amount"
                                                                   placeholder="تا" name="to[]"
                                                                   onkeyup="($(this).val(numberFormat($(this).val())))"
                                                                   value="{{ number_format(floor($item->extra_field2 *100)/100, 2)}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-sm btn-info add"><i
                                                                class="simple-icon-plus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-sm btn-danger remove"><i
                                                                class="simple-icon-minus"></i></button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="row row-btt">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" name="percent[]" class="form-control"
                                                               placeholder="مقدار کارمزد"
                                                               onkeyup="($(this).val(numberFormat($(this).val())))">
                                                        <input type="hidden" name="id[]">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <hr>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="از"
                                                               name="from[]"
                                                               onkeyup="($(this).val(numberFormat($(this).val())))">
                                                        <input type="text" class="form-control" placeholder="تا"
                                                               name="to[]"
                                                               onkeyup="($(this).val(numberFormat($(this).val())))">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-sm btn-info add"><i
                                                            class="simple-icon-plus"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger remove"><i
                                                            class="simple-icon-minus"></i></button>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت کارمزد و حداقل برداشت ارزها</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('manage-withdraw-update')}}" method="post">
                                @csrf
                                <div class="record">
                                    <div class="row row-btt">
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <div class="input-group ">
                                                    <label class="col-12">نوع ارز</label>
                                                    <select class="form-control" id="unit2"
                                                            onchange="changeUnitForWithdraw()" name="unit"
                                                            autocomplete="off">
                                                        @foreach($units as $row)
                                                            <option value="{{ $row->unit }}"
                                                                    @if($row->unit == 'rial' || $row->unit == 'rls') selected @endif>{{ $row->unit }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row withdraw">
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">حداقل برداشت</label>

                                                    <input name="min_withdraw" type="text" class="form-control"
                                                           style="width: 200px"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{ $min_mithdraw }}">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <div class="input-group ">
                                                    <label class="col-12">کارمزد برداشت</label>
                                                    <input name="fee_withdraw" type="text" class="form-control"
                                                           style="width: 200px"
                                                           onkeyup="($(this).val(numberFormat($(this).val())))"
                                                           value="{{  $fee_mithdraw }}">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mt-3">مدیریت کارمزد انتقال ارز های از کاربران به کیف پول
                                    ادمین</h4>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title-box -->
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('manage-fee-withdraw-admin-update')}}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group ">
                                            <label class="col-12">trc10</label>
                                            <input class="form-control" name="fee_withdraw_admin_trc10"
                                                   value="{{ (!empty($fee_mithdraw_admin))?$fee_mithdraw_admin->trc10:'' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group ">
                                            <label class="col-12">trc20</label>
                                            <input class="form-control" name="fee_withdraw_admin_trc20"
                                                   value="{{ (!empty($fee_mithdraw_admin))?$fee_mithdraw_admin->trc20:'' }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <button type="button" class="btn btn-success ajaxStore">ثبت</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container fluid -->
    </div>
@endsection
<!-- Button trigger modal -->
@section('script')
    {{--    <script src="https://cdn.jsdelivr.net/npm/cleave.js@1.5.3/dist/cleave.min.js"></script>--}}

    <script>
        document.querySelectorAll('.loan_max_amount').forEach(inp => new Cleave(inp, {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        }))


        $(document).ready(function () {
            $(document).on('click', '.add', function () {
                var form = $(this).closest('.row-btt').clone();
                $(form).find("input").val("");
                $(".record").append(form);
            });

            $(document).on('click', '.remove', function () {
                var length = $('.row-btt').length;
                if (length > 1) {
                    $(this).closest('.row-btt').remove();
                }
            });
        });

        function changeUnit() {

            var unit = $('#unit1').val();

            $.ajax({
                url: '{{route('change-unit')}}',
                type: 'post',
                data: {unit: unit, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    $(".form").html(data)
                }
            });
        }

        function changeUnitForWithdraw() {
            var unit = $('#unit2').val();

            $.ajax({
                url: '{{route('change-unit-for-withdraw')}}',
                type: 'post',
                data: {unit: unit, "_token": "{{ csrf_token() }}"},
                headers:
                    {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                success: function (data) {
                    console.log(data)
                    $(".withdraw").html(data)
                }
            });
        }
    </script>
@endsection
