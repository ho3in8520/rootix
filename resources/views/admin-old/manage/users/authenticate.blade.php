@extends('templates.admin.master_page')

@section('title_browser')
    ویرایش اطلاعات کاربر
@endsection
@section('style')
    <style>
        .disabled {
            background-color: #f3f3f3 !important;
        }
    </style>
@endsection
@php
$array_steps=status_steps_auth_user($step_forms,$step_completed, 'all','array');
$array_steps_notif=status_steps_auth_user($step_forms,$step_completed, 'all','notif');
@endphp
@section('content')
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active {{$array_steps['step.verify-mobile']['status']==-1?'disabled':'' }}"
                       id="mobile-email-tab" data-toggle="tab" href="#mobile-email" role="tab"
                       aria-controls="mobile-email" aria-selected="true"> اطلاعات
                        تماس {!!$array_steps_notif['step.verify-mobile'] !!} </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $array_steps['step.upload']['status']==-1?'disabled':'' }}"
                       id="documents-tab" data-toggle="tab" href="#documents" role="tab"
                       aria-controls="documents" aria-selected="false">
                        مدارک {!! $array_steps_notif['step.upload'] !!}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $array_steps['step.information']['status']==-1?'disabled':'' }}"
                       id="information-tab" data-toggle="tab" href="#information" role="tab"
                       aria-controls="information" aria-selected="false"> آدرس و
                        تلفن {!! $array_steps_notif['step.information'] !!}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ $array_steps['step.bank']['status']==-1?'disabled':'' }}"
                       id="bank-tab" data-toggle="tab" href="#bank" role="tab"
                       aria-controls="bank" aria-selected="false"> اطلاعات
                        بانکی {!! $array_steps_notif['step.bank'] !!}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="mobile-email" role="tabpanel"
                     aria-labelledby="mobile-email-tab">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره موبایل</label>
                                        <input type="text" class="form-control" value="{{$user->mobile}}"
                                               id="inputEmail4" placeholder="شماره موبایل" disabled>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">ایمیل</label>
                                        <input type="text" class="form-control" value="{{$user->email}}"
                                               id="inputEmail4" placeholder="ایمیل" disabled>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                    <div class="form-row">
                        <div class="col-xs-4 col-lg-2 col-12 mr-4">
                            <div class="card bg-dark text-white">
                                <img class="card-img"
                                     src="{{ (!empty($images['national']))?getImage($images['national']->path):asset('theme/admin/img/cards/thumb-1.jpg') }}"
                                     alt="Card image">
                            </div>
                            <button type="button" class="btn btn-outline-primary mt-2 " data-toggle="modal"
                                    data-target="#exampleModalPopovers">
                                کارت ملی
                            </button>
                        </div>
                        <div class="col-xs-4 col-lg-2 col-12 mb-4">
                            <div class="card bg-dark text-white">
                                <img class="card-img"
                                     src="{{ (!empty($images['phone_bill']))?getImage($images['phone_bill']->path):asset('theme/admin/img/cards/thumb-1.jpg') }}"
                                     alt="Card image">
                            </div>
                            <button type="button" class="btn btn-outline-primary mt-2 " data-toggle="modal"
                                    data-target="#exampleModalPopovers2">
                                قبض تلفن
                            </button>
                        </div>
                    </div>
                    @if($array_steps['step.upload']['status']==1 ||$array_steps['step.upload']['status']==0)
                    <div class="row">
                            <a class="btn btn-success text-white  mt-3"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                صلاحیت</a>
                            <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد صلاحیت</a>
                    </div>
                        @elseif($array_steps['step.upload']['status']==3)
                        <div class="row">
                            <a class="btn btn-success text-white  mt-3"
                               href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.upload']['id'])}}">تایید
                                صلاحیت</a>
                            <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد صلاحیت</a>
                        </div>
                    @elseif($array_steps['step.upload']['status']==2)
                        <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.upload']['id'] }})">رد صلاحیت</a>
                    @endif
                    <div id="exampleModalPopovers" class="modal fade show" tabindex="-1" role="dialog"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalPopoversLabel">تصویر</h5>

                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center">
                                    <img class="card-img"
                                         src="{{(!empty($images['national']))?getImage($images['national']->path):asset('theme/admin/img/cards/thumb-1.jpg')}}"
                                         alt="Card image">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        بستن
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="exampleModalPopovers2" class="modal fade show" tabindex="-1" role="dialog"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalPopoversLabel">تصویر 2</h5>

                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center">
                                    <img class="card-img"
                                         src="{{(!empty($images['phone_bill']))?getImage($images['phone_bill']->path):asset('theme/admin/img/cards/thumb-1.jpg')}}"
                                         alt="Card image">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        بستن
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="information" role="tabpanel" aria-labelledby="information-tab">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">نام</label>
                                        <input disabled type="text" class="form-control" value="{{$user->first_name}}"
                                               id="inputEmail4" placeholder="نام">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">نام خانوادگی</label>
                                        <input disabled type="text" class="form-control" value="{{$user->last_name}}"
                                               id="inputPassword4" placeholder="نام خانوادگی">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">جنسیت</label>
                                        <input disabled type="email" class="form-control" value=
                                        @switch($user->gender_id)
                                        @case(1)
                                            مرد
                                               @break
                                               @case(2)
                                               زن
                                               @break
                                               @default
                                               نامشخص
                                               @endswitch
                                               id="inputEmail4" placeholder="جنسیت">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">تاریخ تولد</label>
                                        <input disabled type="text" class="form-control" value="{{$user->birth_day}}"
                                               id="inputEmail4" placeholder="تاریخ تولد">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputPassword4">کد ملی</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{$user->national_code}}" id="inputPassword4"
                                               placeholder="کد ملی">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره تلفن ثابت</label>
                                        <input disabled type="email" class="form-control" value="{{$user->phone}}"
                                               id="inputEmail4" placeholder="شماره تلفن ثابت">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputCity">شهر</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{!empty($user->city_name->name) ? $user->city_name->name : ' '}}"
                                               id="inputCity">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputState">استان</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{!empty($user->state_name->name) ? $user->state_name->name : ' '}}"
                                               id="inputState">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="inputCity">آدرس</label>
                                        <textarea type="text" class="form-control"
                                                  id="inputCity" disabled>{{$user->address}}</textarea>
                                    </div>

                                </div>
                                @if($array_steps['step.information']['status']==1 || $array_steps['step.information']['status']==0)
                                    <div class="row">
                                        <a class="btn btn-success text-white  mt-3"
                                           href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                            صلاحیت</a>
                                        <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد صلاحیت</a>
                                    </div>
                                @elseif($array_steps['step.information']['status']==3)
                                    <div class="row">
                                        <a class="btn btn-success text-white  mt-3"
                                           href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.information']['id'])}}">تایید
                                            صلاحیت</a>
                                        <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد صلاحیت</a>
                                    </div>
                                @elseif($array_steps['step.information']['status']==2)
                                    <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.information']['id'] }})">رد صلاحیت</a>
                                @endif
                            </form>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="bank-tab">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4"> فرم</h5>

                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">نام بانک</label>
                                        <input disabled type="text" class="form-control"
                                               value="{{ (!empty($user->bank[0]))?$user->bank[0]->name:'' }}"
                                               id="inputEmail4" placeholder="نام بانک">
                                    </div>
                                    {{--                                    @foreach($user->bank as $bank)--}}
                                    {{--                                    <div class="form-group col-md-4">--}}
                                    {{--                                        <label for="inputPassword4">شماره حساب</label>--}}
                                    {{--                                        <input disabled type="text" class="form-control" value="{{$bank->account_number}}" id="inputPassword4" placeholder="شماره حساب">--}}
                                    {{--                                    </div>--}}
                                    {{--                                    @endforeach--}}
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره شبا</label>
                                        <input disabled type="email" class="form-control"
                                               value="{{ (!empty($user->bank[0]))?$user->bank[0]->sheba_number:'' }}"
                                               id="inputEmail4" placeholder="شماره شبا">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره حساب</label>
                                        <input disabled type="email" class="form-control"
                                               value="{{ (!empty($user->bank[0]))?$user->bank[0]->account_number:'' }}"
                                               id="inputEmail4" placeholder="شماره شبا">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="inputEmail4">شماره کارت</label>
                                        <div class="input-group">
                                            <input readonly class="form-control"
                                                   value="{{ (!empty($user->bank[0]))?$user->bank[0]->card_number:'' }}"
                                                   id="card-number" placeholder="شماره کارت">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-primary rounded-right copy-btn" type="button"
                                                        data-input="card-number">کپی
                                                </button>
                                            </div>
                                        </div>

                                        @if($array_steps['step.bank']['status']==1 || $array_steps['step.bank']['status']==0)
                                            <div class="row">
                                                <a class="btn btn-success text-white  mt-3"
                                                   href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.bank']['id'])}}">تایید
                                                    صلاحیت</a>
                                                <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد صلاحیت</a>
                                            </div>
                                        @elseif($array_steps['step.bank']['status']==3)
                                            <div class="row">
                                                <a class="btn btn-success text-white  mt-3"
                                                   href="{{url('admin/user/changeStatus?id='.$user->id.'&status=3&form_id='.$array_steps['step.bank']['id'])}}">تایید
                                                    صلاحیت</a>
                                                <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد صلاحیت</a>
                                            </div>
                                        @elseif($array_steps['step.bank']['status']==2)
                                            <a class="btn btn-danger text-white  mt-3" onclick="rejectUser(2,{{ $array_steps['step.bank']['id'] }})">رد صلاحیت</a>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script>
        function rejectUser(status,form_id) {

            swal("دلیل لغو رد صلاحیت خود را بنویسید:", {
                content: "input",
            })
                .then((value) => {
                    if (value !== null) {
                        $.ajax({
                            url: "{{url('admin/user/disableStatus')}}",
                            type: 'get',
                            data: {status: status, id: "{{$user->id}}", value: value,form_id: form_id, "_token": "{{csrf_token()}}"},
                            headers:
                                {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                },
                            success: function (data) {
                                swal('موفق', data['msg'], data['type']);
                                setTimeout(function () {
                                    location.reload();
                                }, 2000)
                            }
                        });
                    }
                });
        }
    </script>
@endsection
