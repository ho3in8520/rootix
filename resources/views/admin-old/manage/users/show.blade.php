@extends('templates.admin.master_page')
@section('title_browser')
    اطلاعات کاربر
@endsection
@section('style')
    <style>
        .card-border {
            box-shadow: unset !important;
            border: solid 1px lightgrey !important;
            border-radius: 4px;
        }

        .avatar {
            width: 85px;
            border-radius: 50%;
            border: solid 2px;
        }

        .custom-header {
            background: #204f6f;
            padding: 6px;
            color: #fff;
            text-align: center;
            font-weight: bold;
        }
    </style>
@endsection
@section('after-title')
    <a target="_blank" href="{{ route('admin.user.login-panel',$user) }}" class="btn btn-primary mx-2">رفتن به پنل
        کاربر</a>
@endsection
@section('content')
    <div class="card">
        <div class="card-header pl-0 pr-0">
            <ul class="nav nav-tabs card-header-tabs  ml-0 mr-0" role="tablist">
                <li class="nav-item w-20 text-center">
                    <a class="nav-link active" id="information-tab_" data-toggle="tab" href="#informationFull"
                       role="tab" aria-controls="information" aria-selected="true">
                        مشخصات کاربر
                    </a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="documents-tab_" data-toggle="tab" href="#documentsFull" role="tab"
                       aria-controls="documents" aria-selected="false">مدارک</a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="roles-tab_" data-toggle="tab" href="#rolesFull" role="tab"
                       aria-controls="roles" aria-selected="false">نقش ها</a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="settings-tab_" data-toggle="tab" href="#settingsFull" role="tab"
                       aria-controls="settings" aria-selected="false">تنظیمات</a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="wallet-tab_" data-toggle="tab" href="#wallet" role="tab"
                       aria-controls="charge-wallet" aria-selected="false">کیف پول</a>
                </li>
                <li class="nav-item w-20 text-center">
                    <a class="nav-link" id="wallet-tab_" data-toggle="tab" href="#wallet" role="tab"
                       aria-controls="reports" aria-selected="false">گزارشات</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <!-- محتویات تب اطلاعات کاربر -->
                <div class="tab-pane fade show active" id="informationFull" role="tabpanel"
                     aria-labelledby="information-tab_">

                    <!-- آواتار و اطلاعات نمایشی کاربر -->
                    <div class="card card-border mb-2 flex-md-row flex-sm-column p-2 align-items-center">
                        <div class="">
                            <img src="{{ $user->avatar }}" class="avatar"
                                 style="border-color: {{ $user->status == 0 ? '#c43d4b':'#3e884f' }}">
                        </div>
                        <div
                            class="col-md col-sm-12 d-flex flex-column justify-content-center align-items-md-start align-items-center mt-md-0 mt-2">
                            <h5 class="mb-0"> {{ $user->name }}
                                @if ($user->status == 0)
                                    <span class="badge badge-danger">غیرفعال</span>
                                @else
                                    <span class="badge badge-success">فعال</span>
                                @endif
                            </h5>
                            <p class="mb-0">سطح کاربر: {{ convertStepToPersian($user->step_complate) }}</p>
                        </div>
                    </div><!-- آواتار و اطلاعات نمایشی کاربر -->
                    <form method="post" id="personal-information"
                          action="{{ route('admin.user.edit',$user->id) }}" class="row">
                    @csrf
                        <input type="hidden" name="tab" value="information">
                    <!-- اطلاعات شخصی -->
                        <div class="card card-border mb-2">
                            <div class="custom-card-header border-bottom-2">
                                <h6 class="m-0 float-left">اطلاعات شخصی</h6>
                                <button class="btn btn-info float-right btn-sm open-edit"
                                        data-form="personal-information">
                                    ویرایش
                                </button>
                            </div>
                            <div class="card-body">

                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام</label>
                                    <input type="text" class="form-control" name="first_name"
                                           value="{{ $user->first_name }}" disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>نام خانوادگی</label>
                                    <input type="text" class="form-control" name="last_name"
                                           value="{{ $user->last_name }}" disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label> موبایل</label>
                                    <input type="text" class="form-control" name="mobile" value="{{ $user->mobile }}"
                                           disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label> تلفن</label>
                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}"
                                           disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>ایمیل</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}"
                                           disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>کد ملی</label>
                                    <input type="text" class="form-control" name="national_code"
                                           value="{{ $user->national_code }}" disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>تاریخ تولد</label>
                                    <input type="text" class="form-control date-picker" name="birth_day"
                                           value="{{ $user->birth_day }}"
                                           disabled>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>جنسیت: </label>
                                    <select class="form-control" name="gender_id" disabled>
                                        <option value="" {{ !$user->gender?'selected':'' }}>نامشخص</option>
                                        <option value="1" {{ $user->gender=='1'?'selected':'' }}>مرد</option>
                                        <option value="0" {{ $user->gender=='0'?'selected':'' }}>زن</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>وضعیت پنل</label>
                                    <select class="form-control" name="status" disabled>
                                        <option value="0" {{ $user->status == 0?'selected':'' }}>غیرفعال</option>
                                        <option value="1" {{ $user->status == 1?'selected':'' }}>فعال</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-success ajaxStore float-right" disabled>ثبت
                                    </button>
                                </div>
                            </div>
                        </div><!--## اطلاعات شخصی ##-->

                        <!-- آدرس کاربر -->
                        <div class="card card-border" style="">
                            <div class="custom-card-header border-bottom-2">
                                <h6 class="m-0 float-left"> آدرس </h6>
                                <button class="btn btn-info float-right btn-sm open-edit"
                                        data-form="address-information">
                                    ویرایش
                                </button>
                            </div>
                            <div class="card-body">
                                <div method="post" id="address-information"
                                     action="{{ route('admin.user.edit',$user->id) }}" class="row">
                                    @csrf
                                    <div class="form-group col-sm-6">
                                        <label>استان</label>
                                        <select class="form-control state" name="state" disabled>
                                            <option value="" {{ $user->state?'selected':'' }}>انتخاب کنید</option>
                                            @foreach($states as $item)
                                                <option
                                                    value="{{ $item->id }}" {{ $item->id==$user->state?'selected':'' }}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>شهر</label>
                                        <select class="form-control city" name="city" disabled>
                                            @if(count($cities) > 0)
                                                @foreach($cities as $item)
                                                    <option
                                                        value="{{ $item->id }}" {{ $item->id==$user->state?'selected':'' }}>{{$item->name}}</option>
                                                @endforeach
                                            @else
                                                <option value="" selected>ابتدا استان را انتخاب کنید</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>کد پستی</label>
                                        <input type="text" class="form-control" value="{{ $user->postal_code }}"
                                               disabled>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>آدرس</label>
                                        <textarea class="form-control" disabled>{{ $user->address }}</textarea>
                                    </div>
                                    <div class="form-group col-12">
                                        <button type="button" class="btn btn-success ajaxStore float-right" disabled>ثبت
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div><!--## آدرس کاربر ##-->
                    </form>
                </div><!--## محتویات تب اطلاعات کاربر ##-->

                <!-- محتویات تب مدارک کاربر -->
                <div class="tab-pane fade" id="documentsFull" role="tabpanel"
                     aria-labelledby="documents-tab_">

                    <!-- مدارک ارسال شده کاربر-->
                    <div class="card card-border mb-2">
                        <div class="custom-card-header border-bottom-2">
                            <h6 class="m-0 float-left">مدارک کاربر</h6>
                        </div>
                        <div class="card-body row">
                            @foreach($documents as $item)
                                <div class="col-md-3 col-ms-4 col-6">
                                    <div class="card">
                                        <div class="position-relative">
                                            <img class="card-img-top" src="{{ getImage($item->path) }}"
                                                 alt="کد ملی">
                                        </div>
                                        <div class="card-body">
                                            <p class="list-item-heading mb-4">{{ convert_upload_file_to_persian($item->type) }}</p>
                                            <footer>
                                                <p class="text-muted text-small mb-0 font-weight-light">{{ jdate_from_gregorian($item->updated_at,'Y/m/d') }}</p>
                                            </footer>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div><!--## مدارک ارسال شده کاربر ##-->

                </div><!--## محتویات تب مدارک کاربر ##-->

                <!-- محتوایات تب نقش های کاربر -->
                <div class="tab-pane fade" id="rolesFull" role="tabpanel" aria-labelledby="roles-tab_">

                    <!-- نقش های کاربر-->
                    <div class="card card-border mb-2">
                        <div class="custom-card-header border-bottom-2">
                            <h6 class="m-0 float-left">نقش‌های کاربر

                            </h6>
                            <button class="btn btn-info float-right btn-sm open-edit" data-form="roles">ویرایش
                            </button>
                        </div>
                        <form id="roles" method="post" action="{{ route('admin.user.edit',$user->id) }}">
                            @csrf
                            <div class="card-body row">
                                <div class="mb-3 form-group d-flex flex-column align-items-center col-12">
                                    <button href='#' class="btn btn-success mb-2" id='select-all'>انتخاب همه
                                        نقش ها
                                    </button>
                                    <button href='#' class="btn btn-danger" id='deselect-all'>حذف همه
                                        نقش های کاربر
                                    </button>
                                </div>
                                <input type="hidden" name="roles-user">
                                <div class="form-group col-12 d-flex flex-column align-items-center">
                                    <select multiple="multiple" id="my-select" name="roles[]">
                                        @foreach($roles as $item)
                                            <option
                                                value="{{ $item->name }}" {{ in_array($item->id,$user->roles->pluck('id')->toArray())?'selected':'' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-success ajaxStore d-block"
                                            style="margin: auto">ثبت
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--## نقش های کاربر ##-->

                </div><!--## محتوایات تب نقش های کاربر ##-->

                <!-- محتویات تب تنظیمات -->
                <div class="tab-pane fade" id="settingsFull" role="tabpanel" aria-labelledby="settings-tab_">
                    <!-- نقش های کاربر-->
                    <div class="card card-border mb-2">
                        <div class="custom-card-header border-bottom-2">
                            <h6 class="m-0 float-left">تنظیمات کاربر</h6>
                            <button class="btn btn-info float-right btn-sm open-edit" data-form="settings-user">ویرایش
                            </button>
                        </div>
                        <form id="settings-user" method="post" action="{{ route('admin.user.edit',$user->id) }}">
                            @csrf
                            <div class="card-body row">
                                <div class="form-group col-md-3 col-sm-4 col-6 row d-flex align-items-end">
                                    <label class="">روش تایید sms:</label>
                                    <div class="col">
                                        <div class="custom-switch custom-switch-secondary mb-2">
                                            @if($user->confirm_type != 'sms')
                                                <input class="custom-switch-input" id="switch2" type="checkbox"
                                                       name="confirm_type"
                                                       {{ $user->confirm_type=='sms'?'checked':'' }} disabled>
                                            @else
                                                <span class="badge badge-success">فعال</span>
                                            @endif
                                            <label class="custom-switch-btn" for="switch2"></label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="settings-user">
                                <div class="form-group col-md-3 col-sm-4 col-6">
                                    <label>رمز عبور <sub>در صورت عدم تغییر پسورد این فیلد را خالی بگذارید</sub></label>
                                    <div class="position-relative">
                                        <input type="password" class="form-control password" name="password" disabled>
                                        <div class="input-group-append m-auto show-pass-icon">
                                            <i class="fa fa-eye text-secondary"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-success ajaxStore float-right" disabled>ثبت
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div><!--## نقش های کاربر ##-->
                </div><!--## محتویات تب تنظیمات ##-->

                <!-- محتویات تب شارژ کیف پول -->
                <div class="tab-pane fade" id="wallet" role="tabpanel" aria-labelledby="wallet-tab_">
                    <div class="card">

                        <div class="card-body">
                            <h6 class="mb-4">کیف پول کاربر</h6>

                            <form action="{{route('admin.users.wallet',$user)}}" method="post">
                                @csrf
                                <div class="row mt-5">
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                        <label for="unit" class="">
                                            ارز
                                            <sub class="px-1 text-danger" id="inventory">موجودی : -</sub>
                                        </label>
                                        <select id="assets" name="unit" class="form-control unit">
                                            <option value="">انتخاب کنید...</option>
                                            @foreach($assets as $item)
                                                <option value="{{ $item->unit }}"
                                                        data-amount="{{ $item->amount }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6 amount_div">
                                        <label for="amount" class="">مقدار</label>
                                        <input id="amount" type="text" name="amount"
                                               class="form-control @error('amount') has-danger @enderror">
                                        @error('amount') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                    <div class="form-group col-lg-3 col-md-4 col-sm-6">
                                        <label for="transact_type" class="">نوع تراکنش</label>
                                        <select id="transact_type" name="transact_type" class="form-control">
                                            <option value="increase">افزایش موجودی</option>
                                            <option value="decrease">کاهش موجودی</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-3 col-12 d-flex">
                                        <button type="submit" class="btn btn-info btn-lg ajaxStore submit my-auto">ثبت
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div><!--## محتویات تب شارژ کیف پول ##-->

                <!-- محتویات تب گزارشات -->
                <div class="tab-pane fade" id="reportsFull" role="tabpanel" aria-labelledby="reports-tab_">
                    <h6 class="mb-4">گزارشات کاربر</h6>
                </div><!--## محتویات تب گزارشات ##-->
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#my-select').multiSelect({
            selectableHeader: "<div class='custom-header'>نقش های موجود</div>",
            selectionHeader: "<div class='custom-header'>نقش های کاربر</div>",
        });
        $('#select-all').click(function () {
            $('#my-select').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#my-select').multiSelect('deselect_all');
            return false;
        });
        $(".open-edit").click(function () {
            let id = $(this).data('form');
            $(`#${id} *`).removeAttr('disabled').removeClass('disabled');
            $(this).removeClass('btn-info').text('در حال ویرایش');
        });
        $("#assets").change(function () {
            let val = $(this).find('option:selected').data('amount');
            $("#inventory").text('موجودی: ' + numberFormat(val))
        })
    </script>
@endsection
