@extends('templates.admin.master_page')
@section('title_browser')
    حساب های بانکی کاربران
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <div class="row">
                <div class="col-12" id="basic-form-layouts">
                    <div class="card mb-2">
                        <div class="card-body">
                            <form method="get" action="" class="row">
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>کد کاربر: </label>
                                    <input type="text" class="form-control" name="code" value="{{ request()->code }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>بانک: </label>
                                    <input type="text" class="form-control" name="name" value="{{ request()->name }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>شماره کارت: </label>
                                    <input type="text" class="form-control" name="card_number"
                                           value="{{ request()->card_number }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>شماره حساب: </label>
                                    <input type="number" class="form-control" name="account_number"
                                           value="{{ request()->account_number }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>شبا: </label>
                                    <input type="email" class="form-control" name="sheba_number"
                                           value="{{ request()->sheba_number }}">
                                </div>
                                <div class="form-group col-md-3 col-sm-4">
                                    <label>وضعیت: </label>
                                    <select class="form-control custom-select" name="status">
                                        <option value="" {{ !request()->status?'selected':'' }}>همه</option>
                                        <option value="0" {{ request()->status==='0'?'selected':'' }}>تازه ایجاد شده
                                        </option>
                                        <option value="1" {{ request()->status==1?'selected':'' }}>تایید شده</option>
                                        <option value="2" {{ request()->status==2?'selected':'' }}>ویرایش شده</option>
                                        <option value="3" {{ request()->status==3?'selected':'' }}>رد شده</option>
                                    </select>
                                </div>
                                <div class="form-group col-12">
                                    <button type="button" class="btn btn-success float-right search-ajax">جستجو</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card m-b-30">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>بانک</th>
                                        <th>کد کاربر</th>
                                        <th>شماره حساب</th>
                                        <th>شماره کارت</th>
                                        <th>شماره شبا</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($banks) > 0)
                                        @foreach($banks as $bank)
                                            <tr>
                                                <td>{{ $bank->name }}</td>
                                                <td>{{ $bank->user->code }}</td>
                                                <td>{{ $bank->account_number }}</td>
                                                <td>{{ $bank->card_number }}</td>
                                                <td>{{ $bank->sheba_number }}</td>
                                                <td>
                                                <span class="badge badge-{{ $bank->status_color }}">
                                                    {{ $bank->status_text }}
                                                </span>
                                                    @if($bank->status==3)
                                                        <br>
                                                        <sub class="text-dark">دلیل: {{ $bank->reject_reason }}</sub>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($bank->status!=3)
                                                        <form method="post" class="d-inline"
                                                              action="{{ route('admin.bank.reject',[$bank]) }}">
                                                            @csrf
                                                            <input type="hidden" name="reject_reason" value="">
                                                            <button type="button" data-swal="0" class="btn btn-danger p-2 reject-btn ajaxStore">
                                                                رد کردن
                                                            </button>
                                                        </form>
                                                    @endif
                                                    @if($bank->status!=1)
                                                        <form method="post" class="d-inline"
                                                              action="{{ route('admin.bank.confirm',[$bank]) }}">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $bank->id }}">
                                                            <button type="button" class="btn btn-success p-2 ajaxStore">
                                                                تایید کردن
                                                            </button>
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="10" class="text-center">موردی برای نمایش وجود ندارد!</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                {!! $banks->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#basic-form-layouts').delegate(".reject-btn", 'click', function (event) {
            let elm= $(this);
            if (elm.attr('data-swal') == '1') {
                return;
            }
            event.stopImmediatePropagation();
            swal("دلیل رد صلاحیت خود را بنویسید:", {
                content: "input",
            })
                .then((value) => {
                    if (value != null && value) {
                        elm.closest('form').find('input[name=reject_reason]').val(value);
                        elm.attr('data-swal','1');
                        elm.click();
                    }else {
                        elm.attr('data-swal','0');
                    }
                });
        })
    </script>
@endsection
