@extends('templates.admin.master_page')
@section('title_browser')
    {{ isset($currency)?"ویرایش '{$currency->full_name}'":'ایجاد ارز جدید' }}
@endsection
@section('after-title')
    <div class="text-zero top-right-button-container">
        <a href="{{ route('currencies.index') }}" class="btn btn-primary btn-lg top-right-button mr-1">لیست ارزها</a>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12 mb-4">
            <div class="card h-100">
                <div class="card-body row">
                    <form method="post" class="col-12 row" action="@if(isset($currency)) {{ route('currencies.update',$currency) }} @else {{ route('currencies.store') }} @endif" enctype="multipart/form-data">
                        @csrf
                        @if(isset($currency)) {{ method_field('put') }} @endif
                        <div class="form-group mb-3 col-md-6">
                            <label>نام کامل ارز</label>
                            <input type="text" class="form-control" name="full_name" placeholder="نام ارز" value="{{ isset($currency)?$currency->full_name:'' }}">
                        </div>
                        <div class="form-group mb-3 col-md-6">
                            <label>نام مخفف ارز</label>
                            <input type="text" class="form-control" name="exclusive_name" placeholder="نام مخفف ارز" value="{{ isset($currency)?$currency->exclusive_name:'' }}">
                        </div>
                        <div class="form-group mb-3 col-md-6">
                            <label>قیمت</label>
                            <input type="text" onkeyup="$(this).val(numberFormat( $(this).val() ))" min="0" class="form-control" name="price" placeholder="قیمت" value="{{ isset($currency)?$currency->price:'' }}">
                        </div>
                        <div class="form-group row mb-1 ">
                            <label class="col-12 col-form-label">وضیت</label>
                            <div class="col-12">
                                <div class="custom-switch custom-switch-secondary mb-2">
                                    <input class="custom-switch-input" name="status" id="switch2" type="checkbox" @if( (isset($currency) && $currency->status==1) || !isset($currency) ) checked @endif >
                                    <label class="custom-switch-btn" for="switch2"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-1 ">
                            <label class="col-12 col-form-label">اضافه به دارایی</label>
                            <div class="col-12">
                                <div class="custom-switch custom-switch-secondary mb-2">
                                    <input class="custom-switch-input" name="add_to_asset" id="switch3" type="checkbox" @if( (isset($currency) && $currency->add_to_asset==1) || !isset($currency) ) checked @endif >
                                    <label class="custom-switch-btn" for="switch3"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3 col-md-6">
                            <div class=" image-infomation">
                                <div class="card-body">
                                    <label>عکس</label>
                                    <div id="deleteImage">
                                    </div>
                                    <img title="" style="cursor:pointer;height: 75px; margin: auto; display: block" id="blah"
                                         src="{{ (isset($currency) && !empty($currency->files))?getImage($currency->files[0]->path):asset('theme/user/assets/img/no-image.png') }}"
                                         alt="your image" height="75">
                                    <input accept="image/jpeg" name="image" class="imgInp d-none"
                                           type="file"
                                           style="display: none">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <button class="btn btn-success d-block ml-auto ajaxStore" @if(isset($currency)) data-reload="false" @endif>{{ isset($currency)?'ویرایش کردن':'اضافه کردن' }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("input[name='price']").keyup();
        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // $('#blah').attr('src', e.target.result);
                    $(input).closest('.card-body').find("img").attr('src', e.target.result)
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(".imgInp").change(function () {
            previewImage(this);
            if ($(this).val() != "")
                $(this).closest('.card-body').find("#deleteImage").html("<i id='deleteImageUpload' title=\"حذف عکس\" class=\"fa fa-trash text-danger\"></i>")
        });

        $(document).on('click', '#blah', function () {
            $(this).closest('.card-body').find("input").click();
        });
        $(document).on('click', '#deleteImageUpload', function () {
            $(this).closest('.card-body').find(".imgInp").val('')
            $(this).closest('.card-body').find("#blah").attr('src', "{{ asset('theme/user/assets/img/no-image.png') }}");
            $(this).closest('.card-body').find("#deleteImage").html('')
        });
    </script>
@endsection
