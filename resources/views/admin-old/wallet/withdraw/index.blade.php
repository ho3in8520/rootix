@extends('templates.admin.master_page')
@section('title_browser')
    لیست درخواست های برداشت
@endsection

@section('content')
    <section id="basic-form-layouts">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <form action="" method="get" style="display:contents">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>کد کاربر</label>
                                        <input type="text" class="form-control" name="code" value="{{ request()->query('code') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>ایمیل کاربر</label>
                                        <input type="text" class="form-control" name="email" value="{{ request()->query('email') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>مبلغ</label>
                                        <input type="text" class="form-control" name="amount" value="{{ request()->query('amount') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>وضعیت</label>
                                        <select class="form-control" name="status">
                                            <option value="" {{ request()->query('status')==''?'selected':'' }}>انتخاب کنید...</option>
                                            <option value="3" {{ request()->query('status')==3?'selected':'' }}>درحال بررسی</option>
                                            <option value="1" {{ request()->query('status')==1?'selected':'' }}>تایید شده</option>
                                            <option value="2" {{ request()->query('status')==2?'selected':'' }}>رد شده</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>تاریخ درخواست</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="date_from"  value="{{ request()->query('date_from') }}"
                                                   placeholder="از">
                                            <input type="text" class="form-control" name="date_to" value="{{ request()->query('date_to') }}"
                                                   placeholder="تا">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" class="btn btn-primary search-ajax mt-4">اعمال</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">کد کاربری</th>
                                    <th scope="col">ایمیل</th>
                                    <th scope="col">مبلغ</th>
                                    <th scope="col">وضعیت</th>
                                    <th scope="col">تاریخ درخواست</th>
                                    <th scope="col">عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($withdraws->count() > 0)
                                @foreach($withdraws as $withdraw)
                                    <tr>
                                        <th scope="row">{{ $withdraw->user->code }}</th>
                                        <td>{{ $withdraw->user->email }}</td>
                                        <td>{{ $withdraw->amount }}</td>
                                        <td>{!! $withdraw->status() !!}</td>
                                        <td dir="ltr">{{ jdate_from_gregorian($withdraw->created_at,'Y/m/d | H:i:s') }}</td>
                                        <td>
                                            <a class="btn btn-primary btn-sm" title="نمایش" href="{{ route('admin.request.withdraw.show',$withdraw->id) }}"><i
                                                    class="fa fa-eye"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                                    @else
                                <tr>
                                    <th colspan="6" class="text-center">موردی یافت نشد!</th>
                                </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        {!! $withdraws->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
