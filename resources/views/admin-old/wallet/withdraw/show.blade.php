@extends('templates.admin.master_page')
@section('title_browser')
    درخواست برداشت
@endsection

@section('content')
    <section id="basic-form-layouts">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h3>مشخصات کاربر</h3>
                        <hr class="mt-0">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>نام و نام خانوادگی</label>
                                    <input class="form-control" value="{{ $result->user->full_name }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>کد کاربر</label>
                                    <input class="form-control" value="{{ $result->user->code }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>ایمیل کاربر</label>
                                    <input class="form-control" value="{{ $result->user->email }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>سطح کاربر</label>
                                    <input class="form-control" value="{{ $result->user->step_complate }}" disabled>
                                </div>
                            </div>
                        </div>
                        <h3 class="mt-4">اطلاعات درخواست</h3>
                        <hr class="mt-0">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>نام ارز</label>
                                    <input class="form-control" value="{{ strtoupper($result->unit) }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>شبکه</label>
                                    <input class="form-control" value="{{ strtoupper($result->network) }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>مبلغ درخواست شده با احتساب کارمزد</label>
                                    <input class="form-control" value=" {{ $result->amount - $result->fee }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>کارمزد</label>
                                    <input class="form-control" value="{{ $result->fee }}" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>موجودی ارز در کیف پول ادمین</label>
                                    <span class="spinner-border spinner-border-sm"></span>
                                    <input class="form-control amount-admin" value="" disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>آدرس کیف پول مقصد</label>
                                    <div class="input-group">
                                        <input class="form-control" id="address" value="{{ $result->address }}" readonly>
                                        <div class="input-group-append">
                                            <button type="button" class="btb btn-primary copy-btn" data-input="address"><i class="fa fa-copy"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>وضعیت درخواست</label>
                                    {!! $result->statusAdmin('input') !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>تاریخ ایجاد درخواست</label>
                                    <input class="form-control" dir="ltr"
                                           value="{{ jdate_from_gregorian($result->created_at,'Y/m/d || H:i:s') }}"
                                           disabled>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>علت عدم تایید</label>
                                    <input class="form-control" value="{{ $result->reject_reason }}" disabled>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-0">
                        @if($result->status==0 || $result->status==3)
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-success confirm">تایید کردن</button>
                                    <button type="button" class="btn btn-danger reject" data-toggle="modal"
                                            data-target="#exampleModal">رد کردن
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @if($result->status==0 || $result->status==3)
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">رد کردن درخواست</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{ route("admin.request.withdraw.update-status") }}">
                        @csrf
                        <input type="hidden" name="mode" value="2">
                        <input type="hidden" name="key" value="{{ $result->id }}">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>علت عدم تایید</label>
                                    <textarea class="form-control" rows="6" name="reject_reason"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                            <button type="button" class="btn btn-primary ajaxStore">ثبت</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $.post(`{{ route('admin.request.withdraw.balance-admin') }}`, {
                _token: `{{ csrf_token() }}`,
                unit: `{{ $result->unit }}`
            }, balance_admin)

            function balance_admin(response) {
                if (response.status == 100) {
                    $(".spinner-border").hide();
                    $(".amount-admin").val(response.amount)
                }
            }

            $(".confirm").click(function () {
                swal({
                    title: "اطمینان از تایید درخواست دارید؟",
                    text: "پس از تایید درخواست به سمت بلاکچین ارسال میشود و امکان لغو درخواست وجود ندارد",
                    icon: "warning",
                    buttons: ['انصراف', 'تایید'],
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.post(`{{ route('admin.request.withdraw.update-status') }}`, {
                                _token: `{{ csrf_token() }}`
                                , mode: 1, key: `{{ $result->id }}`
                            }, function (response) {
                                if (response.status == 100) {
                                    swalResponse(response.status, response.msg, 'موفق');
                                    setTimeout(function () {
                                        window.location.href = response.url;
                                    }, 3000)
                                } else {
                                    swalResponse(response.status, response.msg, 'خطا');
                                }
                            });
                        }
                    });
            })
        })
    </script>
@endsection
